function testscript

%% read data file
filename = '11421002.abf';
[d, si, abfHeader]=abfload(filename);

%% figure out the channel to analyse
intraMatchString =  '._Vm_sca';
VRMatchString = 'IN 10';
intraNo = 0;
VRNo = 0;

IntraTemplate = struct (...
    'raw',[],...
    'si',si,...
    'detectionCommand',[],... % exact command used to detect this
    'DetectionOption',[],... % detection option
    'detectedIndex',[],... % index of the detected
    'isDeleted',[],... % binary array, it should be the same size with 'detected' field, 1 represents delete
    'added',[]...
    );

VRTemplate = struct(...
    'raw',[],...
    'si',si,...
    'detectionCommand',[],... % exact command used to detect this
    'DetectionOption',[],... % detection option
    'detectedIndex',[],... % index of the detected
    'isDeleted',[],... % binary array, it should be the same size with 'detected' field, 1 represents delete
    'addedIndex',[]...
    );


% I should make this part to a function so that I can extend this part easily
% 
for iCh = 1:numel(abfHeader.recChNames)
    intraMatchStart = regexp(abfHeader.recChNames{iCh}, intraMatchString,'start');
    
    if   intraMatchStart == 1
        intraNo = intraNo +1;
        intra{intraNo} = IntraTemplate;
        eval(['intra{',abfHeader.recChNames{iCh}(intraMatchStart),'}.raw = d(:,iCh);']);
    end
    
    VRMatchStart = regexp(abfHeader.recChNames{iCh}, VRMatchString, 'start');
    
    if VRMatchStart == 1
        VRNo = VRNo +1;
        VR{VRNo} = VRTemplate;
        VR{VRNo}.raw = d(:,iCh);
    end
end

% clear iCh d  VRMatchStart intraMatchStart;

%% process VR recordings

iVR = 1;
display('detecting spikes ... ');
VR{iVR}.detectionCommand = 'mkDetectVrSpikes';
VR{iVR}.DetectionOption = struct(...
    'Wid', [0.5 1],...
    'Ns', 6,...
    'option','l',...
    'L', 0.05,...
    'wname','bior1.5');

VR{iVR} = mkDetectVrSpikes(VR{iVR});
display('done!');
    
%% need to figure out how to omit spikes related to electrical shock

%% check spike detection of VR

d = VR{iVR}.raw; si = VR{iVR}.si;
t = (0:numel(d)-1).*si*10^-6;
Event.original = VR{iVR}.detectedIndex;
Event.isDeleted = VR{iVR}.isDeleted;
Event.added = [];

% plot raw signal
signalRange =[-0.2 0.2];
monitorPosition = get(0, 'MonitorPosition');
signalName = 'VR1';
figurePosition = [monitorPosition(1,1),monitorPosition(1,4)/4, monitorPosition(1,3),monitorPosition(1,4)/2];
hFigure = figure('NumberTitle', 'off', 'Name', signalName,...
    'OuterPosition',figurePosition);
xMargin = 0.05;
yMargin = 0.1;
hAx = axes('Position', [xMargin yMargin 1-2*xMargin 1-2*yMargin]);

h = mkTurboPlot((0:numel(d)-1).*si*10^-6,d,'k',signalRange);

% edit spikes
toleranceWindow = 1*10^-3 % tolerance window to select spikes [s]

isEditingSpikes='a';
while isEditingSpikes ~= 'q'
%     h_s=[]; %handle for lines representing spikes
%     h_s=mark_lines(TE*si,h_f,h_s,'r');
    hLine = updateEventPlot(hFigure,[],Event,signalRange)
    edit_spikes = input('Add or Delete spikes? Press q if you are done. [a/d/q]: ', 's');
%     h_c=[]; %handle for lines to confirm
    if edit_spikes == 'a'
        display('Adding spikes ...');
        display('Mark spike onsets. Press Enter if you are done.');
        [x, y] = ginput2('g*');
        hLine = updateEventPlot(hFigure,[],Event,signalRange)
        accept = input('Accept these spikes? [y/n]: ', 's');
        if accept == 'y'
            TE=sort([TE round(x/si)']);
        end
    elseif edit_spikes == 'd'
        display('Deleting spikes ...');
        display('Mark spike onsets. Press Enter if you are done.');
        [x, y] = ginput2('b*');
        del_idx=[];
        for i=1:length(x)
            del_idx=[del_idx ...
                find(round(x(i)/si)-tol_win/si < TE & TE < round(x(i)/si)+tol_win/si)];
        end
        h_c=mark_lines(TE(del_idx)*si,h_f,h_c,'b');
        accept = input('Accept deleting these spikes? [y/n]: ', 's');
        
        if accept == 'y'
            TE(del_idx)=[];
        end
    end
    delete(h_c);delete(h_s);
end

function hLine = updateEventPlot(hFigure,hLine,Event,signalRange)
    figure(hFigure);
    delete(hLine);
    reserved = Event.original(find(Event.isDeleted == 0));
    deleted = Event.original(find(Event.isDeleted == 1));
    added = Event.added;
    hLine.Reserved = arrayfun(@(x)line([x x],signalRange./4,'Color',[0 1 0]),(reserved-1).*si*10^-6);
    hLine.Deleted = arrayfun(@(x)line([x x],signalRange./4,'Color',[0 0 1]),(deleted-1).*si*10^-6);
    hLine.Added = arrayfun(@(x)line([x x],signalRange./4,'Color',[1 0 0]),(added-1).*si*10^-6);
end