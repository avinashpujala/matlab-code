function [epi, vr, si] = mk_vr_freq_auto()
[FileName, PathName, FilterIndex] = uigetfile('.abf','Select abf file');
cd(PathName);
[d, si,abfHeader] = abfload(FileName);

%% figure out the channel to analyse
VRMatchString = 'IN 10';
VRNo = 0;

VRTemplate = struct(...
    'raw',[],...
    'si',si,...
    'detectionCommand',[],... % exact command used to detect this
    'DetectionOption',[],... % detection option
    'detectedIndex',[],... % index of the detected
    'curatedIndex',[]... % index of the curated
    );


% I should make this part to a function so that I can extend this part easily
% 
for iCh = 1:numel(abfHeader.recChNames)    
    VRMatchStart = regexp(abfHeader.recChNames{iCh}, VRMatchString, 'start');
    
    if VRMatchStart == 1
        VRNo = VRNo +1;
        VR{VRNo} = VRTemplate;
        VR{VRNo}.raw = d(:,iCh);
    end
end

% clear iCh d  VRMatchStart intraMatchStart;

%% process VR recordings

iVR = 1;
display('detecting spikes ... ');
VR{iVR}.detectionCommand = 'mkDetectVrSpikes';
VR{iVR}.DetectionOption = struct(...
    'Wid', [0.5 1],...
    'Ns', 6,...
    'option','l',...
    'L', 0.05,...
    'wname','bior1.5');

VR{iVR} = mkDetectVrSpikes(VR{iVR});
display('done!');

%%

vr =VR{iVR}.raw; si = VR{iVR}.si;
TE =VR{iVR}.detectedIndex;
h_f=figure('Name',FileName);
subplot('Position',[0.05 0.05 0.9 0.9])
hold on;
plotLDS(5e5,(0:length(vr)-1)*si, vr,'k');
YLIM=[-0.6 0.6];
ylim(YLIM);

h = zoom;
set(h,'Motion','horizontal','Enable','on');
h = pan;
set(h,'Motion','horizontal','Enable','on');

tol_win = 1*10^3 % tolerance window to select spikes [us]

edit_spikes='a';
while edit_spikes ~= 'q'
    h_s=[]; %handle for lines representing spikes
    h_s=mark_lines(TE*si,h_f,h_s,'r');
    edit_spikes = input('Add or Delete spikes? Press q if you are done. [a/d/q]: ', 's');
    h_c=[]; %handle for lines to confirm
    if edit_spikes == 'a'
        display('Adding spikes ...');
        display('Mark spike onsets. Press Enter if you are done.');
        [x, y] = ginput2('g*');
        h_c=mark_lines(x,h_f,h_c,'g');
        accept = input('Accept these spikes? [y/n]: ', 's');
        if accept == 'y'
            TE=sort([TE round(x/si)']);
        end
    elseif edit_spikes == 'd'
        display('Deleting spikes ...');
        display('Mark spike onsets. Press Enter if you are done.');
        [x, y] = ginput2('b*');
        del_idx=[];
        for i=1:length(x)
            del_idx=[del_idx ...
                find(round(x(i)/si)-tol_win/si < TE & TE < round(x(i)/si)+tol_win/si)];
        end
        h_c=mark_lines(TE(del_idx)*si,h_f,h_c,'b');
        accept = input('Accept deleting these spikes? [y/n]: ', 's');
        
        if accept == 'y'
            TE(del_idx)=[];
        end
    end
    delete(h_c);delete(h_s);
end

h_s=[];
h_s=mark_lines(TE*si,h_f,h_s,'r');

VR{iVR}.curatedIndex = TE;

%defining episodes
min_burst_delay = 10*10^3 % [us]
min_episode_delay = 100*10^3 % [us] 
epi=[];
if length(TE)>1
    noepi=1;
    noburst=1;% in the episode
    epi{noepi}.burst{noburst}.spikes=TE(1)*si;
    for i=2:length(TE)
        dt=(TE(i)-TE(i-1))*si;
        if dt<min_burst_delay
            tmp=epi{noepi}.burst{noburst}.spikes;
            epi{noepi}.burst{noburst}.spikes=[tmp TE(i)*si];
        elseif min_burst_delay<=dt & dt<min_episode_delay
            noburst=noburst+1;
            epi{noepi}.burst{noburst}.spikes=TE(i)*si;
        elseif min_episode_delay < dt
            noepi=noepi+1;
            noburst=1;
            epi{noepi}.burst{noburst}.spikes=TE(i)*si;
        end
    end
end

for i=1:length(epi)
    onsets=[];
    for j=1:length(epi{i}.burst)
        onsets=[onsets min(epi{i}.burst{j}.spikes)*10^-6];
        if length(epi{i}.burst{j}.spikes)>1
            burst=[epi{i}.burst{j}.spikes(1), epi{i}.burst{j}.spikes(end)];
        elseif length(epi{i}.burst{j}.spikes) == 1
            burst=epi{i}.burst{j}.spikes(1)+[-0.5 0.5]*10^3;
        end
        patch([burst, burst(end:-1:1)],[YLIM(1), YLIM(1), YLIM(2), YLIM(2)],...
            'g', 'FaceAlpha', 0.3,'EdgeColor','none');
        
    end
    freq=1./diff(onsets);
    for k=1:length(freq)
        text(mean(onsets(k:k+1))*10^6, 0.2, [num2str(freq(k), '%0.1f'),'Hz'],...
            'HorizontalAlignment','center');
    end
    epi{i}.onsets=onsets;
    epi{i}.freq=freq;
    episode=[onsets(1),onsets(end)]*10^6;
    patch([episode, episode(end:-1:1)],[YLIM(1), YLIM(1), YLIM(2), YLIM(2)],...
        'r', 'FaceAlpha', 0.3,'EdgeColor','none');     
end

    function h_l=mark_lines (t,h_f,h_l,LineSpec)
        figure(h_f);
        delete(h_l);
        for i=1:length(t)
            h_l(i)=plot((t(i)-1)*[1 1], [-0.1 0.1], LineSpec);
        end
    end

save([FileName(1:end-4),'_epi_',datestr(clock,30)], ...
    'epi','VR');
end