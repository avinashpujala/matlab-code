function [intra] = intraSpikeDetection(channelName,filename)
% intraSpikeDetection
%
%

if nargin < 2
    [FileName, PathName] = uigetfile('.abf','Select abf file');
    [d, si,abfHeader] = abfload([PathName, FileName], channelname);
end
if nargin < 1
    warning('Please specify channelname');
end
%% figure out the channel to analyse
intraMatchString =  '._Vm_sca';
intraNo = 0;

IntraTemplate = struct (...
    'raw',[],...
    'si',si,...
    'detectionCommand',[],... % exact command used to detect this
    'DetectionOption',[],... % detection option
    'detectedIndex',[],... % index of the detected
    'curatedIndex',[]... % index of the curated
    );

% I should make this part to a function so that I can extend this part easily
% 
for iCh = 1:numel(abfHeader.recChNames)
    intraMatchStart = regexp(abfHeader.recChNames{iCh}, intraMatchString,'start');
    
    if   intraMatchStart == 1
        intraNo = intraNo +1;
        intra{intraNo} = IntraTemplate;
        eval(['intra{',abfHeader.recChNames{iCh}(intraMatchStart),'}.raw = d(:,iCh);']);
    end
end

% clear iCh d  VRMatchStart intraMatchStart;

%% detect intracellular spikes 
% for iIntra=1:intraNo

iIntra = 2
raw = intra{iIntra}.raw;
command = 'findpeaks';
option{1,1} = 'MINPEAKHEIGHT';         option{2,1} = -20;
option{1,2} = 'MINPEAKDISTANCE';         option{2,2} = 10^3/si;
indexs = [];
eval(['[~, indexs]=',command,'(raw,option{:});']);
intra{iIntra}.detectionCommand = command;
intra{iIntra}.DetectionOption = cell2struct(option,option(1,:),2);
intra{iIntra}.detectedIndex = indexs;


%%

TE =intra{iIntra}.detectedIndex;
h_f=figure('Name',FileName);
subplot('Position',[0.05 0.05 0.9 0.9])
hold on;
plotLDS(5e5,(0:length(raw)-1)*si, raw,'k');
signalRange=[-80 30];
ylim(signalRange);

h = zoom;
set(h,'Motion','horizontal','Enable','on');
h = pan;
set(h,'Motion','horizontal','Enable','on');

tol_win = 1*10^3 % tolerance window to select spikes [us]

edit_spikes='a';
while edit_spikes ~= 'q'
    h_s=[]; %handle for lines representing spikes
    h_s=mark_lines(TE*si,h_f,h_s,'r');
    edit_spikes = input('Add or Delete spikes? Press q if you are done. [a/d/q]: ', 's');
    h_c=[]; %handle for lines to confirm
    if edit_spikes == 'a'
        display('Adding spikes ...');
        display('Mark spike onsets. Press Enter if you are done.');
        [x, y] = ginput2('g*');
        h_c=mark_lines(x,h_f,h_c,'g');
        accept = input('Accept these spikes? [y/n]: ', 's');
        if accept == 'y'
            TE=sort([TE round(x/si)']);
        end
    elseif edit_spikes == 'd'
        display('Deleting spikes ...');
        display('Mark spike onsets. Press Enter if you are done.');
        [x, y] = ginput2('b*');
        del_idx=[];
        for i=1:length(x)
            del_idx=[del_idx ...
                find(round(x(i)/si)-tol_win/si < TE & TE < round(x(i)/si)+tol_win/si)];
        end
        h_c=mark_lines(TE(del_idx)*si,h_f,h_c,'b');
        accept = input('Accept deleting these spikes? [y/n]: ', 's');
        
        if accept == 'y'
            TE(del_idx)=[];
        end
    end
    delete(h_c);delete(h_s);
end

intra{iIntra}.curatedIndex = TE;

h_s=[];
h_s=mark_lines(TE*si,h_f,h_s,'r');


    function h_l=mark_lines (t,h_f,h_l,LineSpec)
        figure(h_f);
        delete(h_l);
        for i=1:length(t)
            h_l(i)=plot((t(i)-1)*[1 1], signalRange/4, LineSpec);
        end
    end

save([FileName(1:end-4),'_intra_',datestr(clock,30)], ...
    'intra', 'si');
end