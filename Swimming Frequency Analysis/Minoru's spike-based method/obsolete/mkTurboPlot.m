function varargout = mkTurboPlot(x, y, s, yRange)
% TPLOT   TURBO Linear plot.
% 
%	TPLOT works in similar manner to PLOT but can display very large data
%	sets much faster. Zoom and resize are also much faster than PLOT.
% 
%	TPLOT(X,Y) plots vector Y versus vector X. If X or Y is a matrix,
%	then the vector is plotted versus the rows or columns of the matrix,
%	whichever line up. The function asserts that X is uniformly spaced
%	monotonically increasing vector.
%	X can also be a 2 element vector [X0 X1], in which case the function
%	creates a uniformaly spaced vector X between these limits.
%	If X and/or Y are complex, the imaginary parts are ignored.
% 
%	Please note that function creates and plots a set of local minima and
%	maxima. The use of the AXIS function after TPLOT may result in invalid
%   displayed data. To invoke the invoke the internal update_plot function
%   resize the figure or type:
%
%   zoom(1);
%
%   TPLOT(Y) plots the columns of Y versus their index.
%
%   Various line types, plot symbols and colors may be obtained with
%   TPLOT(X,Y,S) where S is a character string made from one element
%   from any or all the following 3 columns:
%
%          b     blue          .     point              -     solid
%          g     green         o     circle             :     dotted
%          r     red           x     x-mark             -.    dashdot 
%          c     cyan          +     plus               --    dashed   
%          m     magenta       *     star             (none)  no line
%          y     yellow        s     square
%          k     black         d     diamond
%          w     white         v     triangle (down)
%                              ^     triangle (up)
%                              <     triangle (left)
%                              >     triangle (right)
%                              p     pentagram
%                              h     hexagram
%                         
%   For example, TPLOT(X,Y,'c+:') plots a cyan dotted line with a plus 
%   at each data point; PLOT(X,Y,'bd') plots blue diamond at each data 
%   point but does not draw any line.
%   Please note that some markers may be omitted. Only local maxima and
%   minima are shown.
%
%   The TPLOT command, if no color is specified, makes automatic use of
%   the colors specified by the axes ColorOrder property.  By default,
%   TPLOT cycles through the colors in the ColorOrder property.  For
%   monochrome systems, TPLOT cycles over the axes LineStyleOrder property.
%
%   If you do not specify a marker type, TPLOT uses no marker. 
%   If you do not specify a line style, TPLOT uses a solid line.
%
%   TPLOT returns a column vector of handles to lineseries objects, one
%   handle per plotted line. 
%
%   Example:
%      N = 1e7;
%      x = 0:2*pi/(N-1):2*pi;
%      y = [sin(x) + randn(1,N); sin(x)];
%      tplot(x,y)

% Copyright (c) 2010 Yuval Cohen - Audio Pixels LTD.
% email: yuval@audiopixels.com
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without 
% modification, are permitted provided that the following conditions are 
% met:
% 
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the
%       distribution
%       
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
% IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
% THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
% PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
% EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
% PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
% PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
% LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
% NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

if nargin<4
    yRange = [min(y) max(y)];
end
if nargin<3;
	s = '';
end
if nargin<2;
	y = x;
	x = [1 length(y)];
end

% check input variable X
if size(x,1)~=1
	if size(x,2)==1
		x = x';
	else
		error('X must be a single line vector');
	end
end
if ~isreal(x)
	x = real(x);
end

% check input variable Y
N	= size(x,2);
if N~=2
	[x, xi]		= sort(x);
	if size(y,2)~=N
		if size(y,1)==N
			y = y';
		else
			error('Vectors must be the same lengths.');
		end
	end
	y			= y(:,xi);
end
if ~isreal(y)
	y = real(y);
end

% x ranges
x	= [x(1) x(end)];

h		= plot(x,[min(y,[],2) max(y,[],2)],s);
% axis tight 

data.x	= x;
data.y	= y;
data.h	= h;
% data.axis = axis;
data.axis = [x yRange];

% take care of resize
set(gcf, 'UserData', data, 'ResizeFcn', @update_plot);

% zoom 
hZoom = zoom;
set(hZoom,'ActionPostCallback',@update_plot);

% pan added by Minoru
hPan = pan;
set(hPan,'Motion','horizontal','Enable','on',...
    'ActionPostCallback',@update_plot);

if nargout
	varargout{1} = h;
end


% -------------------------------------------------------------------------
function update_plot(~, ~)
newLim	= get(gca,'XLim');
data	= get(gcf,'UserData');
x		= linspace(data.x(1),data.x(2),size(data.y,2));
x0		= length(find(x<newLim(1)))+1;
x1		= length(find(x<=newLim(2)));

if x1-x0<10
	return
end
ax_h = gca;
set(ax_h,'Units','Pixels');

pos	= round(get(ax_h,'Position'));
set(ax_h,'Units','Normalized');

N	= x1-x0+1;
w	= pos(3);
k	= size(data.y,1); % the number of y data sets

if N<w*10
	% plot as is
	xx	= x(x0:x1);
	for yi		= 1:k;
		yy	= data.y(yi,x0:x1);
		set(data.h(yi), 'XData', xx, 'YData', yy);
	end
else
	% min max
	m			= floor(N/(w-1));
	x1			= x0+m*(w-1);
	xx			= reshape([1;1] * x(x0:m:x1+m-1),1,w*2);
	for yi		= 1:k;
		if m*w>N
			% padd vector with last valus
			n		= x1-x0+1;
			padd	= [min(data.y(yi,x1+1:end)) max(data.y(yi,x1+1:end))*ones(1,m*w-n-1)];
			y_matrix	= reshape([data.y(yi,x0:x1) padd], m, w);
		else
			y_matrix	= reshape(data.y(yi,x0:x1+m-1), m, w);
		end
		yy		= reshape([min(y_matrix,[],1); max(y_matrix,[],1)],1,w*2);
		set(data.h(yi), 'XData', xx, 'YData', yy);
	end
end
axis([x(x0) x(x1) data.axis(3:4)]);
