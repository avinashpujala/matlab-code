function  [X,Y,BUTTON,SCALEMAT] = ginput2(varargin)
% GINPUT2 - Graphical input from mouse with zoom, plot and georeference.
%   
%   Syntax:
%                     XY    = ginput2;
%                  [X,Y]    = ginput2;
%           [X,Y,BUTTON]    = ginput2;
%     [X,Y,BUTTON,SCALEMAT] = ginput2;
%                     [...] = ginput2(SCALEOPT);
%                     [...] = ginput2(PLOTOPT);
%                     [...] = ginput2(SCALEOPT,PLOTOPT);
%                     [...] = ginput2(N,...);
%
%   Input:
%     SCALEOPT - Single logical specifying whether the IMAGE should be
%                interactively scaled (georeferenced), or it can be the 2x4
%                SCALEMAT matrix for automatically scaling.
%                Default: false (do not scales/georeferences)
%     PLOTOPT  - String and/or parameter/value pairs specifying the drawn
%                points properties (see PLOT for details). 
%                Default: 'none' (do not plot any point)
%     N        - Number of points to be selected. One of 0,1,2,...,Inf
%                Default: Inf (selects until ENTER or ESCAPE is pressed)
%
%   Output:
%     XY        - X- and Y-coordinate(s).
%     X         - X-coordinate(s).
%     Y         - Y-coordinate(s).
%     BUTTON    - Pressed button.
%     SCALEMAT  - 2x4 matrix specifying the coordinates of two different
%                 points (1) and (2) in the Actual image coordinates and 
%                 the Real one: [Ax1 Ay1 Ax2 Ay1; Rx1 Ry1 Rx2 Ry2] to
%                 be use for scaling/georeferencing.
%
%   Description:
%     This program uses MATLAB's GINPUT function to get the coordinates
%     of a mouse-selected point in the current figure (see GINPUT for
%     details), but with four major improvements:
%                  1. ZOOMING
%                  2. DELETING
%                  3. PLOTING.
%                  4. SCALING or GEOREFERENCE IMAGES.
%     The differences are:
%      a) Obviously, the SCALEOPT and PLOTOPT optional arguments.
%      b) When click is made outside the axes, it is ignored.
%      c) When LEFT-click, the point is selected (normal).
%      d) When RIGHT-click, ZOOM IN is performed right into the selected
%         point (PANNING).
%      e) When DOUBLE-click, ZOOM OUT is done.
%      f) When MIDDLE-click, ZOOM RESET is done (see ZOOM for details).
%      g) When pressed any KEY follows the next rules: 
%          A) If ENTER is pressed, the selection is terminated. If no point
%             was already selected, the outputs are empty's.
%          B) If BACKSPACE key is pressed, the last selected point is
%             deleted.
%          C) If (almost?) any other KEY is pressed the mouse current
%             position or NANs coordinates are given, depending whether the
%             mouse was inside or outside any of the current figure axes,
%             respectively. In this latter case, the selection is NOT
%             counted as one of the N points. Besides, when drawing the
%             color is changed. 
%
%   Notes:
%     * The function can be used for interactively digitalize/vectorize
%       RASTER images.
%     * The function can be used only as a georeference function with 
%       >> ginput2(0,true)
%     * The scale/georeference only works when the current axes has an
%       IMAGE type children (see Image for details). 
%     * The x and y data from axes and image are changed when scale/
%       georeference is used.
%     * The drawn points are deleted from the graphics once the selection
%       is finished. 
%     * The priority of the inputs are: N, then SCALEOPT and finally
%       PLOTOPT. If the first (integer) is missing, the next is taken into
%       account (logical or 2x4 matrix) and so on.
%
%   Examples:
%     xy = ginput2;
%
%     [x,y] = ginput2(5);
%
%     [x,y,button] = ginput2(1);
%
%     imagesc(peaks(40))
%     [x,y,button,scalemat] = ginput2(4,true,'k*');
%     hold on, plot(x,y), hold off
%
%   See also GINPUT and PLOT.


%   Copyright 2008 Carlos Adrian Vargas Aguilera
%   $Revision: 1.0$  $Date: 2008/07/09 19:30:00$

%   Written by
%   M.S. Carlos Adrian Vargas Aguilera
%   Physical Oceanography PhD candidate
%   CICESE 
%   Mexico, 2008
%   nubeobscura@hotmail.com
%
%   Download from:
%   http://www.mathworks.com/matlabcentral/fileexchange/loadAuthor.do?objec
%   tType=author&objectId=1093874

%% Defaults:
N         = Inf;
SCALEOPT  = false;
PLOTOPT   = {'none'};

%% Constants KEYs (on my personal keyboard):
DOUBLECLICK    =   0;
LEFTCLICK      =   1;
MIDDLECLICK    =   2;
RIGHTCLICK     =   3;
BACKSPACE      =   8;
ESCAPE         =  27;
LEFTARROW      =  28;
RIGHTARROW     =  29;
UPARROW        =  30;
DOWNARROW      =  31;
SPACEBAR       =  32;
DELETE         = 127;
ASCII          = [33:64 65:90    91:96 97:122   123:126 161:255];
%                [UKEYS ULETTERS LKEYS LLETTERS LKEY    FOREING]

%% Functionality:
% Selection  buttons:
% NOTE: I left all this KAY because the user may use this special case for
% other special purposes outside this function.
SELECTS   = [LEFTCLICK ASCII ESCAPE LEFTARROW RIGHTARROW ...
             UPARROW DOWNARROW SPACEBAR DELETE];
% Deletion   buttons:
DELETES   = BACKSPACE;           
% Finishes   buttons:
FINISHES  = [];
% ZOOM(2)    buttons:
ZOOMIN    = RIGHTCLICK;          
% ZOOM RESET buttons:
ZOOMRESET = MIDDLECLICK;         
% ZOOM OUT   buttons:
ZOOMOUT   = DOUBLECLICK;         

%% Other parameters
secpause  = 0.3;   % Seconds to wait for double-click response.
YESERROR  = true;  % If there is an error with GINPUT, it tells to display 
                   % an ERROR or a WARNING message.

%% Checks input:
Nargin = nargin;
% First checks N:
if Nargin && isfloat(varargin{1}) && numel(varargin{1})==1 && ...
    round(varargin{1})==varargin{1} && varargin{1}>=0
 N           = varargin{1};
 varargin(1) = [];
 Nargin      = Nargin-1;
end
% Second checks SCALEOPT:
if Nargin && (((length(varargin{1})==1) && islogical(varargin{1})) ...
                || all(size(varargin{1})==[2 4]))
 SCALEOPT    = varargin{1};
 varargin(1) = [];
 Nargin      = Nargin-1;
end
% Finally checks PLOTOPT:
if Nargin && ischar(varargin{1})
 PLOTOPT     = varargin;
 clear varargin Nargin
end

%% Scales/Georeferences?:
SCALEMAT = [];
if (islogical(SCALEOPT) && SCALEOPT) || (numel(SCALEOPT)==8)
 method = 'linear';
 extrap = 'extrap';
 iaxis  = gca;
 himage = findobj(get(iaxis,'Children'),'Type','image');
 if ~isempty(himage)
  himage   = himage(1);
  xlim     = get(iaxis,'XLim');
  ylim     = get(iaxis,'YLim');
  zlim     = get(iaxis,'ZLim');
  z        = repmat(max(zlim),1,5);
  xdata    = get(himage,'XData');
  ydata    = get(himage,'YData');
  if islogical(SCALEOPT) % interactivelly
   axes(iaxis)
   Ax1 = round(min(xdata)); Ax2 = round(max(xdata));
   Ay1 = round(min(ydata)); Ay2 = round(max(ydata));
   % Default (equal):
   Rx1 = Ax1; Rx2 = Ax2;
   Ry1 = Ay1; Ry2 = Ay2;
   hgeo = [];
   dlgTitle = 'Georeference image';
   lineNo   = 1;
  
   while true
    % Selects first corner:
    questdlg('Select the first corner (1 of 2):',dlgTitle,'OK','OK');
    pause(secpause)
    
    [Ax1,Ay1]  = ginput2(1,false,'none');
    Ax1       = round(Ax1);
    Ay1       = round(Ay1);
    axis(iaxis,[xlim ylim])
    if iaxis==gca && ~isempty(Ax1) && ~isnan(Ax1)
     hgeo(1) = line([xlim NaN Ax1 Ax1],[Ay1 Ay1 NaN ylim],z,'color','m');
     prompt  = {'X-coordinate at 1st corner:',...
                'Y-coordinate at 1st corner:'};
     def     = {int2str(Ax1),int2str(Ay1)};
     answer  = inputdlg(prompt,dlgTitle,lineNo,def);
     answer  = str2num(char(answer{:}));
     break
    end
   end
   
   % Checks inputs:
   if ~isempty(answer) && isfloat(answer) && length(answer)==2 && ...
                                                      all(isfinite(answer))
    Rx1 = answer(1); Ry1 = answer(2);
    secondcorner = true;
   else
    secondcorner = false;
    warning('CVARGAS:ginput2:IncorrectGeoreference',...
            'Ignored incorrect georeference corners.')
   end
  
   while secondcorner
    % Selects second corner:
    questdlg('Select the second corner (2 of 2):',dlgTitle,'OK','OK');
    pause(secpause)
   
    [Ax2,Ay2]  = ginput2(1,false,'none');
    Ax2       = round(Ax2);
    Ay2       = round(Ay2);
    if ~ishandle(iaxis), break, end
    axis(iaxis,[xlim ylim])
    if iaxis==gca && ~isempty(Ax2) && ~isnan(Ax2) && Ax2~=Ax1 && Ay2~=Ay1
     hgeo(2) = line([xlim NaN Ax2 Ax2],[Ay2 Ay2 NaN ylim],z,'color','c');
     prompt  = {'X-coordinate at 2nd corner:',...
                'Y-coordinate at 2nd corner:'};
     def     = {int2str(Ax2),int2str(Ay2)};
     answer  = inputdlg(prompt,dlgTitle,lineNo,def);
     answer  = str2num(char(answer{:}));
     break
    end
   end
   
   % Checks inputs:
   if secondcorner && ~isempty(answer) && isfloat(answer) && ...
                         length(answer)==2 && all(isfinite(answer))
    Rx2 = answer(1); Ry2 = answer(2);
   else
    warning('CVARGAS:ginput2:IncorrectGeoreference',...
            'Ignored incorrect georeference corners.')
   end
  
   % Deletes corner's lines:
   if ~isempty(hgeo)
    delete(hgeo)
   end
   
   % Scale matrix:
    SCALEMAT = [Ax1 Ay1 Ax2 Ay2; Rx1 Ry1 Rx2 Ry2];
  else
   SCALEMAT = SCALEOPT;
  end
 else
  warning('CVARGAS:NoImageFound',...
   'No image found in the current axes to georeference.')
 end
 
 % OK, set the scaling then:
 if ~isempty(SCALEMAT)
  xdata = interp1(SCALEMAT(1,[1 3]),SCALEMAT(2,[1 3]),xdata,method,extrap);
  ydata = interp1(SCALEMAT(1,[2 4]),SCALEMAT(2,[2 4]),ydata,method,extrap);
  xlim2 = interp1(SCALEMAT(1,[1 3]),SCALEMAT(2,[1 3]),xlim ,method,extrap);
  ylim2 = interp1(SCALEMAT(1,[2 4]),SCALEMAT(2,[2 4]),ylim ,method,extrap);
  set( iaxis,'XLim' ,sort(xlim2,'ascend'));
  set( iaxis,'YLim' ,sort(ylim2,'ascend'));
  set(himage,'XData',xdata);
  set(himage,'YData',ydata);  
  % Reverses axis directions:
  if diff(xlim)*diff(xlim2)<1
   if strcmp(get(iaxis,'XDir'),'normal')
    set(iaxis,'XDir','reverse')
   else
    set(iaxis,'XDir','normal')
   end
  end
  if diff(ylim)*diff(ylim2)<1
   if strcmp(get(iaxis,'YDir'),'normal')
    set(iaxis,'YDir','reverse')
   else
    set(iaxis,'YDir','normal')
   end
  end
 end
 axis normal
 
end

%% Draws?:
if strcmpi(PLOTOPT{1},'none')
 yesdraw = false;
else
 yesdraw = true;
end
% Optional parameters:
if yesdraw
 hpoints  = [];
 % Check for linestyle color:
 yescolor     = true;
 Nplotopt     = length(PLOTOPT);
 yeslinestyle = rem(Nplotopt,2);
 if yeslinestyle % Given LineStyle
  for k=1:length(PLOTOPT{1})
   switch lower(PLOTOPT{1}(k))
    case 'y', yescolor = false; break
    case 'm', yescolor = false; break
    case 'c', yescolor = false; break
    case 'r', yescolor = false; break
    case 'g', yescolor = false; break
    case 'b', yescolor = false; break
    case 'w', yescolor = false; break
    case 'k', yescolor = false; break
    otherwise, % no color specified
   end
  end
 end
 if ~yescolor && (Nplotopt*yeslinestyle~=1)
  for k = yeslinestyle+1:2:Nplotopt    % Given 'Color'
   if strncmpi(PLOTOPT{k},'co',2), yescolor = false; break, end
  end
 end
 if yescolor
  contnan  = 1;
  colors   = get(gca,'colororder');
  ncolors  = size(colors,1);
  color    = colors(1,:);
 end
end

%% Gets the points:
X        = [];
Y        = [];
BUTTON   = [];
cont     = 0;
while cont<N
 
 try
  % Uses normal GINPUT:
  [x,y,button] = ginput(1);
 catch ME
  % GINPUT error:
  if YESERROR
     error('CVARGAS:ginput2:ExecutionError',ME.message)
  else
   warning('CVARGAS:ginput2:ExecutionError',ME.message)
   return
  end
 end

 % No ASCII or MOUSE key:
 if isempty(button) || ismember(button,FINISHES)
  % Deletes drawn points:
  if yesdraw && ~isempty(hpoints)
    delete(hpoints)
  end
  % Finishes selection:
  break
 end
 
 % Checks if the mouse was inside an axes of the current figure;
 lim = axis;
 outside = x<lim(1) || x>lim(2) || y<lim(3) || y>lim(4);
 
 % Checks for double click:
 pause(secpause) % Gives time for response
 if strcmp(get(gcf,'selectiontype'),'open')
  button = DOUBLECLICK;
 end
 
 % DELETION:
 if ismember(button,DELETES)
  if ~isempty(X)
   inan = isnan(X(end));
   if yesdraw
    if ~inan
     % Deletes last drawn point:
     delete(hpoints(end)), hpoints(end) = [];
    elseif yescolor
     % Change color as necesary:
     contnan = contnan-1;
     color   = colors(mod(contnan-1,ncolors)+1,:);
    end
   end
   % Deletes the last selected point:
   X(end)      = [];
   Y(end)      = [];
   BUTTON(end) = [];
   % Checks if the last point was NaN:
   if ~inan
    cont = cont-1;
   end
  end
  continue
 end
 
 % ZOOM OUT:
 if ismember(button,ZOOMOUT)
  zoom(1/2)
  continue
 end
 
 % ZOOM in with PAN:
 if ismember(button,ZOOMIN)
  zoom(2)
  % Do the PANNIG:
  if ~outside
   lim = axis;
   lim = [x+diff(lim(1:2))/2*[-1 1] y+diff(lim(3:4))/2*[-1 1]];
   axis(lim) 
  end
  continue
 end
 
 % ZOOM RESET:
 if ismember(button,ZOOMRESET)
  zoom reset
  continue
 end
 
 if ismember(button,SELECTS)
  
  % Sets NaNs if outside the axes:
  if outside 
   % Change color:
   if yesdraw && yescolor 
    contnan = contnan+1;
    color   = colors(mod(contnan-1,ncolors)+1,:);
   end
   % Adds NaNs but the point counters do not take it into account:
   X      = [X;      NaN];
   Y      = [Y;      NaN];
   BUTTON = [BUTTON; button];
  else
    % Draws the result:
   if yesdraw
    % Search for last point:
    x0 = []; y0 = []; z0 = [];
    inan = isnan([NaN; X; NaN]);
    if ~inan(end-1)
     inan        = find(inan);
     nlastpoints = inan(end)-inan(end-1)-1;
     npoints     = length(hpoints);
     range       = npoints-nlastpoints+1:npoints;
     hlastaxes   = get(hpoints(range),'Parent');
     if iscell(hlastaxes), hlastaxes  = cell2mat(hlastaxes); end
     [loc,loc] = ismember(gca,hlastaxes);
     if loc
      x0 = get(hpoints(range(loc)),'XData');
      y0 = get(hpoints(range(loc)),'YData');
      z0 = get(hpoints(range(loc)),'ZData');
     end
    end
    holdon = ishold;
    hold on
    h = plot([x0 x],[y0 y],PLOTOPT{:});
    % Elevates the value:
    z = get(gca,'ZLim'); z = z(2);
    set(h,'Zdata',[z0 z])
    % Sets the color:
    if yescolor
     set(h,'Color',color)
    end
    hpoints = [hpoints; h];
    if ~holdon, hold off, end 
    % Restores limits:
    axis(lim)
   end
   
   % Saves the result:
   X      = [X;      x];
   Y      = [Y;      y];
   BUTTON = [BUTTON; button];
   cont   = cont+1;
  end
   continue
 end

 % If it is still here, an unknown button was pressed
 warning('CVARGAS:ginput2:UnknownPressedButton','Unknown pressed button.')
 
end

%% Checks outputs:
if nargout<=1 
 X = [X Y]; 
end