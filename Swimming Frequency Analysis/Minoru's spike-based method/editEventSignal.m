function [eventIndex] = editEventSignal(eventIndex, signal, si,signalRange)
% simple gui interface to edit event index
% si ... sampling interval in [sec]

% make sure 'eventIndex' is row vector
if ~isrow(eventIndex)
    eventIndex = eventIndex';
end

monitorPosition = get(0, 'MonitorPosition');

hFigure = figure('OuterPosition',[monitorPosition(1,1),...
    monitorPosition(1,4)/2,monitorPosition(1,3),monitorPosition(1,4)/2]);
axes('Position', [0.05 0.15 0.9 0.8]);
% hScrollbar = uicontrol('style','slider',...
%     'units','normalized','position',[0.05 0.05 0.9 0.1,...
%     'callback',{},
hold on;
plotLDS((0:(length(signal)-1))*si,signal, 'k','n_points', 5e5);
ylim(signalRange);
zoom xon; pan xon;

editEvent = 'a';
hEvent = []; % handles for lines representing events

while editEvent ~='q'
    hEvent = markEvents(eventIndex,si,signalRange,hFigure,hEvent,[1 0 0]);
    pan xon;
    editEvent = input('Add or Delete events? Press q if you are done. [a/d/q]: ', 's');
    hEditEvent = [];
    if editEvent == 'a'
        display('Adding events ...');
        display('Mark event onsets. Press Enter if you are done.');
        [x, ~] = ginput2('g*');
        addIndex = round(x/si)+1;
        hEditEvent = markEvents(addIndex, si, signalRange,hFigure, hEditEvent, [0 1 0]);
        accept = input('Accept these events? [y/n]: ','s');
        if accept == 'y'
            eventIndex = sort([eventIndex addIndex']);
        end
    elseif editEvent == 'd'
        display('Deleting events ...');
        display('Mark events onsets. Press Enter if you are done.');
%         [x, ~] = ginput2('b*');
        [x,~] = ginput2(2,false,'b*');
        if numel(x) ==2
            deleteIndex = [];
%         figurePosition = get(gcf,'Position');
%         axisPosition = get(gca,'Position');
%         axisXLim = get(gca, 'XLim');
%         tolrerance = diff(axisXLim)/(figurePosition(3)*axisPosition(3))
%         for i=1:numel(x)
%             deleteIndex = [deleteIndex ...
%                 find((x(i)-tolrerance < (eventIndex-1)*si) & ((eventIndex-1)*si < x(i)+tolrerance))];
%         end
            deleteIndex = find(x(1) < (eventIndex-1)*si & (eventIndex-1)*si< x(2));
            hEditEvent = markEvents(eventIndex(deleteIndex),si,signalRange, ...
                hFigure, hEditEvent, [0 0 1]);
            accept = input('Accept deleting these events? [y/n]: ', 's');
            if accept == 'y'
                eventIndex(deleteIndex) = [];
            end
        end
    end
    delete(hEditEvent);
end
close(hFigure);

    
    function hEvent = markEvents(index,si,signalRange,hFigure,hEvent,color)
        figure(hFigure);
        delete(hEvent);
        hEvent = arrayfun(@(x)line([x x],signalRange/4,'Color',color),(index-1)*si);
