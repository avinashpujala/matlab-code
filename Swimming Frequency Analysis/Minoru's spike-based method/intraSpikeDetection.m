function [intra] = intraSpikeDetection(channelName,fullFileName)
% [intra] = intraSpikeDetection(channelName,fullFileName)
%

if nargin < 2
    [FileName, PathName] = uigetfile('.abf','Select abf file');
    fullFileName = [PathName FileName];
    [d, si, AbfHeader] = abfload(fullFileName);
    if nargin <1
        channelIndex = [];
        while isempty(channelIndex)
            channelIndex = listdlg('ListString',AbfHeader.recChNames)
            if ~isempty(channelIndex)
                channelName = AbfHeader.recChNames{channelIndex};
            end
        end
    end
    raw = d(:,channelIndex);
    clear d;
else
    [raw,si] = abfload(fullFileName, 'channels', {channelName});
    [PathName, FileName] = fileparts(fullFileName);
    
end

intra.raw = raw; 
intra.si = si*10^-6; % convert the unit from us to s
si = intra.si;

%% detect intracellular spikes 
command = 'findpeaks';
option{1,1} = 'MINPEAKHEIGHT';         option{2,1} = -20;
option{1,2} = 'MINPEAKDISTANCE';         option{2,2} = int16(10^-3/si);
index = [];
eval(['[~, index]=',command,'(raw,option{:});']);

intra.detectionCommand = command;
intra.DetectionOption = cell2struct(option,option(1,:),2);
intra.detectedIndex = index;


%%
signalRange = [-80 30];
intra.curatedIndex = editEventSignal(intra.detectedIndex, intra.raw, intra.si, signalRange);

save([FileName(1:end-4),'_intra_',datestr(clock,30)], ...
    'intra');
end