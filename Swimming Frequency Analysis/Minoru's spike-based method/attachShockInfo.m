function attachShockInfo(FileName)
if nargin < 1
    [FileName, PathName] = uigetfile('.mat','Select epi_vr file');
else
    PathName = [pwd filesep];
end

load([PathName FileName]);

minPeakDistance = floor(7*10^-3/vr.si);
minPeakHeight = 5;
detectedShockIndex = detectShockVR(vr.raw,minPeakHeight, minPeakDistance);
vr.shockIndex = editEventSignal(detectedShockIndex, vr.raw, vr.si,...
    minPeakHeight*[-1 1]);

firstUnderScore = min(find(FileName=='_'));

save([PathName, FileName(1:firstUnderScore-1),'_epi_vr_shock_',datestr(clock,30)],'epi','vr'); 
