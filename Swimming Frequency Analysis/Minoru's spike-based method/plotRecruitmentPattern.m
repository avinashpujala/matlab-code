function plotRecruitmentPattern(hAx,freqVector,nSpikesVector,c)
% default value
points = 12;
axes(hAx);hold on;
hs = scatter(freqVector,nSpikesVector,points,c);
% alpha(hs,5);

binSize = 10;
binNumber = floor(max(freqVector)/binSize);
x = [];y =[];sd=[];se=[];
ste = @(x) std(x)/(length(x)^1/2);
for i=1:0.1:binNumber
    index = find(freqVector>(i-1)*binSize & freqVector <(i)*binSize);
    if numel(index) >3
        x = [x (i-1/2)*binSize];
        temp = nSpikesVector(index);
        y = [y mean(temp)];
        sd = [sd std(temp)];
        se = [se ste(temp)];
    end
end

y(find(isnan(y))) = 0;
line(x,y,'Color',c);
arrayfun(@(x,y,se) line([x x],[y-se y+se],'Color',c),x, y, se);




