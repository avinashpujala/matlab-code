function shockIndex = detectShockVR(signal,minPeakHeight,minPeakDistance)
% minPeakDistance in a sampling unit
% minPeakHeight in a given unit
[~,shockIndex] = findpeaks(diff(abs(signal)),'MINPEAKHEIGHT',minPeakHeight);
if ~isempty(shockIndex)
    peakDistance = [shockIndex(1); diff(shockIndex)];
    shockIndex = shockIndex(find(peakDistance > minPeakDistance));
end

