function varargout = timeseriesAnalysis(varargin)
%TIMESERIESANALYSIS Analyze timeseries data
%   Documentation Here!
%
%

% AUTHOR 
% ======
% Minoru Koyama (minoru "_dot_" koyama "_AT_" google "_dot_" com)
%
% TODO & BUGS
% ===========
%
%
%
% VERSION
% =======
% 1.0 First release
%
%

hMainFigure = figure('Name', 'Timeseries analysis',...
    'Toolbar','none',...
    'Menubar','none',...
    'NumberTitle','off'...
    );

% user interface
hFileMenu = uimenu(hMainFigure,'Label','File');
hOpenAbf = uimenu(hFileMenu,'Label','Open ABF',...
    'Callback',@openAbf);

hSignalMenu = uimenu(hMainFigure,'Label','Signal');
hSelectSignals = uimenu(hSignalMenu,'Label','Select Signals',...
    'Callback',@selectSignals);

% hSignalSubmenu = [];

jFrame = get(handle(hMainFigure),'JavaFrame');
pause(0.1);
jFrame.setMaximized(true);

% hSignalPanel = uipanel(hMainFigure,...
%     'Title','Signals',...
%     'Position',[.01 0.8 .05 .2]...
%     );
% 
% hSignalCheckbox =[];
hAxes =[];

% data structure
Tsa = guidata(hMainFigure);
Tsa = struct(...
    'Signal',[],...
    'gui', []);
guidata(hMainFigure, Tsa);

% callback functions
    function openAbf(hObject,eventdata)
        [fileName, pathName] = uigetfile('.abf','Select abf file');
        [d, si, h] = abfload([pathName fileName]);
        display('done!');
        signal = [];
        for iCh = 1:numel(h.recChNames)
            signal{iCh}.raw = d(:,iCh);
            signal{iCh}.si = si*1e-6;
            signal{iCh}.name = h.recChNames{iCh};
            signal{iCh}.unit = h.recChUnits{iCh};
            signal{iCh}.visible = 1;
        end
        Tsa = guidata(hMainFigure);
        Tsa.Signal = signal;
        guidata(hMainFigure,Tsa);
        drawPlot;
%         updateSignalPanel;
%         initializeSignalSubmenu;
    end

    function selectSignals(hObject, eventdata)
        Tsa = guidata(hMainFigure);
        moniPos = get(0,'MonitorPosition');
        hF = figure('Name','Select signals','Toolbar','none',...
        'Outerposition', [moniPos(1,3:4)/2 0 0]+[-50 -150 100 300],...
        'Menubar','none','NumberTitle','off');
        nSignal = numel(Tsa.Signal);
        heightCheckbox = 0.8/nSignal;
        for i=1:nSignal
            hChkbx{i} = uicontrol(hF,'Style','checkbox',...
                'String',Tsa.Signal{i}.name,...
                'Units','normalized',...
                'Max',1,'Min',0,'Value',Tsa.Signal{i}.visible,...
                'Position',[.05 .15+(i-1)*heightCheckbox .9 heightCheckbox]);
        end
        hOkButton = uicontrol(hF,'Style','pushbutton',...
            'String','OK','Units','normalized',...
            'Callback',{},...
            'Position',[.05 .05 .4 .1]);
        hCancelButton = uicontrol(hF,'Style','pushbutton',...
            'String','Cancel','Units','normalized',...
            'Callback',[],...
            'Position',[.55 .05 .4 .1]);
%         uiwait(hF);
        
    end

%     function updateGui(hObject, eventdata)
%        updateSignalPanel
%            
%     end

%     function updateSignalPanel(hObject, eventdata)
%         Tsa = guidata(hMainFigure);
%         nSignal = numel(Tsa.Signal);
%         heightCheckbox = 0.9/nSignal;
%         for i=1:nSignal
%             hSignalCheckbox{i} = uicontrol(hSignalPanel,'Style','checkbox',...
%                 'String',Tsa.Signal{i}.name,...
%                 'Units','normalized',...
%                 'Callback',@updatePlot,...
%                 'Position',[.05 .05+(i-1)*heightCheckbox .9 heightCheckbox]);
%         end
%     end
       
%     function initializeSignalSubmenu(hObject, eventdata)
%         Tsa = guidata(hMainFigure);
%         Signal = Tsa.Signal;
%         nSignal = numel(Signal);
%         for i = 1:nSignal
%             if Signal{i}.visible
%                 toggle = 'on';
%             else
%                 toggle = 'off';
%             end
%             hSignalSubmenu(i) = uimenu(hSignalMenu,...
%                 'Label',Signal{i}.name,...
%                 'Checked',toggle,...
%                 'Callback',@updateSignalSubmenu);
%         end
%         drawPlot;
%     end

%     function updateSignalSubmenu(hObject, eventdata)
%         Tsa = guidata(hMainFigure);
%         clickedSignalSubmenu = find(hSignalSubmenu == hObject); 
%         if strcmp(get(hObject,'Checked'), 'on')
%             set(hObject,'Checked','off');
%             Tsa.Signal{clickedSignalSubmenu}.visible = 0;
%         else
%             set(hObject,'Checked','on');
%             Tsa.Signal{clickedSignalSubmenu}.visible = 1;
%         end
%         guidata(hMainFigure, Tsa);
%         drawPlot;
%     end
% 
%     function updatePlot(hObject, eventdata)
%         Tsa = guidata(hMainFigure);
%         nSignal = numel(Tsa.Signal);
%         for i=1:nSignal
%             Tsa.Signal{i}.visible = get(hSignalCheckbox{i},'Value');
%         end
%         guidata(hMainFigure, Tsa);
%         drawPlot;
%     end

    function drawPlot(hObject, eventdata)
        Tsa = guidata(hMainFigure);
        delete(hAxes);
        Signal = Tsa.Signal;
        nSignal = numel(Signal);
        signalSelectedArray = zeros(1,nSignal);
        for i = 1:nSignal;
            if Signal{i}.visible
                signalSelectedArray(i) = 1;
            end
        end
        indexVisibleSignal = find(signalSelectedArray == 1);
        nVisibleSignal = numel(indexVisibleSignal);
        heightPlot = 0.9/nVisibleSignal;
        hAxes = nan(1,nVisibleSignal);
        for i = 1:nVisibleSignal
            hAxes(i) = axes('Position', [.05 .99-i*heightPlot .9 heightPlot]);
            hold on;
            tmp = Signal{indexVisibleSignal(i)};
            plotLDS((0:(length(tmp.raw)-1))*tmp.si, tmp.raw,'k','n_points',5e5);
            axis tight;
            ylabel([tmp.name,' [',tmp.unit,']'],'Interpreter','none');
            if i <nVisibleSignal
                set(hAxes(i),'XtickLabel',{});
            else
                xlabel('Time [sec]');
            end
        end
        linkaxes(hAxes,'x');
        guidata(hMainFigure,Tsa);
    end
% utility functions



end