function analyzeRecruitmentPattern(filename)
% filename should be string cell array
% filename ={'11527015','11527016','11527017'};
% maxShockDelay ... optional, unit [s], default value 80*10^-3
% phaseDelay ... optional, [0 .. 1], default value 1/4
% preWindow ... optional, unit [s], default 10*10^-3

allSpontaFreqVector = []; allSpontaNSpikesVector = [];
allShockFreqVector = []; allShockNSpikesVector = [];
for iFile = 1:numel(filename)
    %% load all the relevant mat files for this dataset
    load(ls([filename{iFile},'_intra_*.mat']));
    load(ls([filename{iFile},'_editted_epi_*.mat']));
    load(ls([filename{iFile},'_epi_vr_shock_*.mat']));
    
  
    figure('Name',[filename{iFile}, ' raw']);
    %% plot intra recording
    hIntra = axes('Position', [0.05 0.55 0.9 0.4]);
    title(filename{iFile});
    plotLDS((0:length(intra.raw)-1)*intra.si, intra.raw,'k','n_points',5e5);
    intraRange=[-80 30];
    ylim(intraRange);
    zoom xon; pan xon;

    arrayfun(@(x)line([x x],intraRange/4,'Color',[1 0 0]),(intra.curatedIndex-1)*intra.si);

    %% plot vr recording
    hVr = axes('Position', [0.05 0.05 0.9 0.4]);
    hold on;
    plotLDS((0:length(vr.raw)-1)*vr.si, vr.raw, 'k','n_points',5e5);
    vrRange = [-.2 .2];
    ylim(vrRange);
    zoom xon; pan xon;

    arrayfun(@(x)line([x x],vrRange/4,'Color',[1 0 0]),(vr.curatedIndex-1)*vr.si);

    %% overlay episode information
    epi = editted_epi;
    for i=1:length(epi)
        burst = epi{i}.burst;
        for j=1:length(burst)
            if length(burst{j}.spikes)>1
                burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
            elseif length(burst{j}.spikes) == 1
                burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
            end
            patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)],...
                'g', 'FaceAlpha', 0.3,'EdgeColor','none');
        end

        onsets = epi{i}.onsets;    
        freq = epi{i}.freq;

        for k=1:length(freq)
            text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                'HorizontalAlignment','center','Parent',hVr);
        end
    
    end

    %% overlay shock onsets
    plot(hVr,(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
    
    linkaxes([hIntra hVr],'x');

    %% determine if the episode is induced by shock
    for iEpi = 1:numel(epi)
        onsets = epi{iEpi}.onsets;
        maxShockDelay = 80*10^-3; %  80 ms
        episodeWindow = [onsets(1)-maxShockDelay, onsets(end)];
        shockOnsets = (vr.shockIndex-1)*vr.si;
        if any(episodeWindow(1) < shockOnsets & shockOnsets <episodeWindow(2))
            shockOnset = shockOnsets((episodeWindow(1) < shockOnsets) & ...
                (shockOnsets <episodeWindow(2)));
            epi{iEpi}.shockInduced = true;
            epi{iEpi}.afterShock = (onsets-shockOnset)>0;
        else
            epi{iEpi}.shockInduced = false;
            epi{iEpi}.afterShock = onsets.*0;
        end
    end
    %% asign intra spikes to each cycle
    phaseDelay = 1/4; % [0 .. 1]
    preWindow = 10*10^-3; % 10 ms
    nTotalCycle=0;
    for iEpi = 1:numel(epi)
        onsets = epi{iEpi}.onsets;
        freq = epi{iEpi}.freq;
        afterShock = epi{iEpi}.afterShock;
        if numel(onsets) <2
        else
            windowArray = (1-phaseDelay)*[ 2*onsets(1)-onsets(2), onsets(1:end-1)] +...
                phaseDelay*onsets;
            windowArray(1) = windowArray(1)-preWindow;
            axes(hIntra);
            arrayfun(@(x)line([x x],intraRange,'Color',[0 0 0]),windowArray);
            for k=1:length(freq)
                textColor = [0 0 0];
                if afterShock(k)
                    textColor = [1 0 0];
                end
                text(mean(windowArray(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                    'HorizontalAlignment','center','Color',textColor,'Parent',hIntra);
            end

            for iCycle = 1:numel(epi{iEpi}.freq)
                nTotalCycle=nTotalCycle+1;
                window = windowArray(iCycle:iCycle+1);
                for iCell = 1:numel(intra)
                    spikes = (intra.curatedIndex-1)*intra.si;
                    spikesInWindow = spikes((window(1) < spikes) &...
                        (spikes < window(2)))-onsets(iCycle);
                    epi{iEpi}.cycle{iCycle}.cell{iCell}.spikes = spikesInWindow;
                end
            end
        end
    end

    %% transform information to matrix format
    %  recruitmentMatrix.data: 
    % | frequency | nSpikes | shock |
        
    for iIntra = 1:numel(intra)
%         recruitmentMatrix{iFile}.filename = filename{iFile};
%         recruitmentMatrix{iFile}.data = nan(3,nTotalCycle);
        freqVector = nan(1,nTotalCycle);
        nSpikesVector = nan(1,nTotalCycle);
        afterShockVector = nan(1,nTotalCycle);
        shockInducedVector = nan(1,nTotalCycle);
        iTotalCycle = 0;
        for iEpi = 1:numel(epi)
            if numel(epi{iEpi}.onsets) < 2

            else
                for iCycle = 1:numel(epi{iEpi}.cycle)
                    iTotalCycle = iTotalCycle +1;
%                     recruitmentMatrix{iFile}.data(:,iTotalCycle) = ...
%                         [epi{iEpi}.freq(iCycle), ...
%                         numel(epi{iEpi}.cycle{iCycle}.cell{iIntra}.spikes),...
%                         epi{iEpi}.afterShock(iCycle)];
                    freqVector(iTotalCycle) = epi{iEpi}.freq(iCycle);
                    nSpikesVector(iTotalCycle) = numel(epi{iEpi}.cycle{iCycle}.cell{iIntra}.spikes);
                    afterShockVector(iTotalCycle) = epi{iEpi}.afterShock(iCycle);
                    shockInducedVector(iTotalCycle) = epi{iEpi}.afterShock(1);
                end
            end
        end
    end
     
    spontaFreqVector{iFile} = freqVector(shockInducedVector == false & afterShockVector == false);
    spontaNSpikesVector{iFile} = nSpikesVector(shockInducedVector == false & afterShockVector == false);
    shockFreqVector{iFile} = freqVector(shockInducedVector == true);
    shockNSpikesVector{iFile} = nSpikesVector(shockInducedVector == true);
       
    figure('Name',[filename{iFile},' anly']);
    hold on;
    hScatter =gca;
    plotRecruitmentPattern(hScatter,spontaFreqVector{iFile},...
        spontaNSpikesVector{iFile},[0 0 1]);
    plotRecruitmentPattern(hScatter,shockFreqVector{iFile},...
        shockNSpikesVector{iFile},[1 0 0]);
  
    allSpontaFreqVector = [allSpontaFreqVector spontaFreqVector{iFile}];
    allSpontaNSpikesVector = [ allSpontaNSpikesVector spontaNSpikesVector{iFile}];
    allShockFreqVector = [allShockFreqVector shockFreqVector{iFile}];
    allShockNSpikesVector = [allShockNSpikesVector shockNSpikesVector{iFile}];
end

figure('Name','all anly');
hold on;
hScatter =gca;
plotRecruitmentPattern(hScatter,allSpontaFreqVector,...
    allSpontaNSpikesVector,[0 0 1]);
plotRecruitmentPattern(hScatter,allShockFreqVector,...
    allShockNSpikesVector,[1 0 0]);

