function [Episode] = editEpisodeSignal(Episode, signal, si,signalRange)
% simple gui interface to edit event index
% si ... sampling interval in [sec]

monitorPosition = get(0, 'MonitorPosition');

hFigure = figure('OuterPosition',[monitorPosition(1,1),...
    monitorPosition(1,4)/2,monitorPosition(1,3),monitorPosition(1,4)/2]);
hAx = axes('Parent',hFigure,'Position', [0.05 0.15 0.9 0.8]);

% hScrollbar = uicontrol('style','slider',...
%     'units','normalized','position',[0.05 0.05 0.9 0.1,...
%     'callback',{},

% hold on;
% plotLDS((0:(length(signal)-1))*si,signal, 'k','n_points', 5e5);
ylim(signalRange);
zoom xon; pan xon;

% utility functions 
mcolon = @(x) x(1):x(2);
forceRange = @(x,xrange) max(min(x,xrange(2)),xrange(1));
checkRange = @(x,xrange) (xrange(1)<x & x<xrange(2));

edittingSession = 'y';
currentEpisodeIndex = 1;
edittingEpisode = 'y';
hPatch = []; % handles for patches representing episode
hText = [];

timeMargin = 0.05
while edittingSession ~='q'
    epi = Episode{currentEpisodeIndex}
    timeRange = [max(epi.onsets(1)-timeMargin,0) epi.onsets(end)+timeMargin];
    indexRange = floor(timeRange./si)+1;
    axes(hAx);hold off;
    plot(mcolon(indexRange).*si, signal(mcolon(indexRange)),'k');
    title(sprintf('Episode No: %.0f',currentEpisodeIndex));
    ylim(signalRange);
    hold on;
    onsets =[];patchRange = [];
    spikes = [];
    if ~isfield(epi,'spikes')
        for i = 1:numel(epi.burst)
            spikes = [spikes epi.burst{i}.spikes];
        end
        epi.spikes = spikes; % store original spikes
    end
    arrayfun(@(x)line([x x],signalRange/10,'Color',[1 0 0]),epi.spikes);
    hold off;
    [hPatch,hText, patchRange] = drawEpisode(epi,hAx,hPatch, hText);
    axes(hAx);
    edittingSession = input('Edit this episode? [e/n/p/g/q]: ', 's');
    if edittingSession == 'e'
        display('Editting this episode...');
        edittingEpisode = 'y';
        while edittingEpisode ~='o'
            axes(hAx);
            edittingEpisode = input('Add or delete a burst patch. [a/d/o]: ', 's');
            if edittingEpisode == 'a'
                display('Adding a burst patch...');
%                 [x, ~] = ginput2(2,false,'g*');
                [x, ~] = ginput(2);
                x = sort(x);
                [~,startSpikeIndex] = min(abs(epi.spikes-x(1)));
                [~,endSpikeIndex] = min(abs(epi.spikes-x(2)));
                temp.spikes = epi.spikes(startSpikeIndex:endSpikeIndex);
                if numel(temp.spikes) > 0
                    display('no spikes selected?');
                    epi.burst = [epi.burst temp];
                end
                epi = updateEpisode(epi);
                
            elseif edittingEpisode == 'd'
                display('Deleting burst patch ...');
%                 [x,~] = ginput2(1,false,'r*');
                [x,~] = ginput(1);
                x = sort(x);
                deletePatchArray = [];
                for j = 1:numel(patchRange)
                    deletePatchArray = [deletePatchArray ...
                        checkRange(x,patchRange{j})];
                end
                deletePatchIndex = find(deletePatchArray);
                epi.burst(deletePatchIndex) = [];
                epi = updateEpisode(epi);

            elseif edittingEpisode == 'o'
                Episode{currentEpisodeIndex} = epi;
                
            end
            [hPatch,hText, patchRange] = drawEpisode(epi,hAx,hPatch, hText);
        end
    elseif edittingSession == 'p';
        Episode{currentEpisodeIndex} = epi;
        display('Moving to previous episode...');
        currentEpisodeIndex = max(currentEpisodeIndex-1,1);
    elseif edittingSession == 'n';
        Episode{currentEpisodeIndex} = epi;
        display('Moving to next episode...');
        currentEpisodeIndex = min(currentEpisodeIndex+1,numel(Episode));
    elseif edittingSession == 'g';
        Episode{currentEpisodeIndex} = epi;
        currentEpisodeIndex = forceRange(input('Episode No: '),[1,numel(Episode)]);
    end
    
end
end

function Episode = updateEpisode(Episode)
    onsets =[];
    for i = 1:numel(Episode.burst)
        onsets = [onsets min(Episode.burst{i}.spikes)];
    end
    onsets = sort(onsets);
    freq =1./diff(onsets);
    Episode.onsets = onsets;
    Episode.freq = freq;
end

function [hPatch,hText, patchRange] = drawEpisode(epi,hAx,hPatch, hText)
    axes(hAx);hold on;
    if ishandle(hPatch)
        delete(hPatch)
    end
    if ishandle(hText)
        delete(hText)
    end
    patchRange = [];hPatch =[];hText=[];
    YLIM = ylim;
    hold on;
    for i = 1:numel(epi.burst)
        if length(epi.burst{i}.spikes)>1
            patchRange{i}=[epi.burst{i}.spikes(1), epi.burst{i}.spikes(end)];
        elseif length(epi.burst{i}.spikes) == 1;
            patchRange{i}=epi.burst{i}.spikes(1)+[-0.5 0.5]*10^-3;
        end
        hPatch = [hPatch patch([patchRange{i},patchRange{i}(2:-1:1)],[YLIM(1), YLIM(1), YLIM(2), YLIM(2)],...
            'g', 'FaceAlpha', 0.3,'EdgeColor','none','Parent',hAx)];

    end
    for k=1:length(epi.freq)
        hText = [hText text(mean(epi.onsets(k:k+1)), YLIM(2)/3, [num2str(epi.freq(k), '%0.1f')],...
            'HorizontalAlignment','center')];
    end    
    hold off;
end
