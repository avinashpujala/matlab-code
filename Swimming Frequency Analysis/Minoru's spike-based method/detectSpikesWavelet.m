function detectedIndex = detectSpikesWavelet(signal, samplingInterval,Option)
% detectedIndex = detectSpikesWavelet(signal, samplingInterval,Option)
% samplingInterval [sec]
% wrapper function of detect_spikes_wavelet

% truncate the large deflection assuming it's related to electrical
% artifact
signal(find(signal>Option.yTruncate)) = Option.yTruncate;
signal(find(signal<-Option.yTruncate)) = -Option.yTruncate;

detectedIndex = detect_spikes_wavelet(signal, 10^-3/samplingInterval,...
    Option.Wid, Option.Ns,Option.option, Option.L, Option.wname, 0, 0);
