function [epi, vr] = vrEpisodeDetection(channelName, fullFileName, DetectionOption)
% [epi, vr] = vrEpisodeDetection(channelName, fullFileName, DetectionOption)
isReanalysis = input('Reanalyze the vr recording? [y/n]: ','s');
if isReanalysis ~= 'y'
    if nargin < 3
        DetectionOption = struct (...
            'Wid', [0.5 1],...
            'Ns', 6, ...
            'option','l',...
            'L', 0.05,...
            'wname','bior1.5',....
            'yTruncate',.2 ... % truncate signal larger than this
            );
    end
    if nargin < 2
        [FileName, PathName] = uigetfile('.abf','Select abf file');
        fullFileName = [PathName FileName];
        [d, si, AbfHeader] = abfload(fullFileName);
        if nargin <1
            channelIndex = [];
            while isempty(channelIndex)
                channelIndex = listdlg('ListString',AbfHeader.recChNames)
                if ~isempty(channelIndex)
                    channelName = AbfHeader.recChNames{channelIndex};
                end
            end
        end
        raw = d(:,channelIndex);
        clear d;
    else
        [raw,si] = abfload(fullFileName, 'channels', {channelName});
        [PathName, FileName] = fileparts(fullFileName);
        
    end
    
    vr.raw = raw; vr.si = si*10^-6;
    si = vr.si;
    
    %% detect extracellular spikes in vr
    command = 'detectSpikesWavelet';
    index = [];
    display('detecting spikes ... ');
    eval(['index =',command,'(raw,si,DetectionOption);']);
    display('done!');
    
    vr.detectionCommand = command;
    vr.DetectionOption = DetectionOption;
    vr.detectedIndex = index;
    
    %% detect shock onsets in vr
    minPeakDistance = floor(7*10^-3/vr.si);
    minPeakHeight = 5;
    detectedShockIndex = detectShockVR(vr.raw,minPeakHeight, minPeakDistance);
    vr.shockIndex = editEventSignal(detectedShockIndex, vr.raw, vr.si, minPeakHeight*[-1 1]);
    clear detectedShockIndex;
    %% exclude spikes after the shock
    tmp = []; shock = vr.shockIndex;
    for i = 1: numel(shock)
        tmp = [tmp find((0 < index-shock(i)) .* (index-shock(i) < minPeakDistance))];
    end
    vr.artifactFreeIndex = index;
    vr.artifactFreeIndex(tmp) =[];
    vr.artifactIndex = index(tmp);
    clear tmp shock;
    %% check detected spikes
    signalRange = [-.2 .2];
    YLIM = signalRange;
    vr.curatedIndex = editEventSignal(vr.artifactFreeIndex, vr.raw, vr.si, signalRange);
else
    [FileName, PathName] = uigetfile('.mat','Select epi_vr mat file');
    fullFileName = [PathName FileName];
    load(fullFileName, 'vr');
    si = vr.si;
    signalRange = [-.2 .2];
    YLIM = signalRange;
    vr.curatedIndex = editEventSignal(vr.curatedIndex, vr.raw, vr.si, signalRange);
end

%% defining episodes
TE = vr.curatedIndex;
minBurstDelay = 12*10^-3; % [sec]
minEpisodeDelay = 1/15; % [sec]
epi=[];
if length(TE)>1
    noepi=1;
    noburst=1;% in the episode
    epi{noepi}.burst{noburst}.spikes=TE(1)*si;
    for i=2:length(TE)
        dt=(TE(i)-TE(i-1))*si;
        if dt<minBurstDelay
            tmp=epi{noepi}.burst{noburst}.spikes;
            epi{noepi}.burst{noburst}.spikes=[tmp TE(i)*si];
        elseif minBurstDelay<=dt & dt<minEpisodeDelay
            noburst=noburst+1;
            epi{noepi}.burst{noburst}.spikes=TE(i)*si;
        elseif minEpisodeDelay < dt
            noepi=noepi+1;
            noburst=1;
            epi{noepi}.burst{noburst}.spikes=TE(i)*si;
        end
    end
end

%% calculate the onsets, frequency
for i=1:length(epi)
    onsets=[];
    for j=1:length(epi{i}.burst)
        onsets=[onsets min(epi{i}.burst{j}.spikes)];
    end
    epi{i}.freq=1./diff(onsets);
    epi{i}.onsets=onsets;
end

save([FileName(1:end-4),'_epi_vr_shock_',datestr(clock,30)], ...
    'epi','vr');

[editted_epi] = editEpisodeSignal(epi,vr.raw,vr.si,signalRange);
save([FileName(1:end-4),'_editted_epi_',datestr(clock,30)], ...
    'editted_epi');


end