% function analyseRecruitmentPattern
% if there's load *_epi_vr*.mat 
% load *_intra_*.mat
clear all; close all;
filename ={'11427032','11427033','11427034'};
allFreqVector = [];
allNSpikesVector = [];
for iFile = 1:numel(filename)

    matFiles = ls([filename{iFile},'*.mat']);
    for iMatFiles = 1:size(matFiles,1)
        load(matFiles(iMatFiles,:));
    end
    figure;
%     title(filename{iFile})
    %% intra
    hIntra = axes('Position', [0.05 0.55 0.9 0.4]);
    title(filename{iFile});
    % plotLDS((0:(length(signal)-1))*si,signal, 'k','n_points', 5e5)
    plotLDS((0:length(intra.raw)-1)*intra.si, intra.raw,'k','n_points',5e5);
    intraRange=[-80 30];
    ylim(intraRange);
    zoom xon; pan xon;

    arrayfun(@(x)line([x x],intraRange/4,'Color',[1 0 0]),(intra.curatedIndex-1)*intra.si)

    %% vr
    hVr = axes('Position', [0.05 0.05 0.9 0.4]);
    plotLDS((0:length(vr.raw)-1)*vr.si, vr.raw, 'k','n_points',5e5);
    vrRange = [-2 2];
    ylim(vrRange);
    zoom xon; pan xon;

    arrayfun(@(x)line([x x],vrRange/4,'Color',[1 0 0]),(vr.curatedIndex-1)*vr.si)

    %% epi
    epi = editted_epi
    for i=1:length(epi)
        burst = epi{i}.burst;
        for j=1:length(burst)
            if length(burst{j}.spikes)>1
                burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
            elseif length(burst{j}.spikes) == 1
                burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
            end
            patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)],...
                'g', 'FaceAlpha', 0.3,'EdgeColor','none');
        end

        onsets = epi{i}.onsets;    
        freq = epi{i}.freq;

        for k=1:length(freq)
            text(mean(onsets(k:k+1))*10^6, vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                'HorizontalAlignment','center','Parent',hVr);
        end

    %     episodePatch=[onsets(1),onsets(end)]*10^6;
    %     patch([episodePatch, episodePatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)],...
    %         'r', 'FaceAlpha', 0.3,'EdgeColor','none');     
    end

    linkaxes([hIntra hVr],'x');

    %% asign intra spikes to each cycle
    phaseDelay = 1/2; % [0 .. 1]
    convMatrix = eye(2) +phaseDelay.*[1 -1;-1 1];
    nTotalCycle=0;
    for iEpi = 1:numel(epi)
        onsets = epi{iEpi}.onsets;
        if numel(onsets) <2
        else
            windowArray = (1-phaseDelay)*[ 2*onsets(1)-onsets(2), onsets(1:end-1)] +...
                phaseDelay*onsets;
            for iCycle = 1:numel(epi{iEpi}.freq)
                nTotalCycle=nTotalCycle+1;
                window = windowArray(iCycle:iCycle+1);
                for iCell = 1:numel(intra)
                    spikes = intra.curatedIndex*intra.si;
                    spikesInWindow = spikes(find(window(1) < spikes &...
                        spikes < window(2)))-onsets(iCycle);
                    epi{iEpi}.cycle{iCycle}.cell{iCell}.spikes = spikesInWindow;
                end
            end
        end
    end

    %% 
    for iIntra = 1:numel(intra)
        freqVector{iFile} = nan(1,nTotalCycle);
        nSpikesVector{iFile} = nan(1,nTotalCycle);
        iTotalCycle = 0;
        for iEpi = 1:numel(epi)
            if numel(epi{iEpi}.onsets) < 2

            else
                for iCycle = 1:numel(epi{iEpi}.cycle)
                    iTotalCycle = iTotalCycle +1;
                    freqVector{iFile}(iTotalCycle) = epi{iEpi}.freq(iCycle);
                    nSpikesVector{iFile}(iTotalCycle) = numel(epi{iEpi}.cycle{iCycle}.cell{iIntra}.spikes);
                end
            end
        end
    end


    figure;scatter(freqVector{iFile},nSpikesVector{iFile});
    title(filename{iFile});
    binSize =10;
    binNumber = floor(max(freqVector{iFile})/binSize);
    x = [];y =[];sd=[];se=[];
    ste = @(x) std(x)/(length(x)^1/2)
    for i=1:binNumber
        x = [x (i-1/2)*binSize];
        temp = nSpikesVector{iFile}(find(freqVector{iFile}>(i-1)*binSize & freqVector{iFile} <i*binSize));
        y = [y mean(temp)];
        sd = [sd std(temp)];
        se = [se ste(temp)];
    end

    hold on;
    plot(x,y);
    arrayfun(@(x,y,se) line([x x],[y-se y+se]),x, y, se);
    
    allFreqVector = [allFreqVector freqVector{iFile}];
    allNSpikesVector = [ allNSpikesVector nSpikesVector{iFile}];
end

figure;scatter(allFreqVector,allNSpikesVector);
title('total');
binSize =10;
binNumber = floor(max(allFreqVector)/binSize);
x = [];y =[];sd=[];se=[];
ste = @(x) std(x)/(length(x)^1/2)
for i=1:binNumber
    x = [x (i-1/2)*binSize];
    temp = allNSpikesVector(find(allFreqVector>(i-1)*binSize & allFreqVector <i*binSize));
    y = [y mean(temp)];
    sd = [sd std(temp)];
    se = [se ste(temp)];
end

hold on;
plot(x,y);
arrayfun(@(x,y,se) line([x x],[y-se y+se]),x, y, se);
