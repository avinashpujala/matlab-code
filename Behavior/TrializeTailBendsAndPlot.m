%% Inputs
tic
nFramesInTrl = procData.nFramesInTrl;
fps = procData.fps;
stimTime = 100; % In ms

tailCurv = procData.tailCurv;

time = (0:size(tailCurv,3)-1)*(1/fps);
nTrls = size(tailCurv,3)/nFramesInTrl;



disp('Calculating tail angles...')
% tailCurv = tailCurv_uncorrected;
% tA = GetTailTangents(tailCurv);
if exist('dsVecs')==1
    tA = GetTailTangents(tailCurv,[],dsVecs);
else
    tA = GetTailTangents_20180304(tailCurv);
end

tA_trl = reshape(tA,size(tA,1),nFramesInTrl,nTrls);
time_trl = reshape(time,nFramesInTrl,nTrls);

tA_5 = GetTailTangents(tailCurv,5);
curv = tA_5(end,:)';
% curv = (tA_5(end,:)-tA_5(1,:))';
% curv = max(tA_5)';
curv_trl = reshape(curv,nFramesInTrl,nTrls);

toc


%% Plotting
nTrls = round(nTrls);
trls = 1:nTrls;
xLim = [-inf inf]; % In frames
cLim = [-225 225];
yShift = 350;

blah = tA_trl(:,:,trls(1));
nRows = size(tA_trl,1);
yTick = cumsum(repmat(nRows,numel(trls),1))+round(nRows/2)-nRows;
yTL = cell(numel(yTick),1);
yTL{1} = num2str(trls(1));
for jj = 2:numel(trls)
    temp = squeeze(tA_trl(:,:,trls(jj)));
    blah = cat(1,blah,temp);
    yTL{jj} = num2str(trls(jj));
end

figure
cMap = jet(64*4);
imagesc(time_trl(:,1)*1000,1:size(blah,1),blah),colormap(cMap)
box off
colorbar
xTick = [50 250:250:nFramesInTrl]*2;
xtl = xTick;
set(gca,'ytick',yTick,'yticklabel',yTL,'xtick',xTick,'xTickLabel',xtl,'tickdir','out','clim',cLim)
xlim(xLim)
xlabel('Time (ms)')
ylabel('Trl #')
title('Tail undulations for different trials')


figure('Name','Trlzed curv ts')
yTick = zeros(numel(trls),1);
count = 0;
for trl = trls
    count = count + 1;
    yTick(count) = yShift*(count-1);
    if mod(count,2)==0
        plot((time_trl(:,1)*1000),curv_trl(:,trl)-yTick(count),'r')
        hold on
    else
        plot((time_trl(:,1)*1000),curv_trl(:,trl)-yTick(count),'b')
        hold on
    end
end
xlim((xLim/fps)*1000)
yLim =[-yTick(end)-yShift yShift];
ylim(yLim)
plot([stimTime, stimTime],yLim,'k:')
xlabel('Time (ms)')
set(gca,'tickdir','out','ytick',flipud(-yTick),'yticklabel',fliplr(trls),'xtick',[100 500:500:time_trl(end)*1000])
box off
title(['Timeseries of total tail bend angle for different trials (yShift = ' num2str(yShift) '^o)'])

