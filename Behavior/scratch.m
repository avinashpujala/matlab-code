
%% Inputs
file_summary    = 'T:\Masashi\RelaxedImaging\RelaxedImagingSummaryV2.xlsx';
poolSize        = 1;
outDir_suffix   = 'AnalyCont';

%% Add paths
tic
disp('Adding relevant paths...')
addpath(genpath('V:\Code\General'));
addpath(genpath('V:\Code\GeciAnalysis'));
addpath(genpath('V:\Code\EphysReader'));
addpath('V:\Code\ExperimentSummary');
addpath('V:\Code\EphysAnalysis');
addpath('V:\Code\MotorAdaptation'); % swimCharacterize, swimCharacterize2 also exist
addpath(genpath('V:\Code\ImagingAnalysis'));
addpath('V:\Code\ImagingAnalysis\bfmatlab'); % Required for readSI2016Tiff.m
javaaddpath('V:\Code\ImagingAnalysis\bfmatlab\bioformats_package.jar');
javaaddpath('V:\Code\ImagingAnalysis\bfmatlab\bioformats_package(2).jar');
javaaddpath('C:\Program Files\ImageJ\ij.jar'); % This needs to be run to read SI2015 tif
toc

%% Read summary excel sheet as a table
if isempty(gcp('nocreate'))
    ppObj = parpool(poolSize);
end

disp('Reading table...')
tbl = readtable(file_summary);

%--- Fish indices
inds_fish = unique(tbl.idxData(find(~isnan(tbl.idxData))));
procData_cell = cell(length(inds_fish),1);
inds_skipped = zeros(length(inds_fish),1);

%% Go through each fish and make sure procData exists
for fishNum = 1:length(inds_fish)
    fprintf('Fish: %d/%d\n',[fishNum, length(inds_fish)])
    idx_fish = inds_fish(fishNum);    
    idx_row = find(tbl.idxData == idx_fish);
    dir_ca = tbl(idx_row(1),:).Path{1};
    ind_x = strfind(dir_ca,'X:');
    dir_ca(ind_x:ind_x+1) = 'T:';
    %     cd(dir_ca)
    dir_out = fullfile(dir_ca,outDir_suffix);
    fn = GetFilenames(dir_out,'ext','mat','searchStr','procData');
    if isempty(fn)
        fprintf('procData.mat not found for fish with index = %d, %s\n', idx_fish)
        try
            relaxedContinuousEpisodic_V9short_AP          
        catch
            fprintf('Skipped fish %d because of error\n',idx_fish)
            inds_skipped(fishNum) = 1;
        end   
    end
    try
        procData_now = OpenMatFile(dir_out,'nameMatchStr','procData');
    catch
        disp(['fishNum = ' num2str(fishNum)]);
    end
    if sum(strcmpi(fieldnames(procData_now),'roi_ts'))
        procData_cell{fishNum} = OpenMatFile(dir_out);
    else
        fprintf('\nprocData.mat found, but no "roi_ts" field, so skipping fish # %d', idx_fish)
        inds_skipped(fishNum) = 1;
    end
end

%% Try processing skipped fish
skippedFish = find(inds_skipped);
processSkippedFish = input('Process skipped fish? (y/n) : ', 's');
if strcmpi(processSkippedFish,'y')
    for fishNum = skippedFish(:)'
        fprintf('Fish: %d/%d\n',[fishNum, length(inds_fish)])
        idx_fish = inds_fish(fishNum);
        idx_row = find(tbl.idxData == idx_fish);
        dir_ca = tbl(idx_row(1),:).Path{1};
        ind_x = strfind(dir_ca,'X:');
        dir_ca(ind_x:ind_x+1) = 'T:';
        %     cd(dir_ca)
        dir_out = fullfile(dir_ca,outDir_suffix);
        fn = GetFilenames(dir_out,'ext','mat','searchStr','procData');
        
        relaxedContinuousEpisodic_V9short_AP        
    end
end



%% Save all procData
procData_cell_keep = procData_cell(find(1-inds_skipped));
fileName_proc = 'procData_cell_relaxed.mat';
[dir_summary,~,~] = fileparts(file_summary);
dir_summary = fullfile(dir_summary,'Avi_temp');
fprintf('Saving procData_cell at %s\n',dir_summary)
tic
save(fullfile(dir_summary,fileName_proc),'procData_cell_keep')
toc




