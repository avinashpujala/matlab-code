%% Inputs

imgDir = 'S:\Avinash\Ablations and behavior\Alx\20171220-Alx-Kaede-42hrs conv-60hrs abl\20171223_AlxKaede_conv42hpf_abl60hpf_behav5dpf\f1_ctrl\slowDir_12-30-17-015401\vib';
imgExt = 'bmp';
imgInds = [];
headArea = 1.5; % Approx head area in sq-mm
spatialFilt = 30;
lineLen = 15;
frameRate = 30; % Hz
trlLen = 60; % In seconds
thr_dist = 100; % Max-speed fish can travel in mm/sec (It is better to set this value a bit lower than expected)

dt = 1/frameRate;
nImagesInTrl = frameRate * trlLen;
disp('Reading image file names in dir...')
nFilesInDir = length(GetFilenames(imgDir,'ext','bmp'));
nTrls = nFilesInDir/nImagesInTrl;
disp([num2str(nTrls) ' trls detected!'])

%% Getting fish position for all time points
fp = nan(nImagesInTrl,2,nTrls);
for trl = 1:nTrls
    disp(['Trl # ' num2str(trl)])   
    trlStartInd = (trl-1)*nImagesInTrl + 1; 
    trlEndInd = trlStartInd + nImagesInTrl - 1;
    imgInds = trlStartInd:trlEndInd;
    I = ReadImgSequence(imgDir,imgExt,imgInds);
    if imgInds(1)==1
        I(:,:,1) = I(:,:,2); % The first frame is usually messed up for some reason.
    end
     if trl ==1
        [I_backSub, img_bg] = SubtractBackground(I,'method','mean');
        ipd = GetPxlSpacing(img_bg,'diam',50);
        thr_dist = ceil((thr_dist*dt)/ipd);
        spatialFilt = ceil(headArea/ipd);
        disp(['Inter pxl distance = ' num2str(ipd)])
        close        
     else
%          I_backSub = SubtractBackground(I,'backImg',img_bg);
          I_backSub = SubtractBackground(I,'method','mean'); % Use when there is drift across trials, although slower 
     end
     nHeadPxls = ceil(headArea/ipd);
    [foo,blah] = GetFishPos(I_backSub, nHeadPxls,'filter',spatialFilt,...
        'lineLen',lineLen,'process','parallel','thr_dist',thr_dist,...
        'tryCorrect',true);  
    fp(:,:,trl)  = foo;
end

%% Save
disp('Saving fish position to mat file...')
saveDir = fullfile(imgDir,'proc');
if exist(saveDir)~=7
    mkdir(saveDir)
end
ts = datestr(now,30);
slowSwim = matfile(fullfile(saveDir,['slowSwim_' ts '.mat']), 'Writable',true);
slowSwim.fishPos = fp;
slowSwim.frameRate = frameRate;
slowSwim.trlLen = trlLen;
slowSwim.nTrls = nTrls;
slowSwim.img_bg = img_bg;
slowSwim.interPxlDist = ipd;

%% Distance analysis
dDist  = nan(nImagesInTrl,nTrls);
totalDist = nan(nTrls,1);
for trl = 1:nTrls
    dDist(1,trl) = 0;
    dDist(2:end,trl) = deltaDistance(squeeze(fp(:,:,trl)));   
    totalDist(trl) = sum(dDist(:,trl));
end
dDist = (dDist*ipd)/dt;
dDist_all = dDist(:);
pkInds = GetPks(dDist_all,'polarity',1,'peakThr',0,'thrType','abs');
dDist_pkInds = pkInds;
dDist_pks = dDist_all(pkInds);
disp('Gaussian mixture modeling to separate noise, real peaks, and large errors...')
% rng(now)
[label,model,llh] = mixGaussEm(dDist_pks(:)',3);
[mu_sort,sortInds] = sort(model.mu,'ascend');
sig_sort = squeeze(model.Sigma);
sig_sort = sig_sort(sortInds);

figure('Name','Peak sorting')
clrs = {'m','g','c'};
for lbl = 1:length(mu_sort)
    inds = find(label ==lbl);
    semilogy(inds,dDist_pks(inds),'.','color', clrs{lbl})
    hold on
end
box off
set(gca,'color','k')
ylim([-inf inf])
xlabel('Indices')
ylabel('Distance peaks')

lbl = unique(label);
labelOfInterest = lbl(sortInds(2));
thr_lower = min(dDist_pks(label ==labelOfInterest));
thr_upper = max(dDist_pks(label ==  labelOfInterest));

nTrlsToDisp = min([nTrls,5]);
figure('Name','3xample trials')
trlInds = randperm(nTrls);
trlInds = trlInds(1:nTrlsToDisp);
for jj = 1:nTrlsToDisp
    subplot(nTrlsToDisp,1,jj)
    plot(dDist(:,trlInds(jj)),'g')
    hold on
    plot(ones(size(dDist(:,1)))*thr_lower,'m--')
    plot(ones(size(dDist(:,1)))*thr_upper,'m--')
    set(gca,'color','k')
    box off
    ylim([-5 50])
    xlim([-inf inf])
    ylabel(['Trl # ' num2str(trlInds(jj))])
end

break

%% Some quantification
inds = find(label == labelOfInterest);
dDist_pkInds_keep = dDist_pkInds(inds);
dDist_pks_keep = dDist_pks(inds);
isi = diff(dDist_pkInds_keep)*dt*1000;
inds = find(isi<200)+1;
isi(inds)=[];
dDist_pkInds_keep(inds) =[];
dDist_pks_keep(inds) = [];
nSwimBouts = numel(dDist_pks_keep);

figure('Name','Peak velocity histogram')
[p,vals] = hist(dDist_pks_keep,30);
p = p/sum(p);
mu = mean(dDist_pks_keep);
bar(vals,p,1)
hold on
plot([mu,mu],[0, max(p)],'r--')
text(mu,max(p),num2str(round(mu*10)/10))
xlim([0,inf])
box off
xlabel('Peak velocity (mm/sec)')
ylabel('Probability')

figure('Name','Inter-swim interval')

[p,vals] = hist(isi,30);
p = p/sum(p);
mu = mean(isi);
bar(vals,p,1)
hold on
plot([mu,mu],[0, max(p)],'r--')
text(mu,max(p),num2str(round(mu*10)/10))
xlim([0,inf])
box off
xlabel('Inter-swim interval (ms)')
ylabel('Probability')


%% Swim onsets and offsets
crossInds = LevelCrossings(dDist_all,mu_sort(1));
dur = nan(nSwimBouts,1);
boutDist = dur;
for jj = 1:nSwimBouts
    onInds = crossInds{1};
    foo = dDist_pkInds_keep(jj)-onInds;
    negInds = foo<0;
    foo(negInds)=[];
    onInds(negInds)=[];
    [~,startInd] = min(foo);
    startInd = onInds(startInd);
    
    offInds = crossInds{2};
    foo = offInds-dDist_pkInds_keep(jj);
    negInds = foo<0;
    foo(negInds)=[];
    offInds(negInds)=[];
    [~,endInd] = min(foo);
    endInd = offInds(endInd);    
    inds = startInd:endInd;    
    if ~isempty(inds)
        dur(jj) = endInd-startInd;
        boutDist(jj) = sum(dDist_all(inds));
    end   
end
dur = dur*dt*1000;
boutDist = boutDist*dt;
nanInds = isnan(dur);
dur(nanInds)=[];
boutDist(nanInds)=[];

figure('Name','Swim duration histogram')
[p,vals] = hist(dur,12);
p = p/sum(p);
mu = mean(dur);
bar(vals,p,1)
hold on
plot([mu,mu],[0, max(p)],'r--')
text(mu,max(p),num2str(round(mu*10)/10))
xlim([0,inf])
box off
xlabel('Swim duration (ms)')
ylabel('Probability')


figure('Name','Swim bout dist histogram')
[p,vals] = hist(boutDist,30);
p = p/sum(p);
mu = mean(boutDist);
bar(vals,p,1)
hold on
plot([mu,mu],[0, max(p)],'r--')
text(mu,max(p),num2str(round(mu*10)/10))
xlim([0,inf])
box off
xlabel('Swim bout distance (mm)')
ylabel('Probability')

%% Save stuff
disp('Saving stats data')
dataMat = [dDist_pkInds_keep(:), dDist_pks_keep(:), dur(:), boutDist(:)];
varNames = {'dDistance peak ind','dDistance pk val (mm/sec)', 'Bour duration (ms)',' Bout distance (mm)'};
stats = {varNames; dataMat};
slowSwim.stats = stats;
disp('Done!')
