

%% Inputs
clear all
%imgDir = 'S:\Avinash\Ablations and behavior\Alx\2018\Mar\20180321\toBeProc\f4_ctrl\fastDir_03-21-18-235255\vib';
imgDir = 'V:\Matthew Wootten\JaneliaDatasets\f1s3';
readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [1:1000];
% imgInds = (8000):(8000+1500);
fps = 500;
nFramesInTrl = 750;
% nFramesInTrl = length(imgInds);
spatialFilt = 30;

lineLen = 15;
nBlocks = 1;
cropWid = 90; %( For imgDims ~ [900,900])
poolSize  = 10;
backMethod = 'mean';
tryCorrect = true;
headDiam = 1;
len_midlines = 50;
headDiam = 1; % In mm
trlNums =[];

%% Checking and correcting inputs

imgExt(strfind(imgExt,'.'))=[];
% if nargin ==0
%     [~, imgDir] = uigetfile(['*.' imgExt]);
% else
%     imgDir = varargin{1};
%     if isempty(imgDir)
%         [~,imgDir] = uigetfile(['*.' imgExt]);
%     end
% end

% switch readMode
%     case 'fromMishVid'       
%         [I, outDir] = ReadMishVid();
%         imgInds = 1:size(I,3);
%     case 'fromImages'           
%         I = ReadImgSequence(imgDir,imgExt,imgInds);
% end

imgNamesInDir = GetFilenames(imgDir,'ext',imgExt);
totalNumImgsInDir = length(imgNamesInDir);

outDir = fullfile(imgDir,'proc');
if ~exist(outDir,'dir')
    mkdir(outDir)
end


%% Background removal and fish position tracking in blocks of images
tic
poolObj = gcp('nocreate');
if isempty(poolObj)
    poolObj = parpool(poolSize);
end

nTrls = min(totalNumImgsInDir,numel(imgInds))/nFramesInTrl;
disp([num2str(nTrls) ' trls detected'])

disp('Getting image info..')
imgInfo = imfinfo(fullfile(imgDir,imgNamesInDir{1}));
imgDims = [imgInfo.Height imgInfo.Width];
imgDims = [imgDims, totalNumImgsInDir];
ts = datestr(now,30);

%---Compute background
% rp = randperm(totalNumImgsInDir);
% refFrames = rp(1:nFramesInTrl);
% disp(['Reading random sequence of ' num2str(numel(refFrames)) ' images, and computing background...'])
% I = ReadImgSequence(imgDir,[],[],imgNamesInDir(refFrames));
% [~,ref_all] = SubtractBackground(I,'method',backMethod);

if isempty(trlNums)
    if nBlocks == -1
        nBlocks = max([nTrls, 1]);
    end
    
    disp('Block-by-block processing...')
    if isempty(imgInds)
        imgInds = 1:totalNumImgsInDir;   
    end
    
    blockList = SubListsFromList(imgInds,round(totalNumImgsInDir/nBlocks));
    procName = ['procData_' ts '.mat'];
    
    disp('Creating a new file at ')
    disp(fullfile(outDir,procName))
    
%     procData = matfile(fullfile(outDir,procName),'Writable',true);
%     procData.nFramesInTrl = nFramesInTrl;
%     procData.fps = fps;
%     procData.I_proc_crop = zeros(2*cropWid + 1, 2*cropWid + 1, totalNumImgsInDir);
%     procData.tailCurv = zeros(len_midlines,2,totalNumImgsInDir);
%     procData.curv = zeros(len_midlines,totalNumImgsInDir);
%     procData.fishPos = zeros(totalNumImgsInDir,2);
%     ref = zeros(imgDims(1),imgDims(2),nBlocks);  
    mlInds = cell(imgDims(3),1);
    dsVecs = cell(imgDims(3),1);
    for block = 1:length(blockList)
%         imgFrames = blockInds(block):blockInds(block+1)-1;
    imgFrames = blockList{block};
        disp(['Block # ' num2str(block),...
            ', images: ' num2str(imgFrames(1)) ' - ' num2str(imgFrames(end))])
        disp('Reading images...')
        I = ReadImgSequence(imgDir,[],[],imgNamesInDir(imgFrames));
        disp('Subtracting background...')
%         [im_proc,ref(:,:,block)] = SubtractBackground(I,'method',backMethod,'backImg',ref_all);
        
         if size(I,3)> 100
             backInds = unique(round(linspace(1,size(I,3),100)));
         end
        [im_proc,ref(:,:,block)] = SubtractBackground(I,'method',backMethod,...
            'refFrames',backInds);
        
        % If, first block, then get pixel length
        if block ==1
            pxlLen = GetPxlSpacing(mean(ref(:,:,block),3),'diam',50);
%             procData.pxlLen = pxlLen;
            kerSize = ceil(headDiam/pxlLen);
        end
        
        % I need to find fish position in trial sized chunks because of the
        %   sequential correction procedure
        subLists = SubListsFromList(imgFrames,nFramesInTrl);
        indsList = SubListsFromList(1:numel(imgFrames),nFramesInTrl);
        fp = nan(numel(imgFrames),2);
        for kk = 1:length(indsList)
            disp(['Trl # ' num2str((block-1)*kk + 1)])
            if size(im_proc,3)>2
            fp_temp = GetFishPos_180307(im_proc(:,:,indsList{kk}),...
                'kerSize',kerSize,'process','serial',...
                'tryCorrect', tryCorrect,'method','mean');
            else
                 fp_temp = GetFishPos_180307(im_proc(:,:,indsList{kk}),'kerSize',kerSize,'process','parallel',...
                'tryCorrect', 0);
            end
            fp(indsList{kk},:) = fp_temp;
            disp('Cropping images...')
            foo = CropImgsAroundPxl(im_proc(:,:,indsList{kk}),fp(indsList{kk},:),cropWid,'procType','parallel'); % Parallel, not working, need to fix bugs
            disp('Tracking midlines...')
            a = 1;
            I_smooth = FindFishPxlsForMidlineTracking_20180325(foo,'headDiam',headDiam,'pxlLen',pxlLen);
            [midlines,mlInds_trl,dsVecs_trl] =  GetMidlinesByThinning_new(I_smooth,'process','parallel','plotBool',0);
            tA = GetTailTangents(midlines);
            
            
            I_smooth2 = FindFishPxlsForMidlineTracking_20180325(foo,'headDiam',headDiam,'pxlLen',pxlLen);
            [midlines2,mlInds_trl2,dsVecs_trl2] =  GetMidlinesByThinning_new(I_smooth2,'process','parallel','plotBool',0);
            tA2 = GetTailTangents(midlines2);
            
            if fps > 120
                for tt = 1:size(tA,1)
                    tA(tt,:) = chebfilt(tA(tt,:),1/fps,60,'low');
                end
            end
            disp('Updating procData.mat ...')
%             procData.fishPos(subLists{kk},:) = fp_temp;
%             procData.I_proc_crop(:,:,subLists{kk}) = foo;
%             procData.tailCurv(:,:,subLists{kk}) = midlines;
%             procData.curv(:,subLists{kk}) = tA;
            mlInds(subLists{kk}) = mlInds_trl; % These cannot be directly updated in procData!!
            dsVecs(subLists{kk}) = dsVecs_trl;
        end
    end
    ref = mean(ref,3);
    disp('Appending addition variables to procData...')
%     procData.ref = ref;
%     procData.imgDims_crop = size(foo(:,:,1));
%     procData.imgDims = size(I(:,:,1));
%     procData.mlInds = mlInds;
%     procData.dsVecs = dsVecs;
    clear im_proc fp hOr_temp    
else
    %%%% Check to see if there are any procData.mat files in outDir
    procFiles = GetFilenames(outDir, 'searchStr','procData','ext','mat');
    if ~isempty(procFiles)
        disp(['Found procData file! Will read and write to ' procFiles{end}])     
        %%%% Since each procFile name has a time-stamp in it, the last one will
        %%%% be the most recent one, so read and append to this one.
%         try
%             procData = OpenMatFile(fullfile(outDir,procFiles{end}));
%             procData.Properties.Writable = true;
%         catch
%             error('Cannot read procData.mat file! Check for errors.')
%         end
    else        
        procName = ['procData_' ts '.mat'];
        %%%% Presently I will not allow for the option to compute from
        %%%% scratch, if no procData.mat file is found.
        error('procData.mat file not found, try running from the start!')     
    end
    trlNumToImgInds = @(trlNum,nFramesInTrl) (trlNum-1)*nFramesInTrl+1:(trlNum-1)*nFramesInTrl + nFramesInTrl;
%     pxlLen = procData.pxlLen;
%     nFramesInTrl = procData.nFramesInTrl;
    mlInds = procData.mlInds;
    dsVecs = procData.dsVecs;
    for trl = trlNums(:)'
        disp(['Trl num # ' num2str(trl)])
        imgInds = trlNumToImgInds(trl,nFramesInTrl);
        disp('Reading processed images...')
        im_proc = procData.I_proc_crop(:,:,imgInds);
        I_smooth = FindFishPxlsForMidlineTracking(im_proc,'headDiam',headDiam,'pxlLen',pxlLen);
        [midlines,mlInds_trl,dsVecs_trl] =  GetMidlinesByThinning_new(I_smooth,'process','parallel','plotBool',0);
        tA = GetTailTangents(midlines);
        disp('Updating procData.mat ...')       
%         procData.tailCurv(:,:,imgInds) = midlines;
%         procData.curv(:,imgInds) = tA(1:size(procData.curv,1),:); % The size has to be specified because a change can result in mismatch if only reprocessing a few trials instead of computing from start
        mlInds(imgInds) = mlInds_trl; % These cannot be directly updated in procData!!
        dsVecs(imgInds) = dsVecs_trl;
    end
%     procData.mlInds = mlInds;
%     procData.dsVecs = dsVecs;
end
toc


%% Plot trialized tail bends
close all
TrializeTailBendsAndPlot
disp('Saving trialized tail curvature figures figures...')
suffix = ['_Trialized tail curvatures_' ts];
h = get(0,'children');
for fig = 1:length(h)
    prefix = ['Fig_' sprintf('%0.2d', fig)];
    saveas(h(fig), fullfile(outDir,[prefix, suffix]))
end

% %% Saving processed images
% saveOrNot = 'y';
% tic
% if strcmpi('y',saveOrNot)
%     disp('Cropping images...')
%     tic
%     IM_crop = CropImgsAroundPxl(IM,fishPos,cropWid);
%     clear IM
%     toc
%     disp('Saving cropped stacks...')
%     tic
%     procData.IM_crop = IM_crop;
%     toc
% else
%     disp('Data not saved!')
% end
% toc
