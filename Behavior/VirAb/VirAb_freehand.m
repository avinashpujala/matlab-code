%VirAb_multithr_beta - Script for digitally removing noise from ablation
%stacks. This is the best working version.

%% Read img stack

[file,path] = uigetfile('*.tif');
[~,fileStem] = fileparts(file);
imgStack = ReadImgStack(fullfile(path,file));

%% Inputs
nChannels = 3;
chOfInterest = 3;
imgStack_st = imgStack(:,:,1:nChannels:end);

imgStack_b = imgStack(:,:,chOfInterest:nChannels:end);
% imgStack_thr = Standardize(imgStack_b);
imgStack_thr = imNormalize(imgStack_b,9999);

var1 = imgStack_thr; % Set this to imgStack_thr_ab for second round of ablation

figure('Name','Pre-processed ')
imagesc(max(imgStack_thr,[],3)),axis image
axis off
title('Max-int of pre-processed image stack')

%% Getting rois
nImages = size(imgStack_thr,3);
imgInds = 1:nImages;
% imgInds = 41;
imgInds = imgInds(:)';
roi = cell(nImages,1);

for jj = imgInds
    img = var1(:,:,jj);
    %     roi{jj} = setEllipticalRois_ap(img,jet(256));
    roi{jj} = SetFreehandRois(img,'clrMap',gray(256),'figTitle',num2str(jj));
end

saveOrNot = input('Would you like to save ROIs? (y/n): ', 's');
if strcmpi(saveOrNot,'y')
    fName = input('Enter file name (w/o extension): ', 's');
    disp('Saving ROIs...')
    fName = [fName '_' fileStem '.mat'];
    save(fullfile(path,fName), 'roi');    
end

%% Virtual excising - Method 1 (Custom)
disp('Getting threshold...')
thr = multithresh(mean(var1,3),2);
thr = thr(1);

backPxls = var1(var1 < thr);
forePxls = var1(var1 >= thr);

ker = ones(20,20)/100;
disp('Ablating...')
blah = [];
imgStack_thr_ab = var1;

sliceInds = 1:size(var1,3);

for jj = imgInds(:)'
    disp(['Slice # ' num2str(jj)])
    if ~isempty(roi{jj})
        img = var1(:,:,jj);
        img_roi = img*0;
        [img_dist, img_dist_wt] = DistTransformFromRoiCtr(img,roi{jj});
        img_dist = Standardize(img_dist);
        img_dist_wt = Standardize(img_dist_wt);
        mu = zeros(length(roi{jj}),1);
        inds_inRoi = cell(length(roi{jj}),1);
        backImg = reshape(backPxls(randperm(prod(size(img)))),size(img));
        backImg = backImg - min(backImg(:));
        for rr = 1:length(roi{jj});
            img_roi_current = img*0;
            if length(roi{jj})>1
                roi_current = roi{jj}{rr};
            else
                roi_current = roi{jj}{1};
            end
            if ~isempty(roi_current)
                disp(['Roi # ' num2str(rr)])
                inds = roi_current.idx;
                img_roi(inds)=1;
                img_roi_current(inds)=1; 
                img_roi_current = Standardize(conv2(img_roi_current,ker,'same')) + rand((size(img)))*0.2;
                inds_inRoi{rr} = find(img_roi_current >= 0.2);
                vals_roi = img(inds_inRoi{rr});
                mu(rr) = min(mean(vals_roi),median(vals_roi));
                inds_inRoi{rr} = inds_inRoi{rr}(img(inds_inRoi{rr}) >= 1*mu(rr));
            end
        end
        img_dist = Standardize(img_dist.^0.3)+0*backImg;        
        img_dist_wt = Standardize(img_dist_wt.^0.3)+ 0*backImg;
        inds_inRoi = cell2mat(inds_inRoi);
        inds_inRoi = inds_inRoi(:);
        inds_outRoi = setdiff(find(ones(size(img))),inds_inRoi);
        img_dist(inds_outRoi) = 1;
        img_dist_wt(inds_outRoi)= 1;
        backImg(inds_inRoi) = backImg(inds_inRoi) + (min(mu)-mean(backImg(:)));
        backImg(inds_outRoi) =0;
        blah = imgStack_thr_ab(:,:,jj).*(0.1*img_dist_wt+0.9*img_dist);   
        imgStack_thr_ab(:,:,jj) = blah + 0.2*backImg;        
    end
end

% sliceInds = unique([imgInds(1)-1; imgInds(:); imgInds(end)+1]);
% sliceInds(sliceInds < 1 | sliceInds > nImages)=[];
zKer = ones(1,1,3).*reshape(gausswin(3),[1,1,3]);
zKer = zKer/(sum(zKer(:)));
disp('3D convolution...')
% imgStack_thr_ab(:,:,sliceInds) = convn(imgStack_thr_ab(:,:,sliceInds),zKer,'same');
disp('Done!')


%% Virtual excising - Method 2 (Using roifill)
disp('Getting threshold...')
% thr = multithresh(mean(var1,3),2);
% thr = thr(1);
% 
% backPxls = var1(var1 < thr);
% forePxls = var1(var1 >= thr);
% 
% ker = ones(20,20)/100;
disp('Ablating...')
blah = [];
imgStack_thr_ab = var1;

sliceInds = 1:size(var1,3);

for jj = imgInds(:)'
    disp(['Slice # ' num2str(jj)])
    if ~isempty(roi{jj})
        img = var1(:,:,jj);
        bw = zeros(size(img));
        for rr = 1:length(roi{jj});
            bw = (bw | roi{jj}{rr}.bw);
        end
        img = roifill(img,bw);
        imgStack_thr_ab(:,:,jj) = img;
    end
end

% sliceInds = unique([imgInds(1)-1; imgInds(:); imgInds(end)+1]);
% sliceInds(sliceInds < 1 | sliceInds > nImages)=[];
zKer = ones(1,1,3).*reshape(gausswin(3),[1,1,3]);
zKer = zKer/(sum(zKer(:)));
disp('3D convolution...')
imgStack_thr_ab(:,:,sliceInds) = convn(imgStack_thr_ab(:,:,sliceInds),zKer,'same');
disp('Done!')


%% Saving stack
fName = [fileStem '_denoised'];
imgStack_new = imgStack;
disp('Saving image stack...')

fullOrNot = input('Save all channels (y/n) ?','s');
if strcmpi(fullOrNot,'n')
    SaveImageStack(imgStack_thr_ab,path,fName)
elseif strcmpi(fullOrNot,'y')
    otherChannels = setdiff(1:nChannels,chOfInterest);
    for ch = otherChannels(:)'
        foo = Standardize(imgStack_new(:,:,ch:nChannels:end));
        imgStack_new(:,:,ch:nChannels:end) = foo;
    end      
    imgStack_new(:,:,chOfInterest:nChannels:end) = imgStack_thr_ab;
    SaveImages(imgStack_new,path,fName,'imgFormat','stack','ext','tif')    
end
fprintf('\n Saved as \n')
disp(fullfile(path,fName))



fName_max = [fileStem '_filt_maxInt'];
imgStack_max = max(imgStack_thr_ab(:,:,46:end),[],3);
imgStack_max = BandpassImgStack(imgStack_max,0,250);
% imgStack_max = imrotate(imgStack_max,5);
SaveImageStack(imgStack_max,path,fName_max)

