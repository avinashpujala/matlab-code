function PlayFishTrials(procData,trlList, varargin)
% PlayFishTrials - Plays the videos of the fish for specified trials with superimposed
%   midline and bend angle timeseries(all bend angles along the body or total bend angle)
%
% PlayFishTrials(procData,trlList,'frameInds','frameInds','pauseDur',pauseDur,
%   'bendAngleDispMode',bendAngleDispMode,'writeVideo',writeVideo,'saveDir',saveDir);
% PlayFishTrials([],trlList,....);
%
% Inputs:
% procData - Processed data with all the relevant info created by
%   ProcessFishImages. If [], then selects for interactive selection of
%   procData;
% trlList - List of trials to play for a given fish. e.g., [1 3  10];
% frameInds - Indices of frames to play, which are a subset of the
%   specified trials. For instance, frameInds = 1:100, plays only the first
%   100 frames of each of the trials.
% pauseDur - The duration of pause between displaying successive frames.
% bendAngleDispMode - 'all','total','none';
%   'all' results in the displaying, in a subplot under the images, of all the angles
%       along the length of the fish as matrix.
%   'total' - displays the total bend angle from head to tail as a single
%       trace
%   'none' - does not display bend angle
% nDispPts - The maximum number of surrounding time points of the bend angle to display at
%   any given time point; determines the temporal dilation of the bend angle timeseries plotted below the
%   image frames. If thsi exceeds the total number of available frames,
%   defaults to the number of available frames.
% 'writeVideo' - 0 or 1; the latter causes the fish video to be saved in
%   the current directory, or another directory if specified by saveDir
% 'saveDir' - Directory in which to save written movies, if [], then writes
%   in current directory
% clrMap - Colormap to use while displaying fish. Default is viridis.
% overlayMidline - True [default] or false; Determines whether or not to overlay
%   midline atop the fish
% dispTraj - Boolean [default: false].  Determines whether or not to plot
%   the fish trajectory in the image as a set of points. Not yet
%   implemented, do not use.
% nTrajPts - Number of trajectory points to display
% dispTangents - Boolean (default: false). If true, plots the tangent lines
%   atop the midline
% tangentLen - Scalar (default = 20). Length of tangents overlaid on fish.
%
% Avinash Pujala, Koyama lab/ JRC (HHMI), 2017

frameInds  = [];
pauseDur = 0;
bendAngleDispMode = 'all';
nDispPts = 150;
nTrajPts = 50;
writeVideo = 0;
saveDir = [];
lineClr = 'r';
clrMap = viridis(128);
overlayMidline = true;
dispTraj = false;
dispTangents = false;
tangentLen = 20;

if nargin < 2
    error('Minimum 2 inputs required!')
end

if isempty(procData)
    procData = OpenMatFile();
end


for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'frameinds'
                frameInds = varargin{jj+1};
            case 'pausedur'
                pauseDur = varargin{jj+1};
            case lower('bendAngleDispMode')
                bendAngleDispMode = varargin{jj+1};
            case 'ndisppts'
                nDispPts = varargin{jj+1};
            case 'writevideo'
                writeVideo = varargin{jj+1};
            case 'savedir'
                saveDir = varargin{jj+1};
            case 'clrmap'
                clrMap = varargin{jj+1};
            case 'overlaymidline'
                overlayMidline = varargin{jj+1};
            case 'disptraj'
                dispTraj = varargin{jj+1};
            case 'ntrajpts'
                nTrajPts = varargin{jj+1};
            case 'disptangents'
                dispTangents = varargin{jj+1};
            case 'tangentlen'
                tangentLen = varargin{jj+1};
        end
    end
end

nFramesInTrl  = procData.nFramesInTrl;
if isempty(frameInds)
    frameInds = 1:nFramesInTrl;
end

nDispPts = min(max(1,nDispPts),numel(frameInds));

disp('Reading relevant info from procData...')
N = size(procData.tailCurv,3);
trlList_all = SubListsFromList(1:N, nFramesInTrl);
trlList_sub = trlList_all(trlList);
imgFrames = trlList_all{[trlList]};
fp = procData.fishPos(imgFrames,:);
tC = procData.tailCurv(:,:,imgFrames);
% I = procData.I_proc_crop;


if sum(strcmpi(fieldnames(procData),'w'))
    w = procData.W;
    keptTrls = w.trlList;
else
    nTrls =size(I,3)/nFramesInTrl;
    keptTrls = 1:nTrls;
end

if isempty(trlList)
    trlList = keptTrls;
else
    trlList = keptTrls(keptTrls == trlList);
end



tC = tC(4:end,:,:);
tA = GetTailTangents(tC);
dTA = diff(tA,[],2);

curv = tA(end,:);
F= @(trlNum)(trlNum-1)*nFramesInTrl + 1: (trlNum-1)*nFramesInTrl + nFramesInTrl;
cLim = [0 0.9];
for trl = trlList(:)'
    disp(['Trl # ' num2str(trl)])
    if writeVideo
        if isempty(saveDir)
            saveDir = cd;
        end
        ts = datestr(now,30);
        fName = ['FishTrackingVideo_trl #' num2str(trl) '_' ts '.avi'];
        vidObj = VideoWriter(fullfile(saveDir,fName));
        open(vidObj);
        disp('Writing to video...')
    end
    inds_trl = F(trl);
    I_this = Standardize(I(:,:,inds_trl(frameInds)));
    tC_this = tC(:,:,inds_trl(frameInds));
    tA_this = tA(:,inds_trl(frameInds));
    curv_this = curv(inds_trl(frameInds));
    fp_this  = fp(inds_trl(frameInds),:);
    nonNanInds = find(1-isnan(tC_this(1,1,:)));
    origin = tC_this(1,:,nonNanInds);
    
    fh = figure('Name','Fish Tracking');
    if (strcmpi(bendAngleDispMode,'total') || strcmpi(bendAngleDispMode,'all'))
        ax = cell(2,1);
        ax{1} = [0.6 0.6 0.2 0.35];
        ax{2}=[1 0.3 0 0];
        axH = CreateSubaxes(fh, ax{1}, ax{2});
    end
    cMap = clrMap;
    
    count = 0;
    traj = nan(length(frameInds),2);
    for imgNum = 1:length(frameInds)
        count = count + 1;
        x = tC_this(:,1,count);
        y = tC_this(:,2,count);
        if strcmpi(bendAngleDispMode,'all')
            figure(fh)
            axes(axH(1))
            imagesc(I_this(:,:,count),'parent',axH(1)),axis image, axis off
            colormap(clrMap)
            hold on
            set(gca,'clim',cLim)
            if overlayMidline
                plot(x,y,'color',lineClr)
            end
            if dispTangents
                T_curv = TangentsOnACurve([x(:),y(:)],tangentLen);
                for tt = 2:3:size(T_curv,1)-1
                    patchline([x(tt),T_curv(tt,1)],[y(tt),T_curv(tt,2)],'edgecolor',[1 1 1],...
                        'linewidth',2, 'edgealpha',0.5);
                end
                patchline([x(1),T_curv(1,1)],[y(1),T_curv(1,2)],'edgecolor','c',...
                    'linewidth',3, 'edgealpha',0.5);
                patchline([x(end),T_curv(end,1)],[y(end),T_curv(end,2)],'edgecolor','c',...
                    'linewidth',3, 'edgealpha',0.5);
            end
            if dispTraj
                if count == 1
                    [traj(count,1),traj(count,2)] = deal(origin(1),origin(2));
                    plot(traj(:,1),traj(:,2),'m.')
                else
                    foo = [traj(count-1,1),traj(count-1,2)] - ...
                        ([fp_this(count-1,1),fp_this(count-1,2)]-[fp_this(count,1), fp_this(count,2)]);
                    [traj(count,1),traj(count,2)] = deal(foo(1),foo(2));
                    temp_traj = traj;
                    temp_traj(:,1) = traj(:,1) - traj(count,1) + origin(1);
                    temp_traj(:,2) = traj(:,2) - traj(count,2) + origin(2);
                    outInds = (temp_traj(:,1)<1) | (temp_traj(:,2)<1);
                    overInds = (temp_traj(:,1)>size(I_this,1)) | (temp_traj(:,2)>size(I_this,2));
                    temp_traj(outInds|overInds,:) = nan;
                    plot(temp_traj(:,1),temp_traj(:,2),'m.')
                end
            end
            drawnow
            title(['Abs Frame: ' num2str(frameInds(count)) ', Frame # ' num2str(count)...
                ', Angle: ' num2str(round(tC_this(end,count)*10)/10) '^o '])
            hold off
            if count >= nDispPts
                blah = tA_this(:,count-(nDispPts-1):count);
            else
                blah = zeros(size(tA_this,1),nDispPts);
                blah(:,end-count+1:end) = tA_this(:,1:count);
            end
            figure(fh)
            axes(axH(2))
            imagesc(count-nDispPts+1:count,1:size(blah,1),blah,'parent',axH(2));
            box off
            xt = get(gca,'xtick');
            colormap(axH(2),cMap)
            set(gca,'clim',[-150 150],'ytick',[5 size(blah,1)-5],'yticklabel',{'Head','Tail'},'xticklabel',xt);
            colorbar
            drawnow
            if isempty(pauseDur)
                pause()
            else
                pause(pauseDur)
            end
        elseif strcmpi(bendAngleDispMode,'total')
            figure(fh)
            axes(axH(1))
            imagesc(I_this(:,:,count),'parent',axH(1)),axis image, axis off
            colormap(clrMap)
            hold on
            set(gca,'clim',cLim)
            if overlayMidline
                plot(x,y,'color',lineClr)
            end
            if dispTangents
                T_curv = TangentsOnACurve([x(:),y(:)],tangentLen);
                for tt = 2:3:size(T_curv,1)-1
                    patchline([x(tt),T_curv(tt,1)],[y(tt),T_curv(tt,2)],'edgecolor',[1 1 1],...
                        'linewidth',2, 'edgealpha',0.5);
                end
                patchline([x(1),T_curv(1,1)],[y(1),T_curv(1,2)],'edgecolor','c',...
                    'linewidth',3, 'edgealpha',0.5);
                patchline([x(end),T_curv(end,1)],[y(end),T_curv(end,2)],'edgecolor','c',...
                    'linewidth',3, 'edgealpha',0.5);
            end
            if dispTraj
                if count == 1
                    [traj(count,1),traj(count,2)] = deal(origin(1),origin(2));
                    plot(traj(:,1),traj(:,2),'m.')
                else
                    foo = [traj(count-1,1),traj(count-1,2)] - ...
                        ([fp_this(count-1,1),fp_this(count-1,2)]-[fp_this(count,1), fp_this(count,2)]);
                    [traj(count,1),traj(count,2)] = deal(foo(1),foo(2));
                    temp_traj = traj;
                    temp_traj(:,1) = traj(:,1) - traj(count,1) + origin(1);
                    temp_traj(:,2) = traj(:,2) - traj(count,2) + origin(2);
                    outInds = (temp_traj(:,1)<1) | (temp_traj(:,2)<1);
                    overInds = (temp_traj(:,1)>size(I_this,1)) | (temp_traj(:,2)>size(I_this,2));
                    temp_traj(outInds|overInds,:) = nan;
                    plot(temp_traj(:,1),temp_traj(:,2),'m.')
                end
            end
            drawnow
            title(['Abs frame # ' num2str(frameInds(count)), ' Rel frame # ' num2str(count)])
            hold off
            
            axes(axH(2))
            box off
            inds = (count-nDispPts:count);
            inds(inds<=0)=[];
            plot(inds,curv_this(inds),'color',clrMap(end-2,:))
            box off
            xlim([count-nDispPts count]);
            yl = [min(min(curv_this),-nDispPts) max(max(curv_this),nDispPts)];
            ylim(yl)
            xt = get(gca,'xtick');
            set(gca,'color',clrMap(1,:),'tick','out','ytick',...
                [round((yl(1)*0.75)/10)*10 0 round((yl(2)*0.75)/10)*10],'xticklabel',xt);
            ylabel('Body bend angle (deg)')
            drawnow
            if isempty(pauseDur)
                pause()
            else
                pause(pauseDur)
            end
        else
            figure(fh)
            imagesc(I_this(:,:,count)),axis image, axis off
            title(['Abs frame # ' num2str(frameInds(count)), ' Rel frame # ' num2str(count)])
            colormap(clrMap)
            hold on
            set(gca,'clim',cLim)
            if overlayMidline
                plot(x,y,'color',lineClr,'linewidth',1)
                plot(x,y,'.','color', lineClr)
            end
            
            if dispTangents
                T_curv = TangentsOnACurve([x(:),y(:)],tangentLen);
                for tt = 2:3:size(T_curv,1)-1
                    %                     patchline([x(tt),T_curv(tt,1)],[y(tt),T_curv(tt,2)],'edgecolor',[1 1 1],...
                    %                         'linewidth',2, 'edgealpha',0.5);
                    plot([x(tt),T_curv(tt,1)],[y(tt),T_curv(tt,2)],'w','linewidth',2);
                end
                %                 patchline([x(1),T_curv(1,1)],[y(1),T_curv(1,2)],'edgecolor','c',...
                %                     'linewidth',3, 'edgealpha',0.5);
                %                 patchline([x(end),T_curv(end,1)],[y(end),T_curv(end,2)],'edgecolor','c',...
                %                     'linewidth',3, 'edgealpha',0.5);
                plot([x(1),T_curv(1,1)],[y(1),T_curv(1,2)],'c','linewidth',3);
                plot([x(end),T_curv(end,1)],[y(end),T_curv(end,2)],'c','linewidth',3);
            end
            
            if dispTraj
                if count == 1
                    [traj(count,1),traj(count,2)] = deal(origin(1),origin(2));
                    plot(traj(:,1),traj(:,2),'m.')
                else
                    foo = [traj(count-1,1),traj(count-1,2)] - ...
                        ([fp_this(count-1,1),fp_this(count-1,2)]-[fp_this(count,1), fp_this(count,2)]);
                    [traj(count,1),traj(count,2)] = deal(foo(1),foo(2));
                    temp_traj = traj;
                    temp_traj(:,1) = traj(:,1) - traj(count,1) + origin(1);
                    temp_traj(:,2) = traj(:,2) - traj(count,2) + origin(2);
                    outInds = (temp_traj(:,1)<1) | (temp_traj(:,2)<1);
                    overInds = (temp_traj(:,1)>size(I_this,1)) | (temp_traj(:,2)>size(I_this,2));
                    temp_traj(outInds|overInds,:) = nan;
                    plot(temp_traj(:,1),temp_traj(:,2),'m.')
                end
            end
            drawnow
            hold off
            if frameInds(count)==224
                pause()
            end
            
            if isempty(pauseDur)
                pause()
            else
                pause(pauseDur)
            end
        end
        
        if writeVideo
            currFrame = getframe(fh);
            vidObj.writeVideo(currFrame);
        end
    end
    if writeVideo
        close(vidObj)
    end
end
print('Done!')
end


