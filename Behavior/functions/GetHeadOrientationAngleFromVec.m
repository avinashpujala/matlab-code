function theta = GetHeadOrientationAngleFromVec(hOr)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

theta = zeros(length(hOr),1);
for jj = 1:length(hOr)
    foo = hOr{jj}-repmat(hOr{jj}(1,:),[length(hOr{jj}),1]);
    theta(jj) = round(angle(foo(end,1) + foo(end,2)*1i)*(180/pi));
end


end

