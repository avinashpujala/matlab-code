function varargout = FindFishPxlsForMidlineTracking_20180325(I,varargin)
%FindFishPxlsForMidlineTracking Given an image stack and a few optional
%   parameters, returns binary images with fish pixels set to 1. Assumes
%   that the fish head position is in the middle of the image for each
%   image. Use CropImages.m to create such head-centered images.
% I_fish = FindFishPxlsForMidlineTracking(I);
% [I_fish,thr_all,boxPts,boxInds] = FindFishPxlsForMidlineTracking(I,'headDiam',headDiam,'thr',thr);
% Inputs:
% I - Image stack in which to find fish pixels. Size = [M, N, T], where T
%   is # of time points
% Optional inputs:
% headDiam - Approximate diameter of the head in mm (Default = 1). This determines
%   the size of the gaussian kernel used for smoothing, and later for thickening
%   connections between fragmented blobs and filling holes in fish.
% thr - Scalar. If given as input, then uses this value to threshold fish
%   pixels instead of computing
% Outputs:
% I_fish - Binary image stack with same size as I with fish pixels set to 1.
% thr_all - A vector of threshold values used on each image
%
% Avinash Pujala, Koyama lab/JRC, 2018

%=== Default values
headDiam = 1; % In mm
pxlLen = 0.05; % Approximate length of pxl. This is what it usually ends up being for my images.
minExpectedFishLen = 3; % In mm
nWorkers = 10;
thr =[];

for jj  = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'pxllen'
                pxlLen = varargin{jj+1};
            case 'thr'
                thr = varargin{jj+1};
            case 'minexpectedfishlen'
                minExpectedFishLen = varargin{jj+1};
        end
    end
end

p = gcp('nocreate');
if isempty(p)
    p = parpool(nWorkers);
end

headDiam_pxl = round(headDiam/pxlLen);
ker = gausswin(ceil(headDiam_pxl/2));
ker = (ker(:)*ker(:)');
ker = ker./sum(ker(:));

I(I<0)=0;
I_fish = convn(I,ker,'same');

if isempty(thr)
    thr =  GetMultiThr(I_fish,3);
    thr_prev = thr(2);
else
    thr_prev = thr;
end

I_fish(I_fish < thr_prev) = 0;

%--- A quick estimate of the expected # of fish pxls in each image
nFishPxls = sum(I_fish(:)>0)/size(I,3);
nFishPxls = round([nFishPxls*0.9,1.5*nFishPxls]); % Better to have false +ves than false -ves
% nFishPxls_target = mean(nFishPxls);

thr_all = zeros(size(I,3),1);
imgInds = 1:size(I,3);
imgInds = imgInds(:)';
disp('Initial, single threshold estimation of fish pixels...')
parfor jj = imgInds    
    [I_fish(:,:,jj),thr_all(jj)] = ffp(I_fish(:,:,jj),thr_prev,nFishPxls);
end

% I_fish = I>=thr(2);

%% Get rid of regions that do not include fish head position

boxInds = cell(numel(imgInds),1);
boxPts = boxInds;
disp('Final, iterative threshold estimation of fish pixels...')
for jj = imgInds
    disp(['Img #' num2str(jj)])
    try
    [I_fish(:,:,jj),boxPts{jj},boxInds{jj}] = fpp_iter(I_fish(:,:,jj),headDiam,...
        minExpectedFishLen,pxlLen);
    catch
        [I_fish(:,:,jj),boxPts{jj},boxInds{jj}] = fpp_iter(I_fish(:,:,jj),headDiam,...
        minExpectedFishLen,pxlLen);
    end
end

%--- Make a smaller smoothing kernel for use in bridging gaps between blobs
ker_small = gausswin(max(ceil(headDiam_pxl/7),2));
ker_small = (ker_small(:)*ker_small(:)');
ker_small = ker_small./sum(ker_small(:));
disp('Thickening binary images by smoothing a bit...')
I_fish = convn(double(I_fish),ker_small,'same');

varargout{1} = I_fish;
varargout{2} = thr_all;
varargout{3} = boxPts;
varargout{4} = boxInds;

end

function [img,boxPts,boxInds] = fpp_iter(img,headDiam,minExpectedFishLen,pxlLen)
% Iteratively find fish pixels in image in which fish pixels have been
% found previously using a single threshold
[boxPts,boxInds] = deal([]);
headDiam_pxl = round(headDiam/pxlLen);
minExpectedFishLen_pxl = round(minExpectedFishLen/pxlLen);
calcDist = @(manyPts,singlePt) sqrt(sum((repmat(singlePt,size(manyPts,1),1)-manyPts).^2,2));
img_orig = img;
img_bin = imbinarize(img,0);
% In these images, the fish head position is always in the middle of the image
fp = ceil(size(img)/2);
rp = regionprops(bwconncomp(img_bin),'PixelList','PixelIdxList','MajorAxisLength',...
    'MinorAxisLength','Eccentricity','Extent','Orientation','ConvexHull');
d_min = nan(length(rp),1);
for kk = 1:length(rp)
    d_min(kk) = min(calcDist(rp(kk).PixelList,fp));
end
[~,headBlobInd] = min(d_min);
allInds = 1:length(rp);
otherInds = setdiff(allInds,headBlobInd);
rpDelInds = zeros(length(rp),1);
for kk = otherInds(:)'
    if (rp(kk).MajorAxisLength*pxlLen > 4) || rp(kk).Extent <0.2
        rpDelInds(kk)=1;
    end
end
delInds = find(rpDelInds);
rp(delInds)=[];
allInds(delInds) = [];
headBlobInd = find(allInds==headBlobInd);
img_rp = img_orig*0;
if length(rp)>1
    for kk = 1:length(rp)
        img_rp(rp(kk).PixelIdxList)=1;
    end
    foo = img_orig*0;
    foo(rp(headBlobInd).PixelIdxList) = 1;
    [~,~,dsVec] = GetMidlinesByThinning_new(foo,'process','serial');
    if ~isempty(dsVec{1})
        temp = dsVec{1}(end);
    else
        temp = 0;
    end
    if temp < (minExpectedFishLen_pxl)         
        params.head_len = headDiam_pxl;
        [~,boxPts] = FitBoxTrainToFish_20180410(img_rp, params);
        boxPts = round([boxPts{1};boxPts{2}; boxPts{3}]);
        boxInds = sub2ind(size(img_rp),boxPts(:,1),boxPts(:,2));
    end
    otherInds = setdiff(1:length(rp),headBlobInd);
    delInds = otherInds*0;
    for kk = otherInds(:)'
        commonInds = intersect(rp(kk).PixelIdxList,boxInds);
        if (numel(commonInds)/numel(rp(kk).PixelIdxList)) < 0.3
            delInds(kk) = 1;
        end
    end
end
if ~isempty(delInds)
    try
    rp(find(delInds)) = [];
    catch
        a = 1
    end
end
foo = img_orig*0;
if ~isempty(rp)
    for kk = 1:length(rp)
        foo(rp(kk).PixelIdxList) = img(rp(kk).PixelIdxList);
    end
else
    disp('No fish pxls detected!')
end

if length(rp)>1
    disp(['Connecting ' num2str(length(rp)) '  blobs'])
end
for kk = 1:length(rp)-1
    blob1 = rp(kk).PixelList;
    blob2 = rp(kk+1).PixelList;
    closestPts = FindClosestPoints(blob1,blob2);
    line_x = linspace(closestPts{1}(1,1),closestPts{2}(1,1),20);
    line_y = linspace(closestPts{1}(1,2),closestPts{2}(1,2),20);
    lineInds = sub2ind(size(foo),round(line_y),round(line_x));
    foo(lineInds) = max(foo(:));
end
img = foo;
end

function [img,thr_new] = ffp(img,thr_prev,nFishPxls)
nFishPxls_target = mean(nFishPxls);
iter = 0;
thr_new = thr_prev;
img_new = imbinarize(img,thr_prev);
nFishPxls_img = sum(img_new(:));
dThr = 10;
while (iter <100) && ((nFishPxls_img < nFishPxls(1)) || (nFishPxls_img > nFishPxls(2))) && abs(dThr)>0.5
    epsilon = (nFishPxls_img-nFishPxls_target)/(nFishPxls_target+nFishPxls_img);
    sf = 1+epsilon; % So that it never becomes 0
    thr_prev = thr_new;
    if (nFishPxls_img > nFishPxls(2))
        thr_new = thr_new*sf;
    elseif (nFishPxls_img < nFishPxls(1))
        thr_new = thr_new*sf;
    end
    dThr = abs(thr_new-thr_prev);
    img_new = imbinarize(img,thr_new);
    nFishPxls_img = sum(img_new(:));
    %        cc = bwconncomp(img_new);
    iter = iter + 1;
    img(img<thr_new) = 0;
end

end

