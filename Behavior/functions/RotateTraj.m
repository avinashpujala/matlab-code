function traj_rot = RotateTraj(traj, theta)
%RotateTraj - Given a trajectory  in 2D of size [N, 2], returns the 
% trajectory after rotation by specified angle.
% traj_rot = RotateTraj(traj,theta);
% Inputs:
% traj - Matrix of size [N, 2], where is the number of points in the
%   trajectory, and the 2 columns are the x and y coordinates respectively
% theta - Angle in degrees by which all the points in the trajectory are to
%   be rotated
% 
% Avinash Pujala, Koyama lab/HHMI, 2016

if size(traj,1)==2 && size(traj,2)~=2
    traj = traj';
end
theta = theta*(pi/180); % Convert to radians
T_rot =[[cos(theta), -sin(theta)]; [sin(theta) cos(theta)]]; % Rotation transform
traj_rot = (T_rot*traj')';

end

