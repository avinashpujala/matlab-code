function data = GetSwimParams(Y,dt,varargin)
% GetSwimInfo - Extract swim parameters and append to data
%   structure
% data = GetSwimInfo(data)
% data = GetSwimInfo(data,'ampThr',ampThr,'maxFreq',maxFreq)
% Inputs:
% Y - Bend angle timeseries of a single episode. If multiple epiodes
%   present then swim duration estimate can be wrong. If size is [N,2],
%   then assumes that 1st col is head angles and 2nd col is tail angles
%   and returns phase information as well.
% dt - Sampling interval in seconds.
% ampThr - Relative amplitude threshold (see GetPks.m) for detecting peaks.
%   Default = 4.
% maxFreq - Maximum expected frequency (Hz). This determines the minimum interval
% interval between peaks. Default = 60.
% Outputs:
% data - Structure variable with fields containing various relevant swim
%   parameters.
% data.pkAmps = Absolute amplitudes. These are the values of the signal at
%                peaks
%      .bendAmps = The amplitudes of bends to one side and then another, i.e.
%                 the difference between successive pkAmps. The first
%                 bendAmp value is w.r.t the value of the signal at onset.
%       .bendInts - Time intervals (sec) between successive bends in one direction
%           and then another.
%       .zeroCrossingInts - Time interals between zero crossings.
% Avinash Pujala, Koyama lab/HHMI, 2018

ampThr = 4; % Amp threshold in degrees
maxFreq = 60; % Time window (in sec) in which to count swim episodes for computing swim rate

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'ampthr'
                ampThr = varargin{jj+1};
            case 'maxfreq'
                maxFreq = varargin{jj+1};
        end
    end
end

%% Resampling if lower that 500 Hz
if abs((1/dt)-500) > 0.5
    disp('Resampling to 500 Hz')
    fps = round(1/dt);
    Y = resample(Y,500,fps);
    dt = 1/500;
end
Fs = 500;
freqRange = [0.5, maxFreq];
minPkDist = round((0.5/60)*(1/dt));
time = (0:size(Y,1)-1)*dt;
nSigs = size(Y,2);

% [data.pkAmps,data.bendAmps,data.bendInts, data.ampInds,...
%     data.zeroCrossingInts, data.pkVels,data.velInds,...
%     data.pkAccs, data.accInds]= deal(cell(nSigs,1));

data = struct;
data(nSigs).pkAmps = nan;
for jj = 1:size(Y,2)
    y = Y(:,jj);    
    %--- Wavelet filtering
    [S,freq] = wsst(y,Fs);
    f_min = max(min(freq),min(freqRange));
    f_max = min(max(freq),max(freqRange));
    y = iwsst(S,freq,[f_min,f_max]);
    pks = GetPks(y,'polarity',0,'peakThr',ampThr,'minPkDist',minPkDist);
    if length(pks)>=3
        y_pks = y(pks);
        y_rad = y*pi/180;
        dy = gradient(y_rad)/dt;
        d2y = gradient(dy)/dt;
        pks_vel = GetPks(dy,'polarity',0,'minPkDist',minPkDist);
        pks_acc = GetPks(d2y,'polarity',0,'minPkDist',minPkDist);        
     
        zeroCrossings = LevelCrossings(y,0);        
        
        upCrossings = zeroCrossings{1};
        upCrossings(upCrossings>=pks(1))=[];
        if ~isempty(upCrossings)
            onset = upCrossings(end);
        else
            onset = 1;
        end
        
        downCrossings = zeroCrossings{2};
        downCrossings(downCrossings<pks(end)) = [];
        
        if ~isempty(downCrossings)
            offset = downCrossings(1);
        else
            offset = length(y);
        end
        
%         bendAmps = abs(diff([y(onset); y_pks(:); y(offset)]));
        ampInds = [onset; pks(:); offset];
        bendAmps = diff(y(ampInds));
        bendInts = diff(time(ampInds));
        
        inds = find(pks_vel>= onset & pks_vel <= offset);
        firstInd = max(inds(1)-1,1);
        lastInd = min(inds(end)+1,length(pks_vel));
        velInds = unique([firstInd; inds(:);lastInd]);
        pks_vel = pks_vel(velInds);
        dy_pks = dy(pks_vel); % radians/sec
        
        inds = find(pks_acc>= onset & pks_acc <= offset);
        firstInd = max(inds(1)-1,1);
        lastInd = min(inds(end)+1,length(pks_acc));
        accInds = unique([firstInd; inds(:);lastInd]);
        pks_acc = pks_acc(accInds);
        d2y_pks = d2y(pks_acc); % radians/sec^2
        
        zeroCrossings = union(zeroCrossings{1},zeroCrossings{2});
        foo = zeroCrossings-onset;
        foo(foo>0)=-inf;
        [~,firstCrossing] = max(foo);
        firstCrossing = zeroCrossings(firstCrossing);
        foo = zeroCrossings-offset;
        foo(foo<0)=inf;
        [~,lastCrossing] = min(foo);
        lastCrossing = zeroCrossings(lastCrossing);
        zeroCrossings(zeroCrossings < onset | zeroCrossings>offset)=[];
        zeroCrossings = unique([firstCrossing; zeroCrossings(:); lastCrossing]);
        zeroCrossings = diff(time(zeroCrossings));
        
        nBends = length(bendAmps);
        nVels = length(dy_pks);
        if nVels > nBends
            dy_pks = dy_pks(1:nBends);
        elseif nVels < nBends
            lenDiff = nBends-nVels;
            dy_pks(end:end+lenDiff) = 0;
            try
            velInds(end+1:end+lenDiff) = (1:lenDiff) + velInds(end);
            catch
                velInds(end+1:end+lenDiff) = (1:lenDiff) + velInds(end);
            end
        end
        
        nAccs = length(d2y_pks);
        if nAccs > nBends
            d2y_pks = d2y_pks(1:nBends);
        elseif nAccs < nBends
            lenDiff = nBends-nAccs;
            d2y_pks(end:end+lenDiff) = 0;
            accInds(end+1:end+lenDiff) = (1:lenDiff) + accInds(end);
        end        
        data(jj).pkAmps = y_pks;
        data(jj).bendAmps = bendAmps;
        data(jj).ampInds = ampInds(1:length(bendAmps));
        data(jj).bendInts = bendInts;
        data(jj).zeroCrossingInts = zeroCrossings;
        data(jj).pkVels = dy_pks;
        if length(dy_pks) <= length(velInds)
             data(jj).velInds = velInds(1:length(dy_pks));
        else
            data(jj).velInds = velInds;
        end       
        data(jj).pkAccs = d2y_pks;
        if length(d2y_pks) <= length(accInds)
             data(jj).accInds = accInds(1:length(d2y_pks));
        else
            data(jj).accInds = accInds;
        end       
    else
        disp('No peaks above specified threshold!') 
        data(jj).pkAmps = nan;
        data(jj).bendAmps = nan;
        data(jj).ampInds = nan;
        data(jj).bendInts = nan;
        data(jj).zeroCrossingInts = nan;
        data(jj).pkVels = nan;
        data(jj).velInds = nan;
        data(jj).pkAccs = nan;
        data(jj).accInds = nan;
    end    
end

allSigs = 1:nSigs;
for jj = 1:nSigs
    otherInd = setdiff(allSigs,jj);
    inds = data(jj).ampInds;
    if ~isnan(sum(inds))
        data(jj).bendAmps_corr = Y(data(jj).ampInds,otherInd);
        foo = gradient(Y(:,otherInd));
        foo = (foo*pi)/(180*dt);
        data(jj).pkVels_corr = foo(data(jj).velInds);
    else
        data(jj).bendAmps_corr = nan;
        data(jj).pkVels_corr = nan;
    end 
end

if numel(data(jj).bendAmps)>=3
    [W,freq] = ComputeXWT(Y(:,1),Y(:,2),dt,'freqRange',freqRange, 'dj',1/32);
    if ~isempty(W)
        [~,data(1).phaseDiff] = instantaneouswavephase(W);
        data(1).meanFreq = instantaneouswavefreq(W,freq);
    else
        data(1).phaseDiff = ones(size(Y,1),1)*nan;
        data(1).meanFreq = ones(size(Y,1),1)*nan;
    end
else
    data(1).phaseDiff = ones(size(Y,1),1)*nan;
    data(1).meanFreq = ones(size(Y,1),1)*nan;
end

end

