
clear all, close all
fishDir = '\\Koyama-S2\Data3\Rig2\Avinash\Ablations and Behavior\M-homologs\20190521_fsb';
% fishDir = 'S:\Avinash\Miscellaneous\test';

fps = 500;
nBlocks = 1; % # of blocks in trl
nFramesInTrl = 750;

%% Getting the names of fish image dirs

disp('Getting the names of fish dirs...')
tic
blah = dir(fishDir);
imgDirs = {};
count = 0;
for jj = 1:length(blah)
    nm = blah(jj).name;
    if isempty(strfind(nm,'.'))
        count = count + 1;
        imgDirs{count} = fullfile(fishDir,nm);
    end
end
toc



%% Go into fast dir and process
tic
for fishNum = 1:length(imgDirs)
    fishName = ['f' num2str(fishNum)];
    disp(['Fish # ' num2str(fishNum)]);
    blah  = dir(imgDirs{fishNum});
    for jj = 1:length(blah)
        nm = blah(jj).name;
        if isempty(strfind(lower(nm),'.')) && ~isempty(strfind(lower(nm),'fast'))
            path = fullfile(imgDirs{fishNum},nm);
%             disp(nm)
%             blah2 = dir(path);
%             for kk = 1:length(blah2)
%                 nm = blah2(kk).name;
%                 if isempty(strfind(lower(nm),'.'))
%                     imgDir = fullfile(path,nm);
%                     ProcessFishImages(imgDir)
                    ProcessFishImages_180629(path,'nBlocks',nBlocks,'nFramesInTrl',nFramesInTrl,'fps',fps)                 
%                 end
%             end
        end
    end
end
toc
