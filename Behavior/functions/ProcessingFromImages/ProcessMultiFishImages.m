function varargout = ProcessMultiFishImages(varargin)
% ProcessMultiFishImages(pathList);
% procData = ProcessMultiFishImages(pathList);
% procData = ProcessMultiFishImages(pathList,'readMode',readMode,'fps',fps,'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,...
%   'headDiam',headDiam,'fishLen',fishLen,'lineLen',lineLen,'spatialFilt',spatialFilt,'imgInds',imgInds,...
%   'blockSize',blockSize,'cropWid',cropWid,'backMethod',backMethod);
% Inputs:
% If no inputs are specified, allows for interactive selection of image
%   directory and use default parameters for processing.
% pathList - Cell array of paths, where each path points to the location of
%   a set of images such as all vibration or dark flash trials from an
%   experiment.
% readMode - 'fromImages' or 'fromMishVid'. The former results in reading
%   of an image sequence, whereas the latter reads .mishVid created by
%   Misha Ahrens.
% imgExt - Image extension; reads only images in specified folder with this
%   extension ['bmp']
% fps - Frames per second [500].
% nFramesInTrl = Number of frames in a single trial [750];
% spatialFilt - Spatial filter used to smooth images before finding fish's
%   head centroid
% imgInds - Vector of image indices to read. If empty, reads all images.
%   By default reads all images.
% headDiam - Approx diam of fish head in mm [default = 1.5]
% fishLen - Approx length of fish in mm [default = 10]
% blockSize - Loading all images can take up too much memory, so this
%   allows processing by splitting total number of images in this many
%   blocks and processing one block at a time. If empty, or by default
%   blockSize = 4.
% cropWid - Crop width. After finding fish, crops image by this width
%   around the fish to save storage space.
% lineLen - Length of line in pixels to use for estimating head orienation
%   [15].
% backMethod - 'mean'[default], 'median', or 'maxInt'; Method for
%   background subtraction
% trackMidlines - Boolean specifying whether or not to track midlines
% Outputs:
% procData - Mat file saving all relevant info after processing.
%
% Avinash Pujala, Koyama lab/HHMI, 2016

readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [];
fps = 500;
nFramesInTrl = 750;
spatialFilt = 30;
headDiam = 1.5; % Approx diam of fish head in mm
fishLen = 10;
lineLen = 15;
blockSize = -1; % If blockSize == -1, then chunks into trials
cropWid = 90; %(For imgDims ~ [900,900])
backMethod = 'mean';
trackMidlines = true;
process = 'parallel';

for jj  = 1:nargin
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'readmode'
                readMode = varargin{jj+1};
            case 'imgext'
                imgExt = varargin{jj+1};
            case 'imginds'
                imgInds = varargin{jj+1};
            case 'fps'
                fps = val;
            case 'nframesintrl'
                nFramesInTral = varargin{jj+1};
            case 'spatialfilt'
                spatialFilt = varargin{jj+1};
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'fishlen'
                fishLen = varargin{jj+1};
            case 'linelen'
                lineLen = varargin{jj+1};
            case 'blocksize'
                blockSize = varargin{jj+1};
            case 'cropwid'
                cropWid = varargin{jj+1};
            case 'backmethod'
                backMethod = varargin{jj+1};
            case 'trackmidlines'
                trackMidlines = varargin{jj+1};
            case 'process'
                process = varagin{jj+1};
        end
    end
end

pathList = varargin{1};
if ~iscell(pathList)
    pathList = {pathList}; % Encapsulate single path string in a cell.
    
end
procData = cell(length(pathList),1);

tic
for pp = 1:length(pathList)
    [currPath,currFile] = fileparts(pathList{pp});
    if ~strcmpi(currFile,'proc')
        currPath = fullfile(currPath,currFile);
    end
    disp(['Processing data at # ' ])
    disp(currPath)
    
    procData{pp} = ProcessMultiFishImages_subset(currPath,'readMode',readMode,'imgInds',imgInds,'fps',fps,...
        'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,'headDiam',headDiam,'fishLen',fishLen,'lineLen',lineLen,...
        'spatialFilt',spatialFilt,'blockSize',blockSize,'cropWid',cropWid,'backMethod',backMethod,...
        'trackMidlines',trackMidlines,'process',process);
    toc
end
toc

varargout{1}= procData;

end


function varargout = ProcessMultiFishImages_subset(varargin)
% ProcessMultiFishImages();
% procData = ProcessMultiFishImages();
% procData = ProcessMultiFishImages(imgDir);
% procData = ProcessMultiFishImages(imgDir,'readMode',readMode,'fps',fps,'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,...
%   'nHeadPxls',nHeadPxls,'lineLen',lineLen,'spatialFilt',spatialFilt,'imgInds',imgInds,'blockSize',blockSize,'cropWid',cropWid);
% Inputs:
% If no inputs are specified, allows for interactive selection of image
%   directory and use default parameters for processing.
% imgDir - Path to image directory. If empty, allows for interactive
%   selection of path by selecting any file in the directory.
% readMode - 'fromImages' or 'fromMishVid'. The former results in reading
%   of an image sequence, whereas the latter reads .mishVid created by
%   Misha Ahrens.
% imgExt - Image extension; reads only images in specified folder with this
%   extension ['bmp']
% fps - Frames per second [500].
% nFramesInTrl = Number of frames in a single trial [750];
% spatialFilt - Spatial filter used to smooth images before finding fish's
%   head centroid
% imgInds - Vector of image indices to read. If empty, reads all images.
%   By default reads all images
% headDiam - Approx diameter of fish head in mm (for spatial filtering)[default = 1.5]
% fishLen - Approx length of fish in mm [default = 10]
% blockSize - Loading all images can take up too much memory, so this
%   allows processing by splitting total number of images in this many
%   blocks and processing one block at a time. If empty, or by default
%   blockSize = 4.
% cropWid - Crop width. After finding fish, crops image by this width after
%   aroung the fish to save space
% lineLen - Length of line in pixels to use for estimating head orienation
%   [15].
% backMethod - 'mean'[default], 'median', or 'maxInt'; Method for
%   background subtraction
% trackMidlines - Boolean. If true, tracks fish midlines as well.
% Outputs:
% procData - Mat file saving all relevant info after processing
%
% Avinash Pujala, Koyama lab/HHMI, 2016

imgDir = [];
readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [];
fps = 500;
nFramesInTrl = 750;
spatialFilt = 30;
headDiam = 1.5;
fishLen = 10; % In mm
lineLen = 15;
blockSize = -1;
cropWid = 90; %( For imgDims ~ [900,900])
poolSize  = 10;
backMethod = 'mean';
trackMidlines = true;
process = 'parallel';

nFish = -1;

for jj  = 2:nargin
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'readmode'
                readMode =  varargin{jj+1};
            case 'imgext'
                imgExt =  varargin{jj+1};
            case 'imginds'
                imgInds =  varargin{jj+1};
            case 'fps'
                fps = varargin{jj+1};
            case 'nframesintrl'
                nFramesInTral =  varargin{jj+1};
            case 'spatialfilt'
                spatialFilt =  varargin{jj+1};
            case 'headdiam'
                headDiam =  varargin{jj+1};
            case 'fishlen'
                fishLen = varargin{jj+1};
            case 'linelen'
                lineLen =  varargin{jj+1};
            case 'blocksize'
                blockSize =  varargin{jj+1};
            case 'cropwid'
                cropWid =  varargin{jj+1};
            case 'backmethod'
                backMethod = varargin{jj+1};
            case 'trackmidlines'
                trackMidlines = varargin{jj+1};
            case 'process'
                process = varargin{jj+1};
        end
    end
end

imgExt(strfind(imgExt,'.'))=[];
if nargin ==0
    [~, imgDir] = uigetfile(['*.' imgExt]);
else
    imgDir = varargin{1};
    if isempty(imgDir)
        [~,imgDir] = uigetfile(['*.' imgExt]);
    end
end

switch readMode
    case 'fromMishVid'
        [I, outDir] = ReadMishVid();
        imgInds = 1:size(I,3);
    case 'fromImages'
        I = ReadImgSequence(imgDir,imgExt,imgInds);
end

outDir = fullfile(imgDir,'proc');
if ~exist(outDir,'dir')
    mkdir(outDir)
end


%% Processing images block-by-block
tic
if matlabpool('size')==0
    matlabpool(poolSize)
end

nTrls = size(I,3)/nFramesInTrl;
disp([num2str(nTrls) ' trl detected'])
if blockSize == -1
    blockSize = max([nTrls, 1]);
end
disp('Block-by-block processing...')
blockInds = round((1:blockSize)*(size(I,3)/blockSize));
blockInds = [1,blockInds(:)'];
blockInds(end) = size(I,3);
imgDims = size(I);
fishPos = cell(numel(blockInds)-1,1);
imgInds_block = fishPos;
midlines_block = fishPos;
failedInds_block = fishPos;
im_proc = [];
for block = 1:numel(blockInds)-1
    if block ==1
        imgFrames = blockInds(block):blockInds(block+1);
        disp(['Block # ' num2str(block),...
            ', images: ' num2str(imgFrames(1)) ' - ' num2str(imgFrames(end))])
        [im_proc,ref] = SubtractBackground(I(:,:,imgFrames),'method',backMethod);
        pxlLen = GetPxlSpacing(ref,'diam',50);
        [fishPos{block},rp]= GetMultiFishPos_new(im_proc,'pxlLen',pxlLen,'headDiam',headDiam,'nFish',nFish);
        nFish = length(fishPos{block});
    else
        imgFrames = blockInds(block)+1:blockInds(block+1);
        disp(['Block # ' num2str(block),...
            ', images: ' num2str(imgFrames(1)) ' - ' num2str(imgFrames(end))])
        [im_proc,~] = SubtractBackground(I(:,:,imgFrames),'backImg',ref);
        fishPos{block}= GetMultiFishPos_new(im_proc,'pxlLen',pxlLen,'headDiam',headDiam,'nFish',nFish,'regionprops',rp);
    end
    imgInds_block{block} = imgFrames;
    midlines_block{block} = cell(length(fishPos{block}),1);
    failedInds_block{block} = cell(length(fishPos{block}),1);
    if trackMidlines
        for fish = 1:length(fishPos{block})
            disp(['Fish # ' num2str(fish)])
            im_spot = zeros(size(im_proc));
            parfor kk = 1:numel(imgFrames)
                im_spot(:,:,kk) = SpotlightFish(im_proc(:,:,kk),fishPos{block}{fish}(kk,:),ceil(fishLen/pxlLen),'gauss');
            end
            fp = fishPos{block}{fish};
            [mlInds,dsVecs,failedInds, mLens] = GetMidlinesByThinning_new(im_spot,'fishPos',fp,'process',process,...
                'plotBool',0,'kerSize',8,'pxlLen',pxlLen);
            [tC, tC_uncorr] = SmoothenMidlines_new(mlInds,im_spot,3,'plotBool',0,...
                'pauseDur',0,'smoothFactor',8,'dsVecs',dsVecs);
            midlines_block{block}{fish} = tC;
            failedInds_block{block}{fish} = failedInds;
        end
    end
end

toc



%% Saving data
disp('Saving data ....')
ts = datestr(now,30);
procData = matfile(fullfile(outDir,['procData_' ts '.mat']),'Writable',true);
procData.fishPos = fishPos;
procData.ref = ref;
procData.fps = fps;
procData.imgDims = size(I);
procData.pxlLen = pxlLen;
if trackMidlines
    procData.midlineInds = midlines_block;
    procData.failedInds = failedInds_block;
    procData.imgInds = imgInds_block;
else
    [procData.midlineInds,procData.failedInds, procData.imgInds]  =  deal(nan);
end

toc

varargout{1} = procData;

end

function Y = Matricize(X)
nDims = nan(1,3);
nDims(3) = length(X{1});
nDims(1:2) = size(X{1}{1});
Y = nan(nDims);
outer = [];
for bb = 1:length(X)
    inner = [];
    for ff =1:length(X{bb})
        inner = cat(3,inner,X{bb}{ff});
    end
    outer = cat(1,outer,inner);
end
Y = outer;
end
