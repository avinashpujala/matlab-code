
function varargout = ProcessFishImages_180312(varargin)
% ProcessFishImages(pathList);
% procData = ProcessFishImages(pathList);
% procData = ProcessFishImages(pathList,'readMode',readMode,'fps',fps,'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,...
%   'nHeadPxls',nHeadPxls,'imgInds',imgInds,...
%   'blockSize',blockSize,'cropWid',cropWid,'backMethod',backMethod,'trlNums',trlNums);
% Inputs:
% If no inputs are specified, allows for interactive selection of image
%   directory and use default parameters for processing.
% pathList - Cell array of paths, where each path points to the location of
%   a set of images such as all vibration or dark flash trials from an
%   experiment.
% readMode - 'fromImages' or 'fromMishVid'. The former results in reading
%   of an image sequence, whereas the latter reads .mishVid created by
%   Misha Ahrens.
% imgExt - Image extension; reads only images in specified folder with this
%   extension ['bmp']
% fps - Frames per second [500].
% nFramesInTrl = Number of frames in a single trial [750];
% spatialFilt - Spatial filter used to smooth images before finding fish's
%   head centroid
% imgInds - Vector of image indices to read. If empty, reads all images.
%   By default reads all images.
% nBlocks - Loading all images can take up too much memory, so this
%   allows processing by splitting total number of images in this many
%   blocks and processing one block at a time. If empty, or by default
%   blockSize = 4. If blockSize = -1, then processes in trial-sized blocks
% cropWid - Crop width. After finding fish, crops image by this width
%   around the fish to save storage space.
% backMethod - 'mean'[default], 'median', or 'maxInt'; Method for
%   background subtraction
% tryCorrect' - Boolean; If true, tries to correct the erroneous fish
%   positions based on the assumption that these can jump around from one
%   frame to the next.
% trlNums - If specified, then only processes these trials and appends the
%   updated info the most recent procData.mat file in the relevant
%   directory. By default, trlNums = [], in which case starts from scratch.
% headDiam = Approx head diameter in mm (Default = 1). Determines smoothing
%   when finding fish pixels prior to midline tracking
% Outputs:
% procData - Mat file saving all relevant info after processing.
%
% Avinash Pujala, Koyama lab/HHMI, 2016

readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [];
fps = 500;
nFramesInTrl = 750;
nBlocks = 4;
cropWid = 90; %(For imgDims ~ [900,900])
backMethod = 'mean';
tryCorrect = true;
headDiam = 1; % In mm
trlNums = [];

for jj  = 1:nargin
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'readmode'
                readMode = varargin{jj+1};
            case 'imgext'
                imgExt = varargin{jj+1};
            case 'imginds'
                imgInds = varargin{jj+1};
            case 'fps'
                fps = varargin{jj+1};
            case 'nframesintrl'
                nFramesInTrl = varargin{jj+1};         
            case 'nblocks'
                nBlocks = varargin{jj+1};
            case 'cropwid'
                cropWid = varargin{jj+1};
            case 'backmethod'
                backMethod = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'trlnums'
                trlNums = varargin{jj+1};
        end
    end
end

pathList = varargin{1};
if ~iscell(pathList)
    pathList = {pathList}; % Encapsulate single path string in a cell.
    
end
procData = cell(length(pathList),1);

tic
for pp = 1:length(pathList)
    [currPath,currFile] = fileparts(pathList{pp});
    if ~strcmpi(currFile,'proc')
        currPath = fullfile(currPath,currFile);
    end
    fprintf('Processing ... \n')
    disp(currPath)
    procData{pp} = ProcessFishImages_subset(currPath,'readMode',readMode,'imgInds',imgInds,'fps',fps,...
        'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,'nblocks',nBlocks,'cropWid',cropWid,'backMethod',backMethod,...
        'tryCorrect',tryCorrect,'headDiam',headDiam,'trlNums',trlNums);
    toc
end
toc

varargout{1}= procData;

end


function varargout = ProcessFishImages_subset(varargin)
% ProcessFishImages();
% procData = ProcessFishImages();
% procData = ProcessFishImages(imgDir);
% procData = ProcessFishImages(imgDir,'readMode',readMode,'fps',fps,'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,...
%   'imgInds',imgInds,'blockSize',blockSize,'cropWid',cropWid);
% Inputs:
% If no inputs are specified, allows for interactive selection of image
%   directory and use default parameters for processing.
% imgDir - Path to image directory. If empty, allows for interactive
%   selection of path by selecting any file in the directory.
% readMode - 'fromImages' or 'fromMishVid'. The former results in reading
%   of an image sequence, whereas the latter reads .mishVid created by
%   Misha Ahrens.
% imgExt - Image extension; reads only images in specified folder with this
%   extension ['bmp']
% fps - Frames per second [500].
% nFramesInTrl = Number of frames in a single trial [750];
% spatialFilt - Spatial filter used to smooth images before finding fish's
%   head centroid
% imgInds - Vector of image indices to read. If empty, reads all images.
%   By default reads all images
% nHeadPxls - Number of head pixels; This is the number of pixels to use
%   for finding head centroid of fish [25]
% nBlocks - Loading all images can take up too much memory, so this
%   allows processing by splitting total number of images in this many
%   blocks and processing one block at a time. If empty, or by default
%   blockSize = 4.
% cropWid - Crop width. After finding fish, crops image by this width after
%   aroung the fish to save space
% backMethod - 'mean'[default], 'median', or 'maxInt'; Method for
%   background subtraction
% 'tryCorrect' - Boolean; If true, tries to correct the erroneous fish
%   positions based on the assumption that these can jump around from one
%    frame to the next.
% trlNums - If specified, then only processes these trials and appends the
%   updated info the most recent procData.mat file in the relevant
%   directory. By default, trlNums = [], in which case starts from scratch.
% Outputs:
% procData - Mat file saving all relevant info after processing
%
% Avinash Pujala, Koyama lab/HHMI, 2016

imgDir = [];
readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [];
fps = 500;
nFramesInTrl = 750;
nBlocks = 4;
cropWid = 90; %( For imgDims ~ [900,900])
poolSize  = 10;
backMethod = 'mode'; % Changed from 'mean' to 'mode' a la Marques, Orger et al. (2018).
tryCorrect = true;
len_midlines = 50;
headDiam = 1; % In mm
trlNums =[];

for jj  = 2:nargin
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'readmode'
                readMode =  varargin{jj+1};
            case 'imgext'
                imgExt =  varargin{jj+1};
            case 'imginds'
                imgInds =  varargin{jj+1};
            case 'len_midlines'
                len_midlines = varargin{jj+1};
            case 'fps'
                fps = varargin{jj+1};
            case 'nframesintrl'
                nFramesInTrl =  varargin{jj+1};
            case 'nblocks'
                nBlocks =  varargin{jj+1};
            case 'cropwid'
                cropWid =  varargin{jj+1};
            case 'backmethod'
                backMethod = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'trlnums'
                trlNums = varargin{jj+1};
        end
    end
end

imgExt(strfind(imgExt,'.'))=[];
if nargin ==0
    [~, imgDir] = uigetfile(['*.' imgExt]);
else
    imgDir = varargin{1};
    if isempty(imgDir)
        [~,imgDir] = uigetfile(['*.' imgExt]);
    end
end

% switch readMode
%     case 'fromMishVid'
%         [I, outDir] = ReadMishVid();
%         imgInds = 1:size(I,3);
%     case 'fromImages'
%         I = ReadImgSequence(imgDir,imgExt,imgInds);
% end

imgNamesInDir = GetFilenames(imgDir,'ext',imgExt);
totalNumImgsInDir = length(imgNamesInDir);

%---This is to speed up background subtraction and also to improve
%---background image when the not many images can be loaded because of
%---memory constraints
if totalNumImgsInDir >1000
    rp = randperm(totalNumImgsInDir);
    refFrames = rp(1:1000);   
end

outDir = fullfile(imgDir,'proc');
if ~exist(outDir,'dir')
    mkdir(outDir)
end


%% Background removal and fish position tracking in blocks of images
tic
poolObj = gcp('nocreate');
if isempty(poolObj)
    poolObj = parpool(poolSize);
end

nTrls = totalNumImgsInDir/nFramesInTrl;
disp([num2str(nTrls) ' trls detected'])

disp('Getting image info..')
imgInfo = imfinfo(fullfile(imgDir,imgNamesInDir{1}));
imgDims = [imgInfo.Height imgInfo.Width];
imgDims = [imgDims, totalNumImgsInDir];
ts = datestr(now,30);

%---Compute background
% disp(['Reading random sequence of ' num2str(numel(refFrames)) ' images, and computing background...'])
% I = ReadImgSequence(imgDir,[],[],imgNamesInDir(refFrames));
% [~,ref_all] = SubtractBackground(I,'method',backMethod);

if isempty(trlNums)
    if nBlocks == -1
        nBlocks = max([nTrls, 1]);
    end
    
    disp('Block-by-block processing...')
    blockSize = max(round(totalNumImgsInDir/nBlocks),nFramesInTrl);   
    blockInds = 1:blockSize:totalNumImgsInDir;
    blockInds = [blockInds, totalNumImgsInDir + 1];        
    
    procName = ['procData_' ts '.mat'];
    
    disp('Creating a new file at ')
    disp(fullfile(outDir,procName))
    
    procData = matfile(fullfile(outDir,procName),'Writable',true);
    procData.nFramesInTrl = nFramesInTrl;
    procData.fps = fps;
    procData.I_proc_crop = zeros(2*cropWid + 1, 2*cropWid + 1, totalNumImgsInDir);
    procData.tailCurv = zeros(len_midlines,2,totalNumImgsInDir);
    procData.curv = zeros(len_midlines,totalNumImgsInDir);
    procData.fishPos = zeros(totalNumImgsInDir,2);
    ref = zeros(imgDims(1),imgDims(2),nBlocks);  
    mlInds = cell(imgDims(3),1);
    dsVecs = cell(imgDims(3),1);
    for block = 1:numel(blockInds)-1
        imgFrames = blockInds(block):blockInds(block+1)-1;
        disp(['Block # ' num2str(block),...
            ', images: ' num2str(imgFrames(1)) ' - ' num2str(imgFrames(end))])
        disp('Reading images...')
        I = ReadImgSequence(imgDir,[],[],imgNamesInDir(imgFrames));
        disp('Subtracting background...')
%         [im_proc,ref(:,:,block)] = SubtractBackground(I,'method',backMethod,'backImg',ref_all);
        [im_proc,ref(:,:,block)] = SubtractBackground(I,'method',backMethod);
        % If, first block, then get pixel length
        if block ==1
            pxlLen = GetPxlSpacing(mean(ref(:,:,block),3),'diam',50);
            procData.pxlLen = pxlLen;
            kerSize = ceil(headDiam/pxlLen);
        end
        
        % I need to find fish position in trial sized chunks because of the
        %   sequential correction procedure
        subLists = SubListsFromList(imgFrames,nFramesInTrl);
        indsList = SubListsFromList(1:numel(imgFrames),nFramesInTrl);
        fp = nan(numel(imgFrames),2);
        for kk = 1:length(indsList)
            disp(['Trl # ' num2str((block-1)*kk + 1)])
            fp_temp = GetFishPos_180307(im_proc(:,:,indsList{kk}),'kerSize',kerSize,'process','parallel',...
                'tryCorrect', tryCorrect);
            fp(indsList{kk},:) = fp_temp;
            disp('Cropping images...')
            foo = CropImgsAroundPxl(im_proc(:,:,indsList{kk}),fp(indsList{kk},:),cropWid,'procType','parallel'); % Parallel, not working, need to fix bugs
            disp('Tracking midlines...')
            I_smooth = FindFishPxlsForMidlineTracking_20180325(foo,'headDiam',headDiam,'pxlLen',pxlLen);
            [midlines,mlInds_trl,dsVecs_trl] =  GetMidlinesByThinning_new(I_smooth,'process','parallel','plotBool',0);
            tA = GetTailTangents(midlines);           
            if fps > 120
                for tt = 1:size(tA,1)
                    tA(tt,:) = chebfilt(tA(tt,:),1/fps,60,'low');
                end
            end
            disp('Updating procData.mat ...')            
            procData.fishPos(subLists{kk},:) = fp_temp;           
            procData.I_proc_crop(:,:,subLists{kk}) = foo;
            procData.tailCurv(:,:,subLists{kk}) = midlines;
            procData.curv(:,subLists{kk}) = tA;
            mlInds(subLists{kk}) = mlInds_trl; % These cannot be directly updated in procData!!
            dsVecs(subLists{kk}) = dsVecs_trl;
        end
    end
    ref = mean(ref,3);
    disp('Appending addition variables to procData...')
    procData.ref = ref;
    procData.imgDims_crop = size(foo(:,:,1));
    procData.imgDims = size(I(:,:,1));
    procData.mlInds = mlInds;
    procData.dsVecs = dsVecs;
    clear im_proc fp hOr_temp    
else
    %%%% Check to see if there are any procData.mat files in outDir
    procFiles = GetFilenames(outDir, 'searchStr','procData','ext','mat');
    if ~isempty(procFiles)
        disp(['Found procData file! Will read and write to ' procFiles{end}])     
        %%%% Since each procFile name has a time-stamp in it, the last one will
        %%%% be the most recent one, so read and append to this one.
        try
            procData = OpenMatFile(fullfile(outDir,procFiles{end}));
            procData.Properties.Writable = true;
        catch
            error('Cannot read procData.mat file! Check for errors.')
        end
    else        
        procName = ['procData_' ts '.mat'];
        %%%% Presently I will not allow for the option to compute from
        %%%% scratch, if no procData.mat file is found.
        error('procData.mat file not found, try running from the start!')     
    end
    trlNumToImgInds = @(trlNum,nFramesInTrl) (trlNum-1)*nFramesInTrl+1:(trlNum-1)*nFramesInTrl + nFramesInTrl;
    pxlLen = procData.pxlLen;
    nFramesInTrl = procData.nFramesInTrl;
    mlInds = procData.mlInds;
    dsVecs = procData.dsVecs;
    for trl = trlNums(:)'
        disp(['Trl num # ' num2str(trl)])
        imgInds = trlNumToImgInds(trl,nFramesInTrl);
        disp('Reading processed images...')
        im_proc = procData.I_proc_crop(:,:,imgInds);
        I_smooth = FindFishPxlsForMidlineTracking(im_proc,'headDiam',headDiam,'pxlLen',pxlLen);
        [midlines,mlInds_trl,dsVecs_trl] =  GetMidlinesByThinning_new(I_smooth,'process','parallel','plotBool',0);
        tA = GetTailTangents(midlines);
        disp('Updating procData.mat ...')       
        procData.tailCurv(:,:,imgInds) = midlines;
        procData.curv(:,imgInds) = tA(1:size(procData.curv,1),:); % The size has to be specified because a change can result in mismatch if only reprocessing a few trials instead of computing from start
        mlInds(imgInds) = mlInds_trl; % These cannot be directly updated in procData!!
        dsVecs(imgInds) = dsVecs_trl;
    end
    procData.mlInds = mlInds;
    procData.dsVecs = dsVecs;
end
toc


%% Plot trialized tail bends
close all
TrializeTailBendsAndPlot
disp('Saving trialized tail curvature figures figures...')
suffix = ['_Trialized tail curvatures_' ts];
h = get(0,'children');
for fig = 1:length(h)
    prefix = ['Fig_' sprintf('%0.2d', fig)];
    saveas(h(fig), fullfile(outDir,[prefix, suffix]))
end


varargout{1} = procData;

end
