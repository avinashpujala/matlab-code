
function varargout = ProcessFishImages_180629(varargin)
% ProcessFishImages(pathList); % In this version, I am trying to further
%   reduce demands on memory.
% procData = ProcessFishImages(pathList);
% procData = ProcessFishImages(pathList,'readMode',readMode,'fps',fps,'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,...
%   'nHeadPxls',nHeadPxls,'imgInds',imgInds,...
%   'blockSize',blockSize,'cropWid',cropWid,'backMethod',backMethod,'trlNums',trlNums);
% Inputs:
% If no inputs are specified, allows for interactive selection of image
%   directory and use default parameters for processing.
% pathList - Cell array of paths, where each path points to the location of
%   a set of images such as all vibration or dark flash trials from an
%   experiment.
% readMode - 'fromImages' or 'fromMishVid'. The former results in reading
%   of an image sequence, whereas the latter reads .mishVid created by
%   Misha Ahrens.
% imgExt - Image extension; reads only images in specified folder with this
%   extension ['bmp']
% fps - Frames per second [500].
% nFramesInTrl = Number of frames in a single trial [750];
% spatialFilt - Spatial filter used to smooth images before finding fish's
%   head centroid
% imgInds - Vector of image indices to read. If empty, reads all images.
%   By default reads all images.
% nBlocks - Loading all images can take up too much memory, so this
%   allows processing by splitting total number of images in this many
%   blocks and processing one block at a time. If empty, or by default
%   blockSize = 4. If blockSize = -1, then processes in trial-sized blocks
% cropWid - Crop width. After finding fish, crops image by this width
%   around the fish to save storage space.
% backMethod - 'mean'[default], 'median', or 'maxInt'; Method for
%   background subtraction
% tryCorrect' - Boolean; If true, tries to correct the erroneous fish
%   positions based on the assumption that these can jump around from one
%   frame to the next.
% trlNums - If specified, then only processes these trials and appends the
%   updated info the most recent procData.mat file in the relevant
%   directory. By default, trlNums = [], in which case starts from scratch.
% headDiam = Approx head diameter in mm (Default = 1). Determines smoothing
%   when finding fish pixels prior to midline tracking
% Outputs:
% procData - Mat file saving all relevant info after processing.
%
% Avinash Pujala, Koyama lab/HHMI, 2016

readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [];
fps = 500;
nFramesInTrl = 750;
nBlocks = 4;
cropWid = 90; %(For imgDims ~ [900,900])
backMethod = 'mean';
tryCorrect = true;
headDiam = 1; % In mm
trlNums = [];
registerBool = 0;
pxlLen = [];

for jj  = 1:nargin
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'readmode'
                readMode = varargin{jj+1};
            case 'imgext'
                imgExt = varargin{jj+1};
            case 'imginds'
                imgInds = varargin{jj+1};
            case 'fps'
                fps = varargin{jj+1};
            case 'nframesintrl'
                nFramesInTrl = varargin{jj+1};
            case 'nblocks'
                nBlocks = varargin{jj+1};
            case 'cropwid'
                cropWid = varargin{jj+1};
            case 'backmethod'
                backMethod = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'trlnums'
                trlNums = varargin{jj+1};
            case 'registerbool'
                registerBool = varargin{jj+1};
            case 'pxllen'
                pxlLen = varargin{jj+1};
        end
    end
end

pathList = varargin{1};
if ~iscell(pathList)
    pathList = {pathList}; % Encapsulate single path string in a cell.
    
end
procData = cell(length(pathList),1);

tic
for pp = 1:length(pathList)
    [currPath,currFile] = fileparts(pathList{pp});
    if ~strcmpi(currFile,'proc')
        currPath = fullfile(currPath,currFile);
    end
    fprintf('Processing ... \n')
    disp(currPath)
    procData{pp} = ProcessFishImages_subset(currPath,'readMode',readMode,'imgInds',imgInds,'fps',fps,...
        'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,'nblocks',nBlocks,'cropWid',cropWid,'backMethod',backMethod,...
        'tryCorrect',tryCorrect,'headDiam',headDiam,'trlNums',trlNums,...
        'registerBool',registerBool,'pxlLen',pxlLen);
    toc
end
toc

varargout{1}= procData;

end


function varargout = ProcessFishImages_subset(varargin)
% ProcessFishImages();
% procData = ProcessFishImages();
% procData = ProcessFishImages(imgDir);
% procData = ProcessFishImages(imgDir,'readMode',readMode,'fps',fps,'imgExt',imgExt,'nFramesInTrl',nFramesInTrl,...
%   'imgInds',imgInds,'blockSize',blockSize,'cropWid',cropWid);
% Inputs:
% If no inputs are specified, allows for interactive selection of image
%   directory and use default parameters for processing.
% imgDir - Path to image directory. If empty, allows for interactive
%   selection of path by selecting any file in the directory.
% readMode - 'fromImages' or 'fromMishVid'. The former results in reading
%   of an image sequence, whereas the latter reads .mishVid created by
%   Misha Ahrens.
% imgExt - Image extension; reads only images in specified folder with this
%   extension ['bmp']
% fps - Frames per second [500].
% nFramesInTrl = Number of frames in a single trial [750];
% spatialFilt - Spatial filter used to smooth images before finding fish's
%   head centroid
% imgInds - Vector of image indices to read. If empty, reads all images.
%   By default reads all images
% nHeadPxls - Number of head pixels; This is the number of pixels to use
%   for finding head centroid of fish [25]
% nBlocks - Loading all images can take up too much memory, so this
%   allows processing by splitting total number of images in this many
%   blocks and processing one block at a time. If empty, or by default
%   blockSize = 4.
% cropWid - Crop width. After finding fish, crops image by this width after
%   aroung the fish to save space
% backMethod - 'mean'[default], 'median', or 'maxInt'; Method for
%   background subtraction
% 'tryCorrect' - Boolean; If true, tries to correct the erroneous fish
%   positions based on the assumption that these can jump around from one
%    frame to the next.
% trlNums - If specified, then only processes these trials and appends the
%   updated info the most recent procData.mat file in the relevant
%   directory. By default, trlNums = [], in which case starts from scratch.
% Outputs:
% procData - Mat file saving all relevant info after processing
%
% Avinash Pujala, Koyama lab/HHMI, 2016

imgDir = [];
readMode =  'fromImages';
imgExt = 'bmp';
imgInds = [];
fps = 500;
nFramesInTrl = 750;
nBlocks = 4; % This now will be number of blocks per trial
cropWid = 90; %( For imgDims ~ [900,900])
poolSize  = 10;
backMethod = 'mean';
tryCorrect = true;
len_midlines = 50;
headDiam = 1; % In mm
trlNums =[];
arenaDiam = 50; % In mm
registerBool = 0;
pxlLen = [];

for jj  = 2:nargin
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'readmode'
                readMode =  varargin{jj+1};
            case 'imgext'
                imgExt =  varargin{jj+1};
            case 'imginds'
                imgInds =  varargin{jj+1};
            case 'len_midlines'
                len_midlines = varargin{jj+1};
            case 'fps'
                fps = varargin{jj+1};
            case 'nframesintrl'
                nFramesInTrl =  varargin{jj+1};
            case 'nblocks'
                nBlocks =  varargin{jj+1};
            case 'cropwid'
                cropWid =  varargin{jj+1};
            case 'backmethod'
                backMethod = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'trlnums'
                trlNums = varargin{jj+1};
            case 'registerbool'
                registerBool = varargin{jj+1};
            case 'pxllen'
                pxlLen = varargin{jj+1};
        end
    end
end

imgExt(strfind(imgExt,'.'))=[];
if nargin ==0
    [~, imgDir] = uigetfile(['*.' imgExt]);
else
    imgDir = varargin{1};
    if isempty(imgDir)
        [~,imgDir] = uigetfile(['*.' imgExt]);
    end
end

% switch readMode
%     case 'fromMishVid'
%         [I, outDir] = ReadMishVid();
%         imgInds = 1:size(I,3);
%     case 'fromImages'
%         I = ReadImgSequence(imgDir,imgExt,imgInds);
% end

imgNamesInDir = GetFilenames(imgDir,'ext',imgExt);
totalNumImgsInDir = length(imgNamesInDir);

%---This is to speed up background subtraction and also to improve
%---background image when the not many images can be loaded because of
%---memory constraints
if totalNumImgsInDir >1000
    rp = randperm(totalNumImgsInDir);
    refFrames = rp(1:1000);
end

outDir = fullfile(imgDir,'proc');
if ~exist(outDir,'dir')
    mkdir(outDir)
end


%% Background removal and fish position tracking in blocks of images
tic
poolObj = gcp('nocreate');
if isempty(poolObj)
    poolObj = parpool(poolSize);
end

nTrls = totalNumImgsInDir/nFramesInTrl;
disp([num2str(nTrls) ' trls detected'])
imgInds_all = 1:totalNumImgsInDir;


disp('Getting image info..')
imgInfo = imfinfo(fullfile(imgDir,imgNamesInDir{1}));
imgDims = [imgInfo.Height imgInfo.Width];
imgDims = [imgDims, totalNumImgsInDir];
ts = datestr(now,30);
fileInfo = dir([outDir '/*.mat']);
createBool = true;
dn = zeros(length(fileInfo),1);
if ~isempty(dn)    
    for jj = 1:length(fileInfo)
        dn(jj) = fileInfo(jj).datenum;
    end
    [dn,sortInds] = sort(dn,'descend');
    dn = dn(1);
    dn_now = now;
    dn_diff = (dn_now-dn)*(1e5)/(3600);    
    if dn_diff < 1 % 1 hr
        createBool =false;
    end
end
%---Compute background
disp(['Reading random sequence of ' num2str(numel(refFrames)) ' images, and computing background...'])
refFrames = [1; refFrames(:); totalNumImgsInDir];
refFrames = unique(refFrames);
I = ReadImgSequence(imgDir,[],[],imgNamesInDir(refFrames));
[~,ref_all] = SubtractBackground(I,'method',backMethod);

if registerBool
    [dx,dy] = deal(zeros(size(I,3),1));
    I_reg = I*0;
    I_reg(:,:,end) = I(:,:,end);
    
    %--- Register a subset of images and interpolate afterwards
    regRef = I(:,:,end);
    loopInds = 1:size(I,3);
    parfor jj = loopInds
        [dx(jj),dy(jj),I_reg(:,:,jj)] = RegisterImageStacks(regRef,I(:,:,jj));
    end
    inds_all = 1:totalNumImgsInDir;
    xx = round(interp1(refFrames,dx,inds_all));
    yy = round(interp1(refFrames,dy,inds_all));
    ref_all = mean(I_reg,3);    
end

if isempty(pxlLen)
    pxlLen = GetPxlSpacing(ref_all,'diam',arenaDiam);
end
kerSize = ceil(headDiam/pxlLen);
clear I % To free memory
if isempty(trlNums)
    trlNums  = 1:nTrls;
    imgInds_trl = SubListsFromList(imgInds_all,nFramesInTrl);
    if createBool
        procName = ['procData_' ts '.mat'];
        disp('Creating a new file at ')
        disp(fullfile(outDir,procName))
        procData = matfile(fullfile(outDir,procName),'Writable',true);
    else
        disp('Reopening previous file...')
        fName = fileInfo(sortInds(1)).name;
        procData = OpenMatFile(fullfile(outDir,fName));       
        procData.Properties.Writable = true;
    end
    
    procData.nFramesInTrl = nFramesInTrl;
    procData.fps = fps;
    procData.I_proc_crop = zeros(2*cropWid + 1, 2*cropWid + 1, totalNumImgsInDir);
    procData.tailCurv = zeros(len_midlines,2,totalNumImgsInDir);
    procData.curv = zeros(len_midlines,totalNumImgsInDir);
    procData.fishPos = zeros(totalNumImgsInDir,2);
    procData.ref = ref_all;
    procData.pxlLen = pxlLen;
    mlInds = cell(imgDims(3),1);
    dsVecs = cell(imgDims(3),1);
    for trl = trlNums(:)'
        blockLen = round(length(imgInds_trl{trl})/nBlocks);
        imgInds_block = SubListsFromList(imgInds_trl{trl},blockLen);
        for block = 1:length(imgInds_block)
            imgInds_now = imgInds_block{block};
            disp(['Trl # ' num2str(trl) ', block # ' num2str(block)])
            disp('Reading images...')
            I = ReadImgSequence(imgDir,imgExt,[],imgNamesInDir(imgInds_now));
            %--- Circularly shift
            if registerBool
                disp('Translating images...')
                loopInds = 1:length(imgInds_now);
                for kk = loopInds
                    I(:,:,kk) = circshift(I(:,:,kk),...
                        -[xx(imgInds_now(kk)),yy(imgInds_now(kk))]);
                end
            end
            disp('Subtracting background...')
            [I_back,~] = SubtractBackground(I,'method',backMethod,'backImg',ref_all);
            clear I           
            fp_now = GetFishPos_180307(I_back,'kerSize',kerSize,'process','parallel',...
                'tryCorrect', tryCorrect);           
            disp('Cropping images...')
            I_crop = CropImgsAroundPxl(I_back,fp_now,cropWid,'procType','parallel'); % Parallel, not working, need to fix bugs
            disp('Tracking midlines...')
            try
                I_smooth = FindFishPxlsForMidlineTracking_20180713(I_crop,'headDiam',headDiam,'pxlLen',pxlLen);
            catch
                I_smooth = FindFishPxlsForMidlineTracking_20180325(I_crop,'headDiam',headDiam,'pxlLen',pxlLen);
            end
            [midlines,mlInds_trl,dsVecs_trl] =  GetMidlinesByThinning_new(I_smooth,'process','parallel','plotBool',0);
            tA = GetTailTangents(midlines);
            clear I_smooth
            disp('Updating procData.mat ...')
            procData.fishPos(imgInds_now,:) = fp_now;
            procData.I_proc_crop(:,:,imgInds_now) = I_crop;
            procData.tailCurv(:,:,imgInds_now) = midlines;
            procData.curv(:,imgInds_now) = tA;
            mlInds(imgInds_now) = mlInds_trl; % These cannot be directly updated in procData!!
            dsVecs(imgInds_now) = dsVecs_trl;
        end
    end
    disp('Appending addition variables to procData...')
    procData.imgDims_crop = size(I_crop(:,:,1));
    procData.imgDims = imgDims;
    procData.mlInds = mlInds;
    procData.dsVecs = dsVecs;
    clear im_proc fp hOr_temp
else
    %%%% Check to see if there are any procData.mat files in outDir
    procFiles = GetFilenames(outDir, 'searchStr','procData','ext','mat');
    if ~isempty(procFiles)
        disp(['Found procData file! Will read and write to ' procFiles{end}])
        %%%% Since each procFile name has a time-stamp in it, the last one will
        %%%% be the most recent one, so read and append to this one.
        try
            procData = OpenMatFile(fullfile(outDir,procFiles{end}));
            procData.Properties.Writable = true;
        catch
            error('Cannot read procData.mat file! Check for errors.')
        end
    else
        procName = ['procData_' ts '.mat'];
        %%%% Presently I will not allow for the option to compute from
        %%%% scratch, if no procData.mat file is found.
        error('procData.mat file not found, try running from the start!')
    end
    trlNumToImgInds = @(trlNum,nFramesInTrl) (trlNum-1)*nFramesInTrl+1:(trlNum-1)*nFramesInTrl + nFramesInTrl;
    pxlLen = procData.pxlLen;
    nFramesInTrl = procData.nFramesInTrl;
    mlInds = procData.mlInds;
    dsVecs = procData.dsVecs;
    for trl = trlNums(:)'
        disp(['Trl num # ' num2str(trl)])
        imgInds = trlNumToImgInds(trl,nFramesInTrl);
        disp('Reading processed images...')
        im_proc = procData.I_proc_crop(:,:,imgInds);
        I_smooth = FindFishPxlsForMidlineTracking(im_proc,'headDiam',headDiam,'pxlLen',pxlLen);
        [midlines,mlInds_trl,dsVecs_trl] =  GetMidlinesByThinning_new(I_smooth,'process','parallel','plotBool',0);
        tA = GetTailTangents(midlines);
        disp('Updating procData.mat ...')
        procData.tailCurv(:,:,imgInds) = midlines;
        procData.curv(:,imgInds) = tA(1:size(procData.curv,1),:); % The size has to be specified because a change can result in mismatch if only reprocessing a few trials instead of computing from start
        mlInds(imgInds) = mlInds_trl; % These cannot be directly updated in procData!!
        dsVecs(imgInds) = dsVecs_trl;
    end
    procData.mlInds = mlInds;
    procData.dsVecs = dsVecs;
end
toc


%% Plot trialized tail bends
close all
try
    TrializeTailBendsAndPlot
catch
    disp('Skipped plotting because of an encoutered error!');
end
disp('Saving trialized tail curvature figures figures...')
suffix = ['_Trialized tail curvatures_' ts];
h = get(0,'children');
for fig = 1:length(h)
    prefix = ['Fig_' sprintf('%0.2d', fig)];
    saveas(h(fig), fullfile(outDir,[prefix, suffix]))
end


varargout{1} = procData;

end
