function varargout = SlowSwim(pathList,varargin)
%SlowSwim Given a list of paths to the image directories where images were
%   acquired at slow frame rate, returns a list of matfiles that contain
%   information about swims.
% SlowSwim(pathList)
% slowSwim = SlowSwim(pathList,'imgExt',imgExt,'headArea',headArea,
%   'spatialFilt', spatialFilt,'frameRate',frameRate,'trlLen',trlLen
%   'thr_dist',thr_dist);

imgExt = 'bmp';
headDiam = 1.2; % Approx head diam in mm (I usually use 1-1.25mm)
spatialFilt = 30;
frameRate = 30; % Hz
trlLen = 60; % In seconds
thr_dist = 100; % Max-speed fish can travel in mm/sec (It is better to set this value a bit lower than expected)
tryCorrect = true; % COrrection for fish position
nTrlBlocks = 2; % Processes these many trial blocks for better background subtraction

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'imgext'
                imgExt = varargin{jj+1};
            case 'headDiam'
                headDiam = varargin{jj+1};
            case 'spatialfilt'
                spatialFilt = varargin{jj+1};
            case 'framerate'
                frameRate = varargin{jj+1};
            case 'trllen'
                trlLen = varargin{jj+1};
            case 'thr_dist'
                thr_dist = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'ntrlblocks'
                nTrlBlocks = varargin{jj+1};
        end
    end
end

if ~iscell(pathList)
    slowSwim = SlowSwim_single(pathList,imgExt,headDiam,spatialFilt,...
        frameRate,trlLen,thr_dist,tryCorrect,nTrlBlocks);
else
    nPaths = length(pathList);
    slowSwim = cell(nPaths,1);
    for jj = 1:nPaths        
        disp(['Fish/Path # ' num2str(jj)])
        slowSwim{jj} = SlowSwim_single(pathList{jj},imgExt,headDiam,spatialFilt,frameRate,...
            trlLen,thr_dist,tryCorrect,nTrlBlocks);
    end
end

varargout{1} = slowSwim;

end

function slowSwim = SlowSwim_single(imgDir,imgExt,headDiam,spatialFilt,...
    frameRate,trlLen,thr_dist,tryCorrect,nTrlBlocks)

%--- Read all file names
disp('Reading file names...')
fileNames = GetFilenames(imgDir,'ext',imgExt);
dt = 1/frameRate;
nImagesInTrl = frameRate * trlLen;
nFilesInDir = length(fileNames);
nTrls = floor(nFilesInDir/nImagesInTrl);
disp([num2str(nTrls) ' trls detected!'])

%---Determine a threshold for extracting fish head pxls to reduce possible
%   detection errors in fish pos tracking
disp('Determining a threshold for fish head position detection...')
nImages_thr = min(750,nFilesInDir);
imgInds_thr = randperm(nFilesInDir);
imgInds_thr = imgInds_thr(1:nImages_thr);
fileNames_thr = fileNames(imgInds_thr);
I_thr = ReadImgSequence(imgDir,imgExt,[],fileNames_thr);
[I_thr_sub, img_bg] = SubtractBackground(I_thr);
I_thr_sub(I_thr_sub<=0)=[];
try
    thr = multithresh(I_thr_sub);
catch
    thr = GetMultiThr(I_thr_sub,1);
end
thr = 0.5*thr; % Better to have false +ves
clear I_thr I_thr_sub


%Save
disp('Creating slowSwim.mat file to write fish data...')
saveDir = fullfile(imgDir,'proc');
if exist(saveDir)~=7
    mkdir(saveDir)
end
ts = datestr(now,30);
slowSwim = matfile(fullfile(saveDir,['slowSwim_' ts '.mat']), 'Writable',true);
slowSwim.frameRate = frameRate;
slowSwim.trlLen = trlLen;
slowSwim.nTrls = nTrls;

% Getting fish position for all time points
slowSwim.fishPos = nan(nImagesInTrl,2,nTrls);
fileNames_block = SubListsFromList(fileNames,nTrlBlocks*nImagesInTrl);
imgInds = 1:length(fileNames);
imgInds_blocks = SubListsFromList(imgInds,nTrlBlocks*nImagesInTrl);
inds_trl = SubListsFromList(1:nTrlBlocks*nImagesInTrl,nImagesInTrl);
count = 0;
for block = 1:length(imgInds_blocks)
    I = ReadImgSequence(imgDir,imgExt,imgInds_blocks{block},fileNames_block{block});
%     [I_backSub, img_bg] = SubtractBackground(I,'method','mean');
    I_backSub = SubtractBackground(I,'backImg',img_bg);
    if block ==1
        ipd = GetPxlSpacing(img_bg,'diam',50);
        thr_dist = ceil((thr_dist*dt)/ipd);      
        spatialFilt = ceil(headDiam/ipd);
        disp(['Inter pxl distance = ' num2str(ipd)])
        slowSwim.img_bg = img_bg;
        slowSwim.interPxlDist = ipd;
        close
    end
    imgInds_trl = SubListsFromList(imgInds_blocks{block},nImagesInTrl);
    for trl = 1:nTrlBlocks
        count = count + 1;
        disp(['Trl # ' num2str(count)])
        foo = GetFishPos_180307(I_backSub(:,:,inds_trl{trl}),'kerSize',spatialFilt,'process','parallel',...
            'tryCorrect', tryCorrect,'thr',thr);
        disp('Updating slowSwim.mat file...')
        slowSwim.fishPos(:,:,count) = foo;
    end
end


end

