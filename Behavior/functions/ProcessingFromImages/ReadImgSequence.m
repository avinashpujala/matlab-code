function I = ReadImgSequence(imgDir,varargin)
%ReadImgSequence Reads a sequences of images with a specified extension in
%   a specfied dir
% I = ReadImgSequence(imgDir)
% I = ReadImgSequence(imgDir,imgExt)
% I = ReadImgSequence(imgDir,imgExt,imgNums,fNames)
% Inputs:
% imgDir - Directory where images are stored
% imgExt - Extension of the img, e.g., {'bmp'}['jpg']['tif'] ['png']
% imgNums - Vector specifying the subset of images to read from all the
%   images in the directory. For e.g., imgNuums = [1 3 5], will only cause
%   the images 1, 3, and 5 to be read from the image sequence
% fNames - 1D cell array containing names of image files to read. If fNames
%   is given as input to the program then this overrides imgInds and imgExt
%   inputs because the fNames must contain the image ext, e.g.,
%   "img_00001.bmp".
% imgInds - A vector of indices indicating which images in the entire
%   sequence to read

imgInds = [];
imgExt = 'bmp';
fNames = [];
poolSize = 10;

if nargin == 2
    imgExt = varargin{1};
elseif nargin ==3
    imgExt = varargin{1};
    imgInds = varargin{2};
elseif nargin == 4
    imgExt = varargin{1};
    imgInds = varargin{2};
    fNames = varargin{3};
end

tic
if isempty(fNames)
    disp('Scanning all image files in the dir...')   
    searchToken = ['*.' imgExt];
    files = dir(fullfile(imgDir,searchToken));
    fNames = {files.name};
    if isempty(fNames)
        error('No files found in directory, please check path!')
    end    
    if ~isempty(imgInds)
        fNames = fNames(imgInds);
    end
else
    [~,~,imgExt] = fileparts(fNames{1});
end

imgInfo = imfinfo(fullfile(imgDir,fNames{1}));
imSize = [imgInfo.Height imgInfo.Width];
I = zeros(imSize(1),imSize(2),length(fNames));
% I = MappedTensor(imSize(1),imSize(2),length(fNames));
disp(['Reading ' imgExt ' images from dir...'])
imgNums = 1:length(fNames);
try
    p = gcp('nocreate');
    if isempty(p)
        p = parpool(poolSize);
    end
    parfor jj = imgNums
        img = imread(fullfile(imgDir,fNames{jj}));
        if length(size(img))==3
            if jj ==1
                disp('Great! Images are rgb and are being converted to gray...')
            end
            img = rgb2Gray(img);
        end
        I(:,:,jj) = img;
    end
catch
    disp('Parallel failed,reading serially... ')
    for jj = imgNums
    try
        img = imread(fullfile(imgDir,fNames{jj}));
    catch
        img_pre = imread(fullfile(imgDir,fNames{jj-1}));
        img_post = imread(fullfile(imgDir,fNames{jj+1}));
        img = (img_pre + img_post)/2;
        SaveImages(img,imgDir,fNames{jj});
        img = imread(fullfile(imgDir,fNames{jj}));
    end
        if length(size(img))==3
            if jj ==1
                disp('Great! Images are rgb and are being converted to gray...')
            end
            img = rgb2Gray(img);
        end
        I(:,:,jj) = img;
    end
end

toc
end

function img_gray = rgb2Gray(img_rgb)
img_rgb(:,:,1) = img_rgb(:,:,1)*0.299;
img_rgb(:,:,2) = img_rgb(:,:,2)*0.587;
img_rgb(:,:,3) = img_rgb(:,:,3)*0.114;
img_gray = sum(img_rgb,3);
end
