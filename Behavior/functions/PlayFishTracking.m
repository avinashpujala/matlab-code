function PlayFishTracking(IM,varargin)
% PlayFishTracking - Plays the video of the fish along with orientation
%   line
% PlayFishTracking(IM, 'fishPos',fishpos,'tailCurv',tailCurv,'startFrame',startFrame,...
%           'endFrame',endFrame,'pauseDur',pauseDur,'tailAngles',tailAngles, 'periPxls',periPxls)
%
% Inputs:
% IM - Image stack of size M x N x T, where M = image height, N = image
%   width, T = # of time points
% 'fishPos' - T x 2 vec where 1st & 2nd cols contain row & col positions
%   of the fish head centroid in the images
% nTracePts - Number of trace points of the trjectory to display.
Default = []; 
% 'midlineInds' - A cell array of length T, where each array contains indices
%   of the fish midline
% 'frameInds' - Indices of frames to display
% 'pauseDur' - Duration of pause between subsequent frames
% 'plotCurv' - 0 or 1; 1 results in plotting of curvatures in a subplot
%   below the fish images
% 'saveDir' - Directory to save images to
% 'lineClr' - Color of plotted midline
% 'writeVideo' - Boolean for writing to video or not. 1 results in writing,
%   0 does not

plotCurv = 0;
frameInds  = [];
saveDir = [];
pauseDur = 0;
tailAngles = [];
lineClr = 'r';
writeVideo = 0;
tailCurv = [];
nTracePts =[];
fishPos = [];

for jj = 1:numel(varargin)
    if isstr(varargin{jj})
        switch lower(varargin{jj})
            case 'frameinds'
                frameInds = varargin{jj+1};
            case 'fishpos'
                fishPos = varargin{jj+1};
                if numel(fishPos)==2
                    fishPos = fishPos(:)';
                    fishPos = repmat(fishPos,size(IM,3),1);
                end
            case 'pausedur'
                pauseDur = varargin{jj+1};
            case 'tailcurv'
                tailCurv = varargin{jj+1};
            case 'savedir'
                saveDir = varargin{jj+1};
            case 'plotcurv'
                plotCurv = varargin{jj+1};
            case 'tailangles'
                tailAngles = varargin{jj+1};
            case 'lineclr'
                lineClr = varargin{jj+1};
            case 'writevideo'
                writeVideo = varargin{jj+1};
        end
    end
end

% Determining whether there's a single or many fish to track
if iscell(fishPos)
    PlayMultiFishTracking(IM,varargin)
else
    PlaySingleFishTracking(IM,varargin)    
end

end
function PlaySingleFishTracking(IM,varargin)
% PlaySingleFishTracking - Plays the video of the fish along with orientation
%   line
% PlaySingleFishTracking(IM, 'fishPos',fishpos,'tailCurv',tailCurv,'startFrame',startFrame,...
%           'endFrame',endFrame,'pauseDur',pauseDur,'tailAngles',tailAngles, 'periPxls',periPxls)
%
% Inputs:
% IM - Image stack of size M x N x T, where M = image height, N = image
%   width, T = # of time points
% 'fishPos' - T x 2 vec where 1st & 2nd cols contain row & col positions
%   of the fish head centroid in the images
% 'nTracePts' - If not empyty(default), then plots these many fish
%   trjectory points, preceding the current fish position
% 'midlineInds' - A cell array of length T, where each array contains indices
%   of the fish midline
% 'frameInds' - Indices of frames to display
% 'pauseDur' - Duration of pause between subsequent frames
% 'plotCurv' - 0 or 1; 1 results in plotting of curvatures in a subplot
%   below the fish images
% 'saveDir' - Directory to save images to
% 'lineClr' - Color of plotted midline
% 'writeVideo' - Boolean for writing to video or not. 1 results in writing,
%   0 does not
% 'lineWid_midline' - Width of midline overlaid on fish

plotCurv = 0;
frameInds  = [];
saveDir = [];
pauseDur = 0;
tailAngles = [];
lineClr = 'r';
writeVideo = 0;
tailCurv = [];
nTracePts = [];
fishPos = [];
lineWid_midline =1;

varargin = varargin{1};
for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'frameinds'
                frameInds = varargin{jj+1};
            case 'fishpos'
                fishPos = varargin{jj+1};
                if numel(fishPos)==2
                    fishPos = fishPos(:)';
                    fishPos = repmat(fishPos,size(IM,3),1);
                end
            case 'pausedur'
                pauseDur = varargin{jj+1};
            case 'tailcurv'
                tailCurv = varargin{jj+1};
            case 'savedir'
                saveDir = varargin{jj+1};
            case 'plotcurv'
                plotCurv = varargin{jj+1};
            case 'tailangles'
                tailAngles = varargin{jj+1};
            case 'lineclr'
                lineClr = varargin{jj+1};
            case 'writevideo'
                writeVideo = varargin{jj+1};
            case 'ntracepts'
                nTracePts = varargin{jj+1};
            case 'linewid_midline'
                lineWid_midline = varargin{jj+1};
        end
    end
end

if isempty(frameInds)
    frameInds = 1:size(IM,3);
end


if (isempty(tailAngles) && ~isempty(tailCurv))
    tailAngles = GetTailTangents(tailCurv);
    tailCurv = tailCurv(:,:,frameInds);
end

if ~isempty(tailAngles)
    tailAngles = tailAngles(:,frameInds);
end

IM = IM(:,:,frameInds);
if isempty(fishPos)
    fishPos = nan(numel(frameInds),2);
else
    fishPos = fishPos(frameInds,:);
end


imgDims =[size(IM,1), size(IM,2)];
cLim = [min(IM(:)), max(IM(:))*0.9];

if writeVideo
    vidObj = VideoWriter('FishTrackingVideo.avi');
    open(vidObj);
    disp('Writing to video...')
end
fh = figure('Name', 'Fish Tracking');
count = 0;
blah = zeros(size(tailAngles,1),100);
for imgNum = frameInds(:)'
    count = count + 1;
    if ~isempty(tailCurv)
        x = tailCurv(:,1,count);
        y = tailCurv(:,2,count);
    end
    if plotCurv
        figure(fh)
        ax1 = subplot(2,1,1);
        cla
        cMap = jet(64*4);
        imagesc(IM(:,:,count),'parent',ax1); axis image, axis on, colormap(ax1,jet),axis off
        hold on
        set(gca,'clim',cLim)
        plot(x,y,'color',lineClr,'linewidth',lineWid_midline)
        plot(x(1),y(1),'bo','markersize',10)
        plot(fishPos(count,1),fishPos(count,2),'g*','markersize',10)
        drawnow
        title(['Abs Frame: ' num2str(imgNum) ', Rel frame # ' num2str(count)...
            ', Angle: ' num2str(round(tailAngles(end,count)*10)/10) '^o '])
        if count >= 100
            blah = tailAngles(:,count-99:count);
        else
            blah = zeros(size(tailAngles,1),100);
            blah(:,end-count+1:end) = tailAngles(:,1:count);
        end
        figure(fh)
        ax2 = subplot(2,1,2);
        cla
        imagesc(1:size(blah,2),1:size(blah,1),blah,'parent',ax2); colormap(ax2,cMap)
        set(gca,'clim',[-150 150],'ytick',[5 size(blah,1)-5],'yticklabel',{'Head','Tail'},'xtick',[]);
        %         box off
        colorbar
       
        if isempty(pauseDur)
            pause()
        else
            pause(pauseDur)
        end
        
    else
        figure(fh)
        cla
        imagesc(IM(:,:,imgNum)),axis image, axis on, colormap(gray)
        hold on
        set(gca,'clim',cLim)
        if ~isempty(fishPos)
            if ~isempty(nTracePts)
                traceInds = (imgNum-nTracePts):imgNum;
                traceInds(traceInds<1)=[];
                plot(fishPos(imgNum,1),fishPos(imgNum,2),'ro')
                plot(fishPos(traceInds,1),fishPos(traceInds,2),'.')
            else
                 plot(fishPos(imgNum,1),fishPos(imgNum,2),'ro')
            end
           
        end
        if ~isempty(tailCurv)
            plot(x,y,'r','linewidth',lineWid_midline)
        end
        
        axis off
        if ~isempty(saveDir)
            suffix = sprintf('%0.5d',imgNum);
            fName = ['Img_' suffix];
            print(fullfile(saveDir,fName),'-dpng')
        end
        drawnow
        %         title(['Frame: ' num2str(imgNum) ', Angle: ' num2str(round(orientation(imgNum))) '^o ' ...
        %             ', Frame rate = ' num2str(round(1/eTime)) ' fps'])
        title(['Frame # ' num2str(imgNum)])
        shg
        if isempty(pauseDur)
            pause()
        else
            pause(pauseDur)
        end
    end
    drawnow
    if writeVideo
        currFrame = getframe(fh);
        vidObj.writeVideo(currFrame);
    end
end
if writeVideo
    close(vidObj)
end
end

function PlayMultiFishTracking(IM,varargin)
% PlayMultiFishTracking - Plays the video of the fish along with orientation
%   line
% PlayMultiFishTracking(IM, 'fishPos',fishpos,'tailCurv',tailCurv,'startFrame',startFrame,...
%           'endFrame',endFrame,'pauseDur',pauseDur,'tailAngles',tailAngles, 'periPxls',periPxls)
%
% Inputs:
% IM - Image stack of size M x N x T, where M = image height, N = image
%   width, T = # of time points
% 'fishPos' - T x 2 vec where 1st & 2nd cols contain row & col positions
%   of the fish head centroid in the images
% 'midlineInds' - A cell array of length T, where each array contains indices
%   of the fish midline
% 'frameInds' - Indices of frames to display
% 'pauseDur' - Duration of pause between subsequent frames
% 'plotCurv' - 0 or 1; 1 results in plotting of curvatures in a subplot
%   below the fish images
% 'saveDir' - Directory to save images to
% 'lineClr' - Color of plotted midline
% 'writeVideo' - Boolean for writing to video or not. 1 results in writing,
%   0 does not

plotCurv = 0;
frameInds  = [];
saveDir = [];
pauseDur = 0;
tailAngles = [];
lineClr = 'r';
writeVideo = 0;
tailCurv = [];

varargin = varargin{1};
for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'frameinds'
                frameInds = varargin{jj+1};
            case 'fishpos'
                fishPos = varargin{jj+1};              
            case 'pausedur'
                pauseDur = varargin{jj+1};
            case 'tailcurv'
                tailCurv = varargin{jj+1};
            case 'savedir'
                saveDir = varargin{jj+1};
            case 'plotcurv'
                plotCurv = varargin{jj+1};
            case 'tailangles'
                tailAngles = varargin{jj+1};
            case 'lineclr'
                lineClr = varargin{jj+1};
            case 'writevideo'
                writeVideo = varargin{jj+1};
        end
    end
end

if isempty(fishPos)
    error('Fish position input cannot be empty!')
end

if isempty(frameInds)
    frameInds = 1:size(IM,3);
end
IM = IM(:,:,frameInds);

% if (isempty(tailAngles) && ~isempty(tailCurv))
%     tailAngles = GetTailTangents(tailCurv);
%     tailCurv = tailCurv(:,:,frameInds);
% elseif ~isempty(tailAngles)
%       tailAngles = tailAngles(:,frameInds);    
% end
% 
% fishPos = fishPos(frameInds,:);

% imgDims = size(IM);
% imgDims = imgDims(1:2);

cLim = [min(IM(:)), max(IM(:))*0.9];

if writeVideo
    vidObj = VideoWriter('FishTrackingVideo.avi');
    open(vidObj);
    disp('Writing to video...')
end

fh = figure('Name', 'Fish Tracking');
count = 0;
% blah = zeros(size(tailAngles,1),100);
for imgNum = frameInds(:)'
    count = count + 1;
    cla
    imagesc(IM(:,:,count)); axis image, axis on, colormap(gray),axis off
    hold on
    set(gca,'clim',cLim) 
    nFish = length(fishPos);
    for ff = 1:nFish        
%         if ~isempty(tailCurv)
%             x = tailCurv(:,1,count);
%             y = tailCurv(:,2,count);
%         end
        clrInds = ceil(linspace(1,64,nFish+1));
        clrMap = jet(64);
        clrs = clrMap(clrInds,:);
        if plotCurv
            figure(fh)
            ax1 = subplot(2,1,1);
            cla
            cMap = jet(64*4);
            imagesc(IM(:,:,count),'parent',ax1); axis image, axis on, colormap(ax1,jet),axis off
            hold on
            set(gca,'clim',cLim)
            plot(x,y,'color',lineClr,'linewidth',2)
            plot(x(1),y(1),'bo','markersize',10)
            plot(fishPos(count,1),fishPos(count,2),'g*','markersize',10)
            drawnow
            title(['Abs Frame: ' num2str(imgNum) ', Rel frame # ' num2str(count)...
                ', Angle: ' num2str(round(tailAngles(end,count)*10)/10) '^o '])
            if count >= 100
                blah = tailAngles(:,count-99:count);
            else
                blah = zeros(size(tailAngles,1),100);
                blah(:,end-count+1:end) = tailAngles(:,1:count);
            end
            figure(fh)
            ax2 = subplot(2,1,2);
            cla
            imagesc(1:size(blah,2),1:size(blah,1),blah,'parent',ax2); colormap(ax2,cMap)
            set(gca,'clim',[-150 150],'ytick',[5 size(blah,1)-5],'yticklabel',{'Head','Tail'},'xtick',[]);      
            colorbar
            drawnow
            if writeVideo
                currFrame = getframe(fh);
                vidObj.writeVideo(currFrame);
            end
            if isempty(pauseDur)
                pause()
            else
                pause(pauseDur)
            end            
        else
            plot(fishPos{ff}(imgNum,1),fishPos{ff}(imgNum,2),'o','color',clrs(ff,:))            
            if ~isempty(tailCurv)
                plot(tailCurv{ff}(:,1,count),tailCurv{ff}(:,2,count),'color',clrs(ff,:))
            end            
            axis off
            if ~isempty(saveDir)
                suffix = sprintf('%0.5d',imgNum);
                fName = ['Img_' suffix];
                print(fullfile(saveDir,fName),'-dpng')
            end              
        end
    end
    drawnow
    title(['Frame # ' num2str(imgNum)])
    shg
    if isempty(pauseDur)
        pause()
    else
        pause(pauseDur)
    end
end
if writeVideo
    close(vidObj)
end
end

