function varargout = DeltaDistance(xyCoord)
%deltaDistance - When given x and y coordinates, computes distance traveled
%   from previous point
% dS = deltaDistance(xyCoordinates);
% [dS_diff, dS_gradient] = DeltaDistance(xyCoordinates);
% Inputs:
%   xyCoordinates - n-by-2 matrix where n is number of coordinate points, and the 2 columns
%        are x and y coordinates respectively
% Outputs:
% dS_diff = Uses 'diff.m' to compute distances
% dS_gradient = Uses 'gradient.m' to compute distances
% 
% Avinash Pujala, Koyama lab/JRC, 2018

dS = sqrt(sum(diff(xyCoord,[],1).^2,2));


[~,dS2] = gradient(xyCoord);
dS2 = sqrt(sum(dS2.^2,2));

varargout{1} = dS;
varargout{2} = dS2;

end

