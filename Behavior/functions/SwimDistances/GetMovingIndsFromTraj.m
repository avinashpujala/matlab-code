function varargout = GetMovingIndsFromTraj(traj)
%GetMovingIndsFromTraj Given a trajectory in 2D (such as fish) returns indices where the
%   trajectory is thought to be real motion. Uses a Gaussian mixture model
%   to distinguish real motion from noise (low jitter or large outliers).
%   From trajectory, first extracts distance between successive frames,
%   then uses peaks in the distance vector to model with 3 Gaussian
%   distributions thought to represent background noise, real motion, large
%   jumps due to erroneous tracking of moving object.
% motionInds = GetMovingIndsFromTraj(traj);
% [motionInds,backNoiseInds,outlierInds] = GetMovingIndsFromTraj(traj);
% Inputs:
% traj - N x 2 matrix, where N = # of time points, and N(:,1) =
%    x-coordinates of traj and N(:,2) = y-coordinates.
% Outputs:
% motionInds - Vector of indices estimated to be real motion.
% backNoiseInds - Vector of indices estimated to be noise.
% outlierInds - Vector of indices estimated to be large jumps in trajectory
%   resulting from failuers in the tracking of the moving object.
% 
% Avinash Pujala, Koyama lab/JRC, 2018

D = @(traj) sqrt(sum(diff(traj,[],1).^2,2));

ds = D(traj);
ds_pkInds = GetPks(ds,'polarity',1,'peakThr',0,'thrType','abs');
ds_pks = ds(ds_pkInds);
[label,model,~] = mixGaussEm(ds_pks(:)',3);
[~,sortInds] = sort(model.mu,'ascend');
lbls = unique(label);
lbls = lbls(sortInds);

[motionInds,backNoiseInds,outlierInds] = deal([]);
indVec = 1:numel(ds);
if numel(lbls)>2
   outlierInds = indVec(ds_pkInds(label == lbls(3)));
    motionInds = indVec(ds_pkInds(label == lbls(2)));
    backNoiseInds = indVec(ds_pkInds(label == lbls(1)));
elseif numel(lbls)>1
     motionInds = indVec(ds_pkInds(label == lbls(2)));
    backNoiseInds = indVec(ds_pkInds(label == lbls(1)));
elseif numel(lbls)==1
     backNoiseInds = indVec(ds_pkInds(label == lbls(1)));
else
    disp('No indices of any sort found. Check data!')
end


varargout{1} = motionInds+1;
varargout{2} = backNoiseInds+1;
varargout{3} = outlierInds+1;

end

