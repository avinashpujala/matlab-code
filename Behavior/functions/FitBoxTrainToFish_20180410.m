function varargout = FitBoxTrainToFish_20180410(img, params)
% FitBoxTrainToFish_20180410 - Given an image and parameters for making a
%   box train model of the fish returns updated parameters after fitting model
%   to fish in the input image. This does not use any optimization toolbox.
% params = FitBoxTrainToFish_20180410(img,params);
% [params, boxTrain_pts, img_bin] = ....
% Inputs:
% params - Structure variable with the following fields
%   .head_len - Scalar representing the length in pixels of the box used to
%       encompass the head. Default = 18 (approx 1 mm in my recording setup);
%   .head_trans - A 2-element vector that specifies the translation of the
%       head box in the x-y plane.
%
% Outputs:
% params - Structure variable containing the input information as well as
%   the rotation angles for the head box (absolute angle), the trunk box and
%   the tail box (relative angles w.r.t head box for trunk and tail).
% boxTrain_pts - The points (x,y coordinate) corresponding to the box train
%   model that has been fit to the fish in the image
% img_bin - Binary image of size(img), but with box train model points set
%       to 1

dRot = 2;
head_rot = 0:dRot:360;
trunk_rot = -80:dRot:80;
tail_rot = -80:dRot:80;

imgDims = size(img);

if ~isfield(params,'head_trans')
    params.head_trans = ceil(imgDims(1:2)/2);
elseif isempty(params.head_trans)
    params.head_trans = ceil(imgDims(1:2)/2);
end

if ~isfield(params,'head_len')
    params.head_len = 18;
elseif isempty(params.head_trans)
    params.head_len = 18;
end
params.trunk_rot = 0;
params.tail_rot = 0;

backToBack = @(x)[x(:); x(:); x(:)]';
middleThird  = @(x)x(((length(x)/3)+1):((2*length(x)/3)));

%% Find possible head angles
hr_now = zeros(length(head_rot),1);
% fitSum = hr_now;
count = 0;
for hr = head_rot
    count = count + 1;
    params.head_rot = hr;
    box_train = MakeBoxTrainFish(params);
    box_pts = round(box_train{1});
    box_train_inds = sub2ind(imgDims(1:2),box_pts(:,1),box_pts(:,2));
    box_pxls = img(box_train_inds);
    hr_now(count) = sum(box_pxls(:));
end

lenInds = 1:length(hr_now);
hr_now = backToBack(hr_now);
lenInds = backToBack(lenInds);
ker = gausswin(10);
ker = ker(:)/sum(ker);
hr_now = conv2(hr_now(:),ker(:),'same');
pkInds = GetPks(zscore(hr_now),'polarity',1);
pkInds = unique(lenInds(pkInds));
hr_now = head_rot(pkInds);


%% Find possible trunk angles that work well with possible head angles found before
count = 0;
sumVec = zeros(numel(hr_now)*numel(trunk_rot),1);
hr_keep = sumVec;
trr_keep =  sumVec;
for hr = hr_now(:)'
    for trr = trunk_rot
        count = count + 1;
        params.head_rot = hr;
        params.trunk_rot = trr;
        box_train = MakeBoxTrainFish(params);
        box_pts = round([box_train{1}; box_train{2}]);
        box_train_inds = sub2ind(imgDims(1:2),box_pts(:,1),box_pts(:,2));
        box_pxls = img(box_train_inds);
        sumVec(count) = sum(box_pxls(:));
        hr_keep(count) = hr;
        trr_keep(count) = trr;
    end
end
lenInds = 1:length(sumVec);
sumVec = backToBack(sumVec);
lenInds = backToBack(lenInds);
sumVec = conv2(sumVec(:),ker(:),'same');
pkInds = GetPks(sumVec,'polarity',1);
pkInds = unique(lenInds(pkInds));
sumVec = middleThird(sumVec);
[~,tempInds] = sort(sumVec(pkInds),'descend');
if ~isempty(tempInds)
    lastInd = min(3,length(tempInds));
    tempInds = tempInds(1:lastInd);
end
pkInds = pkInds(tempInds);
hr_keep = hr_keep(pkInds);
trr_keep = trr_keep(pkInds);

%% Find possible tail angles that work well with possible head and trunk angles found before
sumVec = zeros(numel(hr_keep)*numel(trr_keep)*numel(tail_rot),1);
hr_now = sumVec;
trr_now =  sumVec;
tar_now = sumVec;
count =0;
for hr = hr_keep(:)'
%     disp(hr)
    for trr = trr_keep(:)'
        for tar = tail_rot(:)'
            count = count + 1;
            params.head_rot = hr;
            params.trunk_rot = trr;
            params.tail_rot = tar;
            [~,box_pts] = MakeBoxTrainFish(params);
            box_pts = round(box_pts);
            box_train_inds = sub2ind(imgDims(1:2),box_pts(:,1),box_pts(:,2));
            box_pxls = img(box_train_inds);
            sumVec(count) = sum(box_pxls(:));
            hr_now(count) = hr;
            trr_now(count) = trr;
            tar_now(count) = tar;
        end
    end
end
lenInds = 1:length(sumVec);
sumVec = backToBack(sumVec);
lenInds = backToBack(lenInds);
sumVec = conv2(sumVec(:),ker(:),'same');
[~,maxInd] = max(sumVec);
maxInd = lenInds(maxInd);
hr_keep = hr_now(maxInd);
trr_keep = trr_now(maxInd);
tar_keep = tar_now(maxInd);


params.head_rot = hr_keep;
params.trunk_rot= trr_keep;
params.tail_rot = tar_keep;

if nargout >1
    box_train = MakeBoxTrainFish(params);
    box_train_all = [box_train{1}; box_train{2}; box_train{3}];
    box_train_round = round(box_train_all);
    box_train_inds = sub2ind(size(img), box_train_round(:,1),box_train_round(:,2));
    img_bin = img*0;
    img_bin(box_train_inds) = 1;
    img_bin = bwmorph(img_bin,'fill','holes');
end

varargout{1} = params;
varargout{2} = box_train;
varargout{3} = img_bin;

end

