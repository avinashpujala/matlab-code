function varargout = GetOffTrajInds_180308(fp,varargin)
%GetOffTrajInds From the trajectory of the fish, estimates the points that
%   deviate from a smooth spatial trajectory and returns their indices
%   The algorithm assumes that given the sufficiently fast frame rate the
%   fish swims in smooth trajectories and that any jump-like
%   discontinuities result from noise in the images.
% offTrajInds = GetOffTrajInds(fp);
% offTrajInds = GetOffTrajInds(fp,thr,smoothness);
% [offTrajInds,eta,fp_fit] = GetOffTrajInds(...)
% Inputs:
% fp - N-by-2 matrix giving fish trajectory, where N is the number of spatial points in
%   the trajectory, fp(:,1) is the x-positions and fp(:,2) is the y-positions
% thr - Arbitrary threshold for the error (similar to rms) between the
%   detected trajectory and fit trajectory (Default = 5);
% smoothness - Smoothness of the B-spline fit to the trajectory (Default = 6)
% Outputs:
% offTrajInds - Indices of points estimated not to be part of the trajectory
% eta - Computed error
% fp_fit - The fit trajectory used to detect the error
% offTrajInds2 - Off trajectory inds esimated with AND condition for thetas
% 
% Avinash Pujala, JRC, 2017

thr = 0.2;
smoothness = 6;

R = @(traj,traj_fit) sqrt(sum((traj-traj_fit).^2,2))/size(traj,1);


fp_fit = FitBSplineToCurve(fp,[],smoothness);

eta = R(fp,fp_fit);

crossInds = LevelCrossings(eta,thr);
onInds = crossInds{1}(1:2:end);
if length(crossInds{2})>1
    offInds = crossInds{2}(2:2:end);
else
    offInds = crossInds{2}(1:2:end);
end

if numel(offInds) < numel(onInds)
    offInds(end+1) = length(fp); 
end

offTrajInds = [];
for jj = 1:length(onInds)
    offTrajInds = union(offTrajInds,onInds(jj):offInds(jj));
end

onTrajInds = setdiff(1:size(fp,1),offTrajInds);


% fp_interp = FitBSplineToCurve(fp(onTrajInds,:),[],smoothness,size(fp,1));

varargout{1} = offTrajInds;
varargout{2} = eta;
varargout{3} = fp_fit;
% varargout{4} = fp_interp;

end

