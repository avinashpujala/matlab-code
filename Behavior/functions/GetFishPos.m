function varargout = GetFishPos(IM,varargin)
%GetFishPos Give an image stack returns the x,y coordinates of the fish in
%   each of the images of the stack
% fishPos = GetFishPos(IM);
% fishpos = GetFishPos(IM,nPixels)
% fishPos = GetFishPos(IM, nPixels,...)
% [fishPos,headOrientationVector] = GetFishPos(IM,nPixels,...);
% [fishPos,headOr]
% Inputs:
% IM - Image stack
% 'nPxls' - # of bright pixels for centroid determination. Setting nPixels
%   to [], results in default value of 30.
% Optional input arguments
% 'method' - Centroid detection method: {'median'}, ['mean'],'blob' (Uses regionprops
%           - not yet implemented).
% 'filter' - Bandpass values, e.g., [15 25]; If bp is a scalar then creates
%   a gaussian kernel of this size and uses this to convolve the image
%   instead of using 'gaussianbpf'
% 'process' - 'serial' or 'parallel'; Process in serial or parallel
% 'lineLen' - Length of line segment to use to determine head orientation
%   (default: 15 pxls). If not specified, then does not get head
%   orientation, which is computationally time-consuming.
% 'pxlLim' - Limit of how many pixels the fish can traverse in the imaging
%   setup. This is can be used to reduce false positives. (Not yet implemented)
% 'tryCorrect' - Boolean; If true, tries to correct the erroneous fish
%    positions based on the assumption that these can jump around from one
%    frame to the next.
%
% Outputs:
% fishPos - x, y coordinates of fish head position
% headOrientationVector - x,y coordinates of the head orientation vector (length
%   is determined by the variable 'lineLen')
%
% Avinash Pujala, HHMI, 2016

global method process filterFlag fltOrKer orFlag lineLen nPxls
method = 'mean';
process = 'serial';
filterFlag = 0;
fltOrKer = [];
orFlag = 0;
nPxls = 30;
plotBool = 1;
poolSize = 10;
lineLen = [];
tryCorrect = false;
thr_dist = 20; % # pixels fish can cover from one frame to the next
% (~ 300mm/sec with my recording conditions)

nArgs = length(varargin);
for jj = 1:nArgs
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'method'
                method =  varargin{jj+1};
            case 'filter'
                bp =  varargin{jj+1};
                if ~isempty(bp)
                    filterFlag = 1;
                end
            case 'process'
                process =  varargin{jj+1};
            case lower('pxlLim')
                pxlLim = varargin{jj+1};
            case lower('lineLen')
                lineLen = varargin{jj+1};
                if nargout > 1
                    orFlag = 1;
                    hOr = cell(size(IM,3),1);
                    if isempty(lineLen)
                        lineLen = 15;
                    end
                end
            case 'plotbool'
                plotBool = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'npxls'
                nPxls = varargin{jj+1};
            case 'thr_dist'
                thr_dist = varargin{jj+1};
        end
    end
end


if filterFlag
    if numel(bp)==1
        ker = gausswin(bp);
        ker = ker(:)*ker(:)';
        ker = ker/numel(ker);
        fltOrKer = ker;
        filterFlag = 2;
    elseif numel(bp)==2
        [~,flt] = gaussianbpf(IM(:,:,1),bp(1),bp(2));
        fltOrKer = flt;
    else
        error('Check inputs, filter input must be a scalar or a 2-element vector!')
    end
end

dispChunk = round(size(IM,3)/50)+1;
fishPos = zeros(size(IM,3),2);
tic
disp('Tracking fish...')
if strcmpi(process,'serial')
    for jj=1:size(IM,3)
        [fishPos(jj,:),hOr{jj},img] = FishPosAndHeadVec(IM(:,:,jj),filterFlag,orFlag,...
            fltOrKer,nPxls,method,lineLen);
        if mod(jj,dispChunk)==0
            disp(['Img # ' num2str(jj)])
        end
        if plotBool
            ShowFishPos(img,[fishPos(jj,2),fishPos(jj,1)],hOr{jj},jj)
        end
    end
elseif strcmpi(process, 'parallel')
    imgFrames = 1:size(IM,3);
    if matlabpool('size')==0
        matlabpool(poolSize)
    end
    parfor jj=imgFrames
        [fishPos(jj,:),hOr{jj},~] = FishPosAndHeadVec(IM(:,:,jj),filterFlag,orFlag,...
            fltOrKer,nPxls,method,lineLen);
        if mod(jj,dispChunk)==0
            disp(['Img # ' num2str(jj)])
        end
    end
end

if tryCorrect
    [fishPos,inds_corr] = JumpCorrectFishPos(fishPos,IM);
end

disp('Correcting head orientation for jumps...')
if ~isempty(cell2mat(hOr))
    hOr_corr = JumpCorrectHeadOr(hOr);
else
    hOr_corr = [];
end


toc
varargout{1} = fishPos;
varargout{2} = hOr_corr;


    function [fishPos, inds_corr] = JumpCorrectFishPos(fishPos,IM)      
        imgDims = size(IM);
        thr_dist = ceil(min([min(imgDims(1:2))/6, thr_dist]));
        ks = mean(bp);
        [offTrajInds,thetas,dists,~,fp_interp] = GetOffTrajInds(fishPos,imgDims(1:2),ks);
        onTrajInds = setdiff(1:size(fishPos,1),offTrajInds);        
        inds_corr = [];
        if ~ isempty(offTrajInds)
            disp('Propagating changes...')
            for pk = offTrajInds(:)'
                startInd = onTrajInds(onTrajInds< pk);
                if isempty(startInd)
                    startInd = pk;
                    stopInd = min(onTrajInds(onTrajInds > pk));
                    inds_start_stop = setdiff(startInd:stopInd,inds_corr);
                    inds_start_stop = sort(inds_start_stop,'descend');
                    if ~isempty(inds_start_stop)
                        disp('Backward...')
                        inds_corr = union(inds_corr,inds_start_stop);
                        disp([num2str(numel(inds_start_stop)) ' frames...'])
                    end
                    for jj = inds_start_stop(:)'
                        if jj == size(IM,3)
                             IM(:,:,jj) = SpotlightFish(IM(:,:,jj),fishPos(jj-1,:),thr_dist*2,'gauss');
                            [fishPos(jj,:),hOr{jj},~] = FishPosAndHeadVec(IM(:,:,jj),filterFlag,orFlag,...
                                fltOrKer,nPxls,method,lineLen);
                        else
                            IM(:,:,jj+1) = SpotlightFish(IM(:,:,jj+1),fishPos(jj,:),thr_dist*2,'gauss');
                            [fishPos(jj+1,:),hOr{jj+1},~] = FishPosAndHeadVec(IM(:,:,jj+1),filterFlag,orFlag,...
                                fltOrKer,nPxls,method,lineLen);
                        end
                    end
                else
                    startInd = max(startInd);
                    stopInd = onTrajInds(onTrajInds > pk);
                    if isempty(stopInd)
                        stopInd = pk;
                    else
                        stopInd = min(stopInd);
                    end
                    inds_start_stop = startInd:stopInd;
                    inds_start_stop = setdiff(inds_start_stop,inds_corr);
                    if ~isempty(inds_start_stop)
                        disp('Forwards...')
                        inds_corr = union(inds_corr,inds_start_stop);
                        disp([num2str(numel(inds_start_stop)) ' frames...'])
                    end
                    for jj = inds_start_stop(:)'
                        if jj == size(IM,3)
                            IM(:,:,jj) = SpotlightFish(IM(:,:,jj),fishPos(jj-1,:),thr_dist*2,'gauss');
                            [fishPos(jj,:),hOr{jj},~] = FishPosAndHeadVec(IM(:,:,jj),filterFlag,orFlag,...
                                fltOrKer,nPxls,method,lineLen);
                        else
                            IM(:,:,jj+1) = SpotlightFish(IM(:,:,jj+1),fishPos(jj,:),thr_dist*2,'gauss');
                            [fishPos(jj+1,:),hOr{jj+1},~] = FishPosAndHeadVec(IM(:,:,jj+1),filterFlag,orFlag,...
                                fltOrKer,nPxls,method,lineLen);
                        end                     
                    end
                end
            end
        else
            disp('No erroneous jumps in fish position detected!')
        end
    end

end


function varargout = FishPosAndHeadVec(img,filterFlag,orFlag,fltOrKer,nPxls,method,lineLen)
if filterFlag ==1
    img = gaussianbpf(img,fltOrKer);
elseif filterFlag ==2
    img = conv2(img,fltOrKer,'same');
end
[r,c] = FishPosInImg(img,nPxls,method);
x = c;
y = r;
if orFlag
    blah= GetMidlines(img,[c,r],lineLen,'plotBool',0);
    inds = blah{1}{1};
    [yy,xx] = ind2sub(size(img),inds);
    hOr = [xx,yy];
else
    hOr = [];
end
varargout{1} = [x,y];
varargout{2} = hOr;
varargout{3} = img;
end

function [r,c] = FishPosInImg(img,nPxls,method)
switch lower(method)
    case 'median'
        [~,maxInds] = sort(img(:),'descend');
        maxInds = maxInds(1:nPxls);
        [r,c] = ind2sub(size(img),maxInds);
        r = round(median(r));
        c = round(median(c));
    case 'mean'
        [~,maxInds] = sort(img(:),'descend');
        maxInds = maxInds(1:nPxls);
        [r,c] = ind2sub(size(img),maxInds);
        r = round(sum(r(:).*img(maxInds))/sum(img(maxInds)));
        c = round(sum(c(:).*img(maxInds))/sum(img(maxInds)));
    case 'blob'
        blah = img;
        blah(blah<0)=0;
        thr  = multithresh(blah,1);
        img_bw = img*0;
        img_bw(img>=thr)= 1;
        cc = bwconncomp(img_bw);
        rp = regionprops(cc,'Area','Centroid','ConvexArea',...
            'Eccentricity','Extent','FilledArea','MajorAxisLength','MinorAxisLength');
end


end

function ShowFishPos(img,fishPos,hOr,imgNum)
cla
if size(hOr,2)==1
    [y,x] = ind2sub(size(img),hOr);
else
    x = hOr(:,1);
    y = hOr(:,2);
end

imagesc(img), axis image, colormap(gray)
hold on
plot(fishPos(2), fishPos(1),'ro')
plot(x,y,'g.')
title(['Frame # ' num2str(imgNum)])
drawnow
shg
end

function hOr_corr = JumpCorrectHeadOr(hOr)
A = @(v1,v2)angle(v1*conj(v2))*180/pi;
hOr_corr = hOr;
for n = 2:length(hOr_corr)-1
    c_prev  = Line2Vec(hOr_corr{n-1});
    c_now = Line2Vec(hOr_corr{n});
    c_next = Line2Vec(hOr_corr{n+1});
    a1 = A(c_prev,c_now);
    a2 = A(c_now,c_next);
    a3 = A(c_prev,c_next);
    if (abs(a1)> abs(a3)) && (abs(a2)> abs(a3)) && (mean([abs(a1), abs(a2)])>100)
        hOr_corr{n}  = round((hOr_corr{n-1} + hOr_corr{n+1})/2);
        disp(['Corrected # ' num2str(n)])
    end
end
    function c = Line2Vec(xy)
        a = xy(1,:)-xy(end,:);
        c = a(1) + a(2)*1i;
    end
end
