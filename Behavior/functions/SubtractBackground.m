function varargout = SubtractBackground(I,varargin)
%SubtractBackground Subtracts background from images
% I_backSub = SubtractBackground(I)
% [I_backSub, backImg] = SubtractBackground(IM,'refFrames', refFrames,'method',method,'backImg',backImg,'alpha',alpha);
% Inputs:
% IM - Image stack where 3rd time is time
% refFrames - A vector specifying the frames to average for background
%   subtraction. This is useful for considering only frames in which fish
%   moves
% method - 'mean'[default], 'median','maxInt', 'erase'; Method of computing
%   background. For 'mean', 'median', 'maxInt' computes the mean, median,
%   or max-intensity along the 3rd dimension (time) of the image stack and
%   substracts this from all the images. For 'erase', selects and image in
%   which the fish can be 'erased' out using roifill and then this image is
%   used as the background image that is subtracted from all other images.
% backImg - Image used for background subtraction. If empty, then computes
%   by one of the methods specified by the input 'method'.
% alpha - A scalar between 0 and 1 (default = 0) indicating the fraction of
%   the original image to mix with the background subtracted image. In other
%   words, img_bg = 0.05*img_orig + (1-0.05)*img_bg, where img_bg is background
%   subtracted image and img_orig is the original image.
% Outputs:
% IM_proc - Stack with background subtracted images
% backImg = Reference image (mean,median,or maxInt of image stack) used for background
%   subtraction
%
% Avinash Pujala, Koyama lab/HHMI, 2016

poolSize = 10;
refFrames = [];
method = 'mean';
backImg = [];
alpha = 0;

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'refframes'
                refFrames = varargin{jj+1};
            case 'method'
                method = varargin{jj+1};
            case 'backimg'
                backImg = varargin{jj+1};
            case 'alpha'
                alpha = varargin{jj+1};
        end
    end
end

disp('Computing ref frame...')
if isempty(refFrames)
    if size(I,3)>1
        if size(I,3) > 500
            rp = randperm(size(I,3));
            rp(rp==1)=[];
            refFrames = rp(1:500);
        else
            refFrames = 2:size(I,3); % For some reason the 1st image doesn't record properly [AP - 20160621]
        end
    else
        refFrames = 1;
    end
end

if isempty(backImg)
    disp('Computing background... ')
    switch lower(method)
        case 'mean'
            disp('Mean subtraction')
            backImg = mean(I(:,:,refFrames),3);
        case 'median'
            disp('Median subtraction')
            backImg = median(I(:,:,refFrames),3);
        case 'mode'
            disp('Mode subtraction')
            backImg = mode(I(:,:,refFrames),3);
        case 'maxint'
            disp('Max-int subtraction')
            backImg = max(I(:,:,refFrames),[],3);
        case 'erase'
            ind = round(size(I,3)/2);
            backImg = I(:,:,ind);
            eraseMore = 'y';
            while strcmpi(eraseMore,'y')
                fh = figure();
                imagesc(backImg),axis image, colormap(gray)
                title('Zoom on fish and then click enter to escape')
                shg
                %                 zoom on
                %                 pause
                %                 zoom off
                
                title('Draw roi around fish, double click to exit')
                backImg = roifill;
                close(fh)
                eraseMore = input('Erase another fish/spot (y/n)? ','s');
            end
    end
end


disp('Subtracting background...')
if size(I,3)>=500
    IM_proc = ProcInParallel(I,backImg,poolSize,alpha);
else
    IM_proc = ProcInSerial(I,backImg,alpha);
end

varargout{1} = IM_proc;
varargout{2} = backImg;

end
function IM_proc = ProcInParallel(IM,ref,poolSize,alpha)
if strcmpi(class(IM),'mappedTensor')
    IM_proc = MappedTensor(size(IM));
else
    IM_proc = zeros(size(IM));
end

imgFrames= 1:size(IM,3);
poolOpened = 0;
poolObj = gcp('nocreate');
if isempty(poolObj)
    poolObj = parpool(poolSize);
    poolOpened = 1;
end
dispChunk = round(numel(imgFrames)/30);
parfor jj=imgFrames
    IM_proc(:,:,jj) = -squeeze(IM(:,:,jj))+ref;
    if mod(jj, dispChunk)==0
        disp(['Img# ' num2str(jj)])
    end
end
if alpha
    IM_proc = alpha*(-IM) + (1-alpha)*IM_proc;
end
% if poolOpened
%     delete(poolObj)
% end
end

function IM_proc = ProcInSerial(IM,ref,alpha)
if strcmpi(class(IM),'mappedTensor')
    IM_proc = MappedTensor(size(IM));
else
    IM_proc = zeros(size(IM));
end

imgFrames= 1:size(IM,3);
dispChunk = round(numel(imgFrames)/30);
for jj=imgFrames
    %     IM_proc(:,:,jj) = conv2(-squeeze(IM(:,:,jj))+ref,ones(5)/25,'same');
    IM_proc(:,:,jj) = -squeeze(IM(:,:,jj))+ref;
    IM_proc(:,:,jj) = alpha*(-IM(:,:,jj)) + (1-alpha)*IM_proc(:,:,jj);
    if mod(jj, dispChunk)==0
        disp(['Img# ' num2str(jj)])
    end
end
end