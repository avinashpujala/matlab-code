function varargout = FindFishPxlsForMidlineTracking(I,varargin)
%FindFishPxlsForMidlineTracking Given an image stack and a few optional
%   parameters, returns binary images with fish pixels set to 1. Assumes
%   that the fish head position is in the middle of the image for each
%   image. Use CropImages.m to create such head-centered images.
% fishPxls = FindFishPxlsForMidlineTracking(I);
% fishPxls = FindFishPxlsForMidlineTracking(I,'headDiam',headDiam);
% Inputs:
% I - Image stack in which to find fish pixels. Size = [M, N, T], where T
%   is # of time points
% Optional inputs:
% headDiam - Approximate diameter of the head in mm (Default = 1). This determines
%   the size of the gaussian kernel used for smoothing, and later for thickening
%   connections between fragmented blobs and filling holes in fish.
% Outputs:
% I_fish - Binary image stack with same size as I with fish pixels set to 1.
%
% Avinash Pujala, Koyama lab/JRC, 2018

%=== Default values
headDiam = 1; % In mm
pxlLen = 0.05; % Approximate length of pxl. This is what it usually ends up being for my images.

for jj  = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'pxllen'
                pxlLen = varargin{jj+1};
        end
    end
end


headDiam_pxl = round(headDiam/pxlLen);
ker = gausswin(ceil(headDiam_pxl/2));
ker = (ker(:)*ker(:)');
ker = ker./sum(ker(:));

I(I<0)=0;
I_smooth = convn(I,ker,'same');


thr =  GetMultiThr(I_smooth,3);
I_fish = I>=thr(2);

%% Get rid of regions that do not include fish head position
fp = ceil(size(I)/2);
fp = fp(1:2);

imgInds = 1:size(I,3);
calcDist = @(manyPts,singlePt) sqrt(sum((repmat(singlePt,size(manyPts,1),1)-manyPts).^2,2));
nFishPxls = zeros(numel(imgInds),1);
dists = cell(numel(imgInds),1);
for jj = imgInds(:)'
    rp = regionprops(bwconncomp(I_fish(:,:,jj)),'PixelList','PixelIdxList');
    rpDelInds = zeros(length(rp),1);
    for kk = 1:length(rp)
        d = calcDist(rp(kk).PixelList,fp);
        if min(d) >=3
            rpDelInds(kk)=1;
        else
            dists{jj} = d;
        end
    end
    rp(find(rpDelInds))=[];
    foo = I_fish(:,:,jj);
    foo = foo*0;
    if length(rp)>0
        foo(rp(1).PixelIdxList) = 1;
        nFishPxls(jj) = length(rp(1).PixelIdxList);
    else
        nFishPxls(jj) = nan;
        disp(['No fish pxls detected for img # ' num2str(jj)])
    end
     I_fish(:,:,jj) = foo;  
end


%% Iterate
blah = nFishPxls;
% inds_nonNan = find(~isnan(blah)); 
blah(isnan(blah))=[]; 
if length(blah)>=50
    blah_smooth = FitBSplineToCurve(blah,4,10);
    eta = zscore(abs(blah_smooth-blah));
    delInds = find(eta>=2);
    blah(delInds)=[];
    mu = mean(blah) + std(blah); % Turns out the first 50 frames are much more reliable
else
    mu = mean(blah) + std(blah);
end

if ~(all(isnan(nFishPxls)))
    sigma = std(nFishPxls);
    overInds = find(nFishPxls > (mu + sigma));
    if isempty(overInds)
        disp('No excess pixel frames!')
    else
        disp(['Correcting ' num2str(numel(overInds)) ' with excess fish pxls...'])
    end
    
    itermax = 100;
    for jj = overInds(:)'
        lineInds = [];
        thr_new = thr(2);
        wt = 0.5;
        count = 0;
        foo = I_smooth(:,:,jj);
        T = []; N = [];
        n_prev = abs(nFishPxls(jj)-mu);
        shrinking = true;
        while (count < itermax) && (abs(nFishPxls(jj)- mu) > sigma);
            count = count + 1;
            T(count) = thr_new;
            N(count) = nFishPxls(jj);
            thr_new = wt*thr(1) + (1-wt)*thr(2);
            blah = foo>=thr_new;
            nFishPxls(jj) = sum(blah(:));
            n_now = abs(nFishPxls(jj)-mu);
            if n_now < n_prev
                shrinking = true;
            else
                shrinking = false;
            end
            if (nFishPxls(jj)-mu > 0) && shrinking
                wt = wt*1.05;
            elseif (nFishPxls(jj)-mu < 0) && shrinking
                wt = wt*0.95;
            end
        end
        foo(foo<thr_new) = 0;
        foo(foo>=thr_new) = 1;
%         foo = imfill(foo,'holes');
        rp = regionprops(bwconncomp(foo),'PixelIdxList','PixelList','Centroid','MajorAxisLength');
        if length(rp)>1
            line_inds = cell(length(rp),1);
            cp = nan(length(rp),2);
            for bb = 1:length(rp)
                cp(bb,:) = rp(bb).Centroid;
            end
            %=== Connect fragmented fish blobs with straight lines
            for bb = 1:length(rp)
                blob1 = rp(bb);
                d = calcDist(cp,blob1.Centroid);
                d(d==0) = inf;
                [~,minInd] = min(d);
                blob2 = rp(minInd);
                closestPts = FindClosestPoints(blob1.PixelList,blob2.PixelList);
                for pp = 1:size(closestPts,1)
                    try
                    line_x = linspace(closestPts(pp,1,1),closestPts(pp,1,2),20);
                    line_y = linspace(closestPts(pp,2,1),closestPts(pp,2,2),20);
                    line_inds{bb} = sub2ind(size(foo),round(line_y),round(line_x));
                    catch
                        a = 1;
                    end
                end
            end
            lineInds = unique([line_inds{:}]);
        end
        %== Smooth images a bit, especially to thicken connections between
        % formerly fragmented blobs
        blah = foo;
        blah(lineInds) = 1;
        I_fish(:,:,jj) = blah;
    end
    
    underInds = find(nFishPxls < (mu - sigma));
    if isempty(underInds)
        disp('No deficient pixel inds!')
    else
        disp(['Correcting ' num2str(numel(underInds)) ' with too few fish pxls...'])
    end
    for jj = underInds(:)'
        lineInds = [];
        thr_new = thr(2);
        wt = 0.5;
        count = 0;
        foo = I_smooth(:,:,jj);
        T = []; N = [];
        n_prev = abs(nFishPxls(jj)-mu);
        shrinking = true;
        frag = true;
        while (count < 100) && (abs(nFishPxls(jj)- mu) > sigma) && shrinking && frag;
            count = count + 1;
            T(count) = thr_new;
            N(count) = nFishPxls(jj);
            thr_new = wt*thr(2) + (1-wt)*0;
            blah = foo>=thr_new;
            nFishPxls(jj) = sum(blah(:));
            n_now = abs(nFishPxls(jj)-mu);
            if n_now < n_prev
                shrinking = true;
            else
                shrinking = false;
            end
            if (nFishPxls(jj)-mu > 0) && shrinking
                wt = wt*1.05;
            elseif (nFishPxls(jj)-mu < sigma) && shrinking
                wt = wt*0.95;
            end
        end
        foo(foo<thr_new) = 0;
        foo(foo>=thr_new) = 1;
%         foo = imfill(foo,'holes');
        rp = regionprops(bwconncomp(foo),'PixelIdxList','PixelList','Centroid','MajorAxisLength');
        if length(rp)>1
            line_inds = cell(length(rp),1);
            cp = nan(length(rp),2);
            for bb = 1:length(rp)
                cp(bb,:) = rp(bb).Centroid;
            end
            %=== Connect fragmented fish blobs with straight lines
            for bb = 1:length(rp)
                blob1 = rp(bb);
                d = calcDist(cp,blob1.Centroid);
                d(d==0) = inf;
                [~,minInd] = min(d);
                blob2 = rp(minInd);
                [closestPts,dists_closest] = FindClosestPoints(blob1.PixelList,blob2.PixelList);
                farInds = find(dists_closest > headDiam_pxl/headDiam_pxl); % At the moment I don't want to connect
%                 dists_closet(farInds) = [];
                closestPts(farInds,:,:) = [];
                for pp = 1:size(closestPts,1)
                    cp1 = squeeze(closestPts(pp,:,1));
                    cp2 = squeeze(closestPts(pp,:,2));
                    d_pts = sqrt(sum((cp1-cp2).^2))*pxlLen;
                    if d_pts <= (2*headDiam)
                        line_x = linspace(closestPts(pp,1,1),closestPts(pp,1,2),20);
                        line_y = linspace(closestPts(pp,2,1),closestPts(pp,2,2),20);
                        line_inds{bb} = sub2ind(size(foo),round(line_y),round(line_x));
                    end
                end
            end
            lineInds = unique([line_inds{:}]);
        end
        %== Smooth images a bit, especially to thicken connections between
        % formerly fragmented blobs
        blah = foo;
        blah(lineInds) = 1;
        I_fish(:,:,jj) = blah;
    end
end

%=== Make a smaller smoothing kernel for use in bridging gaps between blobs
ker_small = gausswin(max(ceil(headDiam_pxl/7),2));
ker_small = (ker_small(:)*ker_small(:)');
ker_small = ker_small./sum(ker_small(:));
disp('Thickening binary images by smoothing a bit...')
tic
I_fish = convn(double(I_fish),ker_small,'same');
toc

varargout{1} = I_fish;

end

