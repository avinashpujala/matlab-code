
function varargout = GetMidlinesByThinning_20180324(I,varargin)
%GetMidlinesByThinning Gets midlines of fish in each image of imgStack by thinning
%   process.
% midlines = GetMidlinesByThinning(I);
% [midlines,midlines_raw,dsVec,spineLens] = GetMidlinesByThinning(I,'len_midline',len_midline,
%   'spline_order',spline_order,'process',process,'plotBool',plotBool,'pauseDur',pauseDur);
% Inputs:
% I - Image stack positive values for only fish pixels
% len_midline - Scalar indicating the desired length of the smoothened
%   midline
% spline_order - Order of the spline basis used to fit raw midline
% spline_res - Resolution of the fit spline curve (default = 6). lower
%   values lead to smoother curves
% 'process' - Setting to 'parallel' results in parallel processing
% 'plotBool' - Seting to 1, results in diplaying of images with midlines
% 'pauseDur' - Pause interval between successive plots
% Outputs:
% midlines - Coordines of smoothened midlines. Size =  [len_midline,2,T], where
%   len_midline = length of midline, T = # of time points. 2nd dim gives x,
%   and y values respectively
% midlines_raw - Cell array of raw midline coordines extracted from images.
% dsVecs - A cell array  like mlInds, but where each cell contains the
%   distances from the head centroid of the fish to the midline points.
% splineLen - A vector of real lengths of midlines.
%
% Avinash Pujala, Koyama lab/HHMI, 2016

process = 'serial';
plotBool = 0;
pauseDur = 0;
nWorkers = 10;
len_midline = 50;
spline_order = 4;
spline_res = 8;

for in = 1:numel(varargin)
    if ischar(varargin{in})
        switch lower(varargin{in})
            case 'len_midline'
                len_midline = varargin{in+1};
            case 'spline_order'
                spline_order = varargin{in+1};
            case 'spline_res'
                spline_res = varargin{in+1};
            case 'process'
                process = varargin{in+1};
            case 'plotbool'
                plotBool = varargin{in+1};
            case 'pausedur'
                pauseDur = varargin{in+1};
        end
    end
end

mlInds = cell(size(I,3),1);
mlCoords = mlInds;
dsVecs = mlInds;
imgInds = 1:size(I,3);
imgDims = size(I);
imgDims = imgDims(1:2);

% Assuming that the image stacks have been adjusted such that fish head
%   centroid is in the center of the image
fp = ceil(imgDims/2);
fp = fp(1:2);

dispChunk = round(size(I,3)/5);
if plotBool && ~strcmpi(process,'parallel')
    figure('Name','Midline inds by thinning')
end
spineLens_nPts = zeros(numel(imgInds),1);
spineLens_dist = spineLens_nPts;
midlines = nan(len_midline,2,numel(imgInds));
failedInds = zeros(numel(imgInds),1);
if strcmpi(process,'serial')
    count = 0;
    try
    for tt = imgInds(:)'
        img = I(:,:,tt);
        [~,spineInds,~] = GetSpine(img);
        if isempty(spineInds)
            failedInds(tt) = 1;
            disp(['No midline in img # ' num2str(tt)])
        else
            [mlInds{tt},dsVecs{tt}] = GetMidlineCaudalToFishPos(fp,spineInds,imgDims);
            %             spineLens_nPts(tt) = length(mlInds{tt});
            if ~isempty(dsVecs{tt})
                spineLens_dist(tt)= dsVecs{tt}(end);
                [mlCoords{tt}(:,2),mlCoords{tt}(:,1)] = ind2sub(size(img),mlInds{tt});
                midlines(:,:,tt) = FitBSplineToCurve(mlCoords{tt},spline_order,spline_res,len_midline);
            end            
        end
        if plotBool
            cla
            imagesc(img),axis image, colormap(gray)
            hold on
            plot(midlines(:,1,tt),midlines(:,2,tt),'r','linewidth',2)
            title(['Img #' num2str(tt)])
            drawnow
            shg
            if isempty(pauseDur)
                pause()
            else
                pause(pauseDur)
            end
        end
        count = count + 1;
        if mod(tt,dispChunk)==0
            disp([num2str(100*(count/numel(imgInds))) '% completed...'])
        end
    end
    catch
       midlines(:,:,tt) = FitBSplineToCurve(mlCoords{tt},spline_order,spline_res,len_midline);
    end
else
    poolObj = gcp('nocreate');
    if isempty(poolObj)
        poolObj = parpool(nWorkers);
    end
    parfor tt = imgInds(:)'
        img = I(:,:,tt);
        [~,spineInds,~] = GetSpine(img);
        if isempty(spineInds)
            failedInds(tt) = 1;
            disp(['No midline in img # ' num2str(tt)])
        else
            [mlInds{tt},dsVecs{tt}] = GetMidlineCaudalToFishPos(fp,spineInds,imgDims);
            if ~isempty(dsVecs{tt})
                spineLens_dist(tt)= dsVecs{tt}(end);
                [mlCoords{tt}(:,2),mlCoords{tt}(:,1)] = ind2sub(size(img),mlInds{tt});
                midlines(:,:,tt) = FitBSplineToCurve(mlCoords{tt},spline_order,spline_res,len_midline);
            end
        end
        if mod(tt,dispChunk)==0
            disp(['Img # ' num2str(tt)])
        end
    end
end

failedInds = find(failedInds);
if ~isempty(failedInds)
    perc_failed = round((100*numel(failedInds)/numel(imgInds))*10)/10;    
    disp([num2str(numel(failedInds)) '/' num2str(numel(imgInds)) ' failed - ' num2str(perc_failed) ' %'])
end

disp('Correcting for possible reversal of point order in midlines...')
% [midlines,mlCoords,corrInds] = CorrectOrder(midlines,mlCoords,fp);

[midlines,mlCoords,~] = CorrectOrder_180301(midlines,mlCoords,fp); % This
% would work on with in a trial, but since I am not using fish head
% position as a tether, it can totally flip things around between trials

varargout{1} = midlines;
varargout{2} = mlCoords;
varargout{3} = dsVecs;
varargout{4} = spineLens_dist;
varargout{5} = failedInds;

end

function varargout = GetSpine(img)
%GetSpine Given an image, binarizes and returns the indices of the
%   spine obtained by morphological thinning as well as the spinalized
%   image
% spine = GetSpine(img);
% [spine,spineInds,img_thin] = GetSpine(img);
% Inputs:
% img - Image with positive values for fish pixels
% Outputs:
% spine - x, and y coordinates of spine obtained by thinning.
%   Size = [N, 2], where N is spine length, spine(:,1) = x-coordinates
%   and spine(:,2) = y-coordinates
% spineInds - Indices of spine. Size = [N, 1].
% img_thin - Spinalized image

img(img>0)= 1; % Binarize
img_thin = bwmorph(bwmorph(img,'thin',Inf),'spur');
[spine(:,2),spine(:,1)] = find(img_thin);
spineInds = find(img_thin);

varargout{1} = spine;
varargout{2} = spineInds;
varargout{3} = img_thin;

end

function [mlInds, distVec] = GetMidlineCaudalToFishPos(fp,mlInds,imgDims)
[mlCoords(:,2),mlCoords(:,1)] = ind2sub(imgDims,mlInds);

%## Find midline pts rostral to head centroid pos and remove
[mlCoords_ord,s_ord] = IncreasinglyDistantPtsFromRef(fp,mlCoords);
[~,jumpInd] = max(s_ord);
if jumpInd > length(s_ord)-jumpInd
    mlCoords_ord(jumpInd:end,:)=[];
    s_ord(jumpInd:end) = [];
else
    mlCoords_ord(1:jumpInd-1,:)=[];
    s_ord(1:jumpInd-1) = [];
end
%###
if ~isempty(mlCoords_ord)
    mlCoords_ord = [round(fp); mlCoords_ord];
    s_ord = [0; s_ord];
end

mlInds = sub2ind(imgDims,mlCoords_ord(:,2),mlCoords_ord(:,1));
distVec = cumsum(s_ord);

end

function [pts_order,s_order] = IncreasinglyDistantPtsFromRef(refPt,pts)
S = @(v,V) sqrt(sum((repmat(v,size(V,1),1)-V).^2,2));
pts_new = pts;
s = S(refPt,pts_new);
s_new = s;
count= 0;
pts_order = zeros(size(pts));
s_order = zeros(size(s));
while ~isempty(s_new) %length(s_new)>0
    count = count + 1;
    [d, ind] = min(s_new);
    s_order(count)=d;
    pts_order(count,:) = pts_new(ind,:);
    refPt = pts_new(ind,:);
    %     s_new(ind)=[];
    pts_new(ind,:) = [];
    s_new = S(refPt,pts_new);
end
zerInds = find(pts_order(:,1)==0 | pts_order(:,2)==0);
pts_order(zerInds,:) = [];
s_order(zerInds) = [];
end

function [midlines,mlCoords,corrInds] = CorrectOrder_180301(midlines,mlCoords,fp)
%% Inputs
% Every once in a while,the order of points on a midline curve gets flipped such that
%   the first point in the numbering actually goes from the tail to the head. This can
%   happen especially when the fish makes an O-bend and the tail is
%   practically over the head.

% An inline func for getting the sum of the point-by-point distance between
%   two curves
R =@(ml_one,ml_two)sum(sqrt(sum((ml_one-ml_two).^2,2)));
DistFromOtherPts = @(otherPts,currentPt) sum(sqrt(sum((repmat(currentPt,size(otherPts,1),1)-otherPts).^2,2)));
corrInds = zeros(size(midlines,3),1);
% tLenVec = 1:size(midlines,3);
for jj = 1:size(midlines,3)-1
    d1 = R(midlines(:,:,jj),midlines(:,:,jj+1));
    d2 = R(midlines(:,:,jj),flipud(midlines(:,:,jj+1)));
    if (d2<d1)
        d_head = sqrt(sum((midlines(1,:,jj+1)-fp).^2,2));
        d_tail = sqrt(sum((midlines(end,:,jj+1)-fp).^2,2));
%         midlines(:,:,jj+1) = flipud(midlines(:,:,jj+1));
        %%%%% Prevent sequential propagation of a mistake
        if d_head < 3*d_tail
            midlines(:,:,jj+1) = flipud(midlines(:,:,jj+1));
            mlCoords{jj+1} = flipud(mlCoords{jj+1});
            corrInds(jj+1) = 1-corrInds(jj+1);
        end    
    end    
end
corrInds = find(corrInds);


end

function [midlines,mlCoords,corrInds] = CorrectOrder(midlines,mlCoords,fp)
%% Inputs
% Every once in a while,the order of points on a midline curve gets flipped such that
%   the first point in the numbering actually goes from the tail to the head. This can
%   happen especially when the fish makes an O-bend and the tail is
%   practically over the head.

% An inline func for getting the sum of the point-by-point distance between
%   two curves
R =@(ml_one,ml_two)sum(sqrt(sum((ml_one-ml_two).^2,2)));
corrInds = zeros(size(midlines,3),1);
for jj = 1:size(midlines,3)-1
    d1 = R(midlines(:,:,jj),midlines(:,:,jj+1));
    d2 = R(midlines(:,:,jj),flipud(midlines(:,:,jj+1)));
    if (d2<d1)
        d_head = sqrt(sum((midlines(1,:,jj+1)-fp).^2,2));
        d_tail = sqrt(sum((midlines(end,:,jj+1)-fp).^2,2));
        midPt = ceil(size(midlines,1)/2);
        midPt = midlines(midPt,:,jj+1);
        d_middle = sqrt(sum((midlines(end,:,jj+1)-midPt).^2,2));
        Prevent sequential propagation of a mistake
        if d_tail < d_middle
            midlines(:,:,jj+1) = flipud(midlines(:,:,jj+1));
            mlCoords{jj+1} = flipud(mlCoords{jj+1});
            corrInds(jj+1) = 1-corrInds(jj+1);
        end
    end
end
corrInds_one = find(corrInds);


end
