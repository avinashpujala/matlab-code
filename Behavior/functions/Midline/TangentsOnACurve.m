function varargout = TangentsOnACurve(curv,tLen,varargin)
%TangentsOnaAcurve Given a curve, returns end points of tangents drawn on
%   the curve.
% T_curv = TangentsOnACurve(curve,len);
% [T_curv,slopes,angles] =
%   TangentsOnACurve(curve,len,'plotOrNot',plotOrNot);
% Inputs:
% curv - X and Y coordinates of a curve in 2D. Size = [nPts, 2], where nPts
%   is the number of points on the curve and each of the 2 columns are the
%   x & y coordinates respectively.
% tLen - Length of the tangents, in units of the line segments connecting
%   adjacent points on the curve
% 'plotOrNot'
% Outputs:
% T_curv - End points of tangents of length 'len' drawn on the curve
%   'curv'. Size = [nPts-1, 2]
%
% Avinash Pujala

plotOrNot = 0;

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'plotornot'
                plotOrNot = varargin{jj+1};
        end
    end
end

Ang = @(pt1,pt2) angle((pt2(1)-pt1(1)) + (pt2(2)-pt2(1))*1i);
Mag = @(pt1,pt2) norm(pt2-pt1);
Slo = @(pt1,pt2) (pt2(2)-pt1(2))/(pt2(1)-pt1(1));
nPts = size(curv,1);
x = curv(:,1);
y = curv(:,2);
T_curv = nan(nPts-1,2);
slopes = nan(nPts-1,1);
angles = slopes;
if plotOrNot
    figure('Name','Testing tangents');
    plot(x,y,'g.')
    hold on
    plot(x,y,'r')
    box off
    set(gca,'tickdir','out','color','k')
end
for jj = 1:size(curv,1)-1
    pt_now = [x(jj), y(jj)];
    pt_next = [x(jj+1), y(jj+1)];
    len = Mag(pt_now, pt_next);
    if x(jj+1)-x(jj)~=0
        m = Slo(pt_now,pt_next);
        slopes(jj) = m;
        theta =  Ang(pt_now,pt_next);
        angles(jj) = theta*180/pi;
        b = y(jj) -m*x(jj);
        scale = tLen/len;
        len_x = len*cos(atan(m));
        len_x2 = len*cos(theta);
        len_y = len*sin(atan(m));
        len_x = len_x*(abs(len_x2)/len_x2);
        x_new = len_x*scale + x(jj);
        y_new = m*x_new + b;
    else
        x_new = 0;
        y_new = y(jj) + tLen;
        slopes(jj) = inf;
        angles(jj) = 90;
    end
    T_curv(jj,:) = [x_new,y_new];
    if plotOrNot
        plot([x(jj),x_new],[y(jj),y_new],'color','w')
          
    end
end
if plotOrNot
    title('Curve with tangents on top')
    shg
    % axis image
end
varargout{1} = T_curv;
varargout{2} = slopes;
varargout{3} = angles;

end

