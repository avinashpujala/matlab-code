
function varargout = GetMidlinesByThinning_new2(imgStack,varargin)
%GetMidlinesByThinning Gets midlines of fish in each image of imgStack by thinning
%   process.
% mlInds = GetMidlinesByThinning(imgStack);
% mlInds = GetMidlinesByThinning(imgStack,'zThr',zThr,'mu',mu,'sigma',sigma,...
%   'minPxls',minPxls,'fishLen',fishLen,'fishPos',fishPos,'nIter',nIter);
% [mlInds,dsVecs,failedInds, mLens] = GetMidlinesByThinning(...);
% Inputs:
% imgStack - Image with fish in it
% 'nThr' - Number of threshold levels to go through to identify fish (see GetMultiThr)
% 'minThr' - Minimum threshold value to use in determining fish pixels for
%   binarization (see GetMultiThr).
% 'fishPos' - Position of fish, so as to exlude midlines that are too far.
% 'mu' - Mean to subtract when computing z-scores. If not specified, mu is
%   computed for input image, but when available it is better to use the mu
%   computed for the entire image stack.
% 'sigma' - Standard deviation to use for computign z-scores. If not
%   specified, it is computed.
% 'minPxls' - Min number of presumed pxls for midline. Default - 40
% 'fishLen' - Approx length of fish in mm. Used to select midline [default = 11]
% 'pxlLen' - Length of pixel (can be estimated by GetPxlSpacing.m). Default = 0.058;
% 'nIter' - Number of iteration to vary binarization threshold to try and
%   find real midline indices.
% 'kerSize' - Gaussian kernel size for smoothing of image(s).
% 'process' - Setting to 'parallel' results in parallel processing
% 'plotBool' - Seting to 1, results in diplaying of images with midlines
% 'pauseDur' - Pause interval between successive plots
% Outputs:
% mlInds - A cell array of the size of the image stack, where each cell
%   contains the fish midline indices for the corresponding image in the
%   stack
% dsVecs - A cell array  like mlInds, but where each cell contains the
%   distances from the head centroid of the fish to the midline points.
% Outputs:
% mlInds - Cell array of the size of image stack where each cell contains
%   the midline pixel indices of the fish
% dsVecs - Cell array like mlInds, where each cell contains distances for
%   between midline pixels and fish head centroid pixel
% failedInds - Indices of images in which midline detection failed.
% mLens - Lengths of midlines obtained
% 
% Avinash Pujala, Koyama lab/HHMI, 2016

nThr = 4;
minThr = 0;
mu = [];
sigma = [];
minPxls = 20;
fishLen = 11; % In mm
pxlLen = 0.058;
fishPos = [];
nIter = 20;
process = 'serial';
plotBool = 0;
kerSize = 7;
pauseDur = 0;
nWorkers = 10;

for in = 1:numel(varargin)
    if ischar(varargin{in})
        switch lower(varargin{in})
            case 'nthr'
                nThr = varargin{in+1};
            case 'minthr'
                minThr = varargin{in+1};
            case 'fishpos'
                fishPos = varargin{in+1};
            case 'mu'
                mu = varargin{in+1};
            case 'sigma'
                sigma = varargin{in+1};
            case 'minpxls'
                minPxls = varargin{in+1};
            case 'maxpxls'
                fishLen = varargin{in+1};
            case 'niter'
                nIter = varargin{in+1};
            case 'process'
                process = varargin{in+1};
            case 'plotbool'
                plotBool = varargin{in+1};
            case 'kersize'
                kerSize = varargin{in+1};
            case 'pausedur'
                pauseDur = varargin{in+1};
            case 'pxllen'
                pxlLen = varargin{in+1};
        end
    end
end

%-------------------------
% I am hoping that zeroing negative elements will help because if the fish
% is stationary in many frames then the pxls where the fish was stationary
% can have very negative values that can affect thresholding
imgStack(imgStack<0)=0;
% ------------------------

if isempty(mu)
    Z = @(x) (x-mean(x(:)))/std(x(:));
    imgStack = Z(imgStack);
    mu = 0;
    sigma = 1;
else
    imgStack = (imgStack-mu)/sigma;
end
ker = gausswin(kerSize)*gausswin(kerSize)';
ker = ker/sum(ker(:));

mlInds = cell(size(imgStack,3),1);
dsVecs = mlInds;
imgInds = 1:size(imgStack,3);
dispChunk = round(size(imgStack,3)/5);
if plotBool && ~strcmpi(process,'parallel')
    figure('Name','Midline inds by thinning')
end
imgDims = size(imgStack);
if strcmpi(process,'serial')
    count = 0;
    for tt = imgInds(:)'
        img = conv2(imgStack(:,:,tt),ker,'same');
        fp = fishPos(tt,:);
        blah = GetMidlineByThinning(img,'nThr',nThr,'minThr',minThr,'mu',mu,'sigma',sigma,...
            'minPxls',minPxls,'fishLen',fishLen,'fishPos',fp,'pxlLen',pxlLen);
        if length(blah)>100
            blah = GetMidlineByThinning(img,'nThr',nThr,'minThr',minThr,'mu',mu,'sigma',sigma,...
                'minPxls',minPxls,'fishLen',fishLen,'fishPos',fp,'pxlLen',pxlLen);
        end
        [mlInds{tt},dsVecs{tt}] = GetMidlineCaudalToFishPos(fp,blah,imgDims(1:2));
        if plotBool
            cla
            imagesc(img),axis image, colormap(gray)
            hold on
            [rInds,cInds] = ind2sub(size(img),mlInds{tt});
            plot(cInds,rInds,'r')
            plot(fp(1),fp(2),'go','markersize',8)
            title(['Img #' num2str(tt)])
            drawnow
            shg
            if isempty(pauseDur)
                pause()
            else
                pause(pauseDur)
            end
        end
        count= count + 1;
        if mod(tt,dispChunk)==0
            disp([num2str(100*(count/numel(imgInds))) '% completed...'])
        end
    end
else
    if matlabpool('size')==0
        matlabpool(nWorkers);
    end
    parfor tt = imgInds(:)'
        img = conv2(imgStack(:,:,tt),ker,'same');
        fp = fishPos(tt,:);
        blah = GetMidlineByThinning(img,'nThr',nThr,'minThr',minThr,'mu',mu,'sigma',sigma,...
            'minPxls',minPxls,'fishLen',fishLen,'fishPos',fp,'pxlLen',pxlLen);
        [blah,dsVecs{tt}] = GetMidlineCaudalToFishPos(fp,blah,imgDims(1:2));
        mlInds{tt} = blah;
        if mod(tt,dispChunk)==0
            disp(['Img # ' num2str(tt)])
        end
    end
end


if length(mlInds)==1
    mlInds = mlInds{1};
end
if length(dsVecs)==1
    dsVecs = dsVecs{1};
end

lens = zeros(length(mlInds),1);
for jj = 1:length(mlInds)
    if iscell(mlInds)
        lens(jj) = length(mlInds{jj});
    else
        lens(jj) = length(mlInds);
    end
    
end

zerInds  = find(lens==0);
% nonZerInds = setdiff(1:length(mlInds),zerInds);
% if length(mlInds)>1
%     for jj = zerInds(:)'
%         if jj==1
%             try
%             mlInds{jj} = mlInds{nonZerInds(1)};
%             catch
%                 a =1;
%             end
%             dsVecs{jj} = dsVecs{nonZerInds(1)};
%         elseif jj == length(mlInds)
%             mlInds{jj} = mlInds{jj-1};
%             dsVecs{jj} = dsVecs{nonZerInds(end)};
%         else
%             [r0,c0] = ind2sub(imgDims(1:2),mlInds{jj-1});
%             [r2,c2] = ind2sub(imgDims(1:2),mlInds{jj+1});
%             ds0 = dsVecs{jj-1};
%             ds2 = dsVecs{jj+1};
%             n = min([length(r0),length(r2)]);
%             rc = round(0.5*([r0(1:n), c0(1:n)] + [r2(1:n), c2(1:n)]));
%             dsVecs{jj} = 0.5*(ds0(1:n)+ ds2(1:n));
%             mlInds{jj} = sub2ind(imgDims(1:2),rc(:,1),rc(:,2));
%         end
%     end
% end

% disp('Correcting midline point order...')
% mlInds = CorrectOrder(mlInds,imgDims); %-->  Feel like it's better to
%   correct after smoothening

mLens = zeros(length(mlInds),1);
for tt = 1:length(mlInds)
    mLens(tt) = length(mlInds{tt});
end
varargout{1} = mlInds;
varargout{2} = dsVecs;
varargout{3} = zerInds;
varargout{4} = mLens;

end

function mlInds = GetMidlineByThinning(img,varargin)
%GetMidlineByThinnng Gets midline of fish in input image by thinning
%   process
% mlInds = GetMidlinesByThinning(img);
% mlInds = GetMidlinesByThinning(img,'zThr',zThr,'mu',mu,'sigma',sigma,...
%   'minPxls',minPxls,'fishLen',fishLen,'fishPos',fishPos,'nIter',nIter);
% Inputs:
% img - Image with fish in it
% 'zThr' - Threshold for image binarization after expressing pixel
%   intensities as z-score
% 'nThr' - Number of threshold levels
% 'minThr' - Minimum value for threshold
% 'fishPos' - Position of fish, so as to exlude midlines that are too far.
% 'mu' - Mean to subtract when computing z-scores. If not specified, mu is
%   computed for input image, but when available it is better to use the mu
%   computed for the entire image stack.
% 'sigma' - Standard deviation to use for computign z-scores. If not
%   specified, it is computed.
% 'minPxls' - Min number of presumed pxls for midline. Default - 40
% 'fishLen' - Approx fish length in mm. Used to select midlines. Default - 10


for in = 1:numel(varargin)
    if ischar(varargin{in})
        switch lower(varargin{in})
            case 'nthr'
                nThr = varargin{in+1};
            case 'minthr'
                minThr = varargin{in+1};
            case 'fishpos'
                fishPos = varargin{in+1};
            case 'mu'
                mu = varargin{in+1};
            case 'sigma'
                sigma = varargin{in+1};
            case 'minpxls'
                minPxls = varargin{in+1};
            case 'fishlen'
                fishLen = varargin{in+1};
            case 'pxllen'
                pxlLen = varargin{in+1};
        end
    end
end
[mlInds,img_denoised] = GetMLBT2(img,nThr,minThr,minPxls,fishLen,fishPos,pxlLen);


    function varargout = GetMLBT2(img,nThr,minThr,minPxls,fishLen,fishPos,pxlLen)
        %         S = @(v1,v2)(sqrt(sum((v1-v2).^2,2)));
        S2 = @(v1,v2)sqrt(sum((repmat(v1,size(v2,1),1)-v2).^2,2));
        img_bw = zeros(size(img));
        [thr,img_quant] = GetMultiThr(img,nThr,'minThr',minThr);
        thr(thr<=minThr)=[];
        thr(thr==0)=[];
        mthr = multithresh(img,10);
        nPxls = zeros(size(mthr));
        for tt = 1:length(mthr)
            blah = img>=mthr(tt);
            nPxls(tt) = sum(blah(:));
        end
        nPxls_norm = abs(diff(nPxls))./abs(diff(mthr));
        nPxls_norm = nPxls_norm/std(nPxls_norm);
        img_denoised = img;
        oneInds = [];
        lvls = unique(img_quant(:));
        lvls(lvls==0)=[];
        count = 0;
        lvls = sort(lvls,'descend');
        rp_prev = cell(numel(lvls),1);
        flds = {'Perimeter','PixelIdxList','BoundingBox'};
        flds_scalar = {'Perimeter','BoundingBox'};
        imgDims = size(img);
        fpInd = sub2ind(imgDims,round(fishPos(2)), round(fishPos(1)));
        rp_thin = cell(size(lvls));
        for lvl = lvls(:)'
            count = count +1;
            zerImg = img_quant >= lvl;
            zerImg = imfill(zerImg,'holes');
            blobs = bwconncomp(zerImg);
            
            rp = regionprops(blobs,'Area','Centroid','Perimeter','PixelIdxList',...
                'Extent','EquivDiameter','BoundingBox');            
            delInds = [];
            for blob = 1:blobs.NumObjects;
                [rInds,cInds] = ind2sub(size(img),rp(blob).PixelIdxList);
                dists = S2(fishPos,[cInds,rInds]);
                rp(blob).BoundingBox = prod(rp(blob).BoundingBox(end-1:end));
                if (isempty(intersect(rp(blob).PixelIdxList,fpInd)) && isempty(dists(dists<=1)))
                    delInds = [delInds,blob]; % Mark regions that don't contain fish head pos for deletion
                end
            end
            ind_blob_keep = setdiff(1:blobs.NumObjects,delInds);
            
            % Thin blobs to obtain possible midlines and make decision
            % later
            if isempty(ind_blob_keep)
                if lvl ==1
                    disp('No fish/blob found!')
                end
                %                 emptyLvlInds =[emptyLvlInds,lvl];
            else
                rp_prev{lvl} = rp(ind_blob_keep);
                img_blob = zerImg*0;
                for rr = 1:length(rp_prev{lvl})
                    img_blob(rp_prev{lvl}(rr).PixelIdxList) = 1;
                end
%                 img_blob = imfill(img_blob,'holes');
                %                 img_thin = bwmorph(img_blob,'thin',Inf);
                img_thin = bwmorph(bwmorph(img_blob,'thin',Inf),'spur');
                rp_thin{count} = regionprops(bwconncomp(img_thin),flds);
                rp_thin{count}.BoundingBox = prod(rp_thin{count}.BoundingBox(end-1:end));
                
                % Eliminate possible midlines on the basis that they must be
                % larger than previous ones
                %                 if count > 1
                %                     remInds = zeros(length(rp_thin),1);
                %                     for bb = 1:length(rp_thin{count})
                %                         if ~isempty(rp_thin{count}) && ~isempty(rp_thin{count-1})
                %                                 if rp_thin{count}(bb).Area < rp_thin{count-1}.Area;
                %                                     remInds(bb)=1;
                %                                 end
                %                         end
                %                     end
                %                     rp_thin{count}(find(remInds))=[];
                %                 end
                
                % Keep the largest of the possible midlines
                if length(rp_thin{count})>1
                    areas = zeros(length(rp_thin{count}));
                    for bb = 1:length(rp_thin{count})
                        areas(bb) = rp(bb).Area;
                    end
                    [~,keepInd] = max(areas);
                    rp_thin = rp_thin(keepInd);
                end
            end
        end
        % Remove empty levels from rp_thin
        emptyInds = zeros(size(lvls));
        nanInds = zeros(size(flds));
        propMat = nan(length(flds_scalar),length(rp_thin));
        propMat2 = propMat;
        rp_prev = flipud(rp_prev);
        for lvl = 1:length(rp_thin)
            if isempty(rp_thin{lvl})
                emptyInds(lvl) =1;
            else
                for ff = 1:length(flds_scalar)
                    if ~isempty(rp_thin{lvl})
                        val = rp_thin{lvl}.(flds_scalar{ff});
                    end
                    if ~isempty(rp_prev{lvl})
                        val2 = rp_prev{lvl}.(flds_scalar{ff});
                    end
                    if isscalar(val)
                        propMat(ff,lvl) = val;
                        propMat2(ff,lvl) = val2;
                    else
                        nanInds(ff) = 1;
                    end
                end
            end
        end
        %         propMat2 = fliplr(propMat2);
        emptyInds = find(emptyInds);
        nanInds = find(nanInds);
        rp_thin(emptyInds)=[];
        rp_prev(emptyInds)=[];
        propMat(:,emptyInds)=[];
        propMat(nanInds,:)=[];
        propMat2(:,emptyInds) = [];
        propMat2(nanInds,:) = [];
        if size(propMat,2) >1
            %             firstMat = repmat(propMat(:,1),[1,size(propMat,2)-1]);
            firstMat  = repmat(median(propMat,2),[1,size(propMat,2)-1]);
            propMat = abs(diff(propMat,[],2))./firstMat;
            %             firstMat = repmat(propMat2(:,1),[1,size(propMat2,2)-1]);
            firstMat  = repmat(median(propMat2,2),[1,size(propMat2,2)-1]);
            propMat2 = abs(diff(propMat2,[],2))./firstMat;
            gm = (prod(propMat,1)).^(1/size(propMat,1));
            gm2 = (prod(propMat2,1)).^(1/size(propMat2,1));
            %             delInds = find(gm>=2 & gm2>=2)+1;
            delInds = find((gm*0.5 + gm2*0.5)>=2)+1;
            rp_thin(delInds)=[];
        end
        img_bw = img_quant*0;
        lvls_keep = sort(lvls,'ascend');
        lvls_keep(delInds) = [];
        img_bw(img_quant >= min(lvls_keep))=1;       
%       img_bw(img_bw~=0)=1;
        rp = regionprops(bwconncomp(img_bw),'Area','PixelIdxList');
        [~,maxInd] = max([rp(:).Area]);
        rp = rp(maxInd);
        img_bw = img_bw*0;
        img_bw(rp.PixelIdxList)=1;
%         img_bw = imfill(img_bw,'holes');
        img_thin = bwmorph(bwmorph(img_bw,'thin',Inf),'spur');
        mlInds = find(img_thin);
        if length(mlInds) < 20
            delInds = delInds+1;
            delInds(delInds < 1)=[];
            img_bw = img_quant*0;
            lvls_keep = lvls;
            lvls_keep(delInds:end) = [];
            img_bw(img_quant >= min(lvls_keep))=1;
            %       img_bw(img_bw~=0)=1;
            rp = regionprops(bwconncomp(img_bw),'Area','PixelIdxList');
            [~,maxInd] = max([rp(:).Area]);
            rp = rp(maxInd);
            img_bw = img_bw*0;
            img_bw(rp.PixelIdxList)=1;
            %         img_bw = imfill(img_bw,'holes');
            img_thin = bwmorph(bwmorph(img_bw,'thin',Inf),'spur');
            mlInds = find(img_thin);
        elseif length(mlInds) > 70
            delInds = delInds-1;
            delInds(delInds < 1)=[];
            img_bw = img_quant*0;
            lvls_keep = lvls;
            lvls_keep(delInds:end) = [];
            img_bw(img_quant >= min(lvls_keep))=1;
            %       img_bw(img_bw~=0)=1;
            rp = regionprops(bwconncomp(img_bw),'Area','PixelIdxList');
            [~,maxInd] = max([rp(:).Area]);
            rp = rp(maxInd);
            img_bw = img_bw*0;
            img_bw(rp.PixelIdxList)=1;
            %         img_bw = imfill(img_bw,'holes');
            img_thin = bwmorph(bwmorph(img_bw,'thin',Inf),'spur');
            mlInds = find(img_thin);
        end
        varargout{1} = mlInds;
        varargout{2} = img_denoised;
    end
end

function [mlInds, distVec] = GetMidlineCaudalToFishPos(fp,mlInds,imgDims)
[mlCoords(:,2),mlCoords(:,1)] = ind2sub(imgDims,mlInds);

%## Find midline pts rostral to head centroid pos and remove
[mlCoords_ord,s_ord] = IncreasinglyDistantPtsFromRef(fp,mlCoords);
[~,jumpInd] = max(s_ord);
if jumpInd > length(s_ord)-jumpInd
    mlCoords_ord(jumpInd:end,:)=[];
    s_ord(jumpInd:end) = [];
else
    mlCoords_ord(1:jumpInd-1,:)=[];
    s_ord(1:jumpInd-1) = [];
end
%###
if ~isempty(mlCoords_ord)
    mlCoords_ord = [round(fp); mlCoords_ord];
    s_ord = [0; s_ord];
end

mlInds = sub2ind(imgDims,mlCoords_ord(:,2),mlCoords_ord(:,1));
distVec = cumsum(s_ord);

end

function [pts_order,s_order] = IncreasinglyDistantPtsFromRef(refPt,pts)
S = @(v,V) sqrt(sum((repmat(v,size(V,1),1)-V).^2,2));
pts_new = pts;
s = S(refPt,pts_new);
s_new = s;
count= 0;
pts_order = zeros(size(pts));
s_order = zeros(size(s));
while length(s_new)>1
    count = count + 1;
    [d, ind] = min(s_new);
    s_order(count)=d;
    pts_order(count,:) = pts_new(ind,:);
    refPt = pts_new(ind,:);
    %     s_new(ind)=[];
    pts_new(ind,:) = [];
    s_new = S(refPt,pts_new);
end
zerInds = find(pts_order(:,1)==0 | pts_order(:,2)==0);
pts_order(zerInds,:) = [];
s_order(zerInds) = [];
end

function mlInds = CorrectOrder(mlInds,imgDims)
for n = 3:length(mlInds)
    clear sub1 sub2
    [sub1(:,2),sub1(:,1)] = ind2sub(imgDims(1:2), mlInds{n-1});
    [sub2(:,2),sub2(:,1)] = ind2sub(imgDims(1:2),mlInds{n});
    len = min([size(sub1,1), size(sub2,1)]);
    a1 = GetDiffAngles(sub1);
    a2 = GetDiffAngles(sub2);
    if len~=0
        d = sum(sqrt(sum((sub1(1:len,:)- sub2(1:len,:)).^2,2)),1);
        d_flip = sum(sqrt(sum((sub1(1:len,:)- sub2(len:-1:1,:)).^2,2)),1);cla
        if (d_flip < d) || ((a1*a2 <0) && (abs(a1-a2)> abs(a1--a2))) && (abs(a1-a2)>360)
            mlInds{n} = flipud(mlInds{n});
            disp(['Corrected frame # ' num2str(n)])
        end
    end
end

    function A = GetDiffAngles(vec)
        dV = diff(vec,[],1);
        C = dV(:,1) + dV(:,2)*i;
        A = angle(C(1:end-1).*conj(C(2:end)));
        A = sum(A)*180/pi;
    end
end

