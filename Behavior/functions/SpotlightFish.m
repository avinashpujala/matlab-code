
function spotlightImg = SpotlightFish(im,varargin)
% SpotlightFish - Returns an image where pixels outside a radius from fish
%   head centroid are set to 0
% spotLightImg = SpotlightFish(im)
% spotLightImg = SpotlightFish(im,fishPos)
% spotLightImg = SpotlightFish(...,radius)
% Inputs:
% im - Image containing fish
% fishPos - x,y coordinates of head centroid. If not specified or [], then
%   automatically determines fish pos
% radius - Spotlight radius (default = 70)
% func = 'sin', 'gauss' or []. Name of function with which to modulate the
%   spotlight on the fish. If empty, then does not modulate.
% 
% Avinash Pujala, HHMI, 2016

radius = 70;
func = [];

if nargin ==1
    fishPos = GetFishPos(im,30);
elseif nargin ==2
    fishPos = varargin{1};
elseif nargin == 3
    fishPos = varargin{1};
    radius = varargin{2};
elseif nargin ==4
     fishPos = varargin{1};
    radius = varargin{2};
    func = varargin{3};
end
if isempty(fishPos)
    fishPos = GetFishPos(im,30);
end
[row,col] = find(ones(size(im)));
S = sqrt(sum([col-fishPos(1) row-fishPos(2)].^2,2));
outPxls = find(S>=radius);

if ~isempty(func)
    im_delta = zeros(size(im));
    [r,c] = deal(round(fishPos(2)),round(fishPos(1)));
    r(r==0) = 1;
    c(c==0) = 1;
    r(r>size(im,1)) = size(im,1);
    c(c>size(im,2)) = size(im,2);
    try
    im_delta(r,c) =1;
    catch
        a = 1;
    end
    switch lower(func)
        case 'gauss'
            ker = gausswin(radius)*gausswin(radius)';             
        case 'sin'
            t = linspace(0,1,ceil(radius));
            ker = sin(2*pi*0.5*t);
            ker = ker(:)*ker(:)';            
    end
    ker = ker/sum(ker(:));
    im_conv = conv2(im_delta,ker,'same');   
    spotlightImg = im.*im_conv;
else
    spotlightImg = im;
    spotlightImg(outPxls)=0;
end


end