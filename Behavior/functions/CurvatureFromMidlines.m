function varargout = CurvatureFromMidlines(midlines)
%CurvatureFromMidlines Given midlines extracted by
%   GetMidlinesByThinning_new, returns curvatures, tangents, etc
% kappa = CurvatureFromMidlines(midlines);

T = zeros(size(midlines));
kappa = zeros(size(midlines,1),size(midlines,3));
for jj = 1:size(midlines,3);
    curv = midlines(:,:,jj);
    [t,~,~,k,~] = frenet(curv(:,1),curv(:,2));
    T(:,:,jj) = T(:,1:2);
    kappa(:,jj) = k;
end

varargout{1} = kappa;

end

