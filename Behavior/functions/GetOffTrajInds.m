function varargout = GetOffTrajInds(fp,imgDims,varargin)
%GetOffTrajInds From the trajectory of the fish, estimates the points that
%   deviate from a smooth spatial trajectory and returns their indices
%   The algorithm assumes that given the sufficiently fast frame rate the
%   fish swims in smooth trajectories and that any jump-like
%   discontinuities result from noise in the images.
% offTrajInds = GetOffTrajInds(fp,imgDims);
% offTrajInds = GetOffTrajInds(fp,imgDims,kerSize);
% [offTrajInds,thetas,dists, offTrajInds2,fp_interp] = GetOffTrajInds(...)
% Inputs:
% fp - N-by-2 matrix giving fish trajectory, where N is the number of spatial points in
%   the trajectory, fp(:,1) is the x-positions and fp(:,2) is the y-positions
% imgDims - M-by-N matrix giving dimensions of the images.
% kerSize - A scalar specifying the size of the gaussian kernel used to
%   smooth the fish trajectory. Default = 20;
% Outputs:
% offTrajInds - Indices of points estimated not to be part of the trajectory
% thetas - Angles of all the vectors from centroid of trajectory to each
%   point on the trajectory
% dists - Distances from the centroid to each point on the trajectory.
% offTrajInds2 - Off trajectory inds esimated with AND condition for thetas
%   and dists
% fp_interp - Spline interpolated trajectory after removing off trajectory
%   points
% 
% Avinash Pujala, JRC, 2017

kerSize = 20;
thr = 2;
if nargin >2
    kerSize = varargin{1};
end

ker = gausswin(kerSize)*gausswin(kerSize)';

blah = zeros(imgDims);
for jj = 1:size(fp,1)
    blah(fp(jj,2),fp(jj,1))=1;
end
blah_conv = conv2(blah,ker,'same');
[rInds,cInds] = find(blah_conv);
wts = SparseIndex(blah_conv,rInds,cInds);
wts = wts/sum(wts);
traj_com = round([sum(rInds.*wts), sum(cInds.*wts)]);
vecs =  repmat(fliplr(traj_com),size(fp,1),1)-fp;
thetas = unwrap(angle(vecs(:,1) + vecs(:,2)*1i));

dTh = zscore(diff(thetas));
offTrajInds_on = find(dTh>=thr);
offTrajInds_off = find(dTh<=-thr);
if min(offTrajInds_on)>min(offTrajInds_off)
    temp = offTrajInds_on;
    offTrajInds_on = offTrajInds_off;
    offTrajInds_off = temp;
end

moreInds_th = [];
for jj = 1:numel(offTrajInds_on)
    startInd = offTrajInds_on(jj);
    stopInd = min(offTrajInds_off(offTrajInds_off > startInd));
    if ~isempty(stopInd)
        inds = startInd:stopInd;
        moreInds_th = union(moreInds_th,inds);
    end
end


dists = nan(size(thetas));
for jj = 1:size(vecs,1)
    dists(jj) = norm(vecs(jj,:));
end

dS = zscore(diff(dists));
offTrajInds_on = find(dS >= thr);
offTrajInds_off = find(dS <=-thr);
if min(offTrajInds_on)>min(offTrajInds_off)
    temp = offTrajInds_on;
    offTrajInds_on = offTrajInds_off;
    offTrajInds_off = temp;
end
moreInds_s = [];
for jj = 1:numel(offTrajInds_on)
    startInd = offTrajInds_on(jj);
    stopInd = min(offTrajInds_off(offTrajInds_off > startInd));
    if ~isempty(stopInd)
        inds = startInd:stopInd;
        moreInds_s = union(moreInds_s,inds);
    end
end

offTrajInds = union(moreInds_th,moreInds_s);
offTrajInds2 = intersect(moreInds_th,moreInds_s);

tt = 1:size(fp,1);
t = tt;
t(offTrajInds)=[];
x = fp(:,1);
x(offTrajInds)=[];
y = fp(:,2);
y(offTrajInds)=[];

xx = round(spline(t,x,tt));
yy = round(spline(t,y,tt));

varargout{1} = offTrajInds;
varargout{2} = thetas;
varargout{3} = dists;
varargout{4} = offTrajInds2;
varargout{5} = [xx(:),yy(:)];

end

