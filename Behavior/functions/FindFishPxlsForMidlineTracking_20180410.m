function varargout = FindFishPxlsForMidlineTracking_20180410(I,varargin)
%FindFishPxlsForMidlineTracking Given an image stack and a few optional
%   parameters, returns binary images with fish pixels set to 1. Assumes
%   that the fish head position is in the middle of the image for each
%   image. Use CropImages.m to create such head-centered images.
% fishPxls = FindFishPxlsForMidlineTracking(I);
% fishPxls = FindFishPxlsForMidlineTracking(I,'headDiam',headDiam);
% Inputs:
% I - Image stack in which to find fish pixels. Size = [M, N, T], where T
%   is # of time points
% Optional inputs:
% headDiam - Approximate diameter of the head in mm (Default = 1). This determines
%   the size of the gaussian kernel used for smoothing, and later for thickening
%   connections between fragmented blobs and filling holes in fish.
% Outputs:
% I_fish - Binary image stack with same size as I with fish pixels set to 1.
%
% Avinash Pujala, Koyama lab/JRC, 2018

%=== Default values
headDiam = 1; % In mm
pxlLen = 0.05; % Approximate length of pxl. This is what it usually ends up being for my images.
plotBool = 0;
minExpectedFishLen = 3; % In mm
nWorkers = 10;

for jj  = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'pxllen'
                pxlLen = varargin{jj+1};
        end
    end
end

p = gcp('nocreate');
if isempty(p)
    p = parpool(nWorkers);
end

headDiam_pxl = round(headDiam/pxlLen);
ker = gausswin(ceil(headDiam_pxl/2));
ker = (ker(:)*ker(:)');
ker = ker./sum(ker(:));

% I(I<0)=0;
I_fish = I*0;
params.head_len = headDiam_pxl;
imgInds = 1:size(I,3);
nImages = numel(imgInds);
dispChunk = round(numel(imgInds)/10);
parfor jj = imgInds(:)'
    [~,~,I_fish(:,:,jj)] = FitBoxTrainToFish_20180410(I(:,:,jj),params);
    if mod(jj,dispChunk)==0
        disp(['Img # ' num2str(jj) '/' num2str(nImages)])
    end
end

I_fish = convn(I_fish,ker,'same');


varargout{1} = I_fish;

end


function [boxTrainImg,boxInds] = GetBoxTrainFit(fishImg,headLen,varargin)
% Approximate fit to image with fragmented fish blobs using a box-train
% fish model
% [boxTrainImg, boxInds] = GetBoxTrainFitI(fishImg,headLen);
% [boxTrainImg, boxInds] = GetBoxTrainFitI(fishImg,headLen,method);
% X - Input parameters to optimization func. X =
%   [head_x,head_y,head_angle,trunk_angle,tail_angle];
% Inputs:
% fishImg - Img with fish
% headLen - Approx head length
% solver - 'gs' or patternsearch

solver = varargin{1};

rng default % For reproducibility
headPos = ceil(size(fishImg)/2);
Aineq = [];
Bineq =[];
Aeq =[];
Beq =[];
lb = [headPos(1)-headLen headPos(2)-headLen 0 -60 -60];
ub = [headPos(1)+headLen headPos(2)+headLen 360 60 60];
nonlcon =[];
X0(1:2) = headPos;
X0(3:5) = deal(0);

if strcmpi(solver,'gs')
    objectiveFunc = @(X)FitBoxTrainToFish(X,headLen,fishImg);
    opts = optimoptions(@fmincon,'Algorithm','interior-point');
    problem = createOptimProblem('fmincon','objective',objectiveFunc,...
        'x0',X0,'lb',lb,'ub',ub,'options',opts);
    gs = GlobalSearch;
    gs.NumTrialPoints = 1000;
    X_opts = cell(3,1);
    fval = zeros(3,1);
    for jj = 1:3
        [X_opts{jj},fval(jj)] = run(gs,problem);
    end
elseif strcmpi(solver,'patternsearch')
    opts = [];
    objectiveFunc = @(X)FitBoxTrainToFish_gravity(X,headLen,fishImg);
    X_opts = cell(2,1);
    fval = zeros(2,1);
    for jj = 1:2
        [X_opts{jj},fval(jj)] = patternsearch(objectiveFunc,X0, Aineq,Bineq,Aeq,Beq,lb,ub,nonlcon,opts);
    end
end
[~,bestSol] = min(fval);
X_opts = X_opts{bestSol};
params.head_trans = X_opts(1:2);
params.head_rot = X_opts(3);
params.trunk_rot = X_opts(4);
params.tail_rot = X_opts(5);
params.head_len = headLen;

[~,boxPts] = MakeBoxTrainFish(params);

boxPts = round(boxPts);
boxInds = sub2ind(size(fishImg),boxPts(:,2),boxPts(:,1));

boxTrainImg = fishImg*0;
boxTrainImg(boxInds) = 1;

end

