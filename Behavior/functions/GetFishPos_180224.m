function varargout = GetFishPos180224(IM,varargin)
%GetFishPos Give an image stack returns the x,y coordinates of the fish in
%   each of the images of the stack
% fishPos = GetFishPos(IM);
% fishpos = GetFishPos(IM,nPixels)
% fishPos = GetFishPos(IM, nPixels,...)
% [fishPos,headOrientationVector] = GetFishPos(IM,nPixels,...);
% [fishPos,headOr]
% Inputs:
% IM - Image stack
% Optional input arguments
% 'filter' - Bandpass values, e.g., [15 25]; If bp is a scalar then creates
%   a gaussian kernel of this size and uses this to convolve the image
%   instead of using 'gaussianbpf'
% 'process' - 'serial' or 'parallel'; Process in serial or parallel
% thr_error - Threshold that determines off trajectory points
% smoothness - The smoothness of the B-spline fit to the fish trajectory
%   when determining off trajectory inds.
% 'tryCorrect' - Boolean; If true, tries to correct the erroneous fish
%    positions based on the assumption that these can jump around from one
%    frame to the next.
%
% Outputs:
% fishPos - x, y coordinates of fish head position
%
% Avinash Pujala, HHMI, 2016

process = 'serial';
plotBool = 1;
poolSize = 10;
tryCorrect = false;
kerSize = 25;
thr_dist = 20; % # pixels fish can cover from one frame to the next
% (~ 300mm/sec with my recording conditions)
thr_error =5;
smoothness = 6;

nArgs = length(varargin);
for jj = 1:nArgs
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'process'
                process =  varargin{jj+1};
            case 'kersize'
                kerSize = varargin{jj+1};
            case lower('pxlLim')
                pxlLim = varargin{jj+1};
            case 'plotbool'
                plotBool = varargin{jj+1};
            case 'trycorrect'
                tryCorrect = varargin{jj+1};
            case 'thr_dist'
                thr_dist = varargin{jj+1};
            case 'thr_error'
                thr_error = varargin{jj+1};
            case 'smoothness'
                smoothness = varargin{jj+1};
        end
    end
end

dispChunk = round(size(IM,3)/50)+1;
fishPos = zeros(size(IM,3),2);
tic

ker1 = MakeGaussian(3*kerSize,0,0.5);
ker2 = MakeGaussian(3*kerSize,0,3);
ker = ker1-ker2;
ker = ker(:)*ker(:)';
disp('Tracking fish...')
if strcmpi(process,'serial')
    for jj=1:size(IM,3)
        [fishPos(jj,:),img] = getfp(IM(:,:,jj),ker);
        if mod(jj,dispChunk)==0
            disp(['Img # ' num2str(jj)])
        end
        if plotBool
            ShowFishPos(img,[fishPos(jj,2),fishPos(jj,1)],jj)
        end
    end
elseif strcmpi(process, 'parallel')
    imgFrames = 1:size(IM,3);
    if matlabpool('size')==0
        matlabpool(poolSize)
    end
    parfor jj=imgFrames
        [fishPos(jj,:),img] = getfp(IM(:,:,jj),ker);
        if mod(jj,dispChunk)==0
            disp(['Img # ' num2str(jj)])
        end
    end
end

if tryCorrect
    try
    [fishPos,inds_corr] = JumpCorrectFishPos(fishPos,IM,thr_dist,thr_error,smoothness,ker);
    catch
         [fishPos,inds_corr] = JumpCorrectFishPos(fishPos,IM,thr_dist,thr_error,smoothness,ker);
    end
end

toc
varargout{1} = fishPos;
% varargout{2} = hOr_corr;


end

function [fishPos, inds_corr] = JumpCorrectFishPos(fishPos,IM,thr_dist,thr_error,smoothness,ker)
imgDims = size(IM);
thr_dist = ceil(min([min(imgDims(1:2))/6, thr_dist]));
offTrajInds = GetOffTrajInds_180224(fishPos,thr_error,smoothness);
onTrajInds = setdiff(1:size(fishPos,1),offTrajInds);
inds_corr = [];
if ~ isempty(offTrajInds)
    disp('Propagating changes...')
    for pk = offTrajInds(:)'
        startInd = onTrajInds(onTrajInds< pk);
        if isempty(startInd)
            startInd = pk;
            stopInd = min(onTrajInds(onTrajInds > pk));
            inds_start_stop = setdiff(startInd:stopInd,inds_corr);
            inds_start_stop = sort(inds_start_stop,'descend');
            if ~isempty(inds_start_stop)
                disp('Backward...')
                inds_corr = union(inds_corr,inds_start_stop);
                disp([num2str(numel(inds_start_stop)) ' frames...'])
            end
            for jj = inds_start_stop(:)'
                if jj == size(IM,3)
                    IM(:,:,jj) = SpotlightFish(IM(:,:,jj),fishPos(jj-1,:),thr_dist*2,'gauss');
                    fishPos(jj,:) = getpf(IM(:,:,jj),ker);
                else
                    IM(:,:,jj+1) = SpotlightFish(IM(:,:,jj+1),fishPos(jj,:),thr_dist*2,'gauss');
                    fishPos(jj+1,:) = getfp(IM(:,:,jj+1), ker);
                end
            end
        else
            startInd = max(startInd);
            stopInd = onTrajInds(onTrajInds > pk);
            if isempty(stopInd)
                stopInd = pk;
            else
                stopInd = min(stopInd);
            end
            inds_start_stop = startInd:stopInd;
            inds_start_stop = setdiff(inds_start_stop,inds_corr);
            if ~isempty(inds_start_stop)
                disp('Forwards...')
                inds_corr = union(inds_corr,inds_start_stop);
                disp([num2str(numel(inds_start_stop)) ' frames...'])
            end
            for jj = inds_start_stop(:)'
                if jj == size(IM,3)
                    IM(:,:,jj) = SpotlightFish(IM(:,:,jj),fishPos(jj-1,:),thr_dist*2,'gauss');
                    fishPos(jj,:) = getfp(IM(:,:,jj),ker);
                else
                    IM(:,:,jj+1) = SpotlightFish(IM(:,:,jj+1),fishPos(jj,:),thr_dist*2,'gauss');
                    fishPos(jj+1,:) = getfp(IM(:,:,jj+1),ker);
                end
            end
        end
    end
else
    disp('No erroneous jumps in fish position detected!')
end

end

function varargout = getfp(img,ker)
img = conv2(img,ker,'same');
[r,c] = FishPosInImg(img,round(size(ker,1)/3),'mean');
x = c;
y = r;
varargout{1} = [x,y];
varargout{2} = img;
end


function [r,c] = FishPosInImg(img,nPxls,method)
switch lower(method)
    case 'median'
        [~,maxInds] = sort(img(:),'descend');
        maxInds = maxInds(1:nPxls);
        [r,c] = ind2sub(size(img),maxInds);
        r = round(median(r));
        c = round(median(c));
    case 'mean'
        [~,maxInds] = sort(img(:),'descend');
        maxInds = maxInds(1:nPxls);
        [r,c] = ind2sub(size(img),maxInds);
        r = round(sum(r(:).*img(maxInds))/sum(img(maxInds)));
        c = round(sum(c(:).*img(maxInds))/sum(img(maxInds)));
    case 'blob'
        blah = img;
        blah(blah<0)=0;
        thr  = multithresh(blah,1);
        img_bw = img*0;
        img_bw(img>=thr)= 1;
        cc = bwconncomp(img_bw);
        rp = regionprops(cc,'Area','Centroid','ConvexArea',...
            'Eccentricity','Extent','FilledArea','MajorAxisLength','MinorAxisLength');
end


end

function ShowFishPos(img,fishPos,hOr,imgNum)
cla
if size(hOr,2)==1
    [y,x] = ind2sub(size(img),hOr);
else
    x = hOr(:,1);
    y = hOr(:,2);
end

imagesc(img), axis image, colormap(gray)
hold on
plot(fishPos(2), fishPos(1),'ro')
plot(x,y,'g.')
title(['Frame # ' num2str(imgNum)])
drawnow
shg
end