function theta = HeadOrientationAngleFromVec(hOr)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

theta = zeros(length(hOr),1);
for jj = 1:length(hOr)
    foo = hOr{jj}-repmat(hOr{jj}(1,:),[length(hOr{jj}),1]);
    theta(jj) = foo(end,1) + foo(end,2)*1i;
end
th1  = angle(theta(1))*180/pi;
dTh = angle(diff(theta,1))*180/pi;
dTh = cumsum(dTh);
dTh = dTh + th1;

end

