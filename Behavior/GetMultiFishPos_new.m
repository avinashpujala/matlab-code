function varargout = GetMultiFishPos_new(I, varargin)
% GetMultiFishPos - Function for finding fish and fixing position and orientation
% fp = GetMultiFishPos(I);
% fp = GetMultiFishPos(I,'pxlSize',pxlSize,'headDiam',headDiam, 'nFish',nFish);
% Inputs:
% I - Matrix of size [M, N, T], where T is number of time points or slices. This is the
%   image stack in which to find the fish
% 'pxlSize' - Scalar indicating pixel size [default = 0.06; Typically what it comes out to for
%       my recording conditions]
% 'headDiam' - Scalar indicating approx head diam [default = 1.5 mm]. Used
%   for finding spatial smoothing of images and finding fish.
% 'nFish' - Scalar indicating total # of fish in images. If nFish = -1,
%   then automatically estimates in one of the images, confirms with user
%   by making him/her draw ROIs around the fish and then fixes the # of
%   fish to this number for all images.
% 'lim_dist' - Scalar indicating distance limit between frames for fish swimming.
%
% Avinash Pujala

%% Some variables
pxlSize = 0.06; % In mm (~ for 896 x 896 images and 50mm diam arena)
headDiam = 1.5; % In mm (this can be a bit larger than real size)
nFish = -1; % If -1, then auto detects and seeks confirmation from user.
plotSingly = false; 
plotTogether = true;
lim_dist = 800; % In mm/sec.
frameRate = 500; 
process = 'parallel';
rp_given = [];
nWorkers = 10; % Matlab poolsize for parfor, etc


for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'headdiam'
                headDiam = varargin{jj+1};
            case 'pxlsize'
                pxlSize = varargin{jj+1};
            case 'nfish'
                nFish = varargin{jj+1};
            case 'plotsingly'
                plotSingly = varargin{jj+1};
            case 'plottogether'
                plotTogether = varargin{jj+1};
            case 'lim_dist'
                lim_dist = varargin{jj+1};
            case 'framerate'
                frameRate = varargin{jj+1};
            case 'process'
                process = vararging{jj+1};
            case 'regionprops'
                rp_given = varargin{jj+1};
        end
    end
end


kerSize = ceil(headDiam/pxlSize);
ker = gausswin(kerSize)*gausswin(kerSize)';
ker = ker/sum(ker(:));

iNum = ceil(size(I,3)/2);
if nFish ==-1
    ftl = 'Draw around fish to start tracking';
    fRois = SetFreehandRois(I(:,:,iNum),'clrMap',jet(256),'figTitle',ftl);
    nFish = length(fRois);
    disp([num2str(nFish) ' selected by ROIs'])
    allFishMask = zeros(size(I(:,:,iNum)));
    for ff = 1:length(fRois)
        allFishMask = (allFishMask | fRois{ff}.bw);
    end
    drewRois = true;
else
    drewRois = false;
end


%% Filter image stack
disp('Spatial filtering images for fish head detection...')
tic
imgDims = size(I);
I_flt = SmoothImageStack(I,kerSize,process);
toc

thr = multithresh(I_flt,2);
thr = thr(2);

I_bw = ones(imgDims(1:2));
I_bw(I_flt(:,:,iNum)<thr) = 0;

nFrames = size(I,3);
blobs = bwconncomp(I_bw);

count = 0;
countcount = 0;
alpha = 0.01;
nBlobs = length(blobs.PixelIdxList);
% Iteratively lower threshold if not enough blobs found
while (nBlobs < nFish) 
    disp('Lowering threshold to find the right number of fish...')
    thr = thr*(1-alpha);
    I_bw = ones(imgDims(1:2));
    I_bw(I_flt(:,:,iNum)<thr) = 0;
    blobs = bwconncomp(I_bw);
    nBlobs = length(blobs.PixelIdxList);   
    count = count + 1;
    if count > 1000
        disp([num2str(nFish-nBlobs) ' fewer fish found'])
        break
    end
end
rp = regionprops(blobs,'Centroid','Area','PixelIdxList','Perimeter','EquivDiameter');

% blobPos = cell(size(rp));
blobDelInds = zeros(1,length(rp));
overlap_frac = blobDelInds;
for blob = 1:length(rp)
    % Find blobs at least partially in fish ROIs (if drawn previously)
    if drewRois
        % The assumption here is that # blobs >= # rois (# fish)
        overlaps = intersect(blobs.PixelIdxList{blob},find(allFishMask));
        overlap_frac(blob) = length(overlaps)/length(rp(blob).PixelIdxList);
        if overlap_frac(blob) < 0.5;
            blobDelInds(blob) = 1;
        end
    end
end

blobDelInds = find(blobDelInds);
rp(blobDelInds) =[]; % Delete blobs that are not contained in fish ROIs
overlap_frac(blobDelInds) = [];

% If multiple blobs in single fish ROI, then keep that is most within
% fish ROI
if (drewRois && (length(rp)>length(fRois)))
    [~,sortInds] = sort(overlap_frac,'descend');
    rp(sortInds(length(fRois)+1:end)) =[];
end

% Arrange all the properties into matrices both for the given and the
%   found region props and then keep only the regionprops most similar to
%   given
if (~isempty(rp_given)) && (length(rp) > length(rp_given))  
    fNames_given = fieldnames(rp_given);
    R_given = nan(length(fNames_given),length(rp_given));
    R = nan(length(fNames_given),length(rp));
    delInds = zeros(length(fNames_given),1);
    for ff = 1:length(fNames_given)
        fName = fNames_given{ff};
        if isfield(rp,fName)
            for kk = 1:size(R_given,2)
                if isscalar(rp_given(kk).(fName));
                    R_given(ff,kk) = rp_given(kk).(fName);
                else
                    delInds(ff) = 1;
                end
            end
            for kk = 1:size(R,2)
                if isscalar(rp(kk).(fName))
                    R(ff,kk) = rp(kk).(fName);
                end
            end
        end
    end
    % Unitize the vectors prior to dot products
    delInds = find(delInds);
    R(delInds,:) = [];
    R_given(delInds,:)=[];
    for kk = 1:size(R_given,2);
        R_given(:,kk) = R_given(:,kk)/norm( R_given(:,kk));
    end
     for kk = 1:size(R,2);
        R(:,kk) = R(:,kk)/norm( R(:,kk));
     end
     % Mean of dot products with all given blobs to choose best matched
     % found blobs
     [~,sortInds] = sort(mean(R_given'*R,1),'descend');
     rp = rp(sortInds(1:nFish));
end

nFish = length(rp);
disp(['Tracking ' num2str(nFish)])
lim_dist = ceil(lim_dist/(frameRate*pxlSize));
fishPos = cell(nFish,1);
if strcmpi(process,'serial')
    for ff = 1:nFish        
        fishPos{ff} = GetSingleFishPos(I_flt,iNum,rp(ff),thr,'lim_dist',lim_dist,'nPxls',kerSize);        
    end
else
    fishInds = 1:nFish;
     parfor ff = fishInds;        
        fishPos{ff} = GetSingleFishPos(I_flt,iNum,rp(ff),thr,'lim_dist',lim_dist,'nPxls',kerSize);
    end
end

varargout{1} = fishPos;
varargout{2} = rp;

%% Utility functions
    function voteMat = DistMat2VoteMat(distMat)
        voteMat = nan(size(distMat));
        for col = 1:size(distMat,2)
            for row = 1:size(distMat,1)
                blah = distMat;
                blah(row,:)=[];
                blah(:,col)  =[];
                voteMat(row,col) = sum(blah(:));
            end
        end
    end

    function optInds = GetOptimalInds(distMat)
        [sorts,inds] = sort(distMat,'ascend');
        rowInds = []; colInds = [];
        if size(inds,2) > 1
            for c = 1:size(inds,2)-1
                sameInds = find(inds(1,:)==inds(1,c));
                if numel(sameInds)>1
                    addVec = 1:length(sameInds);
                    blah = nan(length(addVec),1);
                    temp = {};
                    for jj = 1:length(addVec)
                        addVec = circshift(addVec,[0,jj-1]);
                        temp{jj} = addVec;
                        blah(jj) = sum(SparseIndex(sorts,addVec,sameInds));
                    end
                    [~, mindInd] = min(blah);
                    rowInds = [rowInds,temp{mindInd}];
                    colInds = [colInds,sameInds];
                    optInds = SparseIndex(inds,rowInds,colInds);
                else
                    optInds = inds(1,:);
                end
            end
        else
            optInds= inds(1,1);
        end
    end

    function out = GetDistMat(m1,m2)
        S = @(x,y) sqrt(sum((x(1)-y(1)).^2 + (x(2)-y(2)).^2));
        out = nan(size(m1,1),size(m2,1));
        for one = 1:size(out,1)
            for two = 1:size(out,2)
                out(one,two) = S(m1(one,:), m2(two,:));
            end
        end
    end

    function IM_flt = BandpassStack(IM,lowerLim,upperLim)
        IM_flt = zeros(size(IM));
        for imNum = 1:size(IM,3)
            IM_flt(:,:,imNum) = Bandpass(IM(:,:,imNum),lowerLim,upperLim);
        end
    end

    function img_flt = Bandpass(img, lowerLim,  upperLim)
        getCtr = @(img)[round(size(img,1)/2+0.599), round(size(img,2)/2+0.599)];
        imgCtr = getCtr(img);
        r_in = lowerLim;
        r_out = upperLim;
        se_in = strel('disk',r_in);
        se_out = strel('disk',r_out);
        se_in_ctr = getCtr(se_in.getnhood);
        se_out_ctr = getCtr(se_out.getnhood);
        mask = zeros(size(img));
        inInds = se_in.getneighbors;
        inInds(:,1) = inInds(:,1) + imgCtr(1);%-se_in_ctr(1);
        inInds(:,2) = inInds(:,2) + imgCtr(2);%-se_in_ctr(2);
        inInds = sub2ind(size(img),inInds(:,1),inInds(:,2));
        outInds = se_out.getneighbors;
        outInds(:,1) = outInds(:,1) + imgCtr(1);%-se_out_ctr(1);
        outInds(:,2) = outInds(:,2) + imgCtr(2);%-se_out_ctr(2);
        outInds = sub2ind(size(img),outInds(:,1),outInds(:,2));
        mask(outInds) = 1;
        mask(inInds) =0;
        img_fft = fftshift(fft2(img));
        img_fft = img_fft.*mask;
        img_flt = ifft2(fftshift(img_fft));
    end

    function I_smooth = SmoothImageStack(I,kerSize,process)
        % ker = ones(10,10); ker = ker/sum(ker);
        ker = gausswin(kerSize);
        ker = ker(:)*ker(:)';
        ker = ker./sum(ker(:));
        I_smooth = zeros(size(I));
        if strcmpi(process,'serial')
        for jj = 1:size(I_smooth,3)
            I_smooth(:,:,jj) = conv2(I(:,:,jj),ker,'same');
        end
        else
            imgInds = 1:size(I_smooth,3);
            parfor jj = imgInds
                 I_smooth(:,:,jj) = conv2(I(:,:,jj),ker,'same');
            end
        end
    end



end

function fishPos = GetSingleFishPos(I,iNum,rp,thr, varargin)
% GetSingleFishPos - Given an image stack (I), and seed image
%    number (iNum) and fish position within it (fp), returns the fish
%    positions in all the other stacks.
% Inputs:
% I - 3D matrisx of size [M, N, T]. Image stack in which to find fish
%   positions
% iNum - Scalar specifying the number of image being used as seed
%   image
% rp - Region properties obtained using regionprops
% thr - Scalar indicating threshold for blob detection
% 'lim_dist' - A limit on the maximum distance a fish can travel between
%   frames, derived from max swim speed limit.
% 'nPxls' - Number of pixels to use for finding head centroid

lim_dist = 30;
nPxls = 25;

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'lim_dist'
                lim_dist = varargin{jj+1};
            case 'npxls'
                nPxls = varargin{jj+1};         
        end
    end
end

imgDims = size(I);
lim_dist = ceil(min([min(imgDims(1:2))/6, lim_dist]));
fishPos = nan(imgDims(3),2);
foo = round(rp.Centroid);

foo(1) = min([foo(1),imgDims(1)]);
foo(2) = min([foo(2),imgDims(2)]);

fishPos(iNum,:) = rp.Centroid;

% rp = rmfield(rp,'PixelIdxList');
% fldNames = fieldnames(rp);

disp('Detecting fish from seed on backwards...')
for jj = iNum:-1:2   
%     I(:,:,jj-1) = SpotlightFish(I(:,:,jj-1),fishPos(jj,:),lim_dist*2,'gauss');
    I(:,:,jj-1) = SpotlightFish(I(:,:,jj-1),fishPos(jj,:),lim_dist,[]);
    fishPos(jj-1,:) = FishPosInImg(I(:,:,jj-1),nPxls,'mean');
end

disp('Detecting fish from seed on forwards...')
for jj = iNum:imgDims(3)-1    
     I(:,:,jj+1) = SpotlightFish(I(:,:,jj+1),fishPos(jj,:),lim_dist,[]);
     fishPos(jj+1,:) = FishPosInImg(I(:,:,jj+1),nPxls,'mean');
end


end

function fp = FishPosInImg(img,nPxls,method)
switch lower(method)
    case 'median'
        [~,maxInds] = sort(img(:),'descend');
        maxInds = maxInds(1:nPxls);
        [r,c] = ind2sub(size(img),maxInds);
        r = round(median(r));
        c = round(median(c));
    case 'mean'
        [~,maxInds] = sort(img(:),'descend');
        maxInds = maxInds(1:nPxls);
        [r,c] = ind2sub(size(img),maxInds);
        r = round(sum(r(:).*img(maxInds))/sum(img(maxInds)));
        c = round(sum(c(:).*img(maxInds))/sum(img(maxInds)));
    case 'blob'
        blah = img;
        blah(blah<0)=0;
        thr  = multithresh(blah,1);
        img_bw = img*0;
        img_bw(img>=thr)= 1;
        cc = bwconncomp(img_bw);
        rp = regionprops(cc,'Area','Centroid','ConvexArea',...
            'Eccentricity','Extent','FilledArea','MajorAxisLength','MinorAxisLength');
end
fp = [c,r];

end

