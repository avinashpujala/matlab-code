function fitSum = FitBoxTrainToFish_gravity(X,head_len,fishImg)
%FitBoxTrainToImg Beta testing still
% box_train = FitBoxTrainToFish(params,fishImg);
% head_len = 40; % In pxls
% head_trans = [0 0]; % Default: origin
% head_rot =  0; 
% trunk_rot = 60;
% tail_rot = 60;
% plotBool = 1;

% fp = ceil(size(fishImg)/2);

params.head_trans = X(1:2);
params.head_rot = X(3);
params.trunk_rot = X(4);
params.tail_rot = X(5);
params.head_len = head_len;
box_train = MakeBoxTrainFish(params);

imgDims = size(fishImg);

[y,x] = find(fishImg);
fishPts = [x(:),y(:)];
box_pts = [];

img_box = zeros(size(fishImg));
for jj = 1:length(box_train)
    foo = box_train{jj};
    foo_round = round(foo);
    box_pts = cat(1,box_pts,foo_round);
    boxInds = sub2ind(size(img_box),foo_round(:,2),foo_round(:,1));
    img_box(boxInds) = length(box_train)+1-jj;
end

fishInds = sub2ind(size(fishImg),fishPts(:,2),fishPts(:,1));
boxInds = sub2ind(size(img_box),box_pts(:,2),box_pts(:,1));
[~,~,D] = FindClosestPoints(fishPts,box_pts);
M1 = repmat(fishImg(fishInds),[1, length(boxInds)]);
M2 = repmat(img_box(boxInds)',[length(fishInds),1]);
D = D+0.5;
F = -(1e-3)*(M1.*M2)./(D.^2);
fitSum = double(sum(F(:)));






% img_fish(fishInds) = 1;
% img_box(boxInds) = 1;
% 
% img_fish = bwdist(img_fish);
% % img_fish = img_fish-max(img_fish);
% % img_fish = img_fish.*fishImg;
% % img_box = bwdist(img_box);
% % res = (img_box-img_fish).^2;
% res = (img_box.*img_fish);
% 
% % res = (img_box-fishImg).^2;
% fitSum = sqrt(double(sum(res(:))));


end
