
function varargout = TailRes(tc)
% Given tail curves, obtains residual errors

res = zeros(size(tc,1),size(tc,3)-1);
res_sum = zeros(1,size(tc,3)-1);
for jj = 1:size(tc,3)-1
    res(:,jj) = sqrt(sum((tc(:,:,jj+1)-tc(:,:,jj)).^2,2));
    res_sum(jj) = sum(res(:,jj));
end

varargout{1} = res;
varargout{2} = res_sum;
end
