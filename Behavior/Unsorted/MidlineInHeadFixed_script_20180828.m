
%% 
imgDir = '\\Koyama-S1\Data2\Rig1\Data\Minoru\2017-01-13\behav\f1s2';
imgInds = 1:10e3;
nFramesInTrl = 8e3;
nBlocks = 4;
registerBool = 0;
pxlLen = 14/1e3; % MK told me that pxlsize = 14 microns
imgExt = 'bmp';
nImgsInBlock = 2500;
lineLens = [200, 125, 75];
F_s = 500;
F_resample = 300;

%% Counting images in directory
% procData = ProcessFishImages_180629({imgDir},'imgInds',imgInds,...
%     'nFramesInTrl',nFramesInTrl,'registerBool',registerBool, 'pxlLen',pxlLen);

disp('Getting file names...')
imgNamesInDir = GetFilenames(imgDir,'ext',imgExt);
totalNumImgsInDir = length(imgNamesInDir);

if totalNumImgsInDir >1000
    rp = randperm(totalNumImgsInDir);
    refFrames = rp(1:1000);
end

%% Background removal and fish position tracking in blocks of images
tic
poolObj = gcp('nocreate');
if isempty(poolObj)
    poolObj = parpool(10);
end

nTrls = totalNumImgsInDir/nFramesInTrl;
disp([num2str(nTrls) ' trls detected'])
imgInds_all = 1:totalNumImgsInDir;


disp('Getting image info..')
imgInfo = imfinfo(fullfile(imgDir,imgNamesInDir{1}));
imgDims = [imgInfo.Height imgInfo.Width];
imgDims = [imgDims, totalNumImgsInDir];
ts = datestr(now,30);

outDir = fullfile(imgDir,'proc');
if ~exist(outDir,'dir')
    mkdir(outDir)
end

fileInfo = dir([outDir '/*.mat']);
createBool = true;
dn = zeros(length(fileInfo),1);
if ~isempty(dn)    
    for jj = 1:length(fileInfo)
        dn(jj) = fileInfo(jj).datenum;
    end
    [dn,sortInds] = sort(dn,'descend');
    dn = dn(1);
    dn_now = now;
    dn_diff = (dn_now-dn)*(1e5)/(3600);    
    if dn_diff < 1 % 1 hr
        createBool =false;
    end
end
%---Compute background
disp(['Reading random sequence of ' num2str(numel(refFrames)) ' images, and computing background...'])
refFrames = [1; refFrames(:); totalNumImgsInDir];
refFrames = unique(refFrames);
I = ReadImgSequence(imgDir,[],[],imgNamesInDir(refFrames));
[~,ref_all] = SubtractBackground(I);

%% Getting crop indices
figure
imagesc(ref_all),axis image
title('Click on two points on fish: 1st - fixation, 2nd - tail tip')
[x,y] = ginput(2);
close

x_min = floor(min(x));
ref_crop = ref_all(:,1:x_min);

%% Getting pivot point

figure
imagesc(ref_crop),axis image
shg

blah = Standardize(sum(ref_crop,2));
[~,pivotInd] = max(blah);

pivotPos = [size(ref_crop,2),pivotInd];
hold on
plot(pivotPos(1), pivotPos(2),'ro')

%% Create procData...

outDir = fullfile(imgDir,'proc');
if exist(outDir)~=7
    mkdir(outDir);
end
ts = datestr(now,'ddhh');
procName = ['procData_' ts '.mat'];
disp(['Creating procData at ' outDir]);
procData = OpenMatFile(fullfile(outDir,procName));
procData.Properties.Writable = true;
procData.ref = ref_all;
procData.F_s = F_s;
procData.F_resample = F_resample;
imgInds_all = round(1:(F_s/F_resample):totalNumImgsInDir);
procData.imgInds_all = imgInds_all;
cLen = sum(lineLens);
procData.midlines = zeros(cLen,2,totalNumImgsInDir);
procData.midlines_fit = zeros(cLen,2,totalNumImgsInDir);
procData.curv = zeros(cLen,totalNumImgsInDir);

%%

imgInds_list = SubListsFromList(imgInds_all,nImgsInBlock);
imgInds_list_cont = SubListsFromList(1:numel(imgInds_all),nImgsInBlock);
tic
for block = 1:length(imgInds_list)
    imgInds = imgInds_list{block};
    imgInds_cont = imgInds_list_cont{block};
    disp(['Block # ' num2str(block) ', Reading images: ' ... 
        num2str(imgInds(1)) ' - ' num2str(imgInds(end))])
    
    I = ReadImgSequence(imgDir,[],[],imgNamesInDir(imgInds));
    
    disp('Cropping Images')
    I_crop = I(:,1:x_min,:);
    
    disp('Getting midlines...')
    % ml = GetMidlines(I_crop, pivotPos,[200, 150, 50],'procType','serial','plotBool',1);
    ml = GetMidlines(I_crop, pivotPos,lineLens,'procType','parallel');
    
    disp('Getting smooth midlines...')
    curv = zeros(cLen,2,length(ml));
    curv_fit = curv;
    for tt = 1:length(ml)
        foo = [];
        for seg = 1:length(ml{1})
            foo = [foo;ml{tt}{seg}(:)];
        end
        [c,r] = ind2sub(size(I_crop),foo);
        curv(:,1,tt) = c;
        curv(:,2,tt) = r;
        blah = FitBSplineToCurve(squeeze(curv(:,:,tt)),[],50);
        curv_fit(:,1,tt) = blah(:,1);
        curv_fit(:,2,tt) = blah(:,2);
    end
    
    disp('Getting tail tangents...')
    tc = GetTailTangents(curv_fit);
    
    disp('Updating procData...')
    procData.midlines(:,:,imgInds_cont) = curv;
    procData.midlines_fit(:,:,imgInds_cont) = curv_fit;
    procData.curv(:,imgInds_cont) = tc;
    
    toc
end


%%
tic
   disp('Filtering total body bend...')
    ta = chebfilt(tc(end,:),1/500,[0.01,60]);
toc

%% 


%% Test

figure
for jj = imgInds
    imagesc(I_crop(:,:,jj)),axis image
    colormap(gray)
    hold on
    plot(curv(:,2,jj),curv(:,1,jj),'r')
    shg
    pause()
end







