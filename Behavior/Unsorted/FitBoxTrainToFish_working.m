function fitSum = FitBoxTrainToFish_working(X,head_len,fishImg)
%FitBoxTrainToImg Beta testing still

params.head_trans = X(1:2);
params.head_rot = X(3);
params.trunk_rot = X(4);
params.tail_rot = X(5);
params.head_len = head_len;
box_train = MakeBoxTrainFish(params);

imgDims = size(fishImg);

[y,x] = find(fishImg);
fishPts = [x(:),y(:)];
box_pts = [];

for jj = 1:length(box_train)
    foo = box_train{jj};
    foo_round = round(foo);
    box_pts = cat(1,box_pts,foo_round);  
end

fishMins = min(fishPts);
boxMins = min(box_pts);

bothMins = min([fishMins;boxMins])-1;

fishPts = fishPts - repmat(bothMins,[size(fishPts,1),1]);
box_pts = box_pts - repmat(bothMins,[size(box_pts,1),1]);

maxPts = max([fishPts; box_pts]);

img_fish = zeros(maxPts(2),maxPts(1));
img_box = img_fish;
fishInds = sub2ind(size(img_fish),fishPts(:,2),fishPts(:,1));
boxInds = sub2ind(size(img_box),box_pts(:,2),box_pts(:,1));

img_fish(fishInds) = 1;
img_box(boxInds) = 1;
res = (img_box-img_fish).^2;
fitSum = sum(res(:));

end
