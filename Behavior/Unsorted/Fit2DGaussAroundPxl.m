

%% Inputs
eyeDiam = 0.5; % In mm
pxlLen = 0.05; % In mm

%%
kerSize = ceil(1.25*eyeDiam/pxlLen);
ker1 = MakeGaussian(kerSize,0,0.5);
ker2 = MakeGaussian(kerSize,0,4);
ker = ker1-ker2;
ker = ker(:)*ker(:)';

%% Smooth with eye-sized kernel
tic
I_smooth= convn(I,ker,'same');
toc

%%
ind = 200;
img = I_smooth(:,:,ind);
img_pks = imregionalmax(I_smooth(:,:,ind));
img_pks_int = img_pks.*img;
inds_pk = find(img_pks_int);
int_pk = img_pks_int(inds_pk);
[int_pk_sort,inds_sort] = sort(int_pk,'descend');
inds_pk = inds_pk(inds_sort(1:3));
int_pk = int_pk(inds_sort(1:3));

figure
imagesc(img),axis image, colormap(gray)
hold on
[r,c] = ind2sub(size(img),inds_pk);
plot(c,r,'k*','markersize',20)

%% Test my optimization with a made up image
img = zeros(size(I_smooth(:,:,1)));
midPt = ceil(size(img)/2);
% img = img + randn(size(img));
img(midPt(1),midPt(2)) = 100;

% kerSize = ceil(1.25*eyeDiam/pxlLen);
kerSize = 100;
ker1 = MakeGaussian(kerSize,0,1.9);
% ker2 = MakeGaussian(kerSize,0,4);
% ker = ker1-ker2;
ker = ker1(:)*ker1(:)';

img = conv2(img,ker,'smooth');


img_pks = imregionalmax(img);
img_pks_int = img_pks.*img;
inds_pk = find(img_pks_int);
int_pk = img_pks_int(inds_pk);
[int_pk_sort,inds_sort] = sort(int_pk,'descend');
inds_pk = inds_pk(inds_sort(1:3));
int_pk = int_pk(inds_sort(1:3));

figure
imagesc(img),axis image, colormap(fake_parula)
hold on
[r,c] = ind2sub(size(img),inds_pk);
plot(c,r,'k*','markersize',20)


%% Optimization
clc
rng default % For reproducibility
% Aineq = [1 0 0 0;-1 0 0 0];
% Bineq = [size(img,1) 0 ]';
Aineq = [];
Bineq =[];
Aeq =[];
Beq =[];
lb = 0.2;
ub = 8;
nonlcon =[];
opts =[];
% N = 100;
N = kerSize;
% N = ceil(size(img,1)/6);
% opts = optimset('PlotFcn', @psplotbestf);
sigma = nan(numel(inds_pk),1);
for jj = 1:numel(inds_pk)
    disp(jj)
    objectiveFunc = @(sigma)Objective_2DGauss(sigma,img,inds_pk(jj),N);
    X0 = lb + rand(size(lb)).*(ub-lb);
    sigma(jj) = patternsearch(objectiveFunc,X0, Aineq,Bineq,Aeq,Beq,lb,ub,nonlcon,opts);
end
disp(sigma)
% sigma_pxls = sigma*(N/6);
% sigma_mm = sigma*pxlLen;
% disp('Fit sigmas in pxls = ' )
% disp(sigma_pxls)
% disp('Fit sigmas in mm = ')
% disp(sigma_mm)

