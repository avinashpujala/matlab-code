function varargout = MakeBoxTrainFish(params)
%FitBoxTrainToImg Beta testing still
% box_train = FitBoxTrainToFish(params,fishImg);
% Inputs:
% params.
%   .head_len - Head segment length in pixels
%   .head_trans - Head translation coordinates, [x,y]
%   .head_rot - Head segment rotation angle (in degrees)
%   .trunk_rot - Trunk segment rotation angle w.r.t head segmnt
%   .tail_rot - Tail segment rotation angle, w.r.t. trunk segment
% head_len = 40; % In pxls
% head_trans = [0 0]; % Default: origin
% head_rot =  0; 
% trunk_rot = 60;
% tail_rot = 60;
plotBool = 0;

head_len = params.head_len;
head_trans = params.head_trans;
head_rot = params.head_rot;
trunk_rot = params.trunk_rot;
tail_rot = params.tail_rot;
if isempty(head_rot)
    head_rot =0;
end
if isempty(trunk_rot)
    trunk_rot = 0;
end
if isempty(tail_rot)
    tail_rot = 0;
end


%% Keeping things simple for the time being and fixing certain dimensions w.r.t head_len

head_wid = head_len/2; % In pxls

trunk_len = 1.2*head_len;
trunk_wid = head_wid/1.5;

tail_len = trunk_len;
tail_wid = trunk_wid/1.5;

%% Make head rectangle
head_rect = MakeRectangle(head_len,head_wid,[head_len/2,0],0);
head_rect = RotateAboutAPoint(head_rect,[0,0],head_rot);
head_rect = head_rect + repmat(head_trans,[size(head_rect,1),1]);

%% Make trunk rectangle
trunk_rect = MakeRectangle(trunk_len,trunk_wid,[trunk_len/2,0],0);
trunk_rot = head_rot + trunk_rot;
trunk_rect = RotateAboutAPoint(trunk_rect,[0,0],trunk_rot);
trunk_trans = head_trans + head_len*([cos(head_rot*pi/180),sin(head_rot*pi/180)]);
trunk_rect = trunk_rect + repmat(trunk_trans,[size(trunk_rect,1),1]);

%% Make tail rectangle
tail_rect = MakeRectangle(tail_len,tail_wid,[tail_len/2,0],0);
tail_rot =  trunk_rot + tail_rot;
tail_rect = RotateAboutAPoint(tail_rect,[0,0],tail_rot);
tail_trans = trunk_trans + trunk_len*([cos(trunk_rot*pi/180),sin(trunk_rot*pi/180)]);
tail_rect = tail_rect + repmat(tail_trans,[size(tail_rect,1),1]);

%% Plot, if specified
if plotBool
    figure
    plot(head_rect(:,1),head_rect(:,2),'o-')
    hold on
    plot(trunk_rect(:,1),trunk_rect(:,2),'o-')
    plot(tail_rect(:,1),tail_rect(:,2),'o-')
    axis image    
end

box_train = {head_rect,trunk_rect,tail_rect};
box_train_pts = [];
for jj = 1:length(box_train)
    box_train_pts = cat(1,box_train_pts,box_train{jj});
end
varargout{1} = box_train;
varargout{2} = box_train_pts;

end

function rect_rot_trans = MakeRectangle(len,wid,trans,rot)
x = -len/2:1:len/2;
y = -wid/2:1:wid/2;
[X,Y] = meshgrid(x,y);
x = X(:);
y = Y(:);
rect = [x(:),y(:)];
rect_rot = RotateTraj(rect,rot);
rect_rot_trans = rect_rot + repmat(trans,[size(rect_rot,1),1]);
end

function rot_pts = RotateAboutAPoint(pts,rot_pt,rot_angle)
rot_pts = pts;
rot_pts = rot_pts - repmat(rot_pt,[size(rot_pts,1),1]);
rot_pts = RotateTraj(rot_pts,rot_angle);
rot_pts = rot_pts + repmat(rot_pt,[size(rot_pts,1),1]);
end