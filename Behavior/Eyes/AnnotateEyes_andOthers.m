
%% Inputs
imgDirs = {'S:\Avinash\Ablations and behavior\Alx_eyeMovts\20181004-05\f9_abl\images';...
    'S:\Avinash\Ablations and behavior\Alx_eyeMovts\20181004-05\f10_abl\images';
    'S:\Avinash\Ablations and behavior\Alx_eyeMovts\20181004-05\f1\images';
    'S:\Avinash\Ablations and behavior\Alx_eyeMovts\20181004-05\f3\images'};
nImgsToAnnotate = 250;
imgExt = 'bmp';
% pxlLen = 14e-3; % MK told me that pxlsize = 14 microns. Converting to mm.
% eyeDiam = 0.8; % In mm


%%
disp('Reading filenames...')
tic
nImgsToAnnotate_sub = floor(nImgsToAnnotate/length(imgDirs));
imgNames = {};
for jj = 1:length(imgDirs)
    imgNames_now = GetFilenames(imgDirs{jj},'ext',imgExt);
    disp([ num2str(length(imgNames_now)) ' images found'])
    rp = randperm(length(imgNames_now));
    rp = rp(1:nImgsToAnnotate_sub);
    imgNames_now = imgNames_now(rp);
    for kk = 1:length(imgNames_now)
        imgNames_now{kk} = fullfile(imgDirs{jj},imgNames_now{kk});
    end
    imgNames = [imgNames, imgNames_now];
end
toc
% nImgs = length(imgNames);
%%
img_info = imfinfo(imgNames{1});
imgDims = [img_info.Height, img_info.Width];
I_rand = zeros(imgDims(1),imgDims(2),length(imgNames));
imgInds = 1:length(imgNames);
parfor jj = imgInds
    I_rand(:,:,jj) = imread(imgNames{jj});
end



%% Draw ROIs around eyes to annotate

% radius = 150;
coords = cell(size(I_rand,3),1);
% r = 1:size(I_rand,1);
% c = 1:size(I_rand,2);
% blah = I_rand(:,:,1)*0 + 1;
% [rr,cc] = find(blah);
for jj = 1:nImgsToAnnotate
    img = I_rand(:,:,jj);    
%     [r_com, c_com] = find(img == max(img(:)));    
%     r_com = round(mean(r_com));
%     c_com = round(mean(c_com));
%     rInds = r_com-radius:r_com+radius;
%     rInds = min(max(rInds,1),size(img,1));
%     cInds = c_com-radius:c_com+radius;
%     cInds = min(max(cInds,1),size(img,2));
%     img = img(rInds, cInds);
    roi = SetFreehandRois(img,'clrMap','parula');
    foo = [];
    disp(['Img # ' num2str(jj) '/' num2str(nImgsToAnnotate)])
    for rr = 1:length(roi)
        foo = [foo; [roi{rr}.xi, roi{rr}.yi]];
    end
    coords{jj} = [foo(:,1),foo(:,2)];
end

%% Check to see how they worked
figure;
for jj = 1:nImgsToAnnotate
    cla
    imagesc(I_rand(:,:,jj)),axis image
    colormap(gray)
    hold on
    plot(coords{jj}(:,2), coords{jj}(:,1)) 
    pause()
end

%% Save stuff
fName = 'headAnnotInfo.mat';
disp('Saving data')
tic
rootDir = fileparts(imgDirs{1});
outDir = fullfile(rootDir,'proc');
if exist(outDir, 'dir')~=7
    mkdir(outDir)
end
crop_howTo = 'I(cropInds(1):cropInds(2),cropInds(3):cropInds(4),:)';
% imgInds_rand = rp;
nImgs_annot = nImgsToAnnotate;
% imgNames_annot = imgNames(imgInds_rand(nImagesToAnnotate));
imgNames_annot = imgNames;
I_annot = I_rand;
save(fullfile(outDir,fName),'imgNames_annot','nImgs_annot','coords','I_annot');
toc

%% Save images used for annotation


