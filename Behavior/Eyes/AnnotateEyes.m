
%% Inputs
imgDir = '\\Koyama-S1\Data2\Rig1\Data\Minoru\2017-01-13\behav\f1s3';
nImagesToAnnotate = 100;
imgExt = 'bmp';
pxlLen = 14e-3; % MK told me that pxlsize = 14 microns. Converting to mm.
eyeDiam = 0.8; % In mm

%%
disp('Reading filenames...')
tic
imgNames = GetFilenames(imgDir,'ext',imgExt);
toc
nImgs = length(imgNames);
disp([ num2str(nImgs) ' images found'])


%% Read 1000 or fewer random images and compute average image
rp = randperm(nImgs);
rp = rp(1:min([1000, nImgs]));
disp('Reading and averaging images...')
tic
I_rand = ReadImgSequence(imgDir,imgExt,[],imgNames(rp));
I_avg = mean(I_rand,3);
toc

%% Cropping images
figure
imagesc(I_avg),axis image
title('Click on two points on fish: 1st - fixation, 2nd - tail tip')
[x,y] = ginput(2);
close

x_min = floor(min(x));

blah = Standardize(sum(I_avg,2));
[~,pivotInd] = max(blah);

eyeDiam_pxls = ceil(eyeDiam/pxlLen);
cropInds = [(pivotInd-3*eyeDiam_pxls), pivotInd+3*eyeDiam_pxls; x_min, size(I_rand,2)];
% I_rand_crop = I_rand((pivotInd-3*eyeDiam_pxls):(pivotInd+3*eyeDiam_pxls),x_min:end,:);
I_rand_crop = I_rand(cropInds(1,1):cropInds(1,2),cropInds(2,1):cropInds(2,2),:);



%% Draw ROIs around eyes to annotate
coords = cell(nImagesToAnnotate,1);
for jj = 1:nImagesToAnnotate
    roi = SetFreehandRois(I_rand_crop(:,:,jj));
    foo = [];
    for rr = 1:length(roi)
        foo = [foo; [roi{rr}.xi, roi{rr}.yi]];
    end
    coords{jj} = [foo(:,1),foo(:,2)];
end

%% Check to see how they worked
figure;
for jj = 1:nImagesToAnnotate
    cla
    imagesc(I_rand_crop(:,:,jj)),axis image
    colormap(gray)
    hold on
    plot(coords{jj}(:,2), coords{jj}(:,1))
    pause()
end

%% Save stuff
disp('Saving data')
tic
outDir = fullfile(imgDir,'proc');
if exist(outDir, 'dir')~=7
    mkdir(outDir)
end
crop_howTo = 'I(cropInds(1,1):cropInds(1,2),cropInds(2,1):cropInds(2,2),:)';
imgInds_rand = rp;
nImgs_annot = nImagesToAnnotate;
imgNames_annot = imgNames(imgInds_rand(nImagesToAnnotate));
save(fullfile(outDir,'eyeAnnotInfo.mat'),'imgInds_rand','imgNames_annot',...
    'nImgs_annot','cropInds','crop_howTo','coords')
toc



