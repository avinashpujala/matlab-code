function [x,y,z,s,th,X,Frenet] = modcurvesplineP(p,beta,wormradius,len,PAR)
%[x,y,z,s,th,Y,Frenet] = modcurvesplineP(p,eta,wormradius,len,PAR)

%This function takes the state and model parameters and the fish and
%calculates the point locations of the model in a normalized coordinate
%frame at the origin.  The (x,y,z) returned still have to be scaled and
%translated to match its location in the image.

%It is called by model_shell_velren.m which does the scaling and
%translating


xi = 1;%+beta;

eta2 = beta(1)*PAR.dt;

%I want a c'th order periodic spline,
c = PAR.c;
knotv = oknotP(length(p),c,len(1),PAR.etamax);

%M = @(s) dbasisP(c,s,length(p),knotv);
M = @(s) dbasisPZero(c,s,length(p),knotv);

F1 = @(s) xi.*cos(M(s)*p');
F2 = @(s) xi.*sin(M(s)*p');

inttol = 1e-5;

th = linspace(-1,1,3);

if PAR.pectoral
    %This will be a non-uniform sample along the fish's length. It makes it so
    %that edge measurements are not taken in a specific region along the length
    %(e.g. the pectoral region)
    reg1 = PAR.pectoralRegion(1);
    reg2 = PAR.pectoralRegion(2);
%     s1 = linspace(0,reg1*sum(len),round(reg1*PAR.L));
%     s2 = linspace(reg2*sum(len),sum(len),PAR.L - round(reg1*PAR.L));
    s1 = linspace(0,reg1*sum(len),round(2/3*PAR.L));
    s2 = linspace(reg2*sum(len),sum(len),round(1/3*PAR.L));
    s = [s1 s2] - len(1) + eta2;
else
    %This is the uniform spacing for the model when no pectoral fins are
    %present.
    s = linspace(-len(1),len(2),PAR.L) + eta2;
end

%initialize vectors
TEMP = zeros(length(s),3);

x = zeros(length(s),length(th));
y = x; z = x;

X = modelRomIntP(p,s,c,knotv);

T = [F1(s') F2(s') zeros(size(s,2),1)];
B = [zeros(size(s,2),2) ones(size(s,2),1)];

skew_z = [0 -1 0
    1 0 0
    0 0 0];

N = (skew_z*T')';
R =  radiusspline1(s'-eta2,wormradius,length(s),0,len);

for j = 1:length(th)
    for i = 1:3
        TEMP(:,i) = R.*(th(j).*N(:,i) + th(j).*B(:,i));
    end
    RR = [X zeros(size(s,2),1)] + TEMP;
    x(:,j) = RR(:,1);
    y(:,j) = RR(:,2);
    z(:,j) = RR(:,3);
end
Frenet.T = T;
Frenet.N = N;
Frenet.B = B;
