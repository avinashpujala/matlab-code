function Y = recalcICPx(pp,len,PAR,NN)
% Y = recalcICPx(pp,len,PAR,NN)
% This functions predicts the new state given that the fish moves
% along its length with velocity 'eta1' and accleration 'NN' 
% p is a row vector
% len, pixpermm, and etamax are scalars
% NN is the noise acceleration

etamax = PAR.etamax;
pixpermm = PAR.pixpermm;
dt = PAR.dt;

p = pp(1:end-3);
x = pp(end-2);
y = pp(end-1);
eta1 = pp(end);

c = PAR.c;
knotv = oknotP(length(p),c,len(1),etamax);

M = @(s) dbasisP(c,s,length(p),knotv);
%M = @(s) dbasisPZero(c,s,length(p),knotv);
Theta = @(s) M(s)*p';
F1 = @(s) xi.*cos(M(s)*p');
F2 = @(s) xi.*sin(M(s)*p');


% parameter domain is in the tail region only
s = linspace(-len(1),0,100);
%s = linspace(-len(1),len(2),100);

eta = eta1*dt + NN*(dt^2/2);

ss = s + eta;

% beta = eta;
% 
% numpts = 20;
% X = modelIntP(p,s,c,knotv);
% Xnew = zeros(size(X));
% 
% %knotvx = oknotP(numpts,c,len,etamax+beta);
% %Nx  = dbasisP(c,s,numpts,knotvx);
% %XX = pinv(Nx)*X;
% 
% %Nx_ss = dbasisP(c,ss,numpts,knotvx);
% 
% Xnew(:,1) = spline(s,X(:,1),ss);
% Xnew(:,2) = spline(s,X(:,2),ss);
% 
% N  = dbasisP(c,s,size(p,2),knotv);
% alpha1 = N*p'; %Theta(u) from the bspline parameters
% sgn1 = sign(alpha1(1));
% 
% dxtmp = diff(Xnew,1,1);
% alpha = atan2(dxtmp(:,2),dxtmp(:,1)); %Theta(u) from the atan of
%                                       %extrapolated certerline
%                                       %points [-pi pi].
% %get rid of last index of 'ss' since the angle is not calculated
% %there
% ss = ss(1:end-1);
% 
% %Interpolate the current shape parameters in arc length domain of 'ss'
% knotvv = oknotP(length(p),c,len(1),abs(eta));
% Ninterp  = dbasisP(c,ss,size(p,2),knotvv);
% alpha2 = Ninterp*p';
% 
% %Now Compare the difference between the curves and change locations
% %in alpha that have a +/- pi discontinuity
% 
% Dalpha = (alpha2-alpha);
% idx = find(abs(Dalpha) >=pi/2);
% sgn = sign(Dalpha(idx));
% alpha(idx) = alpha(idx) + sgn.*2*pi;
% 
% N  = dbasisP(c,s(1:end-1),size(p,2),knotv);
% 
% P = pinv(N)*alpha;

P = pinv(M(s))*Theta(ss);

%E1 = [ quadl(F1,0,eta) ; quadl(F2,0,eta)].*pixpermm;
E1 = modelRomIntP(p,eta,c,knotv).*pixpermm;
Y = [P ;([x;y] + E1')];

%add acceleration to eta1 
Y(end+1,:) = [eta1 + NN*dt];

%Pass through accleration
%Y(end+1,:) = NN*(dt^2/2);


