% paste_image_auto.m
%
% This function plots the tracking results for a particular frame of a
% movie sequence defined by the user
%
% The result is created by plotting the image in one axis (ax1) and 
% plotting the model result and feature points in an identical axis (ax2)
% on top of the image.
%
% parameters at the beginning of the file need to be changed accordingly
%================================
%
% If a file "ManualFit" already exists in memory, this function will use it
% to plot the data.
%
% If not, it will query the user to select a sequence.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('ManualFit','var') == 1
    PAR = ManualFit.ImageData;
else
    [FileName,PathName] = uigetfile({'*.mat'},'Select "ManualFit" data file for the video sequence');
    load([PathName FileName]);
       
    % Check that the paths stored in 'ManualFit' match the location that you
    % just selected the file from
    % If 75% of the paths match , then we'll assume everything's okay.
    endd = round(.75*length(ManualFit.ImageData.solutionpath));
    
    if strcmp(PathName(1:endd),ManualFit.ImageData.solutionpath(1:endd)) == 0
        %if different, run Loadvideo routine
        warning('The directories stored in ManualFit structure do not match its current location.\n You are prompted to relocate the directories',[]);
        PAR = LoadVideo;
        ManualFit.ImageData = PAR;
        save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
    else
        PAR = ManualFit.ImageData;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figure number to plot in and frame to plot
fignum = 1;
frame = 15;

%changes the scaling of the final figure, as percentage of original image
%resolution to make fit on screen.
plotscale = 1;


plotpredict = 0;

cptsTag = 0;   %Set to 1 to see the corresponding points
KF_tag = 0;    %Set to 1 to see the 1D measurement lines searching for edge points
nrmlTag = 0;     %Set to 1 to see the normal vectors

uncertainty_plot = 0; %Set to 1 to plot the time evolution of the
%state variance.
levelsettag = 0;    %Set to 1 to plot the Level Set Segmentation

clear IMG Pixpts TRI



%--------------------------------
% Define the Tracking Parameters
PAR.pwr = 0;
PAR.scale = 1/(2^PAR.pwr);
PAR.etamax = 0;
PAR.statedim = 11;

%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is.
PAR.L = 30;

if isfield(ManualFit,'pectoralRegion')
    PAR.pectoral = true; %false
    PAR.pectoralRegion = ManualFit(1).pectoralRegion;
else
    PAR.pectoral = false; 
end

PAR.mdlpar = [8 8 8]; % length(PAR.mdlpar) should be equal to
% numfish.  It is the number of parameters
% used in the each fish model.
PAR.fishnum = [1];
PAR.numfish = length(PAR.fishnum);
PAR.pixpermm = ManualFit(1).pixpermm*PAR.scale;
PAR.modelfun_H = @modcurvesplineP_display;
PAR.c = 4; %spline order

PAR.dt = 1/25; % Frame Rate

% --- Second, for each measurement point find the points
% that fall within a circle of radius gamma
% Confidence values for Gate (Probability of finding the
% measurement) fron chi-square distribution tables
% 99.5% ==> 10.59663
% 99% ==> 9.21034
% 97.5% ==> 7.37776
% 95% ==> 5.99146
% 90% ==> 4.60517
% 75% ==> 2.77259
PAR.gamma = [20 5.99146];

PAR.IMthresh = 10; % Segmentation threshold for background
% subtraction

% Length parameters of fish [length of tail , length of head]
PAR.len = ManualFit(1).length(1:2,:)./(PAR.pixpermm);
% PAR.len = ManualFit(1).length./(PAR.pixpermm);

% Radius parameters of fish (B-spline control points)
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts);

%---------------------
% Define Movie Frame
%---------------------
figure(fignum); clf

digits = length(num2str(PAR.numframes));
image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,frame,PAR.image_filter(2:end));
                
im1 = imread([PAR.imagepath image2read],PAR.image_filter(3:end));

% load fish02_SynVideo.mat
% im1 = fishim(:,:,frame);
% im1 = imabsdiff(255.*im1,im);

im1 = subsample2(im1,PAR.pwr);

PAR.imgres = size(im1);
PAR.imgres_crop = PAR.imgres;

MAXbuffer = 0;%1024 - 878;
colormap gray


% ------------------------------------
% Load the solution data
load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(frame) '.mat']);
load([PAR.solutionpath 'Features_' PAR.stub '/Features' num2str(frame) ...
    '.mat']);
% load([PAR.solutionpath 'fish_' PAR.stub '_Reg2edge/fish' num2str(frame) '.mat']);
% load([PAR.solutionpath 'Features_' PAR.stub '_Reg2edge/Features' num2str(frame) ...
%      '.mat']);

%----------------------------
% Evaluate Model at solution
%----------------------------
% Manual fit solution at first frame
% sol = reshape(ManualFit(frame).soln',[],1)';

% Predicted solution at current frame
%sol1 = InternalVariablesDS.xh_';

% Actual solution
sol = xh';

if plotpredict == 1
    load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(frame+1) '.mat']);
    sol1 = InternalVariablesDS.xh_';
    
    load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(frame) '.mat']);
else
    % Predicted solution at current frame
    sol1 = InternalVariablesDS.xh_;
end

[x,y,z,Frenet,s,t] = model_shell_velren(sol,0,PAR);
[x1,y1,z1,Frenet,s,t] = model_shell_velren(sol1,0,PAR);


%----------------------
% Plot Movie Frame or Rendered model image
%----------------------
imagesc(im1);
colormap gray

ax1 = gca;

set(ax1,'units','pixels','visible','off','position',[0 0 PAR.imgres(2) PAR.imgres(1)].*plotscale);
axis normal

%------------------------------------------
% Overlay the Model Mesh and other Features as described
%------------------------------------------

impos = get(ax1,'position');
ax2 = axes('position',[0 0 .5 .5]);
color = {'g','r','b','c'};
for k = PAR.fishnum
    hold on;

    % Either plot the model outline
    %   plotpts = [x{k}(1,1) y{k}(1,1)
    %              x{k}(1,2) y{k}(1,2)
    %              x{k}(1,3) y{k}(1,3)
    %              x{k}(2:end-1,3) y{k}(2:end-1,3)
    %              x{k}(end,3) y{k}(end,3)
    %              x{k}(end,2) y{k}(end,2)
    %              x{k}(end,1) y{k}(end,1)
    %              x{k}(end-1:-1:2,1) y{k}(end-1:-1:2,1)
    %              x{k}(1,1) y{k}(1,1)];
    %   surf_h(k) = plot(plotpts(:,1),plotpts(:,2)-MAXbuffer,'color',color{k},'linewidth',3);

    % Or the model mesh
    surf_h(k) = surf(ax2,x{k},y{k},z{k},'facecolor', ...
        'none','edgecolor',color{k},'linestyle','-','linewidth',1);
    
    if plotpredict == 1
        surf(ax2,x1{k},y1{k},z1{k},'facecolor', ...
            'none','edgecolor','y','linestyle','--','linewidth',.5);
    end

    % 'EBmeshplot' makes a different surface plot with more control over the
    % line appearance
    % hh = EBmeshplot(fignum,x1{1},y1{1},{2,color{k},'--'},{.5,color{k},'--'});
    % hh = EBmeshplot(fignum,x{1},y{1},{2,'w','-'},{.5,'w','-'});
    % [xx,Frenet,theta,kappa] = refitCenterline(x{1}(:,2),y{1}(:,2),1,10,PAR.len(:,k));
    % plot(ax2,xx(:,1),xx(:,2),'.-','color',color{k},'linewidth',2);

    % The Model points will change depending on the observation function I used

    % this is 3 points at the tail/head and the Left/Right boundary points
    %   mdlpts = [x{k}(1,1) y{k}(1,1)
    %             x{k}(1,2) y{k}(1,2)
    %             x{k}(1,3) y{k}(1,3)
    %             x{k}(end,1) y{k}(end,1)
    %             x{k}(end,2) y{k}(end,2)
    %             x{k}(end,3) y{k}(end,3)
    %             x{k}(2:end-1,1) y{k}(2:end-1,1)
    %             x{k}(2:end-1,3) y{k}(2:end-1,3)];

    % this is 1 points at the tail, 3 points at head, and the Left/Right boundary points
    % mdlpts = [x{k}(1,2) y{k}(1,2)
    %     x{k}(end,1) y{k}(end,1)
    %     x{k}(end,2) y{k}(end,2)
    %     x{k}(end,3) y{k}(end,3)
    %     x{k}(1:end-1,1) y{k}(1:end-1,1)
    %     x{k}(1:end-1,3) y{k}(1:end-1,3)];

    % this is 3 points at the head and the Left/Right boundary points
    mdlpts = [x{k}(end,1) y{k}(end,1)
        x{k}(end,2) y{k}(end,2)
        x{k}(end,3) y{k}(end,3)
        x{k}(1:end-1,1) y{k}(1:end-1,1)
        x{k}(1:end-1,3) y{k}(1:end-1,3)];

    % mdlpts = [x1{k}(end,1) y1{k}(end,1)
    %     x1{k}(end,2) y1{k}(end,2)
    %     x1{k}(end,3) y1{k}(end,3)
    %     x1{k}(1:end-1,1) y1{k}(1:end-1,1)
    %     x1{k}(1:end-1,3) y1{k}(1:end-1,3)];

%     if isfield(Features(frame),'DataptsFull')
%         datapts = Features(frame).DataptsFull{k};
%     end

    %Uncomment this if you want to plot the point correspondence at the initial
    %condition.
    if isfield(Features(frame),'DataptsFullIC')
          datapts = Features(frame).DataptsFull{k};
    end

    %   plot(Features(frame).All_Bndy_Points(:,1), ...
    %          Features(frame).All_Bndy_Points(:,2),'b.');

    %=====================================================================
    if cptsTag == 1;
        %--- Plot Correspondence between points
        for j = 1:size(datapts,1)
            if datapts(j,:) == [0 0]
                %This point is occluded. Plot as yellow star.
                plot(mdlpts(j,1),mdlpts(j,2),'y*')
            else
                plot([mdlpts(j,1) datapts(j,1)],[mdlpts(j,2) datapts(j,2)], ...
                    [color{k} 'o:'],'markerfacecolor','k');
                %         plot([mdlpts(j,1) datapts(j,1)],[mdlpts(j,2) datapts(j,2)], ...
                %              [color{k} '-']);
                %          plot(mdlpts(j,1),mdlpts(j,2),[color{k} 'o'],'markerfacecolor',color{k});
                %          plot(datapts(j,1),datapts(j,2),[color{k} 'o'],'markerfacecolor','k','markersize',10,'linewidth',2);
            end
        end

        %--------------
        % Plot all the boundary points and high curvature points
        %--------------
        %     plot(Features(frame).All_Bndy_Points(:,1), ...
        %          Features(frame).All_Bndy_Points(:,2),'b.');
        %
        %     plot(Features(frame).Candpts(:,1), ...
        %          Features(frame).Candpts(:,2),'rs','markersize',10);

    end

    %--- Determine the index of points that are not occluded
    %notocc = setdiff(1:size(mdlpts,1),Features(frame).occluded_idx{k});

    %--- Or just set it to the entire point set
    notocc = 1:size(mdlpts,1);

    %=====================================================================
    if nrmlTag == 1
        %--------------------------------------------
        % Plot all the outward normal vectors from the model and the
        % data
        %---------------------------------------------
        quiver(Features(frame).All_Bndy_Points(:,1), ...
            Features(frame).All_Bndy_Points(:,2),Features(frame).All_Nrml_Vectors(:,1),...
            Features(frame).All_Nrml_Vectors(:,2),1.0)

        tail_vec = [-1.*Frenet(k).T(1,:)];
        head_vec = [Frenet(k).T(end,:)];
        %dorsal_vec = [-1.*Frenet(k).N(2:end-1,:)];
        %ventral_vec = [Frenet(k).N(2:end-1,:)];
        dorsal_vec = [-1.*Frenet(k).N(1:end-1,:)];
        ventral_vec = [Frenet(k).N(1:end-1,:)];

        Nrml = [head_vec
            head_vec
            head_vec
            dorsal_vec
            ventral_vec];

        %This normal vector arrangement is when they come from the boundary
        %instead of the centerline (see modcurvesplineP_display)
        %      Nrml = Frenet.N(:,1:2);
        %      Nrml = [Nrml(PAR.L+2:-1:PAR.L,:)
        %         Nrml(end:-1:PAR.L+3,:)
        %         Nrml(1:PAR.L-1,:)];
        %
        %      Nrml = [-Frenet.T(1,1:2)
        %         Nrml];

        %      Nrml = [tail_vec
        %              tail_vec
        %              tail_vec
        %              head_vec
        %              head_vec
        %              head_vec
        %              dorsal_vec
        %              ventral_vec];


        quiver(mdlpts(notocc,1),mdlpts(notocc,2),Nrml(notocc,1),Nrml(notocc,2),1.0,'y');

        %Code to Grab the image intensities under the model of a particular
        %model to use in texture mapping
        if k == 0
            fishtexmap = zeros(PAR.L,50);
            for r = 1:size(x{k},1)
                xx = [x{k}(r,2) y{k}(r,2)];
                NN = Frenet(k).N(r,1:2);

                RR = PAR.pixpermm*radiusspline1(s{k}(r),PAR.fishradius(:,k),[],0,PAR.len(:,k));
                lambda = linspace(-RR,RR,50);
                Xsamp = repmat(xx,length(lambda),1) + ...
                    repmat(lambda,2,1)'.*repmat(NN,length(lambda),1);

                fishtexmap(r,:) = getIntensity(Xsamp,im1);
            end
        end
    end

    %=====================================================================
    if  KF_tag == 1
        %Grab the Normal Vectors
        tail_vec = [-1.*Frenet(k).T(1,:)];
        head_vec = [Frenet(k).T(end,:)];
        dorsal_vec = [-1.*Frenet(k).N(1:end-1,:)];
        ventral_vec = [Frenet(k).N(1:end-1,:)];

        Nrml = [head_vec
            head_vec
            head_vec
            dorsal_vec
            ventral_vec];

        Nrml = Nrml(:,1:2);

%                Nrml = Frenet.N(:,1:2);
%                Nrml = [Nrml(PAR.L+2:-1:PAR.L,:)
%                    Nrml(end:-1:PAR.L+3,:)
%                    Nrml(1:PAR.L-1,:)];
%        
%                Nrml = [-Frenet.T(1,1:2)
%                    Nrml];


        %------------------------------
        % Plot the predicted observation points and their associated
        % uncertainty ellipses from observation covariance matrix
        %-------------------------------
        htidx = [];
        if isempty(htidx)
            htidxEnd = 0;
        else
            htidxEnd = htidx(end);
        end
        i = frame;
        ObsDIM = 2;
        %obsdim = (size(mdlpts,1) -
        %length(Features(i).occluded_idx{k}))*ObsDIM;
        obsdim = size(mdlpts,1) + (htidxEnd*2) - length(htidx);
        %obsdim = size(mdlpts,1);
        idx1 = (k-1)*obsdim + 1;
        idx2 = k*obsdim;

        ptidx1 = (k-1)*size(mdlpts,1)*ObsDIM + 1;
        ptidx2 = k*size(mdlpts,1)*ObsDIM;

        %P_y = InternalVariablesDS.Py_IC(idx1:idx2,idx1:idx2);
        %P_y = InternalVariablesDS.Py_;
        P_y = InternalVariablesDS.Py_(idx1:idx2,idx1:idx2);

        maindiag = diag(P_y,0);
        %NRy = maindiag;

        htdiag = reshape(maindiag(1:2*length(htidx)),2,[]);

        updiag = diag(P_y,1);
        %Take every other one in off diagonal covariance
        updiag = updiag(1:2:end);
        Ry = zeros(2,2,length(htidx));
        for n = 1:size(Ry,3)
            Ry(:,:,n) = diag(htdiag(:,n));
            Ry(2,1,n) = updiag(n);
            Ry(1,2,n) = updiag(n);
        end

        %These are the covariances associated with the normal projection
        NRy = maindiag(2*length(htidx)+1:end);

        %Zhat = InternalVariablesDS.Yh_IC(ptidx1:ptidx2,:);
        Zhat = InternalVariablesDS.Yh_(ptidx1:ptidx2,:);
        Zhat = reshape(Zhat,2,[])';
        ZhatHT = Zhat(htidx,:);
        PredBdypts = Zhat((htidxEnd+1):end,:);
        %PredBdypts = setdiff(Zhat,ZhatHT,'rows');

        if ~isempty(htidx)
            Y = gatebndy(ZhatHT,Ry,PAR.gamma(k));
            for m = 1:length(Y)
                plot(ax2,Y{m}(:,1),Y{m}(:,2),'k-','linewidth',2);
            end

            plot(ZhatHT(:,1),ZhatHT(:,2),'k*');
        end

        for n = 1:size(PredBdypts,1)
            r = n+htidxEnd;
            NN = Nrml(r,:);
            RR = 15;% sqrt(NRy(n));
            lambda = linspace(-RR,RR,30);
            X = repmat(PredBdypts(n,:),length(lambda),1) + repmat(lambda,2,1)'.*repmat(NN,length(lambda),1);
            hold on
            plot(X(:,1),X(:,2),'r-','linewidth',1);


            %            if ~isempty(Features(frame).Edgepts{r,k})
            %                plot(Features(frame).Edgepts{r,k}(:,1),Features(frame).Edgepts{r,k}(:,2),'o','color',color{k})
            %            end
        end
    end
    hold off

end

view([0 90]);

% Add 0.5 so that the grid lines up correctly with pixels since
% Xlim & Ylim of image axis go from 0.5 to max+0.5. It's minus for
% Ylim since we flip the yaxis.

% set axis/figure parameters
set(ax2,'units','pixels','fontsize',12,'position',impos,'color','none','xlim',...
    [0.5 PAR.imgres(2)+0.5],'ylim',[-0.5 PAR.imgres(1)-0.5],'visible','off');

figure(fignum);
set(fignum,'units','pixels','position',[20 50 PAR.imgres(2) PAR.imgres(1)].*plotscale);
%title(['Frame ' num2str(frame) ', IC: p0 = ' num2str([sol(1:8)]) ])
text(PAR.imgres(2)/2 - 50,PAR.imgres(1)-20,['\color{black}Frame ' num2str(frame)], ...
    'fontsize',20);

if levelsettag == 1
  figure(fignum+10)
  imagesc(Features(frame).IM); 
  colormap gray
  hold on;
  imcontour(flipud(Features(frame).phi),[0 0],'r')
  axis image
  hold off
  figure; surfc(Features(frame).phi,'edgecolor','none'); 
  hold on; 
  contour(Features(frame).phi,[0 0],'r');
  colormap gray
  set(gca,'xlim',[1 size(Features(frame).phi,2)],'ylim',[1 size(Features(frame).phi,1)]);
  
end