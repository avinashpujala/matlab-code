%this code allows you to easily view the segmentation performance
%of a particular threshold value.  It pauses after each frame to
%allow visual inspection.  Change values of 'thresh' to find one
%appropriate for the particular wideo sequence.  A difference in
%grayscale value between the background and the current frame that
%is greater than 'thresh' is labeled as a 'fish' pixel. 

%filetag = 'fish03sm';
%load(['video/' filetag '.mat'])

PAR.videopath = 'video/L30-25pct/';
PAR.filetag = 'L30-25pct';
PAR.idtag = '';
PAR.fishID = '11';

%----------------------------------------------------
%load video data into buffer
%----------------------------------------------------
numframes = 100;
fishim = zeros([PAR.imgres numframes],'uint8');

imagepath = sprintf('%s%s/%s_%s_%s/',PAR.videopath,PAR.filetag,PAR.filetag,PAR.fishID,PAR.idtag);


for k = 1:numframes
    image2read = sprintf('%s_%s_%s%06d.tif',lower(PAR.filetag),PAR.fishID,lower(PAR.idtag),k);
    temp = imread([imagepath image2read],'tiff');
    fishim(:,:,k) = temp;
end

PAR.imgres = [size(fishim,1) size(fishim,2)];
PAR.imgres_crop = PAR.imgres;
N  = size(fishim,3);                                    % number of frames

% Define start and end frames of calculation
frames = [78 N];

%figure to plot in
fignum = 2;

%define the initialization frame 
initframe = 1;

%==========================
%set a threshold value
thresh = 140;%ManualFit.thresh;

% select plot mode.
% 1 = dots plotted onto of image represent fish pixels
% 2 = original image and binary image plotted side by side
plotmode = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
framevec = frames(1):frames(2);
for i = framevec
  if i == frames(1)
    %Initialize the background image
    %load(['ManualFit_' filetag]);
    load([imagepath 'ManualFit_' PAR.stub]);
    BG = ManualFit(initframe).BG;
  end
  
  data = fishim(:,:,i);
  
  [data,dum] = imsegment(data,BG,thresh);
  %data = double(imclose(data,strel('disk',5)));
  %data = imfill(data,'holes');
  switch plotmode
   case 1
    figure(fignum);
    clf;
    imagesc(fishim(:,:,i)); colormap gray
    %imagesc(data); colormap gray;
    title(['Frame ' num2str(i)])
    axis image
    
    [r,c] = find(data == 1);
    hold on;
    plot(c,r,'r.');
    hold off
    pause
    
   case 2
    figure(fignum);
    subplot(1,2,1), imagesc(fishim(:,:,i)); colormap gray
    title(['Frame ' num2str(i)])
    axis image
    
    subplot(1,2,2), imagesc(data); colormap gray
    title(['Frame ' num2str(framevec(i))])
    axis image
    pause
  end
end
