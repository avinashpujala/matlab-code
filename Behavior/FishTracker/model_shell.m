function [X,Y,Z,Frenet,s,t,ctrl_ptsX,ctrl_ptsY,IM,TRI,Pixpts,SEG] =...
    model_shell(p,T,PAR)

%[X,Y,Z,s,t] = model_shell(p,T,modcurve_H,fishradius,eta,len,numfish,pixpermm,imgres)
% Includes wave velocity in model.
% The input vector 'p' has the following structure:
% p = [a0 a1 ... a_N | .... |y0 y1 y_N | Kappa] , copied M times
% where M is the number of fish
% where the letters are shape parameters that weight our different
% basis functions, 'N' is the order of the polynomial that we are
% using to fit the shape parameters, and Kappa is the constant wave
% speed of the fish.

fishradius = PAR.fishradius;
numfish = PAR.numfish;
fishnum = PAR.fishnum;

pixpermm = PAR.pixpermm;
imgres = PAR.imgres;
len = PAR.len;
modelfun_H = PAR.modelfun_H;

% Initialize matrices/vectors
IM = zeros([imgres]);
X = cell(numfish,1);
Y = X; Z = X;

%Total # of parameters per fish.
numtot = PAR.mdlpar + 3; %numel(p)/numfish; 

%number of parameters without translation and velocity for each fish
nummodel = PAR.mdlpar;%par/numfish; 

for k = fishnum
  %Initialize structures
  Pixpts(k).tri = [];
  Pixpts(k).pts = [];
   
  pvec = []; %p;
  DXvec = [];
  
  if k == 1
    beg = 1;
  else
    beg = sum(numtot(1:k-1))+1;
  end
  
  P = p(beg:beg+nummodel(k)-1);
  DX = p(beg+nummodel(k):beg+nummodel(k)+1); %units are pixels
  BETA = p(beg+nummodel(k)+2);
  
  rad = fishradius(:,k);
  ETA = T;
  LEN = len(:,k);
  
  [XX,YY,ZZ,s{k},t{k},DUM,Frenet(k)] = modelfun_H(P,BETA,rad,LEN,PAR);
  
  if (isequal(modelfun_H,@bsplineopen) | isequal(modelfun_H,@bspline6))
    pts = reshape(pvec,2,[]);
    ctrl_ptsX{k} = pts(1,:).*pixpermm + DXvec(1);
    ctrl_ptsY{k} = pts(2,:).*pixpermm + DXvec(2);
  else
    ctrl_ptsX{k} = [];
    ctrl_ptsY{k} = [];
  end
    
  X{k} = XX*pixpermm + DX(1);
  Y{k} = YY*pixpermm + DX(2);
  Z{k} = ZZ*pixpermm;
  
  if nargout > 8 %I only want the points and rendered image
    TRI(k) = maketriangles(X{k},Y{k},s{k},t{k});
    
    % For each triangle in model, draw a box around it, and see
    % which pixels fall inside triangle.  These will make a
    % contribution to our image.  Also save which pixels go with
    % Triangle 'n'
    for n = 1:length(TRI(k).pts) %imgres(1)
      pts = TRI(k).pts{n};
      if isempty(IOtriangle(pts,[0 0])) %Triangle is colinear
	continue
      end
      xmin = min(pts(:,1));
      xmax = max(pts(:,1));
      ymin = min(pts(:,2));
      ymax = max(pts(:,2));
      % We need ceil because that is how the the pixel grid works
      cmin = ceil(xmin); cmax = ceil(xmax);
      rmin = ceil(size(IM,1) - ymax);
      rmax = ceil(size(IM,1) - ymin);
      
      %Ignore if outside of bound
      if rmin < 1 || cmin < 1
	continue
      elseif cmax > size(IM,2) || rmax > size(IM,1)
	continue
      end
      
      for i = rmin:rmax
	for j = cmin:cmax
	  val = IOtriangle(pts,[j , size(IM,1)-i]);
	  if val ~= 0
	    IM(i,j) = IM(i,j) + val;
	    %Saturate to 1 if multiple fish share same pixel
	    if IM(i,j) > 1
	      IM(i,j) = 1;
	    end
	    
	    if nargout > 9
	      Pixpts(k).tri = [Pixpts(k).tri ; n];
	      Pixpts(k).pts = [Pixpts(k).pts ; [j size(IM,1)-i]];
	    end
	  end	  
	end
      end     
    end    
  end
  
end



 


    
    
