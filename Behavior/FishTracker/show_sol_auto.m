%show_sol_auto.m
%
%show_sol_auto makes a movie of the tracking result for a particular video
%sequence.  It works similar to 'paste_image_auto' but iterates over each
%frame
%
%After running this program the user should call 'movie2avi(M2,...)' to
%save the movie to whatever filename they choose.

%clear all
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% If a file "ManualFit" already exists in memory, this function will use it
% to plot the data.
%
% If not, it will query the user to select a sequence.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('ManualFit','var') == 1
    PAR = ManualFit.ImageData;
else
    [FileName,PathName] = uigetfile({'*.mat'},'Select "ManualFit" data file for the video sequence');
    load([PathName FileName]);
    PAR = ManualFit.ImageData;
end

plotscale = .75;
fignum = 1;

frames = [1 PAR.numframes]; % number of frames in movie

samplerate = 1;
movidx = frames(1):samplerate:frames(2);

cptsTag = 0;     %Set to 1 to see the corresponding points
nrmlTag = 0;     %Set to 1 to see the normal vectors
KF_tag = 0;      %Set to 1 to see the Predicted Model and Gating Ellipses
interptag = 1;
%--------------------------------
% Define the Tracking Parameters
PAR.pwr = 0;
PAR.scale = 1/(2^PAR.pwr);
PAR.etamax = 0;
PAR.statedim = 11;

PAR.pectoral = false;

%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is. 
PAR.L = 51;

PAR.mdlpar = [8 8 8]; % length(PAR.mdlpar) should be equal to
                    % numfish.  It is the number of parameters
                    % used in the each fish model.
PAR.fishnum = [1];
PAR.numfish = length(PAR.fishnum);
PAR.pixpermm = ManualFit(1).pixpermm*PAR.scale;
PAR.modelfun_H = @modcurvesplineP_display;
PAR.c = 4; %spline order
PAR.gamma = 10.59663;
PAR.dt = 1/1500; % Frame Rate

PAR.IMthresh = 10; % Segmentation threshold for background
                   % subtraction 

% Length parameters of fish [length of tail , length of head]
PAR.len = ManualFit(1).length(1:2,:)./(PAR.pixpermm);
% Radius parameters of fish (8 B-spline control points)
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts);

solidx = [1 length(movidx)];
soln1 = zeros(length(movidx),PAR.numfish*PAR.statedim);
SOLN = zeros(length(frames(1):frames(2)),PAR.numfish*PAR.statedim);
if interptag == 1
    for i=1:length(movidx)
        load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(movidx(i)) ...
            '.mat']);
        soln1(i,:) = xh(1:PAR.numfish*PAR.statedim)';
        clear xh InternalVariablesDS
    end
end

%Now, perform interpolation to calculate the state at in between frames
for k = 1:size(soln1,2)
    SOLN(:,k) = interp1(movidx,soln1(:,k),frames(1):frames(2),'spline')';
end

dt = PAR.dt;
tsteps = 1;
dummy = [];
torder = 1;

M1dum = [];
M2dum = [];
 
T = 0;%linspace(0,12,60);
M2 = moviein(size(SOLN));
Cpts = zeros([PAR.L 2 size(SOLN,1)]);

for i= 1:size(SOLN,1)
  kk = frames(1)+(i-1);
  
  load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(kk) ...
            '.mat']);
  load([PAR.solutionpath 'Features_' PAR.stub '/Features' num2str(kk) ...
      '.mat']);
  
  %------------------
  %Load Image data
  digits = length(num2str(PAR.numframes));
  image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,kk,PAR.image_filter(2:end));
  im = imread([PAR.imagepath image2read],PAR.image_filter(3:end));
  
%   im = ManualFit.BG;
  %im = fishim(1:878,:,i);
                      
    
  MAXbuffer = 0;%1024 - 878;
  
  [x,y,z,Frenet,U,V] = model_shell_velren(SOLN(i,:),0,PAR);
  %[x,y,z,Frenet,U,V,ctrl_ptsX,ctrl_ptsY,imtmp] = model_shell_velren(SOLN(i,:),0,PAR);
  %imtmp(900:end,:) = 0;  
  %fishim1(:,:,i) = imtmp{PAR.numfish+1};
  %[xx,Frenet,theta,kappa] = refitCenterline(x{1}(:,2),y{1}(:,2),1,8);
  
  figure(fignum); 
  clf;
  
  %imagesc(imabsdiff(255.*fishim1(:,:,i),fishim(:,:,i)));
  imagesc(im);
  colormap gray
  ax1 = gca;
  set(ax1,'units','pixels','visible','off','position',[0 0 PAR.imgres(2) PAR.imgres(1)].*plotscale);
  axis image
  
  impos = get(ax1,'position');
  ax2 = axes('position',[0 0 .5 .5]);
  color = {'g','r','b'};
  for k = PAR.fishnum
    hold on
    
    % Plot the model outline
    plotpts = [x{k}(1,1) y{k}(1,1)
               x{k}(1,2) y{k}(1,2)
               x{k}(1,3) y{k}(1,3)
               x{k}(2:end-1,3) y{k}(2:end-1,3)
               x{k}(end,3) y{k}(end,3)         
               x{k}(end,2) y{k}(end,2)         
               x{k}(end,1) y{k}(end,1)         
               x{k}(end-1:-1:2,1) y{k}(end-1:-1:2,1)
               x{k}(1,1) y{k}(1,1)];
    %surf_h(k) = plot(plotpts(:,1),plotpts(:,2)-MAXbuffer,'color',color{k},'linewidth',3);
    
    
    % Or plot the model mesh
    surf_h = surf(ax2,x{k},y{k},z{k},'facecolor','none','edgecolor',color{k},'linewidth',1);
%     plot(ax2,xx(:,1),xx(:,2),'color',color{k},'linewidth',2);
    
    Cpts(:,:,i) = [x{k}(:,2) y{k}(:,2)];
    
    mdlpts = [x{k}(end,1) y{k}(end,1)
        x{k}(end,2) y{k}(end,2)
        x{k}(end,3) y{k}(end,3)
        x{k}(1:end-1,1) y{k}(1:end-1,1)
        x{k}(1:end-1,3) y{k}(1:end-1,3)];

%     mdlpts = [x{k}(1,1) y{k}(1,1)
%               x{k}(1,2) y{k}(1,2)
%               x{k}(1,3) y{k}(1,3)
%               x{k}(end,1) y{k}(end,1)         
%               x{k}(end,2) y{k}(end,2)         
%               x{k}(end,3) y{k}(end,3)         
%               x{k}(2:end-1,1) y{k}(2:end-1,1)
%               x{k}(2:end-1,3) y{k}(2:end-1,3)];
        
    datapts = Features(kk).DataptsFull{k}; 
        
    %--- Plot Correspondence between points
    if cptsTag == 1;  
      for j = 1:size(datapts,1)
        if datapts(j,:) == [0 0]
          %This point is occluded. Plot as yellow star.
          plot(mdlpts(j,1),mdlpts(j,2),'y*')
        else
          plot([mdlpts(j,1) datapts(j,1)],[mdlpts(j,2) datapts(j,2)], ...
               [color{k} 'o-']);
        end
      end  
      
      %--------------
      % Plot all the boundary points and high curvature points
      %--------------
      plot(Features(i).All_Bndy_Points(:,1), ...
           Features(i).All_Bndy_Points(:,2),'b.');
      
      plot(Features(i).Candpts(:,1), ...
           Features(i).Candpts(:,2),'rs','markersize',10);
      
      if (isfield(Features,'Z1') && ~isempty(Features(i).Z1))
        plot(Features(i).Z1(:,1), ...
             Features(i).Z1(:,2),'c^','markersize',10);
      end
    end
    
    %--- Determine the index of points that are not occluded
    notocc = 1:size(mdlpts,1);      
    %notocc = setdiff(1:size(mdlpts,1),Features(i).occluded_idx{k});

    
    if nrmlTag == 1
      %--------------------------------------------
      % Plot all the outward normal vectors from the model and the
      % data
      %---------------------------------------------
      quiver(Features(i).All_Bndy_Points(:,1), ...
             Features(i).All_Bndy_Points(:,2),Features(i).All_Nrml_Vectors(:,1),...
             Features(i).All_Nrml_Vectors(:,2),1.0)
      
      tail_vec = [-1.*Frenet(k).T(1,:)];
      head_vec = [Frenet(k).T(end,:)];
      dorsal_vec = [-1.*Frenet(k).N(2:end-1,:)];
      ventral_vec = [Frenet(k).N(2:end-1,:)];
      
      exvec = [Frenet(k).N(end,:)
               -1.*Frenet(k).N(end,:)];
      
      Nrml = [tail_vec
              tail_vec
              tail_vec
              head_vec
              head_vec
              head_vec
              dorsal_vec
              ventral_vec];
      
      quiver(mdlpts(notocc,1),mdlpts(notocc,2),Nrml(notocc,1),Nrml(notocc,2),1.0,'y');
    end
    
    if  KF_tag == 1 
      %------------------------------
      % Plot the predicted observation points and their associated
      % uncertainty ellipses from observation covariance matrix
      %-------------------------------     
      
      ObsDIM = 2;
      obsdim = (size(mdlpts,1))*ObsDIM;
      idx1 = (k-1)*obsdim + 1;
      idx2 = k*obsdim;
      
      P_y = InternalVariablesDS.Py_(idx1:idx2,idx1:idx2);
            
      maindiag = diag(P_y,0);
      maindiag = reshape(maindiag,2,[]);
      
      updiag = diag(P_y,1);
      %Take every other one in off diaganol covariance
      updiag = updiag(1:2:end);
      Ry = zeros(2,2,size(P_y,1)/2);
      for m = 1:size(Ry,3)
        Ry(:,:,m) = diag(maindiag(:,m));
        Ry(2,1,m) = updiag(m);
        Ry(1,2,m) = updiag(m);
      end
      
      Zhat = InternalVariablesDS.Yh_(idx1:idx2,:);
      Zhat = reshape(Zhat,2,[])';
      Y = gatebndy(Zhat,Ry,PAR.gamma);
      
      for m = 1:length(notocc)
        plot(ax2,Y{notocc(m)}(:,1),Y{notocc(m)}(:,2),'k-','linewidth',2);
      end    
      
      plot(Zhat(:,1),Zhat(:,2),'k*');
    end
    hold off
  end 
  view([0 90]);
  axis image
  
  %-- Create moving frame with 
  %scale = 1/2;
  %ctr = SOLN(i,9:10);
  %dxx = PAR.imgres(2)*scale*0.5;
  %dyy = PAR.imgres(1)*scale*0.5;
  %set(ax1,'xlim',[ctr(1)-dxx ctr(1)+dxx+0.5],'ylim',fliplr(PAR.imgres(1)-[ctr(2)-dyy-0.5 ctr(2)+dyy-0.5]));
  %set(ax2,'color','none','xlim',[ctr(1)-dxx ctr(1)+dxx+0.5],'ylim',[ctr(2)-dyy-0.5 ...
  %                    ctr(2)+dyy-0.5]); 
  
  
  set(ax2,'units','pixels','fontsize',12,'position',impos,'color','none','xlim',...
          [0.5 PAR.imgres(2)+0.5],'ylim',[-0.5 PAR.imgres(1)-0.5],'visible','off');
  
  figure(fignum)
  set(fignum,'units','pixels','position',[50 150 PAR.imgres(2) PAR.imgres(1)].*plotscale);
  %title([filetag ', Frame ' num2str(kk) ]);%', x = ' num2str(soln1(i,:))])
  text(PAR.imgres(2)/2 - 50,PAR.imgres(1)-20,[ '\color{black}Frame ' num2str(kk)], ...
       'fontsize',20);
  
  M2(i) = getframe(fignum);
%   M2(kk) = getframe(fignum);
end

