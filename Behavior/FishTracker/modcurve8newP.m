function [x,y,z,s,th,Nrml] = modcurve8newP(pp,wormradius,len,PAR)
%[x,y,z,s,th,Nrml] = modcurve8newP(pp,wormradius,len,PAR)

%This function is the same as modcurvesplineP except that it
%returns the model points is a particular format that I use in the
%Kalman Filter, instead of the mesh arrays used for plotting

p = pp(1:end-3);
dx = pp(end-2);
dy = pp(end-1);

eta1 = pp(end);
eta2 = eta1*PAR.dt;%0;%pp(end);

%I want a c'th order open spline,
c = PAR.c; 

knotv = oknotP(length(p),c,len(1),PAR.etamax);

%M = @(s) dbasisP(c,s,length(p),knotv);
M = @(s) dbasisPZero(c,s,length(p),knotv);

F1 = @(s) cos(M(s)*p');
F2 = @(s) sin(M(s)*p');

th = linspace(-1,1,3);

if PAR.pectoral
    %This will be a non-uniform sample along the fish's length. It makes it so
    %that edge measurements are not taken in a specific region along the length
    %(e.g. the pectoral region)
    reg1 = PAR.pectoralRegion(1);
    reg2 = PAR.pectoralRegion(2);
    s1 = linspace(0,reg1*sum(len),round(reg1*PAR.L));
    s2 = linspace(reg2*sum(len),sum(len),PAR.L - round(reg1*PAR.L));
    s = [s1 s2] - len(1) + eta2;
else
    %This is the uniform spacing for the model when no pectoral fins are
    %present.
    s = linspace(-len(1),len(2),PAR.L) + eta2;
end

inttol = 1e-5;

% UV grid matrix that looks like
% [tail point
%  head point
%  dorsal points
%  ventral points];

SS = [s(1) th(1)
      s(1) th(2)
      s(1) th(3)
      s(end) th(1)
      s(end) th(2)
      s(end) th(3)
      s(2:end-1)' repmat(th(1),length(s)-2,1)
      s(2:end-1)' repmat(th(3),length(s)-2,1)];
      
%I need to know which length parameters are greater than 0 to
%replace the tangent vector
bodyidx = find(SS(:,1) > 0);


X = modelRomIntP(p,s,c,knotv);

%Create same structure as the UV grid matrix 
X = [repmat(X(1,:),3,1)
     repmat(X(end,:),3,1)
     X(2:end-1,:)
     X(2:end-1,:)];

%initialize vectors
T = zeros(length(SS),3);
B = T; N = T; Nrml = T;

Y = zeros(2,length(s));
x1 = zeros(1,length(s));
x2 = x1;
x = zeros(size(SS,1),1);
y = x; z = x;
R = x;
RR = zeros(size(SS,1),3);
TEMP = RR;

T = [F1(SS(:,1)) F2(SS(:,1)) zeros(size(SS,1),1)];

% Replace the Tangent vectors for the body/head region
T(bodyidx,:) = repmat([F1(0) F2(0) 0],length(bodyidx),1);

B = [zeros(size(SS,1),2) ones(size(SS,1),1)];
skew_z = [0 -1 0
          1 0 0
          0 0 0];
N = (skew_z*T')';

%Radius function vector
R =  radiusspline1(SS(:,1)-eta2,wormradius,length(s),0,len);

%For each component of the vector, calculate the width offset from
%the centerline
for i = 1:3
  TEMP(:,i) = R.*(SS(:,2).*N(:,i) + SS(:,2).*B(:,i));
end

%Add it to the centerline to get the complete model mesh
RR = [X zeros(size(SS,1),1)] + TEMP;
x = RR(:,1);
y = RR(:,2);
z = RR(:,3);

%Create corresponding outward Normal vector array that matches the
%UV mesh from earlier
Nrml = [repmat(-1.*N(2,:),3,1)
        %repmat(-1.*T(2,:),3,1)
        repmat(T(5,:),3,1)
        -1.*N(7:7+length(s)-2-1,:)
        N(7:7+length(s)-2-1,:)];

