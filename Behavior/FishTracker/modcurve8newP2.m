function [x,y,z,s,th,Nrml] = modcurve8newP2(pp,wormradius,len,PAR)
%[x,y,z,s,th,Nrml] = modcurve8newP2(pp,wormradius,len,PAR)

%This function is the same as modcurvesplineP except that it
%returns the model points is a particular format that I use in the
%Kalman Filter, instead of the mesh arrays used for plotting

% This function has a different point layout that has 1 tail feature point
% and the rest is normal projected points

p = pp(1:end-3);
dx = pp(end-2);
dy = pp(end-1);

eta1 = pp(end);
eta2 = eta1*PAR.dt;%0;%pp(end);

%I want a c'th order open spline,
c = PAR.c; 

knotv = oknotP(length(p),c,len(1),PAR.etamax);

%M = @(s) dbasisP(c,s,length(p),knotv);
M = @(s) dbasisPZero(c,s,length(p),knotv);

F1 = @(s) cos(M(s)*p');
F2 = @(s) sin(M(s)*p');

th = linspace(-1,1,3);

if PAR.pectoral
    %This will be a non-uniform sample along the fish's length. It makes it so
    %that edge measurements are not taken in a specific region along the length
    %(e.g. the pectoral region)
    reg1 = PAR.pectoralRegion(1);
    reg2 = PAR.pectoralRegion(2);
    s1 = linspace(0,reg1*sum(len),round(reg1*PAR.L));
    s2 = linspace(reg2*sum(len),sum(len),PAR.L - round(reg1*PAR.L));
    s = [s1 s2] - len(1) + eta2;
else
    %This is the uniform spacing for the model when no pectoral fins are
    %present.
    s = linspace(-len(1),len(2),PAR.L) + eta2;
end

inttol = 1e-5;

% UV grid matrix that looks like
% [tail point
%  head point
%  dorsal points
%  ventral points];

SS = [s(end) th(1)
      s(end) th(2)
      s(end) th(3)
      s(1:end-1)' repmat(th(1),length(s)-1,1)
      s(1:end-1)' repmat(th(3),length(s)-1,1)];
      
%I need to know which length parameters are greater than 0 to
%replace the tangent vector
bodyidx = find(SS(:,1) > 0);


X = modelRomIntP(p,s,c,knotv);

%Create same structure as the UV grid matrix 
X = [repmat(X(end,:),3,1)
     X(1:end-1,:)
     X(1:end-1,:)];
 
%initialize vectors
T = zeros(length(SS),3);
B = T; N = T; Nrml = T;

Y = zeros(2,length(s));
x1 = zeros(1,length(s));
x2 = x1;
x = zeros(size(SS,1),1);
y = x; z = x;
R = x;
RR = zeros(size(SS,1),3);
TEMP = RR;

T = [F1(SS(:,1)) F2(SS(:,1)) zeros(size(SS,1),1)];
Ttail = [F1(s(1)) F2(s(1))];
% Replace the Tangent vectors for the body/head region
T(bodyidx,:) = repmat([F1(0) F2(0) 0],length(bodyidx),1);

B = [zeros(size(SS,1),2) ones(size(SS,1),1)];
skew_z = [0 -1 0
          1 0 0
          0 0 0];
N = (skew_z*T')';

%Radius function vector
R =  radiusspline1(SS(:,1)-eta2,wormradius,length(s),0,len);

%For each component of the vector, calculate the width offset from
%the centerline
for i = 1:3
  TEMP(:,i) = R.*(SS(:,2).*N(:,i) + SS(:,2).*B(:,i));
end

%Add it to the centerline to get the complete model mesh
RR = [X zeros(size(SS,1),1)] + TEMP;

x = RR(:,1);
y = RR(:,2);
z = RR(:,3);

%Create corresponding outward Normal vector array that matches the
%UV mesh from earlier
Nrml1 = [repmat(T(2,:),3,1)
    -1.*N(4:4+length(s)-1-1,:)
    N(4:4+length(s)-1-1,:)];

%===========================================
%Get the Boundary Normal vector instead of the centerline ones
%===========================================

%Head/Tail center pt
hcp = X(1,:);
tcp = X(4,:);

PTS =  [RR(end-(PAR.L-1-1):end,1:2)
    RR(3:-1:1,1:2)
    RR(4+PAR.L-1-1:-1:4,1:2)
    tcp];

dd = reshape(PTS,[],1);

%-- the number of control points be about one sixth of the total
%data points
c = 4; %cubic spline
numrep = c-1; %this is the number of control points that are repeated
%at the beginning and end of the vector;
npts = size(PTS,1);
T = linspace(-1,0,size(PTS,1));
knotv = oknotP(npts,c,1,0);
[N,D1] = dbasisP(c,T,npts,knotv);

Aeq = zeros(2*numrep,2*npts);
for i = 1:2
    Aeq((i-1)*numrep+(1:numrep),(i-1)*npts+(1:numrep)) = eye(numrep);
    Aeq((i-1)*numrep+(1:numrep),(i-1)*npts+(npts-(numrep-1):npts)) = -1*eye(numrep);
end
options = optimset('largescale','off','display','off');
bb = lsqlin(blkdiag(N,N),dd,[],[],Aeq,zeros(2*numrep,1),[],[],[],options);

B = reshape(bb,[],2);

skew_z = [0 -1 0
    1 0 0
    0 0 0];
% --- Perform cross product with skew matrix and make unit length
Tang = D1*B;

%Get rid of vector at last point because we dont include the tail tip.
Tang = Tang(1:end-1,:);

Tang = [Tang zeros(size(Tang,1),1)];
MAG = sqrt(sum(Tang.^2,2));
Tang = Tang./repmat(MAG,1,3);

N = (skew_z*Tang')';

%put vectors back in the correct order
Nrml = N(:,1:2);
Nrml = [Nrml(PAR.L+2:-1:PAR.L,:)
    Nrml(end:-1:PAR.L+3,:)
    Nrml(1:PAR.L-1,:)];

%=======================================
% Up to this point, this function is exactly like modcurve8newP1.  Now add
% the tail point at the top to change the model point layout.

Nrml = [-Ttail(1:2)
    Nrml];

RR = [tcp 0
    RR];

x = RR(:,1);
y = RR(:,2);
z = RR(:,3);
    
