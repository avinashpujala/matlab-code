function [xx,Frenet,theta,kappa,sqerr,bb,Sroot] = refitCenterline(x,y,N,npts,len,x0)
%
% [xx,Frenet,theta,kappa,sqerr,bb,Sroot] = refitCenterline(x,y,N,npts,len,x0)
%
% Performs a constrained optimization to fit a 'c-th' order B-spline curve
% with zero curvature at the endpoints to a set of planar points.
% INPUT
% x - x coordinate of points
% y - y coordiante of points
% N - sample rate of the input points (usually set to 1, every point)
% npts - number of control points for the fitted b-spline
% len - length of fish, [LengthOfTail LengthOfHead];
% x0 - initial guess for the location of control points
%
% OUTPUT
% xx - L x 2 matrix of solution points from fitted b-spline
% Frenet - a structure containing the local Frenet frame of the fitted curve
% theta - bend angle along the curve
% kappa - curvature along the curve
% sqerr - error between original curve and fitted curve 
% bb - vector of b-spline control points for solution
% Sroot - the locations where kappa(s) changes sign (has a root)
%
%
L = size(x,1);
c = 5;

cpts = [x y];

%smaller set of points that we will interpolate from
ccpts = [cpts(1:N:end,:) ];% cpts(end,:)];
dd = reshape(ccpts,[],1);

%estimating parameter
ss = linspace(-len(1),len(2),size(ccpts,1));
s = linspace(-len(1),len(2),L);

knotvc = oknotPstd(npts,c,len,0);

[M,D1,D2] = dbasisP(c,s,npts,knotvc);
[Mc,D1c,D2c] = dbasisP(c,ss,npts,knotvc);
A = blkdiag(Mc,Mc);

if nargin < 6 
    B = pinv(Mc)*ccpts;
    x0 = reshape(B,[],1);
end


options = optimset('largescale','on','Gradobj','on','display','none','Algorithm','interior-point');
bb = fmincon(@myfun,x0,[],[],[],[],[],[],@mycon,options);

B = reshape(bb,[],2);

xx = M*B;

sqerr = sum( (xx-ccpts).^2,2);

%Calculate normal vectors
skew_z = [0 -1 0
    1 0 0
    0 0 0];
% --- Perform cross product with skew matrix and make unit length
Tang = D1*B;

Tang = [Tang zeros(size(Tang,1),1)];
MAG = sqrt(sum(Tang.^2,2));
Tang = Tang./repmat(MAG,1,3);

%Calculate Theta
theta = atan2(Tang(:,2),Tang(:,1));

%Account for atan2 discontinuity at [-pi pi]
theta = unwrap(theta);

nrml = (skew_z*Tang')';

Frenet.N = nrml;
Frenet.T = Tang;

%Calculate curvature from dot product of second derivative and
%normal vector MAG(s) * Tang(s) = D1*B
%   d/ds ->    MAG(s) * Kappa(s) * Nrml(s) = D2*B

kappa = sum( (D2*B) .* nrml(:,1:2),2) ./MAG;

if nargout > 6
    options = optimset('display','none');
    idx = find(abs(diff(sign(kappa))) >0 );
    Sroot = [];
    for i = 1:length(idx)
        if (idx(i) > 1 && idx(i) < length(s)-1)
            temp = fzero(@kappafun,[s(idx(i)) s(idx(i)+1)],options);
            Sroot(end+1) = temp;
        end
    end
end

    function [f,g] = myfun(x)
        f = 0.5*sum((A*x - dd).^2);
        if nargout > 1
            g = zeros(size(x));
            for j = 1:size(g)
                g(j) = sum((A*x-dd).*A(:,j));
            end
        end
    end

    function [c,ceq] = mycon(x);
        B = reshape(x,[],2);
        %Calculate normal vectors
        skew_z = [0 -1 0
            1 0 0
            0 0 0];
        % --- Perform cross product with skew matrix and make unit length
        Tang = D1c*B;
        
        
        Tang = [Tang zeros(size(Tang,1),1)];
        MAG = sqrt(sum(Tang.^2,2));
        Tang = Tang./repmat(MAG,1,3);

        nrml = (skew_z*Tang')';

        %Calculate curvature from dot product of second derivative and
        %normal vector MAG(s) * Tang(s) = D1*B
        %              MAG(s) * Kappa(s) * Nrml(s) = D2*B

        kappa = sum( (D2c*B) .* nrml(:,1:2),2) ./MAG;
        
        c = [];%abs(kappa([2 end-1])) - 0.5;%[];
        headper = .2;
        headlen = 1;%round(headper*size(D2c,1));
        ceq = kappa([1 end]);
    end
    function K = kappafun(u)
        [M,D1,D2] = dbasisP(c,u,npts,knotvc);
        % --- Perform cross product with skew matrix and make unit length
        Tang = D1*B;

        Tang = [Tang zeros(size(Tang,1),1)];
        MAG = sqrt(sum(Tang.^2,2));
        Tang = Tang./repmat(MAG,1,3);

        nrml = (skew_z*Tang')';

        %Calculate curvature from dot product of second derivative and
        %normal vector MAG(s) * Tang(s) = D1*B
        %              MAG(s) * Kappa(s) * Nrml(s) = D2*B

        K = sum( (D2*B) .* nrml(:,1:2),2) ./MAG;
    end
end
    