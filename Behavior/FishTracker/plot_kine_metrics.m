% plot_kine_metrics.m
%
% This programs makes various plots of the data produced by successful
% tracking of a video sequence
%
% PLOTS:
% COM displacement, speed, and acceleration
% 
%
% code is divided into cells to calculate new plots without having to 
% recalculate the kinematics.  Sections may need to be 
% commented/uncommented to make partucilar plots  See comments below.
%
%================================
%
% If a file "ManualFit" already exists in memory, this function will use it
% to plot the data.
%
% If not, it will query the user to select a sequence.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('ManualFit','var') == 1
    PAR = ManualFit.ImageData;
else
    [FileName,PathName] = uigetfile({'*.mat'},'Select "ManualFit" data file for the video sequence');
    load([PathName FileName]);
    PAR = ManualFit.ImageData;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is. 
PAR.L = 51;
PAR.dt = 1/1500; % Frame Rate

% Temporarily copy the ManualFit Structure before loading the COV data.
ManualFit_tmp = ManualFit;

% load data for the COV location from the wildtype fish.  We will plot the 
% displacement, velocity, acceleration this location for wt fish only.
% If the COV location is not known enter an approximate location along the
% fish's body
uCOV = 0.8; %That is, 20% posterior from the snout.

%Recopy Back
ManualFit = ManualFit_tmp;
%=====================================================================


%----------------------------------------------------
%Get Image data
%----------------------------------------------------

frames = [1 PAR.numframes]; % number of frames in movie

samplerate = 1;
movidx = frames(1):samplerate:frames(2);

interptag = 1;

%--------------------------------
% Define the Tracking Parameters
PAR.etamax = 0;
PAR.statedim = 11;

PAR.mdlpar = [8 ]; % length(PAR.mdlpar) should be equal to
                    % numfish.  It is the number of parameters
                    % used in the each fish model.
PAR.fishnum = [1 ];
PAR.numfish = length(PAR.fishnum);
PAR.pixpermm = ManualFit(1).pixpermm;
PAR.modelfun_H = @modcurvesplineP_display;
PAR.c = 4; %spline order
PAR.gamma = 10.59663;

% Segmentation threshold for background subtraction 
PAR.IMthresh = ManualFit.thresh; 

% For the post-processing, we only want to sample the fish model on a
% uniform grid.
PAR.pectoral = false; 

% Length parameters of fish [length of tail , length of head]
PAR.len = ManualFit(1).length(1:2,:)./(PAR.pixpermm);
% Radius parameters of fish (8 B-spline control points)
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts(:,PAR.fishnum));
%%

%===============================================================
% Calculate location of CoA 
R = @(u) radiusspline1(u,PAR.fishradius,[],0,PAR.len)';
R1 = @(u) u.*radiusspline1(u,PAR.fishradius,[],0,PAR.len)';
ubar = quadl(R1,-PAR.len(1),PAR.len(2)) / quadl(R,-PAR.len(1),PAR.len(2));

% Also convert the COV parameter location back into correct parameter range
% for function evaluation.
uCOV = uCOV*(sum(PAR.len)) - PAR.len(1);


% Initialize the locations of the center of volume and area
COA = zeros(PAR.numframes,2);
COV = zeros(PAR.numframes,2);

% Calculate the location of the COA,COV from the stored midline data using
% interpolation
U = linspace(-PAR.len(1),PAR.len(2),PAR.L);

for i = 1:size(ManualFit.xMid,1)
       
    %Estimate CoA/CoV location by interpolation
    COA(i,:) = [interp1(U,ManualFit.xMid(i,:),ubar,'spline') interp1(U,ManualFit.yMid(i,:),ubar,'spline')];
    COV(i,:) = [interp1(U,ManualFit.xMid(i,:),uCOV,'spline') interp1(U,ManualFit.yMid(i,:),uCOV,'spline')];
end


%===============================================================
%% Plot the Displacement, specific speed, and accleration of CoA point

% Changing colors over time
% map = colormap('jet');
% colors = [interp1(1:size(map,1),map(:,1),linspace(1,size(map,1),size(data,2)))...
% interp1(1:size(map,1),map(:,2),linspace(1,size(map,1),size(data,2)))...
% interp1(1:size(map,1),map(:,3),linspace(1,size(map,1),size(data,2)))]';

% =====================================================================
% Speed
% Convert CoA pixel units into body length units
% pixels * mm/pixel * bodylength/mm = bodylength;

if isfield(ManualFit,'StimOnset')
    beg = ManualFit.StimOnset;
    time = time(1:end-(beg-1));
else
    beg = 1;
end

%The variable ApproxCoM is the one that is used for comparison between wt
%and mutant
%ApproxCoM = SOLN(beg:end,9:10);
ApproxCoM = COA(beg:end,:);
ApproxCoV = COV(beg:end,:);

%Convert units from pixels to body length
ApproxCoM = ApproxCoM.*(1/PAR.pixpermm)*(1/sum(len));
ApproxCoV = ApproxCoV.*(1/PAR.pixpermm)*(1/sum(len));

% Use MSE quintic spline method from Walker, JEB 1998 to calculate
% displacements, velocities, and accelerations.  It uses the Matlab spline 
% toolbox 

% The expected error for correctly locating an pixel location is
tol = sqrt(2*0.5^2);  %see Walker equation #13 with h = 0;
% Convert to body length units
tol = tol*(1/PAR.pixpermm)*(1/sum(len));
% data point weighting
w = ones(size(time));

timeSec = time/1000;

% =========================================
% Calculations for the 'ApproxCoM' location

% x-coordinate spline functions
[Xdispfun,Xdisp] = spaps(timeSec,ApproxCoM(:,1),tol,w,3);
Xvelfun = fnder(Xdispfun,1);
Xaccfun = fnder(Xdispfun,2);

% y-coordinate spline functions
[Ydispfun,Ydisp] = spaps(timeSec,ApproxCoM(:,2),tol,w,3);
Yvelfun = fnder(Ydispfun,1);
Yaccfun = fnder(Ydispfun,2);

%Calculate magnitude of displacement with initial location at zero
Disp = [Xdisp' Ydisp'] - repmat([Xdisp(1) Ydisp(1)],length(time),1);
Disp = sqrt(sum(Disp.^2,2));

%Calculate Velocity and Speed
Vel = [fnval(Xvelfun,timeSec) ; fnval(Yvelfun,timeSec)]';
U = sqrt(sum(Vel.^2,2)); %speed

%Calculate Acceleration
accvec = [fnval(Xaccfun,timeSec) ; fnval(Yaccfun,timeSec)]';
Acc = sqrt(sum(accvec.^2,2));

% =========================================
% Calculations for the 'ApproxCoV' location
% x-coordinate spline functions
[Xdispfun,Xdisp] = spaps(timeSec,ApproxCoV(:,1),tol,w,3);
Xvelfun = fnder(Xdispfun,1);
Xaccfun = fnder(Xdispfun,2);

% y-coordinate spline functions
[Ydispfun,Ydisp] = spaps(timeSec,ApproxCoV(:,2),tol,w,3);
Yvelfun = fnder(Ydispfun,1);
Yaccfun = fnder(Ydispfun,2);

%Calculate magnitude of displacement with initial location at zero
DispCOV = [Xdisp' Ydisp'] - repmat([Xdisp(1) Ydisp(1)],length(timeSec),1);
DispCOV = sqrt(sum(Disp.^2,2));

%Calculate Velocity and Speed
Vel = [fnval(Xvelfun,timeSec) ; fnval(Yvelfun,timeSec)]';
U_COV = sqrt(sum(Vel.^2,2)); %speed

%Calculate Acceleration
accvec = [fnval(Xaccfun,timeSec) ; fnval(Yaccfun,timeSec)]';
AccCOV = sqrt(sum(accvec.^2,2));
% 
%% Plot the Values

%set to true if this is wildtype fish
iswt = false;

if iswt
    name = 'Wildtype';
    kk = 2;
else
    name = 'Stocksteif';
    kk = 1;
end

colors = {'b--','k-','r-'};


figure(4);
subplot(3,1,1)
hold on;
hh = plot(time,Disp,colors{kk},'linewidth',2);
if iswt
    plot(time,DispCOV,colors{3},'linewidth',2);
end
ylabel('Displacement','fontsize',30);
set(hh,'DisplayName',name);
set(gca,'fontsize',30);
title('Age 15 days','fontsize',30)

subplot(3,1,2)
hold on;
hh = plot(time,U,colors{kk},'linewidth',2);
if iswt
    plot(time,U_COV,colors{3},'linewidth',2);
end
set(hh,'DisplayName',name)
set(gca,'fontsize',30);
ylabel('Speed (s^{-1})','fontsize',30);

subplot(3,1,3)
hold on;
hh = plot(time,Acc,colors{kk},'linewidth',2);
if iswt
    plot(time,AccCOV,colors{3},'linewidth',2);
end
set(hh,'DisplayName',name);
set(gca,'fontsize',30);
xlabel('Time (ms)');
ylabel('Acceleration (s^{-2})','fontsize',30);

% figure;
% [ax,h1,h2] = plotyy(time,U,time,Acc);
% xlabel('Time (ms)');
% set(get(ax(1),'ylabel'),'string','Speed (s^{-1})')
% set(get(ax(2),'ylabel'),'string','Acceleration (s^{-2})')
