function [X,Y,Z,Frenet,u,v,ctrl_ptsX,ctrl_ptsY,IMout,TRI,Pixpts] =...
    model_shell_velren(p,T,PAR,textag)

% INPUT ----------
% [X,Y,Z,Frenet,u,v] = model_shell_velren(p,T,PAR,Textag)
%
% Includes wave velocity in model.
%
% The input vector 'p' is a ROW VECTOR and has the following structure:
% p = [ [alpha_j TT eta1]  | .... |  [alpha_j TT eta1] ] 
% where [alpha_j TT eta1] is copied M times and M is the number of fish
%
% alpha_j = shape parameters that weight our different B-spline basis functions,
% TT = translation of the model in pixel coordinates
% eta1 = axial wave velocity;
% T = is always 0;
% PAR = a large structure that contains all the information about the
% model, camera scaling, etc.
%
% ==> Only Call this with 3 input arguments!
%
% OUTPUT
% X,Y,Z are cells of length(numfish).  Within each cell are the calculated
% model points that have been scaled and translated to the image frame
% (origin in lower left).  They can be plotted using matlab's 'surf'
% function.
% for a parameterized surface [x(u,v) y(u,v)] the structure is 
% X{1}(i,j) = x(u(i),v(j))
% Y{1}(i,j) = y(u(i),v(j))
%
% Frenet is a structure containing the local Frenet frame calculated along
% the centerline. T=tangent, N=normal, B=binormal
%
% u,v are the local coordinates of the mesh used to calculate the model on.
% the number of samples in u is determined by PAR.L


% persistent fishchecker
% 
% if isempty(fishchecker)
%     load fishtexmap;
%     %fishchecker = fishchecker;
%     fishchecker = fishtexmap;
% end

if (nargin == 4 && strcmp(textag,'tex'))
    maketex = true;
else
    maketex = false;
end

fishradius = PAR.fishradius;
numfish = PAR.numfish;
fishnum = PAR.fishnum;
pixpermm = PAR.pixpermm;
imgres = PAR.imgres_crop;
len = PAR.len;
%The model equation we use. 
modelfun_H = PAR.modelfun_H;

% Initialize matrices/vectors\
IMout = cell(numfish+2,1);% 
IM = zeros([imgres numfish]);
IMtex = zeros([imgres numfish]);
X = cell(numfish,1);
Y = X; Z = X; 

torder = 1;

for n = 1:torder
  time(n,:) = T^(n-1);
end

%Total # of parameters per worm.
numtot = PAR.mdlpar + 3; %numel(p)/numworms; 

%number of parameters without translation and velocity for each worm
nummodel = PAR.mdlpar;%par/numworms; 

for k = fishnum
  %Initialize structures
  Pixpts(k).tri = [];
  Pixpts(k).pts = [];
   
  if k == 1
    beg = 1;
  else
    beg = sum(numtot(1:k-1))+1;
  end
  
  P = p(beg:beg+nummodel(k)-1);
  DX = p(beg+nummodel(k):beg+nummodel(k)+(2*torder - 1)); %units are pixels
  BETA = p(beg+nummodel(k)+2*torder);
  
  %Make sure that P is a row vector 
  P = P(:)';
  
  rad = fishradius(:,k);
  ETA = T;
  LEN = len(:,k);
  
  [XX,YY,ZZ,u{k},v{k},DUM,Frenet(k)] = modelfun_H(P,BETA,rad,LEN,PAR);
  
  [Xren,Yren,Zren] = modcurvesplineP_render(P,BETA,rad,LEN,PAR);
  
  if (isequal(modelfun_H,@bsplineopen) | isequal(modelfun_H,@bspline6))
    pts = reshape(pvec,2,[]);
    ctrl_ptsX{k} = pts(1,:).*pixpermm + DXvec(1);
    ctrl_ptsY{k} = pts(2,:).*pixpermm + DXvec(2);
  else
    ctrl_ptsX{k} = [];
    ctrl_ptsY{k} = [];
  end
    
  X{k} = XX*pixpermm + DX(1);
  Y{k} = YY*pixpermm + DX(2);
  Z{k} = ZZ*pixpermm;
  
  Xren = Xren*pixpermm + DX(1);
  Yren = Yren*pixpermm + DX(2);
  Zren = Zren*pixpermm;
  
 if nargout > 8 %I only want the points and rendered image
      aa = renderfish(Xren,Yren,Zren,imgres);
      IM(end-size(aa,1)+1:end,:,k) = aa;
      if maketex == true
          IMtex(:,:,k) = renderfishtex(Xren,Yren,Zren,imgres,fishchecker);
      end
      IMout{k} = IM(:,:,k);
  end
end

IMout{k+1} = sum(IM,3);
% Set the pixel values greater than 1 equal to 1 for overlap region.
IMout{k+1}(IMout{k+1} > 1) = 1;

if maketex == true
    IMout{k+2} = sum(IMtex,3);
    % Set the pixel values greater than 1 equal to 1 for overlap region.
    IMout{k+2}(IMout{k+2} > 1) = 1;
end


 


    
    
