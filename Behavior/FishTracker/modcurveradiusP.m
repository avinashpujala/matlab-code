function [x,y,z,Frenet] = modcurveradiusP(p,beta,wormradius,len,PAR)
%[x,y,z,s,th,Y,Frenet] = modcurvesplineP(p,eta,wormradius,len,PAR)
%This approximates the bend angle of the worm with a bspline.  THe
%function parameters are the control points of the bspline.

%This is specially created for the fish!

xi = 1;%+beta;

eta2 = beta(1)*PAR.dt;

%I want a c'th order periodic spline,
c = PAR.c;
npts = size(p,1);
knotv = oknotPstd(npts,c,len,PAR.etamax);

th = linspace(-1,1,3);
s = linspace(-len(1),len(2),200);

%Calculate Centerline and Frenet Frame
[N,D1,D2] = dbasisP(c,s,npts,knotv);
X = N*p;
X = [X zeros(size(X,1),1)];

Tang = D1*p;
Tang = [Tang zeros(size(Tang,1),1)];
MAG = sqrt(sum(Tang.^2,2));
T = Tang./repmat(MAG,1,3);

skew_z = [0 -1 0
          1 0 0  
          0 0 0];
      
N = (skew_z*T')';
B = repmat([0 0 1],size(N,1),1);

for j = 1:length(th)
    %evaluate radius function in the parameter domain without eta2
    %shift with a etamax = 0.
    TEMP = zeros(size(X));
    R = PAR.pixpermm.*(radiusspline1(s,wormradius,length(s),0,len));
    for i = 1:3
        TEMP(:,i) = R.*(th(j).*N(:,i) + th(j).*B(:,i));
    end
    RR = X + TEMP;
    x(:,j) = RR(:,1);
    y(:,j) = RR(:,2);
    z(:,j) = RR(:,3);
end
Frenet.T = T;
Frenet.N = N;
Frenet.B = B;
