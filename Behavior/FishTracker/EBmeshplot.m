function [h] = EBmeshplot(fignum,x,y,outtype,intype)

% type{1} = thickness
% type{2} = color
% type{3} = type '--', '-'

figure(fignum);

%plot outline
hold on;
h = [];
for j = 1:size(x,2)
    h(end+1) = plot(x(:,j),y(:,j),'color',outtype{2},'linewidth',outtype{1},'linestyle',outtype{3});
end
h(end+1) = plot(x(1,:),y(1,:),'color',outtype{2},'linewidth',outtype{1},'linestyle',outtype{3});
h(end+1) = plot(x(end,:),y(end,:),'color',outtype{2},'linewidth',outtype{1},'linestyle',outtype{3});

%plot middle
for j = 2:size(x,1)-1
    h(end+1) = plot(x(j,:),y(j,:),'color',intype{2},'linewidth',intype{1},'linestyle',intype{3});
end

    

