function [] = paste_imagefun(sol,frame,fishim,PAR,Features)
fignum = 3;

plotscale = .75;
cptsTag = 0;       %Set to 1 to see the corresponding points
nrmlTag = 0;       %Set to 1 to see the normal vectors
KF_tag = 0;        %Set to 1 to see Gating Ellipses from
                   %measurement covariance

%---------------------
% Define Movie Frame
%---------------------
figure(fignum); clf 
%im1 = fishim(:,:,frame);
im1 = fishim;
imgres = size(im1);

%----------------------------
% Evaluate Model at solution
%----------------------------
sol = sol';
[x,y,z,Frenet,s,t] = model_shell_velren(sol,0,PAR);

%----------------------
% Plot Movie Frame or Rendered model image
%----------------------

imagesc(im1);
colormap gray

ax1 = gca;
set(ax1,'units','pixels','visible','off','position',[0 0 imgres(2) imgres(1)].*plotscale);
axis normal;

%------------------------------------------
% Overlay the Model Mesh and other Features as described
%------------------------------------------
 
impos = get(ax1,'position');
ax2 = axes('position',[0 0 .5 .5]);
color = {'g','b','r','c'};
for k = 1:PAR.numfish
  hold on;
  
  % Either plot the model outline
  plotpts = [x{k}(1,1) y{k}(1,1)
             x{k}(1,2) y{k}(1,2)
             x{k}(1,3) y{k}(1,3)
             x{k}(2:end-1,3) y{k}(2:end-1,3)
             x{k}(end,3) y{k}(end,3)         
             x{k}(end,2) y{k}(end,2)         
             x{k}(end,1) y{k}(end,1)         
             x{k}(end-1:-1:2,1) y{k}(end-1:-1:2,1)
             x{k}(1,1) y{k}(1,1)];
%   surf_h(k) = plot(plotpts(:,1),plotpts(:,2),'color',color{k},'linewidth',3);

  surf_h(k) = surf(ax2,x{k},y{k},z{k},'facecolor', ...
                  'none','edgecolor',color{k},'linewidth',1); 
  
  %surf_h(k) = surf(ax2,x1{k},y1{k},z1{k},'facecolor', ...
  %                 'none','edgecolor',color{k+2},'linewidth',1); 
  
  if frame > 0 && cptsTag == 1;
    %--- Plot Correspondence between points
    
    mdlpts = [x{k}(1,1) y{k}(1,1)
              x{k}(1,2) y{k}(1,2)
              x{k}(1,3) y{k}(1,3)
              x{k}(end,1) y{k}(end,1)         
              x{k}(end,2) y{k}(end,2)         
              x{k}(end,3) y{k}(end,3)         
              x{k}(2:end-1,1) y{k}(2:end-1,1)
              x{k}(2:end-1,3) y{k}(2:end-1,3)];
    
    if isfield(Features(frame),'DataptsFull')
      datapts = Features(frame).DataptsFull{k};
    end
    for j = 1:size(datapts,1)
      if datapts(j,:) == [0 0]
        plot(mdlpts(j,1),mdlpts(j,2),'y*')
      else
        plot([mdlpts(j,1) datapts(j,1)],[mdlpts(j,2) datapts(j,2)], ...
             [color{k} 'o-']);
      end
    end  
    
    %--------------
    % Plot all the boundary points and high curvature points
    %--------------
    plot(Features(frame).All_Bndy_Points(:,1), ...
         Features(frame).All_Bndy_Points(:,2),'b.');
    
    plot(Features(frame).Candpts(:,1), ...
         Features(frame).Candpts(:,2),'rs','markersize',10);
  end
  
  if nrmlTag == 1
    %--------------------------------------------
    % Plot all the outward normal vectors from the model and the
    % data
    %---------------------------------------------
     quiver(Features(frame).All_Bndy_Points(:,1), ...
            Features(frame).All_Bndy_Points(:,2),Features(frame).All_Nrml_Vectors(:,1),...
            Features(frame).All_Nrml_Vectors(:,2))
     
     tail_vec = [-1.*Frenet.T(1,:)];
     head_vec = [Frenet.T(end,:)];
     %dorsal_vec = [Frenet.N(1:end,:)];
     %ventral_vec = [-1.*Frenet.N(1:end,:)];
     dorsal_vec = [Frenet.N(2:end-1,:)];
     ventral_vec = [-1.*Frenet.N(2:end-1,:)];
     
     exvec = [Frenet(k).N(end,:)
              -1.*Frenet(k).N(end,:)];
     Nrml = [tail_vec
             tail_vec
             tail_vec
             head_vec
             head_vec
             head_vec
             dorsal_vec
             ventral_vec];
     quiver(mdlpts(:,1),mdlpts(:,2),Nrml(:,1),Nrml(:,2),'y');
   end
   
   if frame > 0 && KF_tag == 1
     %------------------------------
     % Plot the predicted observation points and their associated
     % uncertainty ellipses from observation covariance matrix
     %-------------------------------     
     i = frame;
     ObsDIM = 2;
     obsdim = (size(mdlpts,1) - length(Features(i).occluded_idx{k}))*ObsDIM;
     idx1 = (k-1)*obsdim + 1;
     idx2 = k*obsdim;
     
     P_y = InternalVariablesDS.Py_(idx1:idx2,idx1:idx2);
     %P_y = InternalVariablesDS.Py_{i,N(i)};
     
     maindiag = diag(P_y,0);
     maindiag = reshape(maindiag,2,[]);
     
     updiag = diag(P_y,1);
     %Take every other one in off diaganol covariance
     updiag = updiag(1:2:end);
     Ry = zeros(2,2,size(P_y,1)/2);
     for m = 1:size(Ry,3)
       Ry(:,:,m) = diag(maindiag(:,m));
       Ry(2,1,m) = updiag(m);
       Ry(1,2,m) = updiag(m);
     end
     
     %Zhat = InternalVariablesDS.yh_{i}(idx1:idx2,:);
     Zhat = InternalVariablesDS.Yh_{i,N(i)};
     Zhat = reshape(Zhat,2,[])';
     Y = gatebndy(Zhat,Ry,PAR.gamma);
     for m = 1:length(Y)
       plot(ax2,Y{m}(:,1),Y{m}(:,2),'w-','linewidth',2);
     end    
     
     plot(Zhat(:,1),Zhat(:,2),'w*');
     
     hold off
   end
end
view([0 90]);

% Add 0.5 so that the grid lines up correctly with pixels since
% Xlim & Ylim of image axis go from 0.5 to max+0.5. It's minus for
% Ylim since we flip the yaxis.

set(ax2,'units','pixels','fontsize',12,'position',impos,'color','none','xlim',...
	[0.5 imgres(2)+0.5],'ylim',[-0.5 imgres(1)-0.5],'visible','off');
figure(fignum);
set(fignum,'units','pixels','position',[20 20 imgres(2) imgres(1)].*plotscale);
text(imgres(2)/2 - 50,imgres(1)-20,['Frame ' num2str(frame)], ...
     'fontsize',20);
%title(['Frame ' num2str(frame) ', IC: p0 = ' num2str([sol(1:8)]) ])



