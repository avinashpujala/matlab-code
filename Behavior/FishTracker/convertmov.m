strvec = {'PIV'};

for n = 1:length(strvec)
  fishinfo = aviinfo(['fish' strvec{n} '.avi']);
  framesize = [fishinfo.Height fishinfo.Width];
  fishim = zeros([framesize fishinfo.NumFrames],'uint8');
  
  for k = 1:size(fishim,3)
    tmp = aviread(['fish' strvec{n} '.avi'],k);  
    fishim(:,:,k) = frame2im(tmp);
%     fishim(:,:,k) = rgb2gray(frame2im(tmp));
  end
  
  save(['fish' strvec{n} '.mat'],'fishim');
  clear fishim;
end

   
