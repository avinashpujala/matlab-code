function [x,y,z,s,th,X,Frenet] = modcurvesplineP_render(p,beta,wormradius,len,PAR)
%[x,y,z,s,th,Y,Frenet] = modcurvesplineP_render(p,eta,wormradius,len,PAR)

%This function takes the state and model parameters and the fish and
%calculates the point locations of the model in a normalized coordinate
%frame at the origin.  The (x,y,z) returned still have to be scaled and
%translated to match its location in the image.

%It is called by model_shell_velren.m which does the scaling and
%translating

%The only difference is it samples the centerline at a fixed 100 points.
%used to render the fish.

xi = 1;

eta2 = beta(1)*PAR.dt;

%I want a c'th order periodic spline,
c = PAR.c;
knotv = oknotP(length(p),c,len(1),PAR.etamax);

M = @(s) dbasisPZero(c,s,length(p),knotv);

F1 = @(s) xi.*cos(M(s)*p');
F2 = @(s) xi.*sin(M(s)*p');

inttol = 1e-5;

th = linspace(-1,1,3);

s = linspace(-len(1),len(2),100) + eta2;

%This will be a non-uniform sample along the fish length. Let's
%input some extra points at the beginning and end.
% $$$ s1 = linspace(0,0.15*len,11);
% $$$ s2 = linspace(0.15*len,0.85*len,20);
% $$$ s3 = linspace(0.85*len,len,11);
% $$$ s = [s1(1:end-1) s2 s3(2:end)] - len/2 + eta2;

%initialize vectors
TEMP = zeros(length(s),3);

x = zeros(length(s),length(th));
y = x; z = x;

X = modelRomIntP(p,s,c,knotv);

T = [F1(s') F2(s') zeros(size(s,2),1)];
B = [zeros(size(s,2),2) ones(size(s,2),1)];

skew_z = [0 -1 0
    1 0 0
    0 0 0];

N = (skew_z*T')';
R =  radiusspline1(s'-eta2,wormradius,length(s),0,len);

for j = 1:length(th)
    for i = 1:3
        TEMP(:,i) = R.*(th(j).*N(:,i) + th(j).*B(:,i));
    end
    RR = [X zeros(size(s,2),1)] + TEMP;
    x(:,j) = RR(:,1);
    y(:,j) = RR(:,2);
    z(:,j) = RR(:,3);
end
Frenet.T = T;
Frenet.N = N;
Frenet.B = B;
