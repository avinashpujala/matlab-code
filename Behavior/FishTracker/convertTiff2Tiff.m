PAR.videopath = 'video/080106/';
PAR.filetag = 'HN167_5dpf';
PAR.idtag = 'C001S0001';
PAR.fishID = '100303';
%----------------------------------------------------
%load video data into buffer
%----------------------------------------------------
imagepathwrite = sprintf('%s%s/%s_%s_%s/',PAR.videopath,PAR.filetag,PAR.filetag,PAR.fishID,PAR.idtag);

imagepathread = sprintf('%s5dpf100303/',PAR.videopath);
image2read = sprintf('%s_%03d.tif','1003X',1);
    
fishinfo = imread([imagepathread image2read],'tiff');
numframes = 105;
PAR.imgres = size(fishinfo);


for k = 1:numframes
    image2read = sprintf('%s_%03d.tif','1003X',k);
    image2write = sprintf('%s_%s_%s%06d.tif',lower(PAR.filetag),PAR.fishID,lower(PAR.idtag),k);
    
    tmp = imread([imagepathread image2read],'tiff');
    
    imwrite(tmp,[imagepathwrite image2write],'tiff','resolution',96);
end
