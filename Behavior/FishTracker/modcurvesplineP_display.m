function [x,y,z,s,th,X,Frenet] = modcurvesplineP_display(p,beta,wormradius,len,PAR)
%[x,y,z,s,th,Y,Frenet] = modcurvesplineP_display(p,eta,wormradius,len,PAR)

%This function takes the state (just the alpha parameters) and model parameters and the fish and
%calculates the point locations of the model in a normalized coordinate
%frame at the origin.  The (x,y,z) returned still have to be scaled and
%translated to match its location in the image.
%
%It is called by model_shell_velren.m which does the scaling and
%translating
%
%The only difference is that it ignores the wave speed in calculating the
%points

xi = 1;

eta2 = 0;

%I want a c'th order periodic spline,
c = PAR.c;
knotv = oknotP(length(p),c,len(1),PAR.etamax);

M = @(s) dbasisPZero(c,s,length(p),knotv);

%Center of Area calculation
% MM = @(s) s.*radiusspline1(s,wormradius,[],0,len)';
% A = @(s) radiusspline1(s,wormradius,[],0,len)';
% sCOM = quadl(MM,-len(1),len(2))/quadl(A,-len(1),len(2));

F1 = @(s) xi.*cos(M(s)*p');
F2 = @(s) xi.*sin(M(s)*p');

inttol = 1e-5;

th = linspace(-1,1,3);

if PAR.pectoral
    %This will be a non-uniform sample along the fish's length. It makes it so
    %that edge measurements are not taken in a specific region along the length
    %(e.g. the pectoral region)
    reg1 = PAR.pectoralRegion(1);
    reg2 = PAR.pectoralRegion(2);
%     s1 = linspace(0,reg1*sum(len),round(reg1*PAR.L));
%     s2 = linspace(reg2*sum(len),sum(len),PAR.L - round(reg1*PAR.L));
    s1 = linspace(0,reg1*sum(len),round(2/3*PAR.L));
    s2 = linspace(reg2*sum(len),sum(len),round(1/3*PAR.L));
    s = [s1 s2] - len(1) + eta2;
else
    %This is the uniform spacing for the model when no pectoral fins are
    %present.
    s = linspace(-len(1),len(2),PAR.L) + eta2;
end

%initialize vectors
TEMP = zeros(length(s),3);

x = zeros(length(s),length(th));
y = x; z = x;

X = modelRomIntP(p,s,c,knotv);

T = [F1(s') F2(s') zeros(size(s,2),1)];
B = [zeros(size(s,2),2) ones(size(s,2),1)];

skew_z = [0 -1 0
    1 0 0
    0 0 0];

N = (skew_z*T')';
R =  radiusspline1(s'-eta2,wormradius,length(s),0,len);

for j = 1:length(th)
    for i = 1:3
        TEMP(:,i) = R.*(th(j).*N(:,i) + th(j).*B(:,i));
    end
    RR = [X zeros(size(s,2),1)] + TEMP;
    x(:,j) = RR(:,1);
    y(:,j) = RR(:,2);
    z(:,j) = RR(:,3);
end

Frenet.T = T;
Frenet.N = N;
Frenet.B = B;

%Get the Boundary Normal vector instead of the centerline ones
% PTS =  [x(1:end,3) y(1:end,3)
%     x(end,2) y(end,2)
%     x(end:-1:1,1) y(end:-1:1,1)
%     x(1,2) y(1,2)];
% 
% dd = reshape(PTS,[],1);
% 
% %-- the number of control points be about one sixth of the total
% %data points
% c = 4; %cubic spline
% numrep = c-1; %this is the number of control points that are repeated
% %at the beginning and end of the vector;
% npts = size(PTS,1);
% T = linspace(-1,0,size(PTS,1));
% knotv = oknotP(npts,c,1,0);
% [N,D1] = dbasisP(c,T,npts,knotv);
% 
% Aeq = zeros(2*numrep,2*npts);
% for i = 1:2
%     Aeq((i-1)*numrep+(1:numrep),(i-1)*npts+(1:numrep)) = eye(numrep);
%     Aeq((i-1)*numrep+(1:numrep),(i-1)*npts+(npts-(numrep-1):npts)) = -1*eye(numrep);
% end
% options = optimset('largescale','off','display','off');
% bb = lsqlin(blkdiag(N,N),dd,[],[],Aeq,zeros(2*numrep,1),[],[],[],options);
% 
% B = reshape(bb,[],2);
% 
% skew_z = [0 -1 0
%     1 0 0
%     0 0 0];
% % --- Perform cross product with skew matrix and make unit length
% Tang = D1*B;
% 
% Tang = Tang(1:end-1,:);
% 
% Tang = [Tang zeros(size(Tang,1),1)];
% MAG = sqrt(sum(Tang.^2,2));
% Tang = Tang./repmat(MAG,1,3);
% 
% Frenet.N = (skew_z*Tang')';
