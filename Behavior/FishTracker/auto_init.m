function ManualFit = auto_init(ManualFit,PAR,mode)

% ----------------------------------
% There are 3 modes to this function:
% 'autoradius', 'calib', and 'BG'
%
% 'autoradius' calculates the model shape and radius function
% from the auto generated outline.
%
% 'calib' will calculate the pixpermm of the video sequence.
%
% 'BG' will calculate the background image of the video sequence.
%
% Ignore the other modes at the bottom


for j = 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch mode
        case 'BG'
            plotBG = 1;
            while plotBG == true
                plotBG = false;
                figure(1); clf
                
                %Load Image data
                
                %Number of digit places for the total number of frames
                digits = length(num2str(PAR.numframes));
                
%                 image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,PAR.ICframe,PAR.image_filter(2:end));
%                 fishim = imread([PAR.imagepath image2read],PAR.image_filter(3:end));
                fishim = imread(fullfile(PAR.imagepath,PAR.imagefilename));               
                
                imagesc(fishim);
                title('background - Zoom in | Click Enter when done');
                colormap gray
                axis image
                
                zoom on
                pause
                zoom off
                
                title('Click around the Fish to erase it. Double click when done');
                BG = roifill;
                ManualFit(j).BG = double(BG);
                
                clf;
                subplot(1,2,1), imagesc(BG);
                colormap gray
                axis image
                drawnow
                
                subplot(1,2,2), imagesc(double(BG) - double(fishim));
                colormap gray
                axis image
                drawnow
                
%                 R = input(['Satisfied with Background?  Enter 1 (yes) or 0 (no)']);
%                 if R == true
%                     plotBG = false;
%                     break
%                 else
%                     continue
%                 end
%               
            end
            
            %Now determine the threshold
            thresh = view_imsegment_func(ManualFit,PAR);
            ManualFit.thresh = thresh;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 'BG_auto'
            I = ReadImgSequence(PAR.imagepath);
            ManualFit(j).BG = double(mean(I(:,:,2:end),3));
            
            %Now determine the threshold          
            thresh = view_imsegment_func(ManualFit,PAR);
            ManualFit.thresh = thresh; 
        case 'BG_done'
            % If inputting already background subtracted images
            I = ReadImgSequence(PAR.imagepath);
            ManualFit(j).BG = min(I,3);
            
            %Now determine the threshold
            thresh = view_imsegment_func(ManualFit,PAR);
            ManualFit.thresh = thresh;
        case 'calib'
            figure(1); clf
            
            %Open Calibration Image
            [FileName,PathName] = uigetfile({'*.bmp';'*.tif'},'Select Calibration Image',PAR.imagepath);
            
            im = imread([PathName FileName],FileName(end-2:end));
            
            imagesc(im); axis image
            colormap gray
            
            %This can be written simpler using impixel!
            ax1 = gca;
            set(ax1,'visible','off');
            ax2 = axes('position',get(ax1,'position'));
            set(ax2,'color','none','xlim',[0 size(im,2)],'ylim',[0 size(im,1)]);
            set(1,'units','normalized');
            title(['Zoom in | Click Enter when done | ' num2str(PAR.length_in_mm) 'mm']);
            
            axes(ax1);
            
            zoom on
            pause
            zoom off
            
            
            xrange = get(ax1,'xlim');
            yrange = get(ax1,'ylim');
            
            clear s X A d Dist alpha
            
            yrange = fliplr(size(im,1) - yrange); %coordinate change to keep origin in lower left.
            
            set(ax2,'color','none','xlim',xrange,'ylim',yrange);
            axes(ax2);
            
            title(['Click between the locations of known distance, ' num2str(PAR.length_in_mm) 'mm. Click outside image when done']);
            
            [x,y] = ginput(1);
            %Initialize
            X = [];
            while x > xrange(1) && x < xrange(2) && y > yrange(1) && y < yrange(2)
                X = [X;x y];
                
                %%%%%%%%%%%%%%%%%%%
                %
                %%%%%%%%%%%%%%%%%%%
                if size(X,1) == 1
                    %%%%%%%%%%%%%%%%%%%%%%%%%%
                    % Use To plot points on Image
                    %%%%%%%%%%%%%%%%%%%%%%%%%%
                    axes(ax2);
                    plot(ax2,X(:,1),X(:,2),'o');
                    impos = get(ax1,'position');
                    set(ax2,'position',impos,'color','none','xlim',xrange,'ylim',yrange);
                elseif size(X,1) >= 2
                    %%%%%%%%%%%%%%%%%%%%%%%%%%
                    % Use To plot points on Image
                    %%%%%%%%%%%%%%%%%%%%%%%%%%
                    axes(ax2);
                    plot(ax2,X(:,1),X(:,2),'o-');
                    impos = get(ax1,'position');
                    set(ax2,'position',impos,'color','none','xlim',xrange,'ylim',yrange);
                end
                
                title(['Click between the locations of known distance: ' num2str(PAR.length_in_mm) ' (mm)).  Click outside image when done']);
                [x,y] = ginput(1);
            end
            
            pts1 = X(1:2:end,:);
            pts2 = X(2:2:end,:);
            
            D = sqrt(sum((pts1 - pts2).^2,2)); %in pixels
            
            D = mean(D);
            
            ManualFit(j).pixpermm = D/PAR.length_in_mm;
            
            %Save the scaling variable wherever the calibration image is
            pixpermm = ManualFit(j).pixpermm;
            save([PathName FileName '_scale.mat'],'pixpermm');
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
        case 'autoradius'
            debug = 0;
            
            %Load Image data
            
            %Number of digit places for the total number of frames
            digits = length(num2str(PAR.numframes));
            
%             image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,PAR.ICframe,PAR.image_filter(2:end));
%             im = imread([PAR.imagepath image2read],PAR.image_filter(3:end));
              im = imread(fullfile(PAR.imagepath,PAR.startImageName)); %(AP - 20180213)
            
%             im = double(im) - ManualFit.BG; % Was trying this (AP)
            PAR.imgres = size(im);
            
            %First, let me erase the pectoral fins from each of the fish
            %        for m = PAR.fishnum
            %            for r=1:2 %there are 2 pectoral fins
            %
            %                figure(1); clf
            %                imagesc(im);
            %                colormap gray
            %                im = roifill;
            %            end
            %        end
            
            %        im = imread('david/cont_dr_Larvae_00.tif','tiff');
            %        ManualFit(j).BG = double(imread('david/cont_dr_Larvae_background_0.tif','tiff'));
            %        PAR.imgres = size(im);
            %        ManualFit(j).pixpermm = 110;
            %        PAR.pixpermm = ManualFit(j).pixpermm;
            
            %Now I will segment the image and extract boundaries
            IMBW = imsegment(im,ManualFit(j).BG,ManualFit(j).thresh);
            
            IMBW = imfill(IMBW,'holes');
            B = bwboundaries(IMBW);
            
            %INSERT THE BOUNDARY FILTERING FROM FEAT_DETECT.M
            bndy2rid = [];
            %Transform axis
            for k = 1:length(B)
                %I will throw away contours that are smaller than 2 mm
                
                if (size(B{k},1) < round(2*PAR.pixpermm) )
                    %This boundary is too small, probably noise
                    bndy2rid(end+1) = k;
                elseif (all(diff(B{k}(:,1)) == 0) || ...
                        all(diff(B{k}(:,2)) == 0))
                    %This boundary is the image border
                    bndy2rid(end+1) = k;
                    % $$$     elseif (size(bndytemp{k},1) == imgres(1) || size(bndytemp{k},1) >= imgres(2))
                    % $$$       %This boundary is also probably an image border
                    % $$$       bndy2rid(end+1) = k;
                end
            end
            
            bndy2keep = setdiff(1:length(B),bndy2rid);
            B = B(bndy2keep);
            %if ~isempty(bndy2rid)
            %  for q = 1:length(bndy2rid)
            %    B{bndy2rid(q)} =[];
            %  end
            %end
            
            for m = PAR.fishnum
                figure(1); clf;
                imagesc(im);
                colormap gray
                axis image
                hold on                
                boundary = B{m};      
                
                extendedboundary = [];
                splitBoundaryFish1 = [];
                splitBoundaryFish2 = [];
                smoothboundary = [];
                
                %order of butterworth filter
                filter_order = 4;
                cutoff_Freq = 2*1/length(boundary)*50;
                %filter parameters definieeren
                [b a] = butter(filter_order, min(cutoff_Freq,0.99));
                %spiegelen van de randen beginpunt is ook eindpunt en dat moet er netjes in
                % mirrorboundary=1*boundary;
                % %volgorde omdraaien
                % omdraaimirrorboundary=mirrorboundary(length(mirrorboundary):-1:1,:);
                extendedboundary(1:length(boundary),:)=boundary(1:length(boundary),:);
                extendedboundary(length(boundary)+1:2*length(boundary),:)=boundary(1:length(boundary),:);
                extendedboundary(2*length(boundary)+1:3*length(boundary),:)=boundary(1:length(boundary),:);
                if debug==1
                    figure,plot(extendedboundary)
                end
                
                LONGsmoothboundary=filtfilt( b, a, extendedboundary);
                smoothboundary(1:length(boundary),:)=LONGsmoothboundary(length(boundary)+1:2*length(boundary),:);
                %close boundary
                % smoothboundary(length(boundary)+1,:)=smoothboundary(1,:);
                % figure,plot(smoothboundary)
                
                if debug==1
                    figure,imshow(label2rgb(L, @jet, [.5 .5 .5]))
                    hold on, plot(smoothboundary(1:length(smoothboundary),2),smoothboundary(1:length(smoothboundary),1), 'w', 'LineWidth', 2);
                    hold off
                end
                
                
                plot(smoothboundary(:,2),smoothboundary(:,1),'.-');
                
                %start interface clickable start and end point-----------------------------
                
                %click start and end point midline
                %find snout with mouse
                title(' Snout - zoom | click enter in matlab');
                zoom on
                pause
                zoom off
                title('click snout | enter');
                [xStartEnd(1),yStartEnd(1),dummy] = impixel;
                zoom out
                clc;
                
                %find tail with mouse
                title('zoom tail | klick enter in matlab');
                zoom on
                pause
                zoom off
                title('click tip tail | enter');
                [xStartEnd(2),yStartEnd(2),dummy] = impixel;
                zoom out
                clc;
                
                %find fin tip with mouse
                title('zoom caudal fin tip | klick enter in matlab');
                zoom on
                pause
                zoom off
                title('click caudal fin tip | enter');
                [xFintip,yFintip,dummy] = impixel;
                clc;
                
                %The units of this value are in 'pixels' like the other lengths
                CaudalFinLength = sqrt(sum([xFintip-xStartEnd(2) yFintip-yStartEnd(2)].^2));
                %end interface clickable start and end point-------------------------------
                
                %I need to determine which point on the smoothboundary is closest
                %to the clicked point.  I will use a kdtree instead of a
                %'for' loop.
                iMinStart = kdtreeidx(smoothboundary,[yStartEnd(1) xStartEnd(1)]);
                iMinEnd = kdtreeidx(smoothboundary,[yStartEnd(2) xStartEnd(2)]);
                
                %start determine both outlines fish devided by snout and tail points-------
                if iMinStart<iMinEnd
                    splitBoundaryFish1(1:(iMinEnd-iMinStart)+1,:) = smoothboundary(iMinStart:iMinEnd,:);
                    splitBoundaryFish2(1:iMinStart,:)=smoothboundary(iMinStart:-1:1,:);
                    splitBoundaryFish2(iMinStart+1:iMinStart+1+ ...
                        length(smoothboundary)-iMinEnd,:)=smoothboundary(length(smoothboundary):-1:iMinEnd,:);
                    if debug==1
                        figure,plot(splitBoundaryFish1(:,2),splitBoundaryFish1(:,1),'b');
                        hold on
                        plot(splitBoundaryFish2(:,2),splitBoundaryFish2(:,1),'r');
                        hold off
                    end
                else
                    splitBoundaryFish2(1:(iMinStart-iMinEnd)+1,:)=smoothboundary(iMinStart:-1:iMinEnd,:);
                    splitBoundaryFish1(1:length(smoothboundary)-iMinStart+1,:)=smoothboundary(iMinStart:end,:);
                    splitBoundaryFish1(length(smoothboundary)-iMinStart+2:...
                        length(smoothboundary)-iMinStart+1+iMinEnd,:)=smoothboundary(1:iMinEnd,:);
                    if debug==1
                        figure,plot(splitBoundaryFish1(:,2),splitBoundaryFish1(:,1),'b');
                        hold on
                        plot(splitBoundaryFish2(:,2),splitBoundaryFish2(:,1),'r');
                        hold off
                    end
                end
                %end determine both outlines fish devided by snout and tail points---------
                
                %Convert the fish boundaries to cartesian axis on lower left
                %corner of image.
                splitBoundaryFish1 = [splitBoundaryFish1(:,2) PAR.imgres(1)-...
                    splitBoundaryFish1(:,1)];
                
                splitBoundaryFish2 = [splitBoundaryFish2(:,2) PAR.imgres(1)-...
                    splitBoundaryFish2(:,1)];
                
                %Flip the arrays to make them go from tail to snout
                splitBoundaryFish1 = flipud(splitBoundaryFish1);
                splitBoundaryFish2 = flipud(splitBoundaryFish2);
                
                radpts1 = splitBoundaryFish1;
                radpts2 = splitBoundaryFish2;
                
                %Determine the median path between the left and right boundary
                %of the fish
                Err = 1;
                
                %When the distance between the left and right boundaries is
                %below 'Err_thresh', quit iterating --> median path is
                %found.
                Err_thresh = 1e-2;
                while Err > Err_thresh
                    DD = zeros(size(radpts1,1),size(radpts2,1));
                    for i = 1:size(radpts1,1)
                        DD(i,:) = sum((repmat(radpts1(i,:),size(radpts2,1),1) - radpts2).^2,2)';
                    end
                    
                    [dist1,nuidx] = min(DD,[],2);
                    [dist2,nnuidx] = min(DD,[],1);
                    
                    %Get rid of points that have converged ontop of each other
                    ctrpts1 = (radpts1 + radpts2(nuidx,:))./2;
                    dd = sqrt(sum(diff(ctrpts1,1,1).^2,2));
                    ctrpts1(dd < 0.5,:) = [];
                    
                    ctrpts2 = (radpts1(nnuidx,:) + radpts2)./2;
                    dd = sqrt(sum(diff(ctrpts2,1,1).^2,2));
                    ctrpts2(dd < 0.5,:) = [];
                    
                    figure(2);
                    hold on;
                    plot(radpts1(:,1),radpts1(:,2),'r.-',radpts2(:,1),radpts2(:,2),'b.-')
                    axis equal
                    
                    for k = 1:size(radpts1,1)
                        plot([radpts1(k,1) radpts2(nuidx(k),1)],[radpts1(k,2) radpts2(nuidx(k),2)],'g-');
                    end
                    
                    for k = 1:size(radpts2,1)
                        plot([radpts1(nnuidx(k),1) radpts2(k,1)],[radpts1(nnuidx(k),2) radpts2(k,2)],'k-');
                    end
                    
                    hold on; plot(ctrpts1(:,1),ctrpts1(:,2),'k--')
                    hold on; plot(ctrpts2(:,1),ctrpts2(:,2),'g--')
                    
                    Err = sqrt(mean([dist1 ;dist2']));
                    
                    radpts1 = ctrpts1;
                    radpts2 = ctrpts2;
                end
                
                dd = sqrt(sum(diff(ctrpts1,1,1).^2,2));
                D = cumsum(dd);
                Dist = sum(dd);
                T = [0 ; D./Dist ]' - 1;
                
                %If there are large jumps in the points, add a few that are
                %linear interpolated
                ctrpts1xtmp = interp1(T,ctrpts1(:,1),linspace(-1,0,size(ctrpts1,1)));
                ctrpts1ytmp = interp1(T,ctrpts1(:,2),linspace(-1,0,size(ctrpts1,1)));
                
                %Reassign the smoothed points
                ctrpts1 = [ctrpts1xtmp' ctrpts1ytmp'];
                
                X{m}(:,2) = ctrpts1(:,1);
                Y{m}(:,2) = ctrpts1(:,2);
                
                % Fit spline to centerline to smooth it more
                Ytemp = ctrpts1;
                npts = round(size(Ytemp,1)/6);
                c = 4; %cubic spline
                
                % Generate B-spline basis and obtain fit to centerline
                % using this basis (AP)
                knotv = oknotP(npts,c,1,0);
                T = linspace(-1,0,size(Ytemp,1));
                [N,D1,D2] = dbasisP(c,T,npts,knotv);
                BB = pinv(N)*Ytemp;
                ctrpts = N*BB;
                
                %I will get an initial estimate of the width of the fish by
                %reparameterizing the radius curve
                radpts1 = splitBoundaryFish1;
                radpts2 = splitBoundaryFish2;
                
                rad1x = interp1(1:size(radpts1,1),radpts1(:,1),linspace(1,size(radpts1,1),size(ctrpts,1)));
                rad1y = interp1(1:size(radpts1,1),radpts1(:,2),linspace(1,size(radpts1,1),size(ctrpts,1)));
                R = sqrt(sum(([rad1x' rad1y'] - ctrpts).^2,2))./(ManualFit(j).pixpermm);
                
                %only positive to account for normal vector facing in the
                %wrong direction
                R = abs(R);
                
                d = sqrt(sum(diff(ctrpts,1,1).^2,2));
                D = cumsum(d);
                %keyboard
                Dist = sum(d);
                s = D./ManualFit(j).pixpermm;
                
                %Add zero to get complete parameter set
                totlen = s(end);
                %tail length is ~80% of the total length
                OneMinusGamma = 1-PAR.Gamma;
                %I assume that only the tail region can bend and it
                %consists of the rear 80% of the fish.  Basically from the
                %pectoral fins back to the tip of the tail
                lentail = OneMinusGamma*totlen;
                
                
                % s is now my approximate arc length sample parameter.
                lenbody = totlen-lentail;
                
                s = [0 ; s];
                s = s - lentail;
                
                %%%%%%%%%%%%
                % THIS IS WHERE THE NUMBER OF CONTROL POINTS USED TO MODEL
                % THE FISH'S WIDTH PROFILE IS SET
                
                npts = PAR.R;
                c = PAR.c;
                knotv = oknot_radius(npts,c,[lentail;lenbody],PAR.etamax);
                N = dbasis(c,s,npts,knotv);
                SplinePts = pinv(N)*R;
                
                %Just take the average width value initially
                p0 = mean(N*SplinePts)*ones(size(SplinePts));
                
                %Also, reparameterize the centerline with this parameterization
                npts = round(size(Ytemp,1)/6);
                knotv = oknotPstd(npts,c,[lentail;lenbody],PAR.etamax);
                N = dbasisP(c,s,npts,knotv);
                BB = pinv(N)*Ytemp;
                
                %Now, refine this estimate by using a nonlinear solver
                SplinePts = findFishWidth(BB,p0,[lentail;lenbody],[radpts1;radpts2],PAR);
                
                pause
                ManualFit(j).radius_splinepts{m} = SplinePts;
                ManualFit(j).X{m} = X;
                ManualFit(j).Y{m} = Y;
                
                %%              %========================================================
                %Now estimate the centerline parameters from the ctrpts
                %calculated above
                
                dxtmp = diff(ctrpts,1,1);
                alpha = atan2(dxtmp(:,2),dxtmp(:,1));
                
                %Account for atan2 discontinuity at [-pi pi]
                Dalpha = abs(diff(alpha));
                idx = find(Dalpha >=pi,1);
                
                %sign of the angle before discontinuity
                sgn = sign(alpha(idx));
                
                %index of points that occur after the discontinuity
                if sgn > 0
                    ii = find(alpha(idx:end) < 0);
                else
                    ii = find(alpha(idx:end) > 0);
                end
                alpha(ii + idx - 1) = alpha(ii+idx-1) + sgn*2*pi;
                
                % Now estimate Modal function coefficients
                % we will estimate with sampled points of arc length parameter
                
                d = sqrt(sum(diff(ctrpts,1,1).^2,2));
                D = cumsum(d);
                Dist = sum(d);
                s = D./ManualFit(j).pixpermm;
                
                % s is now my approximate arc length sample parameter.
                
                %Add zero to get complete parameter set
                s = [0; s]';
                totlen = s(end);
                %tail length is ~80% of the total length
                %I count assume that only the tail region can bend and it
                %consists of the rear 80% of the fish.  Basically from the
                %pectoral fins back to the tip of the tail
                taillen = OneMinusGamma*totlen;
                idx1 = find(s <= taillen);
                idx2 = find(s > taillen);
                
                
                stail = s(idx1); % include 0, and leave out s(end)
                len = taillen;
                %Flip parameterization because we're moving forward from
                %tip of tail to body
                stail = stail - taillen;
                shead = s(idx2)- taillen;
                
                npts = PAR.mdlpar(1);
                c = PAR.c;
                knotv = oknotP(npts,c,len,PAR.etamax);
                
                %To make sure that the initial condition is evaluated
                %exactly at s = 0, include this point.
                %the alpha there will be the alpha from the previous one.
                %N = dbasisP(c,[stail 0],npts,knotv);
                N = dbasisPZero(c,[stail shead(1:end-1)],npts,knotv);
                % Take the first values of alpha that are the length of stail.
                %Btmp = pinv(N)*[alpha(1:length(stail)) ; alpha(length(stail))];
                Btmp = pinv(N)*alpha;
                % Method for centroid translation vector.
                TT(1) = interp1(s,ctrpts(:,1),taillen);
                TT(2) = interp1(s,ctrpts(:,2),taillen);
                
                ManualFit(j).soln(m,:) = [Btmp' TT 0];
                ManualFit(j).pts{m} = ctrpts;
                ManualFit(j).length(1,m) = OneMinusGamma*Dist;
                ManualFit(j).length(2,m) = PAR.Gamma*Dist;
                ManualFit(j).length(3,m) = CaudalFinLength;
                %keyboard
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
        case 'radius'
            for m = PAR.fishnum
                figure(1); clf
                im = fishim(:,:,j);
                
                imagesc(im);
                colormap gray
                
                ax1 = gca;
                set(ax1,'visible','off');
                ax2 = axes('position',get(ax1,'position'));
                set(ax2,'color','none','xlim',[0 size(im,2)],'ylim',[0 size(im,1)]);
                set(1,'units','normalized');
                
                axes(ax1);
                
                zoom on
                pause
                zoom off
                
                
                xrange = get(ax1,'xlim');
                yrange = get(ax1,'ylim');
                
                clear s X A d Dist alpha
                
                yrange = fliplr(size(im,1) - yrange); %coordinate change to keep origin in lower left.
                
                set(ax2,'color','none','xlim',xrange,'ylim',yrange);
                axes(ax2);
                
                title(['Click across the fish width.  Start at the ' ...
                    'tip and move towards head.  Click outside image when done']);
                
                [x,y] = ginput(1);
                
                %Initialize
                X = [];
                while x > xrange(1) && x < xrange(2) && y > yrange(1) && y < yrange(2)
                    X = [X;x y];
                    
                    %%%%%%%%%%%%%%%%%%%
                    % To approximate length of fish by clicking a bunch of times
                    %%%%%%%%%%%%%%%%%%%
                    if size(X,1) == 1
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        % Use To plot points on Image
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        axes(ax2);
                        plot(ax2,X(:,1),X(:,2),'o');
                        impos = get(ax1,'position');
                        set(ax2,'position',impos,'color','none','xlim',xrange,'ylim',yrange);
                    elseif size(X,1) >= 2
                        
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        % Use To plot points on Image
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        axes(ax2);
                        plot(ax2,X(:,1),X(:,2),'o-');
                        impos = get(ax1,'position');
                        set(ax2,'position',impos,'color','none','xlim',xrange,'ylim',yrange);
                        set(1,'units','normalized','position',[.1 .1 .8 .7]);
                    end
                    [x,y] = ginput(1);
                end
                %R = sqrt(sum(diff(X,1,1).^2,2));
                %ManualFit(j).radius(m) = R;
                
                %Assume that you have captured the radius profile of the
                %fish by clicking points in sets of 3.  The first point is
                %the center of the animal and the 2nd is the radius
                %distance.
                
                ctrpts = X(2:3:end,:);
                radpts1 = X(1:3:end,:);
                radpts2 = X(3:3:end,:);
                
                R = sqrt(sum((radpts1 - radpts2).^2,2))./(2*ManualFit(j).pixpermm);
                
                d = sqrt(sum(diff(ctrpts,1,1).^2,2));
                D = cumsum(d);
                Dist = sum(d);
                s = D./ManualFit(j).pixpermm;
                
                %Add zero to get complete parameter set
                totlen = s(end);
                %tail length is ~80% of the total length
                %I count assume that only the tail region can bend and it
                %consists of the rear 80% of the fish.  Basically from the
                %pectoral fins back to the tip of the tail
                lentail = 0.8*totlen;
                
                % s is now my approximate arc length sample parameter.
                lenbody = totlen-lentail;
                s = [0 ; s];
                s = s - lentail;
                
                npts = 8;
                c = PAR.c;
                knotv = oknot_radius(npts,c,[lentail;lenbody],PAR.etamax);
                [N,D1,D2] = dbasis(c,s,npts,knotv);
                SplinePts = pinv(N)*R;
                ManualFit(j).radius_splinepts{m} = SplinePts;
                
                %========================================================
                %Now estimate the centerline parameters from the ctrpts
                %calculated above
                
                dxtmp = diff(ctrpts,1,1);
                alpha = atan2(dxtmp(:,2),dxtmp(:,1));
                
                %Account for atan2 discontinuity at [-pi pi]
                Dalpha = abs(diff(alpha));
                idx = find(Dalpha >=pi,1);
                
                %sign of the angle before discontinuity
                sgn = sign(alpha(idx));
                
                %index of points that occur after the discontinuity
                if sgn > 0
                    ii = find(alpha(idx:end) < 0);
                else
                    ii = find(alpha(idx:end) > 0);
                end
                alpha(ii + idx - 1) = alpha(ii+idx-1) + sgn*2*pi;
                
                % Now estimate Modal function coefficients
                % we will estimate with sampled points of arc length parameter
                
                d = sqrt(sum(diff(ctrpts,1,1).^2,2));
                D = cumsum(d);
                Dist = sum(d);
                s = D./ManualFit(j).pixpermm;
                
                % s is now my approximate arc length sample parameter.
                
                %Add zero to get complete parameter set
                s = [0; s]';
                totlen = s(end);
                %tail length is ~80% of the total length
                %I count assume that only the tail region can bend and it
                %consists of the rear 80% of the fish.  Basically from the
                %pectoral fins back to the tip of the tail
                taillen = 0.8*totlen;
                idx1 = find(s <= taillen);
                idx2 = find(s > taillen);
                
                
                stail = s(idx1); % include 0, and leave out s(end)
                len = taillen;
                %Flip parameterization because we're moving forward from
                %tip of tail to body
                stail = stail - taillen;
                shead = s(idx2)- taillen;
                
                npts = 8;
                c = PAR.c;
                knotv = oknotP(npts,c,len,PAR.etamax);
                
                %To make sure that the initial condition is evaluated
                %exactly at s = 0, include this point.
                %the alpha there will be the alpha from the previous one.
                [N,D1,D2] = dbasisP(c,[stail 0],npts,knotv);
                % Take the first values of alpha that are the length of stail.
                B = pinv(N)*[alpha(1:length(stail)) ; alpha(length(stail))];
                
                % Method for centroid translation vector.
                TT(1) = interp1(s,ctrpts(:,1),taillen);
                TT(2) = interp1(s,ctrpts(:,2),taillen);
                
                ManualFit(j).soln(m,:) = [B' TT 0];
                ManualFit(j).pts{m} = ctrpts;
                ManualFit(j).length(1,m) = 0.8*Dist;
                ManualFit(j).length(2,m) = 0.2*Dist;
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 'shape'
            for m = PAR.fishnum
                figure(1); clf
                im = fishim(:,:,j);
                
                imagesc(im);
                colormap gray
                
                ax1 = gca;
                set(ax1,'visible','off');
                ax2 = axes('position',get(ax1,'position'));
                set(ax2,'color','none','xlim',[0 size(im,2)],'ylim',[0 size(im,1)]);
                set(1,'units','normalized');
                
                axes(ax1);
                
                zoom on
                pause
                zoom off
                
                
                xrange = get(ax1,'xlim');
                yrange = get(ax1,'ylim');
                
                clear s X A d Dist alpha
                
                yrange = fliplr(size(im,1) - yrange); %coordinate change to keep origin in lower left.
                
                set(ax2,'color','none','xlim',xrange,'ylim',yrange);
                axes(ax2);
                
                title(['Click along center of Fish.  Start at the ' ...
                    'tip and move towards head.  Click outside image when done']);
                
                [x,y] = ginput(1);
                
                %Initialize
                X = [];
                while x > xrange(1) && x < xrange(2) && y > yrange(1) && y < yrange(2)
                    X = [X;x y];
                    
                    %%%%%%%%%%%%%%%%%%%
                    % To approximate length of fish by clicking a bunch of times
                    %%%%%%%%%%%%%%%%%%%
                    if size(X,1) == 1
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        % Use To plot points on Image
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        axes(ax2);
                        plot(ax2,X(:,1),X(:,2),'o');
                        impos = get(ax1,'position');
                        set(ax2,'position',impos,'color','none','xlim',xrange,'ylim',yrange);
                    elseif size(X,1) >= 2
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        % Use To plot points on Image
                        %%%%%%%%%%%%%%%%%%%%%%%%%%
                        axes(ax2);
                        plot(ax2,X(:,1),X(:,2),'o-');
                        impos = get(ax1,'position');
                        set(ax2,'position',impos,'color','none','xlim',xrange,'ylim',yrange);
                        set(1,'units','normalized','position',[.1 .1 .8 .7]);
                    end
                    [x,y] = ginput(1);
                end
                
                %======================================
                % Calculate the bend angle at each point.
                if size(X,1) >=2
                    dxtmp = diff(X,1,1);
                    alpha = atan2(dxtmp(:,2),dxtmp(:,1));
                    
                    %Account for atan2 discontinuity at [-pi pi]
                    Dalpha = abs(diff(alpha));
                    idx = find(Dalpha >=pi,1);
                    
                    %sign of the angle before discontinuity
                    sgn = sign(alpha(idx));
                    
                    %index of points that occur after the discontinuity
                    if sgn > 0
                        ii = find(alpha(idx:end) < 0);
                    else
                        ii = find(alpha(idx:end) > 0);
                    end
                    alpha(ii + idx - 1) = alpha(ii+idx-1) + sgn*2*pi;
                    
                    % Now estimate Modal function coefficients
                    % we will estimate with sampled points of arc length parameter
                    
                    d = sqrt(sum(diff(X,1,1).^2,2));
                    D = cumsum(d);
                    Dist = sum(d);
                    s = D./ManualFit(1).pixpermm;
                    
                    % s is now my approximate arc length sample parameter.
                    %Add zero to get complete parameter set
                    s = [0; s]';
                    totlen = s(end);
                    %tail length is ~80% of the total length
                    %I count assume that only the tail region can bend and it
                    %consists of the rear 80% of the fish.  Basically from the
                    %pectoral fins back to the tip of the tail
                    taillen = 0.8*totlen;
                    idx1 = find(s <= taillen);
                    idx2 = find(s > taillen);
                    
                    stail = s(idx1); % include 0, and leave out s(end)
                    len = taillen;
                    %Flip parameterization because we're moving forward from
                    %tip of tail to body
                    stail = stail - taillen;
                    shead = s(idx2)- taillen;
                    
                    npts = 8;
                    c = PAR.c;
                    knotv = oknotP(npts,c,len,PAR.etamax);
                    %To make sure that the initial condition is evaluated
                    %exactly at s = 0, include this point.
                    %the alpha there will be the alpha from the previous one.
                    [N,D1,D2] = dbasisP(c,[stail 0],npts,knotv);
                    %[N,D1,D2] = dbasisPZero(c,[stail shead(1:end-1)],npts,knotv);
                    % Take the first values of alpha that are the length of stail.
                    alphazero = interp1(s(1:end-1),alpha,taillen);
                    B = pinv(N)*[alpha(1:length(stail)) ; alphazero];
                    %B = pinv(N)*alpha;
                end
                
                % Method for centroid translation vector.
                TT(1) = interp1(s,X(:,1),taillen);
                TT(2) = interp1(s,X(:,2),taillen);
                
                ManualFit(j).soln(m,:) = [B' TT 0];
                ManualFit(j).pts{m} = X;
                ManualFit(j).length(1,m) = 0.8*Dist;
                ManualFit(j).length(2,m) = 0.2*Dist;
                
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 'velocity'
            velidx = 1;
            beg = 1;
            endd = 50;
            for m = PAR.fishnum
                for tailflag = [0] %[true false]
                    for jj = beg:PAR.framesample:endd
                        figure(1); clf
                        im = fishim(:,:,jj);
                        
                        imagesc(im);
                        colormap gray
                        axis image
                        
                        ax1 = gca;
                        set(ax1,'visible','off');
                        ax2 = axes('position',get(ax1,'position'));
                        set(ax2,'color','none','xlim',[0 size(im,2)],'ylim',[0 size(im,1)]);
                        set(1,'units','normalized');
                        
                        axes(ax1);
                        
                        xrange = get(ax1,'xlim');
                        yrange = get(ax1,'ylim');
                        
                        clear s X A d Dist alpha
                        
                        yrange = fliplr(size(im,1) - yrange); %coordinate change to keep origin in lower left.
                        
                        set(ax2,'color','none','xlim',xrange,'ylim',yrange,...
                            'PlotBoxAspectRatio',[size(im,2) size(im,1) 1]);
                        axes(ax2);
                        
                        if tailflag == 1
                            title(['Click Tail location fish' num2str(m) '; ' num2str(jj) ' of ' num2str(endd)]);
                        else
                            title(['Click Head location fish' num2str(m) '; ' num2str(jj) ' of ' num2str(endd)]);
                        end
                        
                        [x,y] = ginput(1);
                        X = [x y];
                        if tailflag
                            ManualFit(velidx).(['fish' num2str(m)]).tailpts(jj,:) = X(1,:);
                        else
                            ManualFit(velidx).(['fish' num2str(m)]).headpts(jj,:) = X(1,:);
                        end
                        %ManualFit(velidx).(['fish' num2str(m)]).dir(jj,:) = input(['fish' num2str(m) ' direction. 1 = fwd; -1 = bkwd: ']);
                    end
                    
                    if PAR.framesample ~= 1
                        tt = beg:PAR.framesample:endd;
                        if tailflag
                            xx = ManualFit(velidx).(['fish' num2str(m)]).tailpts(tt,1);
                        else
                            xx = ManualFit(velidx).(['fish' num2str(m)]).headpts(tt,1);
                        end
                        xi = interp1(tt,xx,beg:endd,'spline');
                        
                        %Now do Y-component
                        if tailflag
                            yy = ManualFit(velidx).(['fish' num2str(m)]).tailpts(tt,2);
                        else
                            yy = ManualFit(velidx).(['fish' num2str(m)]).headpts(tt,2);
                        end
                        yi = interp1(tt,yy,beg:endd,'spline');
                        
                        if tailflag
                            ManualFit(velidx).(['fish' num2str(m)]).tailpts(beg:endd,:) = [xi ; yi]';
                        else
                            ManualFit(velidx).(['fish' num2str(m)]).headpts(beg:endd,:) = [xi ; yi]';
                        end
                    end
                    
                    ManualFit(velidx).velocityTag = ['This is Manual velocity statistics of ' ...
                        'the male and hermaph at framerate 1/30 for ' ...
                        'all frames'];
                    
                    %=======================================
                    %Calculate the velocity of the points
                    if tailflag
                        ManualFit(velidx).(['fish' num2str(m)]).tailvel = sqrt(sum(diff(ManualFit(velidx).(['fish' num2str(m)]).tailpts,1,1).^2,2)) ...
                            ./ (PAR.dt*PAR.pixpermm);
                    else
                        ManualFit(velidx).(['fish' num2str(m)]).headvel = sqrt(sum(diff(ManualFit(velidx).(['fish' num2str(m)]).headpts,1,1).^2,2)) ...
                            ./ (PAR.dt*PAR.pixpermm);
                    end
                    
                    %=======================================
                    %Calculate sample variance assuming a zero mean gaussian
                    %acceleration process noise.
                    %Sum()/N-1 since I use the diff command
                    if tailflag
                        ManualFit(velidx).(['fish' num2str(m)]).tailAccVar = sum((diff(ManualFit(velidx).(['fish' num2str(m)]).tailvel)./PAR.dt).^2)/...
                            (length(ManualFit(velidx).(['fish' num2str(m)]).tailvel)-1);
                    else
                        ManualFit(velidx).(['fish' num2str(m)]).headAccVar = sum((diff(ManualFit(velidx).(['fish' num2str(m)]).headvel)./PAR.dt).^2)/...
                            (length(ManualFit(velidx).(['fish' num2str(m)]).headvel)-1); %Sum()/N-1
                    end
                    
                    %save file incrementally
                    save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
                    
                end %end tailflag
            end %end numfish iter
    end
end

close all
% pause(0.5);
