%================================
% Change these strings to coincide with the movie that you would like to
% view
PAR.videopath = 'video/L30-25pct/';
PAR.filetag = 'L30-25pct';
PAR.idtag = '';
PAR.fishID = '11';

% PAR.videopath = 'video/080106/';
% PAR.filetag = 'HN167_5dpf';
% PAR.idtag = 'C001S0001';
% PAR.fishID = '080306_15';

PAR.stub = [PAR.filetag '_' PAR.fishID];
PAR.solutionpath = [PAR.videopath '/solutions/'];

fignum = 1;
frame = 218;
solidx = [1:3:frame];
plotscale = .75;
cptsTag = 0;   %Set to 1 to see the corresponding points
nrmlTag = 0;     %Set to 1 to see the normal vectors
KF_tag = 0;     %Set to 1 to see the Predicted Model and Gating Ellipses
KF_tag1 = 0;

uncertainty_plot = 0; %Set to 1 to plot the time evolution of the
                      %state variance.
levelsettag = 0;    %Set to 1 to plot the Level Set Segmentation

clear IMG Pixpts TRI

% --------------------------------
% Load the ManualFit Parameters
load([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub]);
    
%--------------------------------
% Define the Tracking Parameters
PAR.pwr = 0;
PAR.scale = 1/(2^PAR.pwr);
PAR.etamax = 0;
PAR.statedim = 11;

%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is. 
PAR.L = 30;

PAR.mdlpar = [8 8 8]; % length(PAR.mdlpar) should be equal to
                    % numfish.  It is the number of parameters
                    % used in the each fish model.
PAR.fishnum = [1 ];
PAR.numfish = length(PAR.fishnum);
PAR.pixpermm = ManualFit(1).pixpermm*PAR.scale;
PAR.modelfun_H = @modcurvesplineP_display;
PAR.c = 4; %spline order
PAR.gamma = 10.59663;
PAR.dt = 1/1500; % Frame Rate

PAR.IMthresh = 10; % Segmentation threshold for background
                   % subtraction 

% Length parameters of fish [length of tail , length of head]
PAR.len = ManualFit(1).length(:,:)./(PAR.pixpermm);
% PAR.len = ManualFit(1).length./(PAR.pixpermm);

% Radius parameters of fish (B-spline control points)
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts);

PAR.pectoral = 0;

%---------------------
% Define Movie Frame
%---------------------
figure(fignum); clf 

im1 = imread([PAR.videopath PAR.filetag '/' PAR.filetag '_' ...
        PAR.fishID '_' PAR.idtag '/' lower(PAR.filetag) '_' PAR.fishID '_' lower(PAR.idtag) ...
        repmat(num2str(0),1,6-(1+floor(log10(frame)))) num2str(frame) '.tif'],'tiff');

im1 = subsample2(im1,PAR.pwr);

PAR.imgres = size(im1);
PAR.imgres_crop = PAR.imgres;

MAXbuffer = 0;%1024 - 878;

%----------------------
% Plot Movie Frame or Rendered model image
%----------------------
imagesc(im1);
colormap gray

ax1 = gca;

set(ax1,'units','pixels','visible','off','position',[0 0 PAR.imgres(2) PAR.imgres(1)].*plotscale);
axis normal

%------------------------------------------
% Overlay the Model Mesh and other Features as described
%------------------------------------------
 
impos = get(ax1,'position');
ax2 = axes('position',[0 0 .5 .5]);
color = {'k','b','w','c'};
headpts = zeros(length(solidx),2);
tailpts = headpts;
%Changing colors over time
% map = colormap('winter');
% colors = [interp1(1:size(map,1),map(:,1),linspace(1,size(map,1),length(solidx)))
%     interp1(1:size(map,1),map(:,2),linspace(1,size(map,1),length(solidx)))
%     interp1(1:size(map,1),map(:,3),linspace(1,size(map,1),length(solidx)))]';

for r = 1:length(solidx)
    frame = solidx(r);
    % ------------------------------------
    % Load the solution data
    load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(frame) '.mat']);
    load([PAR.solutionpath 'Features_' PAR.stub '/Features' num2str(frame) ...
        '.mat']);
    
    %ttime(r) = InternalVariablesDS.etime;
    %----------------------------
    % Evaluate Model at solution
    %----------------------------
    % Manual fit solution at first frame
    % sol = reshape(ManualFit(frame).soln',[],1)';

    % Predicted solution at current frame
    sol1 = InternalVariablesDS.xh_';

    % Actual solution
    sol = xh';
%     if r == 1
%         tt = sol(9:10);
%     end

    [x,y,z,Frenet,s,t] = model_shell_velren(sol,0,PAR);
%     [x,y,z,Frenet,s,t] = model_shell_velren([sol(1:8) tt 0],0,PAR);
    
%     K(:,r) = kappa;

    % load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(frame-1) '.mat']);
    % sol1 = xh';%InternalVariablesDS.xh_';
    [x1,y1,z1,Frenet,s,t] = model_shell_velren(sol1,0,PAR);

    %load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(frame) '.mat']);
    
    for k = PAR.fishnum
        hold on;

        % Either plot the model outline
        %   plotpts = [x{k}(1,1) y{k}(1,1)
        %              x{k}(1,2) y{k}(1,2)
        %              x{k}(1,3) y{k}(1,3)
        %              x{k}(2:end-1,3) y{k}(2:end-1,3)
        %              x{k}(end,3) y{k}(end,3)
        %              x{k}(end,2) y{k}(end,2)
        %              x{k}(end,1) y{k}(end,1)
        %              x{k}(end-1:-1:2,1) y{k}(end-1:-1:2,1)
        %              x{k}(1,1) y{k}(1,1)];
        %   surf_h(k) = plot(plotpts(:,1),plotpts(:,2)-MAXbuffer,'color',color{k},'linewidth',3);

        % Or the model mesh
        %   load fishtexmap
        %   warp(x{k},y{k},z{k},fishtexmap);
        %   set(gca,'ydir','normal');
        %   surf_h(k) = surf(ax2,x{k},y{k},z{k},'facecolor', ...
        %                    'none','edgecolor',color{k},'linestyle','--','linewidth',.5);
        %   surf_h(k) = surf(ax2,x1{k},y1{k},z1{k},'facecolor', ...
        %                    'none','edgecolor','w','linestyle','-','linewidth',2);

        %hh = EBmeshplot(fignum,x1{1},y1{1},{2,color{k},'--'},{.5,color{k},'--'});
%         hh = EBmeshplot(fignum,x{1},y{1},{2,'w','-'},{.5,'w','-'});
%         [xx,Frenet,theta,kappa,dum1,dum2,Sroot] = refitCenterline(x{k}(:,2),y{k}(:,2),1,10,PAR.len(:,k));
        xx = [x{k}(:,2) y{k}(:,2)];
        headpts(r,:) = xx(end,:);
        tailpts(r,:) = xx(1,:);
        plot(ax2,xx(:,1),xx(:,2),'color',color{k},'linewidth',.5);
%         plot(ax2,xx(:,1),xx(:,2),'color',colors(r,:),'linewidth',.5);
%         zerocurx = interp1(s{1},xx(:,1),Sroot);
%         zerocury = interp1(s{1},xx(:,2),Sroot);
%         plot(ax2,zerocurx,zerocury,'r*','markersize',10,'linewidth',2);
        %   mdlpts = [x{k}(1,1) y{k}(1,1)
        %             x{k}(1,2) y{k}(1,2)
        %             x{k}(1,3) y{k}(1,3)
        %             x{k}(end,1) y{k}(end,1)
        %             x{k}(end,2) y{k}(end,2)
        %             x{k}(end,3) y{k}(end,3)
        %             x{k}(2:end-1,1) y{k}(2:end-1,1)
        %             x{k}(2:end-1,3) y{k}(2:end-1,3)];
        % mdlpts = [x{k}(end,1) y{k}(end,1)
        %     x{k}(end,2) y{k}(end,2)
        %     x{k}(end,3) y{k}(end,3)
        %     x{k}(1:end-1,1) y{k}(1:end-1,1)
        %     x{k}(1:end-1,3) y{k}(1:end-1,3)];
        mdlpts = [x1{k}(end,1) y1{k}(end,1)
            x1{k}(end,2) y1{k}(end,2)
            x1{k}(end,3) y1{k}(end,3)
            x1{k}(1:end-1,1) y1{k}(1:end-1,1)
            x1{k}(1:end-1,3) y1{k}(1:end-1,3)];
        %   if isfield(Features(frame),'DataptsFull')
        %       datapts = Features(frame).DataptsFull{k};
        %   end
        if isfield(Features(frame),'DataptsFullIC')
            datapts = Features(frame).DataptsFullIC{k};
        end

        %   for r = 1:size(mdlpts,1)
        %       if ~isempty(Features(frame).Edgepts{r,k})
        %           plot(Features(frame).Edgepts{r,k}(:,1),Features(frame).Edgepts{r,k}(:,2),'r.')
        %       end
        %   end
        %   plot(Features(frame).All_Bndy_Points(:,1), ...
        %          Features(frame).All_Bndy_Points(:,2),'b.');

        if cptsTag == 1;
            %--- Plot Correspondence between points
            for j = 1:size(datapts,1)
                if datapts(j,:) == [0 0]
                    %This point is occluded. Plot as yellow star.
                    plot(mdlpts(j,1),mdlpts(j,2),'y*')
                else
                    %plot([mdlpts(j,1) datapts(j,1)],[mdlpts(j,2) datapts(j,2)], ...
                    %     [color{k} 'o:'],'markerfacecolor','k');
                    %         plot([mdlpts(j,1) datapts(j,1)],[mdlpts(j,2) datapts(j,2)], ...
                    %              [color{k} '-']);
                    %          plot(mdlpts(j,1),mdlpts(j,2),[color{k} 'o'],'markerfacecolor',color{k});
                    plot(datapts(j,1),datapts(j,2),[color{k} 'o'],'markerfacecolor','k','markersize',10,'linewidth',2);
                end
            end

            %--------------
            % Plot all the boundary points and high cucrvature points
            %--------------
            %     plot(Features(frame).All_Bndy_Points(:,1), ...
            %          Features(frame).All_Bndy_Points(:,2),'b.');
            %
            %     plot(Features(frame).Candpts(:,1), ...
            %          Features(frame).Candpts(:,2),'rs','markersize',10);

            if (isfield(Features,'Z1') && ~isempty(Features(frame).Z1))
                plot(Features(frame).Z1(:,1), ...
                    Features(frame).Z1(:,2),'c^','markersize',10);
            end
        end

        %--- Determine the index of points that are not occluded
        %notocc = setdiff(1:size(mdlpts,1),Features(frame).occluded_idx{k});

        %--- Or just set it to the entire point set
        notocc = 1:size(mdlpts,1);


        if nrmlTag == 1
            %--------------------------------------------
            % Plot all the outward normal vectors from the model and the
            % data
            %---------------------------------------------
            quiver(Features(frame).All_Bndy_Points(:,1), ...
                Features(frame).All_Bndy_Points(:,2),Features(frame).All_Nrml_Vectors(:,1),...
                Features(frame).All_Nrml_Vectors(:,2),1.0)

            tail_vec = [-1.*Frenet(k).T(1,:)];
            head_vec = [Frenet(k).T(end,:)];
            %dorsal_vec = [-1.*Frenet(k).N(2:end-1,:)];
            %ventral_vec = [Frenet(k).N(2:end-1,:)];
            dorsal_vec = [-1.*Frenet(k).N(1:end-1,:)];
            ventral_vec = [Frenet(k).N(1:end-1,:)];

            Nrml = [head_vec
                head_vec
                head_vec
                dorsal_vec
                ventral_vec];


            %      Nrml = [tail_vec
            %              tail_vec
            %              tail_vec
            %              head_vec
            %              head_vec
            %              head_vec
            %              dorsal_vec
            %              ventral_vec];


            quiver(mdlpts(notocc,1),mdlpts(notocc,2),Nrml(notocc,1),Nrml(notocc,2),1.0,'y');

            %Code to Grab the image intensities under the model of a particular
            %model to use in texture mapping
            if k == 0
                fishtexmap = zeros(PAR.L,50);
                for r = 1:size(x{k},1)
                    xx = [x{k}(r,2) y{k}(r,2)];
                    NN = Frenet(k).N(r,1:2);

                    RR = PAR.pixpermm*radiusspline1(s{k}(r),PAR.fishradius(:,k),[],0,PAR.len(:,k));
                    lambda = linspace(-RR,RR,50);
                    Xsamp = repmat(xx,length(lambda),1) + ...
                        repmat(lambda,2,1)'.*repmat(NN,length(lambda),1);

                    fishtexmap(r,:) = getIntensity(Xsamp,im1);
                end
            end
        end

        if  KF_tag == 1
            %------------------------------
            % Plot the predicted observation points and their associated
            % uncertainty ellipses from observation covariance matrix
            %-------------------------------
            i = frame;
            ObsDIM = 2;
            %obsdim = (size(mdlpts,1) -
            %length(Features(i).occluded_idx{k}))*ObsDIM;
            obsdim = (size(mdlpts,1))*ObsDIM;
            idx1 = (k-1)*obsdim + 1;
            idx2 = k*obsdim;

            P_y = InternalVariablesDS.Py_(idx1:idx2,idx1:idx2);
            %P_y = InternalVariablesDS.Py_;

            % Create 2x2 covariance matrices for each model point , 'Ry'
            maindiag = diag(P_y,0);
            maindiag = reshape(maindiag,2,[]);

            updiag = diag(P_y,1);
            %Take every other one in off-diagonal covariance
            updiag = updiag(1:2:end);
            Ry = zeros(2,2,size(P_y,1)/2);
            for m = 1:size(Ry,3)
                Ry(:,:,m) = diag(maindiag(:,m));
                Ry(2,1,m) = updiag(m);
                Ry(1,2,m) = updiag(m);
            end

            Zhat = InternalVariablesDS.Yh_(idx1:idx2,:);
            Zhat = reshape(Zhat,2,[])';
            Y = gatebndy(Zhat,Ry,PAR.gamma);

            for m = 1:length(notocc)
                plot(ax2,Y{notocc(m)}(:,1),Y{notocc(m)}(:,2),'w-','linewidth',2);
            end

            plot(Zhat(:,1),Zhat(:,2),'w*');
        end

        if  KF_tag1 == 1
            %Grab the Normal Vectors
            tail_vec = [-1.*Frenet(k).T(1,:)];
            head_vec = [Frenet(k).T(end,:)];
            %dorsal_vec = [Frenet(k).N(1:end,:)];
            %ventral_vec = [-1.*Frenet(k).N(1:end,:)];
            dorsal_vec = [-1.*Frenet(k).N(1:end-1,:)];
            ventral_vec = [Frenet(k).N(1:end-1,:)];

            Nrml = [head_vec
                head_vec
                head_vec
                dorsal_vec
                ventral_vec];

            Nrml = Nrml(:,1:2);
            %------------------------------
            % Plot the predicted observation points and their associated
            % uncertainty ellipses from observation covariance matrix
            %-------------------------------
            i = frame;
            ObsDIM = 2;
            %obsdim = (size(mdlpts,1) -
            %length(Features(i).occluded_idx{k}))*ObsDIM;
            obsdim = size(mdlpts,1);
            idx1 = (k-1)*obsdim + 1;
            idx2 = k*obsdim;

            ptidx1 = (k-1)*size(mdlpts,1)*ObsDIM + 1;
            ptidx2 = k*size(mdlpts,1)*ObsDIM;

            P_y = InternalVariablesDS.Py_IC(idx1:idx2,idx1:idx2);
            %P_y = InternalVariablesDS.Py_;

            maindiag = diag(P_y,0);

            NRy = maindiag;

            Zhat = InternalVariablesDS.Yh_IC(ptidx1:ptidx2,:);
            Zhat = reshape(Zhat,2,[])';
            PredBdypts = Zhat;
            for n = 1:size(PredBdypts,1)
                r = n;
                NN = Nrml(r,:);
                RR = 10;% sqrt(NRy(n));
                lambda = linspace(-RR,RR,30);
                X = repmat(PredBdypts(n,:),length(lambda),1) + repmat(lambda,2,1)'.*repmat(NN,length(lambda),1);
                hold on
                plot(X(:,1),X(:,2),'r-','linewidth',1);


                %            if ~isempty(Features(frame).Edgepts{r,k})
                %                plot(Features(frame).Edgepts{r,k}(:,1),Features(frame).Edgepts{r,k}(:,2),'o','color',color{k})
                %            end
            end
        end
        
    end
   
end

plot(headpts(:,1),headpts(:,2),'y',tailpts(:,1),tailpts(:,2),'m')
view([0 90]);

% Add 0.5 so that the grid lines up correctly with pixels since
% Xlim & Ylim of image axis go from 0.5 to max+0.5. It's minus for
% Ylim since we flip the yaxis.

 %-- Create moving frame with 
% $$$     scale = 1/2;
% $$$     ctr = sol(9:10);
% $$$     dxx = PAR.imgres(2)*scale*0.5;
% $$$     dyy = PAR.imgres(1)*scale*0.5;
% $$$     set(ax1,'xlim',[ctr(1)-dxx ctr(1)+dxx+0.5],'ylim',fliplr(PAR.imgres(1)-[ctr(2)-dyy-0.5 ctr(2)+dyy-0.5]));
% $$$     set(ax2,'color','none','xlim',[ctr(1)-dxx ctr(1)+dxx+0.5],'ylim',[ctr(2)-dyy-0.5 ...
% $$$                         ctr(2)+dyy-0.5]); 

set(ax2,'units','pixels','fontsize',12,'position',impos,'color','none','xlim',...
	[0.5 PAR.imgres(2)+0.5],'ylim',[-0.5 PAR.imgres(1)-0.5],'visible','off');

figure(fignum);
set(fignum,'units','pixels','position',[20+1280 20 PAR.imgres(2) PAR.imgres(1)].*plotscale);
%title(['Frame ' num2str(frame) ', IC: p0 = ' num2str([sol(1:8)]) ])
% text(PAR.imgres(2)/2 - 50,PAR.imgres(1)-20,['Frame ' num2str(frame)], ...
%      'fontsize',20);

if levelsettag == 1
  figure(fignum+1)
  imagesc(Features(frame).IM); 
  colormap gray
  hold on;
  imcontour(flipud(Features(frame).phi),[0 0],'g')
  axis image
  hold off
end

%zoom2(1,'in');
if uncertainty_plot == 1
  %=================================
  % Plotting state variance time evolution
  %=================================
  for  k = 1:length(sol)
    if k < 9
      labelM{k} = ['\alpha' num2str(k)];
    elseif k == 9
      labelM{k} = 'dx';
    elseif k == 10
      labelM{k} = 'dy';
    elseif k == 11
      labelM{k} = '\eta_{1}';
    else
      labelM{k} = '\eta_{2}';
    end
  end
  Px = InternalVariablesDS.Px;
  c = 1:frame;
  figure
  for k = 1:size(Px,1)
    hold on;
    H(k)=plot(c,reshape(Px(k,k,1:frame),1,[]),'*-','color',rand(1,3));
    hold off
  end
  grid on
  legend(H,labelM);
end


