% plot_shape_metrics.m
%
% This programs makes various plots of the data produced by successful
% tracking of a video sequence
%
% PLOTS:
% magnitude/frequency
% angular acceleration
% curvature
% bend angle
% 
% 
%
% SAVES:
% Two new fields in the 'ManualFit' structure 'xMid' and 'yMid' which
% contain the x and y coordinates of the centerline over time.  These 
% coordinates are post-processed from the raw tracker estimates.  They are 
% stored as N x M matrices where 
% 
% 'N' is the number of time samples, and 
% 'M' is the number of samples along the fish's length
%
% These centerline points are used to calculate the COM displacement,  
% speed, and acceleration using the function 'plot_kine_metrics'
% 
%-----------------------
% wavespeed is saved in the 'ManualFit' structure too.
%
%
% code is divided into cells to calculate new plots without having to 
% recalculate the kinematics.  
%
% Make sure the logical flags like 'CalcWaveSpeed' & 'MakeErrorPlots' are
% set correctly
%
% See comments below.
%================================

% BEGIN:
%
%
% If a file "ManualFit" already exists in memory, this function will use it
% to plot the data.
%
% If not, it will query the user to select a sequence.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('ManualFit','var') == 1
    PAR = ManualFit.ImageData;
else
    [FileName,PathName] = uigetfile({'*.mat'},'Select "ManualFit" data file for the video sequence');
    load([PathName FileName]);
    PAR = ManualFit.ImageData;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is. 
PAR.L = 51;
PAR.dt = 1/500; % Frame Rate (Fontaine et al used 1/1500)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to true if you want to calculate the wavespeed estimates from the
% zero contours of the curvature plots
CalcWaveSpeed = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to true if you need to determine the appropriate cutoff frequency for
% the temporal smoothing of the curvature plots
CalcCutoffFreq = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to true if you want to plot the error between the smoothed and unsmoothed
% data (i.e., curvature and centerline location) to get a feel for the effect 
% of the smoothing process.  Define the figure numbers for the plots
MakeErrorPlots = true;
CurveErrFignum = 5;
MidlineErrFignum = CurveErrFignum + 1;





%We have different cut off frequencies for low pass filter on the curvature
%values.  These values were determined by visually inspecting the plots
%given in the cell titled "determine curvature cutoff frequency" 

%Stocksteif (3dpf = 300, 15dpf = 200 28dpf = 100)
%WildType (3dpf = 350, 15dpf = 200 28dpf = 180)
Fcutoff = 100;

% These axis ranges are used in the plots
%Curvature range
timeYlim = [0 140];
curYlim = [-10 10]; %3dpf
% curYlim = [-8 8]; %15dpf
% curYlim = [-7 7]; %28dpf

%Frequency Range
freqYlim = [0 150]; %Hz
MagYlim = [0 6];
MagYtick = 0:6;

%Acceleration range
accYlim = [-1.2e6 1.2e6]; %3dpf
% accYlim = [-2.5e5 2.5e5]; %15dpf
% accYlim = [-1e5 1e5]; %28dpf
%=====================================================================


%----------------------------------------------------
%Get Image data
%----------------------------------------------------

frames = [1 PAR.numframes]; % number of frames in movie

samplerate = 1;
movidx = frames(1):samplerate:frames(2);

interptag = 1;

%--------------------------------
% Define the Tracking Parameters
PAR.etamax = 0;
PAR.statedim = 11;

PAR.mdlpar = [8 ]; % length(PAR.mdlpar) should be equal to
                    % numfish.  It is the number of parameters
                    % used in the each fish model.
PAR.fishnum = [1 ];
PAR.numfish = length(PAR.fishnum);
PAR.pixpermm = ManualFit(1).pixpermm;
PAR.modelfun_H = @modcurvesplineP_display;
PAR.c = 4; %spline order
PAR.gamma = 10.59663;

% Segmentation threshold for background subtraction 
PAR.IMthresh = ManualFit.thresh; 

% For the post-processing, we only want to sample the fish model on a
% uniform grid.
PAR.pectoral = false; 

% Length parameters of fish [length of tail , length of head]
PAR.len = ManualFit(1).length(1:2,:)./(PAR.pixpermm);
% Radius parameters of fish (8 B-spline control points)
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts(:,PAR.fishnum));

%% Load solutions into memory
solidx = [1 length(movidx)];
soln1 = zeros(length(movidx),PAR.numfish*PAR.statedim);
SOLN = zeros(length(frames(1):frames(2)),PAR.numfish*PAR.statedim);
if interptag == 1
    for i=1:length(movidx)
        load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(movidx(i)) ...
            '.mat']);
        soln1(i,:) = xh(1:PAR.numfish*PAR.statedim)';
        clear xh InternalVariablesDS
    end
end

% Now, perform interpolation to calculate the state at in between frames
for k = 1:size(soln1,2)
    SOLN(:,k) = interp1(movidx,soln1(:,k),frames(1):frames(2),'spline')';
end


%% Recalculate the bend angle and curvature based on a refitted smooth 
% centerline that gets rid of curvature discontinuity at point of rigidity.

%Initialize
tic
disp('Refitting centerline ...')
Err = zeros(size(SOLN,1),PAR.L);
COA = zeros(size(SOLN,1),2);
COV = zeros(size(SOLN,1),2);

ManualFit.xMid = zeros(size(SOLN,1),PAR.L);
ManualFit.yMid = zeros(size(SOLN,1),PAR.L);

cur = zeros(size(SOLN,1),PAR.L);
the = zeros(size(SOLN,1),PAR.L);

len = PAR.len(:,PAR.fishnum);

chunkSize = round(size(SOLN,1)/10);
for i = 1:size(SOLN,1)
    if mod(i,chunkSize)==0
        disp([num2str(round(100*i/size(SOLN,1))) ' % ...'])
    end
    [x,y,z,Frenet,U,V,ctrl_ptsX,ctrl_ptsY] = model_shell_velren(SOLN(i,:),0,PAR);
    if i == 1
        [xx,Frenet,theta,kappa,sqerr,x0] = refitCenterline(x{1}(:,2),y{1}(:,2),1,10,len);
        Err(i,:) = sqerr';
    else
        [xx,Frenet,theta,kappa,sqerr,x0] = refitCenterline(x{1}(:,2),y{1}(:,2),1,10,len,x0);
        Err(i,:) = sqerr';
    end
    
    %Make Specific curvature
    cur(i,:) = sum(len).*kappa';
    the(i,:) = theta';
   
    %Save smoothed midline location
    ManualFit.xMid(i,:) = xx(:,1)';
    ManualFit.yMid(i,:) = xx(:,2)';
end

% Get rid of angle discontinuities
the = unwrap(the,[],1);
toc


%% Determine curvature cutoff frequency

Fs = 1/PAR.dt;

% This section of code is run to visualize the curvature frequency response
% and visually inspect the plot to determine the cutoff frequency.
if CalcCutoffFreq
    Magnitude = [];
    sig2calc = cur;
    
    % Remove DC values of the curvature signals
    meansub = mean(sig2calc,1);
    sig2calc = sig2calc - repmat(meansub,size(sig2calc,1),1);
    
    for k = 1:size(sig2calc,2)
        sig = sig2calc(:,k);
        L = length(sig);
        NFFT = 2^nextpow2(L);
    %     NFFT = L;
        Y = fft(sig,NFFT)/L;
    %     Y = fft(sig);
    %     Y(1) = [];
        f = Fs/2*linspace(0,1,NFFT/2);
    %     f = (1:NFFT/2)/(NFFT/2)*Fs/2;
        Magnitude(:,k) = 2*abs(Y(1:NFFT/2));
    %     Magnitude(:,k) = abs(Y(1:NFFT/2)).^2;
    %     Magnitude(:,k) = abs(Y(1:length(Y)/2)).^2;
        % hold on;
        % plot(f,Magnitude(:,k),'color',rand(1,3));
        % title(num2str(k));
        % pause
    end
    figure;
    plot(f,Magnitude);
    xlabel('frequency');
    ylabel('Magnitude');
    display('Visually determine the cutoff frequency from the Magnitude plot');
    display('Set "Fcutoff" to this value and rerun with "CalcCutoffFreq" set to false.');
    return
end


%% Smoothing the bend angle and curvature

% Now, smooth the bend angle and curvature in the temporal direction
% Cutoff frequency from fft on a sample signal

filter_order = 4;
[b a] = butter( filter_order,Fcutoff*(2/Fs));

%Copy the unsmoothed values so we can compare the difference
the_unsmooth = the;
cur_unsmooth = cur;

for k = 1:size(the,2)
    temp = the(:,k);
    the(:,k) = filtfilt(b,a,temp);
    temp = cur(:,k);
    cur(:,k) = filtfilt(b,a,temp);
end

%%
% Now calculate the frequency response of the filtered curvature during the 
% time when the fish undergoes continuous swimming. 
%
% ManualFit.contswimframes = [beg end] define the beginning and end of the 
% frames in the sequence where the fish undergoes continuous swimming.  This
% is determined by visually inspecting the curvature contour plot.

Magnitude = [];
% sig2calc = cur(15:81,(s <=0 ));
if isfield(ManualFit,'contswimframes')
    sig2calc = cur(ManualFit.contswimframes(1):ManualFit.contswimframes(2),:);
else
    sig2calc = cur;
    warning(['The frames that correspond to continuous swimming have not been\n '...
        'identified (ManualFit.contswimframes). The frequency plot is based\n '...
        'on all frames'],[]);
end

% Remove DC values of the curvature signals
meansub = mean(sig2calc,1);
sig2calc = sig2calc - repmat(meansub,size(sig2calc,1),1);

for k = 1:size(sig2calc,2)
    sig = sig2calc(:,k);
    L = length(sig);
    NFFT = 2^nextpow2(L);
    Y = fft(sig,NFFT)/L;
    f = Fs/2*linspace(0,1,NFFT/2);
    Magnitude(:,k) = 2*abs(Y(1:NFFT/2));
end

%------------------------------------------
% Calculate the dominant "swimming frequency", FF of the fish by taking a
% weighted average of the frequencies with the magnitude response.
[val,I] = max(Magnitude,[],1);
swimfreq = f(I);
FF = sum(swimfreq.*val)./sum(val);
%figure; plot(swimfreq,'.-')



%%
% Create error plots of the difference between filtered and unfiltered
% curvature and bend angle.

if MakeErrorPlots
    curerr = sqrt(mean((cur-cur_unsmooth).^2,1));
    % Flip the error so values go from head to tail
    curerr = fliplr(curerr);
    
    figure(CurveErrFignum); hold on;
    hh = plot(linspace(0,1,PAR.L),curerr,'b','linewidth',1);
    set(hh,'DisplayName','Age 5 days')
    axis tight
    xlabel('Normalized position along body axis');
    ylabel('Curvature RMS Error')

    Displaceerr = sqrt(mean(Err,1))./(PAR.pixpermm*sum(len));
    % Flip the error so values go from head to tail
    Displaceerr = fliplr(Displaceerr);
    
    figure(MidlineErrFignum); hold on;
    hh = plot(linspace(0,1,PAR.L),Displaceerr,'b','linewidth',1);
    set(hh,'DisplayName','Age 5 days')
    axis tight
    xlabel('Normalized position along body axis');
    ylabel('Centerline RMS Error')
end




%% Calculate the angular acceleration along the length of the fish
RTheta = the;

% Use the 'gradient' function instead of central differencing for the first 
% NN points
NN = 1;

[dum,Angveltmp] = gradient(RTheta,1,PAR.dt);
[dum,Angacctmp] = gradient(Angveltmp,1,PAR.dt);

begmid = 1+NN;
endmid = size(RTheta,1)-NN;

%Calculate accelerations in the middle using central differencing
MidAcc = (RTheta(begmid+NN:end,:) + RTheta(1:endmid-NN,:) - 2.*RTheta(begmid:endmid,:))./(NN*PAR.dt)^2;

AngAcc = [Angacctmp(1:NN,:)
    MidAcc
    Angacctmp(end-NN+1:end,:)];

%Frame # or time
time = (0:size(SOLN,1)-1)*PAR.dt*1000; %in milliseconds 
%time = 1:size(SOLN,1);

fishlen = linspace(0,1,PAR.L);
%fishlen = s;

ManualFit.fishLen = fishlen;
ManualFit.time = time;
ManualFit.cur = cur;
ManualFit.the = the;
ManualFit.AngVel = flipud(Angveltmp');
ManualFit.AngAcc = flipud(AngAcc');
save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');

break;
%% Make Contour Plots

% x and y labels for the contour plots


%================================================================
% For all of the contour plots, flip the orientation of the fishlen
% direction and plot the data so that the head of the fish is at zero.
% This follows the data convention for fish biology articles.
%================================================================

% ANGULAR ACCELERATION PLOT
figure;
contourf(fishlen,time,fliplr(AngAcc),5);
set(gca,'clim',accYlim,'box','off');
ch = colorbar;
set(ch,'clim',accYlim,'ylim',accYlim);
%ylabel('Frame #');
title([PAR.stub],'interpreter','none');
% 



% MAGNITUDE RESPONSE PLOT
figure;
contourf(fishlen,f,fliplr(Magnitude),15);
set(gca,'ylim',freqYlim,'clim',MagYlim,'box','off','PlotBoxAspectRatio',[2 1 1]);
ch = colorbar;
set(ch,'clim',MagYlim,'ylim',MagYlim,'ytick',MagYtick);
%ylabel('Frame #');
title([PAR.stub],'interpreter','none');


% CURVATURE PLOT
figure; 
contourf(fishlen,time,fliplr(cur),10);
set(gca,'clim',curYlim,'box','off');
ch = colorbar;
set(ch,'clim',curYlim,'ylim',curYlim);
%ylabel('Frame #');
title([PAR.stub],'interpreter','none');
%
%hold on; plot(fishlen(idx1)*ones(size(time)),time,'k','linewidth',2);


% BEND ANGLE PLOT
% figure; 
% contourf(fishlen,time,fliplr(the1),15);
% colorbar
% %ylabel('Frame #');
% title([PAR.stub],'interpreter','none');

%% Wave speed

%===============================================================
% Determine the wave speed of the fish

if CalcWaveSpeed
    clear xx yy
    figure;
    C = contour(fishlen,time,fliplr(cur),[0 0]);
    C = C'; %make row matrix
    %Add a column of zeros to know which cell this point was assigned to
    C = [C zeros(size(C,1),1)];

    %----------------------------
    % Parse through the contour data to create a matrix of all the 2D contour
    % points associated with the Zero contour

    parsecontour = 1;
    begidx = 1;
    numpts = C(begidx,2);
    ctidx = 1;
    maxnum = size(C,1);
    while parsecontour == 1
        ct{ctidx} = C(begidx+1:begidx+numpts,1:2);
        hold on; plot(ct{ctidx}(:,1),ct{ctidx}(:,2),'*')
        C(begidx+1:begidx+numpts,3) = ctidx;
        begidx = begidx+numpts+1;
        if begidx > maxnum
            break;
        end
        numpts = C(begidx,2);
        ctidx = ctidx+1;
    end


    %------------------------
    % Now, Select points at beginning and end of linear region of zero
    % contour.  Do this for 3 different lines (numlines) and take the average
    % of the slopes of these lines to estimate wave velocity.

    numlines = 3;
    for k = 1:numlines

        for r = 1:2
            %         zoom on
            %         pause
            %         zoom off
            [xt,yt] = ginput(1);
            %         zoom out
            xx(r,:) = xt;
            yy(r,:) = yt;
        end
        ptidx = kdtreeidx(C(:,1:2),[xx yy]);
        %     for r = 1:length(xx)
        %         [dum,ptidx(r)] = min(sqrt(sum((C(:,1:2) - repmat([xx(r) yy(r)],size(C,1),1)).^2,2)));
        %     end

        ptidx = sort(ptidx);
        linpts = C(ptidx(1):ptidx(2),1:2);

        %Fit a line through the contour points
        [P,S] = polyfit(linpts(:,1),linpts(:,2),1);
        xtmp = linspace(linpts(1,1),linpts(end,1),100);
        [ytmp,delta] = polyval(P,xtmp,S);

        % Plot the fitted line and calculate R^2 value for fit.

        Rsq = 1 - sum((linpts(:,2) - polyval(P,linpts(:,1))).^2)*(1/(size(linpts,1)-1)) / var(linpts(:,2));
        hold on; plot(linpts(:,1),linpts(:,2),'*',xtmp,ytmp,'-','linewidth',2);

        % Save the data from the linear fit
        wavespeed.pts{k} = linpts;
        wavespeed.numpts(:,k) = size(linpts,1);
        wavespeed.Rsq(:,k) = Rsq;
        wavespeed.P(:,k) = P';
        wavespeed.c(:,k) = 1/(P(1)/1000); %Body lengths per second
    end

    wavespeed.freq = FF;
    wavespeed.lambda = mean(wavespeed.c)/FF;
    ManualFit(1).wavespeed = wavespeed;

    save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
end

