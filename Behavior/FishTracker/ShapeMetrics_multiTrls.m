
stimDir = 'S:\Avinash\Ablations and behavior\Intermediate RS\20160901\Behavior\Fish4_ctrl2\fastDir_09-02-16-011042\dark';

trlDirs = GetFilenames(stimDir,'searchStr','Trl','ext','/');
clear data
data(length(trlDirs)) = struct;
delInds = zeros(length(trlDirs),1);
for jj = 1:length(trlDirs)
    pathToTrlDir = fullfile(stimDir,trlDirs{jj});
    subDir = GetFilenames(pathToTrlDir,'searchStr','fish','ext','/');
    if ~isempty(subDir)
        subDir = subDir{1};
        pathToMan  = fullfile(pathToTrlDir,subDir);
        nFiles = length(dir(pathToMan));
        if nFiles >= 750
            disp(['Loading ' num2str(trlDirs{jj}) '...'])
            manFile = GetFilenames(pathToMan,'searchStr','ManualFit');
            load(fullfile(pathToMan,manFile{1}));
            parFile = GetFilenames(pathToMan,'searchStr','PAR');
            load(fullfile(pathToMan,parFile{1}));
            if isfield(ManualFit,'cur')
                try
                data(jj).cur = ManualFit.cur;
                data(jj).time = ManualFit.time;
                data(jj).fishLen = ManualFit.fishLen;
                data(jj).trlNum = trlDirs{jj};
                catch
                    disp('Recomputing shape metrics because of missing fields...')
                   [PAR,ManualFit] = ShapeMetrics(PAR,ManualFit);
                end
            else
                [PAR,ManualFit] = ShapeMetrics(PAR,ManualFit);
            end
        else            
            delInds(jj) = 1;
            disp(['No soln: skipping ' trlDirs{jj} '!'])
        end
    end
end

%% Delete empty trls
delInds = zeros(length(data),1);
for jj = 1:length(data)
    if isempty(data(jj).cur)
        delInds(jj) = 1;
    end
end
delInds = find(delInds);
data(delInds) = [];

%% Plot things
trlList = 1:length(data);
trlList = [1 2 4 5 6];
% trlList = [1 2];
figure('Name','Curvatures')
count = 0;
for jj = trlList
    count = count + 1;
    subaxis(length(trlList),1,count,'SpacingVert',0)
    imagesc(data(jj).time, data(jj).fishLen,cumsum(flipud(data(jj).cur'),1))
    ylabel(['Trl # ' num2str(jj)])
    colormap(viridis)
    box off
    if jj ~= trlList(end)
    set(gca,'xtick',[],'ytick',[],'clim',[-150 150])
    else
        xlabel('Time (ms)')
        ylabel('Fish len')
        set(gca,'clim',[-150 150])
    end
end


figure('Name','Total curvatures')
count = 0;
for jj = trlList
    count = count + 1;
    subaxis(length(trlList),1,count,'SpacingVert',0)
    plot(data(jj).time, sum(data(jj).cur',1))
    colormap(viridis)
    box off
    set(gca,'xtick',[],'ytick',[],'ylim',[-200 200])
end



