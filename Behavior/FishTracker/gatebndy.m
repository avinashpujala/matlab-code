function Y = gatebndy(z,Smat,gamma)

for n = 1:size(z,1)
  
x0 = z(n,1);
y0 = z(n,2);

S = inv(Smat(:,:,n));
t = linspace(0,2*pi,40)';

r = @(t) sqrt(gamma) ./ sqrt( S(1,1).*cos(t).^2 + (S(1,2)+S(2,1)).* ...
                              cos(t).*sin(t) + S(2,2).*sin(t).^2);

% $$$ r = @(t) ((2.*S(1,1).*x0 + (S(1,2) + S(2,1)).*y0).*cos(t) + ((S(1,2) + S(2,1)).*x0 + 2.*S(2,2).*y0).*sin(t)...
% $$$           + sqrt( ( (2.*S(1,1).*x0 + (S(1,2) + S(2,1)).*y0).*cos(t) + ((S(1,2) + S(2,1)).*x0 + 2.*S(2,2).*y0).*sin(t)).^2 -...
% $$$                   4.*(S(1,1).*x0.^2 + y0.*( (S(1,2) + S(2,1)).*x0 + S(2,2).*y0) ...
% $$$                      - gamma) .* (S(1,1).*cos(t).^2 + (S(1,2) + S(2,1)).* ...
% $$$                                  cos(t).*sin(t) + S(2,2).*sin(t).^2) ) ) ./ ...
% $$$     (S(1,1) + S(2,2) + (S(1,1) - S(2,2)).*cos(2.*t) + (S(1,2) + S(2,1)).*sin(2.*t));

R = r(t);

%convert back to cartesian
Y{n} = [R.*cos(t) R.*sin(t)] + repmat([x0 y0],length(t),1);


% $$$ xmin = x0 - sqrt( (-4*S(2,2)*gamma) / ((S(1,2) + S(2,1))^2 - ...
% $$$ 				      4*S(1,1)*S(2,2)) );
% $$$ xmax = x0 + sqrt( (-4*S(2,2)*gamma) / ((S(1,2) + S(2,1))^2 - ...
% $$$ 				      4*S(1,1)*S(2,2)) );
% $$$ 
% $$$ %xmin = x0 - (2*S(2,2)*gamma) / sqrt( S(2,2)*gamma*( -(S(1,2) + S(2,1))^2 + 4*S(1,1)...
% $$$ 	%				     *S(2,2)) );
% $$$ %xmax = x0 + (2*S(2,2)*gamma) / sqrt( S(2,2)*gamma*( -(S(1,2) + S(2,1))^2 + 4*S(1,1)...
% $$$ 	%				     *S(2,2)) );
% $$$ 
% $$$ X = linspace(xmin,xmax,20)';
% $$$ 
% $$$ Ymin = -(1/(2*S(2,2))) .* ( (S(1,2) + S(2,1)).*(X - x0) - 2*S(2,2)*y0 +...
% $$$ 			    sqrt( ((S(1,2) + S(2,1))^2 - 4*S(1,1)*S(2,2))...
% $$$ 				  .*(X - x0).^2 + 4*S(2,2)*gamma) ...
% $$$ 			    );
% $$$ 
% $$$ Ymax = (1/(2*S(2,2))) .* ( -(S(1,2) + S(2,1)).*(X - x0) + 2*S(2,2)*y0 +...
% $$$ 			    sqrt( ((S(1,2) + S(2,1))^2 - 4*S(1,1)*S(2,2))...
% $$$ 				  .*(X - x0).^2 + 4*S(2,2)*gamma) ...
% $$$ 			   );
% $$$ 
% $$$ 
% $$$ Y{n} = [X real(Ymin) ;flipud(X) real(Ymax)];
end
