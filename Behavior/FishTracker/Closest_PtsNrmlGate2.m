function [Features,NNrml,Yh_] = Closest_PtsNrmlGate2(Features,p,Pyfull,i,PAR)

%Features = Closest_PtsNrmlGate(Features,p,Yh_,Pyfull,i,PAR) finds the closest
%points in Features to the model evaluated at p0. It uses the
%outward normal information to create the correspondence.

%This version uses normal feature detection when it can't find a high
%curvature point for the fish

%This version is for use with the ReBEL Toolkit and the function
%modcurve8new

%This function uses a different point layout defined by modcurve8newP2


% Initialize IMDiff measurement vector
Z1 = [];
Z1vec = [];
 
%--- Transform back to cartesian lower left
if ~isempty(Z1)
  Z1 = [Z1(:,2) PAR.imgres(1)-Z1(:,1)];
end
Features(i).Z1 = Z1;

numfeatpts = 30;
htidx = [1];
Edgepts = cell(PAR.obsdim,PAR.numfish);

%Initialize output Normal cell and Likelihood matrices
Ry_all = cell(PAR.numfish,1);
NRy_all = cell(PAR.numfish,1);
mdlpts_all = cell(PAR.numfish,1);

OtherFishPhi = cell(PAR.numfish,1);

NNrml = cell(PAR.numfish,1);
LLCand = zeros(size(Features(i).Candpts,1)+size(Z1,1),PAR.obsdim,PAR.numfish);
LLBdy = zeros(numfeatpts,PAR.obsdim,PAR.numfish);

% %Create a rendered image of the current model
% [x,y,z,Frenet,s,t,ctx,cty,MASKtmp] = model_shell_velren(p,0,PAR);
% 
% %Create SDF's of all other fish present except the current one
% for k = 1:PAR.numfish
%     tmp = zeros(PAR.imgres);
%     for r = 1:PAR.numfish
%         if r ~= k
%             tmp = tmp + MASKtmp{r};
%         end
%     end
%     tmp(tmp > 1) = 1;
%     tmp = subsample2(tmp,PAR.pwr);
%     tmp(tmp == 0) = -1;
%     OtherFishPhi{k} = double((tmp > 0).*(bwdist(tmp < 0)-0.5) - (tmp < 0).*(bwdist(tmp > 0)-0.5));
%     %keyboard
% end

Yh_ = [];

%==============================================
%Iterate over each fish
for k = 1:PAR.numfish
  param = PAR.params((k-1)*PAR.paramdim+1:k*PAR.paramdim);
  state = p((k-1)*PAR.statedim+1:k*PAR.statedim);
  
  [x,y,z,ss,th,Nrml] = modcurve8newP2(state',param(1:end-2),param(end-1:end),PAR);
  
  idx1 = (k-1)*PAR.obsdim+1;
  idx2 = k*PAR.obsdim - PAR.Pixobsdim;
  Py = Pyfull(idx1:idx2,idx1:idx2);
  
  %Use either points from predicted state, or the weighted mean of
  %the observation function.
  
  %Translate and scale the model
  
  x = x.*PAR.pixpermm + state(end-2);
  y = y.*PAR.pixpermm + state(end-1);
  mdlpts = [x y];
  Yh_ = [Yh_ ; reshape(mdlpts',[],1)];
  mdlpts_all{k} = mdlpts;
  
  %Only 2 dimensions
  Nrml = Nrml(:,1:2);
  NNrml{k} = Nrml;
  
  %Construct 2D point covariances from full measurement covariance
  %matrix.
  maindiag = diag(Py,0);
  htdiag = reshape(maindiag(1:2*length(htidx)),2,[]);
  
  updiag = diag(Py,1);
  %Take every other one in off diagonal covariance
  updiag = updiag(1:2:end);
  Ry = zeros(2,2,length(htidx));
  for n = 1:size(Ry,3)
    Ry(:,:,n) = diag(htdiag(:,n));
    Ry(2,1,n) = updiag(n);
    Ry(1,2,n) = updiag(n);
  end
  
  %These are the covariances associated with the normal projection
  NRy = maindiag(2*length(htidx)+1:end);
%   NRy = maindiag;
  
  Ry_all{k} = Ry;
  NRy_all{k} = NRy;
  
  Features(i).Model_Points{k} = mdlpts;
  
  Features(i).sizeofdata = size(mdlpts);
  
  %Initialize, I don't know why this works?
  Features(i).Datapts{k} = [];
  Features(i).Datavec{k} = [];
  Features(i).DataptsFull{k} = [];
  Features(i).DatavecFull{k} = [];
  Features(i).occluded_idx{k} = [];
  
  % -- First Perform a Normal vector check to see if inner
  % product is greater than beta (or less than Angle). beta(1) is
  % for the head/tail points and beta(2) is for the other
  % silhouette points.
  Angle1 = 60;
  Angle2 = 45;
  beta = [cosd(Angle1) cosd(Angle2)];
  
  % --- Second, for each measurement point find the points
  % that fall within a circle of radius gamma
  % Confidence values for Gate (Probability of finding the
  % measurement) fron chi-square distribution tables
  % 99.5% ==> 10.59663
  % 99% ==> 9.21034
  % 97.5% ==> 7.37776
  % 95% ==> 5.99146
  % 90% ==> 4.60517
  % 75% ==> 2.77259
  gamma = [20 5.99146];
  
  %Initialize
  ValidBin = struct([]);
  
  for n = 1:size(mdlpts,1)
    if any(n == 1)
      %datapts = Features(i).All_Bndy_Points;
      %nrmlvec = Features(i).All_Nrml_Vectors;
      datapts = Features(i).Candpts;
      nrmlvec = Features(i).Candvec;
      kappa = Features(i).CandKappa;
      % Add the IMdiff measurement if it exists
      if (~isempty(Z1) && k == 1)
        datapts(end+1:end+size(Z1,1),:) = Z1;
        % give the image diference point the normal vector of the model!
        nrmlvec(end+1:end+size(Z1,1),:) = Z1vec;
      end
    elseif any(n == [2:4]) 
      %datapts = Features(i).All_Bndy_Points;
      %nrmlvec = Features(i).All_Nrml_Vectors;
      datapts = Features(i).Candpts;
      nrmlvec = Features(i).Candvec;
      kappa = Features(i).CandKappa;
      % Add the IMdiff measurement if it exists
      if (~isempty(Z1) && k == 1)
        datapts(end+1:end+size(Z1,1),:) = Z1;
        % give the image diference point the normal vector of the model!
        nrmlvec(end+1:end+size(Z1,1),:) = Z1vec;
      end
    else
      xx = mdlpts(n,:);
      NN = Nrml(n,:);
      thresh = 100;
      C = [-0.25 -0.5 0 0.5 0.25]; 
      %C = fliplr([-0.25 -0.5 0 0.5 0.25]);
        
      %RR = 2*sqrt(NRy(n - htidx(end)));
      RR = 15;
      datapts = find_features(xx,NN,linspace(-RR,RR,numfeatpts),C,thresh,Features(i).IMfull);
      %datapts = find_features1(xx,NN,linspace(-RR,RR,numfeatpts),C,thresh,Features(i).IMfull,OtherFishPhi{k});
      Edgepts{n,k} = datapts;
    end
    
    % Initialize
    gamma_eval = zeros(size(datapts,1),1);
    nrml_dot_eval = zeros(size(datapts,1),1);
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % For each data point, calculate the Gating value
    % for the current model point.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if any(n == htidx)
        for m = 1:size(datapts,1)
            innov_tmp = datapts(m,:) - mdlpts(n,:);
            gamma_eval(m,:) = innov_tmp*inv(Ry(:,:,n))*innov_tmp';
            nrml_dot_eval(m,:) = nrmlvec(m,:)*Nrml(n,:)';


            LLCand(m,n,k) = (exp(-gamma_eval(m,:)/2) / ...
                (2*pi*sqrt(Ry(1,1,n)*Ry(2,2,n) - Ry(1,2,n)^2)));
            %*exp(-(1-nrml_dot_eval(m,:))^2);
        end

    else
        for m = 1:size(datapts,1)
            innov_tmp = (datapts(m,:) - mdlpts(n,:))*Nrml(n,:)';
            gamma_eval(m,:) = innov_tmp^2 / NRy(n - htidx(end));

            LLBdy(m,n,k) = (exp(-gamma_eval(m,:)/2) / (sqrt(2*pi)*sqrt(NRy(n - htidx(end)))));
            %*exp(-(1-nrml_dot_eval(m,:))^2);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Now, place those data points that satisfy the criteria in the
    % appropriate bin that corresponds to the appropriate model
    % point.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if any(n == htidx)
      idx = find( (gamma_eval <= gamma(1) & nrml_dot_eval > beta(1)) );
      ValidBin(n).pts = datapts(idx,:);
      %Set the likelihoods of points outside the validation gate to
      %zero.
      
      idx2rmv = setdiff(1:size(datapts,1),idx);
      LLCand(idx2rmv,n,k) = 0;      
    end
  end 
  
  %Points are occluded by default
  Features(i).DataptsFull{k} = zeros(size(mdlpts));
  Features(i).DatavecFull{k} = zeros(size(mdlpts));
end

% Now Create a 1-1 point correspondence for each data point by
% looking for the maximum likelihood across all model points of all
% worms

% Figure out which Data points are being shared by multiple worms
% and then only allow it to be associated with the worm with the
% maximum likelihood.
% Totwormidx = 1:PAR.numfish;
% for m = 1:size(LLCand,1)
%   [val,wormidx] = max(max(LLCand(m,:,:)));
%   wormidx2rid = setdiff(Totwormidx,wormidx);
%   LLCand(m,:,wormidx2rid) = 0;
% end

% $$$ for m = 1:size(LLBdy,1)
% $$$   [val,wormidx] = max(max(LLBdy(m,:,:)));
% $$$   wormidx2rid = setdiff(Totwormidx,wormidx);
% $$$   LLBdy(m,:,wormidx2rid) = 0;
% $$$ end

for k = 1:PAR.numfish
    % Assign the Boundary Points
    datapts = Features(i).All_Bndy_Points;
    nrmlvec = Features(i).All_Nrml_Vectors;

    % Assign the High Curvature Points
    datapts1 = Features(i).Candpts;
    nrmlvec1 = Features(i).Candvec;
    %datapts1 = Features(i).All_Bndy_Points;
    %nrmlvec1 = Features(i).All_Nrml_Vectors;
    occluded_idx = [];
    % Assign the datapoint to each model point that has the maximum
    % likelihood. keep track of occluded points with max likelihood =
    % 0
    for n = 1:size(mdlpts,1)
        if any(n == [1])
            %Make model Tail points share the same data association
            [val,ptidx] = max(LLCand(:,1,k));
                      
            if val > 0 %the point matches a model point
                Features(i).DataptsFull{k}(n,:) = datapts1(ptidx,:);
            else
                occluded_idx(end+1) = n;
            end
        elseif any(n == [2:4])
            %Make model head points share the same data association
            %[val,dataidx] = max(LLCand(:,4:6,k));
            [val,dataidx] = max(LLBdy(:,2:4,k));
            if length(dataidx) > 1
                %[val,modelidx] = max(max(LLCand(:,4:6,k)));
                [val,modelidx] = max(max(LLBdy(:,2:4,k)));
                ptidx = dataidx(modelidx);
            else
                %There is only 1 feature point
                ptidx = 1;
            end

            %HACK to make the HEAD use edge finder instead of curvature
            %points in Occluded matching later
            val = 0;
            if val > 0 %the point matches a model point
                Features(i).DataptsFull{k}(n,:) = datapts(ptidx,:);

            else
                occluded_idx(end+1) = n;
            end
        else
            [val,ptidx] = max(LLBdy(:,n,k));
            if val > 0 %the point matches a model point
                Features(i).DataptsFull{k}(n,:) = Edgepts{n,k}(ptidx,:);

            else
                occluded_idx(end+1) = n;
            end
        end
    end


    %==================================
    %For each fish, include a search for the occluded head/tail points
    %

    occidx = occluded_idx;
    mdlpts = mdlpts_all{k};
    Nrml = NNrml{k};
    Ry = Ry_all{k};
    NRy = NRy_all{k};
    for j = 1:length(occidx)
        %only for the head/tail points
        if (occidx(j) <= 4 && occidx(j) >= 2) %~isempty(occidx(j))
            
            %assign the point
            xx = mdlpts(occidx(j),:);
            NN = Nrml(occidx(j),:);
            thresh = 100;

            C = [-0.25 -0.5 0 0.5 0.25];
            
            %RR = 2*sqrt(NRy(occidx(j) - htidx(end)));
            %RR = 20; (fish03)
            RR = 10;            
            datapts_in = find_features(xx,NN,linspace(-RR,RR,numfeatpts),C,thresh,Features(i).IMfull);
            datapts = [datapts_in ];
            
            Edgepts{occidx(j),k}(end+1:end+size(datapts,1),:) = datapts;
            
            if ~isempty(datapts)
                Features(i).DataptsFull{k}(occidx(j),:) = kdtree(datapts,xx);
                % This point is no longer occluded
                occidx(j) = 0; %placeholder
                %otherwise, everything stays the same as before.
            end
        end
    end
    %Now, reset 'occluded_idx' to account for any placeholders I put
    %in
    occluded_idx = occidx((occidx ~= 0));

    Features(i).occluded_idx{k} = occluded_idx;

    obs_dumFull = Features(i).DataptsFull{k};


    %Now get rid of place holders!
    obs_dumFull(occluded_idx,:) = [];

    Features(i).Datapts{k} = obs_dumFull;

    Features(i).Edgepts = Edgepts;
    %Assign the pixel intensity values
    %Features(i).Datapix{k} = getIntensity(CtrPts,IMG);
end
