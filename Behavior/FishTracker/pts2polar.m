function A = pts2polar(X)

T = diff(X,1,1);
T = T./norm(T);

N = [-T(2) T(1)];

A(1) = atan2(N(2),N(1));

a = T(2)/T(1);
b = -a*X(1,1) + X(1,2);

A(2) = b / (sin(A(1)) - a*cos(A(1)));
