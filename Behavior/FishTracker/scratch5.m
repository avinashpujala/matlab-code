
I_smooth = FindFishPxlsForMidlineTracking_20180325(foo,'headDiam',headDiam,'pxlLen',pxlLen);


return
%%
imgDir = 'C:\Users\pujalaa\Documents\MATLAB\FreeSwimBehavior\Unsorted\testData';
fishImg = ReadImgSequence(imgDir);
fishImg = fishImg(:,:,1);

%%
params.head_trans = [92,92];
params.head_rot  =0;
params.trunk_rot = 0;
params.tail_rot = 0;
head_len = 18;

% params_init = params;
% params_init.head_trans = ceil(size(fp)/2);

% X= [];
% X(1:2) = params.head_trans;
% X(3) = params.head_rot;
% X(4) = params.trunk_rot;
% X(5) = params.tail_rot;

fp = ceil(size(fishImg)/2);
X0(1:2) = fp;
X0(3:5) = deal(0);

%% Optimization
clc
rng default % For reproducibility
% Aineq = [1 0 0 0;-1 0 0 0];
% Bineq = [size(img,1) 0 ]';
Aineq = [];
Bineq =[];
Aeq =[];
Beq =[];
lb = [fp(1)-head_len fp(2)-head_len 0 -60 -60];
ub = [fp(1)+head_len fp(2)+head_len 360 60 60];
nonlcon =[];

% N = 100;
% N = kerSize;
% N = ceil(size(img,1)/6);
opts = optimset('PlotFcn', @psplotbestf);

for kk = 1:5
objectiveFunc = @(X)FitBoxTrainToFish_gravity(X,head_len,fishImg);
[X_opts,fval] = patternsearch(objectiveFunc,X0, Aineq,Bineq,Aeq,Beq,lb,ub,nonlcon,opts);
disp(fval)
% X_opts = fmincon(objectiveFunc,X0, Aineq,Bineq,Aeq,Beq,lb,ub,nonlcon);

% Plot fit
params.head_trans = X_opts(1:2);
params.head_rot = X_opts(3);
params.trunk_rot = X_opts(4);
params.tail_rot = X_opts(5);
params.head_len = head_len;

[box_train,boxPts] = MakeBoxTrainFish(params);


figure('Name','Fitted box train fish')
[y,x] = find(fishImg);
plot(x,y,'.')
hold on
axis image
for jj = 1:length(box_train)
    plot(box_train{jj}(:,1),box_train{jj}(:,2),'o')
end
title(['Iter # ' num2str(kk)])
shg
end

%%  Global soln

objectiveFunc = @(X)FitBoxTrainToFish_gravity(X,head_len,fishImg);
opts = optimoptions(@fmincon,'Algorithm','sqp');
problem = createOptimProblem('fmincon','objective',objectiveFunc,...
    'x0',X0,'lb',lb,'ub',ub,'options',opts);
gs = GlobalSearch;
gs.NumTrialPoints = 1000;
gs.FunctionTolerance = 1e-10;
% gs.NumStageOnePoints = 1000;

for kk = 1:3

[X_opts,fval] = run(gs,problem);
disp(fval)
% [X_opts,fval] = run(ms,problem,30);

% Plot fit
params.head_trans = X_opts(1:2);
params.head_rot = X_opts(3);
params.trunk_rot = X_opts(4);
params.tail_rot = X_opts(5);
params.head_len = head_len;

[box_train,boxPts] = MakeBoxTrainFish(params);

figure('Name','Fitted box train fish')
[y,x] = find(fishImg);
plot(x,y,'.')
hold on
axis image
for jj = 1:length(box_train)
    plot(box_train{jj}(:,1),box_train{jj}(:,2),'o')
end
shg
end

