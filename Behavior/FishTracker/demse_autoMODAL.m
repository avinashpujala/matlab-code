% DEMSE_autoMODAL  Demonstrate state estimation of planar zebrafish
% larvae parameterized by B-Spline Bend Angle. 
%
%
%   See also in this script:
%   auto_init - initializes the tracker by calculating calibration,
%   background, and model shape
%   
%   gssm_fishOcc - defines the motion and measurement model of the fish
%
%   srcdkf - performs the estimation
% 
%   More details are available in each file
%   


%% START
close all;
clear all;

cd('C:\Users\pujalaa\Documents\MATLAB\FreeSwimBehavior\FishTracker')


% Add the subdirectories that contain important / necessary files
addpath('mex/');
addpath('core/');


fprintf('\nDEMSE_MODAL : This demonstration performs state estimation\n');
fprintf('         for a planar zebrafish whose centerline bend angle is\n');
fprintf('         parameterized by 8 B-spline control points\n'); 
fprintf(['         The pixel observations are corrupted by additive\' ...
         'n']);
fprintf('         white Gaussian noise.\n\n');


trlDir = 'S:\Avinash\Ablations and behavior\Alx\2018\Jan\20180128-AlxKaede-conv 24hpf-abl old 60hpf-behav 5dpf\f1_ctrl\fastDir_01-28-18-232325\dark';

%% START COMMONLY CHANGED PARAMETERS

% These are the only values that you should need to change.

% 'PAR.numframes' is equal to the number of frames the movie sequence has
%
% 'PAR.startframe' is the frame you want to start the tracker from.  If 'getIC'
% is set to true, it is also the frame where the initialization is
% calculated.
%
% 'PAR.endframe' is the last frame you want to track
%

%
% For example, if you performed initialization on frame 25, and tracked to
% the end: 
% getIC = true;
% PAR.startframe = 25;
% PAR.endframe = 100;
% PAR.numframes = 100;

 
% <html>
% <table border=1><tr><td>one</td><td>two</td></tr></table>
% </html>
% 
% Then, go back and run this program with
% getIC = false;
% PAR.startframe = 24;
% PAR.endframe = 1;
% PAR.numframes = 100;
% PAR.framesample = -1;

%Set to true if the Video data needs to be selected because no 'ManualFit'
%file exists for the video sequence you want to analyze.
getImageData = true;

% Set to true if you haven't calculated calibration for this particlar data
% Calibration value is stored in directory where calibraiton image is
getCalibration = true;

% When 'getIC' is set to 'true', this program will calculate and save a
% structure named 'ManualFit' that contains all the tracking parameters
% associated with a particular video sequence.  It saves it in the same
% directory that the solution data is located.
%
% When 'getIC' is set to 'false', you have already calculated the initial
% parameters, so the program just loads them from disk.

% Set to true if you need to calculate shape of fish at start frame.
getIC = true;

% Distance between the 2 clicked points in mm in the calibration image
calLength = 50; % 50 for diameter of arena (AP)

% PAR is a static structure that defines various parameters of the tracking
% sequence
global PAR
% 'PAR.framesample' is the time step for the tracker.  This will almost
% always be set to 1.  However, if you want to track backwards in time, it
% would be set to -1.
PAR.framesample = 1;

PAR.startframe = 1; %(AP commented because this is  now being read automatically in LoadVideo.m);

%PAR.ICframe is the frame number that we initialize the tracker on.  It
%would be identical to 'startframe' when first starting a video, but not on
%subsequent restarts.
% PAR.ICframe = 1;
PAR.ICframe = PAR.startframe;

% PAR.pectoral = true if you DO NOT want to sample the model in the 
% pectoral region
%
% PAR.pectoralRegion defines the percentages along total body length
% (starting from the tail) where the model is NOT sampled.
PAR.pectoral = true;
PAR.pectoralRegion = [0.65 0.72];

%Frame interval of the camera
PAR.dt = 1/500;  

ManualFit.pixpermm = -1; % (-1, x, or [])
% -1 results in autodetection of arena edge and calibration (AP)
% 

backGroundMethod = 'BG'; %('BG','BG_auto','BG_done');
% BG - Background obtained by erasing fish
% BG_auto - Mean image used as background
% BG_done - When images are already background subtractd uses minimum
%   intensity projection as background

%END COMMONLY CHANGED PARAMETERS
%======================================================

%% GET IMAGE DATA
% -------------------------------------------------------------------

if getImageData    
%     PAR = LoadVideo(PAR); 
    PAR = LoadVideo(PAR); % If called from an external script (scratch4 - AP)
        
    ManualFit.ImageData = PAR;
    
    % Make two directories to save the estimated state and the features into
    % if they don't already exist
    if exist([PAR.solutionpath ['fish_' PAR.stub]],'dir') ~= 7
        mkdir(PAR.solutionpath,['fish_' PAR.stub]);
        mkdir(PAR.solutionpath,['Features_' PAR.stub]);
    end

    save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
else
    %Load the previously stored data
    [FileName,PathName] = uigetfile({'*.mat'},'Select "ManualFit" data file for the video sequence');
    load([PathName FileName]);
    
    % Check that the paths stored in 'ManualFit' match the location that you
    % just selected the file from
    % If 75% of the paths match , then we'll assume everything's okay.
    endd = round(.75*length(ManualFit.ImageData.solutionpath));
    
    if strcmp(PathName(1:endd),ManualFit.ImageData.solutionpath(1:endd)) == 0
        %if different, run Loadvideo routine
        warning('The directories stored in ManualFit structure do not match its current location.\n You are prompted to relocate the directories',[]);
        
        PAR = LoadVideo(PAR);
        ManualFit.ImageData = PAR;
        save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
    else
        PAR = ManualFit.ImageData;
    end
end

PAR.endframe = PAR.numframes;


%% ====================================================
% START ADVANCED PARAMETERS
% These are the only values that you should need to change.
%=====================================================

%Change this depending on how many fish you are tracking
PAR.fishnum = 1;  %[1 2];
PAR.numfish = length(PAR.fishnum);


%This is the percentage of the fish's body in the head region that is
%stiff.  For most fish, it is set to 0.2, but for the 28dpf stocksteif, I
%set it to 0.4
PAR.Gamma = 0.2;

%Number of parameters of the model (i.e. 8 control points)
PAR.mdlpar = 8*ones(1,PAR.numfish);

%Function Handle that makes the model mesh.
PAR.modelfun_H = @modcurvesplineP;

%spline order
PAR.c = 4; 

%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is. 
PAR.L = 30;

PAR.etamax = 0;
PAR.statedim = PAR.mdlpar(1) + 3; %3 includes translation and wave velocity 
PAR.pNoisedim = PAR.statedim;


% Dimension of the parameters that define the model shape
% for fish its [R L] (i.e. PAR.paramdim = length([R L])
%
% R = 20 control points that define the B-spline function for the
% radius function R(u); size(R) = [1 20];
PAR.R = 20;

% L = [Length of tail region , Length of body/head region]; size(L) = [1 2];
PAR.paramdim = 22;

%--------------------------------------------------------
% Distance from model boundary in pixels to search for matching edge
% feature points.  For instance, 15 searches 15 pixels inside and outside
% the model boundary in the normal direction at each point (see Figure 4 in
% JEB paper)
PAR.FeatFind = 20; % Default = 15

%Threshold to classify measurement point as edge feature point. (No need to 
%change for measurement images that are binary).  
PAR.Feat_Thresh = 100;

%--------------------------------------------------------
% This is the maximum number of iterations allowed. 
PAR.itermax = 30;

% When the error between the image points and model drops below this value,
% the iteration stops and takes the current value as the solution.
PAR.error_thresh = 100;

% This is the number of iterations beyond the current local minimum the
% algorithm will iterate to search for a new local minimum.  For instance,
% assume that after 2 iterations, a local minimum to the error function is
% found.  If 'PAR.IterBack = 3', the algorithm with iterate up to 3 more times
% to search for a new local minimum.  If it doesn't find one, it quits,
% otherwise, it uses this new solution and continues.
PAR.IterBack = 2;

%--------------------------------------------------------
% These are the measurement variance parameters from the Kalman filter 
% formulation. We assume that the edge feature measurements are corrupted by
% additive noise drawn from a normal distribution with this variance.
% Units are pixels^2.  I have a slightly lower variance for the 3 points in 
% the model corresponding to the head.
PAR.HeadObsVar = 0.25;
PAR.BodyObsVar = 0.5;

%--------------------------------------------------------
% These are the process noise parameters for the dynamic model of the fish.
% They correspond to the standard deviation of the normal distribution
% where the noise vector is drawn from.
%
% The first corresponds to the amount of bending noise to add to the
% b-spline shape parameters (units = degrees).
PAR.SigmaDeg = 2.5;
%
% The second parameter corresponds to the amount noise added to the
% translation parameters (units = pixels).
PAR.DeltaT = 1.3;
%
% The last parameter corresponds to the acceleration noise for the axial 
% displacement (units = mm/s^2)
PAR.DeltaEta2 = 3535;


% END ADVANCED PARAMETERS
%=====================================================


%% Get the Calibration if necessary
if ManualFit.pixpermm == -1 %(Overrides getCalibration = true)
    getCalibration = false;
end

if getCalibration
    %--------------------------
    % This is the length in mm of the distance you click points between
    % on the calibration image.
    PAR.length_in_mm = calLength; % (Arena diam = 50mm, so click from edge to edge along diam)
    
    ManualFit = auto_init(ManualFit,PAR,'calib');
    PAR.pixpermm = ManualFit.pixpermm;
    save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
    save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');
else
    %The calibration may have been done, but it's not saved with this
    %video's data structure.
    
    %Does the 'ManualFit' structure have a field with the scaling?
    if isfield(ManualFit,'pixpermm') == 0        
        %Locate the calibration file and store it with the data structure
        [FileName,PathName] = uigetfile({'*.mat'},'Select the calibration data file for this video sequence');
        load([PathName FileName]);
        ManualFit.pixpermm = pixpermm;
        PAR.pixpermm = ManualFit.pixpermm;
        save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
        save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');  
    end
    
    %otherwise, pixpermm field exists in ManualFit
end




%% Get the initial shape and orientation if necessary
if getIC
    %----------------------------------------------------
    %Calculate Background
    %----------------------------------------------------
    ManualFit = auto_init(ManualFit,PAR, backGroundMethod);
    %     ManualFit = auto_init(ManualFit,PAR,'BG_auto');
    
    if ManualFit.pixpermm == -1
        pxlLen = GetPxlSpacing(ManualFit.BG,'diam',calLength);
        ManualFit.pixpermm = round(1/pxlLen);
        PAR.pixpermm = ManualFit.pixpermm;
    end
    
    %----------------------------------------------------
    % Now, extract fish position and shape
    %----------------------------------------------------
    ManualFit = auto_init(ManualFit,PAR,'autoradius');
    % save all the data
    save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
    save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');
else
    load([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub]);
end

% Assign model parameters
PAR.len = ManualFit(1).length(1:2,:)./PAR.pixpermm;
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts);
PAR.params = reshape([PAR.fishradius;PAR.len],[],1);
PAR.IMthresh = ManualFit(1).thresh;
%Assign the scaling
PAR.pixpermm = ManualFit(1).pixpermm;


%% Pectoral Visualization

% ======================================================================
% Plot the model solution overlay if PAR.pectoral = true to make sure that
% the region defined by PAR.pectoralRegion is good enough.  Kill the
% program if it is incorrect.
if PAR.pectoral
    sol = reshape(ManualFit(1).soln',[],1)';
    
    %-------------------------------
    %Load Image data
    %Number of digit places for the total number of frames
    digits = length(num2str(PAR.numframes));

%     image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,PAR.ICframe,PAR.image_filter(2:end));
%     im = imread([PAR.imagepath image2read],PAR.image_filter(3:end));
    im = imread(fullfile(PAR.imagepath,PAR.startImageName));
    paste_imagefun(sol,PAR.ICframe,im,PAR,[]);
    title('Check that pectoral region is correct')
    pause 
    close all
%     pause(1);
    % save the values of the pectoral region for this particular fish so 
    % that it can be used to plot the tracked model correctly in paste_image_auto.m  
    ManualFit(1).pectoralRegion = PAR.pectoralRegion;
    save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
    save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');
end



 %% TRACKING

%--- Initialise GSSM model from external system description script.
model = gssm_fishOcc('init');

% Define start and end frames of calculation
frames = [PAR.startframe PAR.endframe];

%--- Setup argument data structure which serves as input to
%--- the 'geninfds' function. This function generates the InferenceDS
%--- data structures which are needed by all inference algorithms
%--- in the Rebel toolkit.

Arg.type = 'state';                                  % inference type (state estimation)
Arg.tag = ['State estimation for ' PAR.stub ' data.'];  % arbitrary ID tag
Arg.model = model;                                   % GSSM data structure of external system

% Create inference data structure and
InfDS = geninfds(Arg);                               

% generate process and observation noise sources
[pNoise, oNoise, InfDS] = gensysnoiseds(InfDS, 'srcdkf');       

%Initialize occlusion index
InfDS.model.Occ = cell(1,length(PAR.numfish));

%--- initial estimate of state E[X(0)]
p0 = reshape(ManualFit(1).soln',[],1)';
%p0(11) = 100;
Xh(:,1) = p0;


% 2*standard deviation in pixel distance;
eta2unc = 2*4e-5; %zebrafish%.1; (Default = 4e-5)
%eta2unc = 0.1;
eta2var = (((eta2unc/PAR.pixpermm) / (PAR.dt^2/2))*0.5)^2;

% 2*standard deviation in degrees;
angvar = 10; 
angvar = (angvar*(pi/180)*0.5)^2;

% initial state covariance
Px_ = [angvar.*ones(1,PAR.statedim-3) 2 2 eta2var];

%Create a diagonal covariance matrix by replicating Px_ # of fish
%times and then placing it on the diagonal of a matrix. 
Px = diag(repmat(Px_,1,PAR.numfish));

%--- Call inference algorithm / estimator
% Square Root Central Difference Kalman Filter
%---------------
InfDS.spkfParams  = sqrt(3);    % scale factor (CDKF parameter h)
Sx = chol(Px)';
srcdkf_180227(Xh(:,1),Sx,pNoise,oNoise,InfDS,frames);

save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');

%% Repeat this for other trials - AP

