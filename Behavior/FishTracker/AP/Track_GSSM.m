function varargout = Track_GSSM(PAR,ManualFit)
%Track_GSSM Functionized version of GSSM (Generalized State Space Model)
%   implemented in demse_autoMODAL by Fontaine et al. (2008)
%  
% References:
% Fontaine, E., Lentink, D., Kranenbarg, S., M�ller, U.K., Leeuwen, J.L. van, Barr, A.H., and Burdick, 
% J.W. (2008). Automated visual tracking for studying the ontogeny of zebrafish swimming.
% Journal of Experimental Biology 211, 1305�1316.
% 
% Avinash Pujala, Koyama lab/JRC, 2018

cd('C:\Users\pujalaa\Documents\MATLAB\FreeSwimBehavior\FishTracker')
% Add the subdirectories that contain important / necessary files
addpath('mex/');
addpath('core/');

%--- Initialise GSSM model from external system description script.
tic
model = gssm_fishOcc('init');

% Define start and end frames of calculation
frames = [PAR.startframe PAR.endframe];

%--- Setup argument data structure which serves as input to
%--- the 'geninfds' function. This function generates the InferenceDS
%--- data structures which are needed by all inference algorithms
%--- in the Rebel toolkit.

Arg.type = 'state';                                  % inference type (state estimation)
Arg.tag = ['State estimation for ' PAR.stub ' data.'];  % arbitrary ID tag
Arg.model = model;                                   % GSSM data structure of external system

% Create inference data structure and
InfDS = geninfds(Arg);                               

% generate process and observation noise sources
[pNoise, oNoise, InfDS] = gensysnoiseds(InfDS, 'srcdkf');       

%Initialize occlusion index
InfDS.model.Occ = cell(1,length(PAR.numfish));

%--- initial estimate of state E[X(0)]
p0 = reshape(ManualFit(1).soln',[],1)';
%p0(11) = 100;
Xh(:,1) = p0;


% 2*standard deviation in pixel distance;
eta2unc = 4e-5; %zebrafish%.1;
%eta2unc = 0.1;
eta2var = (((eta2unc/PAR.pixpermm) / (PAR.dt^2/2))*0.5)^2;

% 2*standard deviation in degrees;
angvar = 5; 
angvar = (angvar*(pi/180)*0.5)^2;

% initial state covariance
Px_ = [angvar.*ones(1,PAR.statedim-3) 2 2 eta2var];

%Create a diagonal covariance matrix by replicating Px_ # of fish
%times and then placing it on the diagonal of a matrix. 
Px = diag(repmat(Px_,1,PAR.numfish));

%--- Call inference algorithm / estimator
% Square Root Central Difference Kalman Filter
%---------------
InfDS.spkfParams  = sqrt(3);    % scale factor (CDKF parameter h)
Sx = chol(Px)';
srcdkf(Xh(:,1),Sx,pNoise,oNoise,InfDS,frames);


save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');

toc

varargout{1} = PAR;
varargout{2} = ManualFit;
end

