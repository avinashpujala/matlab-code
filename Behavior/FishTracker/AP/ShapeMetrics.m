function varargout = ShapeMetrics(PAR, ManualFit)
%ShapeMetrics Functionized version of plot_shape_metrics.m by Fontaine et
%al. (2008)
% At present, does not use the subroutines for computing all the metrics in
% plot_shape_metrics.m
% 
% References:
% Fontaine, E., Lentink, D., Kranenbarg, S., M�ller, U.K., Leeuwen, J.L. van, Barr, A.H., and Burdick, 
% J.W. (2008). Automated visual tracking for studying the ontogeny of zebrafish swimming.
% Journal of Experimental Biology 211, 1305�1316.
% 
% Avinash Pujala, Koyama lab/JRC, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is the number of sample points along the length of the fish.
%It changes how fine the mesh is. 
PAR.L = 51;
PAR.dt = 1/500; % Frame Rate (Fontaine et al used 1/1500)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to true if you want to calculate the wavespeed estimates from the
% zero contours of the curvature plots
CalcWaveSpeed = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to true if you need to determine the appropriate cutoff frequency for
% the temporal smoothing of the curvature plots
CalcCutoffFreq = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set to true if you want to plot the error between the smoothed and unsmoothed
% data (i.e., curvature and centerline location) to get a feel for the effect 
% of the smoothing process.  Define the figure numbers for the plots
MakeErrorPlots = false;
CurveErrFignum = 5;
MidlineErrFignum = CurveErrFignum + 1;


%We have different cut off frequencies for low pass filter on the curvature
%values.  These values were determined by visually inspecting the plots
%given in the cell titled "determine curvature cutoff frequency" 

%Stocksteif (3dpf = 300, 15dpf = 200 28dpf = 100)
%WildType (3dpf = 350, 15dpf = 200 28dpf = 180)
Fcutoff = 100;

% These axis ranges are used in the plots
%Curvature range
timeYlim = [0 140];
curYlim = [-10 10]; %3dpf
% curYlim = [-8 8]; %15dpf
% curYlim = [-7 7]; %28dpf

%Frequency Range
freqYlim = [0 150]; %Hz
MagYlim = [0 6];
MagYtick = 0:6;

%Acceleration range
accYlim = [-1.2e6 1.2e6]; %3dpf
% accYlim = [-2.5e5 2.5e5]; %15dpf
% accYlim = [-1e5 1e5]; %28dpf
%=====================================================================


%----------------------------------------------------
%Get Image data
%----------------------------------------------------

frames = [1 PAR.numframes]; % number of frames in movie

samplerate = 1;
movidx = frames(1):samplerate:frames(2);

interptag = 1;

%--------------------------------
% Define the Tracking Parameters
PAR.etamax = 0;
PAR.statedim = 11;

PAR.mdlpar = 8; % length(PAR.mdlpar) should be equal to
                    % numfish.  It is the number of parameters
                    % used in the each fish model.
PAR.fishnum = 1 ;
PAR.numfish = length(PAR.fishnum);
PAR.pixpermm = ManualFit(1).pixpermm;
PAR.modelfun_H = @modcurvesplineP_display;
PAR.c = 4; %spline order
PAR.gamma = 10.59663;

% Segmentation threshold for background subtraction 
PAR.IMthresh = ManualFit.thresh; 

% For the post-processing, we only want to sample the fish model on a
% uniform grid.
PAR.pectoral = false; 

% Length parameters of fish [length of tail , length of head]
PAR.len = ManualFit(1).length(1:2,:)./(PAR.pixpermm);
% Radius parameters of fish (8 B-spline control points)
PAR.fishradius = cell2mat(ManualFit(1).radius_splinepts(:,PAR.fishnum));

%% Load solutions into memory
solidx = [1 length(movidx)];
soln1 = zeros(length(movidx),PAR.numfish*PAR.statedim);
SOLN = zeros(length(frames(1):frames(2)),PAR.numfish*PAR.statedim);
if interptag == 1
    for i=1:length(movidx)
        load([PAR.solutionpath 'fish_' PAR.stub '/fish' num2str(movidx(i)) ...
            '.mat']);
        soln1(i,:) = xh(1:PAR.numfish*PAR.statedim)';
        clear xh InternalVariablesDS
    end
end

% Now, perform interpolation to calculate the state at in between frames
for k = 1:size(soln1,2)
    SOLN(:,k) = interp1(movidx,soln1(:,k),frames(1):frames(2),'spline')';
end


%% Recalculate the bend angle and curvature based on a refitted smooth 
% centerline that gets rid of curvature discontinuity at point of rigidity.

%Initialize
tic
disp('Refitting centerline ...')
Err = zeros(size(SOLN,1),PAR.L);
COA = zeros(size(SOLN,1),2);
COV = zeros(size(SOLN,1),2);

ManualFit.xMid = zeros(size(SOLN,1),PAR.L);
ManualFit.yMid = zeros(size(SOLN,1),PAR.L);

cur = zeros(size(SOLN,1),PAR.L);
the = zeros(size(SOLN,1),PAR.L);

len = PAR.len(:,PAR.fishnum);

chunkSize = round(size(SOLN,1)/10);
for i = 1:size(SOLN,1)
    if mod(i,chunkSize)==0
        disp([num2str(round(100*i/size(SOLN,1))) ' % ...'])
    end
    [x,y,z,Frenet,U,V,ctrl_ptsX,ctrl_ptsY] = model_shell_velren(SOLN(i,:),0,PAR);
    if i == 1
        [xx,Frenet,theta,kappa,sqerr,x0] = refitCenterline(x{1}(:,2),y{1}(:,2),1,10,len);
        Err(i,:) = sqerr';
    else
        [xx,Frenet,theta,kappa,sqerr,x0] = refitCenterline(x{1}(:,2),y{1}(:,2),1,10,len,x0);
        Err(i,:) = sqerr';
    end
    
    %Make Specific curvature
    cur(i,:) = sum(len).*kappa';
    the(i,:) = theta';
   
    %Save smoothed midline location
    ManualFit.xMid(i,:) = xx(:,1)';
    ManualFit.yMid(i,:) = xx(:,2)';
end

% Get rid of angle discontinuities
the = unwrap(the,[],1);
toc


%% Determine curvature cutoff frequency

Fs = 1/PAR.dt;

% This section of code is run to visualize the curvature frequency response
% and visually inspect the plot to determine the cutoff frequency.
if CalcCutoffFreq
    Magnitude = [];
    sig2calc = cur;
    
    % Remove DC values of the curvature signals
    meansub = mean(sig2calc,1);
    sig2calc = sig2calc - repmat(meansub,size(sig2calc,1),1);
    
    for k = 1:size(sig2calc,2)
        sig = sig2calc(:,k);
        L = length(sig);
        NFFT = 2^nextpow2(L);
    %     NFFT = L;
        Y = fft(sig,NFFT)/L;
    %     Y = fft(sig);
    %     Y(1) = [];
        f = Fs/2*linspace(0,1,NFFT/2);
    %     f = (1:NFFT/2)/(NFFT/2)*Fs/2;
        Magnitude(:,k) = 2*abs(Y(1:NFFT/2));
    %     Magnitude(:,k) = abs(Y(1:NFFT/2)).^2;
    %     Magnitude(:,k) = abs(Y(1:length(Y)/2)).^2;
        % hold on;
        % plot(f,Magnitude(:,k),'color',rand(1,3));
        % title(num2str(k));
        % pause
    end
    figure;
    plot(f,Magnitude);
    xlabel('frequency');
    ylabel('Magnitude');
    display('Visually determine the cutoff frequency from the Magnitude plot');
    display('Set "Fcutoff" to this value and rerun with "CalcCutoffFreq" set to false.');
    return
end


%% Smoothing the bend angle and curvature

% Now, smooth the bend angle and curvature in the temporal direction
% Cutoff frequency from fft on a sample signal

filter_order = 4;
[b a] = butter( filter_order,Fcutoff*(2/Fs));

%Copy the unsmoothed values so we can compare the difference
the_unsmooth = the;
cur_unsmooth = cur;

for k = 1:size(the,2)
    temp = the(:,k);
    the(:,k) = filtfilt(b,a,temp);
    temp = cur(:,k);
    cur(:,k) = filtfilt(b,a,temp);
end

%%
% Now calculate the frequency response of the filtered curvature during the 
% time when the fish undergoes continuous swimming. 
%
% ManualFit.contswimframes = [beg end] define the beginning and end of the 
% frames in the sequence where the fish undergoes continuous swimming.  This
% is determined by visually inspecting the curvature contour plot.

Magnitude = [];
% sig2calc = cur(15:81,(s <=0 ));
if isfield(ManualFit,'contswimframes')
    sig2calc = cur(ManualFit.contswimframes(1):ManualFit.contswimframes(2),:);
else
    sig2calc = cur;
    warning(['The frames that correspond to continuous swimming have not been\n '...
        'identified (ManualFit.contswimframes). The frequency plot is based\n '...
        'on all frames'],[]);
end

% Remove DC values of the curvature signals
meansub = mean(sig2calc,1);
sig2calc = sig2calc - repmat(meansub,size(sig2calc,1),1);

for k = 1:size(sig2calc,2)
    sig = sig2calc(:,k);
    L = length(sig);
    NFFT = 2^nextpow2(L);
    Y = fft(sig,NFFT)/L;
    f = Fs/2*linspace(0,1,NFFT/2);
    Magnitude(:,k) = 2*abs(Y(1:NFFT/2));
end

%------------------------------------------
% Calculate the dominant "swimming frequency", FF of the fish by taking a
% weighted average of the frequencies with the magnitude response.
[val,I] = max(Magnitude,[],1);
swimfreq = f(I);
FF = sum(swimfreq.*val)./sum(val);
%figure; plot(swimfreq,'.-')



%%
% Create error plots of the difference between filtered and unfiltered
% curvature and bend angle.

if MakeErrorPlots
    curerr = sqrt(mean((cur-cur_unsmooth).^2,1));
    % Flip the error so values go from head to tail
    curerr = fliplr(curerr);
    
    figure(CurveErrFignum); hold on;
    hh = plot(linspace(0,1,PAR.L),curerr,'b','linewidth',1);
    set(hh,'DisplayName','Age 5 days')
    axis tight
    xlabel('Normalized position along body axis');
    ylabel('Curvature RMS Error')

    Displaceerr = sqrt(mean(Err,1))./(PAR.pixpermm*sum(len));
    % Flip the error so values go from head to tail
    Displaceerr = fliplr(Displaceerr);
    
    figure(MidlineErrFignum); hold on;
    hh = plot(linspace(0,1,PAR.L),Displaceerr,'b','linewidth',1);
    set(hh,'DisplayName','Age 5 days')
    axis tight
    xlabel('Normalized position along body axis');
    ylabel('Centerline RMS Error')
end




%% Calculate the angular acceleration along the length of the fish
RTheta = the;

% Use the 'gradient' function instead of central differencing for the first 
% NN points
NN = 1;

[dum,Angveltmp] = gradient(RTheta,1,PAR.dt);
[dum,Angacctmp] = gradient(Angveltmp,1,PAR.dt);

begmid = 1+NN;
endmid = size(RTheta,1)-NN;

%Calculate accelerations in the middle using central differencing
MidAcc = (RTheta(begmid+NN:end,:) + RTheta(1:endmid-NN,:) - 2.*RTheta(begmid:endmid,:))./(NN*PAR.dt)^2;

AngAcc = [Angacctmp(1:NN,:)
    MidAcc
    Angacctmp(end-NN+1:end,:)];

%Frame # or time
time = (0:size(SOLN,1)-1)*PAR.dt*1000; %in milliseconds 
%time = 1:size(SOLN,1);

fishlen = linspace(0,1,PAR.L);
%fishlen = s;

ManualFit.fishLen = fishlen;
ManualFit.time = time;
ManualFit.cur = cur;
ManualFit.the = the;
ManualFit.AngVel = flipud(Angveltmp');
ManualFit.AngAcc = flipud(AngAcc');

disp('Saving updated variables...')
save([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub],'ManualFit');
save([PAR.solutionpath 'fish_' PAR.stub '/' 'PAR_' PAR.stub],'PAR');

varargout{1} = PAR;
varargout{2} = ManualFit;

toc
end

