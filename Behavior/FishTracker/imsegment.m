function [IMBW,BGnew] = imsegment(im,BG,thresh)

% Right now, the images are grayscale, not RGB

%Learning rate for running average
alpha = 0.05;
%alpha = 0.0;
im = double(im);

%to pick up light colored animals
% diffim = (-BG + im);

%keyboard
%to pick up dark colored animals
diffim = (BG - im);

% diffim = abs(BG - im);

[fidx] = find(diffim > thresh);

BGnew = alpha*im + (1-alpha)*BG; %for the pixels that are background

%Now change it back to the previous BG for the pixels that are
%foreground. Do linear indexing to do vector operation.

BGnew(fidx) = BG(fidx);

IMBW = zeros(size(im));
IMBW(fidx) = 1;

IMBW = bwmorph(IMBW,'close',Inf);
