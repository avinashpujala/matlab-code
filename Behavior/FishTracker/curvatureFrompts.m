function kappa = curvatureFrompts(x,y,nrm)

ds = 1/(length(x)-1);
dx = gradient(x,ds);
dy = gradient(y,ds);

xex = interp1(1:length(x),x,0:length(x)+1,'spline','extrap')';
yex = interp1(1:length(y),y,0:length(y)+1,'spline','extrap')';

ddx = (xex(1:end-2) + xex(3:end) - 2*xex(2:end-1))./ds^2;
ddy = (yex(1:end-2) + yex(3:end) - 2*yex(2:end-1))./ds^2;
% ddx = gradient(dx,ds);
% ddy = gradient(dy,ds);

%kappa = sqrt(ddx.^2 + ddy.^2)./(dx.^2 + dy.^2);
kappa = sum([ddx ddy] .*nrm ,2);
