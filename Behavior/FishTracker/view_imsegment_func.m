function thresh = view_imsegment_func(ManualFit,PAR)

% This code allows you to easily view the segmentation performance
% of a particular threshold value.  It pauses after each frame to
% allow visual inspection.  Change values of 'thresh' to find one
% appropriate for the particular video sequence.  A difference in
% grayscale value between the background and the current frame that
% is greater than 'thresh' is labeled as a 'fish' pixel. 
%
% Currently, it stays 
% It automatically advances through the frames


N  = PAR.numframes;                                    % number of frames

% Define start and end frames of calculation
% frames = [1 N];
frames = [1 N] + PAR.ICframe-1;

%figure to plot in
fignum = 1;
clf;

%define the initialization frame 
initframe = 1;


%==========================
%set a threshold value
thresh = 20;

% select plot mode.
% 1 = dots plotted onto of image represent fish pixels
% 2 = original image and binary image plotted side by side
plotmode = 1;
i = frames(1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%for i = frames(1):frames(2)
while 1 == 1
  if i == frames(1)
    %Initialize the background image
    BG = ManualFit(initframe).BG;
  end
  
  %Load Image data
  
  %Number of digit places for the total number of frames
  digits = length(num2str(PAR.numframes));

%   image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,i,PAR.image_filter(2:end));
%   fishim = imread([PAR.imagepath image2read],PAR.image_filter(3:end));
    fishim = imread(fullfile(PAR.imagepath,PAR.startImageName));

  data = fishim;
  
  [data,dum] = imsegmentabs(data,BG,thresh);
   
  switch plotmode
   case 1
    figure(fignum);
    subplot(1,2,1), imagesc(fishim); colormap gray
    %imagesc(data); colormap gray;
    title(['Frame ' num2str(i)])
    axis image
    
    [r,c] = find(data == 1);
    hold on;
    plot(c,r,'r.');
    hold off
    
    subplot(1,2,2), imagesc(BG - double(fishim)); colormap gray
    %imagesc(data); colormap gray;
    title(['Frame ' num2str(i)])
    axis image
    
    R = input(['Enter number to change threshold [' num2str(thresh) '], \n "q" to quit, \n or Enter to continue: '],'s');
    if strcmpi(R,'q')
        break
    elseif isempty(R)
        continue
    else
        thresh = str2double(R);
    end
    
   case 2
    figure(fignum);
    subplot(1,2,1), imagesc(fishim); colormap gray
    title(['Frame ' num2str(i)])
    axis image
    
    subplot(1,2,2), imagesc(data); colormap gray
    title(['Frame ' num2str(i)])
    axis image
    pause
  end
end

close(fignum)