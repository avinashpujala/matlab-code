function [N,D1,D2] = dbasisPZero(c,s,numpts,knotv)

[N,D1,D2] = dbasisP(c,s,numpts,knotv);
[Nz,D1z,D2z] = dbasisP(c,0,numpts,knotv);

posidx = find(s > 0);

N(posidx,:) = repmat(Nz,length(posidx),1);
D1(posidx,:) = repmat(zeros(size(D1z)),length(posidx),1);
D2(posidx,:) = repmat(zeros(size(D2z)),length(posidx),1);