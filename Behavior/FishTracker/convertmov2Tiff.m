PAR.videopath = 'video/L30-25pct/';
PAR.filetag = 'L30-25pct';
PAR.idtag = '';
PAR.fishID = '11';
%----------------------------------------------------
%load video data into buffer
%----------------------------------------------------
fishinfo = aviinfo([PAR.videopath PAR.filetag '/' PAR.filetag '_' ...
        PAR.fishID '_' PAR.idtag '/' 'L30-25pct-11segment.avi']);
numframes = fishinfo.NumFrames;
PAR.imgres = [fishinfo.Height fishinfo.Width];


for k = 1:numframes
    tmp = aviread([PAR.videopath PAR.filetag '/' PAR.filetag '_' ...
        PAR.fishID '_' PAR.idtag '/' 'L30-25pct-11segment.avi'],k);
%     tmp = aviread([PAR.videopath PAR.filetag '/' PAR.filetag '_' ...
%         PAR.fishID '_' PAR.idtag '/' 'fish' strvec{n} '.avi'],k);
    fishim = frame2im(tmp);
    imwrite(fishim,[PAR.videopath PAR.filetag '/' PAR.filetag '_' ...
        PAR.fishID '_' PAR.idtag '/' lower(PAR.filetag) '_' PAR.fishID '_' lower(PAR.idtag) ...
        repmat(num2str(0),1,6-(1+floor(log10(k)))) num2str(k) '.tif'],'tiff','resolution',96);
end
