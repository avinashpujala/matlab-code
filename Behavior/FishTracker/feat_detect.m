function [Features,IMBW] = feat_detect(n,Features,PAR)

% [Features] = feat_detect(n,Features,PAR)
% The version of the feature extractor uses a simple background
% subtraction routine to segment the image and calculates the
% boundary points and locations of high curvature.


% A function call with 1 argument loads the background image and
% stores it in memory
persistent BG
if nargin == 1
  %n, the first argument, is the PAR structure
%     load(['ManualFit_' n]);
  PAR = n;
  load([PAR.solutionpath 'fish_' PAR.stub '/' 'ManualFit_' PAR.stub]);
  BG = ManualFit.BG;
%   load([n 'BG_checker']);
%   BG = BG;   
  return
end



subs = 6; %amount to subsample boundary

numfish = PAR.numfish;

%Load Image data

%Number of digit places for the total number of frames
digits = length(num2str(PAR.numframes));


% Commented out the following line (AP 20180214)
% image2read = sprintf(['%s%0' num2str(digits) 'd%s'],PAR.stub,n,PAR.image_filter(2:end));

%This is the current frame
% data = imread([PAR.imagepath image2read],PAR.image_filter(3:end));
data = imread(fullfile(PAR.imagepath,PAR.filenameCells{n}));


datagray = data;

IMBW.fishBG = BG;
IMBW.thresh = PAR.IMthresh;

%Don't update background
[data,dum] = imsegment(data,BG,IMBW.thresh);

%data = double(bwmorph(data,'majority'));
% data = imfill(data,'holes');
%Convert to 256 grayscale image
Features(n).IMfull = data.*255;
Features(n).IMfullgray = datagray;



imgres = size(data);

% Find the Silhouettes of the fish:    
[B,L,N,A]= bwboundaries(data,4);

% determine which boundary is which
% Pick the boundaries that are not the perimeter of the image.
borderidx = [];  
for j = 1:N
  if size(B{j},1) == 2*sum(imgres - 1)+1  
    borderidx = j;
  end
end
bndytemp = B;

%Find enclosed boundaries
enclosed_boundaries = [];
for j = 1:length(bndytemp)
  enclosed_boundaries = [enclosed_boundaries ; find(A(:,j))];
end
    
bndy2rid = [];
%Transform axis
for k = 1:length(bndytemp)
  % Subsample Boundary to create a possible better correspondence.
  bndytemp{k} = bndytemp{k}(1:subs:end,:);
  
  %I will throw away contours that are smaller than a particular length
  
  if (size(bndytemp{k},1) < round(8*PAR.pixpermm/subs) )
    %This boundary is too small, probably noise
    bndy2rid(end+1) = k;
  elseif (all(diff(bndytemp{k}(:,1)) == 0) || ...
          all(diff(bndytemp{k}(:,2)) == 0))
    %This boundary is the image border
    bndy2rid(end+1) = k;
  elseif (size(bndytemp{k},1) == imgres(1) || size(bndytemp{k},1) >= imgres(2))
      %This boundary is also probably an image border
      bndy2rid(end+1) = k;
  end
  bndytemp{k} = [bndytemp{k}(:,2) imgres(1)-bndytemp{k}(:,1)];
end

if ~isempty(borderidx)
  bndytemp{borderidx} = [];
end
if ~isempty(bndy2rid)
  for q = 1:length(bndy2rid)
    bndytemp{bndy2rid(q)} =[];
  end
end

%Figure out which cell contains the largest boundary
for k = 1:length(bndytemp)
  dim(k) = size(bndytemp{k},1);
end
[val,largestidx] = max(dim);

% Initialize the nrmltemp cell so it is the same size as bndytemp
nrmltemp = cell(size(bndytemp));
kappa = cell(size(bndytemp));

NumClosedBdy = 0;
for m = 1:size(bndytemp,1)
  if ~isempty(bndytemp{m})
    NumClosedBdy = NumClosedBdy + 1;
    % --- Approximate the boundary with a closed cubic B-spline curve
    % sampled at the same number of points
    bndylen(m) = size(bndytemp{m},1);
    Y = bndytemp{m};
    
    Ytemp = Y;
    dd = reshape(Ytemp,[],1);

    %-- the number of control points be about one sixth of the total
    %data points 
    c = 4; %cubic spline
    numrep = c-1; %this is the number of control points that are repeated
    %at the beginning and end of the vector;
    npts = max(c+numrep,round(size(Ytemp,1)/6));
    T = linspace(-1,0,size(Ytemp,1));
    knotv = oknotP(npts,c,1,0);
    [N,D1,D2] = dbasisP(c,T,npts,knotv);
    
    Aeq = zeros(2*numrep,2*npts);
    for i = 1:2
        Aeq((i-1)*numrep+(1:numrep),(i-1)*npts+(1:numrep)) = eye(numrep);
        Aeq((i-1)*numrep+(1:numrep),(i-1)*npts+(npts-(numrep-1):npts)) = -1*eye(numrep);
    end
    options = optimset('largescale','off');
    bb = lsqlin(blkdiag(N,N),dd,[],[],Aeq,zeros(2*numrep,1),[],[],[],options);

    B = reshape(bb,[],2);
    
        
    bndytemp{m} = N*B;
    
    skew_z = [0 -1 0
              1 0 0
              0 0 0];
    % --- Perform cross product with skew matrix and make unit length
    Tang = D1*B;
    
    Tang = [Tang zeros(size(Tang,1),1)];
    MAG = sqrt(sum(Tang.^2,2));
    Tang = Tang./repmat(MAG,1,3);
    
    nrmltemp{m,:} = (skew_z*Tang')';
    nrmltemp{m,:} = nrmltemp{m,:}(:,1:2);
    
    %Flip normal direction if fully enclosed
    if any(m == enclosed_boundaries)
      nrmltemp{m,:} = -1.*nrmltemp{m,:};
    end
    
    %Calculate curvature from dot product of second derivative and
    %normal vector MAG(s) * Tang(s) = D1*B
    %              MAG(s) * Kappa(s) * Nrml(s) = D2*B
    
    kappa{m} = sum( (D2*B) .* nrmltemp{m},2) ./MAG;
    
    %Take the points with curvature less than Num
    Num = -1.5*sqrt(var(kappa{m}));
    iidx{m,1} = find(kappa{m} < Num);
    
    if m >=2
      %add the length of the previous boundary to make indexes
      %correct for concantation below.
      iidx{m,1} = iidx{m,1} + sum(bndylen(1:m-1)); 
    end
        
  end
end

Features(n).All_Bndy_Points = cell2mat(bndytemp);
Features(n).All_Nrml_Vectors = cell2mat(nrmltemp);  
Features(n).All_Kappa_Values = cell2mat(kappa); 
Features(n).NumClosedBdy = NumClosedBdy;
BNDY = Features(n).All_Bndy_Points;

%Calculate the bounding box for all the boundary points.
buffer = 20;

Xmin = min(Features(n).All_Bndy_Points,[],1);
Xmin = round(Xmin) - buffer; 
%Make sure the bounding box does not fall outside the visible area of the
%image

Xmin(1) = max(Xmin(1),1);
Xmin(2) = max(Xmin(2),1);


Xmax = max(Features(n).All_Bndy_Points,[],1);
Xmax = round(Xmax) + buffer;
%Make sure the bounding box does not fall outside the visible area of the
%image
Xmax(1) = min(Xmax(1),imgres(2));
Xmax(2) = min(Xmax(2),imgres(1));

%Now, convert into matrix indices.
cmin = Xmin(1);
cmax = Xmax(1);

rmin = (imgres(1) + 1) - Xmax(2);
rmax = (imgres(1) + 1) - Xmin(2);

Features(n).boundbox = [rmin rmax cmin cmax];

%Candidate points
Candpts = BNDY(cell2mat(iidx),:);
%Manualpts = [PAR.headpts(n,:); PAR.tailpts(n,:)];
%Candpts = [Candpts ; Manualpts];

%manidx = kdtreeidx(Features(n).All_Bndy_Points,Manualpts);
Candvec = Features(n).All_Nrml_Vectors(cell2mat(iidx),:);
%Candvec = [Candvec ; Features(n).All_Nrml_Vectors(manidx,:)];

CandKappa = Features(n).All_Kappa_Values(cell2mat(iidx),:);
%CandKappa = [CandKappa ; Features(n).All_Kappa_Values(manidx,:)];

Features(n).Candpts = Candpts;
Features(n).Candvec = Candvec;
Features(n).CandKappa = CandKappa;


% This is the end of the program
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
