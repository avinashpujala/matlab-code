function varargout = FlattenGrpDataStruct(grpData, varargin)
%FlattenGrpDataStruct - Given grpData that contains all the ablation
%   information for all the ablation groups, returns a data structure whose
%   fields contain all the data unrolled into a format that makes it easy
%   to write into a xl sheet that can later be read into R.
% data_flat = FlattenGrpDataStruct(grpData);
% [data_flat, data_array] = FlattenGrpDataStruct(grpData,'grps',grps,'delGrps',delGrps)
% Inputs:
% grpData - Contains all the ablation data for all the ablation groups
% Optional inputs:
% grps - Cell array of group names. If empty (default), will restrict flattening to 
%       these ouput variables.            
%       NB: If grpData has any other fields, such as .data_flat, then will result in
%           error. In such cases, grps must be specified explicitly.
% delGrps - Cell array of group names to exclude from unrolling.
% stimTypes - Cell array of stimulus types to consider. Defuault = [],
% results in stimTypes = {'vib','dark'};
% responses - Cell array of response variable names. If [] (default), then
%   responses = {'bendAmp','bendPer','onset'};
% trtmnts - Cell array of treatments. If [], then trtmnts = {'ctrl', 'abl'}
% saveData - Boolean. If true (default), then saves data. 
% saveDir - String specifying path to directory where data is to be found.
%   If not specified, uses current directory
% saveName - String specifying the name of the saved variables (.mat and
%   .xls with same name). If not specified, then auto-generated.
% Outputs:
% data_flat - Data struct whose fields include all the subfields of interest from grpData 
% data_array - 2D cell array from grpData that can be written into an excel
%   sheet
% 
% Next, use GrpData2XLArray to write to excel sheet.

grps = [];
delGrps = [];
stimTypes = [];
responses = [];
trtmnts = [];
saveDir = cd;
saveData = true;
saveName = [];

for jj = 1:numel(varargin)
    if ischar(varargin{jj})        
        switch lower(varargin{jj})
            case 'grps'
                grps = varargin{jj+1};
            case lower('delGrps')
                delGrps = varargin{jj+1};
            case lower('stimTypes')
                stimTypes = varargin{jj+1};  
            case 'responses'
                responses = varargin{jj+1};
            case 'trtmnts'
                trtmnts = varargin{jj+1};
            case 'savedata'
                saveData = varargin{jj+1};
            case 'savedir'
                saveDir = varargin{jj+1};
            case 'savename'
                saveName = varargin{jj+1};
        end
    end
end

if isempty(grps)
    grps = fieldnames(grpData);
end

delInds = [];
if ~isempty(delGrps)
    for jj = 1:length(delGrps)
        delInds  = union(delInds,find(strcmpi(grps,delGrps{jj})));        
    end
    grps(delInds)=[];
end

if isempty(stimTypes)
    stimTypes = {'vib', 'dark'};
end

if isempty(responses)
    responses = {'bendAmp','bendPer','onset'};
end

if isempty(trtmnts)
    trtmnts = {'ctrl','abl'};
end


%% Creating data struct
nResponses = length(responses);
data_flat = struct;
count = 0;
count_entry = 0;
for grp = 1:length(grps)
    for trtmnt = 1:length(trtmnts)
        for stim = 1:length(stimTypes)            
            foo_stim = grpData.(grps{grp}).(trtmnts{trtmnt}).(stimTypes{stim});          
            varName = fieldnames(foo_stim);
            blah = foo_stim.(varName{1});        
            for fish = 1:length(blah)
                disp([upper(grps{grp}) ', ' trtmnts{trtmnt} ', ' stimTypes{stim} ', fish # ' num2str(fish)])
                foo_fish = blah{fish}.elicitedSwimInfo;
                src = blah{fish}.Properties.Source;
                startInd_fast = strfind(lower(src),'fastdir');
                startInd_slow = strfind(lower(src),'slowdir');
                startInd = union(startInd_fast,startInd_slow);
                if numel(startInd)>1
                    error('Cannot unambiguously find expt time from file name! Check file name.')
                end
                stopInd_forward = strfind(src,'/');
                stopInd_back = strfind(src,'\');
                stopInd = union(stopInd_forward,stopInd_back);
                stopInd = min(stopInd(stopInd>startInd));
                 if numel(stopInd)>1
                    error('Cannot unambiguously find expt time from file name! Check file name.')
                end
                subStr = src(startInd:stopInd);
                subStr = subStr(end-6:end-1);
                exptTime = round(str2double(subStr)/1e4); % Expt time rounded to the hour
                if isempty(exptTime) || isnan(exptTime)
                     disp('Detected experiment time not valid! Check file name')
                end
                if exptTime<0 || exptTime >24
                    disp('Detected experiment time not valid! Check file name')
                end               
                if ~isempty(foo_fish)
                    nTrls = length(foo_fish.(responses{1}));
                    for response = 1:nResponses
                        count = count_entry;
                        responseName = responses{response};
                        try
                        foo_response = foo_fish.(responseName);
                        catch
                            a = 1;
                        end
                        nBends = 0;
                        for trl = 1:nTrls
                            if length(foo_response) >= trl
                                nBends = max(nBends,length(foo_response{trl}));
                            end
                        end
                        for trl = 1:nTrls
                            if length(foo_response) >=trl
                                foo_trl = foo_response{trl};
                                for bend = 1:nBends
                                    count = count + 1;
                                    data_flat.GrpName{count} = grps{grp};
                                    data_flat.Treatment{count} = trtmnts{trtmnt};
                                    data_flat.StimType{count} = stimTypes{stim};
                                    data_flat.FishNum(count) = fish;
                                    data_flat.TrlNum(count) = trl;
                                    data_flat.BendNum(count) = bend;
                                    data_flat.ExptTime(count) = exptTime;
                                    if bend > length(foo_trl)
                                        data_flat.(responses{response})(count) = NaN;
                                    else
                                        data_flat.(responses{response})(count) = foo_trl(bend);
                                    end
                                    
                                end
                            end
                        end
                    end
                    count_entry = count;
                else
                    error('No elicited swim info')
                end
            end
        end
    end
end

nanRows = isnan(data_flat.(responses{1}));
flds = fieldnames(data_flat);
for fld = 1:length(flds)
    fldName = flds{fld};
    data_flat.(fldName)(nanRows)=[];
end

data_array = [];
if nargout == 2
    disp('Creating array for xls...')
    fldNames = fieldnames(data_flat);
    data_array = cell(numel(data_flat.bendAmp)+1,length(fieldnames(data_flat)));
    for fld = 1:length(fldNames)
        fldName = fldNames{fld};
        fldName(1) = upper(fldName(1));
        data_array{1,fld} = fldName;
        vars = data_flat.(fldNames{fld});
        if iscell(vars(1))
            data_array(2:end,fld) = data_flat.(fldNames{fld});
        else
            data_array(2:end,fld) = num2cell(data_flat.(fldNames{fld}));
        end
    end
end

if saveData
    if isempty(saveName)
        nom = 'GrpData-';
        for grp = 1:length(grps)
            nom = [nom grps{grp} '-'];
        end
        for stim = 1:length(stimTypes)
            nom = [nom '-' stimTypes{stim} '-'];
        end
        nom = [nom datestr(now,30)];
    end
    grpData.data_flat = data_flat;
    disp('Saving group ablation data...')
    save(fullfile(saveDir,nom),'grpData','-v7.3')
    disp(['Saved at ' saveDir])
    disp(nom)
    if nargout==2
        disp('Writing to xls...')
        nom = [nom '.xls'];
        xlswrite(fullfile(saveDir,nom),data_array)        
    end 
    disp(nom)
end


varargout{1} = data_flat;
varargout{2} = data_array;

end

