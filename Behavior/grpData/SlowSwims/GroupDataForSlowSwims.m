
%% Inputs
% ---- First load paths created by SlowSwimPathsFromXls.m
ipd_default = 0.055;
dt_default = 1/500;

%% Create group data struct
data = struct;
grps = fieldnames(paths);
rowCount = 0;
for grpNum = 1:length(grps)
    grp = grps{grpNum};
    disp(upper(grp))
    trtmnts = fieldnames(paths.(grp));
    for trtNum = 1:length(trtmnts)
        trtmnt = trtmnts{trtNum};
        disp(upper(trtmnt))
        foo = paths.(grp).(trtmnt);
        data.(grp).(trtmnt) = {};
        for fishNum = 1:length(foo)
            disp(['Fish # ' num2str(fishNum)])
            pp  = foo{fishNum};
            [~,nom,~] = fileparts(pp);
            if ~strcmpi(nom,'proc')
                pp = fullfile(pp,'proc');
            end
            CheckFishPosAndRemoveBadTrls_SlowSwims(pp);
            try
                foo_foo = OpenMatFile(pp,'nameMatchStr','slowSwim');
            catch
                foo_foo = OpenMatFile(pp,'nameMatchStr','slowSwim');
            end
            fp = foo_foo.fishPos;
            badTrls = foo_foo.delTrls;
            fp(:,:,badTrls)=[];
            %             fp = reshape(fp,[size(fp,1)*size(fp,3),size(fp,2)]);
            try
                ipd = foo_foo.interPxlDist;
                dt = 1/foo_foo.frameRate;
            catch
                disp('Frame rate or inter-pixel-distance not present, using default values...')
                ipd = ipd_default;
                dt = dt_default;
            end
            % Go through all trials, and extract velocities
            nTrls = size(fp,3);
            disp([num2str(nTrls),' trls'])
            D = zeros(size(fp,1),nTrls);
            for trl = 1:nTrls
                fp_trl = squeeze(fp(:,:,trl));
                % Remove NaN values which represent the fish not being
                % found for some frames
                nanInds = find(isnan(fp(:,1)));
                fp_trl(nanInds,:)=[];
                if size(fp_trl,1)>3
                    fp_smooth = FitBSplineToCurve(fp_trl,[],4);
                    vel = deltaDistance(fp_smooth)*ipd/dt;
                    D(1:length(vel),trl) = vel;
                end
            end
            dDist = D(:);
            %             dDist = deltaDistance(fp)*ipd/dt;
            pkInds = GetPks(dDist,'polarity',1,'peakThr',0,'thrType','abs');
            pks = dDist(pkInds);
            pkInts = diff(pkInds*dt*1000);
            pkInts = [0; pkInts(:)];
            rowCount = rowCount + length(pkInts);
            data.(grp).(trtmnt){fishNum}.fishNum = ones(size(pks(:)))*fishNum;
            data.(grp).(trtmnt){fishNum}.isi = pkInts(:);
            data.(grp).(trtmnt){fishNum}.peakVel = pks(:);
        end
    end
end


%% Make XLS array for writing to xl sheet
colNames = {'GrpName','Treatment','StimType','FishNum','ISI','PeakVel'};

fishCol = find(strcmpi(colNames,'FishNum'));
isiCol = find(strcmpi(colNames,'ISI'));
velCol = find(strcmpi(colNames,'PeakVel'));

nCols= length(colNames);
data_xls = cell(rowCount,nCols);
rowInd = 1;
for grpNum = 1:length(grps)
    grp = grps{grpNum};
    disp(upper(grp))
    trtmnts = fieldnames(paths.(grp));
    for trtNum = 1:length(trtmnts)
        trtmnt = trtmnts{trtNum};
        disp(upper(trtmnt))
        foo = data.(grp).(trtmnt);
        for fishNum = 1:length(foo)
            foo_foo = foo{fishNum};
            disp(['Fish # ' num2str(fishNum)])
            nVals = length(foo_foo.fishNum);
            rowInds = rowInd:rowInd+nVals-1;
            if ~isempty(rowInds)
                disp(['Filling rows: ' num2str(rowInds(1)) ' - ' num2str(rowInds(end))])
                data_xls(rowInds,1) = {grp};
                data_xls(rowInds,2) = {trtmnt};
                data_xls(rowInds,3) = {'spont'};
                data_xls(rowInds,4) = num2cell(foo_foo.fishNum);
                data_xls(rowInds,5) = num2cell(foo_foo.isi);
                data_xls(rowInds,6) = num2cell(foo_foo.peakVel);
                rowInd = rowInds(end)+1;
            end
        end
    end
end


%% Get rid of false swims in data_xls
pks = cell2mat(data_xls(:,end));
pkInts = cell2mat(data_xls(:,end-1));

[label,model,llh] = mixGaussEm(pks(:)',3);
[mu_sort,sortInds] = sort(model.mu,'ascend');
sig_sort = squeeze(model.Sigma);
sig_sort = sig_sort(sortInds);

figure('Name','Peak sorting')
clrs = {'m','g','c'};
for lbl = 1:length(mu_sort)
    inds = find(label ==lbl);
    semilogy(inds,pks(inds),'.','color', clrs{lbl})
    %     plot(pkInts(inds),pks(inds),'.','color',clrs{lbl})
    hold on
end
box off
set(gca,'color','k')
ylim([-inf inf])
xlim([-inf inf])
xlabel('Indices')
ylabel('Distance peaks')

lbl = unique(label);
labelOfInterest = lbl(sortInds(2));
thr_lower = min(pks(label ==labelOfInterest));
thr_upper = max(pks(label ==  labelOfInterest));

% keepInds = find(label == labelOfInterest);
% keepInds = find((pks > thr_lower) & (pks <= 1.5*thr_upper));
%--- The mixture model is not working so well.
keepInds = find(pks<=40);

disp([100*(size(data_xls,1)-length(keepInds))/size(data_xls,1) '% values removed!'])
foo = data_xls(keepInds,:);
data_xls_keep = cell(size(foo,1)+1,size(foo,2));
data_xls_keep(1,:)= colNames;
data_xls_keep(2:end,:) = foo;



%% Save data_xls
ts= datestr(now,30);
fileStem = 'GrpData_slowSwim_Alx-young_';
fName = [fileStem ts '.mat'];
tic
disp('Saving .mat file...')
save(fullfile(saveDir,fName),'data_xls_keep','-v7.3');
toc

disp('Saving xls...')
fName = [fileStem ts '.xlsx'];
xlswrite(fullfile(saveDir,fName),data_xls_keep);
toc





