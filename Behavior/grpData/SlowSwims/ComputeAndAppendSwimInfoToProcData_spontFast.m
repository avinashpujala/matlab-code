
function varargout = ComputeAndAppendSwimInfoToProcData_spontFast(pathList,varargin)
% ComputeAndAppendSwimInfoToProcData_spontFast - When given a list of
%   paths, reads the .mat files at the end of the paths, computes swim info
%   and appends this data to the .mat files
% procData = ComputeAndAppendSwimInfoToProcData_100HzImaging(pathList);
% [procData,swimInfo] =
%   Compute..Imaging(pathList,'overwriteAsk',overWriteAsk);
% lev  = 10;
% wname = 'sym8';
% kerSize = 200; % Kernel size in ms for fish onset and offset detection
freqRange = [0.25 60];
dj = 0.25;
thr_onset = 0.1; % When using ZscoreByHist
thr_peak = 5;
noiseThr = 25; % For residuals
minSwimDist = 300; % Min distance between swims in ms
nBodySegs = 10; % Number of segments to divide the body into when obtaining tail
arenaDiam = 50;
numWorkers = 10;

%   curvature angles
overWriteAsk = true; % If true, asks the user before overwriting exisiting swimInfo.

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'overwriteask'
                overWriteAsk = varargin{jj+1};
        end
    end
end


if ~iscell(pathList)
    pathList = {pathList};
end

procList = cell(length(pathList),1);
swimInfo_cell = procList;

p = gcp('nocreate');
if isempty(p)
    p = parpool(numWorkers);
end

tic
for pp = 1:length(pathList)
    disp(['Fish/path # ' num2str(pp)])
    currentPath = pathList{pp};
    if ischar(currentPath)
        procData = OpenMatFile(currentPath);
    else
        procData = currentPath;
    end
    
    fldNames = fieldnames(procData);
    processFlag = 1;
    if sum(strcmpi(fldNames,'swimInfo'))
        if overWriteAsk
            answer = input('"swimInfo" already exists. Skip? (y/n)','s');
            if strcmpi(answer,'y')
                disp('Skippping processing!')
                processFlag = 0;
            elseif strcmpi(answer,'n')
                processFlag = 1;
            else
                error('Please answer by typing "y" or "n" !');
            end
        end
    end
    if processFlag
        %--- Extract relevant data        
        fp = procData.fishPos;       
        fldNames = fieldnames(procData);
        if sum(strcmpi(fieldnames(procData),'fps'))
            fps = procData.fps;
        else
            fps = 500;
        end
        
        if sum(strcmpi(fldNames,'pxlLen'))==0
            disp('Pixel dimensions not found, computing from reference image...')
            [pxlLen,arenaEdgePts] = GetPxlSpacing(procData.ref,'diam',arenaDiam,'plotBool',0);
            procData.Properties.Writable = true;
            procData.pxlLen = pxlLen;
        else
            if sum(strcmpi(fldNames,'ref'))==0
                pxlLen = 0.058;
                procData.Properties.Writable = true;
                procData.pxlLen = pxlLen;
            else
                [pxlLen,arenaEdgePts] = GetPxlSpacing(procData.ref,'diam',arenaDiam,'plotBool',0);
            end            
        end
        disp(['Frame rate ' num2str(fps)])        
        tc = procData.tailCurv;
        %---Fill in tailcurves will all NaNs
        if sum(isnan(tc(:)))
            disp('Interpolating NaNs in tail curves...')
            tc = NanInterpTC(tc);
        end       
        
        disp('Computing head and tail angles...')
        hta = zeros(2,size(tc,3));       
        balf = zeros(1,size(tc,3));
        imgInds = 1:size(tc,3);      
        tc_spline = tc*0;
        hOr_fit = zeros(1,size(tc,3));
        for kk = imgInds
            tc_spline(:,:,kk) = FitBSplineToCurve(squeeze(tc(:,:,kk)),[],4);
            [hta(:,kk),~, hOr_fit(kk)] = GetHeadTailAngles(tc(:,:,kk),3);
            [~, balf(kk)] = GetHeadTailAngles(tc(:,:,kk),8);
        end       
        hta = resample_ap(hta',500,fps)'; % Resample head-tail angles
        balf = resample_ap(balf',500,fps)';
        fp = resample_ap(fp,500,fps); % Resample fish trajectory as well
        [curv,hOr] = GetTailTangents(tc_spline);
        procData.Properties.Writable = true;
        procData.hOr = hOr;
        curv = resample_ap(curv',500,fps)';
        hOr = resample_ap(hOr,500,fps);
        curv = chebfilt(curv',1/500,freqRange)';
        y_den = chebfilt(balf',1/500,freqRange)';
        
%         disp('Filtering with SST...')
%         [S,freq_s] = wsst(curv(end,:),500);
%         y_den = iwsst(S,freq_s,freqRange);
        
        disp('Computing residuals to identify noise..')
        noiseInds = find(abs(y_den)>300); % high amplitude noise
        y = curv(end,:);
        res = sqrt((y(:)-y_den(:)).^2);
     
        kerLen = ceil(200e-3*500); % Expect typical swims to be ~200ms
        ker = MakeGaussian(kerLen);
        res = ZscoreByHist(conv2(res(:),ker(:),'same'));
        pks = GetPks(res,'peakThr',noiseThr);
        noiseInds = union(noiseInds,pks); % high freq noise     
        disp('Computing WT for swim detection...')
        [W,freq] = ComputeXWT(y_den(:),y_den(:),1/500,...
            'freqRange',[15, freqRange(2)],'stringency',0.5);
        y_swim = ZscoreByHist(mean(abs(W),1));
        
        %--- Get Swim Info
        tic
        foo = chebfilt(y_swim,1/500,0.5,'low');
        y_swim = y_swim-foo;
        inds_lvl = LevelCrossings(y_swim,thr_onset);        
        
        if isempty(inds_lvl{1}) || isempty(inds_lvl{2})
            disp(['Not a single swim detected in fish/path # ' num2str(pp)])
            break
        end
        
        %--- Get rid of downward crossing indices that occur before the 1st upward
        %   crossing index
        delInds = find(inds_lvl{2}<inds_lvl{1}(1));
        inds_lvl{2}(delInds) = [];
        
        
        %--- Get rid of upward crossing indices that occur before the last downward
        %   crossing index
        delInds = find(inds_lvl{1}> inds_lvl{2}(end));
        inds_lvl{1}(delInds) = [];
        
        %--- Find maxima
        minSwimDist_inds = ceil(500*minSwimDist/1000); % From milliseconds to indices
        inds_swim = GetPks(y_swim,'polarity',0,'peakThr',thr_peak,...
            'thrType','abs','minPkDist',minSwimDist_inds);
        
        if isempty(inds_swim)
            disp('No peaks found in signal, check signal or threshold for peak detection!')
        end
        
        [onInds,offInds,durInds,maxFreqs,swimPows,swimDist_total, head_amp_max,...
            head_amp_com, head_amp_mean, head_vel_max, head_vel_mean, head_vel_com,...
            tail_amp_max, tail_amp_mean,tail_amp_com,tail_vel_max,tail_vel_mean,...
            tail_vel_com,phase_init, phase_slope, phase_mean,...
            headOr_amp_max,headOr_vel_max,freq_high_low_ratio,swimVel_max,...
            body_amp_max_s1,body_amp_max_s2,body_amp_mean_s1,...
            body_amp_mean_s2,body_amp_max,body_amp_max_time,...
            bendLoc_com,bendLoc_max,distFromEdge_min] = deal(nan(size(inds_swim)));
        [burstAmps,burstInts,bodyCurvs,totalCurvs,headOrs,swimDists,...
            swimTrajs,headTailAngles, distsFromEdge, bendLocs_cell, bendAmps] = deal(cell(size(inds_swim)));
        keepInds = zeros(size(inds_swim));
        swimInterval = diff(inds_swim);
        minGapBetweenSwims = round(500*300e-3); % 300 milliseconds
        tooCloseInds = find(swimInterval < minGapBetweenSwims)+1;
        if ~isempty(tooCloseInds)
            disp(['Treating ' num2str(numel(tooCloseInds)) ' as indistinct from previous swims'])
        end
        inds_swim(tooCloseInds) =[];
        for jj = 1:numel(inds_swim)
            % --Finding the onset and offset
            beforeInds = inds_lvl{1}-inds_swim(jj);
            beforeInds(beforeInds>=0)=-inf;
            if ~isempty(beforeInds)
                [~,tempInd] = max(beforeInds);
                
                %--- Get 10ms before swim onset
                onInds(jj) = max(inds_lvl{1}(tempInd)-round(100e-3*500),1);
            end
            afterInds = inds_lvl{2}-inds_swim(jj);
            afterInds(afterInds<=0) = inf;
            if ~isempty(afterInds)
                [~,tempInd] = min(afterInds);
                
                %--- Add 50 ms to offInds to increase the chances of getting
                %--- the full episode
                offInds(jj)= min(inds_lvl{2}(tempInd) + round(200e-3*500),length(y_den));
                
                swimInds = onInds(jj):offInds(jj);
                y_now = y_den(swimInds);
                pks_now = GetPks(y_now,'polarity',0,'peakThr',5,'thrType','abs');
                dPks_now = diff(pks_now);
                maxGap = round(500*(100e-3)); % If gap between peaks exceeds this, then discard
                if numel(dPks_now)>1
                    inds_gap = find(dPks_now>=maxGap);
                    if ~isempty(inds_gap)
                        stopInd = inds_gap(1);
                        inds_within = 1:stopInd;
                        pks_now = pks_now(inds_within);
                    end               
                    onInds(jj) = swimInds(max(pks_now(1)-round(100e-3*500),1));
                    offInds(jj) = swimInds(min(pks_now(end) + round(200e-3*500),length(y_now)));                 
                else
                    keepInds(jj) = 0;
                end
                durInds(jj) = offInds(jj)-onInds(jj);
            end
            swimInds = onInds(jj):offInds(jj);
            y_now = y_den(swimInds);
            noiseOverlap = intersect(swimInds,noiseInds);
            if isempty(noiseOverlap)
                hta_now = hta(:,swimInds);
                disp('Filtering head and tail angles...')
                for seg = 1:size(hta_now,1)
                    if size(hta_now,2) > 4                    
                        [s,f_s] = wsst(hta_now(seg,:),500);                        
                        fr = freqRange;
                        fr(1) = max(fr(1),min(f_s));
                        fr(2) = min(fr(2),max(f_s));
                        tmp  = iwsst(s,f_s,fr);
                        hta_now(seg,:) = tmp(:)';                           
                    end                   
                end
                if ~any(isnan(hta_now))
                    keepInds(jj) =1;
                    %---Head and tail angles
                    headTailAngles{jj} = hta_now;
                    %                     thetas_spline = FitBSplineToCurve(thetas_now',[],3);
                    thetas_spline = hta_now';                    
                    [~,thetas_vel] = gradient(thetas_spline);
                    
                    y = thetas_spline(:,1);
                    dy = thetas_vel(:,1);
                    time_norm = linspace(0,100,length(y));
                    head_amp_max(jj) = max(abs(y));
                    minPkDist = round((0.5/60)*500); % 60Hz is the fastest possible freq
                    pks = GetPks(y,'polarity',0,'minPkDist',minPkDist,...
                        'peakThr',5,'thrType','rel');
                    if isempty(pks)
                        pks = 1:length(y);
                    end
                    head_amp_com(jj) = dot(time_norm(pks),abs(y(pks)))/sum(abs(y(pks)));
                    head_amp_mean(jj) = mean(abs(y(pks)));
                    
                    head_vel_max(jj) = max(abs(dy));
                    pks = GetPks(dy,'polarity',0,'minPkDist',minPkDist,...
                        'peakThr',1.5,'thrType','rel');
                    if isempty(pks)
                        pks = 1:length(dy);
                    end
                    head_vel_mean(jj) = mean(abs(dy(pks)));
                    head_vel_com(jj) = dot(time_norm(pks),abs(dy(pks)))/sum(abs(dy(pks)));
                    
                    y =  thetas_spline(:,2);
                    dy = thetas_vel(:,2);
                    
                    pks = GetPks(y,'polarity',0,'minPkDist',minPkDist,...
                        'peakThr',5,'thrType','rel');
                    if isempty(pks)
                        keepInds(jj) = 0;
                        pks = 1:length(y);
                    end
                    tail_amp_com(jj) = dot(time_norm(pks),abs(y(pks)))/sum(abs(y(pks)));
                    tail_amp_max(jj) = max(abs(y));
                    tail_amp_mean(jj) = mean(abs(y(pks)));
                    
                    tail_vel_max(jj) = max(abs(dy));
                    pks = GetPks(dy,'polarity',0,'minPkDist',minPkDist,...
                        'peakThr',1.5,'thrType','rel');
                    if isempty(pks)
                        keepInds(jj) = 0;
                        pks = 1:length(dy);
                    end
                    tail_vel_mean(jj) = mean(abs(dy(pks)));
                    tail_vel_com(jj) = dot(time_norm(pks),abs(dy(pks)))/sum(abs(dy(pks)));
                    
                    W_head_tail = ComputeXWT(thetas_spline(:,1),thetas_spline(:,2),1/500,...
                        'freqRange',freqRange,'dj',dj,'freqScale','lin','stringency',0);
                    [~, phaseDiff_maxPow,~,maxPow] = instantaneouswavephase(W_head_tail);
                    phaseDiff_maxPow(phaseDiff_maxPow>180) = phaseDiff_maxPow(phaseDiff_maxPow>180)-360;
                    time_ep = (0:length(y)-1)/500;
                    if numel(phaseDiff_maxPow)> 2
                        X = [ones(length(time_ep),1),time_ep(:)];
                        M = lscov(X,phaseDiff_maxPow(:),maxPow(:));
                        %            phase_pred = X*M;
                        phase_init(jj) = M(1);
                        phase_slope(jj) = M(2);
                    elseif numel(phaseDiff_maxPow) ==2
                        phase_init(jj) = phaseDiff_maxPow(1);
                        phase_slope(jj) = diff(phaseDiff_maxPow)/diff(time_ep);
                    else
                        phase_init(jj) = 0;
                        phase_slope(jj) = 0;
                    end
                    phase_mean(jj) = sum(phaseDiff_maxPow.*maxPow)/sum(maxPow);
                    
                    %--- Total body curvature
%                     onInd_sub = round(onInds(jj)*fps/500);
%                     offInd_sub = round(offInds(jj)*fps/500);                  
                    bodyCurvs{jj} = curv(:,swimInds);
                    
                    foo = FitBSplineToCurve(y_now(:),[],3);
                    totalCurvs{jj} = foo;
                    body_amp_max_s1(jj) = max(foo);
                    body_amp_max_s2(jj) = abs(min(foo));
                    [body_amp_max(jj),body_amp_max_time(jj)] = max(abs(foo));
                    body_amp_max_time(jj) = time_norm(body_amp_max_time(jj));
                    pks = GetPks(foo,'polarity',0,'minPkDist',minPkDist,...
                        'peakThr',5,'thrType','rel');
                    if isempty(pks)
                        keepInds(jj) = 0;
                        pks = 1:length(y);
                    end                    
                    bendLocs = BendLocForSomePerc(bodyCurvs{jj}(:,pks),50);
                    bendAmps{jj} = foo(pks);
                    bendLocs_cell{jj} = bendLocs;
                    bendLoc_com(jj) = dot(abs(foo(pks)),bendLocs)/sum(abs(foo(pks)));
                    [~,pk_max] = max(abs(foo(pks)));
                    bendLoc_max(jj) = bendLocs(pk_max);
                    burstAmps{jj} = abs(foo(pks));
                    burstInts{jj} = gradient(pks)*(1/500)*1000;
                    foo_pks = foo(pks);
                    body_amp_mean_s1(jj) = mean(foo_pks(foo_pks>0));
                    body_amp_mean_s2(jj) = abs(mean(foo_pks(foo_pks<0)));                    
                    [w,freq] = ComputeXWT(foo(:),foo(:),1/500,'freqRange',freqRange,'dj',dj,...
                        'freqScale','lin');
                    %                     w = W(:,onInds(jj):offInds(jj));
                    [mf,~] = instantaneouswavefreq(sqrt(w),freq);
                    maxFreqs(jj) = max(mf);
                    freqInds = freq>=35;
                    foo = w(freqInds,:);
                    pow_above = mean(foo(:));
                    freqInds = freq<35;
                    foo = w(freqInds,:);
                    pow_below = mean(foo(:));
                    freq_high_low_ratio(jj) = (pow_above/pow_below)*100;                                 
                    
                    %--- Swim trajectories
                    swimTrajs{jj} = fp(onInds(jj):offInds(jj),:);                   
                    lenDiff = size(swimTrajs{jj},1)-length(totalCurvs{jj});
                    if lenDiff>0
                        swimTrajs{jj} = swimTrajs{jj}(1:length(totalCurvs{jj}),:);
                    elseif lenDiff <0                       
                        swimTrajs{jj}(end+1:end-lenDiff,:) = swimTrajs{jj}(end,:);                       
                    end 
                    swimTrajs{jj} = FitBSplineToCurve(swimTrajs{jj},[],4);                    
                    [~, swimDists{jj}] = DeltaDistance(swimTrajs{jj});
                    swimDists{jj} = swimDists{jj}*pxlLen*500;
                    overInds = swimDists{jj} > 100; % Assuming this is too fast (40 mm/s)
                    %--Include end points if at edges
                    if ~isempty(intersect(find(overInds),2))
                        overInds(1) = 1;
                    end
                    if ~isempty(intersect(find(overInds),length(swimDists{jj})-1))
                        overInds(end) = 1;
                    end
                    x = 1:size(swimTrajs{jj},1);
                    swimTrajs{jj}(:,1) = interp1(x(~overInds),swimTrajs{jj}(~overInds,1),...
                        x,'nearest','extrap');
                    swimTrajs{jj}(:,2) = interp1(x(~overInds),swimTrajs{jj}(~overInds,2),...
                        x,'nearest','extrap');
                    [~, swimDists{jj}] = DeltaDistance(swimTrajs{jj});
                    swimDists{jj} = swimDists{jj}*pxlLen*500;
                    swimVel_max(jj) = max(swimDists{jj});
                    swimDists{jj} = cumsum(swimDists{jj}*(1/500));
                    swimDist_total(jj) = max(swimDists{jj});
                    
                     %-- Head orientation
                    foo = hOr(swimInds);                                       
                    %---Resampling based on subsampled inds can change length,
                    %---so adjusting
                    lenDiff = length(foo)-length(totalCurvs{jj});
                    if lenDiff >0
                        foo = foo(1:length(totalCurvs{jj}));
                    elseif lenDiff<0
                        foo(end+1:end-lenDiff) = foo(end);
                    end                   
                    foo = interp1(x(~overInds),foo(~overInds),x,'nearest','extrap');
                    foo = FitBSplineToCurve(foo(:),[],5);
                    foo = foo-foo(1);
                    headOrs{jj} = foo;
                    headOr_amp_max(jj) = max(abs(foo));
                    dfoo = gradient(foo)*500;
                    headOr_vel_max(jj) = max(abs(dfoo));
                    
                    %--- Swim power
                    swimEp_pow = y_swim(swimInds);
                    swimPows(jj) = max(swimEp_pow);
                    
                    %--- Distance from arena edge
                    distsFromEdge{jj} = ShortestDistFromFixed(swimTrajs{jj},arenaEdgePts);
                    distsFromEdge{jj} = 2*(distsFromEdge{jj}*pxlLen)/arenaDiam;
                    distFromEdge_min(jj) = min(distsFromEdge{jj});
                end
            end
        end
%         nanInds = find(isnan(swimPows));
        delInds = zeros(size(keepInds));
        for jj = 2:numel(keepInds)
            if length(totalCurvs{jj-1}) == length(totalCurvs{jj})
                c = corrcoef(totalCurvs{jj-1},totalCurvs{jj});
                c = c(2);
                if (c-1) < 0.01
                    delInds(jj) = 1;
                end
            end
        end
        keepInds = find(keepInds);
        keepInds = setdiff(keepInds,find(delInds));
%         keepInds = union(keepInds,setdiff(1:numel(inds_swim),nanInds));
        
        delInds = setdiff(1:numel(inds_swim),keepInds);
        disp([num2str(numel(delInds)) '/' num2str(numel(inds_swim)) 'swim episodes will be deleted due to noise'])
        toc
        
        dur = (durInds/500)*1000; % Duration in ms
        onsets = (onInds/500)*1000;
        
        %--- Save to procData
        disp('Writing to .mat file...')
        tic
        swimInfo = struct;
        swimInfo.fps = 500;
        swimInfo.bodyCurvs = bodyCurvs(keepInds); % The tail bending angles for all points
        %       along the fish body
        
        swimInfo.totalCurvs = totalCurvs(keepInds); % The sum of all the bending angles along
        %       the fish's body.
        swimInfo.headTailAngles = headTailAngles(keepInds); % 3 line segments fit to the
        %       centerline of the fish and head and tail angles extracted by
        %       comparing angles between the 1st-2nd and 2nd-3rd line segments
        %       respectively. Cell array variable, where each cell corresponds to
        %       an episode. The matrix in each cell has size = [2,k] where k is the
        %       length of the episode (# of time points) while the 1st and 2nd rows
        %       correspond to the head and tail bending angles respectively.
        swimInfo.headOrs = headOrs(keepInds); % Actual head orientation of the fish, but
        %       w.r.t the orientation at the first time point of the fish.
        swimInfo.headOr_amp_max = headOr_amp_max(keepInds);
        swimInfo.headOr_vel_max = headOr_vel_max(keepInds);
        swimInfo.swimTrajs = swimTrajs(keepInds);
        swimInfo.swimDists = swimDists(keepInds);
        swimInfo.swimDist_total = swimDist_total(keepInds);
        swimInfo.swimVel_max = swimVel_max(keepInds);
        swimInfo.onsets = onsets(keepInds);
        swimInfo.onInds = round(onInds(keepInds)*fps/500);
        swimInfo.offInds = round(offInds(keepInds)*fps/500);
        swimInfo.dur = dur(keepInds);
        swimInfo.burstAmps = burstAmps(keepInds);
        swimInfo.burstInts = burstInts(keepInds);
        swimInfo.maxFreqs = maxFreqs(keepInds);
        swimInfo.swimPows = swimPows(keepInds);
        swimInfo.head_amp_max = head_amp_max(keepInds);
        swimInfo.head_amp_mean = head_amp_mean(keepInds);
        swimInfo.head_amp_com = head_amp_com(keepInds);
        swimInfo.head_vel_max = head_vel_max(keepInds);
        swimInfo.head_vel_mean = head_vel_mean(keepInds);
        swimInfo.head_vel_com = head_vel_com(keepInds);
        swimInfo.tail_amp_max = tail_amp_max(keepInds);
        swimInfo.tail_amp_mean = tail_amp_mean(keepInds);
        swimInfo.tail_amp_com = tail_amp_com(keepInds);
        swimInfo.tail_vel_max = tail_vel_max(keepInds);
        swimInfo.tail_vel_mean = tail_vel_mean(keepInds);
        swimInfo.tail_vel_com = tail_vel_com(keepInds);
        swimInfo.body_amp_max_s1 = body_amp_max_s1(keepInds);
        swimInfo.body_amp_max_s2 = body_amp_max_s2(keepInds);
        swimInfo.body_amp_mean_s1 = body_amp_mean_s1(keepInds);
        swimInfo.body_amp_mean_s2 = body_amp_mean_s2(keepInds);
        swimInfo.body_amp_max = body_amp_max(keepInds);
        swimInfo.body_amp_max_time = body_amp_max_time(keepInds);
        swimInfo.bendLocs = bendLocs_cell(keepInds);
        swimInfo.bendLoc_max = bendLoc_max(keepInds);
        swimInfo.bendLoc_com = bendLoc_com(keepInds);
        swimInfo.bendAmps = bendAmps(keepInds);
        swimInfo.phase_init = phase_init(keepInds);
        swimInfo.phase_slope = phase_slope(keepInds);
        swimInfo.phase_mean = phase_mean(keepInds);
        swimInfo.freq_high_low_ratio = freq_high_low_ratio(keepInds);
        swimInfo.distsFromEdge = distsFromEdge(keepInds);
        swimInfo.distFromEdge_min= distFromEdge_min(keepInds);
        procData.Properties.Writable = true;
        procData.swimInfo = swimInfo;
        procData.Properties.Writable = false;
        procList{pp} = procData;
        swimInfo_cell{pp} = swimInfo;
    else
        procList{pp} = procData;
        swimInfo_cell{pp} = procData.swimInfo;
    end
    toc
end
disp('All fish processed!')
toc
varargout{1} = procList;
varargout{2} = swimInfo_cell;

end

function y = BendLocForSomePerc(tailCurv,perc)
L = Standardize(1:size(tailCurv,1))*100;
negInds = find(tailCurv(end,:)<0);
tc = tailCurv;
tc(:,negInds) = -tc(:,negInds);
tc = (tc - repmat(tc(1,:),[size(tc,1),1]))./repmat(tc(end,:)-tc(1,:),[size(tc,1),1]);
tc = tc*100;
minInds = zeros(size(tc,2),1);
for jj = 1:size(tc,2)
    minInds(jj)= find((tc(:,jj)-perc)>=0,1,'first');
end
y = L(minInds);
end

function varargout = GetHeadTailAngles(tailCurv,nSegs)
% Formerly --> varargout = GetHeadTailAngles(tailCurv,smoothingLen)
% L = size(tailCurv,1);
% tc_spline = FitBSplineToCurve(squeeze(tailCurv),[],round(L/smoothingLen));
% [~,y] = FitLineSegmentsToCurve(squeeze(tc_spline),3);

[~,y, hOr] = FitLineSegmentsToCurve(tailCurv,nSegs);
varargout{1} = -y;
varargout{2} = sum(-y);
hOr = hOr(1);
varargout{3} = hOr;

end

function tc = NanInterpTC(tc)
% When given tail curves by ProcessFishImages_, returns the same after
% 2D interpolation, following the removal of nans
% tc = nanInterpTC(tc);

C = squeeze(tc(:,1,:));
nanInds = isnan(sum(C,1));
[X,Y] = meshgrid(1:size(C,2),1:size(C,1));
CC = interp2(X(:,~nanInds),Y(:,~nanInds),C(:,~nanInds),X,Y,'spline');
tc(:,1,:) =CC;
C = squeeze(tc(:,2,:));
nanInds = isnan(sum(C,1));
CC = interp2(X(:,~nanInds),Y(:,~nanInds),C(:,~nanInds),X,Y,'spline');
tc(:,2,:) = CC;
end


