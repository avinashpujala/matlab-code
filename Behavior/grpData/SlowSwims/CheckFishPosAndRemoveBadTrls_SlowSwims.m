function CheckFishPosAndRemoveBadTrls_SlowSwims(pathList,varargin)
% CheckFishPosAndRemoveBadTrls_SlowSwims - When given a list of paths to
%   .mat files written by SlowSwims.m, checks fish position for the correct
%   size and removes any bad trials determined by the fit error between the
%   detected fish position (ProcessFishImages_1802) and a B-spline fit to
%   it by a 4th order curve with smoothness of 10 (FitBSplineToCurve.m).
%   Also appends the corrected fish position variable to the .mat file
% CheckFishPosAndRemoveBadTrls_SlowSwims(pathList)
% CheckFishPosAndRemoveBadTrls_SlowSwims(pathList,'thr_noise',thr_noise,'plotBool',plotBool)
% Inputs:
% pathList - Cell array of paths where the .mat files are located.
% thr_noise - Scalar (default =  3.5; empirically determined) specifying the acceptable level
%   of noise before a trial is discarded. If fit error (eta) < thr_noise,
%   the trial is kept, else discarded
% plotBool - Logical. If true, plots each of the trial

%% Inputs
thr_noise = 3.5; % I checked this for a few trials and seems to work well
plotBool = false;

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'thr_noise'
                thr_noise = varargin{jj+1};
            case 'plotbool'
                plotBool = varargin{jj+1};
        end
    end
end

if iscell(pathList)
    nFish = length(pathList);
else
    nFish = 1;
    pathList = {pathList};
end
DistBetweenPts = @(pt1,pt2)sqrt(sum((pt2-pt1).^2));
for fish = 1:nFish
    disp([num2str(fish) '/' num2str(nFish) ' fish'])
    procData = OpenMatFile(pathList{fish});
    %--- Read relevant variables from proData
    fp = procData.fishPos;
    nFramesInTrl = procData.trlLen * procData.frameRate;
    %     nTrls = procData.nTrls;
    nTrls = size(fp,3);
    if size(fp,1)> nFramesInTrl
        nTrls = floor(size(fp,1)/nFramesInTrl);
    end
    [fp,nTrls] = CorrectFishPosDims(fp,nTrls,nFramesInTrl);
    
    nTrls = min(nTrls,size(fp,3));
    procData.Properties.Writable = true;
    procData.fishPos = fp;
    
    %--- Getting rid of bad trials
    eta = zeros(1,nTrls);
    if plotBool
        figure('Name','Checking trajectories')
        img = procData.img_bg*0;
    end
    
    for trl = 1:nTrls
        if trl ==5
            a = 1;
        end
        curv = squeeze(fp(:,:,trl));
        nanRows = find(sum(isnan(curv),2));
        curv(nanRows,:)=[];
        if size(curv,1) > 3
            curv_fit = FitBSplineToCurve(curv,[],10);
            res = sqrt(sum((curv_fit-curv).^2,2));
            jumpInds = find(zscore(res)>3);
            if ~isempty(jumpInds)
                [blockSizes,blockInds] = GetContiguousBlocks(jumpInds);
                for bb = 1:numel(blockSizes)
                    block = jumpInds(blockInds(bb):blockInds(bb)+blockSizes(bb)-1);
                    if (block(1)>1) && (block(end)< (size(curv,1)))
                        blockPts = curv(block,:);
                        if size(blockPts,1)>1
                              dd = deltaDistance(blockPts);
                              [~,maxInd] = max(dd);
                              maxInd = maxInd(1);
                              block = block(maxInd+1:end);
                              blockPts = curv(block,:);
                              blockSizes(bb) = numel(block);
                        end                      
                        beforeBlockPt = curv(block(1)-1,:);
                        afterBlockPt = curv(block(end)+1,:);
                        jumpDist = DistBetweenPts(beforeBlockPt,blockPts(1,:));
                        gapDist = DistBetweenPts(beforeBlockPt,afterBlockPt);
                        if jumpDist > 2*gapDist
                            blockPts(:,1) = floor(interp1([0,1],[beforeBlockPt(1),afterBlockPt(1)],linspace(0,1,blockSizes(bb))));
                            blockPts(:,2) = floor(interp1([0,1],[beforeBlockPt(2),afterBlockPt(2)],linspace(0,1,blockSizes(bb))));
                            blockPts(blockPts<1) = 1;
                            curv(block,:) = blockPts;
                        end
                    end
                end
                nonNanRows = setdiff(1:size(fp,1),nanRows);              
                fp(nonNanRows,1,trl) = curv(:,1);
                fp(nonNanRows,2,trl) = curv(:,2);              
            end
            curv_fit = FitBSplineToCurve(curv,[],10);
            res = sqrt(sum((curv_fit-curv).^2,2));
            eta(trl) = sum(res)/size(curv,1);
        end
        if plotBool
            cla
            imagesc(img),colormap(gray),box off
            hold on
            plot(curv(:,1),curv(:,2),'b')
            plot(curv_fit(:,1),curv_fit(:,2),'r')
            title(['Trl # ' num2str(trl) ', Error = ' num2str(eta(trl))])
            legend('Actual','Fit')
            shg
            pause()
        end
    end
    delTrls =  find(eta>thr_noise);
    %     fp(:,:,delTrls) =[];
    if isempty(delTrls)
        disp('No bad trials')
    else
        disp(['Bad trials: ' num2str(delTrls)])
    end
    
    
    disp('Appending cleaned fish position variable to .mat file..')
    procData.delTrls= delTrls;
    procData.fishPos = fp;
    procData.Properties.Writable = false;
end


end

function [fp,nTrls_new] = CorrectFishPosDims(fp,nTrls,nFramesInTrl)
%--- Check for incorrect fishPos variable size that may have resulted in a bug in the code being used
%    during the time period = [20180305 - 2010307]
inds_trl = 1:nFramesInTrl;
T = @(trlNum,nFramesInTrl)((trlNum-1)*nFramesInTrl+1):((trlNum-1)*nFramesInTrl)+nFramesInTrl;

count = nTrls;
if nFramesInTrl ~= size(fp,1);
    count = 0;
    disp('Correcting fishPos size!')
    for trl = 1:nTrls
        inds = T(trl,nFramesInTrl);
        inds(inds>size(fp,1))=[];
        if ~isempty(inds)
            count = count + 1;
            fp(inds_trl,:,trl) = fp(inds,:,trl);
        end
    end
end
fp = fp(inds_trl,:,:);
nTrls_new = count;
end
