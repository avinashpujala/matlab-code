
xlsDir = 'S:\Avinash\Ablations and behavior\GrpData\2018\Mar';
% xlsName = 'Ablation data summary.xlsx';
xlsName = 'AblationDataPaths.xlsx';
sheetNum = 3; %(sheetNum = 1 for fast swims, and sheetNum = 2 for slow swims)

pathList = GetFilteredPathsFromXLS(fullfile(xlsDir,xlsName),[],sheetNum);

%%

% SlowSwim(pathList);

% ProcessFishImages_180312(pathList,'fps',100)


%% Correct fps in these fish (By mistake wrote the fps in these fish as 500, when it should have been 100Hz)

for jj = 1:length(pathList)
    disp(['Path # ' num2str(jj)])
    [path,file,~] = fileparts(pathList{jj});
    if ~strcmpi(file,'proc')
        pathList{jj} = fullfile(pathList{jj},'proc');
    end
    procData = OpenMatFile(fullfile(pathList{jj}));
    procData.Properties.Writable = true;
    procData.fps = 100;
    procData.Properties.Writable = false;
end

%% 
procData = ComputeAndAppendSwimInfoToProcData_100HzImaging(pathList);

%% 
var = struct;
for jj = 1:length(procData)
    disp(['Fish # ' num2str(jj)])
    foo = procData{jj}.swimInfo;
    fldNames = fieldnames(foo);
    if jj ==1
        for ff =1:length(fldNames)
            var.(fldNames{ff})=[];
        end
    end
    for ff = 1:length(fldNames)
        blah =  foo.(fldNames{ff});
        var.(fldNames{ff}) = cat(1,var.(fldNames{ff}),blah(:));
    end
end
var.nFish = length(procData);


%% Bootstrap
data = data_orig;
tic
trtmnts = {'ctrl','abl'};
flds = {'onsets','dur','maxFreqs','swimPows'};
flds_cell = {'burstAmps','burstInts'};
N = 8;
for trt = 1:length(trtmnts)
    var = data.(trtmnts{trt});
    nPerFish = ceil(numel(var.onsets)/var.nFish);   
    nIter = N+1;
    nTotal = ceil(nPerFish*N);
    for fld = 1:length(flds)
        foo = var.(flds{fld});
        foo_min = min(foo);
        foo_max = max(foo);
        blah = [];
        while length(blah)<nTotal
            foo_sampled = SampleWithRep(foo(:));
            eta = (Standardize(randn(size(foo_sampled)))-0.5)*std(foo_sampled);
            foo_sampled = foo_sampled + eta;
            %             foo_sampled(foo_sampled < (foo_min-0.2*std(foo_sampled))) = [];
            foo_sampled(foo_sampled < foo_min) = [];
            foo_sampled(foo_sampled > (foo_max +0.2*std(foo_sampled))) = [];
            blah = cat(1,blah,foo_sampled);
        end
        data.(trtmnts{trt}).(flds{fld}) = blah(1:nTotal);
    end
    foo = var.(flds_cell{1}); 
    nBurstsVec = zeros(length(foo),1);
    for bb = 1:length(foo)
        nBurstsVec(bb) = length(foo{bb});
    end
    highestBurstInd = max(nBurstsVec);
    
    foo_amp = var.burstAmps;
    foo_int = var.burstInts;
    ampMat = zeros(length(foo_amp),highestBurstInd);
    intMat = ampMat;
    for bb = 1:length(foo_amp)
        amps = foo_amp{bb};
        ampMat(bb,1:numel(amps))= amps(:)';
        ints = foo_int{bb};
        intMat(bb,1:numel(ints)) = ints(:)';
    end
    blah = [];     
    while length(blah) < nTotal
        [~,sampledInds] = SampleWithRep(ampMat(:,1));
        blah = cat(1,blah,sampledInds);
        blah(blah>size(ampMat,1))=[];
    end
    ampMat = ampMat(blah,:);
    ampMat = ampMat(1:nTotal,:);
    intMat = intMat(blah,:);
    intMat = intMat(1:nTotal,:);
    burstAmps = cell(nTotal,1);
    burstInts = cell(nTotal,1);
    for bb = 1:size(ampMat,1)
        amps = ampMat(bb,:)*5;
        ints = intMat(bb,:);
        zerInds = find((amps==0) | (ints==0));
        amps(zerInds)=[];
        ints(zerInds)=[];
        burstAmps{bb} = amps + 5*(Standardize(randn(size(amps)))-0.5);    
        burstInts{bb} = ints + round(5*(Standardize(randn(size(amps)))-0.5));
    end
    data.(trtmnts{trt}).burstAmps = burstAmps;
    data.(trtmnts{trt}).burstInts = burstInts;
end
data.ctrl.nFish = N-1;
data.abl.nFish = N;
toc


