
%% Setting inputs
xlsDir = 'S:\Avinash\Ablations and behavior\GrpData\2018\Mar';
xlsName = 'AblationDataPaths.xlsx';
sheetNum = 2;

xlsPath = fullfile(xlsDir,xlsName);
saveDir = 'S:\Avinash\Ablations and behavior\GrpData\2018\Mar\proc\Alx_young_and_old\Alx_young\SlowSwims\csv files';


%% Get raw xls data
[num,txt,data_xls] = xlsread(xlsPath,sheetNum);

%% Create grp data struct
headers = data_xls(1,:);

% ---- Find ablation type, ablation bool, and tracked cols ----
ablationTypeCol = find(strcmpi(headers,'AblationType'));
fishNumCol = find(strcmpi(headers,'FishNum'));
ablationCol = find(strcmpi(headers,'AblationBool'));
trackedCol = find(strcmpi(headers,'Tracked'));
pathCol = find(strcmpi(headers,'Path'));
ablationTypes = unique(data_xls(2:end,ablationTypeCol));

data_sub = data_xls(2:end,:);

paths = struct;
for jj = 1:length(ablationTypes)
    abType = ablationTypes{jj};
    rowInds = strcmpi(data_sub(:,ablationTypeCol),abType);
    paths.(abType) = struct;
    foo = data_sub(rowInds,:);
    
    ctrlInds = ([foo{:,ablationCol}]==0 & [foo{:,trackedCol}]==1);
    paths.(abType).ctrl = foo(ctrlInds,pathCol);
    
    ablInds = ([foo{:,ablationCol}]==1 & [foo{:,trackedCol}]==1);
    paths.(abType).abl = foo(ablInds,pathCol);    
end

