
%% Inputs

xlsDir = 'S:\Avinash\Notes\Apr 2018';
xlsName = 'AblationDataPaths_alxOnly.xlsx';
sheetNum = 1; %(sheetNum = 1 for fast swims, and sheetNum = 2 for slow swims,
% sheetNum = 3; %for slow swims at Fs > 100Hz)

saveDir = fullfile(xlsDir,'saved');
if ~exist(saveDir, 'dir')
    mkdir(saveDir)
end
ts = datestr(now,30);
fName = ['grpData_alx_young_old_' ts];


%% Read paths from excel sheet
disp('Reading excel sheet...')
[num,txt,raw] = xlsread(fullfile(xlsDir,xlsName),sheetNum);
raw_vals = raw(2:end,:);

grpCol = find(strcmpi(raw(1,:),'AblationType'));
pathCol = find(strcmpi(raw(1,:),'path'));
stimTypeCol = find(strcmpi(raw(1,:),'StimType'));
ablationBoolCol = find(strcmpi(raw(1,:),'AblationBool'));
trackedCol = find(strcmpi(raw(1,:),'Tracked'));
fishNumCol = find(strcmpi(raw(1,:),'FishNum'));
exptDateCol = find(strcmpi(raw(1,:),'Date'));
exptTimeCol = find(strcmpi(raw(1,:),'Time'));
embedCol = find(strcmpi(raw(1,:),'EmbedBool'));
doubleTricaineCol = find(strcmpi(raw(1,:),'DoubleTricaineBool'));

%--Remove NaNs
nanInds = find(isnan([raw_vals{:,fishNumCol}]));
raw_vals(nanInds,:) = [];


%---Only consider vibration stimulus trials
% vibInds = strcmpi(raw_vals(:,stimTypeCol),'vib');
% raw_vals = raw_vals(vibInds,:);


if isempty(grpCol) || isempty(pathCol) || isempty(stimTypeCol)...
        || isempty(ablationBoolCol)
    error('Did not find specified column, check for errors!')
end

grpNames = unique(raw_vals(:,grpCol));
pathList = raw_vals(:,pathCol);

if isempty(raw_vals)
    error('No matching values found in excel sheet')
else
    disp([num2str(length(pathList)) ' paths found'])
end


%% Extracting data and storing into 1st-level nested structure variable

disp('Creating grpData...')
tic
grpData = struct;
count = 0;
% grpData.ablationType = ablationType;
% grpData.stimType = stimType;
% ignoreInds = [1, 26];
% ignoreInds = [23]; % For escape data
ignoreInds = []; % For spont data
noSwimInds = [];
pathList_keep = setdiff(1:length(pathList),ignoreInds);
emptyInds = zeros(length(pathList_keep),1);
pathLoopCount = 0;
for pp = pathList_keep
    pathLoopCount = pathLoopCount + 1;
    disp(['Fish/path # ' num2str(pp) '/' num2str(length(pathList))])
    disp([raw_vals{pp,grpCol}, ', AblationBool = ' num2str(raw_vals{pp,ablationBoolCol})])
    %     currPath = raw_vals{pp,pathCol};
    currPath = pathList{pp};
    [~,foo] = fileparts(currPath);
    if ~strcmpi(foo,'proc')
        currPath = fullfile(currPath,'proc');
    end
    if exist(currPath,'dir') ~=7
        error('Path not found! Check path')
    end
    try
    procData = OpenMatFile(currPath);
    catch
         procData = OpenMatFile(currPath);
    end
    if ~sum(strcmpi(fieldnames(procData),'swimInfo'))
        disp('swimInfo variable not found, computing')       
        [procData,swimInfo] = ...
            ComputeAndAppendSwimInfoToProcData_spontFast(procData,...
            'overWriteAsk',false);
        procData = procData{1};
        swimInfo = swimInfo{1};
    else
        swimInfo = procData.swimInfo;
        if isempty(swimInfo.bodyCurvs)
            disp('No curvatures detected!')
            disp('Computing swim info for')
            disp(currPath)           
            [procData,swimInfo] = ComputeAndAppendSwimInfoToProcData_spontFast(procData,...
                'overWriteAsk',false);
            procData = procData{1};
            swimInfo = swimInfo{1};
        end
    end
    if ~isempty(swimInfo)
        htas = swimInfo.headTailAngles;
        bodyAmps = swimInfo.totalCurvs;
        pxlLen = procData.pxlLen;
        %     fps = procData.fps;
        fps = swimInfo.fps; % This should be equal to 500 because of resampling in ComputeAndAppend...
        dt = 1/fps;
        nEps = length(htas);
        for ep = 1:nEps
            disp([num2str(ep) '/' num2str(nEps) ' episodes'])
            hta = htas{ep};
            hta = FitBSplineToCurve(hta',[],4)'; % To smoothen derivatives
            bodyAmp = bodyAmps{ep};
            bodyAmp = FitBSplineToCurve(bodyAmp(:),[],4);
            swimTraj = swimInfo.swimTrajs{ep};
            epInds = 1:size(hta,2);
            template = ones(size(epInds));
            grpInds = count + epInds;
            count = grpInds(end);
            blah = GetSwimParams(hta',dt);
            grpData.bodyAmp(grpInds) = bodyAmp;
            grpData.headAmp(grpInds) = hta(1,:);
            dy = (gradient(hta(1,:))/dt)*(pi/180);
            d2y = gradient(dy)/dt;
            grpData.headVel(grpInds) = dy;
            grpData.headAcc(grpInds) = d2y;
            nPks = length(blah(1).bendAmps);
            grpData.headAmp_pkInds(grpInds) = template*nan;
            grpData.headAmp_pkInds(grpInds(1:nPks)) = blah(1).ampInds;
            grpData.headAmp_max(grpInds) = max(hta(1,:));
            grpData.headAmp_bendInts(grpInds) = template*nan;
            grpData.headAmp_bendInts(grpInds(1:nPks)) = blah(1).bendInts;
            grpData.headVel_pkInds(grpInds) = template*nan;
            grpData.headVel_pkInds(grpInds(1:nPks)) = blah(1).velInds;
            grpData.headVel_max(grpInds) = max(dy);
            grpData.headAcc_pkInds(grpInds) = template*nan;
            grpData.headAcc_pkInds(grpInds(1:nPks)) = blah(1).accInds;
            grpData.headAcc_max(grpInds) = max(d2y);
            grpData.tailAmp(grpInds) = hta(2,:);
            dy = (gradient(hta(2,:))/dt)*(pi/180);
            d2y = gradient(dy)/dt;
            grpData.tailVel(grpInds) = dy;
            grpData.tailAcc(grpInds) = d2y;
            nPks = length(blah(2).bendAmps);
            grpData.tailAmp_pkInds(grpInds)= template*nan;
            grpData.tailAmp_pkInds(grpInds(1:nPks)) = blah(2).ampInds;
            grpData.tailAmp_max(grpInds) = max(hta(2,:));
            grpData.tailAmp_bendInts(grpInds) = template*nan;
            grpData.tailAmp_bendInts(grpInds(1:nPks)) = blah(2).bendInts;
            grpData.tailVel_pkInds(grpInds)= template*nan;
            grpData.tailVel_pkInds(grpInds(1:nPks)) = blah(2).velInds;
            grpData.tailVel_max(grpInds) = max(dy);
            grpData.tailAcc_pkInds(grpInds)= template*nan;
            grpData.tailAcc_pkInds(grpInds(1:nPks)) = blah(2).accInds;
            grpData.tailAcc_max(grpInds) = max(d2y);
            grpData.freq(grpInds) = blah(1).meanFreq;
            grpData.phaseDiff(grpInds) = blah(1).phaseDiff;
            or = swimInfo.headOrs{ep};
            grpData.headOr(grpInds) = or(:)';
            or_smooth = FitBSplineToCurve(or,[],10);
            grpData.headOr_max(grpInds) = max(abs(or_smooth));
            swimTraj_smooth = FitBSplineToCurve(swimTraj,[],10);
            trajAngles = GetTailTangents(swimTraj_smooth);
            grpData.swimTraj_x(grpInds) = swimTraj(:,1)';
            grpData.swimTraj_y(grpInds) = swimTraj(:,2)';
            grpData.swimTraj_angles(grpInds)= trajAngles;
            grpData.swimTraj_angles_max(grpInds) = max(trajAngles);
            grpData.swimVel(grpInds) = gradient(swimInfo.swimDists{ep});
            grpData.swimVel_max(grpInds) = template*swimInfo.swimVel_max(ep);
            ablationBool = raw_vals{pp,ablationBoolCol};
            if ablationBool == 0
                trtmnt = 'ctrl';
            elseif ablationBool ==1
                trtmnt = 'abl';
            end
            grpData.ablationBool(grpInds) = repmat({trtmnt},1,epInds(end));
            grpData.fishNum(grpInds) = pp*template;
            grpData.ablationGroup(grpInds) = repmat(raw_vals(pp,grpCol),1,epInds(end));
            grpData.stimType(grpInds) = repmat(raw_vals(pp,stimTypeCol),1,epInds(end));
            grpData.episodeNum(grpInds) = ep*template;
            grpData.swimDist_total(grpInds) = swimInfo.swimDist_total(ep)*template;
            grpData.distFromEdge(grpInds) = swimInfo.distsFromEdge{ep};
            grpData.distFromEdge_min(grpInds) = swimInfo.distFromEdge_min(ep)*template;
            grpData.swimOnInd(grpInds) = swimInfo.onInds(ep)*template;
            grpData.swimOffInd(grpInds) = swimInfo.offInds(ep)*template;
            grpData.exptDate(grpInds) = repmat(raw_vals(pp,exptDateCol),1,epInds(end));
            grpData.exptTime(grpInds) = repmat(raw_vals(pp,exptTimeCol),1,epInds(end));
            %         grpData.exptDate(grpInds) = repmat(raw_vals(pp,exptDateCol),1,epInds(end));
            grpData.embedBool(grpInds) = raw_vals{pp,embedCol}*template;
            grpData.doubleTricaineBool(grpInds) = raw_vals{pp,doubleTricaineCol}*template;
            bendLocs = swimInfo.bendLocs{ep};
            n = length(bendLocs);
            grpData.bendLocs(grpInds) = template*nan;
            grpData.bendLocs(grpInds(1:n)) = swimInfo.bendLocs{ep};
            grpData.bendLoc_com(grpInds) = template*swimInfo.bendLoc_com(ep);
            grpData.bendLoc_max(grpInds) = template*swimInfo.bendLoc_max(ep);
            grpData.bendAmps(grpInds) = template*nan;
            grpData.bendAmps(grpInds(1:n)) = swimInfo.bendAmps{ep};
            grpData.bendAmp_max(grpInds) = template*swimInfo.body_amp_max(ep);
            grpData.bendAmp_max_time(grpInds) = template*swimInfo.body_amp_max_time(ep);
        end
    else
        emptyInds(pathLoopCount) = 1;
    end    
end

grpData_orig = grpData;

grpData.headOr_max = mod(grpData.headOr_max,180);
minInt_ms = (500/60);
lowInds = (grpData.headAmp_bendInts*1000) < minInt_ms;
grpData.headAmp_bendInts(lowInds) = grpData.headAmp_bendInts(lowInds)*0 +...
    ones(size(grpData.headAmp_bendInts(lowInds)))*(minInt_ms/1000) +...
    rand(size(grpData.headAmp_bendInts(lowInds)))/1000;

lowInds = (grpData.tailAmp_bendInts*1000) < minInt_ms;
grpData.tailAmp_bendInts(lowInds) = grpData.tailAmp_bendInts(lowInds)*0 +...
    ones(size(grpData.tailAmp_bendInts(lowInds)))*(minInt_ms/1000) +...
    rand(size(grpData.tailAmp_bendInts(lowInds)))/1000;

%% Save data as .mat file
saveDir = fullfile(xlsDir,'saved/wholeTimeSeries');
if exist(saveDir,'dir')~=7
    mkdir(saveDir)
end
pathList = pathList(pathList_keep);
raw_vals = raw_vals(pathList_keep,:);
save([fullfile(saveDir,fName) '.mat'],'grpData', 'raw', 'raw_vals','pathList');
disp('Saved grpData')

%% Convert to cell array suitable for writing to excel sheet or csv file
disp('Converting to cell array for excel...')
fldNames = fieldnames(grpData);
nSamples = length(grpData.fishNum);
data_cell = cell(nSamples+1,length(fldNames));
tic
for jj = 2:size(data_cell,1)
    for fld = 1:length(fldNames)
        fld_now = fldNames{fld};
        foo = grpData.(fld_now)(jj-1);
        if iscell(foo)
            data_cell{jj,fld} = grpData.(fld_now){jj-1};
        else
            data_cell{jj,fld} = grpData.(fld_now)(jj-1);
        end
    end
end
for fld = 1:length(fldNames)
    fld_now = fldNames{fld};
    data_cell{1,fld} = fld_now;
end
toc

%% Write to excel
disp('Writing to excel...')
tic
xlswrite([fullfile(saveDir,fName) '.xlsx'], data_cell);
disp(['Saved at : ' saveDir])
toc

% %% Write to csv
% disp('Writing to csv...')
% tic
% csvwrite([fullfile(saveDir,fName) '.xlsx'], data_cell(2:end,:));
% disp(['Saved at : ' saveDir])
% toc



