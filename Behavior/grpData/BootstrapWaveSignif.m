%BOOTSTRAPWAVESIGNIF
% Unpacks, wavelet data, and hht data from grpData variable loaded into the MATLAB
%   workspace, and then when specified the following inputs...
%   grp = 'mHom', 'vent', or 'inter'
%   stim = 'vib' or 'dark'
%   nIter = 1000; (default = 1000);
%   ....


%% Get wave data from grpData
[W,H,M] = WaveDataFromGrpData(grpData,'delGrps','data_xls');


%% Inputs
grp = 'mHom';
stim = 'vib';
nIter = 1000;
wOrH  = 'W'; %('W','H', or 'M')

%% To avoid significant regions outside the coi, modulate these areas by the coi itself
if strcmpi(wOrH,'W')
    foo = grpData.(grp).ctrl.(stim).procData{1}.W;
    freq = foo.freq;
    time = foo.time;
    coi = foo.coi;
    coi(1)= coi(2);
    coi(end) = coi(end-1);
    [coiMat,freqMat] = meshgrid(coi,freq);
    coiBool = coiMat>freqMat;
    coiMat_mod = 1-Standardize((coiMat).^0.5.*coiBool);
end


%% Bootstrapping ablated (~15-20 mins in serial, ~ 3 mins in parallel)
abl = (W.(grp).(stim).abl).^2;
abl_shape = size(abl);
abl = abl.*(repmat(coiMat_mod,[1,1,abl_shape(3)]));

abl_vec = transpose(reshape(abl,...
    [abl_shape(1)*abl_shape(2),abl_shape(3)]));
tic
disp('Bootstrapping ablated...')
abl_boot = BootStrapStat(abl_vec,'nIter',nIter, 'processing','parallel');
toc

%% Bootstrapping control (~15-20 mins in serial, ~ 3 mins in parallel)
ctrl = (W.(grp).(stim).ctrl).^2;
ctrl_shape  = size(ctrl);
ctrl = ctrl.*(repmat(coiMat_mod,[1,1,ctrl_shape(3)]));
ctrl_vec = transpose(reshape(ctrl,...
    [ctrl_shape(1)*ctrl_shape(2),ctrl_shape(3)]));
tic
disp('Bootstrapping control...')
ctrl_boot = BootStrapStat(ctrl_vec,'nIter',nIter,'processing','parallel');
toc


%% Difference of bootstrapped means
diff_boot = ctrl_boot - abl_boot;
abl_boot_mat = reshape(abl_boot',[abl_shape(1:2),nIter]);
ctrl_boot_mat = reshape(ctrl_boot',[ctrl_shape(1:2),nIter]);

diff_boot_mat = (ctrl_boot_mat - abl_boot_mat).^2;
% diff_boot_mat_coi = diff_boot_mat.*repmat(1-coiBool,[1,1,nIter]);
% diff_shape = size(diff_boot_mat);
% diff_boot = transpose(reshape(diff_boot_mat_coi,[diff_shape(1)*diff_shape(2),diff_shape(3)]));

%% P values
% tic
% disp('Computing p-values, serially...')
% pVals = PValue(diff_boot,0,'valRange','independent');
% toc

tic
disp('Computing p-values, parallelly...')
[pVals, zScores] = PValue(diff_boot,0,'valRange','independent','processing','parallel');
toc

%%
zThr = 3;
cMap = viridis(256);

alphaVals = (1-pVals)*100;
alphaVals_mat = reshape(pVals,ctrl_shape(1:2));
zScores_mat = reshape(zScores,ctrl_shape(1:2));
zScores_mat_coi = zScores_mat.*(1-coiBool);

% figure
% imagesc(alphaVals_mat), colorbar
foo = grpData.(grp).ctrl.(stim).procData{1}.W;
freq = foo.freq;
time = foo.time;
coi = foo.coi;
coi(1)= coi(2);
coi(end) = coi(end-1);


figure('Name','Average Control')
% imagesc(mean(diff_boot_mat,3)), colorbar
imagesc(time,freq,sqrt(mean(ctrl_boot_mat,3))),colorbar
cl = get(gca,'clim');
set(gca,'ydir','normal','clim',cl)
hold on
contour(time, freq, zScores_mat_coi,[-zThr,zThr],'w--')
plot(time,coi,'k--')
xlabel('Time (s)')
ylabel('Freq (Hz)')
title('Ctrl')
colormap(cMap)

figure('Name','Average Ablated')
imagesc(time, freq,sqrt(mean(abl_boot_mat,3))),colorbar
set(gca,'ydir','normal','clim',cl)
hold on
contour(time, freq, zScores_mat_coi,[-zThr,zThr],'w--')
plot(time,coi,'k--')
xlabel('Time (s)')
ylabel('Freq (Hz)')
title('Ablated')
colormap(cMap)

% figure
% imagesc(mean(diff_boot_mat,3)),colorbar
% hold on
% contour(zScores_mat,[-5,5],'r--')
% title('Diff')

figure('Name','Average of Control - Ablated')
imagesc(time,freq,zScores_mat),colorbar
set(gca,'ydir','normal','clim',[-3*zThr, 3*zThr])
hold on
contour(time, freq, zScores_mat_coi,[-zThr,zThr],'w--')
plot(time,coi,'k--')
xlabel('Time (s)')
ylabel('Freq (Hz)')
title('Z-scores')
colormap(cMap)

break;

%% Saving data

saveDir = 'S:\Avinash\Ablations and behavior\GrpData\Intermediate RS\Bootstrap signif';

ts = datestr(now,30);
fName = ['bootData_' stim '_' ts '.mat'];

bootData = struct;
bootData.ctrl = ctrl_boot_mat;
bootData.abl = abl_boot_mat;
bootData.alphaVals = alphaVals_mat;
bootData.zScores = zScores_mat;

save(fullfile(saveDir,fName),'bootData');




