%% Attempting to correct fish pos

% pathList = paths.alx_young.ctrl;


xlsDir = 'S:\Avinash\Ablations and behavior\GrpData\2018\Mar';
% xlsName = 'Ablation data summary.xlsx';
xlsName = 'AblationDataPaths.xlsx';
sheetNum = 2; %(sheetNum = 1 for fast swims, and sheetNum = 2 for slow swims)

pathList = GetFilteredPathsFromXLS(fullfile(xlsDir,xlsName),[],sheetNum);

%% Append /proc subfolder to paths
for jj = 1:length(pathList)
    currPath = pathList{jj};
    [pathStr,pathName,~] = fileparts(currPath);
    if ~strcmpi(pathName,'proc')
        pathList{jj} = fullfile(currPath,'proc');
    end
end

% CheckFishPosAndRemoveBadTrls_SlowSwims(pathList(16));
CheckFishPosAndRemoveBadTrls_SlowSwims(slowDir);

%% Check corrected trials
plotBadTrls = false; % Boolean, which if true, results in plotting of bad trials as well.

ind_path = randperm(length(pathList));
ind_path = ind_path(1);

% ind_path = 12;
disp(['Fish # ' num2str(ind_path)])
procData = OpenMatFile(pathList{ind_path});
fp = procData.fishPos;
nTrls = procData.nTrls;
if ~plotBadTrls
    if nTrls == size(fp,3)
        if sum(strcmpi(fieldnames(procData),'delTrls'))
            delTrls = procData.delTrls;
            disp('Bad trials being removed...')
            disp(num2str(delTrls))
            fp(:,:, delTrls) = [];
        else
            disp('the field .delTrls not found. Run CheckFishPosAnrRemoveBadTrls_SlowSwims.m')
        end
    else
        disp('Bad trials appear to have been removed already!')
    end
end
nTrls = size(fp,3);
img = zeros(896,896);
figure
trlList = randperm(nTrls);
% trlList = [1,3,5,10,21];
clrMap = hsv(numel(trlList));
for trl = trlList
    cla
    imagesc(img),axis image, box off, colormap([0.2 0.2 0.2])
    hold on
%     plot(fp(:,1,trl),fp(:,2,trl),'.-','color',rand(3,1))
    plot(fp(:,1,trl),fp(:,2,trl),'.-','color',clrMap(trl,:))
    title(['Trl # ' num2str(trl)])
    shg
    pause()
end

%% Plot a selected fish trajectoris after manually removing bad trials

% fishInd = 15;
badTrls = [];

procData = OpenMatFile(pathList{ind_path});
fp = procData.fishPos;
nTrls = size(fp,3);
delTrls = procData.delTrls;

img = zeros(896,896);
figure
imagesc(img),axis image, box off, colormap([0 0 0]+0.2)
hold on
trlList = setdiff(1:nTrls,badTrls);
trlList = setdiff(trlList,delTrls);
% trlList = [1,3,5,10,21];
clrMap = hsv(numel(trlList));
count = 0;
for trl = trlList
    count = count + 1;
    fp_now = squeeze(fp(:,:,trl));
    if size(fp_now,1)>3
        fp_now = FitBSplineToCurve(fp_now,[],6);
    end
%     plot(fp_now(:,1),fp_now(:,2),'.-','color',rand(3,1))
    plot(fp_now(:,1),fp_now(:,2),'.-','color',clrMap(count,:))
    
end
title(['Fish # ' num2str(ind_path)])
set(gca,'tickdir','out','color','k')
axis off
xlim([1 896])
ylim([1 896])
axis image


%% Heat map
fp_good = fp(:,:,trlList);
fp_lin = permute(fp_good,[1 3 2]);
fp_lin = reshape(fp_lin,[size(fp_lin,1)*size(fp_lin,2),size(fp_lin,3)]);
trajHeatMap = Fishpos2HeatMap(fp_lin,size(img));
ker = MakeGaussian(20);
ker = ker(:)*ker(:)';
trajHeatMap = conv2(trajHeatMap,ker,'same');

figure
imagesc(Standardize(trajHeatMap)), axis image
set(gca,'clim',[0 75/size(fp,1)])
colormap(fake_parula)
axis off
title(['Fish # ' num2str(ind_path)])
shg


%% Trajectories starting from zero
figure
count = 0;
for trl = trlList
    count = count + 1;
    fp_now = fp(:,:,trl);
    if size(fp_now,1)>3
        fp_now = FitBSplineToCurve(fp_now,[],6);
    end
    plot(fp_now(:,1)-fp_now(1,1),fp_now(:,2)-fp_now(1,2),'.-','color',clrMap(count,:))
    hold on
end
title(['Fish # ' num2str(ind_path)])
x = fp(:,1,:); x = max(x(:));
y = fp(:,2,:); y = max(y(:));
patchline([-x,x],[0 0],'edgecolor','w','edgealpha',0.5,'linestyle','--');
patchline([0 0],[-y y],'edgecolor','w','edgealpha',0.5,'linestyle','--')
xlim([-x x])
ylim([-y y])
set(gca,'tickdir','out','color',[0 0 0]+0.2)

%% Velocity timeseries

nFigs = 8;
ipd = procData.interPxlDist;
dt = 1/procData.frameRate;
time = (0:size(fp,1)-2)*dt;
count = 0;
subCount = 0;
badTrls = [];
keepTrls = setdiff(trlList,badTrls);
vel = zeros(numel(keepTrls),size(fp,1)-1);
upperLim = 30;
ax = nan(nFigs,1);
subLists = SubListsFromList(keepTrls,nFigs);
outerCount = 0;
for jj = 1:length(subLists)
    figure
    ax = zeros(length(subLists),1);
    count = 0;    
    for trl = subLists{jj}'
        count = count + 1;
        outerCount = outerCount + 1;
        fp_now = squeeze(fp(:,:,trl));
        if size(fp_now,1)>3
            fp_now = FitBSplineToCurve(fp_now,[],4);
        end
        vel(outerCount,:) = deltaDistance(fp_now)*ipd/dt;      
        subaxis(nFigs,1,count,'SpacingVert',0)
          plot(time,vel(outerCount,:),'k')
        vel_max = max(vel(outerCount,:));
        if vel_max> upperLim
            ylim([-inf inf])
            yl = round((vel_max/upperLim)*10)/10;
            ylabel([num2str(yl) 'X'])
        else
            ylim([0 upperLim])
        end
        if trl == subLists{jj}(end)
            xlabel('Time (s)')
            ylabel('Vel (mm/s)')
            set(gca,'ytick',[0 upperLim])
        else
            set(gca,'xtick',[],'ytick',[])
        end
        if count ==1
            title({['Fish # ' num2str(ind_path)], ['Trls # ' num2str(subLists{jj}')]})
        end
    end
 box off
 set(gca,'tickdir','out')    
end
