function pathList = PathsFromGrpDataVar(grpData, varargin)
%PathsFromGrpDataVar - Given "grpData" structure variable created by some
%   of my matlab scripts, returns a cell array of all the paths to data 
% pathList = PathsFromGrpDataVar(grpData);
% pathList = PathsFromGrpDataVar(grpData,writeBool);
% Inputs:
% grpData - Structure variable created by some of my matlab scripts.
% writeBool - Boolean. If true, will write to an excel sheet in the current
%   director with the name 'PathList_temp' along with timestamp. Default is
%   false.

writeBool = false;
if nargin == 2
    writeBool = varargin{1};
end
pathList = {};
grps = fieldnames(grpData);
count = 0;
for grpNum = 1:length(grps)
    grp = grps{grpNum};
    foo_grp = grpData.(grp);
    trtmnts = fieldnames(foo_grp);
    for trtmntNum = 1:length(trtmnts)
        trtmnt = trtmnts{trtmntNum};
        foo_grp_trt = foo_grp.(trtmnt);
        stims = fieldnames(foo_grp_trt);
        for stimNum = 1:length(stims)
            stim = stims{stimNum};
            foo_grp_trt_stim = foo_grp_trt.(stim);
            for fishNum  = 1:length(foo_grp_trt_stim.procData)
                count = count + 1;              
                [pathList{count,1},~,~] = fileparts(foo_grp_trt_stim.procData{fishNum}.Properties.Source);
            end
            
        end
    end
end

if writeBool
    ts = datestr(now,30);
    fName = ['PathList_temp_' ts];
    cp = cd;
    xlswrite(fullfile(cp,fName),pathList);
    disp(['Saved at ' fullfile(cp,fName)]);
end

end

