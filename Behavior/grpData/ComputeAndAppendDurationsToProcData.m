%===== ComputeAndAppendDurationsToProcData.m
%   Script for automatically computing swim durations in fast frame rate
%   trials using wavelet decomposition and then attending these to
%   procData.mat files

%% Input parameters
kerSize = 10; % For smoothing WTs
freqRange = [15 60];
stimTime = 50; % Time of stim after start of trial in ms
thr_dur = 2;

%% Computing duration and appending to procData.mat
disp('Computing durations and appending to procData.mat files...')
% This is the threshold for marking the onset and offset of activity in
% wavelet-filtered trace
ker = gausswin(kerSize)*gausswin(kerSize)';
stimNames = fieldnames(data);
for stim = 1:length(stimNames)
    data_stim = data.(stimNames{stim});
    trtNames = fieldnames(data_stim);
    for trt = 1:length(trtNames)
        data_trt = data_stim.(trtNames{trt});
        data_fish = data_trt.procData;
        for fish = 1:length(data_fish)
            disp([upper(stimNames{stim}) ', ' upper(trtNames{trt}), ', '  'FISH # ' num2str(fish)])
            data_now = data_fish{fish};
            if  isempty(find(strcmpi(fieldnames(data_now),'curv')))                
                curv = GetTailTangents(data_now.tailCurv);
                curv_trl = SubListsFromList(curv',data_now.nFramesInTrl);
                data_now.curv = curv;
            else
                 curv_trl = SubListsFromList(data_now.curv',data_now.nFramesInTrl);               
            end
            esi = data_now.elicitedSwimInfo;
            esi.dur = cell(size(esi.onset));
            for trl = 1:length(curv_trl)
                onset = esi.onset{trl};
                if ~isempty(onset)
                    disp(['Trl # ' num2str(trl)])
                    y = curv_trl{trl}(:,end);
                    t = (0:length(y)-1)/data_now.fps;
                    % Get a time window that starts 50 sec before onset of
                    % activity
                    startTime = max((stimTime + onset(1)-40)/1000,stimTime/1000);
                    inds_t = t>= startTime;
%                     inds_t = t>=(stimTime/1000);
                    y = y(inds_t);
                    t = t(inds_t);
                    t = t(1:2:end);                   
                    y =  FitBSplineToCurve(y(1:2:end),[],4);                  
                    w =  ComputeXWT(y(:),y(:),t(:),'freqRange',freqRange,'dj',1/64);
                    pk_mat = (diff(w(1:end-1,:))>0) & (diff(w(2:end,:))<=0);
                    pk_mat = conv2(cat(1,pk_mat,w(end-1:end,:)*0).*w,ker,'same');
                    y_dur = sum(pk_mat,1)/(max(freqRange)-min(freqRange));
                    levelInds = LevelCrossings(y_dur,thr_dur);
                    if ~isempty(levelInds)
                        if isempty(levelInds{1})
                            % When activity is elicited during ongoing swimming
                            levelInds{1} = 1;
                        end
                        if isempty(levelInds{2})
                            % When activity continues for the whole recording
                            % period
                            levelInds{2} = length(t);
                        end
                        if levelInds{2}(1) < levelInds{1}(1)
                            % When upward level crossing is missed because
                            % activity is high to begin with and downard
                            % level crossing has a value
                            levelInds{1}(1) =1;
                        end
                        onInd = levelInds{1}(1);
                        offInd = find(levelInds{2}>onInd);
                        if isempty(offInd)
                            offInd= length(t);
                        else
                            offInd = levelInds{2}(offInd(1));
                        end                        
                        dur= round(((t(offInd)-t(onInd))*1000)/2)*2;
                        if dur<0
                            a = 1;
                        end
                    else
                        dur = nan;
                    end
                    esi.dur{trl} = ones(size(esi.onset{trl}))*dur;
                end
            end            
            data_now.elicitedSwimInfo = esi;
        end
    end
end





