function varargout = WaveDataFromGrpData(grpData, varargin)
%WaveDataFromGrpData From 'grpData.mat' created by another set of scripts,
%   reads the wavelet coefficient matrices for all ablation groups and
%   concatenates them into 3D arrays of size [F, T, N], where F is the # of
%   frequency scales, T is the # of time points and N is the (number of
%   experiments * number of trials).
% W = WaveDataFromGrpData(grpData);
% [W,H,M] = WaveDataFromGrpData(grpData,'grps',grps,'delGrps',delGrps,'stimTypes', stimTypes);
% Inputs:
% grpData - Group data structure variable created by other scripts, with
%   the fields being set to the names of the ablated groups plus some
%   additional group data.
% Optional inputs:
% grps - Cell array of group names. Will restrict unrolling to these names
% delGrps - Cell array of group names to exclude from unrolling.
% stimTypes - Cell array of stimulus types to consider. Defuault of 'auto'
%   stimTypes = {'vib','dark'};
% Outputs:
% W - Structure data which contains the wavelet coefficient matrices
%   arranged in 3D arrays as desribed above.
% H - Structure data which contains the HHT coefficient matrices arranged
%   in 3D arrays as described above
% M - Structure data which contains max-min envelope data (see
%   GetFishWaves_fish) arranged as an array
% Avinash Pujala, JRC/HHMI, 2017

grps = 'auto';
delGrps = [];
stimTypes = 'auto';

for jj = 1:numel(varargin)
    if isstr(varargin{jj})
        switch lower(varargin{jj})
            case 'grps'
                grps = varargin{jj+1};
            case lower('delGrps')
                delGrps = varargin{jj+1};
            case lower('stimTypes')
                stimTypes = varargin{jj+1};
        end
    end
end

if strcmpi(grps,'auto')
    grps = fieldnames(grpData);
end

delInds = [];
if ~isempty(delGrps)
    for jj = 1:length(delGrps)
        delInds  = union(delInds,find(strcmpi(grps,delGrps)));
    end
    grps(delInds)=[];
end

if all(strcmpi(stimTypes, 'auto'))
    stimTypes = {'vib', 'dark'};
end

trtmnts = {'ctrl','abl'};

%% Creating data struct
[W,H,M,T] = deal(struct);
count = 0;
for grp = 1:length(grps)
    for trtmnt = 1:length(trtmnts)
        for stim = 1:length(stimTypes)
            blah = grpData.(grps{grp}).(trtmnts{trtmnt}).(stimTypes{stim}).procData;
            for fish = 1:length(blah)
                disp([upper(grps{grp}) ', ' trtmnts{trtmnt} ', ' stimTypes{stim} ', fish # ' num2str(fish)])
                foo_fish = blah{fish}.W;
                H_fish = blah{fish}.H;                
                if ~isempty(foo_fish)
                    nTrls = length(foo_fish.curv.coeff);
                    for trl = 1:nTrls
                        count = count + 1;
                        %                         W.(trtmnts{trtmnt}).(stimTypes{stim}).(grps{grp}){count} = foo_fish.curv.coeff{trl};
                        W.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}){count} = foo_fish.curv.coeff{trl};
                        H.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}){count} = H_fish.curv.coeff{trl};
                        M.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}).maxMinusMin{count} = H_fish.curv.ts_maxMinusMin{trl};
                        M.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}).maxPlusMin{count} = H_fish.curv.ts_maxPlusMin{trl};
                        T.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}){count} = foo_fish.curv.ts{trl};
                    end
                else
                    error('No elicited swim info')
                end
            end
            count = 0;
            shape =  [size(W.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}){1}),...
                length(W.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}))];
            W.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}) = ...
                reshape(cell2mat(W.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt})),shape);
            
            shape =  [size(H.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}){1}),...
                length(H.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}))];
            H.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}) = ...
                reshape(cell2mat(H.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt})),shape);
                        
            foo = M.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}).maxMinusMin;
            foo(EmptyInds(foo))=[];
            shape = [size(foo{1}),length(foo)];            
            M.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}).maxMinusMin = ...
                squeeze(reshape(cell2mat(foo),shape));
            
            foo = M.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}).maxPlusMin;
            foo(EmptyInds(foo))=[];
            shape = [size(foo{1}),length(foo)];
            M.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}).maxPlusMin = ...
                squeeze(reshape(cell2mat(foo),shape));
            
            foo = T.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt});
            foo(EmptyInds(foo))=[];
            shape = [size(foo{1}),length(foo)];
            T.(grps{grp}).(stimTypes{stim}).(trtmnts{trtmnt}) = ...
                squeeze(reshape(cell2mat(foo),shape));
        end
    end
end

varargout{1} = W;
varargout{2} = H;
varargout{3} = M;
varargout{4} = T;

end

function eInds = EmptyInds(cellArray)
% Get empty cell indices
% eInds = EmptyInds(cellArray)
eInds = zeros(size(cellArray));
for jj = 1:length(cellArray)
    if isempty(cellArray{jj})
        eInds(jj) = 1;
    end
end
eInds = find(eInds);
end

