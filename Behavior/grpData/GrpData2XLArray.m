
%=== Takes grpData structure and unrolls into format suitable for excel sheet
%    Example format for grpData structure
%    grpData=
%       mHom:       [1x1 struct]
%       inter:      [1x1 struct]
%       vent:       [1x1 struct]
%       alx_young:  [1x1 struct]
%       ...


%% Inputs

saveDir = 'S:\Avinash\Notes\Apr 2018';
grps = fieldnames(grpData);
stimTypes = {'vib', 'dark'};
trtmnts = {'ctrl','abl'};
% flds = {'bendAmp','bendPer','onset'};
flds = {'bendAmp','bendPer','onset','dur'};
% grps = 'auto'; % {'mHom','inter','vent','alx'};
delGrps = {'data_flat','data_xls'};

nom = 'GrpData-';
for grp = 1:length(grps)
    nom = [nom grps{grp} '-'];
end


for stim = 1:length(stimTypes)
    nom = [nom '-' stimTypes(stim) '-' datestr(now)];
end

%% Creating data struct
tic
% If "grpData" structure variable has the field "data_flat", make sure to
%   remove it before running this script
[data_flat,data_array] = FlattenGrpDataStruct(grpData,'saveDir',saveDir,'responses',flds,'delGrps',delGrps);
toc

% --- Append to grpData for faster retrieval and save
grpData.data_xls = data_array;
disp('Saving group ablation data...')
ts = datestr(now,30);
fName = ['Ablation data_alx_youngAndOld_' ts];
save(fullfile(saveDir,fName),'grpData','-v7.3')
disp('Done!')

return

%% Creating cell array and writing to xl sheet
% 
% disp('Writing to xl sheet...')
% fldNames = fieldnames(data_xls);
% data_array = cell(numel(data_xls.bendAmp)+1,length(fieldnames(data_xls)));
% for fld = 1:length(fldNames)
%     fldName = fldNames{fld};
%     fldName(1) = upper(fldName(1));
%     data_array{1,fld} = fldName;
%     vars = data_xls.(fldNames{fld});
%     if iscell(vars(1))
%         data_array(2:end,fld) = data_xls.(fldNames{fld});
%     else
%         data_array(2:end,fld) = num2cell(data_xls.(fldNames{fld}));
%     end    
% end
% 
% fName = ['Peak Info for Group Data_' ts];
% xlswrite(fullfile(saveDir,fName),data_array)
% disp(['Written at ' saveDir])


