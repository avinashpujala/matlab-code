%BOOTSTRAPHHTSIGNIF
% Unpacks, wavelet data, and hht data from grpData variable loaded into the MATLAB
%   workspace, and then when specified the following inputs...
%   grp = 'mHom', 'vent', or 'inter'
%   stim = 'vib' or 'dark'
%   nIter = 1000; (default = 1000);
%   pow = 1; Exponent on H
%   ....


%% Get wave data from grpData
% [~,H,M] = WaveDataFromGrpData(grpData,'delGrps','data_xls');


%% Inputs
grp = 'inter';
stim = 'dark';
nIter = 1000;
pow = 1;

%% Bootstrapping
abl = (H.(grp).(stim).abl).^pow;
abl_shape = size(abl);

ctrl = (H.(grp).(stim).ctrl).^pow;
ctrl_shape  = size(ctrl);
% ctrl = ctrl.*(repmat(coiMat_mod,[1,1,ctrl_shape(3)]));

thr =[];
thr(1) = multithresh(ctrl(:));
thr(2) = multithresh(abl(:));
thr = min(thr);

ctrl(ctrl < thr) = 0;
ctrl(ctrl >=thr) = 1;

abl(abl < thr) = 0;
abl(abl >=thr) = 1;

abl_vec = transpose(reshape(abl,...
    [abl_shape(1)*abl_shape(2),abl_shape(3)]));

ctrl_vec = transpose(reshape(ctrl,...
    [ctrl_shape(1)*ctrl_shape(2),ctrl_shape(3)]));

% Bootstrapping control (~15-20 mins in serial, ~ 3 mins in parallel)
tic
disp('Bootstrapping control...')
ctrl_boot = BootStrapStat(ctrl_vec,'nIter',nIter,'processing','parallel');
toc

% Bootstrapping ablated (~15-20 mins in serial, ~ 3 mins in parallel)
tic
disp('Bootstrapping ablated...')
abl_boot = BootStrapStat(abl_vec,'nIter',nIter, 'processing','parallel');
toc

%% Difference of bootstrapped means

abl_boot_mat = reshape(abl_boot',[abl_shape(1:2),nIter]);
ctrl_boot_mat = reshape(ctrl_boot',[ctrl_shape(1:2),nIter]);

% --- Smooth a bit
ker = gausswin(15)*gausswin(7)';
ker = ker/sum(ker);
for jj = 1:size(ctrl_boot_mat,3)
    ctrl_boot_mat(:,:,jj) = conv2(ctrl_boot_mat(:,:,jj),ker,'same');
    abl_boot_mat(:,:,jj) = conv2(abl_boot_mat(:,:,jj),ker,'same');
end

diff_boot_mat = (ctrl_boot_mat - abl_boot_mat).^pow;
shape = size(diff_boot_mat);
diff_boot = permute(reshape(diff_boot_mat,[shape(1)*shape(2),nIter]),[2,1]);

% diff_boot_mat_coi = diff_boot_mat.*repmat(1-coiBool,[1,1,nIter]);
% diff_shape = size(diff_boot_mat);
% diff_boot = transpose(reshape(diff_boot_mat_coi,[diff_shape(1)*diff_shape(2),diff_shape(3)]));

%% P values
% tic
% disp('Computing p-values, serially...')
% pVals = PValue(diff_boot,0,'valRange','independent');
% toc

tic
disp('Computing p-values, parallelly...')
[pVals, zScores] = PValue(diff_boot,0,'valRange','independent','processing','parallel');
toc

%% Plotting
zThr = 3.25;
cMap = viridis(256);

alphaVals = (1-pVals)*100;
alphaVals_mat = reshape(pVals,ctrl_shape(1:2));
zScores_mat = reshape(zScores,ctrl_shape(1:2));
% zScores_mat_coi = zScores_mat.*(1-coiBool);

% figure
% imagesc(alphaVals_mat), colorbar
foo = grpData.(grp).ctrl.(stim).procData{1}.H;
freq = foo.freq;
time = foo.time;


figure('Name','Average Control')
% imagesc(mean(diff_boot_mat,3)), colorbar
imagesc(time,freq,mean(ctrl_boot_mat,3)),colorbar
cl = get(gca,'clim');
set(gca,'ydir','normal','clim',cl)
hold on
% contour(time, freq, zScores_mat,[-zThr,zThr],'w--')
xlabel('Time (s)')
ylabel('Freq (Hz)')
title('Ctrl')
colormap(cMap)

figure('Name','Average Ablated')
imagesc(time, freq,mean(abl_boot_mat,3)),colorbar
set(gca,'ydir','normal','clim',cl)
hold on
% contour(time, freq, zScores_mat,[-zThr,zThr],'w--')
xlabel('Time (s)')
ylabel('Freq (Hz)')
title('Ablated')
colormap(cMap)

% figure('Name','Zscores mat')
% imagesc(time,freq,zScores_mat),colorbar
% set(gca,'ydir','normal','clim',[-3*zThr, 3*zThr])
% hold on
% contour(time, freq, zScores_mat,[-zThr,zThr],'w--')
% xlabel('Time (s)')
% ylabel('Freq (Hz)')
% title('Z-scores')
% colormap(cMap)

figure('Name','Average of Control - Ablated')
imagesc(time,freq,mean(diff_boot_mat,3)),colorbar
set(gca,'ydir','normal','clim',[-0.3, 0.3])
hold on
contour(time, freq, zScores_mat,[-zThr,zThr],'w--')
xlabel('Time (s)')
ylabel('Freq (Hz)')
title('Ctrl-Abl')
colormap(cMap)

break;

%% Saving data

saveDir = 'S:\Avinash\Ablations and behavior\GrpData\Intermediate RS\Bootstrap signif';

ts = datestr(now,30);
fName = ['bootData_' stim '_' ts '.mat'];

bootData = struct;
bootData.ctrl = ctrl_boot_mat;
bootData.abl = abl_boot_mat;
bootData.alphaVals = alphaVals_mat;
bootData.zScores = zScores_mat;

save(fullfile(saveDir,fName),'bootData');




