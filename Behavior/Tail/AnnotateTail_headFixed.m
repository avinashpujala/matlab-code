
%% Inputs
imgDirs = {'\\Koyama-S1\Data2\Rig1\Data\Minoru\2017-01-13\behav\f1s3';...
    '\\Koyama-S1\Data2\Rig1\Data\Minoru\2017-01-13\behav\f1s2'};
nImgsToAnnotate = 100;
imgExt = 'bmp';
pxlLen = 14e-3; % MK told me that pxlsize = 14 microns. Converting to mm.
eyeDiam = 0.8; % In mm

%%
disp('Reading filenames...')
tic
nImgsToAnnotate_sub = floor(nImagesToAnnotate/length(imgDirs));
imgNames = {};
for jj = 1:length(imgDirs)
    imgNames_now = GetFilenames(imgDirs{jj},'ext',imgExt);
    disp([ num2str(length(imgNames_now)) ' images found'])
    rp = randperm(length(imgNames_now));
    rp = rp(1:nImgsToAnnotate_sub);
    imgNames_now = imgNames_now(rp);
    for kk = 1:length(imgNames_now)
        imgNames_now{kk} = fullfile(imgDirs{jj},imgNames_now{kk});
    end
    imgNames = [imgNames, imgNames_now];
end
toc
% nImgs = length(imgNames);
%%
img_info = imfinfo(imgNames{1});
imgDims = [img_info.Height, img_info.Width];
I_rand = zeros(imgDims(1),imgDims(2),length(imgNames));
imgInds = 1:length(imgNames);
parfor jj = imgInds
    I_rand(:,:,jj) = imread(imgNames{jj});
end



%% Draw ROIs around eyes to annotate
coords = cell(size(I_rand,3),1);
for jj = 1:nImagesToAnnotate
    roi = SetFreehandRois(I_rand(:,:,jj));
    foo = [];
    for rr = 1:length(roi)
        foo = [foo; [roi{rr}.xi, roi{rr}.yi]];
    end
    coords{jj} = [foo(:,1),foo(:,2)];
end

%% Check to see how they worked
figure;
for jj = 1:nImagesToAnnotate
    cla
    imagesc(I_rand(:,:,jj)),axis image
    colormap(gray)
    hold on
    plot(coords{jj}(:,2), coords{jj}(:,1))
    pause()
end

%% Save stuff
disp('Saving data')
tic
rootDir = fileparts(imgDirs{1});
outDir = fullfile(rootDir,'proc');
if exist(outDir, 'dir')~=7
    mkdir(outDir)
end
crop_howTo = 'I(cropInds(1):cropInds(2),cropInds(3):cropInds(4),:)';
% imgInds_rand = rp;
nImgs_annot = nImagesToAnnotate;
% imgNames_annot = imgNames(imgInds_rand(nImagesToAnnotate));
imgNames_annot = imgNames;
I_annot = I_rand;
save(fullfile(outDir,'tailAnnotInfo.mat'),'imgNames_annot',...
    'nImgs_annot','cropInds','crop_howTo','coords','I_annot');
toc

%% Save images used for annotation


