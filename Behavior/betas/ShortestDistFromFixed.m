function varargout = ShortestDistFromFixed(movingCoords,fixedCoords)
%ShortestDistFromObj Given the coordinates of a moving object and those of
%   a fixed object, returns the shortest distance between each position of
%   the moving object and the fixed object
% d = ShortestDistFromObj(movingCoords,fixedCoords);
% [d,nearestInds] = d = ShortestDistFromObj(movingCoords,fixedCoords);
% Inputs:
% movingCoords - N x 2 matrix specifying the coordinates of the moving
%   object in 2D. N(:,1) = x coordinates, N(:,2) = y coordinates.
% fixedCoords - M X 2 matrix specifying the 2D coordinates of a fixed object
% Outputs:
% d = N X 1 vector specifying the shortest distance between each position
%   of the moving object and the fixed object
% nearestInds = N X 1 vector specifying the indices of fixed object that ar
%   closest to each position in the moving object
% 
nWorkers = 10;

p = gcp('nocreate');
if isempty(p)
    parpool(nWorkers);
end

D = @(pt,pts)sqrt(sum((repmat(pt,size(pts,1),1)-pts).^2,2));

N = size(movingCoords,1);
iterable = 1:N;
d = zeros(N,1);
nearestInds = d;
for jj = iterable
    [d(jj),nearestInds(jj)] = min(D(movingCoords(jj,:),fixedCoords));
end

varargout{1} = d;
varargout{2} = nearestInds;

end

