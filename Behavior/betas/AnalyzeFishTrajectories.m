
%% Inputs
xlsDir           = 'S:\Avinash\Notes\Apr 2018';
xlsName          = 'AblationDataPaths_alxOnly.xlsx';
sheetNum         = 4; %(sheetNum = 1 for fast swims, and sheetNum = 2 for slow swims)
arenaDiam        = 50; % In mm


%% Read paths from excel sheet

[num,txt,raw] = xlsread(fullfile(xlsDir,xlsName),sheetNum);

grpCol = find(strcmpi(raw(1,:),'AblationType'));
trtmntCol = find(strcmpi(raw(1,:),'AblationBool'));
trackedCol = find(strcmpi(raw(1,:),'Tracked'));
pathCol = find(strcmpi(raw(1,:),'path'));
rowInds = find([raw{2:end,trackedCol}]==1)+1;
pathList = raw(rowInds,pathCol);
raw_vals = raw(rowInds,:);

%% Read all the procData.mat files
disp('Reading all procData files from paths...')
tic
procData = cell(size(pathList));
for jj = 1:length(pathList)
    disp(['Path # ' num2str(jj)])
    [path,file,~] = fileparts(pathList{jj});
    if ~strcmpi(file,'proc')
        pathList{jj} = fullfile(pathList{jj},'proc');
    end
    procData{jj} = OpenMatFile(pathList{jj});
end
toc

%%
for jj = 1:length(procData)
    pd = procData{jj};
    ref = pd.ref;
    % imagesc(ref),axis image, colormap(gray)
    
    [ipd,arenaEdgePts] = GetPxlSpacing(mean(ref,3),'diam',arenaDiam,'plotBool',false,'nIter',6e3,'tol',0.05);
        
    fp_sub = pd.fishPos;
    t = (0:size(fp_sub,1)-1)*(1/pd.fps);
    tt = linspace(t(1),t(end),round(length(t)*300/pd.fps));
    fp = zeros(length(tt),2);
    fp(:,1) = interp1(t(:),fp_sub(:,1),tt(:),'spline');
    fp(:,2) = interp1(t(:),fp_sub(:,2),tt(:),'spline');
    
    disp('Computing distance...')
    [d,nearestInds] = ShortestDistFromFixed(fp,arenaEdgePts);
    d_mm = d*ipd;
    d_norm = 2*d_mm/arenaDiam;
    [p,~] = hist(d_norm,25);
    p = p/sum(p);
    pd.Properties.Writable = true;
    pd.distFromEdge = d_norm;
    
    disp('Plotting...')
    if raw_vals{jj,ablationBoolCol} == 1
        foo = 'abl';
    else
        foo = 'ctrl';
    end
    figure('Name',[raw_vals{jj,grpCol} ', ' foo ',' num2str(jj)])
    subaxis(2,1,1,'SpacingVert',0)
    plot(arenaEdgePts(:,1),arenaEdgePts(:,2),'k--', 'linewidth', 2), axis image
    hold on
    scatter(fp(:,1),fp(:,2),2,d_norm);
    axis off
    subplot(2,1,2)
%     time = (0:length(d_norm)-1)*1/300;
    plot(tt,d_norm)
    xlim([-inf inf])
    ylim([0, 1])
    box off
    xlabel('Time (s)')
    ylabel('Dist')
    shg
end

%%

