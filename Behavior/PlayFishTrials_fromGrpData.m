
%% Inputs
ablationGroup = 'inter'; %('mHom','inter','vent')
trtmnt = 'ctrl'; %('ctrl','abl')
stim = 'vib'; %('vib','dark')
fishNum = 1;
trlList = [2 6 7 11 14 15 16 17];
% trlList = [6 9];
plotCurv = 1;
writeVideo = 0;
pauseDur = 0;
frameIndsInTrl = 40:300;
originInd = 50; % Stim index

saveDir  = 'S:\Avinash\Ablations and behavior\GrpData\20170424_Ahrens-Koyama lab meeting';



%% Read info from procData
procData = grpData.(ablationGroup).(trtmnt).(stim).procData{fishNum};

%% Plot tail curvatures atop moving fish

% PlayFishTrials(procData,trlList,'frameInds',frameIndsInTrl,'pauseDir',pauseDur,'bendAngleDispMode','total',...
%     'writeVideo',writeVideo,'saveDir',saveDir,'overlayMidline',false,'clrMap',flipud(bone),'dispTraj',true);

w = procData.W;
ts = w.curv.ts;
time = w.time;
keptList = w.trlList;

%% One trajectory at a time
% for trl = trlList
% traj = FishTrajectories(procData,'trlList',trl,'frameIndsInTrl',frameIndsInTrl,'originInd',originInd,'adjustment','both',...
%     'plotBool',true,'clrMode','speed');
% % figure
% % plot(ts{trl})
% % box off
% % title(num2str(keptList(trl)))
% end


%% Many trajectories at once
traj = FishTrajectories(procData,'trlList',trlList,'frameIndsInTrl',frameIndsInTrl,'originInd',originInd,'adjustment','both',...
    'plotBool',true,'clrMode','speed');

