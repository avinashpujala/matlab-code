

img_dir='Z:\Avinash\08-30-2014\f1s9_20140830_232020\Registered';

img_size=double(read_LSstack_size(fullfile(img_dir,'Stack dimensions.log')));
ave_stack=readtiff(fullfile(img_dir,'ave.tif'));
circle_matrix=makeDisk2(5,11);

[r c]=find(circle_matrix);
r=r-6;c=c-6;
inds=img_size(1)*c+r;




stack=read_LSstack_fast1(fullfile(img_dir,'Plane01.stack'),img_size(1:2));
totlen=size(stack,3);
figure(1);
imagesc(ave_stack(:,:,1));
%%
button=0;
while button~=3
    figure(1);
    [x y button]=ginput(1);
    
    xinds=inds;
   
    cinds=(round(x)-1)*img_size(1)+round(y)+xinds;
    cinds=cinds(cinds>0 & cinds< (prod(img_size(1:2))));
    
    sliceinds=int64(0:(totlen-1))*int64(prod(img_size(1:2)));
    
    
    tcourse=single(get_cell_tcourse_mex64(stack,sliceinds,int64(cinds)));
    
    disp(x);
    disp(y);
    figure(2);
    plot(tcourse-rand*100);
    
end

