%clear all;close all;

% 
% input_dir='Z:\SPIM\Avinash\11-1-2014\Fish2\G50_T1_Sl1_1p5_2_20141101_211536';


%skiplist=[1:3]; %% skip first 1-5 trial because of the bleaching
tlen=120; %% frame cycle of the trial
timelist=[2 50 68]; %% set epochs for making average
% time_explain = [1highgain 2pause 3backprobe 4pause 5lowgain 6pause 7backprobe 8pause 
%                 9highgain 10pause 11forprobe 12pause 13lowgain 14pause 15forprobe 16pause];


% ave_stack  = double(readtiff(fullfile(input_dir,'\Registered\ave.tif')));  %% average anatomy stack of SPIM
try
    ave_stack = stack.avg;
catch    
ave_stack  = double(readtiff(fullfile(input_dir,'\ave.tif')));  %% average anatomy stack of SPIM
end

% load(fullfile(input_dir,'\Registered\cell_resp_dim_processed.mat'));
% load(fullfile(input_dir,'\Registered\cell_info_processed.mat'));
% cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);

load(fullfile(input_dir,'\cell_info_processed.mat'));
load(fullfile(input_dir,'\cell_resp_dim_processed.mat'));
%cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);
% load('cell_resp3.mat');
cell_resp=cell_resp3;

skiplist=[1]; %% you can omit specific trials from average

numcell=length(cell_info);
dim=size(ave_stack);
totlen=cell_resp_dim(2);
nrep=floor(totlen/tlen);

%% extracting active neurons in "timelist'

cell_resp_period_mean=zeros(length(cell_info),length(timelist));
timelist2=cumsum(timelist);
timelist2(2:end+1)=timelist2(1:end);
timelist2(1)=0;
% 
% for i=1:length(timelist)
%     cell_resp_period_mean(:,i)=mean(cell_resp_ave(:,timelist2(i)+1:timelist2(i+1)),2);
% end

%%

cells1=find(cell_resp_period_mean(:,2) - cell_resp_period_mean(:,1) > 0.1);  %% red cells
cells2=find(cell_resp_period_mean(:,1)- cell_resp_period_mean(:,2) > 0.1);  %% greeen cells
%  cells1 = responsiveCells_sub;

% cells1  = responsiveCells;
 cells1 = consideredCells;
% cells1 = testCellInds;


anatomy_image_yx = repmat(imNormalize99(max(ave_stack,[],3)),[1 1 3]); % Normalized max-intensity projected image in 3 layers
anatomy_image_yz = repmat(imNormalize99(squeeze(max(ave_stack,[],2))),[1 1 3]);
anatomy_image_yx_ori = anatomy_image_yx;
anatomy_image_yz_ori = anatomy_image_yz;
dimv_yx = size(anatomy_image_yx);
dimv_yz = size(anatomy_image_yz);


anatomy_image_yz2=zeros(dimv_yz(1),dimv_yz(2)*10,3);
anatomy_image_yz2_ori=anatomy_image_yz2;
tot_image=zeros(dimv_yx(1),dimv_yx(2)+dimv_yz(2)*10+10,3);
tot_image(:,dimv_yx(2)+(1:10),:)=1;

circle=makeDisk2(3,15);
[r, v]=find(circle);
r=r-8;v=v-8;
circle_inds  = r*dimv_yx(1)+v;
% yzplane_inds = -5:5;
yzplane_inds = -1:1;

%% Uncomment this cell if same color is desired for al cells
%  colorValues = repmat([1 0 0],length(cells1),1);

%%
disp(2);
for j=1:length(cells1)    
    cinds=(cell_info(cells1(j)).center(2)-1)*dimv_yx(1)+cell_info(cells1(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
%     lut = cmap(decayTimeColorInds(j),:);  
    lut = colorValues(j,:);
    anatomy_image_yx(cinds+circle_inds(labelinds))=lut(1);
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=lut(2);
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=lut(3);
    
%     anatomy_image_yx(cinds+circle_inds(labelinds))=1;
%     anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=0;
%     anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0;

    cinds=(cell_info(cells1(j)).slice-1)*dimv_yz(1)+cell_info(cells1(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=lut(1);
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=lut(2);
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=lut(3);
%     
%     anatomy_image_yz(cinds+yzplane_inds(labelinds))=1;
%     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=0;
%     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0;
end

%%%%%%% I won't display these cells for the time being %%%%%%%%%
% for j=1:length(cells2)    
%     cinds=(cell_info(cells2(j)).center(2)-1)*dimv_yx(1)+cell_info(cells2(j)).center(1);
%     labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
%     anatomy_image_yx(cinds+circle_inds(labelinds))=0;
%     anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=1;
%     anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.75;    
% 
%     cinds=(cell_info(cells2(j)).slice-1)*dimv_yz(1)+cell_info(cells2(j)).center(1);
%     labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
%     anatomy_image_yz(cinds+yzplane_inds(labelinds))=0;
%     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=1;
%     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.75;
% end



for k=1:3
    anatomy_image_yz2(:,:,1)=imresize(anatomy_image_yz(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,2)=imresize(anatomy_image_yz(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,3)=imresize(anatomy_image_yz(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,1)=imresize(anatomy_image_yz_ori(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,2)=imresize(anatomy_image_yz_ori(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,3)=imresize(anatomy_image_yz_ori(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
end


tot_image(:,1:dimv_yx(2),:)=anatomy_image_yx;
tot_image(:,dimv_yx(2)+11:end,:)=anatomy_image_yz2;


tot_image(tot_image(:)>1)=1;
tot_image(tot_image(:)<0)=0;
map = figure('position',[50 50 1000 1000]);
image(tot_image);colormap gray;
hold on
cb = colorbar;
 colormap(cmap_new);
%  cTick = get(cb,'YTick');
%  cTickLabel = get(cb,'YTickLabel'); 
tickGap = round(length(LUT)/10);
cTick = 1:tickGap:length(LUT);
cTickLabel = round(LUT(1:tickGap:end,1));
cTickLabel = num2str(cTickLabel(:));
set(cb,'YTick',cTick,'YTickLabel', cTickLabel)

return;



%%

cellinds_ex=define_region_roi_new( cell_info, cells1, ave_stack);

%%
figure(2);
for j=1:length(cellinds_ex);
    cnum=j;
     subplot(10,10,j);plot(cell_resp_ave(cellinds_ex(j),:),'r','linewidth',1);
    title(num2str(cnum));
    ylim([130 200]);
    xlim([0 sum(timelist)]);
    line([5 5],[0.95 1.2]) 
   % line([104 104],[0.95 1.3]) 
end

%%
hb_cellinds=cellinds_ex;
    
save(fullfile(input_dir,'\Registered\hb_cellinds.mat'),'hb_cellinds');

%%
dim_x = size(cell_resp,2)/400;
IM = reshape(cell_resp(extracted_cellinds(14),:), 400 ,dim_x)';
figure('position',[50 50 1000 800]);
imagesc(IM);
title('cell-14')






