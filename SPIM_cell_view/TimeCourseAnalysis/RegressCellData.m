%% Estimating Ca Impulse Response Function from M-cell activity
mCellIdx = data.cells.mCellIdx;
tData = data.tData;
trialSegData = data.trialSegData;
stack = data.stack;
timeTrl = completeSegInds(1);
data.samplingRate = 6000;
timePts = [-1 9];
tData.strngSwmTrls(tData.strngSwmTrls==data.incompleteSegInds) =[];
mCellStrngSwmDFF = zeros(numel(tData.strngSwmTrls),length(trialSegData(1).cells.time));
for trl = 1:numel(tData.strngSwmTrls)
      mCellStrngSwmDFF(trl,:) = trialSegData(tData.strngSwmTrls(trl)).cells.dFF(mCellIdx,:);    
end
data.cells.CIRF = mean(mCellStrngSwmDFF,1);
inds = find(trialSegData(1).cells.time >=timePts(1) & trialSegData(1).cells.time <= timePts(2));
data.cells.CIRF = data.cells.CIRF(inds);
data.cells.CIRF = data.cells.CIRF-data.cells.CIRF(1);
t = trialSegData(1).cells.time(inds);
% fpt = find(data.cells.CIRF == max(data.cells.CIRF));
% lpt = find(data.cells.CIRF(fpt:end)<=0,1,'first') + fpt -1;
% data.cells.CIRF = data.cells.CIRF(fpt:lpt);
data.cells.CIRF(end)= 0;
data.cells.CIRF = data.cells.CIRF/sum(data.cells.CIRF);

% oldLen = length(data.cells.CIRF);
% tt = linspace(t(1),t(end),oldLen*10e3);
% data.cells.CIRF = interp1(t,data.cells.CIRF,tt,'cubic');
% newLen = length(data.cells.CIRF);
% 
% g = fittype('A*exp(-x/tau)');
% t = (1:length(data.cells.CIRF))*stack.interval;
% f = fit(t(:),data.cells.CIRF(:),g);
% data.cells.CIRF = f.A*exp(-t/f.tau);
% data.cells.CIRF = data.cells.CIRF-min(data.cells.CIRF);
% data.cells.CIRF = data.cells.CIRF/sum(data.cells.CIRF);

%#### Make some regressors ##########
%% Stimulus regressor
stimReg = zeros(size(data.cells.dFF,2),1);
shockStackInds = MapIndsToDifferentlySampledTimeseries(data.stim.inds,stack.inds.start);
stimAmps = data.stim.amps(2,:);
stimReg(shockStackInds) = stimAmps;
origLen = length(stimReg);
stimReg = conv(stimReg(:),data.cells.CIRF(:));
stimReg = stimReg(1:origLen);

%% Motor regressors
nTimePts = size(data.cells.dFF,2);
motTemplate = zeros(numel(stackInds),1);
motTemplate = motTemplate(1:nTimePts);
tData = data.tData;
strngSwmInds = shockStackInds(tData.strngSwmTrls);
strngSwmPows  = tData.swimPow(tData.strngSwmTrls);
strngSwmDurs = tData.swimDur(tData.strngSwmTrls);

motReg.strngPow = motTemplate;
motReg.strngPow(strngSwmInds) = strngSwmPows;

motReg.strngPow = conv(motReg.strngPow(:),data.cells.CIRF(:));
motReg.strngPow = motReg.strngPow(1:nTimePts);
motReg.strngPow = motReg.strngPow/max(motReg.strngPow);

motReg.wkPow = data.smooth.swim(stack.inds.start(1:nTimePts));
motReg.wkPow = conv(motReg.wkPow(:),data.cells.CIRF(:));
motReg.wkPow = motReg.wkPow(1:nTimePts);
motReg.wkPow = motReg.wkPow/max(motReg.wkPow);
motReg.wkPow = motReg.wkPow - motReg.strngPow;
motReg.wkPow(motReg.wkPow < 0) = 0;


X = [ones(length(stimReg),1) stimReg(:) motReg.strngPow(:) motReg.wkPow(:)];
% Y = data.cells.dFF';

sigCell = std(cellMat,[],2);
activeCells = find(sigCell>=mean(sigCell));
ativeCells = union([data.cells.responsiveCellInds(:); data.cells.mCellIdx],activeCells);

Y = data.cells.dFF(activeCells,:)';
%% Regressing
% disp('Regressing...')
% tic
% B = Y\X;
% toc


%% After obtaining regression data and storing it in regData, represent the cells by colors in brain maps
disp('Mapping colored beta values onto the brain...')
tic
B= regData.B;
redVals = (B(:,2)-min(B(:,2)))/(max(B(:,2))-min(B(:,2)));
grnVals = (B(:,3)-min(B(:,3)))/(max(B(:,3))-min(B(:,3)));
blueVals =(B(:,4)-min(B(:,4)))/(max(B(:,4))-min(B(:,4)));
[redMap, grnMap,blueMap] = deal(zeros(size(data.stack.avg)));
N = numel(redMap(:,:,1));
for cellInd = 1:numel(data.activeCells)
    disp(['Cell #' num2str(cellInd)])
    z = data.cells.info(data.activeCells(cellInd)).slice;
    pxlInds  = N*(z-1)+ data.cells.info(data.activeCells(cellInd)).inds;  
    redMap(pxlInds) = redVals(cellInd);
    grnMap(pxlInds) = grnVals(cellInd);
    blueMap(pxlInds) = blueVals(cellInd);
end
toc
regMap = permute(cat(4,redMap,grnMap,blueMap),[1 2 4 3]);
regMap(regMap ==0) =nan;






