function tRoi = GetROITimeseriesFromSelectedLSPlanes_ver2(varargin)
%GetROITimeseriesFromSelectedLSPlanes - Allows for drawing of multiple ROIs
%on selected light sheet planes and obtaining timecourses from these ROIs
%
% tRoi = GetROITimeseriesFromSelectedLSPlanes(inputDir, zPlaneList)

% Outputs
%   tROI = Cell array where each cell contains timeseries from one ROI; the
%   time series from any ROI will be normalized by the area of the ROI
% Inputs
%   inputDir = Path to the directory where registered planes are located
%   zPlaneList = List of z planes to generate a max-int proj from for the
%   selection of ROIs; please note that plane # 1 is the deepest plane

inputDir = varargin{1};
zPlaneList  = varargin{2};

% [fileName,inputDir] = uigetfile({'*.*';'*.tif'},'SELECT MULTIPLE FILES USING CTRL OR SHIFT KEY','Multiselect','on');

%% Load average stack
aveStack = readtiff(fullfile(inputDir,'ave.tif'));
disp(['Size of each image in stack is ' num2str(size(aveStack,1)) ' X ' num2str(size(aveStack,2))]);
% dim=read_LSstack_size(fullfile(inputDir,'Stack dimensions.log'));


%% Error checks
if any(zPlaneList > size(aveStack,3))
    error(['Stack only has' num2str(size(aveStack,3)) 'planes!']);
    return;
elseif any(diff(unique(zPlaneList))~=1)
    error(['Chosen z-planes must be consecutive: please re-enter list']);
    return;
end


%% Read 'Stack_frequency.txt' to obtain number of time points.
fid = fopen(fullfile(inputDir,'Stack_frequency.txt'),'r');
info = fscanf(fid,'%f');
nTimePts = info(3);
fclose(fid);

%% ROIs from max-inf projection of selected planes
maxIntIm = max(aveStack(:,:,zPlaneList),[],3);
rois = setEllipticalRois(maxIntIm);
roiTimeSeries = cell(numel(zPlaneList),size(rois,2));
tStack = cell(1,size(rois,2));
for zz = 1:numel(zPlaneList)
    outputName = [inputDir, '/Plane' num2str(zPlaneList(zz), '%.2d') '.stack'];
    eval(['mtVar' num2str(zz) ' = MappedTensor(outputName,size(aveStack,1),size(aveStack,2),nTimePts,''class'',''uint16'');'])
    % stack = read_LSstack_fast1(outputName,size(aveStack));
    for rr = 1:size(rois,3)
        blah = eval(['mtVar' num2str(zz) '(rois{rr}.xi,rois{rr}.yi,:);'])
     roiTimeSeries(zz,rr) = squeeze(sum(sum(blah,1),2));
    end
end

for zz = 1:numel(zPlaneList)
    if ~isempty(tStack{zPlaneList(zz)})
        
    end
end


end

