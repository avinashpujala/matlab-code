function do
addpath('U:\Code\SPIM_cell_view\functions');
javaaddpath('C:\Program Files\ImageJ\ij.jar');
addpath('U:\Code\EphysReader');
addpath('U:\Code\Swimming Frequency Analysis\Minoru spike-based method\plotLDS');
addpath('U:\Code\Swimming Frequency Analysis\Minoru spike-based method');

datadir = 'W:\Rig1\Data\Minoru\2015-05-23';
cd(datadir);
eDataFile = 'f2-c1-vat-gal4-uas-iglusnr-headstim.dat';
ed = hekaload(eDataFile);
imgDataFileStem = 'f2-c1-vat-gal4-uas-iglusnr-head-stim';
ser = 3;
aho = ed{ser};
si = aho.ch{1}.si;
raw = aho.ch{1}.data;
raw = raw-median(raw);
frameTrigger = aho.ch{2}.data;
nSample = numel(raw);
ep_t = (0:nSample-1)*si;

%% image
imFilename = fullfile(datadir,[imgDataFileStem,'_',num2str(ser,'%03d'),'.tif'])
imd = readtiff(imFilename);
imd = imd - min(imd(:));
aveImg = mean(imd,3);
clims = min(aveImg(:)) + [0 3.0*(max(aveImg(:))-min(aveImg(:)))];
dim = size(imd);
imd_orig = imd;
imd = svd_denoise(imd,15);

%% frame trigger
fi = ijreadtiffinfo(imFilename);
scanFramePeriod = 0.0171934;
acqNumAveragedFrames = 4;

% [pks locs] = findpeaks(diff(frameTrigger),'MINPEAKHEIGHT',0.5,'NPEAKS',1);
% im_t = si*locs + ([1:dim(3)]-1)*scanFramePeriod * acqNumAveragedFrames;
[pks locs] = findpeaks(diff(frameTrigger),'MINPEAKHEIGHT',0.5);
frameIdx = locs(1:4:end);
im_t = [si*frameIdx'  si*frameIdx(end) + (1:(dim(3)-numel(frameIdx)))*scanFramePeriod * acqNumAveragedFrames];

%% ui
activeCursorFlag = 0;
playing = false;
parameterPlotFlag = false;

hf = figure('Name',['series: ', num2str(ser)]);
colormap(gray);

t = 1;
radius = 2;
set(gcf, 'WindowButtonMotionFcn', @mouseMove);
set(gcf, 'WindowButtonUpFcn', @inactivateCursor);
h_slider = uicontrol(hf,'Style','slider','Units','normalized','Position', [0.15 0.55 0.3 0.02],'SliderStep',[1/(dim(3)-1) 1/(dim(3)-1)],'Callback',@slider_callback);
h_text = uicontrol(hf,'Style','text','Units','normalized','Position', [0.25 0.93 0.1 0.05]);axis off;
h_play = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.2 0.5 0.1 0.03],'String','Play','Callback',@play_callback);
h_stop = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.3 0.5 0.1 0.03],'String','Stop','Callback',@stop_callback);
h_hotspot = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.25 0.05 0.1 0.03],'String','HotSpot','Callback',@hotspotMap_callback);
h_anlyVR = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.65 0.5 0.1 0.03],'String','Analyze VR','Callback',@anlyVR_callback);
    
h_checkboxPlotParameter = uicontrol('Style', 'checkbox', 'String', 'Parameter Plot',...
        'Units','normalized',...
        'Position', [0.75 0.5 0.1 0.03],'Max',1,'Min',0,...
        'Value',0,'Enable','off',...
        'Callback', @toggleParameterPlot_callback);

uicontrol('Style', 'pushbutton', 'String', 'Draw ROI',...
        'Position', [20 20 50 20],...
        'Callback', @plotTimecourse);

h_checkboxCursor = uicontrol('Style', 'checkbox', 'String', 'Activate',...
        'Position', [20 50 50 20],'Max',1,'Min',0,...
        'Value',0,...
        'Callback', @activateCursor);


uicontrol('Style', 'text', 'String', 'Radius',...
        'Position', [80 50 50 20]);
    
uicontrol('Style', 'edit','String', num2str(radius),...
        'Position', [ 140 50 50 20],...
        'Callback', @updateRadius);
    
ax_movie = subplot(2,2,1);
imagesc(imd(:,:,t),clims);axis off; axis image;
set(h_text, 'String',['t=',num2str(t)]);

ax_im = subplot(2,2,3);
imagesc(aveImg);
axis image;axis off;
h_rect = line([0 2*radius 2*radius 0],[0 0 2*radius 2*radius],'Color',[1 0 0]);

% ax_bw = subplot(2,2,3);

ax(1) = subplot(2,2,4);

ax(2) = subplot(2,2,2);
vrRange = [-0.5 0.5];
plot(ep_t,raw,'k');
ylim(vrRange.*2);
h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.5 .5 .5]);
hold on;
zoom xon; pan xon;

% ax(3) = subplot(3,2,6);
% plot(ep_t,frameTrigger,'b');
linkaxes(ax,'x');
axes(ax(2));
xlim([-Inf Inf]);

epi =[];vr=[];
% %% convolve
% sm_imd = convn(imd,ones(3)/9);
% norm_sm_imd = zeros(dim);
% cumPosdFF = zeros(dim(1:2));
% for i = 1:dim(1)
%     for j = 1:dim(2)
%         temp = normalize(sm_imd(i,j,:));
%         norm_sm_imd(i,j,:) = temp;
%         aho = squeeze(temp);
%         cumPosdFF(i,j) = sum(aho(find(aho>0)));
%     end
% end
% 
% norm_aveImg = imNormalize999(aveImg);
% level = graythresh(norm_aveImg);
% mask = im2bw(norm_aveImg,level);
% % figure;
% % imshow(mask);
% % figure;
% % imagesc(norm_aveImg);
% 
% figure;
% imagesc(cumPosdFF.*norm_aveImg);
%% utility functions
    function out=normalize(img)
%         temp=sort(img(:),'ascend');
%         threshold=temp(round(length(temp)/5));
        threshold = median(img(:));
        out=(img-threshold)./(threshold-min(img(:)));
    end

    function im_out = svd_denoise(im_in,cut)
        dim = size(im_in);
        Y = reshape(double(im_in),dim(1)*dim(2), dim(3));
        ameanY=mean(Y')';
        Y2=Y-repmat(ameanY,[1,dim(3)]);
        [U,S,V]=svd(Y2,0);
        
        s=diag(S);
        s(cut:end)=0;
        S0=diag(s);
        
        Y0=U*S0*V';
        
        im_out = reshape(repmat(ameanY,[1,dim(3)])+Y0,dim);
        
    end

%% callbacks
    
    function toggleParameterPlot_callback(src,evt)
        state = get(src,'Value');
        if state == 1
            parameterPlotFlag = true;
%             disp('parameterPlotFlag: on');
            updateVR;
        elseif state ==0
            parameterPlotFlag = false;
%             disp('parameterPlotFlag: off');
            updateVR;
        end
    end

    function anlyVR_callback(src,evt)
        [epi vr] = vrEpisodeDetectionGeneral(raw,si);
        updateVR;
        set(h_checkboxPlotParameter,'Enable','on');
%          %% plot vr recording
%         axes(ax(2))
%         arrayfun(@(x)line([x x],vrRange/2,'Color',[.5 0 .5]),(vr.curatedIndex-1)*vr.si);
% 
%         %% overlay episode information
% %         epi = editted_epi;
%         for i=1:length(epi)
%             burst = epi{i}.burst;
%             for j=1:length(burst)
%                 if length(burst{j}.spikes)>1
%                     burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
%                 elseif length(burst{j}.spikes) == 1
%                     burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
%                 end
%                 patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)]/2,...
%                     'g', 'FaceAlpha', 0.3,'EdgeColor','none');
%             end
% 
%             onsets = epi{i}.onsets;    
%             freq = epi{i}.freq;
% 
%             for k=1:length(freq)
%                 text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
%                     'HorizontalAlignment','center','Parent',ax(2));
%             end
% 
%         end
% 
%         %% overlay shock onsets
%         plot(ax(2),(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
    end

    function updateVR
        axes(ax(2)); hold off;
        plot(ep_t,raw,'k');
        ylim(vrRange.*2);
        h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.5 .5 .5]);
        hold on;
        zoom xon; pan xon;
        if ~isempty(epi)
            if parameterPlotFlag
                axes(ax(2));hold off;
                plot(ep_t,10*abs(raw),'k');
                hold on;
%                 [hAx h1 h2]=plotyy([0,1],[0,1],[0,1],[0,1]); 
%                 axes(hAx(1)); cla; hold(hAx(1),'on');
%                 axes(hAx(2)); cla; hold(hAx(2),'on');
                for i=1:length(epi)
                    if ~isempty(epi{i}.freq)
                        onsets = epi{i}.onsets;
                        freq = epi{i}.freq;
                        
                        %                     for k=1:length(freq)
                        %                         text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                        %                             'HorizontalAlignment','center','Parent',ax(2));
                        %                     end
%                         axes(hAx(1));
                        plot(onsets(1:end-1),freq,'g-');
                        hold on;

                        burst = epi{i}.burst;
                        burstT = [];
                        burstDuration = [];
                        for j=1:length(burst)
                            if length(burst{j}.spikes)>1
                                burstDuration = [burstDuration burst{j}.spikes(end)-burst{j}.spikes(1)];
                                burstT = [burstT burst{j}.spikes(1)];
                                %                             burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
                            elseif length(burst{j}.spikes) == 1
                                burstDuration = NaN;
                                burstT = NaN;
                                %                             burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
                            end
                            %                         patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)]/2,...
                            %                             'g', 'FaceAlpha', 0.3,'EdgeColor','none');
                        end
                        
                        plot(burstT,burstDuration*10^3,'r-');

%                         [hAx, hLine1, hLine2] = plotyy(onsets(1:end-1),freq,burstT,burstDuration*10^3);
%                         linkaxes(hAx,'x');
%                         hold(hAx(1), 'on');
%                         hold(hAx(2),'on');

                    end                    
                end

                %% overlay shock onsets
                plot(ax(2),(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
                ylim([0 60]);
%                 line((vr.shockIndex-1)*vr.si,'Parent',ax(2))
%                 axes(ax(2));
%                 arrayfun(@(x)line([x x],get(hAx(1),'YLim'),'Color',[1 0 0]),(vr.shockIndex-1)*vr.si);
            else
                arrayfun(@(x)line([x x],vrRange/2,'Color',[.5 0 .5]),(vr.curatedIndex-1)*vr.si);
                
                %% overlay episode information
                %         epi = editted_epi;
                for i=1:length(epi)
                    burst = epi{i}.burst;
                    for j=1:length(burst)
                        if length(burst{j}.spikes)>1
                            burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
                        elseif length(burst{j}.spikes) == 1
                            burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
                        end
                        patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)]/2,...
                            'g', 'FaceAlpha', 0.3,'EdgeColor','none');
                    end
                    
                    onsets = epi{i}.onsets;
                    freq = epi{i}.freq;
                    
                    for k=1:length(freq)
                        text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                            'HorizontalAlignment','center','Parent',ax(2));
                    end
                    
                end
                
                %% overlay shock onsets
                plot(ax(2),(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
            end
            h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.2 .2 .2]);
        end
    end
    
    function hotspotMap_callback(src,evt)
        sm_imd = convn(imd,ones(3)/9);
        norm_sm_imd = zeros(dim);
        cumPosdFF = zeros(dim(1:2));
        for i = 1:dim(1)
            for j = 1:dim(2)
                temp = normalize(sm_imd(i,j,:));
                norm_sm_imd(i,j,:) = temp;
                aho = squeeze(temp);
                cumPosdFF(i,j) = sum(aho(find(aho>0)));
            end
        end
        
        norm_aveImg = imNormalize999(aveImg);
        level = graythresh(norm_aveImg);
        mask = im2bw(norm_aveImg,level);
        % figure;
        % imshow(mask);
        % figure;
        % imagesc(norm_aveImg);
        
        figure;
        imagesc(cumPosdFF.*norm_aveImg);
        axis image; axis off;
    end

    function slider_callback(src,evt)
        v = get(src,'Value');
        t = round(v*(dim(3)-1))+1;
        axes(ax_movie);
        imagesc(imd(:,:,t),clims); axis off; axis image;
        set(h_text,'String', ['t=',num2str(t)]);
        set(h_timeline,'XData',im_t(t)*[1 1]);

    end

    function play_callback(src,evt)
        startT = t;
        playing = true;
        for t=startT:dim(3)
            axes(ax_movie);
            imagesc(imd(:,:,t),clims); axis off; axis image;
            set(h_text,'String', ['t=',num2str(t)]);
            set(h_slider,'Value',(t-1)/dim(3));
            set(h_timeline,'XData',im_t(t)*[1 1]);

            pause(0.03);
            if ~playing
                break;
            end
        end
    end

    function stop_callback(src,evt)
        playing = false;
        stopT = t;
        axes(ax_movie);
        imagesc(imd(:,:,t),clims); axis off; axis image;
        set(h_text,'String', ['t=',num2str(t)]);
        axes(ax(2));
        set(h_timeline,'XData',im_t(t)*[1 1]);

    end

    function updateRadius(src,evt)
        radius = str2num(get(src, 'String'));
    end

    function activateCursor(src,evt)
        if get(src,'Value') == 1
            activeCursorFlag = 1;
        elseif get(src,'Value') == 0
            activeCursorFlag = 0;
        end
    end

    function inactivateCursor(src, evt)
        activeCursorFlag = 0;
        set(h_checkboxCursor,'Value',0);
    end

    function plotTimecourse(src,evt)
        axes(ax_im);
        [BW] = roipoly;
%         h = imellipse(gca, [10 10 radius radius]);
%         addNewPositionCallback(h,@(p) title(mat2str(p,3)));
%         fcn = makeConstrainToRectFcn('imellipse',get(gca,'XLim'),get(gca,'YLim'));
%         setPositionConstraintFcn(h,fcn);
%         wait(h);
%         BW = createMask(h);
        axes(ax(1));
        flat_imd = reshape(imd, [],dim(3));
        ts = mean(flat_imd(find(BW),:),1);
        plot(im_t,ts);
    end

    function mouseMove(src,evt)
        if activeCursorFlag
            C = get(ax_im, 'CurrentPoint');
    %         position = get(ax_im,'Position'); 
            xrange = get(ax_im,'XLim');
            yrange = get(ax_im,'YLim');
            if  xrange(1)+radius < C(1,1) && C(1,1)< xrange(2)-radius && yrange(1)+radius < C(1,2) && C(1,2)< yrange(2)-radius
                currX = floor(C(1,1));
                currY = floor(C(1,2));
                title(ax_im, ['(X,Y) = (', num2str(currX),', ', num2str(currY), ') radius = ', num2str(radius)]);
    %             h = line(currX+[-radius radius radius -radius],currY+[-radius -radius radius radius]);
                set(h_rect,'XData',currX+[-radius radius radius -radius -radius],'YData',currY+[-radius -radius radius radius -radius]);
                BW = zeros(dim(1:2));
    %             BW(currX-radius:currX+radius,currY-radius:currY+radius) = 1;
                BW(currY-radius:currY+radius,currX-radius:currX+radius) = 1;
    %             axes(ax_bw);imagesc(BW.*aveImg);axis off; axis image;
                axes(ax(1));
                flat_imd = reshape(imd, [],dim(3));
                ts = mean(flat_imd(find(BW(:)),:),1);
                plot(im_t,ts,'k');
            end
        end
    end
end
        
        