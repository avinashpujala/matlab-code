function [segmentedData, incompleteSegmentIndices] = SegmentDataByEventsVer2(unsegmentedData,samplingInt,eventIndices, preEventPeriod, postEventPeriod)
% SegmentDataByEventsVer2 - Segment continuous sweep data; works like
%                           SegmentDataByEvents except does not presume
%                           columnar arrangemnt of timeseries in matrix
%   segmentedData = SegmentData(unsegmentedData,sampingInt,eventIndices, preEventPeriod, postEventPeriod)
%
%  segmentedData - Segments of data centered around eventIndices, inluding
%                   preEventPeriod and postEventPeriod. Segments are
%                   arranged as cell rows
% incompleteSegmentIndices - Indices of segments that fall short of the
%                           specified time period

if nargin < 5
    errordlg('At least 5 inputs required!')
    return;
end

samplingRate = 1/samplingInt;

incompleteSegmentIndices = [];
preEventPts = eventIndices - floor(preEventPeriod*samplingRate);
inds = find(preEventPts < 0);
incompleteSegmentIndices = [incompleteSegmentIndices; inds(:)];
preEventPts(inds) = 1; % In case 1st event occurs before lapse of preEventPeriod


postEventPts = eventIndices + floor(postEventPeriod*samplingRate);
inds = find(postEventPts > size(unsegmentedData,2));
incompleteSegmentIndices = [incompleteSegmentIndices; inds(:)];
postEventPts(inds) = size(unsegmentedData,2); % In case recording terminates before lapse of postStimPeriod after the last event


segmentedData = cell(numel(eventIndices),1);
for event = 1:numel(eventIndices)
    segmentedData{event,1} = unsegmentedData(:, preEventPts(event):postEventPts(event));
end


end

