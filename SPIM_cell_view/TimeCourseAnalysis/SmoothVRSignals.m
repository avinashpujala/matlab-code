
function smSignal = SmoothVRSignals(vrSignal,samplingInt,lpFilt)
% SmoothVRSignals2 - Custom smoothing of VR signals; Version where data is not
% rectified, but squared and then later square-rooted.
% smSignal = SmoothVRSignals(rawVRSignal,samplingIng,lpFilt)


if nargin < 3
    errordlg('SmoothVRSignals requires atleast 3 inputs')
    return;
end

if size(vrSignal,1) < size(vrSignal,2)
    vrSignal = vrSignal';
end

kernelWidth = 0.5/lpFilt;
lenKer = round(kernelWidth/samplingInt);
% kernel = sin(pi*linspace(0,1,lenKer)); % Sinusoidal kernel
kernel = gausswin(lenKer);
kernel = kernel/sum(kernel);

smSignal1  = sqrt(conv2(vrSignal(:).^2, kernel(:),'same'));

% kernel = sin(pi*linspace(0,1,2*lenKer)); % Sinusoidal kernel
kernel = gausswin(2*lenKer);
kernel = kernel/sum(kernel);

smSignal  = ZscoreByHist(conv2(smSignal1(:).^1.2, kernel(:),'same'));

% smSignal = ZscoreByHist((smSignal1+smSignal2)/2);
% smSignal = vrSignal.^2;
% smSignal = ZscoreByHist((conv2(conv2(smSignal(:),kernel(:),'same'), kernel(:),'same')).^0.5); % Double convolution is to really sinusoidalize the signal

end



