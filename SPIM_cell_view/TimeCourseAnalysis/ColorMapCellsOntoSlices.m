%clear all;close all;

%% Setting values
% input_dir='Z:\SPIM\Avinash\11-1-2014\Fish1\G10_T2_20141101_163145';
% cells1 = consideredCells;
cells2 = testCellInds;


%% Loading stuff
ave_stack  = double(readtiff(fullfile(input_dir,'\ave.tif')));  %% average anatomy stack of SPIM
load(fullfile(input_dir,'\cell_info_processed.mat'));
load(fullfile(input_dir,'\cell_resp_dim_processed.mat'));

%cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);
% load('cell_resp3.mat');


%% 
cell_resp=cell_resp3;
normStack = StackNormalize99(ave_stack);
normStack(normStack>1)=1; % This is what TK was doing
normStack_rgb = permute(repmat(normStack,[1 1 1 3]),[1 2 4 3]); % 3rd dim is for color channel, 4th dim is for slice
numcell=length(cell_info);
dim=size(ave_stack);
dimv_yx = size(normStack_rgb);

% cells1  = responsiveCells;

% anatomy_image_yx = repmat(imNormalize99(max(ave_stack,[],3)),[1 1 3]); % Normalized max-intensity projected image in 3 layers
% anatomy_image_yz = repmat(imNormalize99(squeeze(max(ave_stack,[],2))),[1 1 3]);
% anatomy_image_yx_ori = anatomy_image_yx;
% anatomy_image_yz_ori = anatomy_image_yz;

% dimv_yz = size(anatomy_image_yz)

% 
% anatomy_image_yz2=zeros(dimv_yz(1),dimv_yz(2)*10,3);
% anatomy_image_yz2_ori=anatomy_image_yz2;
tot_image=zeros(dimv_yx(1),dimv_yx(2));
% tot_image(:,dimv_yx(2)+(1:10),:)=1;

circle=makeDisk2(7,15);
[r, v]=find(circle);
r=r-8;v=v-8;
circle_inds  = r*dimv_yx(1)+v;
% yzplane_inds = -5:5;

%% Uncomment this cell if same color is desired for al cells
 colorValues = repmat([1 0 0],length(cells1),1);


%% Assigning colors to cells z slice by z slice (Must think of a way to speed this up. Too slow as it stands)
fprintf('\n Assigning colors to cells and mapping to z-splayed image \n')
 zInds_ConsideredCells = [cell_info(cells1).slice];
  cVals =[cells1(:), colorValues];
for zSlice  = 1:dimv_yx(4);
    fprintf(['\n Current Slice: ' num2str(zSlice) ' \n'])
    f = find(zInds_ConsideredCells == zSlice);
    cellsInSlice = cells1(f);
    cVals = colorValues(f,:);
    for jj=1:length(cellsInSlice)
        cinds=(cell_info(cellsInSlice(jj)).center(2)-1)*dimv_yx(1)+cell_info(cellsInSlice(jj)).center(1);
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2));      
        lut = cVals(jj,:);
        anatomy_image_yx = normStack_rgb(:,:,:,zSlice);
         anatomy_image_yx(cinds+circle_inds(labelinds))=lut(1);
       anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=lut(2);
        anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=lut(3);
        normStack_rgb(:,:,:,zSlice) = anatomy_image_yx;
%         anatomy_image_yx(cinds+circle_inds(labelinds))=lut(1);
%         anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=lut(2);
%         anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=lut(3);
        
        %     cinds=(cell_info(cells1(j)).slice-1)*dimv_yz(1)+cell_info(cells1(j)).center(1);
        %     labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2));
        %     anatomy_image_yz(cinds+yzplane_inds(labelinds))=lut(1);
        %     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=lut(2);
        %     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=lut(3);
        
    end
end
%%%%%%% I won't display these cells for the time being %%%%%%%%%
% for j=1:length(cells2)    
%     cinds=(cell_info(cells2(j)).center(2)-1)*dimv_yx(1)+cell_info(cells2(j)).center(1);
%     labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
%     anatomy_image_yx(cinds+circle_inds(labelinds))=0;
%     anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=1;
%     anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.75;    
% 
%     cinds=(cell_info(cells2(j)).slice-1)*dimv_yz(1)+cell_info(cells2(j)).center(1);
%     labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
%     anatomy_image_yz(cinds+yzplane_inds(labelinds))=0;
%     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=1;
%     anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.75;
% end



% for k=1:3
%     anatomy_image_yz2(:,:,1)=imresize(anatomy_image_yz(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
%     anatomy_image_yz2(:,:,2)=imresize(anatomy_image_yz(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
%     anatomy_image_yz2(:,:,3)=imresize(anatomy_image_yz(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
%     anatomy_image_yz2_ori(:,:,1)=imresize(anatomy_image_yz_ori(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
%     anatomy_image_yz2_ori(:,:,2)=imresize(anatomy_image_yz_ori(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
%     anatomy_image_yz2_ori(:,:,3)=imresize(anatomy_image_yz_ori(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
% end

%%
% tot_image(:,1:dimv_yx(2),:)=anatomy_image_yx;
% % tot_image(:,dimv_yx(2)+11:end,:)=anatomy_image_yz2;

% tot_image(tot_image(:)>1)=1;
% tot_image(tot_image(:)<0)=0;

%%  Map Cells

figPos = getMonitorSize;
[figPos(1),figPos(2)] = deal(50);
figPos(4) = figPos(4)*0.8;
figPos(3) = min(0.95*figPos(4), figPos(3)*0.8);
map = figure('position',figPos);
colormap(gray);
normStack_rgb(normStack_rgb>1)=1;
normStack_rgb(normStack_rgb<0)=0;
brightestPixel = max(normStack(:));
for jj = 1 :size(normStack_rgb,4);
    subaxis(5,9,jj, 'Spacing', 0, 'Padding', 0, 'Margin', 0);
    image(normStack_rgb(:,:,:,jj)); % Scaling to use the full color range
    axis tight
    axis off
end
% hold on
figure
cb = colorbar;
colormap(cmap_new);
%  cTick = get(cb,'YTick');
%  cTickLabel = get(cb,'YTickLabel'); 
tickGap = ceil(length(LUT)/10);
cTick = 1:tickGap:length(LUT);
cTickLabel = round(LUT(1:tickGap:end,1));
cTickLabel = num2str(cTickLabel(:));
set(cb,'YTick',cTick,'YTickLabel', cTickLabel)

return;



%%

cellinds_ex=define_region_roi_new( cell_info, cells1, ave_stack);

%%
figure(2);
for j=1:length(cellinds_ex);
    cnum=j;
     subplot(10,10,j);plot(cell_resp_ave(cellinds_ex(j),:),'r','linewidth',1);
    title(num2str(cnum));
    ylim([130 200]);
    xlim([0 sum(timelist)]);
    line([5 5],[0.95 1.2]) 
   % line([104 104],[0.95 1.3]) 
end

%%
hb_cellinds=cellinds_ex;
    
save(fullfile(input_dir,'\Registered\hb_cellinds.mat'),'hb_cellinds');

%%
dim_x = size(cell_resp,2)/400;
IM = reshape(cell_resp(extracted_cellinds(14),:), 400 ,dim_x)';
figure('position',[50 50 1000 800]);
imagesc(IM);
title('cell-14')






