
inputDir = 'Y:\SPIM\Avinash\Registered\May 2015\5-9-2015_relaxed_dpf4\Fish2';

filesInDir = dir(inputDir);
fName= {};
for file = 1:length(filesInDir)
    if strfind(lower(filesInDir(file).name),lower('RegressionData'));
        fName = filesInDir(file).name;
    end
end
if ~exist('regFile')
    regFile = matfile(fullfile(inputDir,fName),'Writable', true);
    disp('Reading data...')
    tic
    regData = regFile.regData;
    toc
end

if (size(regData.X,1) ~= size(regData.Y,1)) & (size(regData.X,1) == size(regData.Y,2))
    regData.Y = regData.Y';
end

% regData = struct;
B = {};
B_int = {};
Rsq = zeros(size(regData.Y,2),1);
res = {};
res_int = {};
stats = {};
tic
for cellInd = 1:size(regData.Y,2);
    disp(['Cell ' num2str(cellInd)])
    [B{cellInd},B_int{cellInd},res{cellInd},res_int{cellInd}] = regress(regData.Y(:,cellInd),regData.X);
    num = sum(res{cellInd}.^2);
    den = sum((regData.Y(:,cellInd)-mean(regData.Y(:,cellInd))).^2);
    Rsq(cellInd) = 1-(num/den);
end
toc
B = cell2mat(B);
B = B';
B_int = cell2mat(B_int);
res = cell2mat(res);
res_int = cell2mat(res_int);

regData.B = B;
regData.B_int = B_int;
regData.res = res;
regData.res_int = res_int;

disp('Writing data to matfile...')
tic
regFile.B = regData.B;
regFile.B_int = regData.B_int;
regFile.res = regData.res;
regFile.res_int = regData.res_int;
regFile.RSq = regData.Rsq;
toc

% disp('Saving regression data...')
% tic
% ts = datestr(now,30);
% save(fullfile(saveDir,['regressionData-' ts '.mat']),'regData','-v7.3');
% toc