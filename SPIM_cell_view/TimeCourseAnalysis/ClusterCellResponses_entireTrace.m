function varargout = ClusterCellResponses_entireTrace(varargin)
%ClusterCellResponses Cluster's cell responses
% kMeansData = ClusterCellResponses_entireTrace(fid,nClusters)  

rng(14,'twister')

fid = varargin{1};
if nargin < 2
    nClusters = 10;
else
nClusters  = varargin{2};
end

tic
disp('Loading cellResponses...')
blah = load(fid)
toc

% stimGrp = size(cellRespMat.trialAvg,4);
% data = cellRespMat.trialAvg(:,:,1,stimGrp);
fn = fieldnames(blah);
data = blah.(fn{1});
seedFlag = 0;
if length(fn) == 2
seedMat = blah.(fn{2});
seedFlag = 1;
end

tic
disp('Performing KMeans Clustering ...')
if seedFlag
    [idx,C,sumd,D] = kmeans(data,[],'start', seedMat);
    nClusters = size(seedMat,1);
else
    [idx, C, sumd, D] = kmeans(data,nClusters);
end

disp(['K-means clustered data into ' num2str(nClusters) ' clusters'])
toc

kData.data = data;
kData.idx = idx;
kData.C = C;
kData.sumd = sumd;
kData.D = D;


% kData = RemoveWeakSignalsFromClusters(kData);

varargout{1} = kData;

bSlashInds = strfind(fid,'\');
outputDir = fid(1:bSlashInds(end)-1);
save(fullfile(outputDir,['kMeansData_entireTrace_' num2str(nClusters) ' clusters']),'kData','-v7.3')
disp(['Saved data at ' outputDir ])

end

function kData = RemoveWeakSignalsFromClusters(kData)
% Go through each cluster and eliminate cells that show weak responses relative to that cluster's mean
disp('Removing cells with weak activity (w.r.t cluster mean) from each cluster')
nClusters = numel(unique(kData.idx));
mu = zeros(nClusters,1);
sigSig = mu;
cutoffThr = mu;
for clstr = 1:nClusters
    cellsInCluster = find(kData.idx == clstr);
    sig = std(kData.data(cellsInCluster,:),[],2);
%     figure
    [counts, vals] = hist(sig,50);
    mu(clstr) = sum((vals(:).*counts(:)))/sum(counts);
    sigSig(clstr) = std(sig);
    cutoffThr(clstr) = mu(clstr) -0.1*sigSig(clstr);
%     plot(vals,counts,'k')
%     hold on
%     stem(mu(clstr),max(counts(:)),'b','marker','none')
%     stem(cutoffThr(clstr),max(counts(:)),'r','marker','none')
    removeInds = cellsInCluster(sig<=cutoffThr(clstr));    
    kData.idx(removeInds,:)= nan;
    kData.data(removeInds,:) = nan;
    kData.D(removeInds,:)= nan;
    nAfter = sum(kData.idx == clstr);
    nBefore = nAfter + numel(removeInds);
    
    disp(['Cluster ' num2str(clstr) ': ' num2str(nBefore - nAfter)...
        ' (' num2str((1-(nAfter/nBefore))*100) '%) cells removed'])
end

end