function outInds = MapIndsToDifferentlySampledTimeseries(inInds,ts2)
% MapIndsToDifferentlySampledTimeseries - Expresses indices of one
%   timeseries as indices of another timeseries sampled at a different interval
% indsInTimeseries2 = 
%    MapIndsToDifferentlySampledTimesereies(indsInTimeseries1, timeseries2);
% Input:
% indsInTimeseries1 - indices in the first input timeseries
% timeseries2 - vector of all samples in the second timeseries, which are
%               assumed to be a sub/superset of all samples in the first 
%               timeseries
% Output:
% indsInTimeseries2 - indices of samples in timeseries2 that correspond to
%                     the input inds indsInTimeseries1

if ~isvector(inInds)
    errordlg('Index input cannot be a matrix!')
elseif length(unique(inInds)) > length(ts2)
    warndlg('Input inds contain non-unique values')
end

outInds = zeros(size(inInds));
for ind = 1:length(inInds)
    [~,outInds(ind)] = min(abs(ts2 - inInds(ind)));
end
b = unique(outInds);
extras = length(inInds)-length(b);
if extras > 0    
    warndlg([num2str(extras) ' redundant indices found and removed!'])
    outInds = unique(outInds);
end