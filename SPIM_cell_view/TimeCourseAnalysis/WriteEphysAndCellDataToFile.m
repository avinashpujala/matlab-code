function WriteEphysAndCellDataToFile(fileName,trialSegData)

responsiveCells = trialSegData(1).cells.responsiveCellInds;
fileName = [fileName '.csv'];
outputfilename = fopen(fileName,'w+');
fprintf(outputfilename,['CellNum, TrialNum, StimulusOnset(s), StimulusAmplitude(V), LatencyOne(ms),LatencyTwo(ms), Duration(ms), MaxFrequency(Hz), MaxSpeed, CaPeakLatency, CaPeakAmp, CaDecayTime\n']);

for ii = 1:numel(responsiveCells)
    for i = 1:numel(trialSegData)
        t = trialSegData(i);             
        fprintf(outputfilename, '%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n', ...
            responsiveCells(ii), i, t.stim.time, t.stim.amps(2), t.swim.onset(1)*1000, ...
            t.swim.onset(2)*1000,t.swim.dur(1)*1000,...
            t.Wxy.maxFreq, sqrt(t.swim.speed(1)*t.swim.speed(2)),...
            t.cells.peakLatency(ii), t.cells.peakAmp(ii),...
            t.cells.decayTime(ii));        
    end
    disp(['Cell Num ' num2str(ii)])
end

fclose(outputfilename);
disp('Writing complete')