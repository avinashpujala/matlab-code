function do_ap_click(varargin)
% My modifications to Minoru's code
% addpath('V:\Code\SPIM_cell_view\functions');
javaaddpath('C:\Program Files\ImageJ\ij.jar');
% addpath('V:\Code\EphysReader');
% addpath('V:\Code\Swimming Frequency Analysis\Minoru spike-based method\plotLDS');
% addpath('V:\Code\Swimming Frequency Analysis\Minoru spike-based method');

dir_img = 'Z:\SPIM\Avinash\Registered\May 2015\5-9-2015_relaxed_dpf4\Fish2\G50_T1_IPI60_20150509_162303\';
fn = 'pyData';
% dir_ephys = 'Z:\SPIM\Avinash\Raw\May 2015\5-9-2015_relaxed_dpf4\Ephys\Fish2';
% eDataFile = 'G50_T1_IPI60.10chFlt';
% % cd(dir_img);

if nargin == 0
    eData = apEphysLoad(fullfile(dir_ephys,eDataFile));
elseif nargin == 1
    eData = varargin{1};
end
samplingRate = 6000;
si = 1/samplingRate;
ep_t = eData.t;

%% image
% dir_proc = [dir_img '\proc'];
% imFilename = fullfile(dir_proc,'ActivityMap.tif');
% imgInfo = imfinfo(imFilename);

imd = [];
% clrMap = {};
% anatImg =[];
% disp('Reading activity map...')
% tic
% % [imd(:,:,1), clrMap{1}] = rgb2ind(imread(imFilename,1),128);
% % for z = 2:size(imgInfo,1)
% %     [imd(:,:,z), clrMap{z}] = rgb2ind(imread(imFilename,z),128);
% %     anatImg(:,:,:,z) = imread(fullfile(dir_proc,'AnatStack.tif'));
% % end
% toc
% imd = flipdim(imd,2); % To give top-down view of z-slices, which is more intuitive & less confusing
imd = eData.activityMap;
% imd  = permute(imd,[2 1 3 4]);
% aveImg = mean(imd,4);
dim = size(imd);
imd_orig = imd;
% imd = svd_denoise(imd,15);

%% Stack processing
% fi = ijreadtiffinfo(imFilename);
 
% stack.inds.start = FindStimPulses(eData.camTrigger,10,3.7,0.1*samplingRate,'exact');
im_t = eData.t(eData.stack.inds.start);
if length(im_t) > size(eData.cells.dFF,2)
    im_t(end) = [];
end

% minS = min(eData.avgImg(:));
% maxS = max(eData.avgImg(:))*0.3;
% eData.avgImg = (eData.avgImg - minS)/(maxS-minS);

%% ui
activeCursorFlag = 0;
playing = false;
parameterPlotFlag = false;

hf = figure('Name','Activity Map','Position',[50 500 1100 600]);


t = 1;
z = 23;
radius = 2;
trl = 2;
cellInd = 2;
trlModeFlag = 0;

set(gcf, 'WindowButtonMotionFcn', @mouseMove);
% set(gcf, 'WindowButtonUpFcn', @inactivateCursor);
set(gcf, 'WindowButtonDownFcn', @inactivateCursor);
h_slider = uicontrol(hf,'Style','slider','Units','normalized',...
    'Position', [0.35 0.5 0.3 0.04],'SliderStep',[1/(dim(4)-1) 1/(dim(4)-1)],'Callback',@slider_callback);
h_text = uicontrol(hf,'Style','text','Units','normalized','Position', [0.45 0.48 0.08 0.04]);axis off;

h_radio_trial= uicontrol(gcf,'Style', 'radiobutton','String','Trl Mode','Units','Normalized',...
    'Position',[0.85 0.5 0.14 0.05],'HandleVisibility','off','Callback', @trialModeRadioCallback);
h_txt_trialNum = uicontrol(gcf,'Style','edit','String','Trl #', 'Units','Normalized',...
    'Position',[0.85 0.55 0.14 0.05], 'Callback', @trlNumTxtCallback);

h_checkboxCursor = uicontrol('Style', 'checkbox', 'String', 'Activate','Units','Normalized',...
    'Position', [0.15 0.48 0.12 0.05],'Max',1,'Min',0,...
    'Value',0,...
    'Callback', @activateCursor);


ax_movie = subplot(3,1,1);
ax(1) = subplot(3,1,2);
ax(2) = subplot(3,1,3);


set(ax_movie, 'position',[0.1 0.55 0.8 0.4],'xtick',[], 'ytick',[]);
set(ax(1),'position',[0.1 0.3 0.8 0.15],'tickdir','out')
set(ax(2),'position',[0.1 0.1 0.8 0.15],'tickdir','out')

axes(ax_movie)
imHandle = imagesc(imd(:,:,:,z));axis image, axis off
set(h_text, 'String',['z=',num2str(z)]);

axes(ax(2))
vrRange = [-20 max(eData.smooth.burst(:))];
plot(eData.t(1:50:end),eData.smooth.burst(1:50:end, 1));
ylim(vrRange);
h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.5 .5 .5]);
hold on;
plot(eData.t(eData.stim.inds),1000*ones(size(eData.stim.inds)),'r*')
zoom xon; pan xon;
box off

linkaxes(ax,'x');
axes(ax(2));
xlim([-Inf Inf]);

epi =[]; vr=[];

%% utility functions
    function out=normalize(img)
        threshold = median(img(:));
        out=(img-threshold)./(threshold-min(img(:)));
    end

    function im_out = svd_denoise(im_in,cut)
        dim = size(im_in);
        Y = reshape(double(im_in),dim(1)*dim(2), dim(3));
        ameanY=mean(Y')';
        Y2=Y-repmat(ameanY,[1,dim(3)]);
        [U,S,V]=svd(Y2,0);
        
        s=diag(S);
        s(cut:end)=0;
        S0=diag(s);
        
        Y0=U*S0*V';
        
        im_out = reshape(repmat(ameanY,[1,dim(3)])+Y0,dim);
        
    end

%% callbacks
    function toggleParameterPlot_callback(src,evt)
        state = get(src,'Value');
        if state == 1
            parameterPlotFlag = true;      
            updateVR;
        elseif state ==0
            parameterPlotFlag = false;   
            updateVR;
        end
    end

    function anlyVR_callback(src,evt)
        [epi vr] = vrEpisodeDetectionGeneral(raw,si);
        updateVR;
        set(h_checkboxPlotParameter,'Enable','on');
        %          %% plot vr recording
        %         axes(ax(2))
        %         arrayfun(@(x)line([x x],vrRange/2,'Color',[.5 0 .5]),(vr.curatedIndex-1)*vr.si);
        %
        %         %% overlay episode information
        % %         epi = editted_epi;
        %         for i=1:length(epi)
        %             burst = epi{i}.burst;
        %             for j=1:length(burst)
        %                 if length(burst{j}.spikes)>1
        %                     burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
        %                 elseif length(burst{j}.spikes) == 1
        %                     burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
        %                 end
        %                 patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)]/2,...
        %                     'g', 'FaceAlpha', 0.3,'EdgeColor','none');
        %             end
        %
        %             onsets = epi{i}.onsets;
        %             freq = epi{i}.freq;
        %
        %             for k=1:length(freq)
        %                 text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
        %                     'HorizontalAlignment','center','Parent',ax(2));
        %             end
        %
        %         end
        %
        %         %% overlay shock onsets
        %         plot(ax(2),(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
    end

    function updateVR
        axes(ax(2)); hold off;
        plot(ep_t,raw,'k');
        ylim(vrRange.*2);
        h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.5 .5 .5]);
        hold on;
        zoom xon; pan xon;
        if ~isempty(epi)
            if parameterPlotFlag
                axes(ax(2));hold off;
                plot(ep_t,10*abs(raw),'k');
                hold on;
                %                 [hAx h1 h2]=plotyy([0,1],[0,1],[0,1],[0,1]);
                %                 axes(hAx(1)); cla; hold(hAx(1),'on');
                %                 axes(hAx(2)); cla; hold(hAx(2),'on');
                for i=1:length(epi)
                    if ~isempty(epi{i}.freq)
                        onsets = epi{i}.onsets;
                        freq = epi{i}.freq;
                        
                        %                     for k=1:length(freq)
                        %                         text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                        %                             'HorizontalAlignment','center','Parent',ax(2));
                        %                     end
                        %                         axes(hAx(1));
                        plot(onsets(1:end-1),freq,'g-');
                        hold on;
                        
                        burst = epi{i}.burst;
                        burstT = [];
                        burstDuration = [];
                        for j=1:length(burst)
                            if length(burst{j}.spikes)>1
                                burstDuration = [burstDuration burst{j}.spikes(end)-burst{j}.spikes(1)];
                                burstT = [burstT burst{j}.spikes(1)];
                                %                             burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
                            elseif length(burst{j}.spikes) == 1
                                burstDuration = NaN;
                                burstT = NaN;
                                %                             burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
                            end
                            %                         patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)]/2,...
                            %                             'g', 'FaceAlpha', 0.3,'EdgeColor','none');
                        end
                        
                        plot(burstT,burstDuration*10^3,'r-');
                        
                        %                         [hAx, hLine1, hLine2] = plotyy(onsets(1:end-1),freq,burstT,burstDuration*10^3);
                        %                         linkaxes(hAx,'x');
                        %                         hold(hAx(1), 'on');
                        %                         hold(hAx(2),'on');
                        
                    end
                end
                
                %% overlay shock onsets
                plot(ax(2),(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
                ylim([0 60]);
                %                 line((vr.shockIndex-1)*vr.si,'Parent',ax(2))
                %                 axes(ax(2));
                %                 arrayfun(@(x)line([x x],get(hAx(1),'YLim'),'Color',[1 0 0]),(vr.shockIndex-1)*vr.si);
            else
                arrayfun(@(x)line([x x],vrRange/2,'Color',[.5 0 .5]),(vr.curatedIndex-1)*vr.si);
                
                %% overlay episode information
                %         epi = editted_epi;
                for i=1:length(epi)
                    burst = epi{i}.burst;
                    for j=1:length(burst)
                        if length(burst{j}.spikes)>1
                            burstPatch=[burst{j}.spikes(1), burst{j}.spikes(end)];
                        elseif length(burst{j}.spikes) == 1
                            burstPatch=burst{j}.spikes(1)+[-0.5 0.5]*10^-3;
                        end
                        patch([burstPatch, burstPatch(end:-1:1)],[vrRange(1), vrRange(1), vrRange(2), vrRange(2)]/2,...
                            'g', 'FaceAlpha', 0.3,'EdgeColor','none');
                    end
                    
                    onsets = epi{i}.onsets;
                    freq = epi{i}.freq;
                    
                    for k=1:length(freq)
                        text(mean(onsets(k:k+1)), vrRange(2)/3, [num2str(freq(k), '%0.1f')],...
                            'HorizontalAlignment','center','Parent',ax(2));
                    end
                    
                end
                
                %% overlay shock onsets
                plot(ax(2),(vr.shockIndex-1)*vr.si, zeros(size(vr.shockIndex)), 'r*');
            end
            h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.2 .2 .2]);
        end
    end

    function hotspotMap_callback(src,evt)
        sm_imd = convn(imd,ones(3)/9);
        norm_sm_imd = zeros(dim);
        cumPosdFF = zeros(dim(1:2));
        for i = 1:dim(1)
            for j = 1:dim(2)
                temp = normalize(sm_imd(i,j,:));
                norm_sm_imd(i,j,:) = temp;
                aho = squeeze(temp);
                cumPosdFF(i,j) = sum(aho(find(aho>0)));
            end
        end
        
%         norm_aveImg = imNormalize999(aveImg);
%         level = graythresh(norm_aveImg);
%         mask = im2bw(norm_aveImg,level);
        % figure;
        % imshow(mask);
        % figure;
        % imagesc(norm_aveImg);
        
        figure;
        imagesc(cumPosdFF.*norm_aveImg);
        axis image; axis off;
    end

    function slider_callback(src,evt)
        v = get(src,'Value');
        z = round(v*(dim(4)-1))+1;
        %           z = ceil(v);
        axes(ax_movie);
        imagesc(imd(:,:,:,z)); axis off; axis image;
        
        %         axes(ax_im)
%         imagesc(eData.avgImg(:,:,z)); axis off; axis image
%         colormap(gray)
%         freezeColors
        set(h_text,'String', ['z=',num2str(z)]);
        %         set(h_timeline,'XData',im_t(t)*[1 1]);
        
    end

%     function play_callback(src,evt)
%         startT = t;
%         playing = true;
%         for t=startT:dim(3)
%             axes(ax_movie);
%             %             imagesc(imd(:,:,t),clims); axis off; axis image;
%             imagesc(imd(:,:,t)); axis off; axis image;
%             set(h_text,'String', ['t=',num2str(t)]);
%             set(h_slider,'Value',(t-1)/dim(3));
%             set(h_timeline,'XData',im_t(t)*[1 1]);
%             
%             pause(0.03);
%             if ~playing
%                 break;
%             end
%         end
%     end

%     function stop_callback(src,evt)
%         playing = false;
%         stopT = t;
%         axes(ax_movie);
%         imagesc(imd(:,:,t)); axis off; axis image;
%         set(h_text,'String', ['t=',num2str(t)]);
%         axes(ax(2));
%         set(h_timeline,'XData',im_t(t)*[1 1]);
%         
%     end

    function updateRadius(src,evt)
        radius = str2num(get(src, 'String'));
    end

    function activateCursor(src,evt)
        if get(src,'Value') == 1
            activeCursorFlag = 1;
        elseif get(src,'Value') == 0
            activeCursorFlag = 0;
        end
    end

    function inactivateCursor(src, evt)
        activeCursorFlag = 0;
        set(h_checkboxCursor,'Value',0);
    end

    function plotTimecourse(src,evt)
        axes(ax_im);
        [BW] = roipoly;
        axes(ax(1));
        flat_imd = reshape(imd, [],dim(3));
        ts = mean(flat_imd(find(BW),:),1);
        plot(im_t,ts);
    end

    function trlNumTxtCallback(hObj,evt)
        trl = str2double(get(hObj,'String'));
        if numel(trl) > 1
            disp('Can only display 1 trial at a time')
            trl = trl(1);
        elseif trl > size(eData.trialSegData,1)
            disp(['Trial # outside range; max trial = ' size(eData.trialSegData,1)])
        end
    end

    function trialModeRadioCallback(hObj,evt)
        if get(hObj,'value') == get(hObj,'Max')
            trlModeFlag = 1;
        else
            trlModeFlag = 0;
        end   
    end

    function mouseMove(src,evt)
        if activeCursorFlag
            C = get(ax_movie, 'CurrentPoint');
            xrange = get(ax_movie,'XLim');
            yrange = get(ax_movie,'YLim');
            if  xrange(1)+radius < C(1,1) && C(1,1)< xrange(2)-radius && yrange(1)+radius < C(1,2) && C(1,2)< yrange(2)-radius
                currX = floor(C(1,1));
                currY = floor(C(1,2));
                title(ax_movie, ['(X,Y) = (', num2str(currX),', ', num2str(currY), '), radius = ', num2str(radius)]);
                %                 set(h_rect,'XData',currX+[-radius radius radius -radius -radius],'YData',currY+[-radius -radius radius radius -radius]);
                %                 BW = zeros(dim(1:2));
                %                 BW(currY-radius:currY+radius,currX-radius:currX+radius) = 1;
                axes(ax(1));
                cla
                %                 flat_imd = reshape(imd, [],dim(3));
                cursorPos = [C(1,1), C(1,2)];
                [cellInd,xyzOffset] = FindClosestCellInPlane(eData.cells.info,cursorPos,z);
                distToClosestCell = sqrt(sum((0.406*xyzOffset(1:2)).^2));
                ts = eData.cells.dFF(cellInd,:);
                if distToClosestCell > 15
                    disp('Nearest cell > 15 microns away')
                    axes(ax(1)), cla
                    plot(nan*zeros(length(ts),1),nan*zeros(length(t),1),'.')
                elseif trlModeFlag
                    axes(ax(1))
                    cla
                    plot(eData.trialSegData(1).cells.time, eData.trialSegData(trl).cells.dFF(cellInd,:));
                    box off
                    set(gca,'tickdir','out')
                    xlim([-10 40])
                    ylim([-inf inf])
                    title(['Trial # ' num2str(trl)])
                    
                    axes(ax(2))
                    cla
                    plot(eData.trialSegData(1).time,eData.trialSegData(trl).smooth.burst(:,1))
                    box off
                    set(gca,'tickdir','out')
                    ylim(vrRange)
                    title(['Trial # ' num2str(trl)])
                elseif trlModeFlag == 0
                    axes(ax(1))
                    cla
                    plot(eData.cells.time,eData.cells.dFF(cellInd,:),'k');
                    hold on
                    plot(eData.cells.time(eData.stack.inds.shock), 0.25*ones(size(eData.stack.inds.shock)),'r*'),
                    title(ax_movie, ['(X,Y) = (', num2str(currX),', ', num2str(currY), '), cell idx = ' num2str(cellInd)]);
                    disp(cellInd)
                    
                    axes(ax(2))
                    cla
                    plot(eData.t(1:50:end), eData.smooth.burst(1:50:end,1))
                    box off
                    set(gca,'tickdir','out')
                    hold on
                    plot(eData.t(eData.stim.inds),1000*ones(size(eData.stim.inds)),'r*')
                end
            end
        end
    end
end

