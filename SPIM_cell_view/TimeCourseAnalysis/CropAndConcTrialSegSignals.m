function y = CropAndConcTrialSegSignals(varargin)
%CropAndConcTrialSegSignals  Crops segmented signals and strings together by
% linking ends
% 
% y = CropAndConcTrialSegSignals(trialSegData,fieldname)
% y = CropAndConSegSignals(..., eventIdx, nPreEventPts, nPosEventPts);
% 
% Inputs: 
% trialSegData - trial segmented data as output by AnalyzeEphysData
% fieldName - name of the field that contains the signal of interest
% eventIdx - event wrt to which to crop
% preEventPts - number of pts before eventIdx to crop the signal from
% postEventPts - number of points after eventIdx to crop up to
% Outputs:
% y - Concatenated signal

if nargin == 2
    if ~isstr(varargin{2})
        error('2nd input must be a string!')
    end
    eventIdx = 1;
    preEventPts = 0;
    postEventPts = [];
elseif nargin == 5
    eventIdx = varargin{3};
    preEventPts = varargin{4};
    postEventPts = varargin{5};
else
    error('Number of inputs must be 2 or 5!')
    return
end

trialSegData = varargin{1};
fn = varargin{2};

% dotInds = strfind(fn,'.');
% blah = [];
% fnC = cell(1);
% for jj = 1:numel(dotInds)+1
%    if jj == 1
%        fnC{jj} = fn(1:dotInds(jj));
%    elseif jj == (numel(dotInds)+1);
%        fnC{jj} = fn(dotInds(jj-1)+2:length(fn));
%    else
%        fnC{jj} = fn(dotInds(jj-1)+2:dotInds(jj)-1);
%    end   
% end

blah = eval(['trialSegData(1).' fn]);
if isempty(postEventPts)
    postEventPts = size(blah,2)-1;
end

y = [];
inds = eventIdx - preEventPts : eventIdx + postEventPts;
for trial = 1:length(trialSegData)
    vn = ['trialSegData(' num2str(trial) ').' fn];
%     var  = detrend(eval(vn)')';  
     var  = eval(vn); 
    if trial > 1
         offset = var(:,1) - y(:,end);
         offset = repmat(offset,1,numel(inds));
         y = [y, var(:,inds) - offset];
    else
        y = [y, detrend(var(:,inds)')'];
    end
   
end
 y = detrend(y')';
end

