% AnalyzeEphysData - Wxy computed for stim-evoked trials
% The script makes the following assumptions
% (i)   Equal number of stimuli are delivered to both sides
% (ii)  All the stimulus pulses have the same width
% (iii) Pulse widths are the same on both sides
% (iv)  Inter-pulse-interval is constant and the same on both sides
% (v)   If stim pulse is biphasic, the switch in polarity occurs half-way
%       through the pulse
%
 data = mkEphysLoad(); % Use this option to interactively select a file


%% Input parameters

stim.isolGain               = [0 50]; %[Stim1, Stim2]
data.samplingRate           = 6000;
hpFilt                      = 300; % Highpass to keep only spike-like transients
lpFilt                      = 100; % Lowpass after half-wave rectification or abs value

Wxy.dj                      = 1/24; % wavelet scale resolution
stringency                  = 3; % For XWT
phaseType                   = 'all'; %('alt', 'synch', or 'both')

preStimPeriod               = 10;
postStimPeriod              = 100; % In seconds (length of trace to evaluate after stimulus)
postStimRefractoryPeriod    = 4e-3; % Time period after stim in which to ignore events (due to artifactual influence)

ePhysChannelToAnalyze       = [1 2]; %(Enter 1 or 2; this corresponds to Anaolog Input 0 or 2 respectively on NI card)

stim.pulseWidth             = 1; % Default value; will be overwritten by estimated value later on
stim.minExpInt              = 30;
slopeThreshold              = 5; % In units of std (for detecting stimulus onsets)
ampThreshold                = 5; % In units of std (additional criterion for stimulus detection)

totTimePeriodToConsider     = inf; % In minutes, after start of expt; inf --> entire trace

xLim                        = [-100 1000];
yLim                        = [-0.2 0.2];
yLim_smooth                 = [-inf inf];
slowestExpectedSwimFreq     = 15; % In Hz

stringency1                 = 1; % For XWT
stringency2                 = 1; % For XWT
onDetThr                    = 15; % For detection of onset (zscore)
phaseCutoff                 = 2*pi/3;
% log2Thr                   = 5; % Threshold for max swim power


%% Truncating expt time in the event responses deteriorate
if ~isinf(totTimePeriodToConsider)
    data_full = data;
    timeIdx = round(min(ceil(totTimePeriodToConsider)*60*data.samplingRate,data.t(end)*data.samplingRate)); % Rounding up to nearest min and then to sampled pt
    fNames = fieldnames(data);
    for jj = 1:length(fNames)-1
        fn  = num2str(fNames{jj});
        data.(fn)(timeIdx+1:end) =[];
    end
end

%% Append some variable to data
data.preStimPeriod = preStimPeriod;
data.postStimPeriod = postStimPeriod;
data.ePhysChannelToAnalyze = ePhysChannelToAnalyze;
data.lpFilt = lpFilt;
data.hpFilt  = hpFilt;
data.stim = stim;
data.Wxy = Wxy;
data.Wxy.slowestExpectedSwimFreq = slowestExpectedSwimFreq;

% data.stim.activeCh = data.patch3; 

%% Input check & stim info
if numel(data.stim.isolGain) ~= 2
    errordlg('The variable data.stimIsolGain must contain exactly 2 entries; enter 0 if not using a stim channel')
end
data = GetStimInfo(data);
stim  = data.stim;
data.postStimPeriod = min(data.postStimPeriod,stim.interval.min);


%% Plot-checking Stim Detection
figure('Name', 'Checking stim detection')
colMapInds = round(linspace(10,50,numel(stim.activeCh)));
fh = figure; cMap = colormap(jet); close(fh)
combStimChan = [];   
for sc = 1:numel(stim.activeCh)
  combStimChan = cat(1,combStimChan,abs(data.(['stim' num2str(stim.activeCh(sc))])));
  plot(data.t,combStimChan(sc,:),'color',cMap(colMapInds(sc),:))
  hold on
  stimInds = stim.inds(stim.amps(stim.activeCh(sc),:)~=0)+1;
  plot(data.t(stimInds),combStimChan(sc,stimInds),'o','color',cMap(colMapInds(sc),:))
end
drawnow

%% Deartifacting and Smoothing Signals
tic
data.artless = zeros(numel(data.ePhysChannelToAnalyze),length(data.t));
data.smooth.burst = data.artless;
dt = (1/data.samplingRate);
for ch = 1:numel(data.ePhysChannelToAnalyze);
    disp(['Deartifacting channel # ' num2str(ch) '...'])    
    temp = data.(['ch' num2str(ch)]);
    temp = detrend(temp);
    data.artless(ch,:) = DeartifactWithGaussInterp(temp,dt,stim.inds,15e-3,500e-3,80);
    data.artless(ch,:) = chebfilt(data.artless(ch,:),dt,data.hpFilt,'high');
    data.artless(ch,:) = BlankArtifactZero(data.artless(ch,:),dt,stim.inds, 5e-3,5e-3);
    disp(['Smoothing channel # ' num2str(ch) '...'])
    data.smooth.burst(ch,:) = SmoothVRSignals(data.artless(ch,:),dt,data.lpFilt);
end
data.smooth.burst = data.smooth.burst';

kerLen = round(20e-3*data.samplingRate);
ker = ones(kerLen,1)/kerLen;
blah = max(data.artless,[],1);
sm = conv(blah(:),ker(:));
sm = sm(1:length(data.artless));

kerLen = round(1*data.samplingRate);
ker = exp(-4*linspace(0,1,kerLen));
% ker = gausswin(kerLen);
ker = ker/sum(ker);
% data.smooth.swim = conv(max(data.smooth.burst,[],2),ker);
data.smooth.swim = conv(max(data.artless,[],1),ker);
data.smooth.swim = ZscoreByHist(data.smooth.swim(1:length(data.t)));
% data.smooth.swim = log(data.smooth.swim - min(data.smooth.swim) + 1);
% data.smooth.swim = ZscoreByHist(chebfilt(data.smooth.swim,1/data.samplingRate,0.5,'low'));
toc

%% Computing & Plotting Sparse Spectrogram for Entire Signal Length
% disp('Computing & plotting sparse spectrogram')
% tic
% colorMap = jet;
% SS = zeros(length(data.t),2,numel(data.ePhysChannelToAnalyze));
% S = cell(numel(data.ePhysChannelToAnalyze),1);
% for ch = 1:numel(data.ePhysChannelToAnalyze)
%     [~,SS(:,:,ch)] = ComputeSparseSpectrogram((data.smooth.burst(:,ch)).^0.5, ...
%         data.t,3,[slowestExpectedSwimFreq lpFilt],colorMap,'n');
%     SS = BlankArtifactZero(SS,dt,stim.inds, 10e-3, 5e-3);
%     inds = find(SS(:,1,ch));
%     S{ch}(:,1) = SS(inds,1,ch);
%     S{ch}(:,2) = SS(inds,2,ch).^2;
%     S{ch}(:,3) = inds;
%     S{ch}(:,4) = inds*dt;
%
%     %%%################ Plot Sparse Spectrogram ###############
%
%     %     colVals = MapValsToColorsVer2(sqrt(S{ch}(:,2)),colorMap);
%     %     figure('Name', 'Mimal Spectrogram')
%     %     subaxis(2,1,1,'mb',0.1,'mt', 0.1)
%     %     hold on
%     %     set(gca,'color','k','tickdir','out', 'xtick',[]), box off
%     %
%     %     stimSig = abs(sum(combStimChan,1));
%     %     plot( data.t, stimSig*max(data.smooth.burst(:,ch))/max(stimSig),'r')
%     %     hold on
%     %     plot(data.t,data.smooth.burst(:,ch),'color','g')
%     %
%     %     title(['Minimal Spectrogram: ' num2str(ch)])
%     %     ylabel('Amp (z-scores)')
%     %     ylim([-inf inf]), xlim([data.t(1) data.t(end)])
%     %     hold off
%     %     subaxis(2,1,2,'mt',0.001,'mb',0.1)
%     %     hold on
%     %     set(gca,'color','k','tickdir','out'), box off
%     %     ylabel('Frequency (Hz)'), xlabel('Time (sec)')
%     %     axis([data.t(1) data.t(end) min(S{ch}(:,1)) max(S{ch}(:,1))])
%     %     % ylim(freqRange), xlim([time(1) time(end)])
%     %     for jj = 1:size(S{ch},1)
%     %         plot(S{ch}(jj,4),S{ch}(jj,1),'color',colVals(jj,:),'marker','o','markersize',5,'markerfacecolor',colVals(jj,:))
%     %     end
%     %     colormap(colorMap)
%     %     ch = colorbar;
%     %     set(ch,'location','NorthOutside')
%     toc
% end

%% Getting information about all swim episodes & creating pyData mat file.
data = GetSwimInfo(data,'swimDetThr',10,'swimCountWin',30);
slashInds = strfind(data.filename, '\');
% ephysDir = data.filename(1:slashInds(end)-1);
[ephysDir,ephysFileName] = fileparts(data.filename);

disp('Creating matfile object and saving data...')
pyFileName = ['pyData_' ephysFileName '.mat'];
pyDataPath = fullfile(ephysDir,pyFileName);
tic
pyData = matfile(pyDataPath,'Writable',true);
pyData.data = data;
toc

%% Segmenting Data by Trials
%### Still not generalized to multiple stim channels
disp('Segmenting data by trials...')
stim = data.stim;
dt = 1/data.samplingRate;
data.raw = [];
for ch = 1:numel(data.ePhysChannelToAnalyze)
    data.raw(ch,:) = data.(['ch' num2str(data.ePhysChannelToAnalyze(ch))]);
end
stimInds = unique(stim.inds);
[blah,incompleteSegInds] = SegmentDataByEvents(data.raw,dt,stimInds,data.preStimPeriod,data.postStimPeriod);
if isempty(incompleteSegInds)
    incompleteSegInds = nan;
end
trialSegData = struct('raw',blah);
% stim.inds(incompleteSegInds)  = []; % Ignoring incomplete segments
% stim.times(incompleteSegInds)  = [];
% stim.amp(:,incompleteSegInds) = [];

blah = SegmentDataByEvents(data.artless,dt,stimInds,data.preStimPeriod,data.postStimPeriod);
[trialSegData(:).artless] = deal(blah{:});

smooth.sine = SegmentDataByEvents(data.smooth.burst,dt,stimInds,data.preStimPeriod,data.postStimPeriod);
smooth.swim = SegmentDataByEvents(data.smooth.swim,dt,stimInds,data.preStimPeriod,data.postStimPeriod);
swimRate_ts = SegmentDataByEvents(data.swim.rate,dt, stimInds, data.preStimPeriod, data.postStimPeriod);
blah = SegmentDataByEvents(data.swim.bool,dt, stimInds, data.preStimPeriod, data.postStimPeriod);
blah2 = SegmentDataByEvents(data.swim.boolOn,dt,stimInds,data.preStimPeriod,data.postStimPeriod);
for trial = 1:numel(stimInds)
    trialSegData(trial).smooth.burst = smooth.sine{trial};
    trialSegData(trial).smooth.swim = smooth.swim{trial};
    trialSegData(trial).swim.rate = swimRate_ts{trial};
    trialSegData(trial).stim.ind = stimInds(trial);
    trialSegData(trial).stim.grp = find(unique(stim.amps(stim.activeCh,:))==stim.amps(stim.activeCh,trial));
    trialSegData(trial).stim.time = stim.times(trial);
    trialSegData(trial).stim.amps = stim.amps(stim.activeCh(1),trial);
    trialSegData(trial).swim.bool = blah{trial};
    trialSegData(trial).swim.boolOn = blah2{trial};
    if (~isempty(intersect(trial,incompleteSegInds)))
        preStimPts_this = find(data.t==stim.times(trial))-1;
        trialSegData(trial).time = ((0:length(trialSegData(trial).raw)-1) - preStimPts_this)*dt;
    end
end
preStimPts  = round(data.preStimPeriod*data.samplingRate);
completeSegInds = setdiff((1:numel(stimInds)),incompleteSegInds);
trialSegData(completeSegInds(1)).time = ((0:length(trialSegData(completeSegInds(1)).raw)-1) - preStimPts)*dt;
data.incompleteSegInds = incompleteSegInds;
disp('Segmented data into trials!')

%% XW on Peri-Stimulus Portion of Trial Segmented Smoothed data
stim = data.stim;
slowestExpectedSwimFreq = data.Wxy.slowestExpectedSwimFreq;
frqRange  = [slowestExpectedSwimFreq 0.7*data.lpFilt]; % For XWT
data.Wxy.frqRange = frqRange;
if numel(data.ePhysChannelToAnalyze) == 2 % Only do XWT when 2 good channels present
    disp('Computing XW Spectra...')
    tic
    [~,~,sigmax] = ZscoreByHist(data.smooth.burst(:,1));
    [~,~,sigmay] = ZscoreByHist(data.smooth.burst(:,2));
    sigmaxy = (sigmax + sigmay)/2;
    plotOrNot = 0;
    
    subSamplingRate = max(frqRange)*4;
    subInt = floor(data.samplingRate/subSamplingRate);
    % Time period before stim
    pow = zeros(numel(stim.inds),1);
    
    for trial = 1:numel(stim.inds);
        if ~isempty(intersect(trial,incompleteSegInds))
            timeTrl  = trial;                        
        else
            timeTrl = completeSegInds(1);            
        end
        fpt = max(min(find(trialSegData(timeTrl).time >= -5)),1);
        if isempty(fpt)
            fpt = 1;
        end
        lpt = min(min(find(trialSegData(timeTrl).time >= 10)),length(trialSegData(timeTrl).time)); % Time period after stim
        if isempty(lpt)
            lpt = length(trialSegData(timeTrl).time);
        end       
        trialSegData(timeTrl).Wxy.time  = trialSegData(timeTrl).time(fpt:lpt);
        trialSegData(timeTrl).Wxy.time = (trialSegData(timeTrl).Wxy.time(1:subInt:end));
        fpt2 = min(find(trialSegData(timeTrl).Wxy.time >= -0.2));
        lpt2 = min(find(trialSegData(timeTrl).Wxy.time >= 1.2)); % Time period after stim to which swim parameter estimates
        tInds  = find((trialSegData(timeTrl).Wxy.time > 2e-3) & (trialSegData(timeTrl).Wxy.time < 0.5));
        if isempty(tInds)
            tInds = [1 length(trialSegData(timeTrl).Wxy.time)-1];
        end

        disp(['Trial: ' num2str(trial)])
        blah = trialSegData(trial).smooth.burst;
        blah = blah(fpt:lpt,:);
        blah = blah(1:subInt:end,:);
        if trial == 61
            pause(0.1)
        end      
        trialSegData(trial).Wxy.xy = [blah(:,1) blah(:,2)];
        imf = MyEMD(trialSegData(trial).Wxy.xy,3);
        blah = blah - imf(3).comp;
        [trialSegData(trial).Wxy.coeffs, period] = ...
            my_xwt(blah(:,1),blah(:,2),trialSegData(timeTrl).Wxy.time,frqRange, ...
            stringency1,stringency2,plotOrNot,sigmaxy);
        
        swimBool = trialSegData(trial).swim.bool(fpt:subInt:lpt);
        swimBool = swimBool(1:size(trialSegData(trial).Wxy.coeffs,2));
        
        %# Separating alternating from synchronous components
        [Wxy_alt, Wxy_sync] = deal(trialSegData(trial).Wxy.coeffs);
        
        Wxy_alt(abs(angle(trialSegData(trial).Wxy.coeffs)) < phaseCutoff) = 0;
        [~,trialSegData(trial).Wxy.tvFreq_alt] = instantaneouswavefreq(Wxy_alt,1./period);
        [~,trialSegData(trial).Wxy.tvPow_alt] = instantaneouswavepow(Wxy_alt);
        trialSegData(trial).Wxy.tvPow_alt = sqrt(trialSegData(trial).Wxy.tvPow_alt);
        [~, trialSegData(trial).Wxy.tvPhase_alt] = instantaneouswavephase(Wxy_alt);
        trialSegData(trial).Wxy.tvFreq_alt = trialSegData(trial).Wxy.tvFreq_alt.*swimBool';
        trialSegData(trial).Wxy.tvPow_alt = trialSegData(trial).Wxy.tvPow_alt.*swimBool';
        trialSegData(trial).Wxy.tvPhase_alt = trialSegData(trial).Wxy.tvPhase_alt.*swimBool';
        
        Wxy_sync(abs(angle(trialSegData(trial).Wxy.coeffs)) >= phaseCutoff) = 0;
        [~,trialSegData(trial).Wxy.tvFreq_sync] = instantaneouswavefreq(Wxy_sync,1./period);
        [~,~, trialSegData(trial).Wxy.tvPow_sync] = instantaneouswavepow(Wxy_sync);
        trialSegData(trial).Wxy.tvPow_sync = sqrt(trialSegData(trial).Wxy.tvPow_sync);
        [~, trialSegData(trial).Wxy.tvPhase_sync] = instantaneouswavephase(Wxy_sync);
        trialSegData(trial).Wxy.tvFreq_sync = trialSegData(trial).Wxy.tvFreq_sync.*swimBool';
        trialSegData(trial).Wxy.tvPow_sync = trialSegData(trial).Wxy.tvPow_sync.*swimBool';
        trialSegData(trial).Wxy.tvPhase_sync = trialSegData(trial).Wxy.tvPhase_sync.*swimBool';
        
        if trial ==1
            trialSegData(trial).Wxy.freqVec = 1./period;
        end
        %# Finding max freq for swim episode from global power spectrum
        gps = mean(abs(Wxy_alt(:,tInds)),2);
        gps = gps/max(gps);
        pks = GetPks(gps);
        pks(gps(pks)< 0.1)=[];
        if ~isempty(pks)
            trialSegData(trial).Wxy.maxFreq = trialSegData(1).Wxy.freqVec(pks(1));
        else
            trialSegData(trial).Wxy.maxFreq = nan;
        end
        %# Finding max pow attained by swim episode
        trialSegData(trial).Wxy.maxPow = max(trialSegData(trial).Wxy.tvPow_alt(fpt2:lpt2));
        if isempty(trialSegData(trial).Wxy.maxPow)
            trialSegData(trial).Wxy.maxPow = 0;
        end     
        pow(trial) = round(trialSegData(trial).Wxy.maxPow);
    end
    disp('Done!')
    toc
    
    % Set Freq & Pow to Zero for Weak Trials
    stimAmps  = unique(stim.amps(stim.activeCh,:));    %
    %     chancePow = mean(pow(stim.amp(2,:)== stimAmps(2)));
    %     thr  = 2*chancePow; % Playing a bit safe
    %     weakInds = find(pow <= thr);
    
    fh = figure('Name','Separating weak and strong swims by power');
%     [kerDen,tt] = ssvkernel(pow);
%     plot(tt, kerDen)
    plot(stim.amps(stim.activeCh,:),pow,'.')
    box off
    title('Click at 2 locations on y-axis to mark the start and end of weak swim episodes')
    xlabel('Stim amp'), ylabel('XW pow')
    spacer = median(diff(unique(stim.amps(:))));
    xlim([min(stim.amps(stim.activeCh,:))-spacer max(stim.amps(stim.activeCh,:))+ spacer])
    set(gca,'tickdir','out','xtick',stimAmps)
    [~,chancePow] = ginput(2);
    close(fh)
    weakInds = find(pow < chancePow(2) & pow > chancePow(1));
    strngInds = find(pow> chancePow(2));
    nonSwmInds = find(pow <= chancePow(1));
    trialSegData(1).Wxy.swimBool = ones(size(trialSegData));
    trialSegData(1).Wxy.swimBool(nonSwmInds)= false;
end

%% Determining response onset latency
disp('Determining response onset latencies for trials')
startTime = 1.5e-3; % Start looking this much time after stim
endTime = 0.5; % Stop looking after stim when this much time has passed
kerLen = round((1/lpFilt)*data.samplingRate);
ker = exp(-4*(linspace(0,1,kerLen)));
ker = ker/sum(ker);
for trial = 1:numel(stimInds)
    completeSegInds = setdiff(1:length(trialSegData),data.incompleteSegInds);
    if any(trial == data.incompleteSegInds);
        timeTrl = trial;
    else
        timeTrl = completeSegInds(1);
    end    
 inds = find((trialSegData(timeTrl).time >= startTime) & (trialSegData(timeTrl).time <= endTime));
    blah = zeros(size(trialSegData(trial).artless));
    temp1 =[]; temp2 =[];
    for ch = 1:numel(data.ePhysChannelToAnalyze)
        temp1 = sqrt(conv((trialSegData(trial).artless(:,ch)).^2,ker(:)));
        blah(:,ch) =  temp1(1:length(trialSegData(trial).artless));
        blah(:,ch) = (blah(:,ch) - mean(blah(:,ch)))* ...
            max(trialSegData(trial).smooth.burst(:,ch))/max(blah(:,ch));
        temp2 = blah(inds,ch);
        onInd = find((temp2(3:end) > onDetThr) & (temp2(2:end-1) > onDetThr) & (temp2(1:end-2)> onDetThr),1,'first');
        if ~isempty(onInd)
            trialSegData(trial).swim.onset(ch) = (onInd*dt) + startTime;
        else
            trialSegData(trial).swim.onset(ch) = 0;
        end
    end
end

%% Segmenting swim parameters by trial, and separating stim-evoked (se) episodes from all others
disp('Segmenting swim parameters by trial ')
swim = data.swim;
startTime = 0;
for trial = 1:numel(stimInds)
    %# In a later implementation, I can simplify this process, by
    %  checking at this point if the current trial is successful or not
    %  using swimBool vector, and then assign values of zero accordingly
    
    %# Find all swim episodes occurring after this stimulus and the next
    swimInds = find((swim.times >= (trialSegData(trial).stim.time + startTime)) & ...
        (swim.times < (trialSegData(trial).stim.time + stim.interval.min-1)));
    
    if ~isempty(swimInds)
        trialSegData(trial).swim.startInds = data.swim.startInds(swimInds);
        trialSegData(trial).swim.endInds = data.swim.endInds(swimInds);
        trialSegData(trial).swim.dur = data.swim.dur(swimInds);
        trialSegData(trial).swim.amp = data.swim.amp(swimInds);
        trialSegData(trial).swim.freq = data.swim.freq.max(swimInds);
        trialSegData(trial).swim.distFromLastStim = data.swim.distFromLastStim(swimInds);
    else
        [trialSegData(trial).swim.startInds, trialSegData(trial).swim.endInds,...
            trialSegData(trial).swim.dur, trialSegData(trial).swim.amp,...
            trialSegData(trial).swim.freq, trialSegData(trial).swim.distFromLastStim]...
            = deal(nan);
    end
    
    %# Find swim episode occurring within a small time window after stim, and that is likely stim-evoked
    swimInd = find((swim.times >= trialSegData(trial).stim.time + startTime)...
        & (swim.times <= trialSegData(trial).stim.time + endTime),1, 'first');
    if ~isempty(swimInd)
        trialSegData(trial).swim.dur_se = data.swim.dur(swimInd);
        trialSegData(trial).swim.amp_se = data.swim.amp(swimInd);
        trialSegData(trial).swim.freq_se = data.swim.freq.max(swimInd);
    else
        [trialSegData(trial).swim.dur_se, trialSegData(trial).swim.amp_se,...
            trialSegData(trial).swim.freq_se] = deal(nan);
    end
end
disp('Updating matfile with trialSegData...')
tic
pyData.trialSegData = trialSegData;
toc

%% Manual Detection of Stim-Evoked Swim Bursts for Determining Swim Freq & Onset
ManuallyDetectStimEvokedBursts;

%% Overriding Autodetermined Values in the Event of Manual Detection.
if strcmpi(answer,'Yes')
    data.swim.freq.max = freqVec;
    data.swim.onset = onsetVec;
end

%% Segmenting Data by Stimulus Condition
disp('Segmenting data by stimulus conditions')
nConditions = numel(unique(stim.amps(1,:)) * numel(unique(stim.amps(2,:))));
% stim.conditions = cell(size(stim.amps,1),1);
% stim.conditions{1} = unique(stim.amps(1,:));
% stim.conditions{2} = unique(stim.amps(2,:));
% stimSegData = struct;
% stimSegData(numel(stim.conditions{1}),numel(stim.conditions{2})).all = [];
% stim.indMat = stimSegData(1,1).all;
% stimTimesMat = stimSegData(1,1).all;
% 
% nTrialMat = zeros(numel(stim.conditions{1}),numel(stim.conditions{2}));
% for one = 1:numel(stim.conditions{1});
%     for two = 1:numel(stim.conditions{2})
%         trialInds = find(stim.amps(1,:) == stim.conditions{1}(one) &  stim.amps(2,:) == stim.conditions{2}(two));
%         stimSegData(one,two).all = trialSegData(trialInds);
%         stimSegData(one,two).trialInds = trialInds;
%         stim.indMat{one,two} = [one, two];
%         stimTimesMat{one,two} = stim.times(trialInds);
%         nTrialMat(one,two) = numel(trialInds);
%     end
% end
data.trialSegData = trialSegData;
[data,stimSegData] = GetStimSegData(data,trialSegData);
stim = data.stim;
nTrialMat = data.stim.nTrialMat;

%% Grouping Swim Parameters
disp('Creating swimParams structure variable')
swimParams = struct;
[swimParams.onset.all, swimParams.freq.all, swimParams.dur.all, ...
    swimParams.speed.all] = deal(zeros(numel(stim.conditions{1}), ...
    numel(stim.conditions{2}), max(nTrialMat(:)), numel(data.ePhysChannelToAnalyze)));
[swimParams.Wxy.freq.all, swimParams.Wxy.pow.all] = ...
    deal(zeros(numel(stim.conditions{1}), numel(stim.conditions{2}), ...
    max(nTrialMat(:))));
[swimParams.onset.mu, swimParams.onset.sigma, swimParams.freq.mu, ...
    swimParams.freq.sigma, swimParams.dur.mu, swimParams.dur.sigma] = ...
    deal(zeros(numel(stim.conditions{1}), numel(stim.conditions{2}), ...
    numel(data.ePhysChannelToAnalyze)));
[swimParams.Wxy.freq.mu, swimParams.Wxy.freq.sigma, ...
    swimParams.Wxy.pow.mu, swimParams.Wxy.pow.sigma] = ...
    deal(zeros(numel(stim.conditions{1}), numel(stim.conditions{2})));

for one = 1:numel(stim.conditions{1})
    for two = 1:numel(stim.conditions{2})
        blah = stimSegData(one,two).all;
        nTrials = size(stimSegData(one,two).all,1);
        for trial = 1:nTrials
            swimParams.Wxy.freq.all(one,two,trial) = blah(trial).Wxy.maxFreq;
            swimParams.Wxy.pow.all(one,two,trial) = blah(trial).Wxy.maxPow;
            swimParams.freq.all(one,two,trial) = ...
                blah(trial).swim.freq_se;
            
            for ch = 1:numel(data.ePhysChannelToAnalyze)
                swimParams.onset.all(one,two,trial,ch) = ...
                    blah(trial).swim.onset(ch);
            end % ch loop
            swimParams.dur.all(one,two,trial) = ...
                blah(trial).swim.dur_se;
        end %trial loop
        fNames = fieldnames(swimParams);
        for fn = 1:length(fNames)
            cfn = fNames{fn};
            if strcmpi(cfn,'Wxy')
                fNames_sub = fieldnames(swimParams.(cfn));
                for fn_sub = 1:length(fNames_sub)
                    cfn_sub = fNames_sub{fn_sub};
                    blah = squeeze(swimParams.(cfn).(cfn_sub).all(one,two,:));
                    blah(blah == 0) = [];
                    swimParams.(cfn).(cfn_sub).mu(one,two) = mean(blah);
                    swimParams.(cfn).(cfn_sub).sigma(one,two) = std(blah);
                end
            else
                blah = squeeze(swimParams.(cfn).all(one,two,:,:));
                for ch = 1:size(blah,2)
                    temp = blah(:,ch);
                    temp(temp==0) = [];
                    swimParams.(cfn).mu(one,two,ch) = mean(temp);
                    swimParams.(cfn).sigma(one,two,ch) = std(temp);
                end % ch loop
            end
        end % fieldnames loop
        
    end % stim2 conditions loop
end % stim1 conditions loop

%% Creating & Plotting Wxy_avg for all stim conditions
% stimSegData.avg.swimDist  = zeros(numel(stim.conditions{1}),numel(stim.conditions{2}));
if numel(data.ePhysChannelToAnalyze) == 2
    trialArray_avgWxy = cell(numel(stim.conditions{1}),numel(stim.conditions{2}));
    for one = 1:numel(stim.conditions{1});
        for two = 1:numel(stim.conditions{2})
            blah = stimSegData(one,two).all;
            trlInds = stimSegData(one,two).trialInds;
            [ints,intInds] = intersect(trlInds,data.incompleteSegInds); 
            trlInds(intInds) = [];
            temp = zeros(size(trialSegData(trlInds(1)).Wxy.coeffs));
            for sc  = 1:length(trlInds)
                temp = temp + trialSegData(trlInds(sc)).Wxy.coeffs;
            end
            stimSegData(one,two).avg.WxyCoeffs = temp/(length(blah)+1);
            W = abs(stimSegData(one,two).avg.WxyCoeffs);
            F = repmat(trialSegData(1).Wxy.freqVec(:),1,size(W,2));
            B = W.*F;
            stimSegData(one,two).avg.swimDist = mean(B(:));
            figure('Name', ['Avg Wxy for Stim Condition [' num2str(one) ',' num2str(two) '] '])
            Wxy = stimSegData(one,two).avg.WxyCoeffs;
            aWxy = angle(Wxy);
            Wxy(abs(aWxy)< phaseCutoff) = 0;
            PlotWxy(Wxy,trialSegData(1).Wxy.time,trialSegData(1).Wxy.freqVec,'clim',[0 20])
        end
    end
end
stimSegData(1,1).time = trialSegData(1).time;
stimSegData(1,1).time_Wxy = trialSegData(1).Wxy.time;
stimSegData(1,1).freqVec_Wxy = trialSegData(1).Wxy.freqVec;
% disp('Writing stimSegData to matfile...')
% tic
% pyData.stimSegData = stimSegData;
% toc


%% Creating Trialized Data for Statistical Analyses
tData = struct;
tData.trlsDurSwm = zeros(numel(stim.inds),1);
for trial = 1:size(trialSegData,1)
    tData.stimOnset(trial) = trialSegData(trial).stim.time;
    tData.stimAmp(trial) = trialSegData(trial).stim.amps(1);
    tData.stimGroup(trial) = find(unique(stim.amps(stim.activeCh,:))== tData.stimAmp(trial));
    tData.swimOnset(trial,:) = min(trialSegData(trial).swim.onset)*1000;
    tData.swimDur(trial) = trialSegData(trial).swim.dur_se*1000;
    tData.swimFreq(trial) = trialSegData(trial).Wxy.maxFreq;
    tData.swimPow(trial)=  abs(trialSegData(trial).Wxy.maxPow);
    tData.swimAmp(trial) = trialSegData(trial).swim.amp_se;
    %# Finding trials that inititated during a swim episode
    if data.swim.endInds(find(data.swim.startInds < trialSegData(trial).stim.ind,1,'last'))...
            > trialSegData(trial).stim.ind
        tData.trlsDurSwm(trial) = 1;
    end
end
tData.trlsDurSwm = find(tData.trlsDurSwm);
%# Eliminating trials where low stim onset values can likely results from
%#       spont activity
fastOnset = min(2,mode(tData.swimOnset(tData.stimGroup == tData.stimGroup(end))));
tooEarlies = find((tData.swimOnset(:) < fastOnset) & (tData.stimGroup(:) <=2));
[tData.swimOnset(tooEarlies),tData.swimFreq(tooEarlies), ...
    tData.swimPow(tooEarlies), tData.swimAmp(tooEarlies), ...
    tData.swimDur(tooEarlies)] = deal(nan);

tooLates = find(tData.swimOnset >= 200);
[tData.swimOnset(tooLates), tData.swimFreq(tooLates), ...
    tData.swimDur(tooLates), tData.swimPow(tooLates), ...
    tData.swimAmp(tooLates)] = deal(nan);

%# Find presumably stim-evoked weak swim trials regardless of stim amp
tData.wkSwmTrls = weakInds(~isnan(tData.swimAmp(weakInds)));

%# Find stim-evoked strong swim trials
tData.strngSwmTrls = strngInds;

%# Non-swm trls
tData.nonSwmTrsl = nonSwmInds;

% Incomplete length trials
tData.incompleteTrls = data.incompleteSegInds;

%# Calculating swim probability for each stim amp/group
tData.swimProb = zeros(size(unique(tData.stimGroup)));
for stimGrp = 1:numel(unique(tData.stimGroup))
    tData.swimProb(stimGrp) = ...
        sum(tData.swimFreq(tData.stimGroup==stimGrp)~=0 & ~isnan(tData.swimFreq(tData.stimGroup == stimGrp)))/sum(tData.stimGroup==stimGrp);
end

% Swm duration determined by burst count
burstInds = {};
combinedBurstInds = [];
for ch = 1:size(data.burst.train,2)
    burstInds{ch} = find(data.burst.train(:,ch));
    combinedBurstInds = union(combinedBurstInds,burstInds{ch});
end
nBurstsInSwm = zeros(numel(data.stim.inds),1);
burstBasedSwmDur = nBurstsInSwm;
for trl = 1:numel(data.stim.inds)
    startInd = data.stim.inds(trl);
    endInd = data.stim.inds(trl) + round(3.5*data.samplingRate);
    postStimBurstTimes = data.t(combinedBurstInds((combinedBurstInds > startInd) & (combinedBurstInds < endInd)));
    dB = diff(postStimBurstTimes);
if ~isempty(dB)
    evokedBurstTimes = [];
   for brst = 1:numel(dB)
       if dB(brst) < 1*(1/2)
           evokedBurstTimes = union(evokedBurstTimes,postStimBurstTimes(brst));
       end
   end    
  evokedBurstTimes = union(evokedBurstTimes, postStimBurstTimes(brst+1));
  nBurstsInSwm(trl) = round(numel(evokedBurstTimes)/2);
  burstBasedSwmDur(trl) = round(1000*(evokedBurstTimes(end)-evokedBurstTimes(1)));
end
end
burstBasedSwmDur(burstBasedSwmDur==0) = nan;
tData.swmDur_burstCount = burstBasedSwmDur;
tData.nBurstsInSwm = nBurstsInSwm;

data.tData = tData;
disp('Writing tData to matfile...')
tic
pyData.tData = tData;
toc

data.trialSegData  = trialSegData;
data.stimSegData = stimSegData;
data.tData = tData;

return

%% Writing Data to a File
% answer = questdlg('Write behavior data to file?','Writing behavior data to .csv file','No');
% if strcmpi(answer,'Yes')
%     disp('Writing data to a file')
%     slashInds = strfind(data.filename, '\');
%     ephysDir = data.filename(1:slashInds(end)-1);
%     csvName = fullfile(ephysDir,'BehaviorSummary');
%     WriteDataToFile(csvName, trialSegData)
% end
% % p_swim = round((max(nfv,[],2)./nTrialMat(:))*100);


%%%%%%%################### Plots and Analyses #######################


%% Histograms of swim Parameters
HistogramStackedByStimGroup(tData,'swimFreq',[0:10:100],1);
HistogramStackedByStimGroup(tData,'swimOnset',[2:5:50],1);
HistogramStackedByStimGroup(tData,'swimDur',[0:20:1000],1);
HistogramStackedByStimGroup(tData,'swimPow',[0:75:800],1);
HistogramStackedByStimGroup(tData,'swimAmp',[0:50:500],1);

%% Swim parameters as a function of stim int
figure('Name','Swim parameters vs stim int')
%# Swim Freq
varName = tData.swimFreq;
subaxis(2,3,1,'MR',0.05, 'ML', 0.1,'MT', 0.05)
plot(tData.stimAmp(varName~=0),varName(varName~=0),'k.')
axis square, box off
set(gca,'tickdir','out','xtick',[])
hold on
nStimGrps = numel(unique(tData.stimGroup));
[mu,med,sig] = deal(zeros(nStimGrps,1));
for stimGrp = 1:nStimGrps
    tInds = find(tData.stimGroup == stimGrp);
    blah = varName(tInds);
    blah(blah==0 | isnan(blah))=[];
    mu(stimGrp) = mean(blah);
    med(stimGrp) = median(blah);
    sig(stimGrp) = std(blah);
end
nStimGroups = numel(unique(tData.stimGroup));
x = unique(tData.stimAmp);
plot(x(~isnan(mu)),mu(~isnan(mu)),'ro-')
plot(x(~isnan(med)),med(~isnan(med)),'bs-')
ylim([min(varName(varName~=0))-5 max(varName)+5])
title('Swim Freq')
ylabel('Hz')

%# Swim Onset
varName = tData.swimOnset;
subaxis(2,3,2,'MR',0.05, 'ML', 0.1,'MT', 0.05)
plot(tData.stimAmp(varName~=0),varName(varName~=0),'k.')
axis square, box off
set(gca,'tickdir','out','xtick',[],'yaxisLocation','left')
hold on
nStimGrps = numel(unique(tData.stimGroup));
[mu,med,sig] = deal(zeros(nStimGrps,1));
for stimGrp = 1:nStimGrps
    tInds = find(tData.stimGroup == stimGrp);
    blah = varName(tInds);
    blah(blah==0 | isnan(blah))=[];
    mu(stimGrp) = mean(blah);
    med(stimGrp) = median(blah);
    sig(stimGrp) = std(blah);
end
x = unique(tData.stimAmp);
plot(x(~isnan(mu)),mu(~isnan(mu)),'ro-')
plot(x(~isnan(med)),med(~isnan(med)),'bs-')
ylim([min(varName(varName~=0))-1 max(varName)+1])
title('Swim Onset')
ylabel('ms')

%# Swim Dur
varName = tData.swmDur_burstCount;
subaxis(2,3,3,'MR',0.05, 'ML', 0.1,'MT', 0.05)
plot(tData.stimAmp(varName~=0),varName(varName~=0),'k.')
axis square, box off,
set(gca,'tickdir','out','xtick',[])
hold on
nStimGrps = numel(unique(tData.stimGroup));
[mu,med,sig] = deal(zeros(nStimGrps,1));
for stimGrp = 1:nStimGrps
    tInds = find(tData.stimGroup == stimGrp);
    blah = varName(tInds);
    blah(blah==0 | isnan(blah))=[];
    mu(stimGrp) = mean(blah);
    med(stimGrp) = median(blah);
    sig(stimGrp) = std(blah);
end
x = unique(tData.stimAmp);
plot(x(~isnan(mu)),mu(~isnan(mu)),'ro-')
plot(x(~isnan(med)),med(~isnan(med)),'bs-')
ylim([min(varName(varName~=0))-5 max(varName)+5])
title('Swim Dur')
ylabel('ms')

%# Swim Power/Dist;
varName = tData.swimPow;
subaxis(2,3,4,'MR',0.05, 'ML', 0.1,'MT', 0.05)
plot(tData.stimAmp(varName~=0),varName(varName~=0),'k.')
axis square, box off
set(gca,'tickdir','out','yaxisLocation','left','xtick',[])
hold on
nStimGrps = numel(unique(tData.stimGroup));
[mu,med,sig] = deal(zeros(nStimGrps,1));
for stimGrp = 1:nStimGrps
    tInds = find(tData.stimGroup == stimGrp);
    blah = varName(tInds);
    blah(blah==0 | isnan(blah))=[];
    mu(stimGrp) = mean(blah);
    med(stimGrp) = median(blah);
    sig(stimGrp) = std(blah);
end
x = unique(tData.stimAmp);
plot(x(~isnan(mu)),mu(~isnan(mu)),'ro-')
plot(x(~isnan(med)),med(~isnan(med)),'bs-')
ylim([min(varName(varName~=0))-10 max(varName)+10])
title('Swim Dist')

%# Swim Amp
varName = tData.swimAmp_raw;
subaxis(2,3,5,'MR',0.05, 'ML', 0.1,'MT', 0.05)
plot(tData.stimAmp(varName~=0),varName(varName~=0),'k.')
axis square, box off
set(gca,'tickdir','out','yaxisLocation','left','xtick',stim.conditions{2})
hold on
nStimGrps = numel(unique(tData.stimGroup));
[mu,med,sig] = deal(zeros(nStimGrps,1));
for stimGrp = 1:nStimGrps
    tInds = find(tData.stimGroup == stimGrp);
    blah = varName(tInds);
    blah(blah==0 | isnan(blah))=[];
    mu(stimGrp) = mean(blah);
    med(stimGrp) = median(blah);
    sig(stimGrp) = std(blah);
end
x = unique(tData.stimAmp);
plot(x(~isnan(mu)),mu(~isnan(mu)),'ro-')
plot(x(~isnan(med)),med(~isnan(med)),'bs-')
ylim([min(varName(varName~=0))-10 max(varName)+10])
xlabel('Stim Amp (V)')
title('Swim Amp')
legend('Actual','Mean','Median')

%% Plot Swim Prob as a Function of Stim Int
figure('Name','Swim Prob vs Stim Int')
markerSize = 4*sqrt((nTrialMat/min(nTrialMat(nTrialMat~=0))));
set(gca,'tickdir','out','xtick',unique(tData.stimAmp),'ytick',[0:0.2:1]), box off
hold on
for stimGrp = 1:numel(unique(tData.stimGroup))
    x = unique(tData.stimAmp);
    plot(x(stimGrp),tData.swimProb(stimGrp),'ko-','markersize',markerSize(stimGrp))
end
plot(x,tData.swimProb,'k')
xlabel('Stim Amp (V)')
ylabel('P(swim)')
ylim([0 1])

%% Plotting swim rate averaged over strong swim trials
figure('Name','Swim Rate for Strong Swim Trials')
plot([0 0 0], [-0.3 0 0.1],'r--')
hold on
fullTrl = completeSegInds(1);
plot(trialSegData(fullTrl).time,zeros(size(trialSegData(fullTrl).time)),'k:')
baseInds = find(trialSegData(fullTrl).time >=-12e-3 & trialSegData(fullTrl).time <-3e-3);
avgRate = zeros(size(trialSegData(fullTrl).time));
for trial = 1:numel(tData.strngSwmTrls)
    if trial ~= data.incompleteSegInds
        tempSig = trialSegData(tData.strngSwmTrls(trial)).swim.rate;
        baseRate = mean(tempSig(baseInds));
        %     plot(trialSegData(1).time, tempSig-baseRate,'color',[0 0.4 0])
        avgRate = avgRate(:) + (tempSig - baseRate);
    end
end
avgRate  = avgRate/(numel(tData.strngSwmTrls)+1);
plot(trialSegData(fullTrl).time, avgRate,'linewidth',2)
xlim([-inf inf])
ylim([-inf inf])
set(gca,'tickdir','out')
box off
ylabel('Change in swim rate with respect to pre-stim rate (Hz)')
xlabel('Time (s)')
title('Swim rate averaged over strong swim trials')



%% Plotting all strng and wk swm trls in separate plots
figure('Name','StrngSwmTrls');
offset = 200;
xLim = [-inf 40];
zerInd = min(find(trialSegData(1).time>=0));
for trl = 1:numel(tData.strngSwmTrls)   
    plot(trialSegData(1).time,trialSegData(tData.strngSwmTrls(trl)).smooth.exp(:,2)-(offset*(trl-1)))    
   hold on  
end
yTick = -offset*fliplr((0:2:numel(tData.strngSwmTrls)-1));
sh = stem(trialSegData(1).time(zerInd),min(yTick)-offset,'r--'); set(sh,'marker','none')
ytl = round(abs(yTick)/offset)+1;
set(gca,'ytick',yTick,'yticklabel',ytl,'tickdir','out')
box off, 
xlim(xLim)
title('Strong Swim Trials')

figure('Name','WkSwmTrls')
% offset = offset/2;
for trl = 1:numel(tData.wkSwmTrls)
    plot(trialSegData(1).time,trialSegData(tData.wkSwmTrls(trl)).smooth.exp(:,2)-(offset*(trl-1)))
   hold on  
end
yTick = -offset*fliplr((0:2:numel(tData.wkSwmTrls)-1));
sh = stem(trialSegData(1).time(zerInd),min(yTick)-offset,'r--'); set(sh,'marker','none')
ytl = round(abs(yTick)/offset)+1;
set(gca,'ytick',yTick,'yticklabel',ytl,'tickdir','out')
box off, 
xlim(xLim)
title('Weak Swim Trials')

%% Raster plot of swim episodes   
figure('Name','Strng & Wsk SwmTrls - Raster Plot');
offset = 1;
xLim = [-inf 40];
zV = zeros(size(trialSegData(1).time));
subplot(2,1,1)
for trl = 1:numel(tData.strngSwmTrls)   
    blah = trialSegData(tData.strngSwmTrls(trl)).swim.bool;
    inds = find(blah(1:end-1)==0 & blah(2:end) ==1);
    ind_strng = inds(min(find(inds>=zerInd)));
    plot(trialSegData(1).time(inds),zV(inds)-((trl-1)*offset),'.')   
   hold on
   plot(trialSegData(1).time(ind_strng),zV(ind_strng)-((trl-1)*offset),'ko')
end
yTick = -offset*fliplr((0:2:numel(tData.strngSwmTrls)-1));
sh = stem(trialSegData(1).time(zerInd),min(yTick)-1,'r--'); set(sh,'marker','none')
ytl = round(abs(yTick)/offset)+1;
set(gca,'ytick',yTick,'yticklabel',ytl,'tickdir','out','xtick',[])
box off, 
xlim(xLim)
ylim([-(numel(tData.strngSwmTrls)) 0])
title('Strong Swim Trials')

subplot(2,1,2)
for trl = 1:numel(tData.wkSwmTrls)   
    blah = trialSegData(tData.wkSwmTrls(trl)).swim.bool;
    inds = find(blah(1:end-1)==0 & blah(2:end) ==1);
    ind_strng = inds(min(find(inds>=zerInd)));
    plot(trialSegData(1).time(inds),zV(inds)-((trl-1)*offset),'.')   
   hold on   
end
yTick = -offset*fliplr((0:2:numel(tData.wkSwmTrls)-1));
sh = stem(trialSegData(1).time(zerInd),min(yTick)-1,'r--'); set(sh,'marker','none')
ytl = round(abs(yTick)/offset)+1;
set(gca,'ytick',yTick,'yticklabel',ytl,'tickdir','out')
box off, 
xlim(xLim)
xlabel('Time (s)')
ylabel('Trial #')
ylim([-(numel(tData.wkSwmTrls)) 0])
title('Weak Swim Trials')

%% Min-max of strng and wk swim episodes
figure('Name','Min-max of strng and wk swim episodes')
scale = 2;
offset = 1;
ch = 2;
[~,wk_min] = min(tData.swimPow(tData.wkSwmTrls));
[~,wk_max] = max(tData.swimPow(tData.wkSwmTrls));
[~,strng_min] = min(tData.swimPow(tData.strngSwmTrls));
[~,strng_max] = max(tData.swimPow(tData.strngSwmTrls));
plot(trialSegData(1).time,trialSegData(tData.strngSwmTrls(strng_min)).artless(:,ch))
hold on
plot(trialSegData(1).time,scale*trialSegData(tData.wkSwmTrls(wk_min)).artless(:,ch)-offset,'r')
plot(trialSegData(1).time,trialSegData(tData.strngSwmTrls(strng_max)).artless(:,ch)-2*offset)
plot(trialSegData(1).time,scale*trialSegData(tData.wkSwmTrls(wk_max)).artless(:,ch)-3*offset,'r')
legend('Strong',['Weak (' num2str(scale) 'x scaled)'])
set(gca,'tickdir','out','ytick',[])
xlabel('Time (s)')
box off
xlim([-0.05 0.7])
ylim([-inf inf])
title('Min-max of strong and weak swim episodes')

% Also plot median strength swim episode in separate plot
medStrengthTrlsbyGrp = zeros(numel(unique(tData.stimGroup)),1);
for grp = 1:numel(unique(tData.stimGroup))
    medSwmPow = median(tData.swimPow(tData.stimGroup == grp));
    [~,medStrengthTrlsbyGrp(grp,1)] = min(abs(tData.swimPow-medSwmPow));    
end


%% Plot all low and highest stim int respnonse
ch = 1;
yOff = 1;
yLim = [-2*yOff, 1*yOff];
xLim = [-50 3500]
cMap = colormap(hsv); close
clrs = cMap(round(linspace(1,64,numel(unique(data.tData.stimGroup))+1)),:);
trialSegData = data.trialSegData; 
for stimInt = 1:numel(unique(data.tData.stimGroup))
    figure('Name',['Stim Group = ' num2str(stimInt)])
    trlsAtThisInt = find(data.tData.stimGroup == stimInt);
    wkestTrl = ...
        trlsAtThisInt(data.tData.swimAmp(trlsAtThisInt)==min(data.tData.swimAmp(trlsAtThisInt)));
    if ~isempty(wkestTrl)
        wkestTrl  = wkestTrl(1);
    end
    strngestTrl = ...
        trlsAtThisInt(data.tData.swimAmp(trlsAtThisInt)==max(data.tData.swimAmp(trlsAtThisInt)));
    if ~isempty(strngestTrl)
        strngestTrl  = strngestTrl(1);
    end
    plot(trialSegData(1).time*1000,trialSegData(wkestTrl).artless(:,ch))
    hold on   
    plot(trialSegData(1).time*1000,trialSegData(strngestTrl).artless(:,ch)-yOff,'r')
    box off
    xlim(xLim), xlabel('Time (ms)')
    ylim(yLim)
    set(gca,'ytick',[],'tickdir','out')
    title(['Weakest and strongest swim episode for stimulus group ' num2str(stimInt)])
    legend('Weakest swim', 'Strongest swim')
    plot([0 0], yLim,'k:')
end

%% Stim-intensity independent bimodality of swim strengths
y = data.tData.swimPow;
cutoff = (max(y)-min(y))/20;
removedInds = find(y<cutoff);
y(removedInds)=[];
y = y/max(y);
x = data.tData.stimAmp;
x(removedInds) = [];
x = x/min(x);
figure('Name','Swim strengths at different stim intensities')
plot(x,y,'k.')
box off
set(gca,'xtick',round(unique(x)*100)/100,'tickdir','out')
dx = median(diff(sort(unique(x),'ascend')));
xlim([min(x)-dx max(x)+dx])
xlabel('Min-normalized stimulus intensity')
ylabel('Max-normalized swim strength')
title('Swim strengths for different stim intensities')


%% Piecemeal smoothed swim traces for the entire expt
kerLen = round(0.015*6000);
expKer = exp(-3*linspace(0,1,kerLen));
expKer = expKer/sum(expKer);
temp = zeros(size(data.smooth.swim));
tic
for ch = 1:size(data.smooth.swim,2)
    disp(['Convolving ch ' num2str(ch) '...'])
    blah = conv(abs(data.smooth.burst(:,ch)),expKer(:));
    temp(:,ch) = blah(1:length(data.smooth.swim));     
end
% temp = median(temp,2);
% temp = temp/max(temp);

blah = swimCharacterize(data.artless(1,:));
temp = sqrt(blah.fltCh);
toc

segLen = 10*60;

if ~isfield(data,'t')
    if ~isfield(data,'samplingRate')
        data.samplingRate = 6000;
        disp(['Assuming sampling rate is ' num2str(6000) '. Correct if not true!'])
    end
    data.t = (0:length(data.artless)-1)*(1/data.samplingRate);
end
nSegs = round((data.t(end)-data.t(1))/segLen);
dt = data.t(2)-data.t(1);
figure('Name','Increase in spont swim rate over time')
for seg = 1:nSegs
    tLim = [(seg-1)*segLen + data.t(1) (seg-1)*segLen + segLen + data.t(1)];    
    segPts = min(floor(tLim/dt)+1,length(data.t));    
    subplot(nSegs,1,seg)
    plot(data.t(segPts(1):40:segPts(2)),temp(segPts(1):40:segPts(2)))
    xlim(data.t(segPts)-5)
    ylim([-0.05 1.05])
    xTicks = data.stim.times(data.stim.times>= tLim(1) & data.stim.times<=tLim(2));
    xTickLbls = floor(xTicks);
    set(gca,'tickdir','out','ytick',[],'xtick',xTicks,'xticklabel','')
    txt= [num2str(tLim(1)) '-' num2str(tLim(2)) ' s'];
    text(floor(mean(tLim)),0.25,txt)
    if seg ==1
        title('Swim activity throughout the expt')
    end
    box off
end


%% Swim Rate
disp('Computing swim rate...')
tic
tWin = 20; 
dt = data.t(2)-data.t(1);
rKer = ones(round(tWin/dt),1)/tWin; % Expresses rate in Hz
swim.rate = conv2(data.swim.boolOn(:),rKer(:),'same');

gKer = gausswin(round((tWin/2)/dt));
gKer = gKer/sum(gKer);
swim.rate = conv2(swim.rate(:),gKer(:),'same');
toc

xTicks = data.stim.times;
ax(1) = subplot(2,1,1);
% plot(data.t(1:40:end),temp(1:40:end))
temp = log2(temp);
temp = temp-mean(temp);
temp(temp<0) = 0;
temp = temp-min(temp);
temp = temp/max(temp);
plot(data.t(1:40:end),temp(1:40:end))
set(gca,'xtick',[],'xticklabel','','tickdir','out')
box off
hold on
y = -0.05*(data.tData.stimGroup/min(data.tData.stimGroup));
stem(data.tData.stimOnset,y,'r','marker','none')
ylabel('Max-normalized amp')
legend('Smoothed EMG/VR', 'Stim pulses')
ax(2) = subplot(2,1,2);
plot(data.t(1:40:end),swim.rate(1:40:end))
hold on
stem(data.tData.stimOnset,y,'r','marker','none')
box off
xlabel('Time (s)')
ylabel('Swim rate (Hz)')
linkaxes(ax,'x')
xlim([-5 inf])
set(gca,'tickdir','out')
box off


%% Also plot median strength swim episode in separate plot
yLim = [-0.4 0.5];
xLim = [-50 1200];
figure('Name','Median strength episode for each stim group')
nGrps = numel(unique(tData.stimGroup));
medStrengthTrlsbyGrp = zeros(nGrps,1);
for grp = 1:nGrps
    swmPows = tData.swimPow(tData.stimGroup ==grp);
    swmPows((swmPows==0) | isnan(swmPows)) =[];
    medSwmPow = median(swmPows);
    blah = tData.swimPow;
    blah(~(tData.stimGroup==grp))=nan;
    [~,medStrengthTrlsbyGrp(grp,1)] = min(abs(blah-medSwmPow));
    ax(grp) = subplot(nGrps,1,grp)
    plot(trialSegData(1).time*1000,trialSegData(medStrengthTrlsbyGrp(grp)).artless(:,ch))
    box off
    if grp ~= nGrps
        set(gca,'xtick',[],'ytick',[])
        if grp ==1
            title('Median strength swim episode for each stim group')
        end
    else
        xlabel('Time (ms)')
        ylabel('Amplitude (V)')
        set(gca,'ytick',[yLim(1) 0 yLim(2)])
    end
     set(gca,'tickdir','out')
     xlim(xLim)
     ylim(yLim)
     text(1700,0.2,['Stim Group: ' num2str(grp)])
end
linkaxes(ax,'x')


%% Raster plot of swims for strong swim trials
swimRaster = [];
trlLen = length(trialSegData(completeSegInds(1)).time);
t = trialSegData(completeSegInds(1)).time;
preStimPer= find(t>=0,1,'first')*(1/data.samplingRate);
for trl = 1:numel(tData.strngSwmTrls)
      y = trialSegData(tData.strngSwmTrls(trl)).swim.boolOn(:)';      
      if length(y) < trlLen
          lenDiff = trlLen-length(y);
          seg = zeros(1,lenDiff);
          y = cat(2,seg,y);          
      end
        escapePt = find(t>=11e-3,1,'first');
        escaped = sum(y(t>=-0.1 & t<=15e-3));
        if ~escaped
            y(escapePt) = 1;
        end    
        swimRaster = cat(1,swimRaster,y); 
end
swimRaster = logical(swimRaster);
% [r c] = find(flipud(swimRaster));
[r c] = find(swimRaster);
plot([c c]'*(1/data.samplingRate)-preStimPer,[r-0.3 r+0.3]','k-')
box off
xlim([-inf inf])
set(gca,'tickdir','out','ydir','reverse','ytick',1:numel(tData.strngSwmTrls))
xlabel('Time (sec)')
ylabel('Trial #')
title('Raster plot of strong swim trials')



%% Raw amp for stim-evoked activity
if ~exist('tData')
    tData = data.tData;
elseif ~ exist('trialSegData')
    trialSegData= data.trialSegData;
end

tData.swimAmp_raw = zeros(size(tData.stimAmp));
tInds = find(trialSegData(1).time >= 4e-3 & trialSegData(1).time <= 0.5);
for trl = 1:length(tData.stimAmp)
    temp = trialSegData(trl).artless(tInds);
    tData.swimAmp_raw(trl) = max(abs(temp(:)));
    if ~trialSegData(1).Wxy.swimBool(trl)
        tData.swimAmp_raw(trl) = nan;
    end
end


%% Raster plot of swims for a list of trials
swimRaster = [];
% nTrials = length(trialSegData);
% trlList = tData.wkSwmTrls;
trlList = [1:3 21:23];
nTrials = length(trlList);
trlLen = length(trialSegData(completeSegInds(1)).time);
t = trialSegData(completeSegInds(1)).time;
preStimPer= find(t>=0,1,'first')*(1/data.samplingRate);
amps = sparse([]);
for trl = 1:numel(trlList)
      y = trialSegData(trlList(trl)).swim.boolOn(:)';
      p = trialSegData(trlList(trl)).smooth.swim;    
      if length(y) < trlLen
          lenDiff = trlLen-length(y);
          seg = zeros(1,lenDiff);
          y = cat(2,seg,y);          
      end
        escapePt = find(t>=11e-3,1,'first');
        escaped = sum(y(t>=-0.1 & t<=15e-3));
        if ~escaped
            y(escapePt) = 1;
        end 

      pInds = find(y);         
      for jj = 1:numel(pInds)-1
          amps(trl,pInds(jj)) = max(p(pInds(jj):pInds(jj+1)));          
      end
      amps(trl,pInds(jj+1)) = max(p(pInds(jj+1):end));
        swimRaster = cat(1,swimRaster,y);
        if trl==1
            amps(trl,pInds(jj)) = 50;
        end
end
swimRaster = logical(swimRaster);
temp = amps';
% powVec = temp(find(temp));
powVec = amps(find(amps));
% powVec(powVec>125) = 200;
% powVec(powVec < 125) = 50;

% powVec = powVec.^2;
powVec = powVec.^0.6;
[~,maxInds] = sort(powVec,'descend');
powVec(maxInds(1)) = powVec(maxInds(2));
blah = (powVec-min(powVec))/(max(powVec)-min(powVec));
% blah = sqrt(blah);
% blah = round(blah*(50-20))+20;
blah = floor(blah*(64-5)) + 5;
% blah = floor(blah*63)+1;
cMap = colormap(jet);
colVals = cMap(blah,:);
% colVals = MapValsToColors(powVec,colormap(jet));

% [r c] = find(flipud(swimRaster));
figure
[r c] = find(swimRaster);
for row = 1:size(r,1)
    idx = sub2ind(size(swimRaster),r(row),c(row));
    clr = colVals(row,:);
    plot([c(row) c(row)]'*(1/data.samplingRate)-preStimPer,[r(row)-0.4 r(row)+0.4]','-','color',clr)
    hold on
 end
% plot([c c]'*(1/data.samplingRate)-preStimPer,[r-0.3 r+0.3]','k-')
box off
xlim([-10 55])
ylim([0 nTrials+1])
set(gca,'tickdir','out','ydir','reverse','ytick',1:nTrials)
xlabel('Time (sec)')
ylabel('Trial #')
title('Raster plot of strong swim trials (color-coded by swim strength)')
set(gca,'color','k')
shg


%% Plotting trial-by-trial
trlList = [1:10];
yShift = 2;
ch = 2;
xLim = [-10 50];
if isempty(trlList)
    trlList = 1:length(trialSegData);
end
figure('Name','Sequential trial responses')
count = 0;
yTick = [];
for trl = trlList(:)'
    if length(trialSegData(1).time) == length(trialSegData(trl).artless(:,ch))
    plot(trialSegData(1).time,trialSegData(trl).artless(:,ch)- count*yShift)  
    hold on
    yTick = [yTick;-count*yShift];
    count = count + 1;   
    end
end
xlim(xLim),ylim([-inf inf])
% ytl = length(trlList)-1:-1:0;
% ytl = fliplr(trlList)
ytl = fliplr([1 2 3 21 22 23]);
box off, set(gca, 'tickdir','out','ytick',flipud(yTick(:)),'yticklabel',ytl)
xlabel('Time (sec)')
ylabel('Trl #')
title('Trials in sequence')
shg

%% Plots - Need minor fixes
return;




%% Checking detection of stacks coincident with shocks
% figure('Name','Checking detection of stacks coincident with shocks')
% plot(data.t,meanStimChan)
% hold on
% plot(stackTimes(shockStackInds), zeros(size(shockStackInds)),'ro')
% xlim([-inf inf])


%% SWIMDATA
% swimData = swimCharacterize(data.(efn));
