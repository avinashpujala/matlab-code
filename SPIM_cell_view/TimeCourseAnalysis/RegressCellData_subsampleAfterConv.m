%% Loading data
disp('Loading data...')
tic
data = pyData.data;
data.tData = pyData.tData;
data.trialSegData = pyData.trialSegData;
data.stack = pyData.stack;
data.cells = pyData.cells;
data.completeSegInds = setdiff(1:length(data.trialSegData),data.incompleteSegInds);
toc

%% Generating the CIRF
disp('Generating Ca2+ impuse response function from M-cell activity...')
tic
timeTrl = completeSegInds(1);
mCellIdx = data.cells.mCellIdx;
data.samplingRate = 6000;
timePts = [-1 9];
tData.strngSwmTrls(tData.strngSwmTrls==data.incompleteSegInds) =[];
mCellStrngSwmDFF = zeros(numel(tData.strngSwmTrls),length(trialSegData(1).cells.time));
for trl = 1:numel(tData.strngSwmTrls)
      mCellStrngSwmDFF(trl,:) = trialSegData(tData.strngSwmTrls(trl)).cells.dFF(mCellIdx,:);    
end
data.cells.CIRF = mean(mCellStrngSwmDFF,1);
inds = find(trialSegData(1).cells.time >=timePts(1) & trialSegData(1).cells.time <= timePts(2));
data.cells.CIRF = data.cells.CIRF(inds);
data.cells.CIRF = data.cells.CIRF-data.cells.CIRF(1);
t = trialSegData(1).cells.time(inds);
epOpSampleRatio= 6000/(1/stack.interval);
blah = linspace(t(1),t(end),length(t));
blah_sup  = linspace(t(1),t(end),round(length(t)*epOpSampleRatio));
temp_sup = interp1(t,data.cells.CIRF,blah_sup,'cubic');

dF = diff(temp_sup);
onsetPt = find(blah_sup>=0,1,'first');
peakPt = find(temp_sup == max(temp_sup));
risePhase = temp_sup(onsetPt:peakPt);
decayPhase = temp_sup(peakPt:end);

f_decay = fittype('A*exp(-x/tau)');
decay_fit = fit((1:length(decayPhase))',decayPhase(:),f_decay);
decay_est = decay_fit(1:length(decayPhase));

risePhase = (risePhase-risePhase(1))/(risePhase(end)-risePhase(1));
decay_est = (decay_est-decay_est(end))/(decay_est(2)-decay_est(end));
decay_est = decay_est(2:end);
sig = cat(1,risePhase(:),decay_est(:));
data.cells.CIRF= sig/sum(sig);
toc

%#### Make some regressors ##########
%% Stimulus regressor
disp('Generating the stimulus regressor...')
stimReg = zeros(size(data.t));
stimReg(data.stim.inds) = tData.stimAmp;
stimReg = conv(stimReg(:),data.cells.CIRF(:));
stimReg = stimReg(1:length(data.t));
stimReg = stimReg(stack.inds.start);
tempTime = data.t(stack.inds.start);
stimReg = interp1(tempTime,stimReg,data.cells.time);
stimReg = unitvec(stimReg);

%% Motor regressors
disp('Generating a boolean that is true during strong swim...')
strngSwmThr = mean(data.swim.pow) + 3*std(data.swim.pow);
strngSwmInds = find(data.swim.pow >= strngSwmThr);
strngSwmBool = zeros(size(data.t));
for trl = 1:numel(strngSwmInds)
    firstTime = data.t(data.swim.startInds(strngSwmInds(trl))) - 1500e-3;
    firstTime = max(1/data.samplingRate,firstTime);
    firstPt = ceil(firstTime*data.samplingRate); 

    lastTime = data.t(data.swim.endInds(strngSwmInds(trl))) + 500e-3;
    lastTime = min(data.t(end),lastTime);
    lastPt = floor(lastTime*data.samplingRate);
    
    strngSwmBool(firstPt:lastPt) = 1;      
end
wkSwmBool = 1-strngSwmBool;
[motReg.strng, motReg.wk] = deal([]);
disp('Generating separate motor regressors for strong and weak swims...')
tic
for ch = 1:size(data.smooth.burst,2)
    disp(['Ch ' num2str(ch) ', strong swim...'])   
    blah = conv(data.smooth.burst(:,ch).*strngSwmBool(:),data.cells.CIRF(:));
    blah = blah(1:length(data.t));
    blah = blah(stack.inds.start(1:size(data.cells.dFF,2)));    
    motReg.strng(:,ch) = unitvec(blah);
%     motReg.strng(:,ch) = blah;
    
    disp(['Ch ' num2str(ch) ', weak swim...'])   
    blah = conv(data.smooth.burst(:,ch).*wkSwmBool(:),data.cells.CIRF(:));
    blah = blah(1:length(data.t));
    blah = blah(stack.inds.start(1:size(data.cells.dFF,2)));
    motReg.wk(:,ch) = unitvec(blah);
%      motReg.wk(:,ch) = blah;
end
toc

%% Pause regressor
disp('Generating a swim pause regressor...')
tic
swimDetThr = 8;
pauseReg = zeros(size(data.t));
pauseReg(data.smooth.swim <= swimDetThr) = 1;
pauseReg = conv(pauseReg(:),data.cells.CIRF(:));
pauseReg = pauseReg(1:length(data.t));
pauseReg = pauseReg(stack.inds.start(1:size(data.cells.dFF,2)));
pauseReg = unitvec(pauseReg);
% pauseReg = unitvec(pauseReg);
toc

%% Find cells to include in regression
dFFDetThr = 0.35;
dFFMax = max(data.cells.dFF,[],2);
maxCells = find(dFFMax >= dFFDetThr);
if sum(maxCells==data.cells.mCellIdx)
    disp('Mauthner cell included!')
else
    disp('Mauthner cell not found!')
end

cellSig = std(data.cells.dFF,[],2);
activeCells = find(cellSig>=mean(cellSig));

% regressedCells = unique([maxCells(:); activeCells(:); data.cells.responsiveCellInds(:)]);
regressedCells = 1:size(data.cells.dFF,1);
disp([num2str(numel(regressedCells)) ' cells included for regression.'])

%% Combine regressors into input matrix X
regData = struct;
X = [unitvec(ones(length(stimReg),1)) stimReg(:) motReg.strng(:,1) motReg.strng(:,2) ...
    motReg.wk(:,1) motReg.wk(:,2) pauseReg(:)];
regData.lbls = {'Offset','Stim','Strng Left','Strng Right', 'Wk Left','Wk Right', 'Pause'};
regData.X = X;
regData.regressedCells = regressedCells;
regData.Y = data.cells.dFF(regressedCells,:)';
ts = datestr(now,30);
disp('Saving regression data...')
tic
regFilePath = fullfile(ephysDir,['regData_' num2str(ts) '.mat']);
save(fullfile(ephysDir,['regData_' num2str(ts) '.mat']),'regData','-v7.3')
toc



%% Regressing
disp('Regressing...')
tic
B = regData.X\regData.Y;
toc
regData.B = B;
% regFile = matfile(regFilePath,'Writable', true);

%% Computing Rsq
disp('Computing Rsq...')
tic
X = regData.X;
Y = regData.Y;
B_prime = regData.B';

Y_est = X*B;
res = Y-Y_est;
SS_res = sum(res.^2,1);
SS_tot = sum((Y - repmat(mean(Y,1),size(Y,1),1)).^2,1);
Rsq = 1 - (SS_res./SS_tot);
regData.res = res;
regData.Rsq = Rsq;
toc

%% After obtaining regression data and storing it in regData, represent the cells by colors in brain maps
% disp('Mapping colored beta values onto the brain...')
% tic
% B= regData.B;
% redVals = (B(:,2)-min(B(:,2)))/(max(B(:,2))-min(B(:,2)));
% grnVals = (B(:,3)-min(B(:,3)))/(max(B(:,3))-min(B(:,3)));
% blueVals =(B(:,4)-min(B(:,4)))/(max(B(:,4))-min(B(:,4)));
% [redMap, grnMap,blueMap] = deal(zeros(size(data.stack.avg)));
% N = numel(redMap(:,:,1));
% for cellInd = 1:numel(data.activeCells)
%     disp(['Cell #' num2str(cellInd)])
%     z = data.cells.info(data.activeCells(cellInd)).slice;
%     pxlInds  = N*(z-1)+ data.cells.info(data.activeCells(cellInd)).inds;  
%     redMap(pxlInds) = redVals(cellInd);
%     grnMap(pxlInds) = grnVals(cellInd);
%     blueMap(pxlInds) = blueVals(cellInd);
% end
% toc
% regMap = permute(cat(4,redMap,grnMap,blueMap),[1 2 4 3]);
% regMap(regMap ==0) =nan;






