% MakeLSMovie_MappedTensor - Script for making movies using Mapped Tensor
%                            class. AnalyzeEphysData must be run before
%                            running this script


%% Input variables
input_dir               = 'Y:\Avinash\SPIM\Registered\May 2015\5-2-2015_relaxed_dpf2\Fish1\G50_T1_20150502_154618';
nWorkers                = 10;
exposureTime            = 18e-3;
preShockFrames          = 4;
postShockFrames         = 10;

%% Run subroutine to load relevant variables
PreMovieRoutine


%% Specify 'indSets' here
% indSets={};
% % indSets{1} = shockStackIndMat{2};
% % indSets{2} = [shockStackIndMat{5}];
% indSets{1} = slowSwimStackInds;
% indSets{2} = fastSwimStackInds;

indSets={shockStackIndMat{2}; shockStackIndMat{3}; shockStackIndMat{4}; shockStackIndMat{5}};
setNames ={'justSubMotor'; 'justSupraMotor' ;'2ndHighestStim'; 'highestStim'};

% indSets={stack.inds.wkSwm; stack.inds.strngSwm};
% setNames ={'weakSwim'; 'strongSwim'};

nStacks = nTimePts;
%% Check for indices too close to the end or beginning and remove, if any.
for jj = 1:length(indSets)
    currentInds = [indSets{jj}];
    currentInds((currentInds - preShockFrames) < 1 | (currentInds + postShockFrames) > (nStacks-1)) = [];
    indSets{jj}  = currentInds;
end


%%

fName = data.filename(max(strfind(data.filename,'\'))+1:end);


%% Dealing with a prime number of z slices

nPlanes = stack.dim(3);
if numel(factor(nPlanes))==1 % Prime number
    while numel(factor(nPlanes))==1
        nPlanes = nPlanes+1;
    end
end
f = factor(nPlanes);
mi = floor(median(1:numel(f)));
nRows = prod(f(1:mi));
nCols = prod(f(mi+1:end));
% temp = [nRows nCols];
% nRows = min(temp);
% nCols = max(temp);% Suitable for a horizontally wider monitor such as mine


timeStamp = datestr(now,30);
aveResponseImg ={};
for thisSet = 1:length(indSets)
aveResponseImg{thisSet}=zeros(stack.dim(1),stack.dim(2), nPlanes, numel(-preShockFrames:postShockFrames));
end


%% Reading registered temporal stacks for 1 zplane at a time
registeredDir = input_dir;
tic
for z = 1:nPlanes
    disp(['Mapping Plane #' num2str(z) ' to a Tensor and Extracting Relevant Time Frames']);
    
    %     stack = double(read_LSstack_fast1(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']),[stack.dim(1) stack.dim(2)]));
    %     stack = stack(:,:,1:min(nStacks,size(stack,3)));
    stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']), [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
    %     stack = stack - backgroundStack; % Subtracting background offset so as to calculate accurate dF/F values    
    for thisSet = 1:length(indSets)
        indexStack = indSets{thisSet};
        indexStack(indexStack - preShockFrames <1)=[];
        indexStack(indexStack > nStacks-postShockFrames)=[]; % Remove indices too large to accommodate post-shock frames
        for i=0:(size(aveResponseImg{thisSet},4)-1)
            disp(['Z plane ' num2str(z) ', Stim/resp group ' num2str(thisSet), ', Time pt ' num2str(i+1)])
            aveResponseImg{thisSet}(:,:,z,i+1) = aveResponseImg{thisSet}(:,:,z,i+1)+ ...
                reshape(sum(double(stackMT(:,:,indexStack-preShockFrames+i)),3), [stack.dim(1) stack.dim(2) 1 1]);
        end
        aveResponseImg{thisSet}(:,:,z,:)=aveResponseImg{thisSet}(:,:,z,:)./length(indexStack);        
    end   
end
 toc

%% Saving aveResponseImg
fn = [ input_dir '\aveRespImg_' timeStamp];
fprintf('\n Saving aveResponseImg \n');
tic
save(fn,'aveResponseImg','-v7.3');
toc


%% Empty cell to separate previous parts of the code from the latter, since the former take a very long time and don't need to be repeated




%% Videowriting
if iscell(aveResponseImg)
aveResponseImg_super = aveResponseImg;
end
aveResponseImg =[];
baselineimg = [];
dff5secImg = [];

for thisSet = 1:length(indSets)
    aveResponseImg = aveResponseImg_super{thisSet};
    baselineImg = mean(aveResponseImg(:,:,:,1:preShockFrames),4);
    dff5secImg = mean(aveResponseImg(:,:,:,preShockFrames+1:preShockFrames+2),4)./baselineImg-1;
    movName = fName(1:strfind(fName,'.')-1);
    movName = [setNames{thisSet} '_' movName];
    
%     WriteLSVideo(aveResponseImg, baselineImg, dff5secImg, nRows, nCols, movName,input_dir)
    WriteLSVideo_4dHyperStack(aveResponseImg, baselineImg, dff5secImg, nRows, nCols, movName,input_dir) % For making 4D-hyperstack movies
    
end

