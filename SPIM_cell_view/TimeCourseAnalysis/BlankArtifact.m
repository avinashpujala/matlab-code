

function artlessSignal = BlankArtifact(signal,samplingInt, stimInds,preStimPeriod, postStimPeriod)

% BlankArtifact - Blank signal in specified peri-stimulus time window.
%   blankedSignal = BlankArtifact(signal,samplingInt,stimIndices,preStimPeriod, postStimPeriod); 
%   signal - signal vector
%   samplingInt - sampling interval
%   preStimPeriod - time period before stimulus to blank
%   postStimPeriod - time period after stimulus to blank

if size(signal,1) < size(signal,2)
    signal = signal';
end
sigmas = std(signal,[],1);
avgs = mean(signal,1);

preStimPts = round(preStimPeriod/samplingInt);
postStimPts = round(postStimPeriod/samplingInt);

artlessSignal = signal;

for stim = 1:numel(stimInds)
    fpt = round(max(stimInds(stim)-preStimPts,1));
    lpt = round(min(stimInds(stim) + postStimPts, length(signal)));
    noise = zeros(size(signal(fpt:lpt,:)));
    noise = repmat(avgs,size(noise,1),1) + 0.5*repmat(sigmas,size(noise,1),1).*randn(size(noise));
    artlessSignal(fpt:lpt,:) = noise;
end

