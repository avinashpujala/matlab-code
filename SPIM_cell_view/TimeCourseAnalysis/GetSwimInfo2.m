function data = GetSwimInfo2(varargin)
% GetSwimInfo2 - Extract swim parameters and append to data
%   structure
% data = GetSwimInfo(data,swimDetThr)
% Inputs:
% data - structure variable created by apEphysLoad
% swimDetThr - threshold for establishing swim episodes, in units of std.
% Outputs:
% data - the same structure variable with some additional fields appended

%% Detecting bursts & swim episodes; constructing swim rate timeseries
disp('Obtaining burst and swim info...')
tic
if nargin > 1
    swimDetThr = varargin{2};
else
    swimDetThr = 2; % Amp threshold in units of std for detecting swim episodes
end
data = varargin{1};
swimCountWin = 20; % For computing swim rate
refractoryPer = 500e-3; % For start of one swim episode and start of the next
postStimRefractoryPeriod = 4e-3;  % Time period after stim in which to ignore events (due to artifactual influence)
minOnPts = round(50e-3*data.samplingRate); % Mininum # of pts for which activity has to be > swimDetThr to be considered a swim episode
rateKer = ones(1,round(swimCountWin*data.samplingRate));
rateKer = rateKer/swimCountWin;
smKer = exp(-4*linspace(0,1,round(length(rateKer)/2)));
smKer = smKer/sum(smKer);
stim = data.stim;
stimInds = stim.inds;
stimTimes = data.t(stimInds);
stimIntervals = round(diff(stimTimes)/0.5)*0.5;
data.burst.train = zeros(size(data.smooth.burst));
data.burst.amp = data.burst.train;
if isfield(data,'swim')
    swim = data.swim;
else
    swim  = struct;
end
[swim.startInds, swim.endInds, swim.dur, swim.pow, swim.amp, swim.strength, swim.freq] = deal([]);
if size(data.smooth.burst,2) > 1
     diffSig = diff(data.smooth.burst,[],2);
else
   diffSig = data.smooth.burst(:,1);
end
sigmaXY = std(diffSig)^2;
[swim.bool,swim.rate] = deal(zeros(size(diffSig)));

dt = 1/data.samplingRate;
for ch = 1:numel(data.ePhysChannelToAnalyze)
    burstInds = FindBursts(data.smooth.burst(:,ch),dt,15e-3, 15, 15, 'fast');
    data.burst.train(burstInds,ch) = 1;
    data.burst.amp(burstInds,ch) = data.smooth.burst(burstInds,ch);
end
combSig = data.smooth.swim(:);
% minPks = findpeaks_hht(-combSig);
dt = mode(diff(data.t));
% time = data.t(:);
% minEnv = interp1([time(1)-dt; time(minPks); time(end)+dt],...
%     [0; combSig(minPks); combSig(end)], time,'cubic');
% combSig = combSig(:) - minEnv(:);

halfThresh = swimDetThr/2;

pks = findpeaks_hht(combSig);
pks(combSig(pks)< swimDetThr) =[];

vals = findpeaks_hht(-combSig);
vals(combSig(vals)> halfThresh) = [];
if vals(1) > pks(1)
    vals = [min(find(combSig>= halfThresh)); vals(:)];
end

swim.startInds  = zeros(size(vals));
for pk = 1:numel(pks)    
    swim.startInds(pk) = max(vals(vals < pks(pk)));
end
swim.startInds(swim.startInds==0)=[];

[~,swim.startInds] = RemovePeaksWithinRefractoryPeriod(ones(size(swim.startInds)),...
    swim.startInds,(refractoryPer*data.samplingRate));
int = round(data.samplingRate/300);
for sInd = 1:numel(swim.startInds)-1
    onInd = swim.startInds(sInd);
    nextOnInd = swim.startInds(sInd+1);
    seg = combSig(onInd:nextOnInd);
    offInds = find((seg(1:end-1) > halfThresh) & (seg(2:end)<= halfThresh)) + onInd;
    offInds((offInds - onInd) < minOnPts) = [];
    if isempty(offInds)
        offInds = nextOnInd;
    end
    offInd = offInds(1);
    swim.endInds(sInd) = offInd;
    swim.dur(sInd) = (offInd - onInd)*dt;
    swimSeg = combSig(swim.startInds(sInd)-10:swim.endInds(sInd));
    swim.pow(sInd) = mean(swimSeg);
    swim.amp(sInd) = max(swimSeg);
    swim.strength(sInd) = sum(swimSeg);
    swimSeg = diffSig(swim.startInds(sInd)-60:swim.endInds(sInd)+30);
    y = swimSeg(1:int:end);
    t = (0:length(y)-1)*(1/300);
    [swim.freq.mean(sInd), swim.freq.maxPow(sInd), swim.freq.max(sInd)] = GetFrequencies(y,t,sigmaXY);
    swim.bool(swim.startInds(sInd):swim.endInds(sInd)) = 1;
end
seg = combSig(swim.startInds(sInd+1):end);
offInds = find((seg(1:end-1) > swimDetThr) & (seg(2:end) <= swimDetThr)) + swim.startInds(sInd+1);
offInds((offInds - swim.startInds(sInd+1)) < minOnPts) = [];
if isempty(offInds)
    swim.endInds(sInd+1) = length(combSig);
else
    swim.endInds(sInd+1) = offInds(1);
end

swim.dur(sInd+1) = dt*(swim.endInds(sInd+1) - swim.startInds(sInd+1));
swimSeg = combSig(swim.startInds(sInd+1)-10:swim.endInds(sInd+1));
swim.pow(sInd+1) = mean(swimSeg);
swim.amp(sInd+1) = max(swimSeg);
swim.strength(sInd+1) = sum(swimSeg);
swimSeg = diffSig(max(swim.startInds(sInd+1)-60,1):min(swim.endInds(sInd+1)+30,length(diffSig)));
y = swimSeg(1:int:end);
t = (0:length(y)-1)*(1/300);
[swim.freq.mean(sInd+1),swim.freq.maxPow(sInd+1), swim.freq.max(sInd+1)] = GetFrequencies(y,t,sigmaXY);

swim.bool(swim.startInds(sInd+1):swim.endInds(sInd+1)) = 1;

disp([num2str(numel(swim.startInds)) ' swim episodes detected'])
swim.boolOn = zeros(size(swim.bool));
swim.boolOn(swim.startInds) = 1;

disp('Computing swim rate vector...')
swim.rate = conv2(conv2(swim.boolOn(:),rateKer(:),'same'),smKer(:),'same');

data.burst.train = BlankArtifactZero(data.burst.train,dt,stimInds, 5e-3, postStimRefractoryPeriod);

disp('Computing the temporal distance of each swim episode from the previous stim...')
swim.distFromLastStim = nan*ones(size(swim.startInds));
swim.times = data.t(swim.startInds);
for stimTime = 1:numel(stimTimes)-1
    inds = find((swim.times > stimTimes(stimTime)) & (swim.times < stimTimes(stimTime+1)-5));
    swim.distFromLastStim(inds) = swim.times(inds)-stimTimes(stimTime);
end
inds = find((swim.times > stimTimes(stimTime+1)) &(swim.times < stimTimes(stimTime+1)+stimIntervals(stimTime)-5));
swim.distFromLastStim(inds) = swim.times(inds)-stimTimes(stimTime+1);
toc
data.swim = swim;

    function [meanF,maxPowF,maxF] = GetFrequencies(x,t,sigmaXY)
        [Wxy,f,~,~] = ComputeXWT(x,x,t,[15 80],1/24,0,'all',sigmaXY);
%         oneMat = zeros(size(Wxy));
%         oneMat(Wxy>200) = 1;
        if sum(abs(Wxy(:))~=0)
            [~,gY] = gradient(abs(Wxy));
            [~,gYY] = gradient(gY);
            gYY = -gYY;
            gYY = gYY/max(gYY(:));
            Wxy(gYY <0.05) = 0;
            gps = mean(Wxy,2);
            gps = gps./(1./f(:))/max(f);
            gps(isnan(gps))=0;
            gps(isinf(gps)) = 0;
            meanF = sum(gps(:).*f(:))/sum(gps);
            maxPowInd = find(gps == max(gps));
            maxPowF = f(maxPowInd(1));
            maxFreqInd = min(find(gps>=5));
            if ~isempty(maxFreqInd)
                maxF = f(maxFreqInd);
                maxF = max(maxF);
            else
                maxF = meanF;
            end
        else
            meanF = 0;
            maxPowF = 0;
            maxF = 0;
        end
        
    end


end
