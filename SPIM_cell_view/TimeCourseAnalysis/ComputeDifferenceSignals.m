function y = ComputeDifferenceSignals(x,kerLen)
%  y = ComputeDifferenceSignals(x,kernelLength);
%ComputeDifferenceSignals Takes the difference of two signals
%   Detailed explanation goes here

if size(x,1) < size(x,2)
    x = x';
end

if size(x,2) ~=2
    errordlg('Input matrix must contain only two columns!')
   return;
end

y  = [];
y(:,1) = x(:,1) - x(:,2);
y(:,2) = x(:,2) - x(:,1);

y(y<0)= 0;

ker = sin(pi*linspace(0,1,kerLen)); % Sine kernel
ker = ker/sum(ker);

y = conv2(y,ker(:),'same');

end

