function [segmentedData, incompleteSegmentIndices] = SegmentDataByEvents(unsegmentedData,samplingInt,eventIndices, preEventPeriod, postEventPeriod)
% SegmentData - Segment continuous sweep data 
%   segmentedData = SegmentDataByEvents(unsegmentedData,samplingInt,eventIndices, preEventPeriod, postEventPeriod)
% 
% Inputs:
% unsegmentedData - A T-by-1 vector (timeseries) or a T-by-N matrix containing vectors to
%   be segmented. T = Number of time points, N = # of signals
% samplingInt - Sampling interval for the timeseries
% eventIndices - Events to use for segmentation
% preEventPeriod - Time period before event to include in segmented chunk
% postEventPeriod - "   "       after       "       "
% 
% Outputs:
%  segmentedData - Segments of data centered around eventIndices, inluding
%                   preEventPeriod and postEventPeriod. Segments are
%                   arranged as cell rows
% incompleteSegmentIndices - Indices of segments that fall short of the
%                           specified time period
% 
% Avinash Pujala, Koyama lab/HHMI, 2015

if nargin < 5
    errordlg('At least 5 inputs required!')
    return;
end

if size(unsegmentedData,1)< size(unsegmentedData,2)
    unsegmentedData = unsegmentedData'; % Assuming that there are more time points in data than channels.
end
samplingRate = round(1/samplingInt);

incompleteSegmentIndices = [];
preEventPts = eventIndices - round(preEventPeriod*samplingRate);
inds = find(preEventPts < 1);
incompleteSegmentIndices = [incompleteSegmentIndices; inds(:)];
preEventPts(preEventPts<1) = 1; % In case 1st event occurs before lapse of preEventPeriod

postEventPts = eventIndices + round(postEventPeriod*samplingRate);
inds = find(postEventPts > length(unsegmentedData));
incompleteSegmentIndices = [incompleteSegmentIndices; inds(:)];
postEventPts(postEventPts> length(unsegmentedData)) = length(unsegmentedData); % In case recording terminates before lapse of postStimPeriod after the last event


segmentedData = cell(numel(eventIndices),1);
for event = 1:numel(eventIndices)
    segmentedData{event} = unsegmentedData(preEventPts(event):postEventPts(event),:,:);
end


end

