
function varargout = FindClosestCellInPlane(cell_info,xyCoordinates,zPlane)
% 
% closestCell = FindClosestCell(cell_info,xyCoordinates, zPlane)
% [closestCell, xyzOffset] = FindClosestCell(...)
% 
% Inputs:
% cell_info - Cell-info matrix created by Takashi's code
% xyCoordinates - X-Y coordinates of the point in the specified z-plane
%   to which the closest cell is to be found
% zPlane -The z slice in which to look for cells.

allCellPos = zeros(length(cell_info),3);
cellInds = 1:length(cell_info);
for cc = 1:length(cell_info)
    allCellPos(cc,:) = [cell_info(cc).center cell_info(cc).slice];
end
inZCellInds = cellInds(allCellPos(:,3) == zPlane);

posMat = repmat([xyCoordinates,zPlane],length(inZCellInds),1);
[~,closestCell] = min(sqrt(sum((allCellPos(inZCellInds,:)-posMat).^2,2)));
closestCell = inZCellInds(closestCell);

varargout{1} = closestCell;
varargout{2} = allCellPos(closestCell,:) - [xyCoordinates zPlane];