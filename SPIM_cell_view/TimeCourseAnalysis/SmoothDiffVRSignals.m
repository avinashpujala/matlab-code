
function smSignal = SmoothDiffVRSignals(vrSignal,samplingInt,hpFilt,lpFilt);
% SmoothVRSignals - Custom smoothing of VR signals
% smSignal = SmoothVRSignals(rawVRSignal,samplingIng,hpFilt,lpFilt)


if nargin < 4
    errodlg('SmoothVRSignals requires atleast 4 inputs')
    return;
end


if size(vrSignal,1) < size(vrSignal,2)
    vrSignal = vrSignal';
end

if size(vrSignal,2)<2
    vrSignal(:,2) = zeros(size(vrSignal(:,1)));
end

kernelWidth = 0.5/lpFilt;
% kernel = hamming(round(kernelWidth/samplingInt));
% kernel = ones(round(kernelWidth/samplingInt),1);
lenKer = round(kernelWidth/samplingInt);
kernel = sin(pi*linspace(0,1,lenKer)); % Sin kernel
kernel = kernel/sum(kernel);

smSignal = chebfilt(vrSignal,samplingInt,hpFilt,'high');
smSignal(smSignal < 0) = 0; % Half-wave rectification
smSignal = conv2(smSignal,kernel(:),'same');

%%%%% Method 1
% smSignal(:,1) = zscore(smSignal(:,1))- zscore(smSignal(:,2));
% smSignal(:,2) = zscore(smSignal(:,2))- zscore(smSignal(:,1));
% smSignal(smSignal<0)=0;
% smSignal = zscore(smSignal.^2);

%%%%% Method 2
smSignal(:,1) = zscore(smSignal(:,1)).* -zscore(smSignal(:,2));
smSignal(:,2) = zscore(smSignal(:,2)).* -zscore(smSignal(:,1));
smSignal(smSignal<0)=0;
smSignal = zscore(smSignal);


