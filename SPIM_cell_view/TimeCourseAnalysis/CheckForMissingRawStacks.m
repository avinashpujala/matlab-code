
rawDir = 'Y:\Avinash\SPIM\Raw\5-9-2015_relaxed_dpf4\Fish3\G50_T1_IPI60_20150509_200754';
cd(rawDir)
D = dir;
fn = cell(size(D,1),1);
stackNum = zeros(size(D,1),1);
for f = 1:size(D,1)
    fn{f} = D(f).name;    
    if strfind(fn{f},'TM') & strfind(fn{f},'_CM')
        ind1= strfind(fn{f},'TM')+2;
        ind2 = strfind(fn{f},'_CM')-1;
        stackNum(f,:) = str2num(fn{f}(ind1:ind2));          
    end
end
stackNum(stackNum==0) = [];
stackNum = [0;stackNum(:)];

if any(diff(stackNum)~=1)
    errordlg('Some stacks missing!')
else
    disp('No missing stacks!')
end