
function peakInds = FindPeak(signalMat)
% FindOnset - Finds the indices of the onsets of the peaks with the sharpest rise
% signalMat - Input of dimensions M x N, where N is the number of time
% points and M is the number of signals

temp = sparse(signalMat);
% Identifying 5 maxima
for iter = 1:5
[~, maxInds] = max(temp,[],2);
temp(:,maxInds) = min(temp,[],2);
end


d2SignalMat = diff(signalMat,2, 2);
[~,slopeInds] = min(d2SignalMat,[],2);
slopeInds = slopeInds+1;


peakInds = peakInds+1;