function WriteDataToFile(fileName,trialSegData)

fn = [fileName, '.csv'];
outputfilename = fopen(fn,'w+');
fprintf(outputfilename,['StimulusOnset,StimulusAmplitude,LatencyOne,LatencyTwo,SwimDuration,SwimFrequency(Hz),SwimSpeed\n']);

for i = 1:numel(trialSegData)
    t = trialSegData(i);
    fprintf(outputfilename,'%d,%d,%d,%d,%d,%d,%d\n', t.stim.time,...
        t.stim.amps(2), t.swim.onset(1)*1000, t.swim.onset(2)*1000,...
        t.swim.dur*1000, t.Wxy.maxFreq, sqrt(t.swim.speed(1)*t.swim.speed(2)));

end 

fclose(outputfilename);
disp('Writing complete')