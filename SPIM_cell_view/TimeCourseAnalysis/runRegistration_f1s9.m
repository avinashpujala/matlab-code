
baseDirectory       = 'W:\Avinash\08-30-2014\f1s9_20140830_232020';
cameraIndex         = 0;
timeRange           = [0 1786];
referenceTimePoints = 885:894;
warpingSettings     = [100 150 6];
forceOverwrite      = 0;
dim=size(imread(fullfile(baseDirectory,'Background_0.tif')));
imSize=[dim(2) dim(1) 50];

poolWorkers         = 12;

%% processing loop

if matlabpool('size') > 0
    matlabpool('close');
end;
matlabpool(poolWorkers);
mkdir([baseDirectory,'/registered2/']);

for t = referenceTimePoints
    disp(['reading data at time point ' num2str(t)]);
    
    timeString = [repmat('0', 1, 5 - length(num2str(t))) num2str(t)];    
    stackName = [baseDirectory '/' 'TM' timeString '_CM0_CHN00.stack'];
        
    currentStack=double(read_LSstack_fast1(stackName,imSize(1:2)));
    
    if t == referenceTimePoints(1)
        stackAverage = currentStack;
    else
        stackAverage = stackAverage + currentStack;
    end
end

stackAverage = stackAverage / length(referenceTimePoints);


ROIs=set_LS_ROIs(stackAverage);

slices=ROIs.zrange(1):ROIs.zrange(2);
zSize = numel(slices);
xrange=ROIs.xrange(1):ROIs.xrange(2);
yrange=ROIs.yrange(1):ROIs.yrange(2);


parfor z = slices
%%for z = slices
    registerStacksMA_fast(z, baseDirectory, stackAverage(:,:,z), warpingSettings, imSize,timeRange, xrange, yrange);
end;

if matlabpool('size') > 0
    matlabpool('close');
end

%%
save(fullfile([baseDirectory, '/registered/'],'ROIs.mat'),'ROIs');