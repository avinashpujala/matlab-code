%% Concatenate wk and strng swm avg response for clustering responsive cells

toClstrData_wkStrng = [wkSwmCellResp(data.cells.responsiveCellInds,:) strngSwmCellResp(data.cells.responsiveCellInds,:)];

save(fullfile(dir_proc,'toClstrData_wkStrng.mat'),'toClstrData_wkStrng');

%% Cluster for entire trace
% toClstrData_entireTrace = data.cells.dFF(data.cells.responsiveCellInds,:);
% save(fullfile(dir_proc,'toClstrData_entireTrace.mat'),'toClstrData_entireTrace');

 %% Plotting clstrs - wkStrng
% clstrToPlt = [18:19];
% clrs = {'b','r'};
% 
% zeroPts = [];
% [~,zeroPts(1)] = min(abs(trialSegData(1).cells.time-0));
% zeroPts(2) = zeroPts(1) + length(trialSegData(1).cells.time);
% 
% time = (0:2*length(trialSegData(1).cells.time)-1)*stack.interval;
% xtl = [trialSegData(1).cells.time trialSegData(1).cells.time];
% 
% figure('Name',['Cluster # ' num2str(clstrToPlt)])
% 
% for c = 1:numel(clstrToPlt)
%     plot(time,kData.C(clstrToPlt(c),:),clrs{c})
%     hold on
% end
% 
% 
% xlim([-inf inf])
% ylim([-inf inf])
% sig = kData.C(clstrToPlt,:);
% sig = sig(:);
% maxY = max(sig);
% sh = stem(time(zeroPts), maxY*ones(2,1),'k--')
% set(sh,'marker', 'none')
% box off
% set(gca,'tickdir','out')
% xlabel('Time (s)'), ylabel('dF/F')
% title(['Cluster # ' num2str(clstrToPlt)])

%% Arrange cell responses by clstr idx and plot heat map of cell activity

% wkStrng_idx = [];
% idxVec = [];
% for i = 1:numel(unique(kData.idx))
%     cInds = find(kData.idx == i);
%     wkStrng_idx = cat(1,wkStrng_idx,toClstrData_wkStrng(cInds,:));
%     idxVec = cat(1,idxVec,kData.idx(cInds));
% end

%% Plotting clstrs - entireTrace
clstrToPlt = [11];
offset = 1;

figure('Name',['Cluster # ' num2str(clstrToPlt)])

for c = 1:numel(clstrToPlt)
    shift = offset*(c-1);
    plot(data.cells.time,kData.C(clstrToPlt(c),:)+ shift)
    hold on
end

shift = offset*c;
sig = data.smooth.exp(:,1);
sig = sig/max(sig);
plot(data.t(1:30:end),3*sig(1:30:end) + shift,'k')

xlim([-inf data.cells.time(end)])
ylim([-inf inf])
sig = kData.C(clstrToPlt,:);
sig = sig(:);
maxY = max(max(sig),shift);
clrList = {'b','g','r','c','m'};

sh = stem(data.cells.time(stack.inds.shock),maxY*ones(numel(stack.inds.shock),1),'r:')
set(sh,'marker','none')
% for grp = 1:numel(unique(tData.stimGroup))
%     nStim = sum(tData.stimGroup==grp);
% %     sh = stem(stim.times(tData.stimGroup == grp), maxY*ones(nStim,1),'color',clrList{grp});
%     sh = stem(stim.times(tData.stimGroup == grp), maxY*ones(nStim,1),'r:');
%     set(sh,'marker', 'none')
% end

box off
set(gca,'tickdir','out')
xlabel('Time (s)'), ylabel('dF/F')
title(['Cluster # ' num2str(clstrToPlt)])


