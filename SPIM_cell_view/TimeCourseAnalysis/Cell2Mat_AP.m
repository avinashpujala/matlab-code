function varargout = Cell2Mat_AP(cellArray)
% Cell2Mat_AP Converts a 2 X 2 cell array to a 2 X 2 matrix array by averaging values in
% cells with more than one entry
%  matrixArray = Cell2Mat_AP(cellArray);
% [matrixArray,sigArray]  = ...
% [matrixArray, sigArray, nArray] = ...
% 
% 
%  matrixArray: contains the values in cells or if a cell has > 1 entry then
%               their mean
% sigArray: contains the std of values in a cell
% nArray: contains the number of elements in a cell

matrixArray = zeros(size(cellArray));
sigArray = zeros((size(cellArray)));
nArray = zeros((size(cellArray)));
for cc = 1:size(cellArray,1)*size(cellArray,2)
    blah = [cellArray{cc}];
    if any(size(blah)>1)
        blah(blah==0)=[]; % Ignoring zero values when calculating mean
        val = mean(blah);
        sig = std(blah);
        N = numel(blah);  
    elseif any(size(blah)<1) % if empty
        val = 0;
        sig = 0;
        N = 0;
    else
        val = blah;
        sig = std(blah);
        N = numel(blah);
    end
    matrixArray(cc) = val;
    sigArray(cc) = sig;
    nArray(cc) = N;
end
varargout{1} = matrixArray;
varargout{2} = sigArray;
varargout{3} = nArray;

end

