% addpath('Z:\Code\MotorAdaptation');
% addpath('Z:\Code\EphysReader');
% addpath('Z:\Code\SPIMAnalysis');
addpath(genpath('C:\Users\pujalaa\Documents\MATLAB'));

ephysData = 'W:\Avinash\10-18-2014\Ephys\10-18-2014\Fish1\G50Tpt5.10chFlt';
sampleRate = 6000;
stimStep = 0.5;
stimGain = 10;
exposureTime = 18/1000; % In seconds
nSlices = 45;
nStack = numel(0:3118);

% data = apEphysLoad(ephysData);
swimData = swimCharacterize(data.(efn));

limitCycleDuringImaging = true;

%% stimulus stuff

% [pk loc] = findpeaks(data.leftStim,'MINPEAKHEIGHT',stimStep*0.9,'MINPEAKDISTANCE',6000*60);
loc = floor(onsets.(sfn)*6000)+1;
pk = amplitudes.(sfn);

stimMult = round(pk/stimStep);
stimRange = [min(stimMult):max(stimMult)];

% displayDuration = 5; % sec
% displayAmplitude = 0.01;
% figure;
for iStim = stimRange
%     subplot(numel(stimRange),1,iStim);
%     title(sprintf('Shock: %0.1f V',stimStep*iStim*stimGain));
%     ylim([0 displayAmplitude]);
%     hold on;
    matchCycle = find(stimMult==iStim);
    sortedCycle{iStim} = matchCycle;
%     for iCycle = 1:numel(matchCycle)
%         plot([0:(displayDuration*sampleRate)]/sampleRate,swimData.fltCh(loc(matchCycle(iCycle))+[0:displayDuration*sampleRate]));
%     end
end

%%
% camTrigger
% [~,stackIndex] = findpeaks(data.camTrigger,'MINPEAKHEIGHT',3.9,'MINPEAKDISTANCE',floor(exposureTime*nSlices*sampleRate));
stackIndices = round(stackTimes*samplingRate)+1;

% finding stackNo corresponding to the shock onset.
cycleToStackNo = zeros([1,numel(loc)]);
for iCycle = 1:numel(loc)
    [~,cycleToStackNo(iCycle)] = min(abs(stackIndices-loc(iCycle)));
end

%%
preShockFrames = -5;
postShockFrames = 10;

fastResponseCycles = [sortedCycle{4}];
slowResponseCycles = [sortedCycle{2}];
noResponseCycles = [sortedCycle{1}];

fastResponseCycles(cycleToStackNo(fastResponseCycles) > nStack) = [];
slowResponseCycles(cycleToStackNo(slowResponseCycles) > nStack) = [];
noResponseCycles(cycleToStackNo(noResponseCycles) > nStack) = [] ;

registeredDir = 'X:\Avinash\08-30-2014\f1s9_20140830_232020\Registered';
% load(fullfile(registeredDir,'ROIs.mat'));

% dim=[ROIs.yrange(2)-ROIs.yrange(1)+1, ROIs.xrange(2)-ROIs.xrange(1)+1,ROIs.zrange(2)-ROIs.zrange(1)+1];
avefastResponseImg=zeros(dim(1), dim(2), dim(3), numel(preShockFrames:postShockFrames));
indexStack = cycleToStackNo(fastResponseCycles(1:end-1));
   

for z = 1:3 % dim(3)
    disp(num2str(z));
    tic
    stack = read_LSstack_fast1(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']),[dim(1) dim(2)]);
    toc
    for i=0:(size(avefastResponseImg,4)-1)
        avefastResponseImg(:,:,z,i+1) = avefastResponseImg(:,:,z,i+1)+ reshape(sum(double(stack(:,:,indexStack+preShockFrames+i)),3), [dim(1) dim(2) 1 1]);
    end
    avefastResponseImg(:,:,z,:)=avefastResponseImg(:,:,z,:)./length(indexStack);
end

baselineImg = mean(avefastResponseImg(:,:,:,1:5),4);
dff5secImg = mean(avefastResponseImg(:,:,:,6:7),4)./baselineImg-1;


figure('units','pixels','outerposition',[0 0 2560 1600]);
writerObj = VideoWriter('f1s9_dff_fastResponse_gray.avi','Uncompressed AVI');
writerObj.FrameRate = 5;
open(writerObj);

for i = 1:size(avefastResponseImg,4)
    f1 = (avefastResponseImg(:,:,:,i)./baselineImg)-1;
    gcf;
    h = imagesc(reshape(permute(reshape(f1,[dim(1) dim(2) 9 5]), [1 4 2 3]), [dim(1)*5, dim(2)*9]),[-0.2 0.5]);
    
    axis off;
    axis image;
    title(['frame no: ', int2str(i)]);
    colorbar;
    colormap gray; 
    set(gca,'Units','normalized','Position',[0.025 0.025 0.95 0.95]);
%     F(i) = getframe(gcf);
    frame = getframe(gcf);
    writeVideo(writerObj,frame);
end

% movie(F,20);
close(writerObj);

%%
dff = zeros(dim(1)*5, dim(2)*9,size(avefastResponseImg,4));
for i = 1:size(avefastResponseImg,4)
    f1 = (avefastResponseImg(:,:,:,i)./baselineImg)-1;
    dff(:,:,i) = reshape(permute(reshape(f1,[dim(1) dim(2) 9 5]), [1 4 2 3]), [dim(1)*5, dim(2)*9]);
end

fh = fopen('dff.bin','w');
fwrite(fh, dff,'double');
fclose(fh);
%%
stflat = zeros(dim(1)*5, dim(2)*9,size(avefastResponseImg,4));
for i = 1:size(avefastResponseImg,4)
    stflat(:,:,i) = reshape(permute(reshape((baselineImg-min(baselineImg(:)))/max(baselineImg(:)),[dim(1) dim(2) 9 5]), [1 4 2 3]), [dim(1)*5, dim(2)*9]);
end

fh = fopen('stflat.bin','w');
fwrite(fh, stflat,'double');
fclose(fh);

%%
dffPlus= zeros(dim);dffNegative = zeros(dim);
plusIndex = dff5secImg(:)>0.02;
negIndex = dff5secImg(:)<-0.02;
dffPlus(plusIndex) = dff5secImg(plusIndex);
dffNegative(negIndex) = -dff5secImg(negIndex);

nor_dffPlus = dffPlus/0.5;
nor_dffNegative = dffNegative/0.5;

figure('position', [0 0 1600*dim(2)/dim(1) 1600]);
imagesc(baselineImg(:,:,24));
axis image;
axis off;
set(gca,'Units','normalized', 'Position', [0 0 1 1]);

writerObj = VideoWriter('f1s9_fastResponseMap.avi','Uncompressed AVI');
writerObj.FrameRate = 1;
open(writerObj);
stImg = (baselineImg-min(baselineImg(:)))/max(baselineImg(:));
for i = 1:size(baselineImg,3)
    imagesc(stImg(:,:,i),[0 0.5]);
    colormap(gray);
    axis image;
    axis off;
    set(gca,'Units','normalized','Position',[0 0 1 1]);
    hold on;
    
    red = cat(3, ones(dim(1:2)),zeros(dim(1:2)),zeros(dim(1:2)));
    green = cat(3, zeros(dim(1:2)),ones(dim(1:2)),zeros(dim(1:2)));
    
    h_red = imshow(red);
    set(h_red,'AlphaData',nor_dffPlus(:,:,i));
    
    h_green = imshow(green);
    set(h_green,'AlphaData',nor_dffNegative(:,:,i));
    hold off;
    
    frame = getframe;
    writeVideo(writerObj,frame);
end

close(writerObj);


