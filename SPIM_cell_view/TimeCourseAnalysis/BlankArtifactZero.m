

function out = BlankArtifactZero(signal,samplingInt, stimInds,preStimPeriod, postStimPeriod)

% BlankArtifactzero - Blank points around specified peri-stimulus time window.
%  blankedSignal = BlankArtifactZero(signal,samplingInt,stimIndices,preStimPeriod, postStimPeriod); 


if size(signal,1) < size(signal,2)
    signal = signal';
end

preStimPts = round(preStimPeriod/samplingInt);
postStimPts = round(postStimPeriod/samplingInt);

out = signal;

for stim = 1:numel(stimInds)
    fpt = round(max(stimInds(stim)-preStimPts,1));
    lpt = round(min(stimInds(stim) + postStimPts, length(signal)));
    noise = zeros(size(signal(fpt:lpt,:,:)));
    out(fpt:lpt,:,:) = noise;
end

