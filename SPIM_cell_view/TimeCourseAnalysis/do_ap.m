function do_ap(varargin)
% My modifications to Minoru's code
javaaddpath('C:\Program Files\ImageJ\ij.jar');
if nargin ==0
    [file,path] = uigetfile('*.mat');
    blah = matfile(fullfile(path,file));
else
    blah = varargin{1};
end


try
    if isdir(blah)
        disp('Opening matfile...')
        pyDir = blah;
        pyData = matfile(fullfile(pyDir,'pyData.mat'));
    elseif exist(blah)==2
        pyData = matfile(blah);
    end
catch
    pyData = blah;
end

tic
disp('Reading data...')
eData = pyData.data;
disp('Reading trial data...')
eData.trialSegData = pyData.trialSegData;
eData.tData = pyData.tData;
disp('Reading stack info...')
eData.stack = pyData.stack;
disp('Reading activity map and cell data...')
eData.activityMap = pyData.activityMap;
eData.cells = pyData.cells;
toc
samplingRate = 6000;
si = 1/samplingRate;
ep_t = eData.t;
rootSigs = [std(eData.smooth.burst(:,1)) std(eData.smooth.burst(:,2))];
[~,betterRoot] = max(rootSigs);

%% image
imd = [];
imd = eData.activityMap;
dim = size(imd);
imd_orig = imd;

%% Stack processing
im_t = eData.t(eData.stack.inds.start);
if length(im_t) > size(eData.cells.dFF,2)
    im_t(end) = [];
end

%% ui
activeCursorFlag = 0;
playing = false;
parameterPlotFlag = false;
hf = figure('Name','Activity Map','Position',[50 500 1100 600]);
t = 1;
z = 23;
radius = 2;
trl = 2;
cellInd = 2;
trlModeFlag = 0;

set(gcf, 'WindowButtonMotionFcn', @mouseMove);
set(gcf, 'WindowButtonDownFcn', @inactivateCursor);
h_slider = uicontrol(hf,'Style','slider','Units','normalized',...
    'Position', [0.35 0.5 0.3 0.04],'SliderStep',[1/(dim(4)-1) 1/(dim(4)-1)],'Callback',@slider_callback);
h_text = uicontrol(hf,'Style','text','Units','normalized','Position', [0.45 0.48 0.08 0.04]);axis off;

h_radio_trial= uicontrol(gcf,'Style', 'radiobutton','String','Trl Mode','Units','Normalized',...
    'Position',[0.85 0.5 0.14 0.05],'HandleVisibility','off','Callback', @trialModeRadioCallback);
h_txt_trialNum = uicontrol(gcf,'Style','edit','String','Trl #', 'Units','Normalized',...
    'Position',[0.85 0.55 0.14 0.05], 'Callback', @trlNumTxtCallback);

h_checkboxCursor = uicontrol('Style', 'checkbox', 'String', 'Activate','Units','Normalized',...
    'Position', [0.15 0.48 0.12 0.05],'Max',1,'Min',0,...
    'Value',0,...
    'Callback', @activateCursor);


ax_movie = subplot(3,1,1);
ax(1) = subplot(3,1,2);
ax(2) = subplot(3,1,3);

set(ax_movie, 'position',[0.1 0.55 0.8 0.4],'xtick',[], 'ytick',[]);
set(ax(1),'position',[0.1 0.3 0.8 0.15],'tickdir','out')
set(ax(2),'position',[0.1 0.1 0.8 0.15],'tickdir','out')

axes(ax_movie)
imHandle = imagesc(imd(:,:,:,z));axis image, axis off
set(h_text, 'String',['z=',num2str(z)]);

axes(ax(2))
% vrRange = [-20 max(eData.smooth.burst(:))];
vrRange = [-inf inf];
% plot(eData.t(1:50:end),eData.smooth.burst(1:50:end, betterRoot));
plot(eData.t(1:50:end),eData.smooth.swim(1:50:end))
yMax = max(eData.smooth.swim);
% ylim(vrRange);
ylim([-inf 0.7*yMax])
h_timeline = line(im_t(t)*[1 1],get(gca,'YLim'),'Color',[.5 .5 .5]);
hold on;
plot(eData.t(eData.stim.inds),500*ones(size(eData.stim.inds)),'r*')
zoom xon; pan xon;
box off

linkaxes(ax,'x');
axes(ax(2));
xlim([-Inf Inf]);

epi =[]; vr=[];

%% utility functions
    function out=normalize(img)
        threshold = median(img(:));
        out=(img-threshold)./(threshold-min(img(:)));
    end

    function im_out = svd_denoise(im_in,cut)
        dim = size(im_in);
        Y = reshape(double(im_in),dim(1)*dim(2), dim(3));
        ameanY=mean(Y')';
        Y2=Y-repmat(ameanY,[1,dim(3)]);
        [U,S,V]=svd(Y2,0);
        
        s=diag(S);
        s(cut:end)=0;
        S0=diag(s);
        
        Y0=U*S0*V';
        
        im_out = reshape(repmat(ameanY,[1,dim(3)])+Y0,dim);
        
    end

%% callbacks
    function toggleParameterPlot_callback(src,evt)
        state = get(src,'Value');
        if state == 1
            parameterPlotFlag = true;      
            updateVR;
        elseif state ==0
            parameterPlotFlag = false;   
            updateVR;
        end
    end


    function hotspotMap_callback(src,evt)
        sm_imd = convn(imd,ones(3)/9);
        norm_sm_imd = zeros(dim);
        cumPosdFF = zeros(dim(1:2));
        for i = 1:dim(1)
            for j = 1:dim(2)
                temp = normalize(sm_imd(i,j,:));
                norm_sm_imd(i,j,:) = temp;
                aho = squeeze(temp);
                cumPosdFF(i,j) = sum(aho(find(aho>0)));
            end
        end        

        
        figure;
        imagesc(cumPosdFF.*norm_aveImg);
        axis image; axis off;
    end

    function slider_callback(src,evt)
        v = get(src,'Value');
        z = round(v*(dim(4)-1))+1;    
        axes(ax_movie);
        imagesc(imd(:,:,:,z)); axis off; axis image;      

        set(h_text,'String', ['z=',num2str(z)]);  
        
    end

    function updateRadius(src,evt)
        radius = str2num(get(src, 'String'));
    end

    function activateCursor(src,evt)
        if get(src,'Value') == 1
            activeCursorFlag = 1;
        elseif get(src,'Value') == 0
            activeCursorFlag = 0;
        end
    end

    function inactivateCursor(src, evt)
        activeCursorFlag = 0;
        set(h_checkboxCursor,'Value',0);
    end

    function plotTimecourse(src,evt)
        axes(ax_im);
        [BW] = roipoly;
        axes(ax(1));
        flat_imd = reshape(imd, [],dim(3));
        ts = mean(flat_imd(find(BW),:),1);
        plot(im_t,ts);
    end

    function trlNumTxtCallback(hObj,evt)
        trl = str2double(get(hObj,'String'));
        if numel(trl) > 1
            disp('Can only display 1 trial at a time')
            trl = trl(1);
        elseif trl > size(eData.trialSegData,1)
            disp(['Trial # outside range; max trial = ' size(eData.trialSegData,1)])
        end
    end

    function trialModeRadioCallback(hObj,evt)
        if get(hObj,'value') == get(hObj,'Max')
            trlModeFlag = 1;
        else
            trlModeFlag = 0;
        end   
    end

    function mouseMove(src,evt)
        if activeCursorFlag
            C = get(ax_movie, 'CurrentPoint');
            xrange = get(ax_movie,'XLim');
            yrange = get(ax_movie,'YLim');
            if  xrange(1)+radius < C(1,1) && C(1,1)< xrange(2)-radius && yrange(1)+radius < C(1,2) && C(1,2)< yrange(2)-radius
                currX = floor(C(1,1));
                currY = floor(C(1,2));
                title(ax_movie, ['(X,Y) = (', num2str(currX),', ', num2str(currY), '), radius = ', num2str(radius)]);        
                axes(ax(1));
                cla
                cursorPos = [C(1,1), C(1,2)];
                [cellInd,xyzOffset] = FindClosestCellInPlane(eData.cells.info,cursorPos,z);
                distToClosestCell = sqrt(sum((0.406*xyzOffset(1:2)).^2));
                ts = eData.cells.dFF(cellInd,:);
                if distToClosestCell > 15
                    disp('Nearest cell > 15 microns away')
                    axes(ax(1)), cla
                    plot(nan*zeros(length(ts),1),nan*zeros(length(t),1),'.')
                    hold on
                    %plot(currX,currY,'*')
                elseif trlModeFlag
                    axes(ax(1))
                    cla
                    plot(eData.trialSegData(1).cells.time, eData.trialSegData(trl).cells.dFF(cellInd,:));
                    box off
                    set(gca,'tickdir','out')
                    xlim([-10 40])
                    ylim([-inf inf])
                    title(['Trial # ' num2str(trl)])
                    
                    axes(ax(2))
                    cla
                    plot(eData.trialSegData(1).time,eData.trialSegData(trl).smooth.burst(:,1))                    
                    box off
                    set(gca,'tickdir','out')
                    ylim(vrRange)
                    title(['Trial # ' num2str(trl)])
                elseif trlModeFlag == 0
                    axes(ax(1))
                    cla
                    plot(eData.cells.time,eData.cells.dFF(cellInd,:),'k');
                    hold on
                    plot(eData.t(eData.stim.inds),0.25*ones(size(eData.stim.inds)),'r*')  
                    title(ax_movie, ['(X,Y) = (', num2str(currX),', ', num2str(currY), '), cell idx = ' num2str(cellInd)]);
                    ylim([-inf inf])
                    disp(cellInd)
                                       
                    axes(ax(2))
                    cla
%                     plot(eData.t(1:50:end), eData.smooth.burst(1:50:end,betterRoot))
                   plot(eData.t(1:50:end), eData.smooth.swim(1:50:end)) 
                   yMax = max(eData.smooth.swim);
                    box off
                    set(gca,'tickdir','out')
                    hold on
                    plot(eData.t(eData.stim.inds),1000*ones(size(eData.stim.inds)),'r*')
                    ylim([-inf 0.7*yMax])
                end
            end
        end
    end
end

