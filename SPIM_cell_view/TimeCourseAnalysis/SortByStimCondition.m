function [outVar] = SortByStimCondition(inVar,stimInds, stimConditionsVec);
% SortByStimCondition - Creates an output array based on unique stim
%                       conditions
% outVar = SortByStimCondition(inVar,stimInds, stimConditionsVec)
%  
if size(stimConditionsvec)
end
nConditions = numel(unique(stimAmp(1,:))) * numel(unique(stimAmp(2,:)));
stim1Conditions = unique(stimAmp(1,:));
stim2Conditions = unique(stimAmp(2,:));
trialMat = cell(numel(stim1Conditions),numel(stim2Conditions));
trialMat_smooth = trialMat;
burstTrainMat = trialMat;
onMat = trialMat;
freqMat = trialMat;
shockStackIndMat = trialMat;
for one = 1:numel(stim1Conditions);
    for two = 1:numel(stim2Conditions)
        trialInds = find(stimAmp(1,:)==stim1Conditions(one) &  stimAmp(2,:) == stim2Conditions(two));
        trialMat{one,two} = trialSegmentedData(trialInds);
        trialMat_smooth{one,two} = trialSegData_smooth(trialInds);
        burstTrainMat{one,two} = trialSegBurstTrain(trialInds);
        onMat{one,two} = swimOnset(trialInds);
        freqMat{one,two} = swimFreq(trialInds);
        shockStackIndMat{one,two} = shockStackInds{trialInds};
    end
end



end

