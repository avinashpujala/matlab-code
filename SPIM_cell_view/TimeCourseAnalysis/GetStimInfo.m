function data = GetStimInfo(data)
%GetStimInfo Get information about stimulus from data structure variable
%   created by apEphysLoad.m
% Inputs:
% data - structure variable created by AnalyzeEphysData.m

%% Getting stimulus indices & estimating inter-stim interval
if isfield(data,'stim')
    stim = data.stim;
end
stim.activeCh = find(stim.isolGain);
stim.inds = cell(numel(stim.activeCh),1);
stim.amps = stim.inds;
stim.times = stim.inds;
stimAmps = cell(size(stim.isolGain));
stimInds = stimAmps;
stimTimes = stimAmps;
for stimChan = stim.activeCh   
        disp(['Getting stimulus info from channel ' num2str(stimChan) '...'] )
        stimInds{stimChan} = FindStimPulses(data.(['stim' num2str(stimChan)]),20,30,0.8*stim.minExpInt);
        disp(['Found ' num2str(numel(stimInds{stimChan})) ' stimuli'])        
        temp = data.(['stim' num2str(stimChan)]);        
        stimAmps{stimChan} = temp(stimInds{stimChan});
        stimAmps{stimChan} = stim.isolGain(stimChan)*(round(stimAmps{stimChan}/0.05)*0.05);
        if any(stimAmps{stimChan}>100)
            warning('Some stimulus pulses seem to be exceeding 100V, please check this!')
            stimAmps{stimChan}(stimAmps{stimChan}>100) = 100; % Since stim isol does not allow above 100V
        end
         stimInds{stimChan} = stimInds{stimChan}-1;  % Onset occurs at least 1 sample pt before peak 
         stimTimes{stimChan} = data.t(stimInds{stimChan});
end

stim.inds =  stimInds;
stim.amps = stimAmps;

blah = zeros(length(stim.amps),length(data.t));
for stimChan = 1:numel(stim.activeCh)
    blah(stim.activeCh(stimChan),stim.inds{stim.activeCh(stimChan)}) = stim.amps{stim.activeCh(stimChan)};
end
stimInds = sort([stim.inds{:}]);
dblCntInds = find(diff(stimInds)< 0.8*stim.minExpInt) + 1;
stimInds(dblCntInds) = [];
nonStimInds = setdiff(1:length(blah),stimInds);
blah(:,nonStimInds) = [];
stim.inds  = stimInds;
stim.amps = blah;

stim.times = data.t(stim.inds);
stim.interval.min = round(min(diff(unique(stim.times))));
stim.interval.med = round(median(diff(unique(stim.times))));
stim.interval.max =  round(max(diff(unique(stim.times))));

data.stim = stim;

end

