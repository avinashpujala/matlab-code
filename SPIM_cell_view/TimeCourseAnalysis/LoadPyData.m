
inputDir   = 'Z:\SPIM\Avinash\Raw\May 2015\5-9-2015_relaxed_dpf4\Ephys\Fish2';
fileName   = 'pyData.mat';

%% Load pyData.mat
tic
fprintf('\nLoading data...')
load(fullfile(inputDir,fileName));
data = pyData;
trialSegData = data.trialSegData;
tData = data.tData;
swim = data.swim;
stim = data.stim;
clear pyData;
fprintf('done!\n')
toc