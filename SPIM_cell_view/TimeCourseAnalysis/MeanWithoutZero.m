function [mu1, mu2] = MeanWithoutZero(inputMatrix)
%MeanWithoutZero Computes mean along both dimensions of a matrix by
%                excluding zero entries
%  [mu1, mu2] = MeanWithoutZero(inputMatrix);
%  mu1 = mean along row dimension
%  mu2 = mean along col dimension

mu1 = zeros(1,size(inputMatrix,2));
for cc = 1:size(inputMatrix,2)
    col = inputMatrix(:,cc);
    col(col==0)=[];
    colMean = mean(col);
    mu1(1,cc) = colMean;
end


mu2 = zeros(size(inputMatrix,1),1);
for rr = 1:size(inputMatrix,1)
    row = inputMatrix(rr,:);
    row(row==0)=[];
    rowMean = mean(row);
    mu2(rr,1) = rowMean;
end


end

