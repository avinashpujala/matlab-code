function ExamineInstSwimParamsForTrl(trialNum, trialSegData)
% ExamineInstSwimParamsForTrl - Plots out all the time-varying swim params
%   for stim-evoked swim episode.
% Inputs:
% trl - trial number
% trialSegData - Trial segmented data struct created by AnalyzeEphysData

trl = trialNum;
xLim = [-0.2 0.5];

% Signal
figure('Name',['Trial ' num2str(trl)])
subplot(4,1,1)
cla
plot(trialSegData(1).time, trialSegData(trl).smooth.sine)
legend('Contra VR','Ispi VR')
xlim(xLim)
tInds = find(trialSegData(1).time >= xLim(1) & trialSegData(1).time <= xLim(2));
maxY = max(max(trialSegData(trl).smooth.sine(tInds,:)));
ylim([-inf maxY])
hold on
sh = stem(0,maxY,'k--');
set(sh,'marker','none')
th = text(-0.08,0.85*maxY, ['Grp:' num2str(trialSegData(trl).stim.grp)]);
set(th,'color','r')
box off
set(gca,'tickdir','out')
ylabel('Amp')

title(['Trial ' num2str(trl)])

% Instantaneous freq
subplot(4,1,2), 
tInds_Wxy = find(trialSegData(1).Wxy.time >= xLim(1) & trialSegData(1).Wxy.time <= xLim(2));
plot(trialSegData(1).Wxy.time(tInds_Wxy), trialSegData(trl).Wxy.tvFreq_alt(tInds_Wxy),'.')
hold on
plot(trialSegData(1).Wxy.time(tInds_Wxy), trialSegData(trl).Wxy.tvFreq_sync(tInds_Wxy),'r.')
legend('Alt', 'Sync')
sh = stem(0,100,'k--');
set(sh,'marker','none')
xlim(xLim)
ylim([-inf 100])
box off
set(gca,'tickdir','out')
ylabel('Freq (Hz)')


% Instantaneous max pow
subplot(4,1,3)
plot(trialSegData(1).Wxy.time(tInds_Wxy), trialSegData(trl).Wxy.tvPow_alt(tInds_Wxy).^2,'.')
hold on
plot(trialSegData(1).Wxy.time(tInds_Wxy), trialSegData(trl).Wxy.tvPow_sync(tInds_Wxy).^2,'r.')
sh = stem(0,700.^2,'k--');
set(sh,'marker','none')
xlim(xLim)
ylim([-inf inf])
box off
set(gca,'tickdir','out')
ylabel('Pow')
% legend('Alt', 'Sync')

% Instantaneous phase
subplot(4,1,4)
cla
plot(trialSegData(1).Wxy.time(tInds_Wxy), trialSegData(trl).Wxy.tvPhase_alt(tInds_Wxy),'.')
hold on
plot(trialSegData(1).Wxy.time(tInds_Wxy), trialSegData(trl).Wxy.tvPhase_sync(tInds_Wxy),'r.')
% legend('Alt', 'Sync')
plot(trialSegData(1).Wxy.time(tInds_Wxy),ones(size(trialSegData(1).Wxy.time(tInds_Wxy)))*180,'k--')
sh = stem(0,360,'k--');
set(sh,'marker','none')
xlim(xLim)
ylim([-inf 360])
set(gca,'ytick',[90 180 270],'tickdir','out')
box off
ylabel('Phase (deg)')
shg

figure('Name',['Trial ' num2str(trl)])
PlotWxy(trialSegData(trl).Wxy.coeffs(:,tInds_Wxy),trialSegData(1).Wxy.time(tInds_Wxy),trialSegData(1).Wxy.freqVec)
