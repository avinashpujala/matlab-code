function varargout = MapCellIndsToAveStack_MaxIntProj(varargin)
%MapCellsIndsToAveStack_MaxIntProj  A function that maps cell indices to 
% their location on the averaged image stack
% 
% fh = MapCellsToAveStack(cellIndices, cellInfo,aveStack);
% fh = MapCellsToAveStack(cellIndices, cellInfo,aveStack, colorValues);
% 
% Inputs:
% cellInds = Vector of indices of cells to be mapped
% cellInfo = Matrix containing cell info (as created by TK's code)
% avgStack = Stack of temporally averaged registered planes onto which
%            the cells are to be mapped
% colorValues = Colors assigned to values in cellInds; size(colorValues) = [numel(cellInds), 3];
%                 if colorValues is not specified, then the same color is assigned
%                 to all cells (default = red)
% Outputs:
% fh = figure handle

if nargin < 3
    error('Minimum 3 inputs required')
    return
elseif nargin < 4
    colorValues = repmat([1 0.1 0.3],numel(varargin{1}),1);
else
    colorValues = varargin{4};
end
cellInds = varargin{1};
cellInfo = varargin{2};
avgStack = varargin{3};

anatomy_image_yx = repmat(imNormalize99(max(avgStack,[],3)),[1 1 3]); % Normalized max-intensity projected image in 3 layers
anatomy_image_yz = repmat(imNormalize99(squeeze(max(avgStack,[],2))),[1 1 3]);
anatomy_image_yx_ori = anatomy_image_yx;
anatomy_image_yz_ori = anatomy_image_yz;
dimv_yx = size(anatomy_image_yx);
dimv_yz = size(anatomy_image_yz);

anatomy_image_yz2=zeros(dimv_yz(1),dimv_yz(2)*10,3);
anatomy_image_yz2_ori=anatomy_image_yz2;
tot_image=zeros(dimv_yx(1),dimv_yx(2)+dimv_yz(2)*10+10,3);
tot_image(:,dimv_yx(2)+(1:10),:)=1;

circle = makeDisk2(7,15);
[r, v]=find(circle);
r=r-8;v=v-8;
circle_inds  = r*dimv_yx(1)+v;
yzplane_inds = -5:5;



%% Going through all cells
for j=1:length(cellInds)    
    cinds=(cellInfo(cellInds(j)).center(2)-1)*dimv_yx(1)+cellInfo(cellInds(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    lut = colorValues(j,:);
    anatomy_image_yx(cinds+circle_inds(labelinds))=lut(1);
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=lut(2);
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=lut(3);
    
    cinds=(cellInfo(cellInds(j)).slice-1)*dimv_yz(1)+cellInfo(cellInds(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=lut(1);
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=lut(2);
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=lut(3);

end


for k=1:3
    anatomy_image_yz2(:,:,1)=imresize(anatomy_image_yz(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,2)=imresize(anatomy_image_yz(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,3)=imresize(anatomy_image_yz(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,1)=imresize(anatomy_image_yz_ori(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,2)=imresize(anatomy_image_yz_ori(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,3)=imresize(anatomy_image_yz_ori(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
end


tot_image(:,1:dimv_yx(2),:)=anatomy_image_yx;
tot_image(:,dimv_yx(2)+11:end,:)=anatomy_image_yz2;


tot_image(tot_image(:)>1)=1;
tot_image(tot_image(:)<0)=0;
fh = figure('position',[50 50 1000 1000]);
image(tot_image);colormap gray;
hold on


varargout{1} = fh;

end

