function CropTStacksAndRewrite(varargin)
% CropTStacksAndRewrite - Loads max-int projection of images along the
%   z-dimension, allows the user to specify edges for cropping and then
%   rewrites images with new dimensions
%
% CropTStacksAndRewrite(imgDir,imgDims,fileExt)
% Inputs:
% imgDir = Path to directory containing the images
% fileExt = File extension, i.e., '.bin' or '.stack'

%% Fixed parameters
nWorkers = 10;

%% Check inputs
imgDir = varargin{1};
if nargin < 2
    fileExt = '.stack';
elseif nargin == 2
    fileExt = varargin{2};
    if ~isstr(fileExt)
        error('2nd input (file extension) must be a string!');
    end
elseif nargin > 2 
    error('Incorrect # of inputs!');
end

%% Get image dimensions
blah = xml2struct(fullfile(imgDir,'ch0.xml'));
stackDimStr = blah.push_config.info{14}.Attributes.dimensions;
xInds = strfind(stackDimStr,'x');
stackDims(1) = str2double(stackDimStr(1:xInds(1)-1));
stackDims(2) = str2double(stackDimStr(xInds(1)+1:xInds(2)-1));
stackDims(3) = str2double(stackDimStr(xInds(2)+1:end));

fid = fopen(fullfile(imgDir,'Stack_frequency.txt'),'r');
info = fscanf(fid,'%f');
stackDims(4) = info(3);
fclose(fid);

%% Read single time point for each plane and create max-int image
avgStack = zeros(stackDims(1:3));
zList = 1:stackDims(3);
matlabpool(nWorkers)
parfor z = zList
    fileName = ['Plane' num2str(z, '%0.2d') fileExt];
    disp(fileName)
    stack = MappedTensor(fullfile(imgDir,fileName),[stackDims(1) stackDims(2) stackDims(4)],'class','uint16');
    avgStack(:,:,z) = stack(:,:,z);
end
matlabpool close

maxIntImg = max(avgStack,[],3);

%% Cropping Image
figure('Name','Cropping Image')
imagesc(maxIntImg), axis image, colormap(gray)
cLim = get(gca,'clim');
set(gca,'clim',[1.1*cLim(1) 0.4*cLim(2)]);
hold on
title('Click on two diagonal points for cropping')
[x,y] = ginput(2);
x(1) = max(round(x(1)),1);
x(2) = min(round(x(2)),stackDims(2));

y(1) = max(round(y(1)),1);
y(2) = min(round(y(2)),stackDims(1));

parfor z = zList
    stack
end

end