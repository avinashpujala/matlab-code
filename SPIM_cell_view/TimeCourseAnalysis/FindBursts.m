function burstInds = FindBursts(smoothedSignal,samplingInt,minBurstInterval, minThr, globalThr, method)
% FindBursts Gets indices of bursts in smoothed signal
%   burstInds = FindBursts(smoothedSignal,samplingInt, minBurstInterval, localPeakThr, globalPeakThr, method)
%               method = 'fast' or 'slow'
%
%  Inputs:
%   localPeakThr - This is the local peak detection threshold used by the
%                  function 'peakdet'. In effect, point is a peak if it's
%                  higher than this threshold value by some point on its
%                  left and right.
%   globalPeakThr - Only detected peaks above this absolute threshold value
%                   will be kept, others will be ignored.



if size(smoothedSignal,1) < size(smoothedSignal,2)
    smoothedSignal = smoothedSignal';
end

pks_min = GetPks(-smoothedSignal);
ss = smoothedSignal(:)';
ss = ss - interp1([0 pks_min(:)' length(ss)+1],[ss(1) ss(pks_min(:)') ss(end)],1:length(ss),'cubic');


if strcmpi(method,'fast')
    pks_max = GetPks(ss);
    if isempty(pks_max)
        burstInds = 1;
    else
        burstInds = pks_max;
    end
elseif strcmpi(method,'slow')
    fprintf('\n ... peak detection using "peakdet" \n')
    [peaks1,~] = peakdet(ss(:),minThr);
    if isempty(peaks1)
        burstInds = 1;
    else
        peaks = peaks1(:,1);
        burstInds = peaks;
    end
else
    errordlg('Error in FindBursts: Please specify correct peak detection method')
    return;
end

burstInds(ss(burstInds) < globalThr)=[];

minBurstInterval = round(minBurstInterval*(1/samplingInt));
[~,burstInds] = RemovePeaksWithinRefractoryPeriod(ss(burstInds),burstInds,minBurstInterval);
fprintf(['\n ' num2str(numel(burstInds)) ' bursts detected \n'])

end

