
function onsetInds = FindOnset(X)
% FindOnset - Given a vetor or matrix of oscillatory signals (say, smoothed 
%   locomotor signals), returns the onsets of oscillatory activity that
%   precedes the first peaks in the signals. For reliable results, the
%   signals need to be sufficiently smooth and 2nd order differentiable.
%   Try spline interpolating the signals with interp1, or fitting the
%   signals with a spline basis to achieve the smoothness.
% X - Input of dimensions M x N, where N is the number of time
% points and M is the number of signals

[~, dX] = gradient(X);
[~,onsetInds] = max(dSignalMat,[],2);
onsetInds = onsetInds;
end

