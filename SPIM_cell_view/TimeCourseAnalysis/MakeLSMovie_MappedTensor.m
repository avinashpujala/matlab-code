% MakeLSMovie_MappedTensor - Script for making movies using Mapped Tensor
%                            class. AnalyzeEphysData must be run before
%                            running this script


%% Input variables
input_dir               = 'Y:\Avinash\SPIM\Registered\Backed Up\Feb 2015\2-28_2015_relaxed\Fish1\G50_Tp15_20150228_061653';
nWorkers                = 10;
exposureTime            = 18e-3;
preShockFrames          = 4;
postShockFrames         = 10;


%% Run subroutine to load relevant variables
PreMovieRoutine


%% Specify 'indSets' here
% indSets={};

% indSets={shockStackIndMat{1}; shockStackIndMat{3}; shockStackIndMat{end};stack.inds.strngSwm;stack.inds.supThreshNonSwm};
% setNames ={'lowestStimInt'; 'medianStimInt' ;'highestStimInt';'strngSwm';'supraThreshNonSwm'};
 
 indSets={shockStackIndMat{1}; shockStackIndMat{3}; shockStackIndMat{end}};
 setNames ={'lowesStimInt'; 'medStimInt'; 'highestStimInt'};

% nStacks = nTimePts;

%% Check for indices too close to the end or beginning and remove, if any.
for jj = 1:length(indSets)
    currentInds = [indSets{jj}];
    currentInds((currentInds - preShockFrames) < 1 | (currentInds + postShockFrames) > (nStacks-1)) = [];
    indSets{jj}  = currentInds;
end

%%
fName = data.filename(max(strfind(data.filename,'\'))+1:end);

%% Dealing with a prime number of z slices

nPlanes = stack.dim(3);
if numel(factor(nPlanes))==1 % Prime number
    while numel(factor(nPlanes))==1
        nPlanes = nPlanes+1;
    end
end
f = factor(nPlanes);
mi = floor(median(1:numel(f)));
nRows = prod(f(1:mi));
nCols = prod(f(mi+1:end));
nPlanes = stack.dim(3);
% temp = [nRows nCols];
% nRows = min(temp);
% nCols = max(temp);% Suitable for a horizontally wider monitor such as mine


timeStamp = datestr(now,30);
aveResponseImg ={};
for thisSet = 1:length(indSets)
aveResponseImg{thisSet}=zeros(stack.dim(1),stack.dim(2), nPlanes, numel(-preShockFrames:postShockFrames));
end


%% Reading registered temporal stacks for 1 zplane at a time
registeredDir = input_dir;
tic
zList  = 1:nPlanes;
for z = zList
    disp(['Mapping Plane #' num2str(z) ' to a Tensor and Extracting Relevant Time Frames']);
    
    %     stack = double(read_LSstack_fast1(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']),[stack.dim(1) stack.dim(2)]));
    %     stack = stack(:,:,1:min(nStacks,size(stack,3)));
    stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']), [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
%     stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.bin']), [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
    %     stack = stack - backgroundStack; % Subtracting background offset so as to calculate accurate dF/F values    
    for thisSet = 1:length(indSets)
        indexStack = indSets{thisSet};
        indexStack(indexStack - preShockFrames <1)=[];
        indexStack(indexStack > nStacks-postShockFrames)=[]; % Remove indices too large to accommodate post-shock frames
        for i=0:(size(aveResponseImg{thisSet},4)-1)
            disp(['Z plane ' num2str(z) ', Stim/resp group ' num2str(thisSet), ', Time pt ' num2str(i+1)])
            aveResponseImg{thisSet}(:,:,z,i+1) = aveResponseImg{thisSet}(:,:,z,i+1)+ ...
                reshape(sum(double(stackMT(:,:,indexStack-preShockFrames+i)),3), [stack.dim(1) stack.dim(2) 1 1]);
        end
        aveResponseImg{thisSet}(:,:,z,:)= aveResponseImg{thisSet}(:,:,z,:)./length(indexStack);        
    end   
end
toc

%% Saving aveResponseImg and relevant  info
fp = [ input_dir '\aveRespImg_' timeStamp];
aveRespImg = struct();
aveRespImg.aveResponseImg = aveResponseImg;
aveRespImg.indSets = indSets;
aveRespImg.setNames = setNames;
fprintf('\n Saving aveResponseImg \n');
tic
save(fp,'aveRespImg','-v7.3');
toc


%% Empty cell to separate previous parts of the code from the latter, since the former take a very long time and don't need to be repeated



%% Videowriting
if iscell(aveResponseImg)
aveResponseImg_super = aveResponseImg;
end
aveResponseImg =[];
baselineimg = [];
dff5secImg = [];

for thisSet = 1:length(indSets)
    aveResponseImg = aveResponseImg_super{thisSet};
    baselineImg = mean(aveResponseImg(:,:,:,1:preShockFrames),4);
    dff5secImg = mean(aveResponseImg(:,:,:,preShockFrames+1:preShockFrames+2),4)./baselineImg-1;
    movName = fName(1:strfind(fName,'.')-1);
    movName = [setNames{thisSet} '_' movName];
    
%     WriteLSVideo(aveResponseImg, baselineImg, dff5secImg, nRows, nCols, movName,input_dir)
    WriteLSVideo_4dHyperStack(aveResponseImg, baselineImg, dff5secImg, nRows, nCols, movName,input_dir) % For making 4D-hyperstack movies
    
end

