function varargout = MapCellClustersOntoAveStack(varargin)
% MapCellClustersOntoAveStack  Maps cells that belong to different clusters
% onto the average stack using a different color for each cluster
%
% MapCellClustersOntoAveStack(kData,cellInds, cellInfo, avgStack);
% MapCellClustersOntoAveStack(kData,cellInds, cellInfo, avgStack, colorMap);
% fh = MapCellClustersOntoAveStack(...,nClustersPerImage);

if nargin < 4
    error('Minimum of 3 inputs required')
    return;
elseif nargin == 4
    kData = varargin{1};
    cellInds = varargin{2};
    cellInfo = varargin{3};
    avgStack = varargin{4};
    colVals = MapValsToColorsVer4(kData.idx);
elseif nargin ==5
    kData = varargin{1};
    cellInds = varargin{2};
    cellInfo = varargin{3};
    avgStack = varargin{4};
    colVals = MapValsToColorsVer4(kData.idx,varargin{5});
elseif nargin == 6
    kData = varargin{1};
    cellInds = varargin{2};
    cellInfo = varargin{3};
    avgStack = varargin{4};
    colVals = MapValsToColorsRand(kData.idx,varargin{5});
end



nClusters = size(kData.sumd,1);
if nClusters > 64
    error('Only 64 max clusters permitted at this point; nearby but clusters may be assigned the same color');
end

if nargin == 6
    nClustersPerImage = varargin{6};
    clstrList = 1:nClusters;
    if rem(nClusters,nClustersPerImage)~=0
        clstrList = [clstrList, clstrList(1:nClustersPerImage-1)];
        %  Elongate list by circshift to make length divisible by nClustersPerImage
    end
    clstrArray = reshape(clstrList,nClustersPerImage,length(clstrList)/nClustersPerImage)';
    inds = [];
    for jj = 1:size(clstrArray,1)
        inds = find(ismember(kData.idx,[clstrArray(jj,:)]));
        fh = MapCellIndsToAveStack_MaxIntProj(cellInds(inds), cellInfo, avgStack, colVals(inds,:));
        title(['Clusters Mapped: ' num2str(clstrArray(jj,:))],'fontsize',14)
    end
else
    fh = MapCellIndsToAveStack_MaxIntProj([cellInds(1:numel(kData.idx))]', cellInfo, avgStack, colVals);
end

varargout{1} = fh;

PlotRepresenativeSignalsFromClusters(kData,10,colVals)
end

function varargout =  PlotRepresenativeSignalsFromClusters(kData,nSignalsPerCluster,colVals)
figure('Name','Representative signals from clusters')
set(gca,'color','k','tickdir','out'), hold on, box off
shift = 0;
for clstr = 1:numel(unique(kData.idx))
    cellsInClstr = find(kData.idx==clstr);
    [~,cellsClosestToCtr] = sort(kData.D(cellsInClstr,clstr),'ascend');
    cellsClosestToCtr = cellsInClstr(cellsClosestToCtr(1:min(nSignalsPerCluster,numel(cellsInClstr))));
    col = colVals(cellsClosestToCtr(1),:);
    shift = shift + max(max(kData.data(cellsClosestToCtr,:)));
    plot(kData.data(cellsClosestToCtr,:)'-shift,'color',col)
    plot(mean(kData.data(cellsClosestToCtr,:),1)-shift,'w','linewidth',1.5)    
end
xlim([-inf inf])
ylim([-inf inf])
set(gca,'ytick',[]);
% varargout{1} = fh;
end


