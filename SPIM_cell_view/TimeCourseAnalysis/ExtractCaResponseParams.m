function varargout = ExtractCaResponseParams(caDffMatrix, dffDetThr, stimIdx, maxRespInd, samplingInt)
% [peakLatency, peakAmp, decayTime] = ExtractCaResponseParams(caDffMatrix, dffDetThr, stimIdx, maxRespInd, samplingInt)
% Inputs:
% caDffMatrix = Matrix of Ca++ responses of size nCells x nTimePts
% dffDetThr = Z-score threshold for Ca++ peak detection
% stimIdx = Frame number when stim occurs
% maxRespInd = Frame number after stim before which to confine search for peak (assume peaks afterwards are not stim-evoked)


dFF = caDffMatrix;
baselineInds = 1:stimIdx-1;
if isempty(baselineInds)
    error('Baseline period in Ca response too short!')
elseif numel(baselineInds) < 2
    warndlg('Only 1 point during baseline period')
end
dt = samplingInt;
responseInds = stimIdx:(stimIdx + maxRespInd);

preMeans = mean(dFF(:,baselineInds),2);
preStds = std(dFF(:,baselineInds),[],2);
postMax = max(dFF(:,responseInds),[],2); 

decayCutoffs = preMeans + 1.5*preStds;
cellInds = 1:size(dFF,1);
responsiveCells = cellInds; % This script already takes only responsiveCells as input
nonResponsiveCells = setdiff(cellInds,responsiveCells);
decayCutoffs(nonResponsiveCells)=[];

relevantTraces = abs(dFF(responsiveCells,:));
nTimePts = size(relevantTraces,2);
nCells = size(relevantTraces,1);

%% Peak Detection
%%%%%% First run of peak detection 
disp('First run of peak detection')
% lastFrame = round(timeWinForPeakDet/dt);
peakInds = ones(size(responsiveCells(:)));
peakVals = zeros(size(responsiveCells(:)));
counter = 0;
for cellNum = 1:numel(responsiveCells)
    thresh = dffDetThr; initVal = 1;
    [p,~] = peakdet(relevantTraces(cellNum,responseInds),thresh);
    if isempty(p) % No peaks
        while isempty(p) % Gradually decrement threshold until at least 1 peak detected
            counter = counter+1;
            if counter >5
                [p(2), p(1)] = max(relevantTraces(cellNum,responseInds),[],2);
            else
                thresh = thresh*(1 - initVal/5); % Reduce threshold to 80% of current threshold
                [p,~] = peakdet(relevantTraces(cellNum,responseInds),thresh);
                decayCutoffs(cellNum,1) = 0;
                %               disp(['Lowering threshold to detect peaks, current thresh = ' num2str(thresh) ', current cell = ' num2str(responsiveCells(cellNum))]);
            end
        end
    elseif numel(p(:,1)) > 1 % More than 1 peak detected
        pv = p(:,2);
        pi = p(:,1);
        orderedInds = sort(pi,'ascend');
        idxFirstPeak = orderedInds(1);
        idxSecondPeak = orderedInds(2);
        firstPeak = pv(orderedInds==idxFirstPeak);
        secondPeak = pv(orderedInds==idxSecondPeak);
        
        if (secondPeak > firstPeak*1.1) && (idxSecondPeak - idxFirstPeak < ceil(1.5/dt));
            p = p(orderedInds== idxSecondPeak,:);
        else
            p = p(orderedInds == idxFirstPeak,:);
        end
    end
    peakInds(cellNum,1) = p(1) + stimIdx;
    peakVals(cellNum,1) = min(p(2),max(relevantTraces(cellNum,:)));
    p = [];
end

%%%%%%% 2nd run of peak detection - eliminating righ extreme points %%%%%
disp('2nd run of peak detection - eliminating right extreme points')
highCutoffIdx = min(floor(mean(peakInds) + 5*std(peakInds)),size(relevantTraces,2));
% lowCutoffIdx = max(ceil(mean(peakInds)-5*std(peakInds)),stimIdx);
extremeCells = find(peakInds>=highCutoffIdx);
lastFrame = round(highCutoffIdx/dt);
counter = 0;
for cellNum = extremeCells(:)'
    thresh = dffDetThr; initVal = 1;
    [p,~] = peakdet(relevantTraces(cellNum,stimIdx:lastFrame),thresh);
    if isempty(p)
        while isempty(p)
            counter = counter+1;
            if counter >5
                [p(2), p(1)] = max(relevantTraces(cellNum,stimIdx:lastFrame),[],2);
            else
                thresh = thresh*(1 - initVal/5); % Reduce threshold by 90% of current threshold
                [p,~] = peakdet(relevantTraces(cellNum,stimIdx+1:lastFrame),thresh);
                decayCutoffs(cellNum,1) = 0;
                disp(['decay cutoff updated: ' num2str(decayCutoffs(cellNum,1))])
                disp(['Lowering threshold to detect peaks, current thresh = ' num2str(thresh)*100 ', current cell = ' num2str(cellNum)]);
            end
            
        end
    elseif numel(p(:,1))>1
        pv = p(:,2);
        pi = p(:,1);
        orderedInds = sort(pi,'ascend');
        idxFirstPeak = orderedInds(1);
        idxSecondPeak = orderedInds(2);
        firstPeak = pv(orderedInds==idxFirstPeak);
        secondPeak = pv(orderedInds==idxSecondPeak);
        
        if (secondPeak > firstPeak*1.1) && (idxSecondPeak - idxFirstPeak < ceil(3/dt));
            p = p(orderedInds== idxSecondPeak,:);
        else
            p = p(orderedInds == idxFirstPeak,:);
        end
    end
    peakInds(cellNum,1) = p(1) + stimIdx;
    peakVals(cellNum,1) = min(p(2),max(relevantTraces(cellNum,:)));
end

%%%%%% Decay determination %%%%%%%
kernel_rect = ones(5,1)/5; % Kernel for computing moving average over a 10 pt window
belowCutoffBools = (relevantTraces - repmat(decayCutoffs,1, nTimePts)) < 0; % Set true for location with values below cutoff
aboveCutoffBools = (relevantTraces-repmat(decayCutoffs,1,nTimePts)) >0;
halfPeakMat = 0.25*repmat(peakVals,1,nTimePts);
belowHalfPeakBools = (relevantTraces-halfPeakMat)<0;

P = repmat(peakInds(:),1,nTimePts); % Matricize vector of peak indices
% [~,Q] = ndgrid(1:nCells,1:nTimePts);
Q = repmat(1:nTimePts,nCells,1);
afterPeakBools = (Q-P)>0; % True values for points occurring after peak

kernel_rect = ones(5,1)/5;
rightMat = conv2(relevantTraces(:,6:end), kernel_rect(:)','same');
leftMat = conv2(relevantTraces(:,1:end-5),kernel_rect(:)','same');
decreasingMeanBools = [zeros(size(relevantTraces,1),5),(rightMat-leftMat)<0];

% decayBools = belowCutoffBools & afterPeakBools & decreasingMeanBools;
decayBools = afterPeakBools & aboveCutoffBools & belowHalfPeakBools;

decayInds = zeros(size(peakInds));
for cellNum = 1:nCells
    s = sum(decayBools(cellNum,:),2);
    if s <1
        decayBools(cellNum,:) = afterPeakBools(cellNum,:) & belowHalfPeakBools(cellNum,:);
    end
    s = sum(decayBools(cellNum,:),2);
    if s < 1
        decayBools(cellNum,end) = 1; % Conclude that the peak does not decay until the last point
    end
    decayInds(cellNum,1) = min(find(decayBools(cellNum,:)));
end


% 1st run of onset detection
disp('1st run of onset detection')
spMat = sparse(zeros(size(relevantTraces)));
sv = stimIdx * ones(size(peakInds,1),1);
lv = peakInds;
for jj = 1:size(sv,1)
    spMat(jj,sv(jj):lv(jj)) = relevantTraces(jj,sv(jj):lv(jj));
end
onsetInds = FindOnset(spMat);


timeToPeakFromStim = (peakInds - stimIdx)*dt;
timeToPeakFromOnset = (peakInds - onsetInds)*dt;
decayTimes = (decayInds-peakInds)*dt;

varargout{1} = timeToPeakFromStim;
varargout{2} = [peakInds(:), peakVals(:)];
varargout{3} = decayTimes;


% baselineMean = baselineMean(responsiveCells,1);
% peakRates = (peakVals-baselineMean)./timeToPeakFromStim;
