function [sig1, sig2] = StdWithoutZero(inputMatrix)
%MeanWithoutZero Computes mean along both dimensions of a matrix by
%                excluding zero entries
%  [mu1, mu2] = MeanWithoutZero(inputMatrix);
%  mu1 = mean along row dimension
%  mu2 = mean along col dimension

sig1 = zeros(1,size(inputMatrix,2));
for cc = 1:size(inputMatrix,2)
    col = inputMatrix(:,cc);
    col(col==0)=[];
    colMean = std(col);
    sig1(1,cc) = colMean;
end


sig2 = zeros(size(inputMatrix,1),1);
for rr = 1:size(inputMatrix,1)
    row = inputMatrix(rr,:);
    row(row==0)=[];
    rowMean = std(row);
    sig2(rr,1) = rowMean;
end


end

