% ActivityMapOnAvgStack - Script for reading average zstack and
%   overlaying activity map on it using some informative color scheme.
%   Requires variables loaded/created by AnalyzeEphysData

%% Input variables
inputDir               = 'Y:\Avinash\SPIM\Registered\Mar 2015\3-14-2015_relaxed\Fish2\G50_T1_20150314_194623';
nWorkers                = 10;
exposureTime            = 18e-3;
preShockFrames          = 4;
postShockFrames         = 10;

%% Run subroutine to load relevant variables
[data, stack, shockStackIndMat, nTimePts] = PreMovieRoutine(pyData,inputDir,'exposureTime',exposureTime);

% Update pyData matfile
disp('Updating pyData.mat...')
tic
pyData.Properties.Writable = true;
disp('.data ...')
pyData.data = data;
disp('.stack ...')
pyData.stack = stack;
toc


%% Specify sets of indices where relevant stimuli occur
% indSets={shockStackIndMat{1}; shockStackIndMat{3}; shockStackIndMat{6}};
% setNames ={'justSubMotor' ;'justSupraMotor';'highestInt'};

indSets={stack.inds.wkSwm; stack.inds.strngSwm};
setNames ={'weakSwim'; 'strongSwim'};

nStacks = nTimePts;
nPlanes = stack.dim(3);
zPlanes = FindPlanesInDir(inputDir,'Plane');
if isempty(zPlanes)
    zPlanes = 1:nPlanes;
end

%% Check for indices too close to the end or beginning and remove, if any.
for jj = 1:length(indSets)
    currentInds = [indSets{jj}];
    currentInds((currentInds - preShockFrames) < 1 | (currentInds + postShockFrames) > (nStacks-1)) = [];
    indSets{jj}  = currentInds;
end

%% Dealing with a prime number of z slices
if numel(factor(nPlanes))==1 % Prime number
    while numel(factor(nPlanes))==1
        nPlanes = nPlanes+1;
    end
end
f = factor(nPlanes);
mi = floor(median(1:numel(f)));
nRows = prod(f(1:mi));
nCols = prod(f(mi+1:end));

%% Read cell data from directory
disp('Reading cell info...')
tic
cellRespFile = [inputDir '\cell_resp3.mat'];
cellInfoFile = fullfile(inputDir,'cell_info_processed.mat');
load(cellRespFile);
load(cellInfoFile);
cells.raw  = cell_resp3;
cells.info = cell_info;
toc

%% Trial-averaged cell response data
preShockFrames  = 10;
postShockFrames = 40;
cells.avgData = cell(size(indSets));
cells.dFFData = cells.avgData;
cells.maxDFF = cells.avgData;
cells.maxDFF_all  = [];
cells.minDFF_all= [];
for indSet = 1:numel(indSets)
    inds = indSets{indSet};
    inds(inds - preShockFrames < 1) = [];
    inds(inds + preShockFrames >  size(cells.raw,2)) = [];
    if ~isempty(inds)
        avgData = zeros(size(cells.raw,1),length(-preShockFrames:postShockFrames));
        for ind  = inds(:)'
            avgData  = avgData + cells.raw(:,ind-preShockFrames: ind + postShockFrames);
        end
        avgData = avgData/numel(inds);
        cells.avgData{indSet} = avgData;
        F = repmat(mean(avgData(:,1:preShockFrames),2),1,size(avgData,2));
        dFFData = avgData./F - 1;
        cells.dFFData{indSet} = dFFData;
        cells.maxDFF{indSet} = max(dFFData(:,preShockFrames+1:end),[],2);
        cells.minDFF{indSet} = abs(min(dFFData(:,preShockFrames+1:end),[],2));
        if isempty(cells.maxDFF_all)
            cells.maxDFF_all = cells.maxDFF{indSet};
            cells.minDFF_all = cells.minDFF{indSet};
        else
            cells.maxDFF_all = max(cells.maxDFF_all,cells.maxDFF{indSet});
            cells.minDFF_all = max(cells.minDFF_all,cells.minDFF{indSet});
        end
        
    else
        [cells.avgData{indSet},cells.dFFData{indSet}, cells.maxDFF{indSet}, cells.minDFF{indSet}] = deal([]);       
    end    
end

%% Creating positive and negative cell maps
disp('Creating positive and negative maps...')
posMap = zeros(size(stack.avg));
negMap = posMap;
cellInds = 1:size(cells.info,2);
tic
for cellInd = cellInds
    z = cells.info(cellInd).slice;
    posImg = posMap(:,:,z);
    posImg(cells.info(cellInd).inds) = cells.maxDFF_all(cellInd);
    posMap(:,:,z) = posImg;
    
    negImg = negMap(:,:,z);
    negImg(cells.info(cellInd).inds) = cells.minDFF_all(cellInd);
    negMap(:,:,z) = negImg;    
    if mod(cellInd,5000)==0
        disp(['Mapped ' num2str(cellInd) ' cells'])
    end
end
toc


%% Create activity map
disp('Creating activity map')
posMap_scaled = (posMap-min(posMap(:)))/(max(posMap(:))-min(posMap(:)));
negMap_scaled = (negMap-min(negMap(:)))/(max(negMap(:))-min(negMap(:)));
avgStack_scaled = (stack.avg-min(stack.avg(:)))/(max(stack.avg(:))-min(stack.avg(:)));
activityMap = permute(cat(4,posMap_scaled, negMap_scaled, 0.25*avgStack_scaled),[2 1 4 3]);


%% Save activity map
disp('Saving activity map')
outDir = [inputDir '\proc'];
SaveImageStack(activityMap,outDir,'activityMap');

%% Updating pyData.mat
disp('Appending activity map to pyData ...')
tic
pyData.activityMap = activityMap;
toc
