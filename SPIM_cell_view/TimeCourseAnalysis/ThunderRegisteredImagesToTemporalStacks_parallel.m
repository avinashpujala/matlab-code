%% ThunderRegisteredImagesToTemporalStacks
% Converts registered Z-stacks written by thunder to temporal stacks a ala
% Takashi's format, except compressed .tif files instead of binaries.


% clear all, close all

imgDir = 'Z:\SPIM\Avinash\Raw\Aug 2015\8-9-2015_Alx RG x 939_4dpf\Fish1\G50_T1_IPI60_20150808_171117\reg';
nWorkers = 10;
regDir = [imgDir '\Registered_tStacks'];
if ~exist(regDir)
    mkdir(regDir);
    disp(['Created dir: ' regDir])
end

%% Obtain z-stack dimensions from xml file
blah = xml2struct(fullfile(imgDir,'ch0.xml'));
stackDimStr = blah.push_config.info{14}.Attributes.dimensions;
xInds = strfind(stackDimStr,'x');

stackDims(1) = str2double(stackDimStr(1:xInds(1)-1));
stackDims(2) = str2double(stackDimStr(xInds(1)+1:xInds(2)-1));
stackDims(3) = str2double(stackDimStr(xInds(2)+1:end));

%% Obtain number of time points from text file
fid = fopen(fullfile(imgDir,'Stack_frequency.txt'),'r');
info = fscanf(fid,'%f');
stackDims(4) = info(3);
fclose(fid);

%% Copy useful files to registered dir
copyfile(fullfile(imgDir,'ch0.xml'),regDir)
copyfile(fullfile(imgDir,'Stack_frequency.txt'),regDir)
copyfile(fullfile(imgDir,'Stack dimensions.log'),regDir)
copyfile(fullfile(imgDir,'Background_0.tif'),regDir)

%% Read files and write in temporal stack format
matlabpool(nWorkers);
blah = [];
zList = 1:stackDims(3);
tList = 0:stackDims(4)-1;
aveStack = zeros(stackDims(1:3));
tic
parfor z = zList
    tifName = ['Plane' num2str(z,'%0.2d')];
    if exist(fullfile(regDir, tifName))==2;
        delete(fullfile(regDir,tifName));
    end
    fid = fopen([fullfile(regDir,tifName) '.bin'],'a+');
    for t = tList
        fName = ['Image-' num2str(t,'%0.5d') '.bin'];
        temp = MappedTensor(fullfile(imgDir,fName),stackDims(1:3),'class','uint16');
        fwrite(fid,temp(:,:,z),'uint16');
        fprintf(['\n z = ' num2str(z) ', t = ' num2str(t) '\n']);
    end
    fclose(fid);     
end
toc
matlabpool close

%% Create and write ave stack (It seems that imwrite doesn't work with mapped tensors)
% disp('Creating average stack')
% tic
% if exist(fullfile(regDir,'ave.tif')) == 2
%     delete(fullfile(regDir,'ave.tif'))
% end
% for z = zList
%     tifName = ['Plane' num2str(z,'%0.2d')];
%     stack = MappedTensor(fullfile(regDir,[tifName '.bin']),[stackDims(1), stackDims(2), stackDims(4)],'class','uint16');
%     aveStack(:,:,z) = mean(stack(:,:,2:12),3);   
%     imwrite(aveStack(:,:,z),fullfile(regDir,'ave.tif'),'tif','WriteMode','append')
%     disp(tifName)
% end
% toc

%% Create a write ave stack
fName = ['Image-' num2str(2, '%0.5d') '.bin'];
fid = fopen(fullfile(imgDir,fName),'r');
zStack1 = fread(fid,prod(stackDims(1:3)),'uint16');
zStack1 = reshape(zStack1,stackDims(1:3));
fclose(fid);
disp(['Read ' fName]);

fName = ['Image-' num2str(3, '%0.5d') '.bin'];
fid = fopen(fullfile(imgDir,fName),'r');
zStack2 = fread(fid,prod(stackDims(1:3)),'uint16');
zStack2 = reshape(zStack2,stackDims(1:3));
fclose(fid);
disp(['Read ' fName]);

aveStack = (zStack1 + zStack2)/2;
aveStack = (aveStack-min(aveStack(:)))/(max(aveStack(:))-min(aveStack(:)));

disp('Saving ave stack as ave.tif')
if exist(fullfile(regDir,'ave.tif'))==2
    delete(fullfile(regDir,'ave.tif'))
end

for z = 1:size(aveStack,3)
    disp(['Plane ' num2str(z)])
    imwrite(aveStack(:,:,z),fullfile(regDir,'ave.tif'),'tiff','WriteMode','append');
end






