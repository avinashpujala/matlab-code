% ActivityMapOnAvgStack - Script for reading average zstack and
%   overlaying activity map on it using some informative color scheme.
%   Requires variables loaded/created by AnalyzeEphysData

%% Input variables
inputDir               = 'S:\Avinash\SPIM\Alx\8-9-2015_Alx RG x 939_4dpf\Fish2\G50_T1_IPI60_20150808_203813\Registered';
nWorkers                = 10; % Not really using this at the moment because the script does not run in parallel
exposureTime            = 18e-3;
preShockFrames          = 4;
postShockFrames         = 10;

%% Run subroutine to load relevant variables
[data, stack, shockStackIndMat, nTimePts] = PreMovieRoutine(pyData,inputDir,'exposureTime',exposureTime);

% Update pyData matfile
disp('Updating pyData.mat...')
tic
pyData.Properties.Writable = true;
disp('.data ...')
pyData.data = data;
disp('.stack ...')
pyData.stack = stack;
toc

%% Specify sets of indices where relevant stimuli occur

% indSets={shockStackIndMat{1}; shockStackIndMat{3}; shockStackIndMat{6}};
% setNames ={'justSubMotor' ;'justSupraMotor';'highestInt'};

indSets={stack.inds.wkSwm; stack.inds.strngSwm};
setNames ={'weakSwim'; 'strongSwim'};

nStacks = nTimePts;
nPlanes = stack.dim(3);
zPlanes = FindPlanesInDir(inputDir,'Plane');
if isempty(zPlanes)
    zPlanes = 1:nPlanes;
end
zPlanes = zPlanes(:)';

%% Check for indices too close to the end or beginning and remove, if any.
for jj = 1:length(indSets)
    currentInds = [indSets{jj}];
    currentInds((currentInds - preShockFrames) < 1 | (currentInds + postShockFrames) > (nStacks-1)) = [];
    indSets{jj}  = currentInds;
end

%% Dealing with a prime number of z slices
if numel(factor(nPlanes))==1 % Prime number
    while numel(factor(nPlanes))==1
        nPlanes = nPlanes+1;
    end
end
f = factor(nPlanes);
mi = floor(median(1:numel(f)));
nRows = prod(f(1:mi));
nCols = prod(f(mi+1:end));

%% Reading registered temporal stacks for 1 zplane at a time
% background = imread(fullfile(inputDir,'Background_0.tif'));
aveResponseImg =cell(length(indSets),1);
dffImg = aveResponseImg;
emptyInds = zeros(length(indSets),1);
for thisSet = 1:length(indSets)
    if ~isempty(indSets{thisSet})
        aveResponseImg{thisSet}=zeros(stack.dim(1),stack.dim(2), nPlanes, ...
            numel(-preShockFrames:postShockFrames));
        dffImg{thisSet} = zeros(stack.dim(1),stack.dim(2), nPlanes, ...
            numel(-preShockFrames:postShockFrames));
    else
        emptyInds(thisSet) = 1;
    end
end
emptyInds = find(emptyInds);
disp(['Removing the following empty indSets, ' num2str(emptyInds)])
indSets(emptyInds) = [];
setNames(emptyInds) = [];

registeredDir = inputDir;
tic
if exist('aveResponseImg_super','var')
    aveResponseImg = aveResponseImg_super;
else
    if matlabpool('size') == 0
        matlabpool(nWorkers)
    end    
    for thisSet = 1:length(indSets)
        temp = zeros(size(aveResponseImg{thisSet}));
        parfor z = zPlanes
            indexStack = indSets{thisSet};
            disp(['Mapping Plane #' num2str(z) ' to a Tensor and Extracting Relevant Time Frames']);
            stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']),...
                [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
            blah = GetTrlAvgStack(stackMT,indexStack,preShockFrames,postShockFrames);
            temp(:,:,z,:) = blah;
        end
        aveResponseImg{thisSet} = temp;
    end
end
toc

%% Create dff stack & activity map
disp('Creating dff stack and colorized planes...')
tic
baselineImg = [];
stackDim = size(aveResponseImg{1});
cutoff = 0.1;
upperLim = 0.8;
[dffImg_green, dffImg_red] = deal(cell(numel(indSets),1));
[allReds, allGrns, allRedGrns, activityMap_red, activityMap_grn] = deal([]);
for thisSet = 1:length(indSets)
    disp(['Ind set ' num2str(thisSet) '...']);
    baselineImg = mean(aveResponseImg{thisSet}(:,:,:,1:preShockFrames),4);
    baselineImg = repmat(baselineImg,[1,1,1,numel(-preShockFrames:postShockFrames)]);
    dffImg{thisSet} = (aveResponseImg{thisSet}-baselineImg)./baselineImg;
    dffImg_max = max(dffImg{thisSet},[],4);
    dffImg_min = min(dffImg{thisSet},[],4);
    reds = dffImg_max;
    greens = abs(dffImg_min);
    maxLim = [mean(reds(:))+ std(reds(:)) mean(reds(:))+ 10*std(reds(:))];
    minLim = [mean(greens(:))+ std(greens(:)) mean(greens(:))+ 5*std(greens(:))];
    redAndGreens = zeros([size(reds), 3]);
    disp('Median filtering & opening each plane...')
    parfor z = zPlanes
        disp(['Plane ' num2str(z)])
        redPlane = imopen(medfilt2(reds(:,:,z),[3 3]),strel('disk',5));
        redPlane(redPlane > maxLim(2)) = maxLim(2);
        redPlane(redPlane < maxLim(1)) = nan;
        reds(:,:,z) = redPlane;
        greenPlane = imopen(medfilt2(greens(:,:,z), [3 3]),strel('disk',5));
        greenPlane(greenPlane > minLim(2)) = minLim(2);
        greenPlane(greenPlane < minLim(1)) = nan;
        greens(:,:,z) = greenPlane;
        redAndGreens(:,:,z,:) = imfuse(redPlane,greenPlane,'falsecolor','Scaling','joint','ColorChannels',[1 2 0]);
    end
    maxDffImg_red{thisSet} = permute(cat(4,reds,...
        zeros(stackDim(1:3)), zeros(stackDim(1:3))),[1 2 4 3]);
    maxDffImg_green{thisSet} = permute(cat(4, zeros(stackDim(1:3)),...
        greens ,zeros(stackDim(1:3))),[1 2 4 3]);
    maxDffImg_rg{thisSet} = permute(redAndGreens,[1 2 4 3]);
    
    allReds = cat(5,allReds,maxDffImg_red{thisSet});
    allGrns = cat(5,allGrns,maxDffImg_green{thisSet});
    allRedGrns = cat(5,allRedGrns, maxDffImg_rg{thisSet});
end
activityMap_red = max(allReds,[],5);
activityMap_grn = max(allGrns,[],5);
activityMap_rg  = max(allRedGrns,[],5);
disp('Activity maps created!')
toc

%% Combining with anatomy stack and saving
activityMap_rg = imNormalize999(activityMap_rg);
activityMap = activityMap_rg;
stImg = imNormalize999(stack.avg);
activityMap(:,:,3,:) = stImg;
activityMap  = permute(activityMap,[2 1 3 4]);
disp('Combined activity map generated!')

disp('Creating & saving +/- maps for each grp/cond...')
outDir = fullfile(inputDir,'proc');
if exist(outDir)~=7
    mkdir(outDir)
end
tic
posNegMaps = cell(size(maxDffImg_rg));
for grp = 1:length(dffImg_red)
    posNegMaps{grp}  = imNormalize999(maxDffImg_rg{grp});
    posNegMaps{grp} = permute(posNegMaps{grp},[2 1 3 4]);
    posNegMaps{grp}(:,:,3,:) = permute(stImg,[2 1 3]);
    fp = fullfile(outDir,['ActivityMap_' num2str(setNames{grp}) '.tif']);
    if (exist(fp)==2)
        delete(fp)
    end
    fprintf(['\n Writing group ' num2str(grp) '... \n'])
    fprintf('Plane ')
    for z = zPlanes %size(posNegMaps{grp},4)
        imwrite(posNegMaps{grp}(:,:,:,z),fp,'tif','WriteMode','append','Compression','None')
        fprintf([ num2str(z) '\t'])
    end
end
fprintf('\n')
toc

%% Saving combined activity map
disp('Saving combined activity...')
if exist(outDir)~=7
    mkdir(outDir)
end

if exist(fullfile(outDir,'ActivityMap.tif'))==2;
    delete(fullfile(outDir,'ActivityMap.tif'));
end
for z = 1:size(activityMap,4)
    disp(['Plane ' num2str(z)]);
    imwrite(activityMap(:,:,:,z),fullfile(outDir, ...
        'ActivityMap.tif'),'tif','WriteMode','append','Compression','None');
end
maxIntRG = permute(max(activityMap_rg,[],4),[2 1 3]);
imwrite(maxIntRG,fullfile(outDir, 'MaxInt_RG.tif'),'tif','WriteMode','append','Compression','None');
disp('Done!')

%% Updating pyData.mat
disp('Appending activity map to pyData ...')
tic
pyData.activityMap = activityMap;
toc




