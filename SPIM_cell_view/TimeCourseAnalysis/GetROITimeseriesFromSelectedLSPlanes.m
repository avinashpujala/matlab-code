function tRoi = GetROITimeseriesFromSelectedLSPlanes(inputDir, zPlaneList)
%GetROITimeseriesFromSelectedLSPlanes - Allows for drawing of multiple ROIs
%on selected light sheet planes and obtaining timecourses from these ROIs
% Outputs
%   tROI = Cell array where each cell contains timeseries from one ROI; the
%   time series from any ROI will be normalized by the area of the ROI
% Inputs
%   inputDir = Path to the directory where registered planes are located
%   zPlaneList = List of z planes to generate a max-int proj from for the
%   selection of ROIs; please note that plane # 1 is the deepest plane

inputDir = varargin{1};
zPlaneList  = varargin{2};

% [fileName,inputDir] = uigetfile({'*.*';'*.tif'},'SELECT MULTIPLE FILES USING CTRL OR SHIFT KEY','Multiselect','on');

aveStack = readtiff(fullfile(inputDir,'ave.tif'));
disp(['Size of each image in stack is ' num2str(size(aveStack,1)) ' X ' num2str(size(aveStack,2))]);

if any(zPlaneList > size(aveStack,3))
    error(['Stack only has' num2str(size(aveStack,3)) 'planes!']);
    return;
end

maxIntIm = max(aveStack(:,:,zPlaneList),[],3);

rois = setEllipticalRois(maxIntIm);

tStack = cell(1,size(rois,2));
for zz = 1:numel(zPlaneList)
    outputName = [inputDir, '/Plane' num2str(zPlaneList(zz), '%.2d') '.stack'];
    stack = read_LSstack_fast1(outputName,size(aveStack));
    for rr = 1:size(rois,3)
      blah = stack*repmat(rois{rr}.bw,[1 1 size(stack,3)]);
      tStack{rr} =  stack*repmat(rois{rr}.bw,[1 1 size(stack,3)]); % Only keep values w/in ROIs for all time planes
      tRoi = zeros(size(rois,2),size(stack,3));
      for tt = 1:size(stack,3)
          blah = tStack{rr}(:,:,tt);
          tRoi(rr,tt) = sum(blah(:)); % Adding up all pixels within the ROI
      end
    end
end

for zz = 1:numel(zPlaneList)
    if ~isempty(tStack{zPlaneList(zz)})
        
    end
end


end

