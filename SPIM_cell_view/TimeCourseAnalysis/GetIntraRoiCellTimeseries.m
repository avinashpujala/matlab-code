function varargout = GetIntraRoiCellTimeseries(inputDir,zPlaneList,cellResponses,cell_info)
%GetIntraRoiCellTimeseries - Allows for drawing of multiple ROIs
%on selected light sheet planes and obtaining timecourses from these ROIs
%
% intraRoiCellResp = GetIntraRoiCellTimeseries(inputDir, zPlaneList, cellResponseMatrix, cellInfoMatrix)
% [intraRoiCellResp, intrRoiCellInds] = GetIntraRoiCellTimeseries(...)
% Outputs
%   intraRoiCellResp = Cell array where each cell contains timeseries from one ROI; the
%   time series from any ROI will be normalized by the area of the ROI
% Inputs
%   inputDir = Path to the directory where registered planes are located
%   zPlaneList = List of z planes to generate a max-int proj from for the
%                selection of ROIs; please note that plane # 1 is the
%                deepest plane
%   cellResponses  = cell response matrix of size nCells X nTimePts
%   cell_infp =  nCells sized structure array that stores cell info such as
%                coordinates, etc.


%%
% [fileName,inputDir] = uigetfile({'*.*';'*.tif'},'SELECT MULTIPLE FILES USING CTRL OR SHIFT KEY','Multiselect','on');

%% Load activity map
% aveStack = readtiff(fullfile(inputDir,'ave.tif'));
imgInfo = imfinfo(fullfile(inputDir,'ActivityMap.tif'));
aveStack = imread(fullfile(inputDir,'ActivityMap.tif'),1);
for z = 2:size(imgInfo,1)
    currentPlane = imread(fullfile(inputDir,'ActivityMap.tif'),z);
    aveStack = cat(4,aveStack,currentPlane);
end
disp(['Size of each image in stack is ' num2str(size(aveStack,1)) ' X ' num2str(size(aveStack,2))]);
% dim=read_LSstack_size(fullfile(inputDir,'Stack dimensions.log'));


%% Error checks
if any(zPlaneList > size(aveStack,4))
    error(['Stack only has' num2str(size(aveStack,4)) 'planes!']);   
elseif any(diff(unique(zPlaneList))~=1)
    error('Chosen z-planes must be consecutive: please re-enter list');  
end


%% ROIs from max-inf projection of selected planes
% maxIntIm = max(aveStack(:,:,zPlaneList),[],3);
maxIntIm = max(aveStack(:,:,:,zPlaneList),[],4);
[maxIntIm,clrMap] = rgb2ind(maxIntIm,128);
rois = setEllipticalRois(maxIntIm,clrMap);


%% Cell indices from within ROIs
intraRoiCellInds = GetIntraRoiCellInds(cell_info,rois,zPlaneList);

%% Cell response timeseries
intraRoiCellResp = cell(size(rois,2),1);
for rr = 1:size(rois,2)
    intraRoiCellResp{rr} = cellResponses(intraRoiCellInds{rr},:);
end


varargout{1} = intraRoiCellResp;
varargout{2} = intraRoiCellInds;

end




