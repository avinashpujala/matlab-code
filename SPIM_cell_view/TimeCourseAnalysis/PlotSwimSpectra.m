function powSpectra = PlotSwimSpectra(varargin)
%PlotSwimSpectra  Plots global power spectra for swim episodes occurring within...
%   a specified time window after stimulus onset
% powSpectra = GetSwimSpectra(stimSegData)
% powSpectra = GetSwimSpectra(..., postStimPeriod)
% powSpectra = GetSwimSpectra(..., plotBool)
% Inputs:
% stimSegData - Data segregated by stimulus type and as returned by
%   AnalyzeEphysData.m
% postStimPeriod - Time period ( in sec) after stimulus within which
%   to compute the global power spectrum. If postStimPeriod is not entered
%   as an input,then computes the spectrum for the entire length of the
%   signal available after stimulus.
% plotBool = True or False; False will suppress plotting. Default value is
%   True
% Outputs:
% Returns power spectra for individual trials and trial averages within a
%   stim group in addition to plotting these

stimSegData = varargin{1};
if nargin ==1
    postStimPeriod = stimSegData1(1).time_Wxy(end);
    plotBool = true;
elseif nargin == 2
    postStimPeriod = varargin{2};
    plotBool = true;
elseif nargin > 2
    postStimPeriod = varargin{2};
    plotBool = varargin{3};
end
powSpectra(size(stimSegData,1),size(stimSegData,2)) = struct;
tInds = find((stimSegData(1).time_Wxy >=0) & (stimSegData(1).time_Wxy <= postStimPeriod));
fh = zeros(size(stimSegData,1),size(stimSegData,2));
cols = MapValsToColors(1:size(stimSegData,1)*size(stimSegData,2),colormap(jet));
colCtr = 1;
lgnd = {};
for stim1 = 1:size(stimSegData,1)
    for stim2 = 1:size(stimSegData,2)
        nTrials = length(stimSegData(stim1,stim2).trialInds);
        powSpectra(stim1,stim2).all = zeros(nTrials,size(stimSegData(1,1).all(1).Wxy.coeffs,1));
        powSpectra(stim1,stim2).mu = zeros(1,size(stimSegData(1,1).all(1).Wxy.coeffs,1));
        powSpectra(stim1,stim2).sig = zeros(1,size(stimSegData(1,1).all(1).Wxy.coeffs,1));
        for trial = 1:length(stimSegData(stim1,stim2).trialInds)
            powSpectra(stim1,stim2).all(trial,:) =...
                mean(abs(stimSegData(stim1,stim2).all(trial).Wxy.coeffs(:,tInds)),2);         
        end
        powSpectra(stim1,stim2).mu = mean(powSpectra(stim1,stim2).all,1);
        powSpectra(stim1,stim2).sig = std(powSpectra(stim1,stim2).all,1);
        powSpectra(stim1,stim2).sem = powSpectra(stim1,stim2).sig/sqrt(size(powSpectra(stim1,stim2).all,1));
        powSpectra(stim1,stim2).comF = ...
            sum(powSpectra(stim1,stim2).mu.* stimSegData(1,1).freqVec_Wxy)/...
            sum(powSpectra(stim1,stim2).mu);
        powSpectra(stim1,stim2).comP = powSpectra(stim1,stim2).mu(find...
            (stimSegData(1,1).freqVec_Wxy <= powSpectra(stim1,stim2).comF,1,'first'));
        powSpectra(stim1,stim2).comPP = sum(powSpectra(stim1,stim2).mu.*stimSegData(1,1).freqVec_Wxy)...
            /sum(stimSegData(1,1).freqVec_Wxy);
        if plotBool
            fh(stim1,stim2) = figure;
            figure(fh(stim1,stim2))
            plot(stimSegData(1,1).freqVec_Wxy, transpose(powSpectra(stim1,stim2).all),'.','color', cols(colCtr,:))
            hold on
            box off
            title(['Pow spectra of individual trials, (Stim1, Stim2) =  (' num2str(stim1) ',' num2str(stim2) ')' ])
            set(gca,'tickdir','out','color',[0.8 0.8 0.8])
            axis([-inf 120 -inf inf])
            xlabel('Freq (Hz)'), ylabel('Power')
           
            if colCtr == 1
                fh2 = figure;
                fh3 = figure;
            end
            
            % Filled plots
            figure(fh2)
            y = [[powSpectra(stim1,stim2).mu(:) + powSpectra(stim1,stim2).sem(:)];...
                flipud([powSpectra(stim1,stim2).mu(:) - powSpectra(stim1,stim2).sem(:)])];
            fp = fill([stimSegData(1,1).freqVec_Wxy(:); flipud(stimSegData(1,1).freqVec_Wxy(:))],...
                y, cols(colCtr,:));
            axis([-inf 120 -inf inf])
            box off
%             set(gca,'tickdir','out','color',[0.4 0.4 0.4])
            set(gca,'tickdir','out','color',[0.8 0.8 0.8])
            set(fp,'facealpha', 1/sqrt(colCtr),'linewidth',2,'linestyle','-')
            hold on
            xlabel('Freq (Hz)'), ylabel('XW pow')
            title('\mu +/- s.e.m of XW power for different stim grps','interpreter', 'tex')
            lgnd{colCtr} = ['StimGrp: (' num2str(stim1) ', ' num2str(stim2) ')'];
                       
            % Trial avg plots
            figure(fh3)
            plot(stimSegData(1,1).freqVec_Wxy,powSpectra(stim1,stim2).mu,'linewidth',2,...
                'color', cols(colCtr,:))
            hold on
%             s = stem(powSpectra(stim1,stim2).comF, powSpectra(stim1,stim2).comP);
%             set(s,'marker','o','markersize',12,'markerfacecolor',cols(colCtr,:),...
%                 'color',cols(colCtr,:),'linestyle', ':')
%             plot(stimSegData(1,1).freqVec_Wxy, transpose(powSpectra(stim1,stim2).all),...
%                 '.','color', cols(colCtr,:),'markersize',1)
%            eh =  errorbar(stimSegData(1,1).freqVec_Wxy(1:10:end),powSpectra(stim1,stim2).mu(1:10:end),...
%                 powSpectra(stim1,stim2).sem(1:10:end),'.');
%            set(eh, 'color',cols(colCtr,:)) 
            axis([-inf 120 -inf inf])
            set(gca,'tickdir','out','color',[0.8 0.8 0.8])
            xlabel('Freq (Hz)'), ylabel('XW pow')
            box off
            
        end
           colCtr = colCtr + 1;
    end 
end
figure(fh2), legend(lgnd);
figure(fh3), legend(lgnd);
end

