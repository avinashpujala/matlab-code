function varargout = MapCellIndsToAveStack(varargin)
%MapCellsIndsToAveStack  A function that maps cell indices to their location
%                        on the averaged image stack
% mappedStack = MapCellsToAveStack(cellIndices, cellInfo,aveStack);
% mappedStack = MapCellsToAveStack(cellIndices, cellInfo,aveStack, colorValues);
% Inputs:
% cellInds = Vector of indices of cells to be mapped
% cellInfo = Matrix containing cell info (as created by TK's code)
% avgStack = Stack of temporally averaged registered planes onto which
%            the cells are to be mapped
% Outputs:
% mappedStack - Image stack with mapped cells

if nargin == 3
    cellInds = varargin{1};
    cellInfo = varargin{2};
    avgStack = varargin{3};
    colorValues = repmat([1 0 0],length(cellInds),1);
elseif nargin ==4
        cellInds = varargin{1};
    cellInfo = varargin{2};
    avgStack = varargin{3};
    colorValues = varargin{4};
end

normStack = StackNormalize99(avgStack);
normStack(normStack>1)=1; % This is what TK was doing
normStack_rgb = 3*permute(repmat(normStack,[1 1 1 3]),[1 2 4 3]); % 3rd dim is for color channel, 4th dim is for slice
normStack_rgb(normStack_rgb>1) = 1;
nCells=length(cellInfo);
dim = size(avgStack);
dimv_yx = size(normStack_rgb);
totImage=zeros(dimv_yx(1),dimv_yx(2));

circle=makeDisk2(7,15);
[r, v]=find(circle);
r=r-8;
v=v-8;
circle_inds  = r*dimv_yx(1)+v;

%%
cellPos_z = [cellInfo(cellInds).slice];

%    cVals =[cellInds(:), colorValues];
for zSlice  = 1:dimv_yx(4);
    fprintf(['\n Current Slice: ' num2str(zSlice) ' \n'])
    f = find(cellPos_z == zSlice);
    cellsInSlice = cellInds(f);
    cVals = colorValues(f,:);
    for jj=1:length(cellsInSlice)
        cinds=(cellInfo(cellsInSlice(jj)).center(2)-1)*dimv_yx(1)+cellInfo(cellsInSlice(jj)).center(1);
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2));
        lut = cVals(jj,:);
        anatomy_image_yx = normStack_rgb(:,:,:,zSlice);
        anatomy_image_yx(cinds+circle_inds(labelinds))=lut(1);
        anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=lut(2);
        anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=lut(3);
        normStack_rgb(:,:,:,zSlice) = anatomy_image_yx;
    end
end

%%  Map cells and save

figPos = getMonitorSize;
[figPos(1),figPos(2)] = deal(50);
figPos(4) = figPos(4)*0.8;
figPos(3) = min(0.95*figPos(4), figPos(3)*0.8);
figHandle = figure('position',figPos);
colormap(gray);
normStack_rgb(normStack_rgb>1)=1;
normStack_rgb(normStack_rgb<0)=0;
brightestPixel = max(normStack(:));
for jj = 1 :size(normStack_rgb,4);
    subaxis(5,9,jj, 'Spacing', 0, 'Padding', 0, 'Margin', 0);
    image(normStack_rgb(:,:,:,jj)); % Scaling to use the full color range
    axis tight
    axis off
end

varargout{1} = normStack_rgb;
timeStamp = datestr(now,30);
fn = ['CellsMappedOntoImg_' num2str(timeStamp) '.tif'];
if exist(fn)
    delete(fn)
end

for z = 1:size(normStack_rgb,4)
    imwrite(normStack_rgb(:,:,:,z),fn,'tif','WriteMode','append','Compression','none');
end

return;


%%

cellinds_ex = define_region_roi_new( cellInfo, cells1, ave_stack);

%%
figure(2);
for j=1:length(cellinds_ex);
    cnum=j;
    subplot(10,10,j);plot(cell_resp_ave(cellinds_ex(j),:),'r','linewidth',1);
    title(num2str(cnum));
    ylim([130 200]);
    xlim([0 sum(timelist)]);
    line([5 5],[0.95 1.2])
    % line([104 104],[0.95 1.3])
end

%%
hb_cellinds=cellinds_ex;

save(fullfile(input_dir,'\Registered\hb_cellinds.mat'),'hb_cellinds');

%%
dim_x = size(cell_resp,2)/400;
IM = reshape(cell_resp(extracted_cellinds(14),:), 400 ,dim_x)';
figure('position',[50 50 1000 800]);
imagesc(IM);
title('cell-14')

end

