% MakeLSMovie_MappedTensor - Script for making movies using Mapped Tensor
%                            class. AnalyzeEphysData must be run before
%                            running this script


%% Input variables
input_dir               = 'Z:\SPIM\Avinash\Raw\Aug 2015\8-9-2015_Alx RG x 939_4dpf\Fish1\Registered_tStacks';
nWorkers                = 10;
exposureTime            = 18e-3;
preShockFrames          = 4;
postShockFrames         = 10;
planesOfInterest        = [22 23 24];


%% Run subroutine to load relevant variables
PreMovieRoutine
% stack = data.stack;
tData = data.tData;
trialSegData = data.trialSegData;

%% Sets of relevant stacks
shockStacks = stack.inds.shock;
outStacks = find(shockStacks-5 < 0 | shockStacks + 10 > numel(stack.inds.start));
if ~isempty(outStacks)    
shockStacks(outStacks) = [];
end

shockStackSets = {};
for trl = 1:length(shockStacks)
    shockStackSets{trl} = shockStacks(trl)-preShockFrames:shockStacks(trl)+postShockFrames;
end

%% Peri-stimulus movies for all trials
tic
registeredDir = input_dir;
movieSets = cell(1,numel(shockStacks));
nFrames = numel(-preShockFrames:postShockFrames);
nPlanes = numel(planesOfInterest);
for trl = 1:length(shockStackSets)
    movieSets{trl} = zeros(stack.dim(1),stack.dim(2), nPlanes ,nFrames);
    fprintf(['\n Trial ' num2str(trl) '... \n'])
    for pln = 1:nPlanes
        stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(planesOfInterest(pln),'%.2d'),'.bin']),...
        [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
        movieSets{trl}(:,:,pln,:) = stackMT(:,:,shockStackSets{trl});
        fprintf(['Plane ' num2str(planesOfInterest(pln)) '\t'])
    end    
end
fprintf('\n')
toc

return;

%% Saving movie sets
fn = ['trlMovies_planes_' num2str(planesOfInterest(1)) '-'...
    num2str(planesOfInterest(end)) '.mat'];
disp('Saving trial movies');
tic
fp = [registeredDir '\movies'];
save(fullfile(fp,fn),'movieSets','-v7.3');
toc

return;
%%

nStacks = nTimePts;

%% Check for indices too close to the end or beginning and remove, if any.
for jj = 1:length(indSets)
    currentInds = [indSets{jj}];
    currentInds((currentInds - preShockFrames) < 1 | (currentInds + postShockFrames) > (nStacks-1)) = [];
    indSets{jj}  = currentInds;
end

%%
fName = data.filename(max(strfind(data.filename,'\'))+1:end);

%% Dealing with a prime number of z slices

nPlanes = stack.dim(3);
if numel(factor(nPlanes))==1 % Prime number
    while numel(factor(nPlanes))==1
        nPlanes = nPlanes+1;
    end
end
f = factor(nPlanes);
mi = floor(median(1:numel(f)));
nRows = prod(f(1:mi));
nCols = prod(f(mi+1:end));
nPlanes = stack.dim(3);
% temp = [nRows nCols];
% nRows = min(temp);
% nCols = max(temp);% Suitable for a horizontally wider monitor such as mine


timeStamp = datestr(now,30);
aveResponseImg ={};
for thisSet = 1:length(indSets)
aveResponseImg{thisSet}=zeros(stack.dim(1),stack.dim(2), nPlanes, numel(-preShockFrames:postShockFrames));
end


%% Reading registered temporal stacks for 1 zplane at a time
registeredDir = input_dir;
tic
zList  = 1:nPlanes;
for z = zList
    disp(['Mapping Plane #' num2str(z) ' to a Tensor and Extracting Relevant Time Frames']);
    
    %     stack = double(read_LSstack_fast1(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']),[stack.dim(1) stack.dim(2)]));
    %     stack = stack(:,:,1:min(nStacks,size(stack,3)));
%     stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.stack']), [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
    stackMT = MappedTensor(fullfile(registeredDir,['Plane',num2str(z,'%.2d'),'.bin']), [stack.dim(1), stack.dim(2), nTimePts],'class','uint16');
    %     stack = stack - backgroundStack; % Subtracting background offset so as to calculate accurate dF/F values    
    for thisSet = 1:length(indSets)
        indexStack = indSets{thisSet};
        indexStack(indexStack - preShockFrames <1)=[];
        indexStack(indexStack > nStacks-postShockFrames)=[]; % Remove indices too large to accommodate post-shock frames
        for i=0:(size(aveResponseImg{thisSet},4)-1)
            disp(['Z plane ' num2str(z) ', Stim/resp group ' num2str(thisSet), ', Time pt ' num2str(i+1)])
            aveResponseImg{thisSet}(:,:,z,i+1) = aveResponseImg{thisSet}(:,:,z,i+1)+ ...
                reshape(sum(double(stackMT(:,:,indexStack-preShockFrames+i)),3), [stack.dim(1) stack.dim(2) 1 1]);
        end
        aveResponseImg{thisSet}(:,:,z,:)= aveResponseImg{thisSet}(:,:,z,:)./length(indexStack);        
    end   
end
toc

%% Saving aveResponseImg
fn = [ input_dir '\aveRespImg_' timeStamp];
fprintf('\n Saving aveResponseImg \n');
tic
save(fn,'aveResponseImg','-v7.3');
toc


%% Empty cell to separate previous parts of the code from the latter, since the former take a very long time and don't need to be repeated



%% Videowriting
if iscell(aveResponseImg)
aveResponseImg_super = aveResponseImg;
end
aveResponseImg =[];
baselineimg = [];
dff5secImg = [];

for thisSet = 1:length(indSets)
    aveResponseImg = aveResponseImg_super{thisSet};
    baselineImg = mean(aveResponseImg(:,:,:,1:preShockFrames),4);
    dff5secImg = mean(aveResponseImg(:,:,:,preShockFrames+1:preShockFrames+2),4)./baselineImg-1;
    movName = fName(1:strfind(fName,'.')-1);
    movName = [setNames{thisSet} '_' movName];
    
%     WriteLSVideo(aveResponseImg, baselineImg, dff5secImg, nRows, nCols, movName,input_dir)
    WriteLSVideo_4dHyperStack(aveResponseImg, baselineImg, dff5secImg, nRows, nCols, movName,input_dir) % For making 4D-hyperstack movies
    
end

