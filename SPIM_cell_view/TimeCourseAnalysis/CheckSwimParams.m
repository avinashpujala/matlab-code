function CheckSwimParams(trialSegData,timeVec)
% CheckSwimParams   Plots raw trials with overlaid swim params for visual
%                   inspection
% CheckSwimParam(trialSegData,timeVec)

xLim = [-100 4000];
labels = {'Lat1'; 'Lat2'; 'Dur'; 'Freq'; 'Pow'; 'Stim'};
maxY = [];
figure('Name','Visual inspection of auto-detected swim parameters')
for trial = 1:size(trialSegData,1)
    cla
    box off, hold on
    for ch = 1:size(trialSegData(1).raw,2)
        if ch ==2
            color = 'r';
        else
            color = 'b';
        end
        maxY = max(abs(trialSegData(trial).artless(:,ch)));
        plot(timeVec*1000, trialSegData(trial).raw(:,ch)-ch*maxY,color)
    end
    values ={round(trialSegData(trial).swim.onset(1)*1000);...
        round(trialSegData(trial).swim.onset(2)*1000);...
        round(trialSegData(trial).swim.dur_se*1000);...
        round(trialSegData(trial).Wxy.maxFreq);...
        max(round(trialSegData(trial).Wxy.maxPow),0);...
        max(trialSegData(trial).stim.amps)};    
    xlim(xLim), ylim([-inf inf])
    set(gca,'tickdir','out')
    title(['Trial # ' num2str(trial)])
    text(xLim(2)-0.5*xLim(2),-1.5*maxY,labels)
    text(xLim(2)-0.4*xLim(2),-1.5*maxY,values)
    pause()
end