function stack = GetStackData(data,tData,cells,imgDir,exposureTime)
% GetStackData - Given the 'data' structure created by AnalyzeEphysData.m
%   returns the structure variable stack, whose fields contain information
%   used by other scripts
% stack = GetStackData(data,tData,cells, imgDir, exposureTime)
% Inputs:
% data - 'data' structure variable created by AnalyzeEphysData.m
% tData - 'tData' structure created by AnalyzephysData.m
% cells - cell info read from cell_resp3.mat and cell_info.mat.
% imgDir - Directory where registered images and 'ave.tif' stack created by TK's registration scripts are stored
% samplingRate - The rate at which ephys data was sampled (default = 6000)
% exposureTime - Camera exposure time (typically, 18e-3)
% 
% Avinash Pujala, Koyama lab/HHMI, 2016


% Read avg stack
try
    stack.avg  = double(readtiff(fullfile(imgDir,'ave.tif')));
catch
    info = imfinfo(fullfile(imgDir,'ave.tif'));
    stack.avg = zeros(info(1).Height,info(1).Width, size(info,1));
    for z = 1:size(info,1)
        stack.avg(:,:,z) = imread(fullfile(imgDir,'ave.tif'),z);
    end
end
stack.dim = size(stack.avg);

fid = fopen(fullfile(imgDir,'Stack_frequency.txt'),'r');
info = fscanf(fid,'%f');
if ~isempty(info)
    nTimePts = info(3);
end
fclose(fid);


% Find stacks coincident with shock stimul()disp('Finding stacks coincident with shocks')
stim = data.stim;
stack.inds.start = FindStimPulses(data.camTrigger,[],3.7,(exposureTime*stack.dim(3)*data.samplingRate),'exact');
if isempty(stack.inds.start)
    error('Camera trigger signal not detected!')
end
stack.times = data.t(stack.inds.start);
stack.N = numel(stack.times);
stack.interval = median(diff(stack.times(:)));

stack.inds.shock = MapIndsToDifferentlySampledTimeseries(stim.inds,stack.inds.start);
stack.inds.shock = unique(stack.inds.shock); % This is because sometimes cam trigger can end before ephys signal.
stack.inds.shock(stack.inds.shock> size(cells.raw,2)) = [];
% stack.inds.shock(stack.inds.shock> nTimePts) = [];

% Strong and weak swim stack inds

disp('Findings stacks coincident with weak swims')
wkSwmInds = data.stim.inds(tData.wkSwmTrls);
stack.inds.wkSwm = MapIndsToDifferentlySampledTimeseries(wkSwmInds,stack.inds.start);
stack.inds.wkSwm = unique(stack.inds.wkSwm);

disp('Findings stacks coincident with strong swims')
strngSwmInds = data.stim.inds(tData.strngSwmTrls);
stack.inds.strngSwm = MapIndsToDifferentlySampledTimeseries(strngSwmInds,stack.inds.start);
stack.inds.strngSwm = unique(stack.inds.strngSwm);


end
