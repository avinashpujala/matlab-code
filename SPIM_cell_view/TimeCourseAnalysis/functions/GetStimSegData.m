function varargout = GetStimSegData(data, trialSegData)
% GetStimSegData Given 'data' and 'trialSegData' structures created by AnalyzeEphysData.m
%    returns updated data, stimSegData and nTrialMat used by other scripts
% stimSegData = GetStimSegData(data)
% [data, stimSegData] = GetStimSegData(data)
% Inputs:
% data - 'data' structure created by AnalyzeEphysData.m
% Outputs:
% data - 'data' structure with updated data.stim. Updated data.stim has the
%   subfields 'indMat', 'nTrialMat', 'conditions'
%   data.stim.inMat - Matrix of stim indices
%   data.stim.nTrialmat  - Matrix showing # of trials for all stim combinations 
%   data.stim.conditions - Cell array with as many cells as there are
%       stimuli. For e.g., stim.conditions{1} replaces stim1Conditions.
% 

stim = data.stim;

stim.conditions = cell(size(stim.amps,1),1);
stim.conditions{1} = unique(stim.amps(1,:));
stim.conditions{2} = unique(stim.amps(2,:));
stimSegData = struct;
stimSegData(numel(stim.conditions{1}),numel(stim.conditions{2})).all = [];
stim.indMat = stimSegData(1,1).all;
stimTimesMat = stimSegData(1,1).all;

stim.nTrialMat = zeros(numel(stim.conditions{1}),numel(stim.conditions{2}));
for one = 1:numel(stim.conditions{1});
    for two = 1:numel(stim.conditions{2})
        trialInds = find(stim.amps(1,:) == stim.conditions{1}(one) &  stim.amps(2,:) == stim.conditions{2}(two));
        stimSegData(one,two).all = trialSegData(trialInds);
        stimSegData(one,two).trialInds = trialInds;
        stim.indMat{one,two} = [one, two];
        stimTimesMat{one,two} = stim.times(trialInds);
        stim.nTrialMat(one,two) = numel(trialInds);
    end
end

data.stim = stim;

varargout{1} = data;
varargout{2} = stimSegData;

end