
function avgStack = GetTrlAvgStack(wholeStack,trlInds,preFrames,postFrames)
% Given a temporal image stack, returns a shorter image stack which is the
%   average of subsets of temporal stacks that represent a trial, etc.
% avgStack = GetTrlAvgStack(wholeStack,trlInds,preFrames, postFrames);

trlInds((trlInds-preFrames) < 1)=[];
trlInds((trlInds + postFrames) > size(wholeStack,3))=[];
avgStack = zeros(size(wholeStack,1), size(wholeStack,2), numel(-preFrames:postFrames));
for i = 1:numel(trlInds)
    blah  = double(wholeStack(:,:,trlInds(i)-preFrames : trlInds(i)+postFrames));
    avgStack = avgStack + blah;
end
avgStack = avgStack/numel(trlInds);
end