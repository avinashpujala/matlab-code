function varargout = PreMovieRoutine(pyData,inputDir, varargin)
%PreMovieRoutine A subroutine (called in ActivityMapOnAvgStack.m) to
%   facilitate the creation of activity maps
% [data, stack, shockStackIndMat, nTimePts] = PreMovieRoutine(data,inputDir,'exposureTime',exposureTime);

exposureTime = 18e-3; % Default value

for jj  = 1:numel(varargin)
   if strcmpi('exposureTime',varargin{jj})
       exposureTime = varargin{jj+1};
   end
end

% Reading relevant data from pyData
disp('Reading data from pyData.mat...')
tic
disp('.data ...')
data = pyData.data;
disp('.tData ...')
tData = pyData.tData;
disp('.trialSegData ...')
trialSegData = pyData.trialSegData;
toc

data = GetStimSegData(data,trialSegData);
stim = data.stim;

% Reading cell data
disp('Reading cell data...')
tic
cellRespFile = [inputDir '\cell_resp3.mat'];
cellInfoFile = fullfile(inputDir,'cell_info_processed.mat');
disp('Loading cell response matrix...')
load(cellRespFile);
load(cellInfoFile);
cells.raw = cell_resp3;
cells.info = cell_info;
toc

% Loading average stack
disp('Loading average stack...')
stack = GetStackData(data,tData,cells,inputDir,exposureTime);

% Determining sampling rate
if isfield(data,'t')
    data.samplingRate = round(1/(data.t(2)-data.t(1)));
elseif isfield(data,'samplingRate')
    data.t = (0:length(data.artless)-1)*(1/data.samplingRate);
else
    data.samplingRate = input('Please enter sampling rate for ephys data (usually, 6000): ');
    data.t = (0:length(data.artless)-1)*(1/data.samplingRate);
end

%# Find trials that are either outside the Ca2+ imaging period for some reason, or that do not...
%#      accommodate specified postStim period
tData.outImgTrls = find(stack.inds.shock > numel(stack.inds.start) - ceil(data.postStimPeriod/stack.interval));
stack.inds.shock(tData.outImgTrls) = [];
trialSegData(tData.outImgTrls(tData.outImgTrls <= size(trialSegData,1))) = [];


%% Finding stacks coincident with trls where stim int > thresh & swim = false
supThreshTrls = find(tData.stimGroup > 2); % Assuming that stim grps > 2 were chosen to be suprathresh
nonSwmTrls = setdiff(1:size(trialSegData,1),union(tData.wkSwmTrls,tData.strngSwmTrls));
tData.supThreshNonSwmTrls = intersect(supThreshTrls,nonSwmTrls);

supThreshNonSwmInds = data.stim.inds(tData.supThreshNonSwmTrls);
stack.inds.supThreshNonSwm = MapIndsToDifferentlySampledTimeseries(supThreshNonSwmInds,stack.inds.start);

%% Read 'Stack_frequency.txt' to obtain number of time points.
fid = fopen(fullfile(inputDir,'Stack_frequency.txt'),'r');
info = fscanf(fid,'%f');
if ~isempty(info)
    nTimePts = info(3);
end
fclose(fid);

%% Uncomment this part, if there are fewer stacks than there were supposed to be for whatever reason.

stack.inds.wkSwm(stack.inds.wkSwm > nTimePts) = [];
stack.inds.strngSwm(stack.inds.strngSwm > nTimePts) = [];

%% Segmenting a dummy trace to find incomplete trials indices
dummyTrace = zeros(1,nTimePts);
[trialSegDummyTrace, incompleteSegInds] = SegmentDataByEventsVer2(dummyTrace, ...
    stack.interval,stack.inds.shock,data.preStimPeriod, ...
    max(data.postStimPeriod,(0.8*stim.interval.min)));

%% Creating shockStackInMat
shockStackIndMat = cell(numel(stim.conditions{1}),numel(stim.conditions{2}));
dummyTraceMat = cell(numel(stim.conditions{1}),numel(stim.conditions{2}));
for one = 1:numel(stim.conditions{1});
    for two = 1:numel(stim.conditions{2})
        trialInds = find(stim.amps(1,:)==stim.conditions{1}(one) &  stim.amps(2,:) == stim.conditions{2}(two));
        trialInds(trialInds > size(trialSegDummyTrace,1))=[];
        shockStackIndMat{one,two} = stack.inds.shock(trialInds);       
    end
end

varargout{1}= data;
varargout{2} = stack;
varargout{3} = shockStackIndMat;
varargout{4} = nTimePts;

end

