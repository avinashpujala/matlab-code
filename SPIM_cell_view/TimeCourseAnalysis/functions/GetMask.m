function varargout = GetMask(varargin)
%GetMask Given the path to an image stack corresponding to a color channel
%   to be used as a mask, returns a mask created using specified parameters
% mask =  GetMask(pathToMask);
% [mask,mask_bin] = GetMask(pathToMask,'filtDims',filtDims,'maskThr',
%       maskThr,'imgExt','tif');
%
% Avinash Pujala, Koyama lab/HHMI, 2016

filtDims = [3 3];
maskThr = 0.1;
imgExt = 'tif';

if nargin == 0
    [maskName,maskDir] = uigetfile('*.*');
    maskPath = fullfile(maskDir,maskName);
else
    maskPath = varargin{1};
end

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        
        switch lower(varargin{jj})
            case 'filtdims'
                filtDims = varargin{jj+1};
            case 'maskthr'
                maskThr  = varargin{jj+1};
            case 'imgext'
                imgExt = varargin{jj+1};
        end
    end
end

mask = ReadImgStack(maskPath,imgExt);
if numel(size(mask))>4
    error('Image stack cannot exceed 4 dimensions!')
elseif numel(size(mask)) ==4
    mask = R2G(mask);
end

mask = Standardize(MF(mask,filtDims));
mask_bin = mask;
mask_bin(mask_bin <= maskThr)=0;
mask_bin(mask_bin > maskThr)=1;

varargout{1} = mask;
varargout{2} = mask_bin;
end

function I_gray = R2G(I_rgb)
siz = size(I_rgb);
I_gray = zeros([siz(1:2) siz(4)]);
for jj = 1:size(I_rgb,4)
    I_gray(:,:,jj) = rgb2gray(I_rgb(:,:,:,jj));
end
end

function I_flt = MF(I,filtDims)
I_flt = zeros(size(I));
disp(['Median filtering with filt of size ' num2str(filtDims)])
dispChunk = round(size(I,3)/3);
for jj = 1:size(I,3)
    if mod(jj, dispChunk)==0
        disp(['Img # ' num2str(jj)])
    end
    I_flt(:,:,jj) = medfilt2(I(:,:,jj),filtDims);
end
end