function incompleteTrlInds = FindIncompleteTrls(trialSegData)
%FindIncompleteTrl - Given trialSegData variable created by
%   AnalyzeEphysData.m, returns indices of incomplete trials, i.e. trials
%   that are of shorter duration than most of the trials because of
%   recording onset and offset delays, etc.
% incompleteTrlInds = FindIncompleteTrls(trialSegData);

trlSizeVec = zeros(length(trialSegData),1);
for jj = 1:length(trialSegData)
    trlSizeVec(jj) = size(trialSegData(jj).raw,1);
end
dSize  = diff(trlSizeVec);
incompleteTrlInds = union(find(dSize<0)+1, find(dSize>0));

if isfield(trialSegData,'cells')
    trlSizeVec = zeros(length(trialSegData),1);
    for jj = 1:length(trialSegData)
        if ~isempty(trialSegData(jj).cells)
            trlSizeVec(jj) = size(trialSegData(jj).cells.raw,2);
        end
    end
    dSize  = diff(trlSizeVec);
end
blah = union(find(dSize<0)+1, find(dSize>0));
incompleteTrlInds = union(blah, incompleteTrlInds);
end

