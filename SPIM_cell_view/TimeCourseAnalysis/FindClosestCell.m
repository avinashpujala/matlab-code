
function varargout = FindClosestCell(cell_info,coordinates)
% 
% closestCell = FindClosestCell(cell_info,coordinates)
% [closestCell, xyzOffset] = FindClosestCell(...)
% 
% Inputs:
% cell_info - Cell-info matrix created by Takashi's code
% coordinates - Coordinates of the point in 3D wrt to which the closest
%               cell is found

allCellPos = zeros(length(cell_info),3);
for cc = 1:length(cell_info)
    allCellPos(cc,:) = [cell_info(cc).center cell_info(cc).slice];
end
posMat = repmat(coordinates,size(allCellPos,1),1);
[~,closestCell] = min(sqrt(sum((allCellPos-posMat).^2,2)));

varargout{1} = closestCell;
varargout{2} = allCellPos(closestCell,:) - coordinates;