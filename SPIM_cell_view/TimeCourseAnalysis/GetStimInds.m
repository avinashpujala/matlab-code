function varargout  = GetStimInds(stimChannel,varargin)
% ExtractStimParams - Extract stimulus parameters such as pulse amplitudes,
% inter-pulse interval, pulse widths, etc from recorded stimulus channel
% stimInds  = ExtractStimParams(stimChannel);
% stimInds  = ExtractStimParams(stimChannel,slopeThreshold);
% stimInds  = ExtractStimParams(stimChannel,[],amplitudeThreshold);
% stimInds  = ExtractStimParams(stimChannel,[],[],minPeakDistance);
%
% stimInds - Vector of stim indices


%% Removing noise from stimulus channel after estimating from histogram
[count,vals] = hist(stimChannel,100);
[~,j] = sort(count,'descend');
noiseLevel = max(vals(j(1))); % Maximum of the 2 most frequently occurring values
stimChannel(abs(stimChannel) < noiseLevel)=0; % Set noise to 0


%% Finding stimulus indices
switch nargin
    case 1
        stimInds = stimartdetect(stimChannel,1:length(stimChannel));
    case 2
        if isempty(varargin{1})
            dStimChannel = abs(diff(stimChannel));
            [count,vals] = hist(dStimChannel,100);
            [~,j] = sort(count,'descend');
            dSThr = max(vals(j(1:2)));
        else
            dSThr = varargin{1};
        end
        stimInds = stimartdetect(stimChannel,1:length(stimChannel),dSThr);
    case 3
        if isempty(varargin{1})
            dStimChannel = abs(diff(stimChannel));
            [count,vals] = hist(dStimChannel,100);
            [~,j] = sort(count,'descend');
            dSThr = max(vals(j(1:2)));
        else
            dSThr = varargin{1};
        end
        if isempty(varargin{2})
            ampThr =noiseLevel;
        else
            ampThr = varargin{2};
        end
        stimInds = stimartdetect(stimChannel,1:length(stimChannel),dSThr,ampThr);
    case 4
        if isempty(varargin{1})
            dStimChannel = abs(diff(stimChannel));
            [count,vals] = hist(dStimChannel,100);
            [~,j] = sort(count,'descend');
            dSThr = max(vals(j(1)));
        else
            dSThr = varargin{1};
        end
        if isempty(varargin{2})
            ampThr = noiseLevel;
        else
            ampThr = varargin{2};
        end
        minPeakDist = varargin{3};
        if isempty(minPeakDist)
            errordlg('Please specify minimum distance between stimuli')
        end
        stimInds = FindStimPulses(stimChannel,1:length(stimChannel),dSThr,ampThr, minPeakDist);
end
stimInds(stimInds==0)=[];
varargout{1} = stimInds;

end

