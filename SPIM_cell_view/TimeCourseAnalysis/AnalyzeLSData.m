
%% Reading 'data' from pyData 
% disp('Reading "data" from pyData...')
% tic
% data = pyData.data;
% toc

%% Input parameters
imgDir                      = 'S:\Avinash\SPIM\Alx\10-3-2015_AlxRG-relaxed-dpf4\Fish1\g50-t1-ipi6-f1_20151003_172727\Registered';
cells.dFFDetThr             = 0.1;
exposureTime                = 18e-3;
data.baselinePeriod         = data.preStimPeriod;
timeSegments                = [data.baselinePeriod 15 30]; % For identifying responsive cells
percOfCellsToPlot           = 5;
smoothingType               = 'low';
% stimCondsToAvg              = stim.indMat{:,3}; % Rows correspond to stim1 conditions, cols to stim2 conditions

zPlaneList                  = []; % Leaving this empty will skip ROI-based analysis

%% Loading cell response matrix and ave_stack

disp('Getting relevant info from pyData...')
trialSegData = pyData.trialSegData;
tData = pyData.tData;

cellRespFile = [imgDir '\cell_resp3.mat'];
cellInfoFile = fullfile(imgDir,'cell_info_processed.mat');
disp('Loading cell response matrix...')
load(cellRespFile);
load(cellInfoFile);
cells.raw  = cell_resp3;
cells.info = cell_info;

if isfield(data,'t')
    data.samplingRate = round(1/(data.t(2)-data.t(1)));
elseif isfield(data,'samplingRate')
    data.t = (0:length(data.artless)-1)*(1/data.samplingRate);
else
    data.samplingRate = input('Please enter sampling rate for ephys data (usually, 6000): ');
    data.t = (0:length(data.artless)-1)*(1/data.samplingRate);
end

stack = GetStackData(data,tData,cells,imgDir,exposureTime);
stim = data.stim;

if round(stim.times(1)) < round(trialSegData(1).stim.time)
    stim.times(1) = [];
    stim.inds(1) = [];
    stim.amps(:,1) = [];
elseif round(stim.times(end)) > round(trialSegData(end).stim.time)
    stim.times(end) = [];
    stim.inds(end) = [];
    stim.amps(:,end) = [];
end
data.stim = stim;


%# Find trials that are either outside the Ca2+ imaging period for some reason, or that do not...
%#      accommodate specified postStim period
tData.outImgTrls = find((stack.inds.shock + ceil(data.postStimPeriod/stack.interval) > size(cells.raw,2)) |...
    (stack.inds.shock - ceil(data.preStimPeriod/stack.interval)) < 1);

% stack.inds.shock(tData.outImgTrls) = [];
% trialSegData(tData.outImgTrls(tData.outImgTrls <= size(trialSegData,1))) = [];
%
% %# Update nTrialMat to reflect removal of outside imaging trials
% stimGrps = tData.stimGroup;
% stimGrps(tData.outImgTrls(tData.outImgTrls <= size(trialSegData,1)))=[];
% for trl = 1:numel(unique(tData.stimGroup))
%     nTrialMat(trl) = sum(stimGrps == trl);
% end


%% Converting raw cells traces to dF/F & filtering a bit
tic
nyqFreq = 0.5*(1/stack.interval);
lpf = 0.9*nyqFreq;
kernelWidth = max(round(1/lpf),5);
kernel = gausswin(kernelWidth); % Not using at the moment
kernel = kernel/sum(kernel);

lastPt = min(stack.N,size(cells.raw,2));
nCells = size(cells.raw,1);
cells.time = data.t(stack.inds.start);
extraStacks = size(cells.raw,2)- length(cells.time);
if extraStacks > 0
%     cells.raw(:,end-extraStacks+1:end)=[]; 
    cells.raw(:,1:extraStacks)=[];
elseif extraStacks < 1
    cells.time = cells.time(1:size(cells.raw,2));
end

nBaselineFrames = round(min(10,data.baselinePeriod)/stack.interval);
baselineInds = [];
for shock = 1:numel(stack.inds.shock())
    trlBaselineInds = stack.inds.shock(shock)-nBaselineFrames:stack.inds.shock(shock)-1;
    if any(trlBaselineInds < 0) || any(trlBaselineInds > numel(stack.inds.start))
        disp(['Skipped trial # ' num2str(shock) ' in computing dF/F'])
    else
        baselineInds = [baselineInds, trlBaselineInds];
    end
end
baselineInds(baselineInds <=0) = [];
baselineInds(baselineInds > size(cells.raw,2)) = [];
baselineF = repmat(mean(cells.raw(:,baselineInds),2),1,size(cells.raw,2));
cells.dFF = (cells.raw - baselineF)./baselineF;

disp(['Smoothing cellular Ca++ responses with lowpass of  ' num2str(lpf)])
if strcmpi(smoothingType,'conv')
    cells.dFF = chebfilt(conv2(double(cells.dFF'),kernel(:)','same'),stack.interval,1/200,'high');
    cells.dFF = cells.dFF';
else
    cells.dFF = chebfilt(chebfilt(double(cells.dFF'),stack.interval,lpf,'low'),stack.interval,1/200,'high');
    cells.dFF = cells.dFF';
end
toc

%% Setting roi flag based on contents of z plane list
if isempty(zPlaneList)
    roiFlag = false;
else
    dir_proc = [imgDir '\proc'];
    roiFlag = true;
    [cells.roi.dFF, cells.roi.inds] = GetIntraRoiCellTimeseries(dir_proc,zPlaneList,cells.dFF,cell_info);
end

%% Segmenting Cell Responses by Trial
tic
disp('Segmenting cell responses by trials')
nBaselineFrames = floor(data.baselinePeriod/stack.interval);
shockStackIdx = nBaselineFrames + 1;

data.postStimPeriod = max(timeSegments(3),(0.8*stim.interval.min));

cells_trial.raw = SegmentDataByEventsVer2(cells.raw,...
    stack.interval,stack.inds.shock,data.baselinePeriod, data.postStimPeriod);

cells_trial.dFF = SegmentDataByEventsVer2(cells.dFF, stack.interval,...
    stack.inds.shock,data.baselinePeriod,data.postStimPeriod);

if ~ isfield(data,'incompleteSegInds')
    data.incompleteSegInds = FindIncompleteTrls(trialSegData);
end
completeSegInds = setdiff(1:size(trialSegData,1),data.incompleteSegInds);
totFrames = size(cells_trial.raw{completeSegInds(1)},2);
trialSegData(1).cells.time = ((1:totFrames)-shockStackIdx)*stack.interval+stack.interval;
nBaselineFrames = round(min(10,data.baselinePeriod)/stack.interval);
baselineInds = shockStackIdx-nBaselineFrames:shockStackIdx-1;
baselineInds(baselineInds<=0)=[];

for trial = 1:numel(stack.inds.shock)   
        trialSegData(trial).stim.stackInds = stack.inds.shock(trial);
        trialSegData(trial).cells.raw = cells_trial.raw{trial};
        baselineF = repmat(mean(cells_trial.dFF{trial}(:,baselineInds),2),1,size(cells_trial.dFF{trial},2));
        trialSegData(trial).cells.dFF = cells_trial.dFF{trial} - baselineF;   
end
toc

disp('Updating trialSegData in pyData...')
tic
if pyData.Properties.Writable
    pyData.trialSegData = trialSegData;
else
    pyData.Properties.Writable = true;
    pyData.trialSegData = trialSegData;
end
toc

%% Finding responsive cells, creating multidimensional cell resp matrix, trial-avged cell response matrix
tic
responseInds = shockStackIdx:shockStackIdx + ceil(10/stack.interval);
responsiveCells = [];

for trial = 1:numel(stack.inds.shock)
    preDeviation = sqrt(sum((trialSegData(trial).cells.dFF(:,baselineInds)- ...
        repmat(trialSegData(trial).cells.dFF(:,shockStackIdx),1,numel(baselineInds))).^2,2))/numel(baselineInds);
    if sum(trial==data.incompleteSegInds)==0    
    postDeviation = (sum((trialSegData(trial).cells.dFF(:,responseInds)- ...
        repmat(trialSegData(trial).cells.dFF(:,shockStackIdx),1,numel(responseInds))).^2,2))/numel(responseInds);
   
    respondingCells = find(abs(postDeviation-preDeviation)./preDeviation > 5);
    responsiveCells = union(responsiveCells,respondingCells);
    end
end

cells.nonResponsiveCellInds = setdiff(1:size(cells.raw,1),responsiveCells);
cells.responsiveCellInds = responsiveCells;
disp([ num2str(numel(responsiveCells)) ' responsive cells identified: ' ...
    num2str(numel(responsiveCells)/size(cells.raw,1)*100)  '% of all cells' ])

disp('Creating Cell Response Matrix and Trial-Averaged Cell Resp Matrix')
cellInds = 1:size(cells.raw,1);
if ~isfield(data.stim,'nTrialMat')
    [data,~] = GetStimSegData(data,trialSegData);
end
nTrialMat = data.stim.nTrialMat;
maxNumTrials = max(nTrialMat(:));

cells.respMat.all= zeros(size(trialSegData(1).cells.dFF,1),size(trialSegData(completeSegInds(1)).cells.dFF,2),...
    numel(data.stim.conditions{1}),numel(data.stim.conditions{2}),maxNumTrials);
stimAmps = tData.stimAmp;
stimAmps(tData.outImgTrls) = [];
for one = 1:numel(data.stim.conditions{1})
    for two = 1:numel(data.stim.conditions{2})
        trialInds = find(stimAmps == nonzeros([data.stim.conditions{1}(one),...
            data.stim.conditions{2}(two)]));
        [~,remInds] = intersect(trialInds,data.incompleteSegInds);
        trialInds(remInds)=[];
        for trial = 1:length(trialInds)
            if ~isempty(trialSegData(trialInds(trial)).cells)
                cells.respMat.all(:,:,one,two,trial) = trialSegData(trialInds(trial)).cells.dFF;
            end            
        end
    end
end
cells.respMat.trialAvg = sum(cells.respMat.all,5);
toc

%% Cell responses during weak and strong swim trials
data.incompleteSegInds = FindIncompleteTrls(trialSegData);
[wkSwmCellResp,strngSwmCellResp] = ...
    deal(zeros(size(cells.info,2),(size(trialSegData(completeSegInds(1)).cells.dFF,2))));
trlInds = tData.wkSwmTrls;
if ~isempty(trlInds)
    trlInds(trlInds > numel(stack.inds.shock)) = [];
    [~, inds] = intersect(trlInds,data.incompleteSegInds);
    trlInds(inds)=[];
    for trial = 1:numel(trlInds)
        wkSwmCellResp  = wkSwmCellResp + trialSegData(trlInds(trial)).cells.dFF;
    end
    wkSwmCellResp = wkSwmCellResp/numel(trlInds);
end

trlInds = tData.strngSwmTrls;
if ~isempty(trlInds)
    trlInds(trlInds > numel(stack.inds.shock)) = [];
    [~, inds] = intersect(trlInds,data.incompleteSegInds);
    trlInds(inds)=[];
    for trial = 1:numel(trlInds)
        strngSwmCellResp  = strngSwmCellResp + trialSegData(trlInds(trial)).cells.dFF;
    end
    strngSwmCellResp = strngSwmCellResp/numel(trlInds);
end

%% Finding responsive cells from cell data
thresh = 0.15;
blah = max(cat(3,abs(wkSwmCellResp),abs(strngSwmCellResp)),[],3);
preMean = mean(blah(:,baselineInds),2);
postMean = max([mean(blah(:,responseInds(1:3)),2),mean(blah(:,responseInds),2)],[],2);
cells.responsiveCellInds = find(postMean - preMean > 0.15);
thresh = 0.1;
blah = max(cat(3,abs(wkSwmCellResp),abs(strngSwmCellResp)),[],3);
preMean = mean(blah(:,baselineInds),2);
postMean = max([mean(blah(:,responseInds(1:3)),2),mean(blah(:,responseInds),2)],[],2);
cells.responsiveCellInds = find(postMean - preMean > thresh);

clrMap = MapValsToColors(postMean-preMean,colormap(hot));

%% Load activity map and append to data struct for use by 'Do_GUI.m'
tic
if  sum(strcmpi(fieldnames(pyData),'activityMap'))==0
    disp('Reading ActivityMap and appending to ''data'' struct')
    dir_proc = [imgDir '\proc'];
    imFileName = fullfile(dir_proc,'ActivityMap.tif');
    imgInfo = imfinfo(imFileName);
    activityMap = [];
    activityMap(:,:,:,1) = imread(imFileName,1);
    for z = 2:size(imgInfo,1)
        disp(['Plane ' num2str(z)])
        activityMap(:,:,:,z) = imread(imFileName,z);
    end
    activityMap = activityMap/max(activityMap(:));
    disp('Updating pyData with activityMap..')
    pyData.activityMap = activityMap;
end
toc

%% Updating matfile
disp('Updating pyData...')
tic
pyData.Properties.Writable = true;
pyData.cells = cells;
pyData.stack = stack;
pyData.Properties.Writable = false;
toc

break;

%% ############ Extra Optional Stuff #####


%% Concatenate peri-stimulus time points in cell traces for clustering
nPreStimFrames = 3;
nPostStimFrames = 30;
nClstrs = 40;
nTrls = numel(data.stack.inds.shock);
blah = cell(nTrls,1);
cells.dFF_periStim = [];
for trl = 1:nTrls
    blah{trl} = cells.dFF(cells.responsiveCellInds,max(1,data.stack.inds.shock(trl)-nPreStimFrames): ...
        min(size(cells.dFF,2),data.stack.inds.shock(trl)+nPostStimFrames));
    cells.dFF_periStim = cat(2,cells.dFF_periStim,blah{trl});
end

%% Use M-cell trace as a seed
seed  = zeros(nClstrs,size(cells.dFF_periStim,2));
mCellIdxInRespCells = find(cells.responsiveCellInds == cells.MCellIdx);
cellList = 1:numel(cells.responsiveCellInds);
cellList(mCellIdxInRespCells) = [];
rng(1);
cellList_shuffled = cellList(randperm(length(cellList)));
seedInds = [mCellIdxInRespCells cellList_shuffled(1:nClstrs-1)];

%% Save
procDir = fullfile(imgDir,'proc');
dataToBeClstred = cells.dFF_periStim;
seedMat = cells.dFF_periStim(seedInds,:);
disp('Saving data...')
save(fullfile(procDir,['dataToBeClstred_' num2str(datestr(now,30))]),...
    'dataToBeClstred','seedMat');


%#### Run clustering and load kData, then....
kData.mCellClstr = kData.idx(cells.responsiveCellInds == cells.MCellIdx);
kData.cellsInMClstr  = cells.responsiveCellInds(kData.idx==kData.mCellClstr);

fh = MapCellIndsToAveStack(cellsInMClstr,allData.cells.info,stack.avg);
disp('Saving data...')
tic
save(fullfile(procDir,'allData'),'data','kData','-v7.3');
toc


%% Save as pyData

slashInds = strfind(data.filename, '\');
ephysDir = data.filename(1:slashInds(end)-1);
savedDir = fullfile(imgDir,'saved');
if ~exist(savedDir)
    mkdir(savedDir)
end
success = copyfile(fullfile(ephysDir,'pyData.mat'),fullfile(savedDir,'pyData.mat'));
if success
    disp('Successfully copied pyData.mat from ephysDir to imgDir\saved')
end

pyData = matfile(fullfile(savedDir,'pyData.mat'));
pyData.cells = cells;
pyData.stack = data.stack;
pyData.activityMap = data.activityMap;
pyData.kData = kData;
pyData.Properties.Writable = false;


%% Older saving

pyData.opTime= trialSegData(1).cells.time;
pyData.stackInds_start = stack.inds.start;
pyData.stackInds_stim  = stack.inds.shock;
pyData.stackInt = stack.interval;
try
    pyData = rmfield(pyData,{'cells','cellRespMat'});
end

answer = questdlg('Save .mat variables?','Saving relevant matlab variables as pyData structure','No');
if strcmpi(answer,'Yes')
    disp('Saving variable structure')
    procDir = [imgDir '\Processed'];
    bSize = whos('pyData');
    bSize = bSize.bytes;
    tic
    try
        save(fullfile(procDir,'pyData'),'pyData', '-v7.3')
    catch
        mkdir(procDir)
        save(fullfile(procDir,'pyData'),'pyData', '-v7.3')
    end
    eTime = toc;
    disp(['Saved ~' num2str(round(bSize/(1000^3))) ' GB in ' num2str(round(eTime*100)/100) 'sec'])
end


%% Estimating Ca Impulse Response Function from M-cell activity
mCellIdx = cells.mCellInd;
timePts = [0 9];
mCellStrngSwmDFF = zeros(numel(tData.strngSwmTrls),length(trialSegData(1).cells.time));
for trl = 1:numel(tData.strngSwmTrls)
    mCellStrngSwmDFF(trl,:) = trialSegData(tData.strngSwmTrls(trl)).cells.dFF(mCellIdx,:);
end
cells.CIRF = mean(mCellStrngSwmDFF(trl,:),1);
inds = find(trialSegData(1).cells.time >=timePts(1) & trialSegData(1).cells.time <= timePts(2));
cells.CIRF = cells.CIRF(inds);
cells.CIRF = cells.CIRF-cells.CIRF(1);
fpt = find(cells.CIRF == max(cells.CIRF));
lpt = find(cells.CIRF(fpt:end)<=0,1,'first') + fpt -1;
cells.CIRF = cells.CIRF(fpt:lpt);

x = 1:length(cells.CIRF);
xx = linspace(1,x(end),(length(cells.CIRF)-1)*stack.interval*data.samplingRate);
cells.CIRF = interp1(x,cells.CIRF,xx,'cubic');

g = fittype('A*exp(-x/tau)');
t = (1:length(cells.CIRF))*stack.interval;
f = fit(t(:),cells.CIRF(:),g);
cells.CIRF = f.A*exp(-t/f.tau);
cells.CIRF = cells.CIRF-min(cells.CIRF);
cells.CIRF = cells.CIRF/sum(cells.CIRF);


%% Attempting some regression
tic
stimInput = zeros(size(data.stim2));
stimInput(data.stim.inds) = data.stim.amps(2,:);
stimRegressor = conv(stimInput(:),cells.CIRF(:));
stimRegressor = stimRegressor(1:length(stimInput));
toc

%% Finding cells in hot zones in activity map
[cellCtrMask, hotPxlMask] = deal(zeros(stack.dim(1:3)));
disp('Creating hot pixel mask')
hotPxls = find(squeeze(sum(activityMap_rg,3))>0);
hotPxlMask(hotPxls)=1;

disp('Creating cell center mask')
respCells = zeros(size(cells.info,2),1);
cellZ = [cells.info(:).slice];
for pln = 1:stack.dim(3)
    cellsInPln = find(cellZ==pln);
    for cc = 1:numel(cellsInPln)
        coord =  cells.info(cellsInPln(cc)).center;
        cellCtrMask(coord(1), coord(2), pln)= 1;
        if cellCtrMask(coord(1), coord(2),pln)*hotPxlMask(coord(1), coord(2),pln) == 1
            respCells(cellsInPln(cc))=1;
        end
    end
end
cellsInHotPxlsMask = cellCtrMask.*hotPxlMask;
cells.respCells = find(respCells);

%%  Extracing Ca++ Response Parameters
% tic
% for trial = 1:size(trialSegData,1)
%     disp(['Extracting Ca++ Response Parameters, Trial # ' num2str(trial)])
%     relevantTraces = trialSegData(trial).cells.dFF(cells.responsiveCells,:);
%     Ca = struct;
%     [Ca.peakLatency,Ca.peakAmp,Ca.decayTime] = ...
%         ExtractCaResponseParams(relevantTraces,cells.dFFDetThr,shockStackIdx,maxRespInd,stack.interval);
%     trialSegData(trial).cells.peakLatency = Ca.peakLatency;
%     trialSegData(trial).cells.peakAmp = Ca.peakAmp;
%     trialSegData(trial).cells.decayTime = Ca.decayTime;
% end
% toc

%%  Extracing Ca++ response parameters from averaged weak and strong swim cell responses
tic
strng = struct;
maxRespInd = ceil(timeSegments(2)/stack.interval);
[strng.peakLatency,strng.peakAmp,strng.decayTime] = ...
    ExtractCaResponseParams(strngSwmCellResp,cells.dFFDetThr,shockStackIdx,maxRespInd,stack.interval);
[wk.peakLatency,wk.peakAmp,wk.decayTime] = ...
    ExtractCaResponseParams(wkSwmCellResp,cells.dFFDetThr,shockStackIdx,maxRespInd,stack.interval);

for c = 1:size(strngSwmCellResp,1)
    strng.peakAmp(c,2) = strngSwmCellResp(c,strng.peakAmp(c,1));
    wk.peakAmp(c,2) = wkSwmCellResp(c,wk.peakAmp(c,1));
end
toc



%% ######## Some Plotting #######


%% Plot strong swim responses for randomly chosen responsive cells & trials
scl = 0.1;
shifts = repmat((1:20)*scl,size(trialSegData(1).cells.dFF,2),1)';
figure('Name', 'Strng swm rsps of a few rndm cells')
for jj = 1:100
    trl = randperm(size(trialSegData,1),1);
    cInds = randperm(numel(cells.responsiveCellInds),20);
    plot(trialSegData(1).cells.time,strngSwmCellResp(cInds,:)-shifts)
    xlim([-inf inf])
    ylim([-inf inf])
    pause()
    shg
end





%% Writing Data to a File
answer = questdlg('Write Trial Data to File?','Writing trial data to .csv file','No');
if strcmpi(answer,'Yes');
    tic
    disp('Writing trial data to a file')
    csvName = fullfile(imgDir,'DataSummary');
    WriteEphysAndCellDataToFile(csvName, trialSegData)
    toc
end

%% Save cellRespMat
tic
outputDir = [imgDir '\' 'Processed'];
cellRespMat = [];
cellRespMat.all = data.cellRespMat.all(cells.responsiveCellInds,:,:,:,:);
cellRespMat.trialAvg = data.cellRespMat.trialAvg(cells.responsiveCellInds,:,:,:);
cellResponses = cells.smooth(cells.responsiveCellInds,:);
try
    save(fullfile(outputDir,'cellRespMat'),'cellRespMat', '-v7.3')
    save(fullfile(outputDir,'cellResponses'),'cellResponses','-v7.3')
catch
    mkdir(outputDir)
    save(fullfile(outputDir,'cellRespMat'),'cellRespMat', '-v7.3')
    save(fullfile(outputDir,'cellResponses'),'cellResponses','-v7.3')
end
disp('Saved cellRespMat & cellResponses')
toc
clear cellRespMat cellResponses

%% Can end here for the time being
return;






%% Testing peak detection
testPlot = questdlg('Plot a few random cells?', 'Plot or Not','No');
if strcmpi('Yes',testPlot)
    nCellsToPlot = round((5/100)*numel(cells.responsiveCellInds));
    plotCellList = round(rand(nCellsToPlot,1)*numel(cells.responsiveCellInds));
    figure('Name','Plot checking peak detection, etc'), box off
    shg
    for someCell = [plotCellList(:)']
        trial = round(rand(1,1)*size(trialSegData,1));
        cla
        hold on
        title(['Cell # ' num2str(someCell),', Trial # ' num2str(trial) , ', Peak latency = ' num2str(trialSegData(trial).cells.peakLatency(someCell))...
            ', Peak amp = ' num2str(trialSegData(trial).cells.peakAmp(someCell)*100)])
        plot(trialSegData(1).cells.time,trialSegData(trial).cells.dFF(someCell,:))
        maxY = max(trialSegData(trial).cells.dFF(someCell,:));
        xlim([trialSegData(1).cells.time(1) trialSegData(1).cells.time(end)]), ylim([-inf maxY+0.01]),
        peakInd = round(trialSegData(trial).cells.peakLatency(someCell)/stack.interval);
        decayInd = round(trialSegData(trial).cells.decayTime(someCell)/stack.interval);
        plot(trialSegData(1).cells.time(peakInd),trialSegData(trial).cells.dFF(someCell,peakInd),'ro')
        plot(trialSegData(1).cells.time(decayInd),relevantTraces(someCell,decayInd),'k*')
        pause()
    end
end


%% Colored raster plot of cell responses
figure('Name', 'Colored Raster Plot of Cell Responses')
imagesc(trialSegData(1).cells.time,1:numel(responsiveCells),relevantTraces), colorbar
set(gca,'tickdir','out'), box off
xlabel('Time (sec)')
ylabel('Cell Number')


%% Plotting mean activity of all responsive cells
figure('Name','Mean dF/F activity of all responsive cells')
plot(trialSegData(1).cells.time, mean(relevantTraces,1)*100)
xlim([-inf inf]), ylim([-inf inf])
box off
xlabel('Time (sec)','fontsize',14)
title('Mean dF/F activity of all responsive cells','fontsize',14)

%% Integrating cell responses for sorting cells based on persistence of responses
intNormRelevantTraces = cumsum(relevantTraces - repmat(min(relevantTraces,[],2),1,size(relevantTraces,2)),2);
intNormRelevantTraces = intNormRelevantTraces./repmat(intNormRelevantTraces(:,end),1, size(relevantTraces,2));
[~,i50] = min(abs(intNormRelevantTraces-0.5),[],2);
t50 = (i50-shockStackIdx)*stack.interval;
[~,i40] = min(abs(intNormRelevantTraces-0.4),[],2);
t40 = (i40-shockStackIdx)*stack.interval;
s45 = 100./(t50-t40); % slope at 0.45

%% Peak Times
% Projecting colors onto cumulative distribution of values
considerdCells = [];
mappedVar = [];
consideredCells = responsiveCells;

% mappedVar = t50;
% mappedVar = s45;

mappedVar = timeToPeakFromStim;

%

% mappedVar = peakVals*100; % Multiplying by 100 to express dF/F as percent change
% badInds = find(peakVals<0);
% mappedVar(badInds)=[];
% consideredCells(badInds)=[];


[count,vals] = hist(mappedVar,25);
figure('Name','Histogram of peak times')
% subplot(3,1,1)
count = count/sum(count);
bar(vals,count)
xlim([-inf inf])
xlabel('Time to peak from stim (s)')
ylabel('Probability')
box off
set(gca,'tickdir','out')
title('Histogram of peak times')
box off
set(gca,'tickdir','out')

cutoff = mean(mappedVar) + 1*std(mappedVar);
[mappedVar, inds] = sort(mappedVar);
[~, lastPt] = min(abs(mappedVar-cutoff));
mappedVar = mappedVar(1:lastPt);
inds = inds(1:lastPt);

consideredCells = consideredCells(inds);
nCells = numel(consideredCells);

[colorValues,LUT,cmap_new] = MapValsToColors(mappedVar);

extract_SPIM_example

%% Decay Times

considerdCells = [];
mappedVar = [];
consideredCells = responsiveCells;

mappedVar = decayTimes;

[count,vals] = hist(mappedVar,50);
figure('Name','Histogram of decay times')
% subplot(3,1,2)
count = count/sum(count);
bar(vals,count)
xlim([-inf inf])
xlabel('Time to decay from response peak (s)')
ylabel('Probability')
box off
set(gca,'tickdir','out')
title('Histogram of decay times')
box off
set(gca,'tickdir','out')

cutoff = mean(mappedVar) + 1*std(mappedVar);
[mappedVar, inds] = sort(mappedVar);
[~, lastPt] = min(abs(mappedVar-cutoff));
mappedVar = mappedVar(1:lastPt);
inds = inds(1:lastPt);

consideredCells = consideredCells(inds);
nCells = numel(consideredCells);
[colorValues,LUT,cmap_new] = MapValsToColors(mappedVar);

% extract_SPIM_example

%% Peak Values

considerdCells = [];
mappedVar = [];
consideredCells = responsiveCells;

mappedVar = peakVals*100; % Multiplying by 100 to express dF/F as percent change
badInds = find(peakVals<0);
mappedVar(badInds)=[];
consideredCells(badInds)=[];
[count,vals] = hist(mappedVar,50);
figure('Name','Histogram of peak values')
% subplot(3,1,3)
count = count/sum(count);
bar(vals,count)
xlim([-inf inf])
xlabel(' Peak value (dF/F)')
ylabel('Probability')
box off
set(gca,'tickdir','out')
title('Histogram of peak values')
box off
set(gca,'tickdir','out')

cutoff = mean(mappedVar) + 1*std(mappedVar);
[mappedVar, inds] = sort(mappedVar);
[~, lastPt] = min(abs(mappedVar-cutoff));
mappedVar = mappedVar(1:lastPt);
inds = inds(1:lastPt);

consideredCells = consideredCells(inds);
nCells = numel(consideredCells);
[colorValues,LUT,cmap_new] = MapValsToColors(mappedVar);

% extract_SPIM_example