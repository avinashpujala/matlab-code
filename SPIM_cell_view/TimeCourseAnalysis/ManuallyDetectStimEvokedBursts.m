
%% Manual Detection of Stim-Evoked Swim Bursts for Determining Swim Freq & Onset

% answer =  questdlg('Would you like to manually detect stim-evoked swim bursts?','Manual Swim Burst Detection',...
%     'Yes','No','Yes');
answer = 'No';
if strcmp(answer, 'Yes')
    thresh = 5;
    jitter = round(2.5e-3 *samplingRate); % Jitter in ms that can occur when manually detecting peaks
    betterChannel = find(nB == max(nB)); % Use signal with more bursts
    trialSegBurstInds = cell(size(trialSegmentedData));    
    monSize = getMonitorSize;
    monSize(4) = monSize(4)*0.9;
    %     monSize = [monSize(1)+100 monSize(2)+500 monSize(3)*0.9 monSize(4)*0.9];
    
    if (min(nB)/max(nB)>=0.5) & sum(nB)> 0;
        channels = ePhysChannelToAnalyze;
    else
        channels = ePhysChannelToAnalyze(betterChannel);
    end
    
    [freqVec,onsetVec] = deal(zeros(length(trialSegmentedData),numel(channels)));
    
    for trial = 1: length(trialSegmentedData)
        for cc = 1:numel(channels)
            fh = figure('Name',['Manual Burst Detection - Trial # '  num2str(trial)],'Position',monSize);
            %         plot(segmentTime*1000, zscore(trialSegmentedData{trial}(:,betterChannel)))
            %         hold on
            %         plot(segmentTime*1000, zscore(trialSegData_smooth{trial}(:,betterChannel)),'r','linewidth',2)
            %         xlim([-100 500])
            %         ylim([-inf inf])
            %         title('Left click & drag to zoom, right click to select range, and hit Enter to exit')
            %         v = zinput(1);
            %         axis(v); hold on
            %         cla
            plot(segmentTime*1000, zscore(trialSegmentedData{trial}(:,cc)))
            grid on
            hold on
            plot(segmentTime*1000, 1.5*zscore(trialSegData_smooth{trial}(:,cc)),'r','linewidth',2)
            plot(segmentTime*1000,thresh*ones(size(trialSegmentedData{trial}(:,cc))),'k:')
            xlim([-50 250]), ylim([-40 40])
            title('Left-click on burst peaks to select values;  Right click or hit Enter to exit')
            [x,~,button] = ginput();
            x = round((x/1000)*samplingRate); % Convert to pts
            if (~isempty(x) & numel(x)>1)
                %             for jj = 1:length(x)
                %                 blah = trialSegData_smooth{trial}(x(jj)-jitter:x(jj)+jitter,betterChannel);
                %                 [~,localMax] = max(blah);
                %                 x(jj) = x(jj)-jitter + localMax;  % Adjusting x value to max around manually detected region
                %             end
                
                %          plot(segmentTime(x)*1000,trialSegData_smooth{trial}(x,betterChannel),'ro')
                %         freqVec(trial) = round(max(1./diff(x*samplingInt)));
                freqVec(trial,cc) = round(1./((x(2)-x(1))*samplingInt));
                onsetVec(trial,cc) = x(1)*samplingInt;
            else
                freqVec(trial,cc)= 0;
                onsetVec(trial,cc) = 0;
            end
            close(fh);
%             trialSegBurstInds{trial} = stimInds(trial)+ x; % Commented
%             out for the time being
        end % channel loop
    end % trial loop
    
end


