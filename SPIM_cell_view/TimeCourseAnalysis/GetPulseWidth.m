function pulseWidth  = GetPulseWidth(stimChannel,maxPulseWidth)
% ExtractStimParams - Extract stimulus parameters such as pulse amplitudes,
% inter-pulse interval, pulse widths, etc from recorded stimulus channel
% pulseWidth  = GetPulseWidth(stimChannel);
% pulseWidth = GetPulseWidth(stimChannel,slopeThreshold)


% pulseWidth - Width of stimulus pulses


%% Removing noise from stimulus channel after estimating from histogram
[count,vals] = hist(stimChannel,100);
[~,j] = sort(count,'descend');
noiseLevel = max(vals(j(1))); % Maximum of the 2 most frequently occurring values
stimChannel(abs(stimChannel) < noiseLevel)=0; % Set noise to 0

%% Finding stimulus indices
d2Stim = abs(diff(stimChannel,2));
[count,vals] = hist(d2Stim,100);
[~,j] = sort(count,'descend');
d2SThr = max(vals(j(1)));

onOffs = find(d2Stim > d2SThr) + 1;
onsets = onOffs(1:2:end);
offsets = onOffs(2:2:end);

offMat = repmat(offsets(:),1,numel(onsets));
onMat = repmat(onsets(:)',size(offMat,1),1);
pwMat = offMat - onMat;
pwVec = abs(pwMat(:));
pwVec(pwVec>=maxPulseWidth)=[];
if any(pwVec~=1)
pwVec(pwVec==1)=[];
end
pulseWidth = mode(pwVec);

% [count,vals] = hist(pwVec,100);
% 
% [~,j] = sort(vals);
% count = count(j); 
% f = find(count >= 2,1,'first');
% 
% pw_approx = vals(f);
% [~,f] = min(abs(pwVec - pw_approx));
% pulseWidth = pwVec(f);



end

