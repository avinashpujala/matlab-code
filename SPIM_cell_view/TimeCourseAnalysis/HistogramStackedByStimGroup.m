function groupMat = HistogramStackedByStimGroup(structData,fieldName,valueVector,zeroRemoveOption)
% fh = HistogramStackedByStimGroup(structData,fieldName,valueVector,zeroRemoveOption)
% 
% zeroRemoveOption = 1; zero values removed, else kept



tData = structData;
var = tData.(fieldName);
if zeroRemoveOption == 1
%     tData.(fieldName)(tData.(fieldName)==0)= nan;
     var(var==0) = nan;
end


fh = figure();
box off, hold on
set(gca,'tickdir','out')
title(['Histogram of stim-evoked ' num2str(fieldName)])


[counts,vals] = hist(var, valueVector);

counter = 0;
% while any(counts <= 1)
%     counter = counter+1;
%     dV  = dV+1;
%     [counts,vals] = hist(var,minVal:dV:maxVal);
%     if counter > 10
%         break;
%     end
% end
nStimGroups = numel(unique(tData.stimGroup));
nDataGroups = numel(counts(counts~=0));
groupMat = zeros(nDataGroups,nStimGroups);
valsWithCount = vals(counts~=0);
for trial = 1:length(var)   
    [~,ind] = min(abs(valsWithCount-var(trial)));
    stimGroup = tData.stimGroup(trial);
    groupMat(ind,stimGroup) = groupMat(ind,stimGroup) + 1;
end

lgnd = cell(nStimGroups,1);
for stimGroup = 1:nStimGroups
   lbl = ['StimGroup: ' num2str(stimGroup)];
   lgnd{stimGroup,1} = lbl;
end
bar(valsWithCount,groupMat,'stacked')
% bar(valsWithCount,groupMat,'grouped')
ylabel('Count')
xlabel(num2str(fieldName))
legend(lgnd)
varargout{1} = groupMat; 
