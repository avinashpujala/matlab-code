%% ThunderRegisteredImagesToTemporalStacks
% Converts registered Z-stacks written by thunder to temporal stacks a ala
% Takashi's format, except compressed .tif files instead of binaries.


% clear all, close all

imgDir = 'Z:\SPIM\Avinash\Raw\May 2015\5-9-2015_relaxed_dpf4\Fish3\G50_T1_IPI60_20150509_200754';
regDir = [imgDir '\reg'];
if ~exist(regDir)   
    mkdir(regDir);
    disp(['Created dir: ' regDir])
end

blah = xml2struct(fullfile(imgDir,'ch0.xml'));
stackDimStr = blah.push_config.info{14}.Attributes.dimensions;
xInds = strfind(stackDimStr,'x');

stackDims(1) = str2double(stackDimStr(1:xInds(1)-1));
stackDims(2) = str2double(stackDimStr(xInds(1)+1:xInds(2)-1));
stackDims(3) = str2double(stackDimStr(xInds(2)+1:end));

fid = fopen(fullfile(imgDir,'Stack_frequency.txt'),'r');
info = fscanf(fid,'%f');
stackDims(4) = info(3);
fclose(fid);

blah = [];
for z = 1:stackDims(3);
    tic
    disp(['Plane ' num2str(z)])
    tifName = ['Plane' num2str(z,'%0.2d')];
     if exist(fullfile(regDir, tifName))==2;
         delete(fullfile(regDir,tifName));
     end
    fid = fopen(fullfile(regDir,tifName),'w');
    for t = 0: stackDims(4)-1;
        fName = ['TM' num2str(t,'%0.5d') '_CM0_CHN00.stack'];
       temp = MappedTensor(fullfile(imgDir,fName),stackDims(1:3),'class','uint16');      
         imwrite(temp(:,:,z),fullfile(regDir,tifName),'tif','WriteMode','append','Compression','lzw');
        disp(['Stack # ' num2str(t+1)])
    end
    fclose(fid);
    toc
end