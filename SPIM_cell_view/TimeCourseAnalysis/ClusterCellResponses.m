function varargout = ClusterCellResponses(varargin)
%ClusterCellResponses Cluster's cell responses
% kMeansData = ClusterCellResponses(fid,nClusters)  

% Inputs;
% fid = path\cellRespMat

rng(14,'twister')

fid = varargin{1};
if nargin < 2
    nClusters = 10;
else
nClusters  = varargin{2};
end

tic
load(fid)
disp('Loaded cellRespMat...')
toc

% stimGrp = size(cellRespMat.trialAvg,4);
% data = cellRespMat.trialAvg(:,:,1,stimGrp);
data = sum(cellRespMat.trialAvg,4); % For now, averageing across stim conditions and trials

tic
[idx, C, sumd, D] = kmeans(data,nClusters);
disp(['K-means clustered data into ' num2str(nClusters) ' clusters'])
toc

kData.data = data;
kData.idx = idx;
kData.C = C;
kData.sumd = sumd;
kData.D = D; 

% kData = RemoveWeakSignalsFromClusters(kData);

varargout{1} = kData;

bSlashInds = strfind(fid,'\');
outputDir = fid(1:bSlashInds(end)-1);
save(fullfile(outputDir,['kMeansData_' num2str(nClusters)]),'kData','-v7.3')
disp(['Saved data at ' outputDir ])
end

function kData = RemoveWeakSignalsFromClusters(kData)
% Go through each cluster and eliminate cells that show weak responses relative to that cluster's mean
disp('Removing cells with weak activity (w.r.t cluster mean) from each cluster')
nClusters = numel(unique(kData.idx));
mu = zeros(nClusters,1);
sigSig = mu;
cutoffThr = mu;
for clstr = 1:nClusters
    cellsInCluster = find(kData.idx == clstr);
    sig = std(kData.data(cellsInCluster,:),[],2);
%     figure
    [counts, vals] = hist(sig,50);
    mu(clstr) = sum((vals(:).*counts(:)))/sum(counts);
    sigSig(clstr) = std(sig);
    cutoffThr(clstr) = mu(clstr) -1*sigSig(clstr);
%     plot(vals,counts,'k')
%     hold on
%     stem(mu(clstr),max(counts(:)),'b','marker','none')
%     stem(cutoffThr(clstr),max(counts(:)),'r','marker','none')
    removeInds = cellsInCluster(sig<=cutoffThr(clstr));    
    kData.idx(removeInds,:)= nan;
    kData.data(removeInds,:) = nan;
    kData.D(removeInds,:)= nan;
    nAfter = sum(kData.idx == clstr);
    nBefore = nAfter + numel(removeInds);
    
    disp(['Cluster ' num2str(clstr) ': ' num2str(nBefore - nAfter)...
        ' (' num2str((1-(nAfter/nBefore))*100) '%) cells removed'])
end

end
