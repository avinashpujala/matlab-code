
% Must load pyData into matlab workspace first

%% Inputs
% --- Location of image stack with mask
maskDir = 'S:\Avinash\SPIM\Alx\9-12-2015-AlxRG-relaxed-dpf4\Fish1\preExptStack_lowRes_20150912_164446\TM00000';

%--- Location of directory with registered images
imgDir  = 'S:\Avinash\SPIM\Alx\9-12-2015-AlxRG-relaxed-dpf4\Fish1\g50-t1-ipi90_20150912_170410';

thresh_mult = 0.85; % This determines the threshold for the mask. Lower threshold results in detection of 
                   % more cells within the mask

%% Readimg average image stack
fName = GetFilenames(imgDir,'searchStr','ave','ext','tif');
% imgStack_avg = ReadImgStack(fullfile(imgDir,fName{1}),'tif');
imgStack_avg = ReadImgStack(fullfile(imgDir,fName{1}));
imgStack_avg = permute(imgStack_avg,[2 1 3]);

%% Read cell data
if ~exist('cells')
    cellRespFile = [imgDir '\cell_resp3.mat'];
    cellInfoFile = fullfile(imgDir,'cell_info_processed.mat');
    disp('Loading cell response matrix...')
    load(cellRespFile);
    load(cellInfoFile);
    cells.raw  = cell_resp3;
    cells.info = cell_info;
end


%% Getting image stack for registration
fName = GetFilenames(maskDir,'ext','tif');
corrVec = nan(size(fName));
img = cell(size(fName));
for jj = 1:length(fName)
    disp(fName{jj})
    img{jj} = ReadImgStack(fullfile(maskDir,fName{jj}));
    m = median(1:size(img{jj},3));
    m = (ceil(m(1)));
    corrVec(jj) = corr2(img{jj}(:,:,m),imgStack_avg(:,:,m));
end

[~, idx_ch] = max(corrVec);
imgStack_mask = img{idx_ch};

disp('Registering...')
tic
xy = RegisterImageStacks(imgStack_avg,imgStack_mask);
toc

%% Plot images to check
imgIdx= m;
% imgIdx = 15;
figure('Name','Unregistered')
imshowpair(imgStack_avg(:,:,imgIdx),imgStack_mask(:,:,imgIdx),'scaling','independent')
title('Unregistered')
axis off
drawnow

figure('Name','Registered')
imshowpair(imgStack_avg(:,:,imgIdx),circshift(imgStack_mask(:,:,imgIdx),-[xy(1,imgIdx), xy(2,imgIdx)]),'scaling','independent')
title('Registered')
axis off
drawnow


%% Applying registration to image mask
idx_mask = setdiff(1:2,idx_ch);
imgStack_mask = img{idx_mask};

disp('Translating and median filtering...')
for zz = 1:size(xy,2)
    disp(['Slice # ' num2str(zz)])
    imgStack_mask(:,:,zz) = medfilt2(circshift(imgStack_mask(:,:,zz),...
        -[xy(1,zz) xy(2,zz)]),[10 10]);
end
thr = GetMultiThr(imgStack_mask(:),1);
% b = 3*(length(thr):-1:1); b = b/sum(b);
% thr = sum(thr(:).*b(:));
thr_new = thr * thresh_mult;

%% Show thresholded mask image for median slice
imgIdx = m;
im = imgStack_mask(:,:,imgIdx);
figure('Name','Registered with mask')
imshowpair(imgStack_avg(:,:,imgIdx),im,'scaling','independent')
axis off
title('Registered with mask')
drawnow

% mask(im>=thr) = 1 ;
im_new = im;
im_new(im < thr_new)=0;

figure
% imshowpair(imgStack_mask(:,:,imgIdx),mask,'scaling','independent');
 imshowpair(imgStack_mask(:,:,imgIdx),im_new)
axis off

%% Get cells within mask
mask = zeros(size(imgStack_mask));
mask(imgStack_mask >= thr_new) = 1;

z = [cells.info(:).slice];
xy_ = [cells.info(:).center];
x = xy_(1:2:end); y = xy_(2:2:end);
inds = 1:length(z);
ixyz = [inds(:) y(:) x(:) z(:)];

disp('Finding cells in mask...')
cellBool = zeros(length(ixyz),1);
for zz = 1:size(mask,3)
disp(['Slice # ' num2str(zz)])
blah = ixyz(find(ixyz(:,end)==zz),:);
img_mask = mask(:,:,zz);
tempBool = SparseIndex(img_mask,blah(:,2),blah(:,3));
cellBool(blah(find(tempBool),1))=1;
end
cells.inds.inMask = find(cellBool);

%% Mapping cells in mask onto avg stack
if ~exist('stack')
    stack = struct;
    try
        stack.avg  = double(readtiff(fullfile(imgDir,'ave.tif')));
    catch
        info = imfinfo(fullfile(imgDir,'ave.tif'));
        stack.avg = zeros(info(1).Height,info(1).Width, size(info,1));
        for z = 1:size(info,1)
            stack.avg(:,:,z) = imread(fullfile(imgDir,'ave.tif'),z);
        end
    end
end

cells.avg = stack.avg;
mappedStack = MapCellIndsToAveStack(cells.inds.inMask,cells.info,stack.avg);

% mappedStack = MapCellIndsToAveStack(cells.inds.inMask,cells.info,...
%     permute(imgStack_mask,[2 1 3]));

%% Save cell data for clustering and regression in iPython
fName_cellData = ['Cell data for clustering_' datestr(now,30) '_.mat'];
saveDir = 'S:\Avinash\SPIM\Alx\9-12-2015-AlxRG-relaxed-dpf4\Fish1\g50-t1-ipi90_20150912_170410\pyData';
save(fullfile(saveDir,fName_cellData),'cells','-v7.3');

%% Also append cell data to pyData
pyData.Properties.Writable = true;
pyData.cells = cells;
