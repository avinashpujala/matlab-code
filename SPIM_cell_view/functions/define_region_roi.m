function cell_inds=define_region_roi(celllabels, cell_ave)

dim=size(cell_ave);
cmask=zeros(dim);
hf=figure;set(hf,'Position',[100 0 600 1000]); 
ha=axes;
for z=1:dim(3)
    
    
    imagesc(cell_ave(:,:,z));
    title(num2str(z));
    [x y button]=ginput(1);
    if button ~=1
        continue;
    else
        h=imfreehand(ha);
        cmask(:,:,z)=cmask(:,:,z)+createMask(h);  
        [x y button]=ginput(1);
        if button ~=1
            continue;
        else            
            delete(h);
            h=imfreehand(ha);
            cmask(:,:,z)=cmask(:,:,z)+createMask(h);  

        end    
        imagesc(cmask(:,:,z));
        drawnow;
        pause;
        
    end    
    
end

celllabel2=celllabels;
celllabel2(~cmask)=0;

CC=bwconncomp(celllabel2);
cell_inds=zeros(CC.NumObjects,1);

for j=1:length(cell_inds);
    cell_inds(j)=round(max(celllabels(CC.PixelIdxList{j})));
end
    