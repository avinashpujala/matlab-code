clear all;close all;

input_dirs=struct;

input_dirs(1).dir='Z:\SPIM\Avinash\Raw\Aug 2015\8-9-2015_Alx RG x 939_4dpf\Fish1\G50_T1_IPI60_20150808_171117\reg\Registered_tStacks';


trial_tlen=120;
corr_thre=0.7;
move_thre=1;
poolnumber=10;

%%
for dd=1:length(input_dirs)
    
    input_dir=input_dirs(dd).dir;

    load(fullfile(input_dir,'cell_info.mat'));
    ave_stack=readtiff(fullfile(input_dir,'ave.tif'));
    background = imread(fullfile(input_dir,'background_0.tif'));  

    numcell=length(cell_info);
    dim=size(ave_stack);

    cell_zlist=[cell_info.slice];

    zplane_list=min(cell_zlist):max(cell_zlist);


%    outputName = [input_dir, '/Plane' num2str(1, '%.2d') '.stack'];
    outputName = [input_dir, '/Plane' num2str(1, '%.2d') '.bin'];
    dim2=read_LSstack_info(outputName,dim(1:2));
    totlen=dim2(3);

    zplane_inds=struct;
    for i=zplane_list
        zplane_inds(i).cellinds=find(cell_zlist==i);
    end

    disp('Getting raw time course and applying low cut filter...');
    cell_resp=zeros(numcell,totlen,'single');

    cell_resp1_cells=cell(1,length(zplane_list));

    matlabpool(poolnumber);

    parfor zplane=zplane_list

        cellinds=zplane_inds(zplane).cellinds;  
%         outputName = [input_dir, '/Plane' num2str(zplane, '%.2d') '.stack'];
        outputName = [input_dir, '/Plane' num2str(zplane, '%.2d') '.bin'];
        stack=read_LSstack_fast1(outputName,dim);
        disp(['Getting time course of plane',num2str(zplane)]);

        cellresp1=zeros(length(cellinds),totlen);
        cinfo=cell_info(cellinds);
        sliceinds=int64(0:(totlen-1))*int64(dim(1)*dim(2));

        for i=1:length(cellinds)    
            inds=cinfo(i).inds;
            tcourse=single(get_cell_tcourse_mex64(stack,sliceinds,int64(inds)));
            cellresp1(i,:)=tcourse;
        end

        cell_resp1_cell{zplane}=cellresp1;

    end

    matlabpool close;

    for i=zplane_list
        cellinds=zplane_inds(i).cellinds;  
        cell_resp(cellinds,:)=cell_resp1_cell{i};
    end

    cell_resp_dim=size(cell_resp);
    write_LSstack_fast_float(fullfile(input_dir,'cell_resp.stackf'),cell_resp);
    save(fullfile(input_dir,'cell_resp_dim'),'cell_resp_dim');
    
    %% low-cut timecourse of cells
    tmp=sort(double(background(:)),'ascend');
    back_value=mean(tmp(1:round(length(tmp)/20)));
    
    totcell=size(cell_resp,1);
    totaltime=size(cell_resp,2);
    nrep=floor(totaltime/trial_tlen);
    baselines=zeros(totcell,nrep);


    bottomlen=round(trial_tlen/3);

    for j=1:nrep
        tmp=cell_resp(:,(j-1)*trial_tlen+(1:trial_tlen));    
        tmp=sort(tmp,2);     
        baselines(:,j)=mean(tmp(:,1:bottomlen), 2)-back_value;
    end


    fitbaseline=log(baselines./repmat(mean(baselines(:,1:2),2),[1 nrep]));
    xs=round(trial_tlen/2)+(0:trial_tlen:((nrep-1)*trial_tlen));


    baseline2=zeros(totcell,totaltime);

    cs=zeros(totcell,1);

    for i=1:totcell
        cf=polyfit(xs,fitbaseline(i,:),1);
        cs(i)=cf(1);
        baseline2(i,:)=exp(cf(1)*(1:totaltime))*mean(baselines(i,1:2));
    end

    cell_resp2=(cell_resp-back_value)./baseline2;
    cell_resp = cell_resp-back_value; % Added this line on 1-25-2015 to remove offset introduced by background
    

    cell_resp_dim=size(cell_resp2);
    write_LSstack_fast_float(fullfile(input_dir,'cell_resp_lowcut.stackf'),cell_resp2);
    save(fullfile(input_dir,'cell_resp_dim_lowcut'),'cell_resp_dim');


    %% removing double_counted cells
   
    totrepeats=floor(cell_resp_dim(2)/ trial_tlen);
    cell_resp2=cell_resp2(:,1:totrepeats*trial_tlen);
    %cell_resp3=cell_resp2(:,1:20*trial_tlen);


    disp('Removing multiple-counted cells...');
    %cell_resp2_ncorr=cell_resp3-repmat(squeeze(mean(reshape(cell_resp3,[cell_resp_dim(1) trial_tlen 20]),3)),[1 20]);
    cell_resp2_ncorr=cell_resp2-repmat(squeeze(mean(reshape(cell_resp2,[cell_resp_dim(1) trial_tlen totrepeats]),3)),[1 totrepeats]);

    [r,c]=find(ones(5,5));
    r=r-3;c=c-3;
    moveinds=c*dim(1)+r;

    cell_remove=zeros(1,length(cell_info));
    for z=1:dim(3)-1

        zplane1=zeros(dim(1),dim(2));
        zplane2=zeros(dim(1),dim(2));

        cinds1=find([cell_info.slice]==z  & cell_remove==0);
        cinds2=find([cell_info.slice]==z+1 & cell_remove==0);

        for i=1:length(cinds1)
            cellcenter=dim(1)*(cell_info(cinds1(i)).center(2)-1)+cell_info(cinds1(i)).center(1);
            mask=find(cellcenter+moveinds>0 & cellcenter+moveinds<dim(1)*dim(2));
            zplane1(cellcenter+moveinds(mask))=cinds1(i);
        end

        for i=1:length(cinds2)
            cellcenter=dim(1)*(cell_info(cinds2(i)).center(2)-1)+cell_info(cinds2(i)).center(1);
            mask=find(cellcenter+moveinds>0 & cellcenter+moveinds<dim(1)*dim(2));
            zplane2(cellcenter+moveinds(mask))=cinds2(i);
        end 

        zplane3=(zplane1>0).*(zplane2>0);

        CC=bwconncomp(zplane3);
        rlist=zeros(1,CC.NumObjects);
        clist=zeros(1,CC.NumObjects);

        for i=1:CC.NumObjects
            tmp=CC.PixelIdxList{i};
            cnum1=max(zplane1(tmp));
            cnum2=max(zplane2(tmp));
            clist(i)=cnum2;

            r=corrcoef_pair_mex(double(cell_resp2_ncorr(cnum1,:)),double(cell_resp2_ncorr(cnum2,:)));
            rlist(i)=r;

        end

        removelist=find(rlist>corr_thre);
        cell_remove(clist(removelist))=1;
    end
    keep_inds=find(cell_remove==0);
    remove_inds=find(cell_remove>0);
    disp([num2str(length(remove_inds)), ' cells removed by multi-counting'])

    cell_info=cell_info(keep_inds);
    cell_resp2=cell_resp2(keep_inds,:);
    cell_resp3 = cell_resp(keep_inds,:);
    
    % Removing multiply counted cells and keeping only half the trace (TK duplicates the trace for some reason!)   


    cell_resp_dim=size(cell_resp2);
    write_LSstack_fast_float(fullfile(input_dir,'cell_resp_processed.stackf'),cell_resp2);
    
    disp('Saving relevant files and variables...')
    save(fullfile(input_dir,'cell_resp_dim_processed'),'cell_resp_dim');
    save(fullfile(input_dir,'cell_info_processed.mat'),'cell_info');
    save(fullfile(input_dir,'cell_resp3.mat'),'cell_resp3','-v7.3'); % Apparently this version is reqd for saving very large files.
%     save('C:\Users\pujalaa\Documents\Data','cell_resp3');
%     savefast(fullfile(input_dir,'cell_resp3.mat','cell_resp3'));
    disp('Done!')
end






