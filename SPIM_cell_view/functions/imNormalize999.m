function out=imNormalize999(im)

im=double(im);
temp=sort(im(:),'descend');
th1=temp(round(length(im(:))/1000));
th2=min(im(:));

out=(im-th2)/(th1-th2);
out(out>1)=1;


