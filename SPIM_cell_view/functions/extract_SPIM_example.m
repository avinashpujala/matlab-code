clear all;close all;


cd 'Y:\Yu\20140312\spon2_20140312_213304_erik\Registered';
input_dir='Y:\Yu\20140312\spon2_20140312_213304_erik';
%skiplist=[1:3]; %% skip first 1-5 trial because of the bleaching
tlen=150; %% frame cycle of the trial
timelist=[60 60  5  5 5 5 100]; %% set epochs for making average
% time_explain = [1highgain 2pause 3backprobe 4pause 5lowgain 6pause 7backprobe 8pause 
%                 9highgain 10pause 11forprobe 12pause 13lowgain 14pause 15forprobe 16pause];


ave_stack  = double(readtiff(fullfile(input_dir,'\Registered\ave.tif')));  %% average anatomy stack of SPIM

% load(fullfile(input_dir,'\Registered\cell_resp_dim_processed.mat'));
% load(fullfile(input_dir,'\Registered\cell_info_processed.mat'));
% cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);

load(fullfile(input_dir,'\Registered\cell_info.mat'));
load(fullfile(input_dir,'\Registered\cell_resp_dim_lowcut.mat'));
cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);
load(fullfile(input_dir,'\swims\frame_swim.mat'));

skiplist=[]; %% you can omit specific trials from average



numcell=length(cell_info);
dim=size(ave_stack);
totlen=cell_resp_dim(2);
nrep=floor(totlen/tlen);

includeinds=setdiff(1:nrep, skiplist);
include_tcourse=zeros(tlen*length(includeinds),1);

for i=1:length(includeinds)
    include_tcourse(tlen*(i-1)+1:tlen*i)=tlen*(includeinds(i)-1)+1:tlen*(includeinds(i));
end

cell_resp=cell_resp(:,1:nrep*tlen);

cell_resp_ave=mean(reshape(cell_resp(:,include_tcourse),[numcell tlen length(includeinds)]),3);
1
%% extracting active neurons in "timelist'

cell_resp_period_mean=zeros(length(cell_info),length(timelist));
timelist2=cumsum(timelist);
timelist2(2:end+1)=timelist2(1:end);
timelist2(1)=0;

for i=1:length(timelist)
    cell_resp_period_mean(:,i)=mean(cell_resp_ave(:,timelist2(i)+1:timelist2(i+1)),2);
end
2
%%
corr_inds=corrcoef_multipair_mex(frame_swim(:,5),double(cell_resp')); 

cells1=find(cell_resp_period_mean(:,5) - cell_resp_period_mean(:,4) > 0.03);  %% red cells
cells2=find(cell_resp_period_mean(:,5)- cell_resp_period_mean(:,2) > 0.05);  %% greeen cells

%cells1=find(cell_resp_period_mean(:,2) - cell_resp_period_mean(:,5) > 0.03);  %% red cells
%cells2=find(cell_resp_period_mean(:,5)- cell_resp_period_mean(:,2) > 0.05);  %% greeen cells
session1 = 3;
session2 = 4;
session1_base = 2;
session2_base = 2;
thld_highlow = 0.03;
thld_lowhigh = 0.03;

responsiveness = 0;
comparison = 1 - responsiveness;

if responsiveness
    cells1=find(cell_resp_period_mean(:,session1) -cell_resp_period_mean(:,session1_base)> thld_highlow);%&(cell_resp_period_mean(:,11)- cell_resp_period_mean(:,10) > 0.04));  %% red cells
    cells2=find(cell_resp_period_mean(:,session2)-cell_resp_period_mean(:,session2_base)> thld_lowhigh);  %% greeen cells
end 

if comparison
    cells1=find((cell_resp_period_mean(:,session1) - cell_resp_period_mean(:,session2) > thld_highlow));%&(cell_resp_period_mean(:,11)- cell_resp_period_mean(:,10) > 0.04));  %% red cells
    cells2=find(cell_resp_period_mean(:,session2)- cell_resp_period_mean(:,session1) > thld_lowhigh);  %% greeen cells
end

anatomy_image_yx = repmat(imNormalize99(max(ave_stack,[],3)),[1 1 3]);
anatomy_image_yz = repmat(imNormalize99(squeeze(max(ave_stack,[],2))),[1 1 3]);
anatomy_image_yx_ori = anatomy_image_yx;
anatomy_image_yz_ori = anatomy_image_yz;
dimv_yx = size(anatomy_image_yx);
dimv_yz = size(anatomy_image_yz);


anatomy_image_yz2=zeros(dimv_yz(1),dimv_yz(2)*10,3);
anatomy_image_yz2_ori=anatomy_image_yz2;
tot_image=zeros(dimv_yx(1),dimv_yx(2)+dimv_yz(2)*10+10,3);
tot_image(:,dimv_yx(2)+(1:10),:)=1;

circle=makeDisk2(7,15);
[r, v]=find(circle);
r=r-8;v=v-8;
circle_inds  = r*dimv_yx(1)+v;
yzplane_inds = -5:5;


disp(2);
for j=1:length(cells1)    
    cinds=(cell_info(cells1(j)).center(2)-1)*dimv_yx(1)+cell_info(cells1(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=1;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=0;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0;

    cinds=(cell_info(cells1(j)).slice-1)*dimv_yz(1)+cell_info(cells1(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=1;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=0;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0;
end

for j=1:length(cells2)    
    cinds=(cell_info(cells2(j)).center(2)-1)*dimv_yx(1)+cell_info(cells2(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=0;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=1;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.75;    

    cinds=(cell_info(cells2(j)).slice-1)*dimv_yz(1)+cell_info(cells2(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=1;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.75;
end



for k=1:3
    anatomy_image_yz2(:,:,1)=imresize(anatomy_image_yz(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,2)=imresize(anatomy_image_yz(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,3)=imresize(anatomy_image_yz(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,1)=imresize(anatomy_image_yz_ori(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,2)=imresize(anatomy_image_yz_ori(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2_ori(:,:,3)=imresize(anatomy_image_yz_ori(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
end


tot_image(:,1:dimv_yx(2),:)=anatomy_image_yx;
tot_image(:,dimv_yx(2)+11:end,:)=anatomy_image_yz2;


tot_image(tot_image(:)>1)=1;
tot_image(tot_image(:)<0)=0;
map = figure('position',[50 50 1000 1000]);
image(tot_image);colormap gray;
figtitle = 'probe: high VS. low (threshold = 0.04)';
title(figtitle);
filename = ['probe_HvsL'] ;
saveas(map,fullfile(filename),'fig');
3
return;


%%  write z-stack of extracted cells
if 1
    
    target_inds=cells2;
    ave_stack2=zeros(size(ave_stack,1), size(ave_stack,2)*2, size(ave_stack,3) ,3);
    zlen=size(ave_stack,3);
    dimv_yxz=size(ave_stack);
    stacklen=numel(ave_stack);

    circle=makeDisk2(10,21);
    [r, v]=find(circle);
    r=r-11;v=v-11;
    circle_inds  = r*dimv_yxz(1)+v;
    yzplane_inds = -1:1;


    for i=1:zlen
        ave_stack2(:,:,i,:)=repmat(imNormalize99(ave_stack(:,:,i)),[1 2 1 3]);
    end

    for j=1:length(target_inds)    
        cinds=(cell_info(target_inds(j)).center(2)-1)*dimv_yxz(1)+cell_info(target_inds(j)).center(1);
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yxz(1)*dimv_yxz(2)); 
        zinds=dimv_yxz(1)*dimv_yxz(2)*2*(cell_info(target_inds(j)).slice-1);
        ave_stack2(cinds+circle_inds(labelinds)+zinds)=1;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*2)=0.3;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*4)=0;
    end

    writetiff8(ave_stack2,fullfile(input_dir,'Registered\nega_cells.tif'));
end

return;

%%

cellinds_ex=define_region_roi_new( cell_info, cells1, ave_stack);

%%
figure('position',[50 50 2300 1200]);
% row_num = ceil(size(cellinds_erx)
q = 1;
for j=169:length(cellinds_ex);
    cnum=j;
    cnum2 = cellinds_ex(j);
        subplot(7,6,q);plot(cell_resp_ave(cellinds_ex(j),:),'r','linewidth',2);
    tname = [num2str(cnum),'-',num2str(cnum2)];
    title(tname);
    ylim([0.95 1.35]);
    xlim([0 300]);
    line([201 201],[0.95 1.3]) 
    line([206 206],[0.95 1.3]) 
    q = q+1;
%     line([280 280],[0.95 1.3]) 
%     line([380 380],[0.95 1.3]) 
   % line([104 104],[0.95 1.3]) 
end
%%
 figure('position',[250 250 1800 700]);;plot(cell_resp(70766,:));hold on;plot(frame_num(1:200:end),rawdata.stimParam3(1:200:end)/2+1,'r')

%%
extracted_cellinds=cellinds_ex;
inds_ori=cellinds_ex;
    
save(fullfile(input_dir,'\Registered\extracted_cellinds_forbrain_LvsH.mat'),'extracted_cellinds');

%%
dim_x = size(cell_resp,2)/400;
IM = reshape(cell_resp(extracted_cellinds(14),:), 400 ,dim_x)';
figure('position',[50 50 1000 800]);
imagesc(IM);
title('cell-14')






