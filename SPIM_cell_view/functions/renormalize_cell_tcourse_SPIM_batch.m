clear all;close all;

input_dirs=struct;

input_dirs(1).dir='Z:\Takashi\06032014\Fish4-2\Registered';
input_dirs(end+1).dir='Z:\Takashi\06122014\Fish1-3\Registered';
input_dirs(end+1).dir='Z:\Takashi\06122014\Fish2-2\Registered';
input_dirs(end+1).dir='Z:\Takashi\06122014\Fish3-2\Registered';
%%

trial_tlen=160;
corr_thre=0.7;
move_thre=1;

%%
for dd=1:length(input_dirs)
    
    input_dir=input_dirs(dd).dir;

    load(fullfile(input_dir,'cell_resp_dim.mat'));
    load(fullfile(input_dir,'cell_info.mat'));
    load(fullfile(input_dir,'motion_param.mat'));
    ave_stack=readtiff(fullfile(input_dir,'ave.tif'));
    cell_resp=read_LSstack_fast_float(fullfile(input_dir,'cell_resp.stackf'),cell_resp_dim);
    background = imread(fullfile(input_dir,'background_0.tif'));  %% average anatomy stack of SPIM
    
    dim=size(ave_stack);
    
    
    tmp=sort(double(background(:)),'ascend');
    back_value=mean(tmp(1:round(length(tmp)/20)));
    
    totcell=size(cell_resp,1);
    totaltime=size(cell_resp,2);
    nrep=floor(totaltime/trial_tlen);

    %% low-cut timecourse of cells

    baselines=zeros(totcell,nrep);


    bottomlen=round(trial_tlen/3);

    for j=1:nrep
        tmp=cell_resp(:,(j-1)*trial_tlen+(1:trial_tlen));    
        tmp=sort(tmp,2);     
        baselines(:,j)=mean(tmp(:,1:bottomlen), 2)-back_value;
    end


    fitbaseline=log(baselines./repmat(mean(baselines(:,1:2),2),[1 nrep]));
    xs=round(trial_tlen/2)+(0:trial_tlen:((nrep-1)*trial_tlen));


    baseline2=zeros(totcell,totaltime);

    cs=zeros(totcell,1);

    for i=1:totcell
        cf=polyfit(xs,fitbaseline(i,:),1);
        cs(i)=cf(1);
        baseline2(i,:)=exp(cf(1)*(1:totaltime))*mean(baselines(i,1:2));
    end

    cell_resp2=(cell_resp-back_value)./baseline2;
    
    
    cell_resp_dim=size(cell_resp2);
    write_LSstack_fast_float(fullfile(input_dir,'cell_resp_lowcut.stackf'),cell_resp2);
    save(fullfile(input_dir,'cell_resp_dim_lowcut'),'cell_resp_dim');

    %% removing double_counted cells

    totrepeats=floor(cell_resp_dim(2)/ trial_tlen);
    cell_resp2=cell_resp2(:,1:totrepeats*trial_tlen);
    %cell_resp3=cell_resp2(:,1:20*trial_tlen);


    disp('Removing multiple-counted cells...');
    %cell_resp2_ncorr=cell_resp3-repmat(squeeze(mean(reshape(cell_resp3,[cell_resp_dim(1) trial_tlen 20]),3)),[1 20]);
    cell_resp2_ncorr=cell_resp2-repmat(squeeze(mean(reshape(cell_resp2,[cell_resp_dim(1) trial_tlen totrepeats]),3)),[1 totrepeats]);

    [r,c]=find(ones(5,5));
    r=r-3;c=c-3;
    moveinds=c*dim(1)+r;

    cell_remove=zeros(1,length(cell_info));
    for z=1:dim(3)-1

        zplane1=zeros(dim(1),dim(2));
        zplane2=zeros(dim(1),dim(2));

        cinds1=find([cell_info.slice]==z  & cell_remove==0);
        cinds2=find([cell_info.slice]==z+1 & cell_remove==0);

        for i=1:length(cinds1)
            cellcenter=dim(1)*(cell_info(cinds1(i)).center(2)-1)+cell_info(cinds1(i)).center(1);
            mask=find(cellcenter+moveinds>0 & cellcenter+moveinds<dim(1)*dim(2));
            zplane1(cellcenter+moveinds(mask))=cinds1(i);
        end

        for i=1:length(cinds2)
            cellcenter=dim(1)*(cell_info(cinds2(i)).center(2)-1)+cell_info(cinds2(i)).center(1);
            mask=find(cellcenter+moveinds>0 & cellcenter+moveinds<dim(1)*dim(2));
            zplane2(cellcenter+moveinds(mask))=cinds2(i);
        end 

        zplane3=(zplane1>0).*(zplane2>0);

        CC=bwconncomp(zplane3);
        rlist=zeros(1,CC.NumObjects);
        clist=zeros(1,CC.NumObjects);

        for i=1:CC.NumObjects
            tmp=CC.PixelIdxList{i};
            cnum1=max(zplane1(tmp));
            cnum2=max(zplane2(tmp));
            clist(i)=cnum2;

            r=corrcoef_pair_mex(double(cell_resp2_ncorr(cnum1,:)),double(cell_resp2_ncorr(cnum2,:)));
            rlist(i)=r;

        end

        removelist=find(rlist>corr_thre);
        cell_remove(clist(removelist))=1;
    end
    keep_inds=find(cell_remove==0);
    remove_inds=find(cell_remove>0);
    disp([num2str(length(remove_inds)), ' cells removed by multi-counting'])

    cell_info=cell_info(keep_inds);
    cell_resp2=cell_resp2(keep_inds,:);


    %%  calculating motion confidence

    disp('Removing deformed cells....');
    dists=zeros(1,length(cell_info));

    for c=1:length(cell_info)

        z=cell_info(c).slice;
        yx=cell_info(c).center;

        grid_y=mod(motion_param(z).indslist,dim(1));grid_y(grid_y==0)=dim(1);
        grid_x=ceil(motion_param(z).indslist/dim(1));

        distance=sqrt((grid_y-yx(1)).^2 + (grid_x-yx(2)).^2);
        [V,inds]=sort(distance,'ascend');

        cell_info(c).motion=mean(motion_param(z).tilt_med(inds(1:min([5,length(inds)])),:),1);  

    end

    motion_matrix=[cell_info.motion];
    motion_y=motion_matrix(1:3:end);
    motion_x=motion_matrix(2:3:end);
    motion_z=motion_matrix(3:3:end);
    remove_inds=find(abs(motion_y)>move_thre | abs(motion_x)>move_thre | abs(motion_z)>move_thre);
    disp([num2str(length(remove_inds)), ' cells removed by motion_assessment'])

    keep_inds=setdiff(1:length(cell_info),remove_inds);

    cell_info=cell_info(keep_inds);
    cell_resp2=cell_resp2(keep_inds,:);


    cell_resp_dim=size(cell_resp2);
    write_LSstack_fast_float(fullfile(input_dir,'cell_resp_processed.stackf'),cell_resp2);
    save(fullfile(input_dir,'cell_resp_dim_processed'),'cell_resp_dim');
    save(fullfile(input_dir,'cell_info_processed.mat'),'cell_info');
end






