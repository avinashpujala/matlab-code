clear all;close all;

% PathName='Z:\YuMu\SPIM_behavior\20140317';
% FileName='\20140317_1_1_spim_cy73_6dpf_spon.10chFlt';
cd 'Z:\Misha\26mar14\gain_bar_5_20140326_201435\Registered';
input_dir='Z:\Misha\26mar14\gain_bar_5_20140326_201435';
% cd 'Y:\Yu\20140308\spon_20140308_184156\Registered';
% input_dir='Y:\Yu\20140308\spon_20140308_184156';
%skiplist=[1:3]; %% skip first 1-5 trial because of the bleaching
tlen=150; %% frame cycle of the trial
timelist=[50 50  5 15 30]; %% set epochs for making average
% time_explain = [1highgain 2pause 3backprobe 4pause 5lowgain 6pause 7backprobe 8pause 
%                 9highgain 10pause 11forprobe 12pause 13lowgain 14pause 15forprobe 16pause];

%%
ave_stack  = double(readtiff(fullfile(input_dir,'\Registered\ave.tif')));  %% average anatomy stack of SPIM
load(fullfile(input_dir,'\Registered\cell_resp_dim_lowcut.mat'));
% load(fullfile(input_dir,'\Registered\cell_resp_dim.mat'));
load(fullfile(input_dir,'\Registered\cell_info.mat'));
cell_resp_dim
cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);
% cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp.stackf'),cell_resp_dim);
load(fullfile(input_dir,'\swims\frame_swim.mat'));

% load(fullfile(input_dir,'\swims\L_HBO.mat'));
% load(fullfile(input_dir,'\swims\R_HBO.mat'));

%{
    frame_turn(:,1)     %binary: left turns
    frame_turn(:,2)     %binary: right turns
    frame_turn(:,3)     %binary: all turns (+1 for left -1 for right)
    frame_turn(:,4)     %binary: forward swims
    frame_turn(:,5)     %binary: all swims
    
    frame_turn(:,6)     %weighted: all turns
    frame_turn(:,7)     %weighted: left turns
    frame_turn(:,8)     %weighted: right turns
    frame_turn(:,9)     %weighted: forward swims
    frame_turn(:,10)    %weighted: all swims
    frame_turn(:,11)    %weighted: left channel
    frame_turn(:,12)    %weighted: right channel   
    
    frame_turn(:,13)    %analog: left channel   
    frame_turn(:,14)    %analog: right channel   
    frame_turn(:,15)    %analog: turns (left - right)  
    frame_turn(:,16)    %analog: swims (left + right)  
%}
skiplist=[]; %% you can omit specific trials from average




numcell=length(cell_info);
dim=size(ave_stack);
totlen=cell_resp_dim(2);
nrep=floor(totlen/tlen);

includeinds=setdiff(1:nrep, skiplist);
include_tcourse=zeros(tlen*length(includeinds),1);

for i=1:length(includeinds)
    include_tcourse(tlen*(i-1)+1:tlen*i)=tlen*(includeinds(i)-1)+1:tlen*(includeinds(i));
end

cell_resp=cell_resp(:,1:nrep*tlen);

cell_resp_ave=mean(reshape(cell_resp(:,include_tcourse),[numcell tlen length(includeinds)]),3);
1
%% extracting active neurons in "timelist'

cell_resp_period_mean=zeros(length(cell_info),length(timelist));
timelist2=cumsum(timelist);
timelist2(2:end+1)=timelist2(1:end);
timelist2(1)=0;

for i=1:length(timelist)
    cell_resp_period_mean(:,i)=mean(cell_resp_ave(:,timelist2(i)+1:timelist2(i+1)),2);
end
2

%%
im_num = size(cell_resp,2);
ephy_num = size(frame_swim,1);


if ephy_num == im_num
    
elseif ephy_num>im_num
    frame_swim = frame_swim(1:im_num,:);
else
    cell_resp = cell_resp(:,1:ephy_num);
end

anatomy_image_yx = repmat(imNormalize99(max(ave_stack,[],3)),[1 1 3]);
anatomy_image_yz = repmat(imNormalize99(squeeze(max(ave_stack,[],2))),[1 1 3]);
anatomy_image_yx_ori = anatomy_image_yx;
anatomy_image_yz_ori = anatomy_image_yz;
dimv_yx = size(anatomy_image_yx);
dimv_yz = size(anatomy_image_yz);


anatomy_image_yz2=zeros(dimv_yz(1),dimv_yz(2)*10,3);
anatomy_image_yz2_ori=anatomy_image_yz2;
tot_image=zeros(dimv_yx(1),dimv_yx(2)+dimv_yz(2)*10+10,3);
tot_image(:,dimv_yx(2)+(1:10),:)=1;

circle=makeDisk2(7,15);
[r, v]=find(circle);
r=r-8;v=v-8;
circle_inds  = r*dimv_yx(1)+v;
yzplane_inds = -5:5;

%%

% calcium_span=0:24;
% calcium_offset = 8;
figure;
plot(frame_swim(:,3))
plot(-frame_swim(:,4),'r')
hold on
plot(frame_swim(:,1)/50,'c');
plot(frame_swim(:,2)/20,'g');
hold off


replay_givingUp = [1600:2161,3600:4161];
replay_fighting = [1401:1561,3397:3561];
replay_pretending = [5400:6161,7400:8161];
closed_baseline = [600:1399,3000:3380,4800:5399,6787:7399];
closed_recovering = [4205:4325,2223:2513];   % hard to know whether it's recovering or still giving up, may refer to the bar moving





if 1

ca_givingUp = mean(cell_resp(:,replay_givingUp),2);
ca_baseline = mean(cell_resp(:,closed_baseline),2);
ca_fighting = mean(cell_resp(:,replay_fighting),2);
ca_recovering = mean(cell_resp(:,closed_recovering),2);
ca_pretending = mean(cell_resp(:,replay_pretending),2);

ca_givingUp = ca_givingUp - prctile(ca_givingUp,10);
ca_baseline = ca_baseline - prctile(ca_baseline,10);
ca_fighting = ca_fighting - prctile(ca_fighting,10);
ca_recovering = ca_recovering - prctile(ca_recovering,10);
ca_pretending = ca_pretending - prctile(ca_pretending,10);


cells1=find((ca_givingUp - ca_baseline)>.1);% & [cell_info.slice]'>2 & [cell_info.slice]'~=6 & [cell_info.slice]'~=11);
cells2=find((ca_givingUp - ca_pretending)>.1);% & [cell_info.slice]'>2 & [cell_info.slice]'~=6 & [cell_info.slice]'~=11);
cells3=find((ca_givingUp - ca_fighting)>.1);% & [cell_info.slice]'>2 & [cell_info.slice]'~=6 & [cell_info.slice]'~=11);
cells4=find((ca_recovering - ca_givingUp)>.1);% & [cell_info.slice]'>2 & [cell_info.slice]'~=6 & [cell_info.slice]'~=11);





disp(2);
for j=1:length(cells1)    
    cinds=(cell_info(cells1(j)).center(2)-1)*dimv_yx(1)+cell_info(cells1(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=1;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=0.0;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.05;

    cinds=(cell_info(cells1(j)).slice-1)*dimv_yz(1)+cell_info(cells1(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=1;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=0;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.05;
end
for j=1:length(cells2)    
    cinds=(cell_info(cells2(j)).center(2)-1)*dimv_yx(1)+cell_info(cells2(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=0.95;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=0.8;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.05;

    cinds=(cell_info(cells2(j)).slice-1)*dimv_yz(1)+cell_info(cells2(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0.95;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=0.8;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.05;
end
for j=1:length(cells3)
    cinds=(cell_info(cells3(j)).center(2)-1)*dimv_yx(1)+cell_info(cells3(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=0.045;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=.5;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.85; 

    cinds=(cell_info(cells3(j)).slice-1)*dimv_yz(1)+cell_info(cells3(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0.045;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=.5;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.85;
end

for j=1:length(cells4)    
    cinds=(cell_info(cells4(j)).center(2)-1)*dimv_yx(1)+cell_info(cells4(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=0;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=1;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=1;    

    cinds=(cell_info(cells4(j)).slice-1)*dimv_yz(1)+cell_info(cells4(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=1;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=1;
    
end
end
% 
% cells1=find((calcium_left-calcium_right)>.014);
% cells2=find((calcium_right-calcium_left)>.0125);

if 0
% regressor=RR;
regressor=frame_turn(1:size(cell_resp,2),6);
% regressor(regressor>0) = regressor(regressor>0)/sum(regressor(regressor>0))*sum(regressor(regressor<0));
regressor=regressor+ regressor([2:end end]) + regressor([3:end end end])+regressor([4:end end end end]) + regressor([5:end end end end end]) + regressor([6:end end end end end end]) + regressor([7:end end end end end end end]);

%regressor=regressor([1 1 1:end-2]);

% regressor(regressor<0) =regressor(regressor<0)/2;
% regressor = abs(regressor);

% % regressor(regressor>0.5)=0.5;
% corr_inds=corrcoef_multipair_mex(double(regressor'),double(cell_resp')); 
corr_inds=corrcoef_multipair_mex(regressor,double(cell_resp')); 
cells3=find(corr_inds >0.1);%& [cell_info.slice]'>2 & [cell_info.slice]'~=6);%14);  %% blue cells


% regressor=-RR;
%regressor=frame_turn(1:size(cell_resp,2),6);
regressor=-regressor;

% regressor(regressor<0) =regressor(regressor<0)/2;
% corr_inds=corrcoef_multipair_mex(double(regressor'),double(cell_resp')); 
corr_inds=corrcoef_multipair_mex(regressor,double(cell_resp')); 
cells4=find(corr_inds >0.1);% & [cell_info.slice]'>2 & [cell_info.slice]'~=6);%14);  %% cyan cells

for j=1:length(cells3)    
    cinds=(cell_info(cells3(j)).center(2)-1)*dimv_yx(1)+cell_info(cells3(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=0.045;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=.5;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=0.85; 

    cinds=(cell_info(cells3(j)).slice-1)*dimv_yz(1)+cell_info(cells3(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0.045;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=.5;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.85;
end

for j=1:length(cells4)    
    cinds=(cell_info(cells4(j)).center(2)-1)*dimv_yx(1)+cell_info(cells4(j)).center(1);
    labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2)); 
    anatomy_image_yx(cinds+circle_inds(labelinds))=0;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=1;
    anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=1;    

    cinds=(cell_info(cells4(j)).slice-1)*dimv_yz(1)+cell_info(cells4(j)).center(1);
    labelinds=find((cinds+yzplane_inds)>0 & (cinds+yzplane_inds)<=dimv_yz(1)*dimv_yz(2)); 
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=1;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=1;
    
end
end





for k=1:3
    anatomy_image_yz2(:,:,1)=imresize(anatomy_image_yz(:,:,1), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,2)=imresize(anatomy_image_yz(:,:,2), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz2(:,:,3)=imresize(anatomy_image_yz(:,:,3), [dimv_yz(1) dimv_yz(2)*10]);
    anatomy_image_yz(cinds+yzplane_inds(labelinds))=0.2;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2))=.8;
    anatomy_image_yz(cinds+yzplane_inds(labelinds)+dimv_yz(1)*dimv_yz(2)*2)=0.2;
end


tot_image(:,1:dimv_yx(2),:)=anatomy_image_yx;
tot_image(:,dimv_yx(2)+11:end,:)=anatomy_image_yz2;


tot_image(tot_image(:)>1)=1;
tot_image(tot_image(:)<0)=0;
map = figure('position',[50 50 1000 1000]);
image(tot_image);colormap gray;
figtitle = 'probe: high VS. low (threshold = 0.04)';
title(figtitle);
filename = ['high threshold for reggression_HBO on back'] ;
%%
saveas(map,fullfile(filename),'fig');
save(fullfile(input_dir,'/Registered/thld.mat'),'thld');
3
return;


%%  write z-stack of extracted cells
if 1
    
    target_inds=cells2;
    ave_stack2=zeros(size(ave_stack,1), size(ave_stack,2)*2, size(ave_stack,3) ,3);
    zlen=size(ave_stack,3);
    dimv_yxz=size(ave_stack);
    stacklen=numel(ave_stack);

    circle=makeDisk2(10,21);
    [r, v]=find(circle);
    r=r-11;v=v-11;
    circle_inds  = r*dimv_yxz(1)+v;
    yzplane_inds = -1:1;


    for i=1:zlen
        ave_stack2(:,:,i,:)=repmat(imNormalize99(ave_stack(:,:,i)),[1 2 1 3]);
    end

    for j=1:length(target_inds)    
        cinds=(cell_info(target_inds(j)).center(2)-1)*dimv_yxz(1)+cell_info(target_inds(j)).center(1);
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yxz(1)*dimv_yxz(2)); 
        zinds=dimv_yxz(1)*dimv_yxz(2)*2*(cell_info(target_inds(j)).slice-1);
        ave_stack2(cinds+circle_inds(labelinds)+zinds)=1;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*2)=0.3;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*4)=0;
    end

    writetiff8(ave_stack2,fullfile(input_dir,'Registered\nega_cells.tif'));
end

return;

%%

cellinds_ex=define_region_roi_new( cell_info, cells1, ave_stack);

%%
figure('position',[50 50 2300 1200]);
% row_num = ceil(size(cellinds_erx)
q = 1;
for j=1:length(cellinds_ex);
    cnum=j;
    cnum2 = cellinds_ex(j);
        subplot(7,6,q);plot(cell_resp_ave(cellinds_ex(j),:),'r','linewidth',2);
    tname = [num2str(cnum),'-',num2str(cnum2)];
    title(tname);
    ylim([0.95 1.35]);
    xlim([0 300]);
    line([201 201],[0.95 1.3]) 
    line([206 206],[0.95 1.3]) 
    q = q+1;
%     line([280 280],[0.95 1.3]) 
%     line([380 380],[0.95 1.3]) 
   % line([104 104],[0.95 1.3]) 
end
%%
fr_num = size(cell_resp,2);
 for i = 1:length(L_HBO_Lateral)
     h = figure(33);
     set(h,'position',[250 250 1800 700]);
%           plot(-frame_turn(fr_num/5:fr_num/1,6)/5+1,'r');
               hold on;
     plot(cell_resp(L_HBO_Lateral(i),fr_num/5:fr_num/1));

     plot(mean(cell_resp(L_HBO_Lateral,fr_num/5:fr_num/1),1),'g');
%        ylim([.8 1.5])
     title(i)
waitforbuttonpress
close
 end
    %%
    h = figure('position',[250 250 1800 700]);
     
     plot(frame_turn(:,6)/6+1,'k')
     hold on;
     
     plot(mean(cell_resp(R_Rostra,:),1),'c');
     plot(mean(cell_resp(R_HBO_Medial,:),1));
       plot(mean(cell_resp(R_HBO_Lateral,:),1),'g');
       
            plot(mean(cell_resp(R_Caudal,:),1),'m');
            
                 plot(-mean(cell_resp(L_Rostra,:),1)+2,'c');
     plot(-mean(cell_resp(L_HBO_Medial,:),1)+2);
       plot(-mean(cell_resp(L_HBO_Lateral,:),1)+2,'g');
       
            plot(-mean(cell_resp(L_Caudal,:),1)+2,'m');
%      plot(mean(-cell_resp(R_Lateral,:)+2,1),'y');
%        plot(mean(-cell_resp(L_HBO,:),1)+2,'r');
       
%      plot(mean(cell_resp(L_deeptec_corLHBO,:),1),'r');
%        plot(mean(cell_resp(R_deeptec_corLHBO,:),1),'m');
       
       
       ylim([.8 1.5])
     filename = ['Ca with Turn'] ;
     %%
saveas(h,fullfile(filename),'fig');
     
%%
extracted_cellinds=cellinds_ex;
inds_ori=cellinds_ex;
    
save(fullfile(input_dir,'\Registered\extracted_cellinds_forbrain_LvsH.mat'),'extracted_cellinds');

%%
dim_x = size(cell_resp,2)/400;
IM = reshape(cell_resp(extracted_cellinds(14),:), 400 ,dim_x)';
figure('position',[50 50 1000 800]);
imagesc(IM);
title('cell-14')

%% for rostra clusters
Ca_turn = zeros(7,fr_num);


Ca_turn(1,:) = mean(cell_resp(R_HBO,:),1);
Ca_turn(2,:) = mean(cell_resp(L_HBO,:),1);
Ca_turn(3,:) = mean(cell_resp(R_Medial,:),1);
Ca_turn(4,:) = mean(cell_resp(L_Medial,:),1);
Ca_turn(5,:) = mean(cell_resp(R_Lateral,:),1);
Ca_turn(6,:) = mean(cell_resp(L_Lateral,:),1);
Ca_turn(7,:) = frame_turn(1:fr_num,6);



save(fullfile(input_dir,'\swims\Ca_turn.mat'),'Ca_turn');

%% for caudal
RostraHBOCaudal = zeros(9,fr_num);


RostraHBOCaudal(1,:) = mean(cell_resp(R_Rostra,:),1);
RostraHBOCaudal(2,:) = mean(cell_resp(L_Rostra,:),1);
RostraHBOCaudal(3,:) = mean(cell_resp(R_HBO_Medial,:),1);
RostraHBOCaudal(4,:) = mean(cell_resp(L_HBO_Medial,:),1);
RostraHBOCaudal(5,:) = mean(cell_resp(R_HBO_Lateral,:),1);
RostraHBOCaudal(6,:) = mean(cell_resp(L_HBO_Lateral,:),1);
RostraHBOCaudal(7,:) = mean(cell_resp(R_Caudal,:),1);
RostraHBOCaudal(8,:) = mean(cell_resp(L_Caudal,:),1);
RostraHBOCaudal(9,:) = frame_turn(1:fr_num,6);



save(fullfile(input_dir,'\swims\RostraHBOCaudal.mat'),'RostraHBOCaudal');

