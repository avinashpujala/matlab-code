if 0
    
clear all;close all;

input_dir='X:\YuMu\20140414\20140414_01_01_cy74_6d_gain_grating_20140414_163521';
%skiplist=[1:3]; %% skip first 1-5 trial because of the bleaching
tlen=240; %% frame cycle of the trial
timelist=[100 20 5 5 5 5 100]; %% set periods for each phase of a trial
%timelist=[30 10 5 30 10 5]; %% set periods for each phase of a trial

offset=5;

ave_stack  = double(readtiff(fullfile(input_dir,'\Registered\ave.tif')));  %% average anatomy stack of SPIM
load(fullfile(input_dir,'\Registered\cell_resp_dim_lowcut.mat'));
load(fullfile(input_dir,'\Registered\cell_info.mat'));
cell_resp = read_LSstack_fast_float(fullfile(input_dir,'\Registered\cell_resp_lowcut.stackf'),cell_resp_dim);
% load(fullfile(input_dir,'\swims\trialpowers.mat'))
% load(fullfile(input_dir,'\swims\skiplist.mat'))
load(fullfile(input_dir,'\Registered\shockCell.mat'));
load(fullfile(input_dir,'\swims\frame_swim.mat'));
skiplist=[]; %% you can omit specific trials from average

end
%%
cellinds=shockCell.IPN_all;



numcell=length(cell_info);
dim=size(ave_stack);
totlen=cell_resp_dim(2);
nrep=floor(totlen/tlen);

includeinds=setdiff(1:nrep, skiplist);
include_tcourse=zeros(tlen*length(includeinds),1);

for i=1:length(includeinds)
    include_tcourse(tlen*(i-1)+1:tlen*i)=tlen*(includeinds(i)-1)+1:tlen*(includeinds(i));
end

cell_resp=cell_resp(:,1:nrep*tlen);

cell_resp_ave=mean(reshape(cell_resp(:,include_tcourse),[numcell tlen length(includeinds)]),3);
cell_resp_ncorr=cell_resp(:,include_tcourse)-repmat(cell_resp_ave,[1 length(includeinds)]);



ncorr_matrix=zeros(length(cellinds));

%% correlation matrix
for ll=1:length(cellinds)
%        tmp1=cell_resp_ncorr(cellinds(ll),:);
%        ncorr_matrix(ll,:)=corrcoef_multipair_mex(double(tmp1),double(cell_resp_ncorr(cellinds,:)'));
%       tmp1=cell_resp(cellinds(ll),1:8000);
      tmp1=cell_resp(cellinds(ll),include_tcourse);
       ncorr_matrix(ll,:)=corrcoef_multipair_mex(double(tmp1),double(cell_resp(cellinds,include_tcourse)'));
end

corrMat = ncorr_matrix - eye(size(ncorr_matrix));
dissimilarity = 1 - corrMat;


%% classifying
Z = linkage(dissimilarity,'complete');
groups = cluster(Z,'maxclust',20);
[groups2, sI]=sort(groups,1,'ascend');

cellinds2=cellinds(sI);

ncorr_matrix2=ncorr_matrix(sI,:);
ncorr_matrix2=ncorr_matrix2(:,sI);

h1 = figure(1);
set(h1,'Position',[100, 100, 2200,1200])
subplot(1,2,1)
imagesc(ncorr_matrix2,[-0.7 0.7]);
%imagesc(ncorr_matrix,[-0.3 0.3]);


% h5=figure(5);set(h5,'Position',[100 100  1500 1200]);

subplot(1,2,2)
for i = 1:max(groups)
    inds = find(groups==i);
    plot(mean(cell_resp(cellinds(inds),:))-i/3,'g','linewidth',2);
    hold on
%     waitforbuttonpress
end
% plot(frame_swim(:,1)/25-(i-1)/3,'b','linewidth',1);
plot(frame_swim(:,2)/25-(i)/6,'b','linewidth',1);
return;
%%
h2 = figure(2);
set(h2, 'position', [100 100 2200 1200])
for j=1:min(length(cellinds2),144);
    cnum=j;
    if groups2(cnum)==1
        subplot(12,12,j);plot(cell_resp_ave(cellinds2(cnum),:),'r','linewidth',3);
    elseif groups2(cnum)==2
        subplot(12,12,j);plot(cell_resp_ave(cellinds2(cnum),:),'c','linewidth',3);
    elseif groups2(cnum)==3
        subplot(12,12,j);plot(cell_resp_ave(cellinds2(cnum),:),'g','linewidth',3);
    else
        subplot(12,12,j);plot(cell_resp_ave(cellinds2(cnum),:),'k','linewidth',3);
    end
%     ylim([0.95 1.5]);
axis off;
    title(num2str(cellinds2(cnum)));
end


%%
swim_ave = mean(reshape(frame_swim(1:3600,5),240,15),2);
figure
 for j=1:min(length(cellinds2));
    cnum=j;
    if groups2(cnum)==1
       plot(cell_resp_ave(cellinds2(cnum),:),'r','linewidth',1);
    elseif groups2(cnum)==2
        plot(cell_resp_ave(cellinds2(cnum),:),'c','linewidth',1);
    elseif groups2(cnum)==3
       plot(cell_resp_ave(cellinds2(cnum),:),'g','linewidth',1);
    else
       plot(cell_resp_ave(cellinds2(cnum),:),'k','linewidth',1);
    end
    ylim([0.95 1.5]);
hold on;
plot(swim_ave/5+1,'b');
waitforbuttonpress
    title(num2str(cellinds2(cnum)));
    hold off;
 end
 %%
figure
 for j=1:min(length(cellinds2));
    cnum=j;
    if groups2(cnum)==1
       plot(Resp(cellinds2(cnum),1:3600),'r','linewidth',1);
    elseif groups2(cnum)==2
        plot(Resp(cellinds2(cnum),1:3600),'c','linewidth',1);
    elseif groups2(cnum)==3
       plot(Resp(cellinds2(cnum),1:3600),'g','linewidth',1);
    else
       plot(Resp(cellinds2(cnum),1:3600),'k','linewidth',1);
    end
%     ylim([0.95 1.5]);
hold on;
plot((frame_swim(:,5))/5000+1,'b');
waitforbuttonpress
    title(num2str(cellinds2(cnum)));
    hold off;
end
%%

inds1=find(groups==1|groups==2|groups==9|groups==10|groups ==11);
inds2=find(groups==8);
inds3=find(groups==5|groups ==14|groups==15);
inds4=find(groups==4);




loc_inds=zeros(size(ave_stack,1),size(ave_stack,2),size(ave_stack,3), 3);
dimv_yxz=size(ave_stack);
stacklen=numel(ave_stack);
for i=1:length(inds3)    
    cinds=(cell_info(cellinds(inds3(i))).center(2)-1)*dimv_yxz(1)+cell_info(cellinds(inds3(i))).center(1);    
    zinds=dimv_yxz(1)*dimv_yxz(2)*(cell_info(cellinds(inds3(i))).slice-1);
    
    loc_inds(cinds+zinds)=0.25;
    loc_inds(cinds+zinds+stacklen)=.25;
    loc_inds(cinds+zinds+stacklen*2)=1;
    
end
for i=1:length(inds1)    
    cinds=(cell_info(cellinds(inds1(i))).center(2)-1)*dimv_yxz(1)+cell_info(cellinds(inds1(i))).center(1);    
    zinds=dimv_yxz(1)*dimv_yxz(2)*(cell_info(cellinds(inds1(i))).slice-1);
    
    loc_inds(cinds+zinds)=1;
    loc_inds(cinds+zinds+stacklen)=0;
    loc_inds(cinds+zinds+stacklen*2)=0;
    
end

for i=1:length(inds2)    
    cinds=(cell_info(cellinds(inds2(i))).center(2)-1)*dimv_yxz(1)+cell_info(cellinds(inds2(i))).center(1);    
    zinds=dimv_yxz(1)*dimv_yxz(2)*(cell_info(cellinds(inds2(i))).slice-1);
    
    loc_inds(cinds+zinds)=0;
    loc_inds(cinds+zinds+stacklen)=1;
    loc_inds(cinds+zinds+stacklen*2)=0;
    
end




figure(3);
image(squeeze(max(loc_inds,[],3)));


%%

h5=figure(5);set(h5,'Position',[100 100  500 1000]);
hold on
subplot(3,1,1);
plot(mean(cell_resp_ave(cellinds(inds1),:)),'g','linewidth',3);
% ylim([195 210]);
% line([120;120],[ones(1,1)*0.95;ones(1,1)*312]);
line([cumsum(timelist);cumsum(timelist)],[ones(1,length(timelist))*0.95;ones(1,length(timelist))*1.2]);
title(['Type 1 : ',num2str(length(inds1)), ' cells']);

subplot(3,1,2);
plot(mean(cell_resp_ave(cellinds(inds2),:)),'g','linewidth',3);
% ylim([205 216]);
% line([120;120],[ones(1,1)*0.95;ones(1,1)*312]);
line([cumsum(timelist);cumsum(timelist)],[ones(1,length(timelist))*0.95;ones(1,length(timelist))*1.2]);
title(['Type 2 : ',num2str(length(inds2)), ' cells']);

subplot(3,1,3);
plot(mean(cell_resp_ave(cellinds(inds3),:)),'g','linewidth',3);
% ylim([238 248]);
% line([120;120],[ones(1,1)*0.95;ones(1,1)*312]);
line([cumsum(timelist);cumsum(timelist)],[ones(1,length(timelist))*0.95;ones(1,length(timelist))*1.2]);
title(['Type 3 : ',num2str(length(inds3)), ' cells']);
% 
% subplot(4,1,4);
% plot(mean(cell_resp_ave(cellinds(inds4),:)),'g','linewidth',3);
% ylim([221 231]);
% % line([120;120],[ones(1,1)*0.95;ones(1,1)*312]);
% line([cumsum(timelist);cumsum(timelist)],[ones(1,length(timelist))*0.95;ones(1,length(timelist))*333]);
% title(['Type 4 : ',num2str(length(inds4)), ' cells']);
%%


%%

subplot(3,1,1);
plot(mean(cell_resp(cellinds(inds1),:)),'r','linewidth',3);
% ylim([195 210]);
title(['Type 1 : ',num2str(length(inds1)), ' cells']);

subplot(3,1,2);
plot(mean(cell_resp(cellinds(inds2),:)),'g','linewidth',3);
% ylim([205 216]);
title(['Type 2 : ',num2str(length(inds2)), ' cells']);

subplot(3,1,3);
plot(mean(cell_resp(cellinds(inds3),:)),'b','linewidth',3);
% ylim([238 248]);
title(['Type 3 : ',num2str(length(inds3)), ' cells']);
% 
% subplot(4,1,4);
% plot(mean(cell_resp_ave(cellinds(inds4),:)),'g','linewidth',3);
% ylim([221 231]);
% % line([120;120],[ones(1,1)*0.95;ones(1,1)*312]);
% line([cumsum(timelist);cumsum(timelist)],[ones(1,length(timelist))*0.95;ones(1,length(timelist))*333]);
% title(['Type 4 : ',num2str(length(inds4)), ' cells']);
%%
saveas(h5,fullfile(input_dir,'\Registered\v_para_tectum.fig'),'fig');
%%

%%
rf_cellinds2=cellinds(inds1);
save(fullfile(input_dir,'\Registered\rf_cellinds2.mat'),'rf_cellinds2');


%%
rf_cellinds3=cellinds(inds2);
save(fullfile(input_dir,'\Registered\rf_cellinds3.mat'),'rf_cellinds3');



%%
if 1   
    
    ave_stack2=zeros(size(ave_stack,1), size(ave_stack,2)*2, size(ave_stack,3) ,3);
    zlen=size(ave_stack,3);
    dimv_yxz=size(ave_stack);
    stacklen=numel(ave_stack);

    circle=makeDisk2(3,7);
    [r, v]=find(circle);
    r=r-4;v=v-4;
    circle_inds  = r*dimv_yxz(1)+v;

    for i=1:zlen
        ave_stack2(:,:,i,:)=repmat(imNormalize99(ave_stack(:,:,i)),[1 2 1 3]);
    end
    
    target_inds=cellinds(inds1); 

    for j=1:length(target_inds)    
        cinds=(cell_info(target_inds(j)).center(2)-1)*dimv_yxz(1)+cell_info(target_inds(j)).center(1);
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yxz(1)*dimv_yxz(2)); 
        zinds=dimv_yxz(1)*dimv_yxz(2)*2*(cell_info(target_inds(j)).slice-1);
        ave_stack2(cinds+circle_inds(labelinds)+zinds)=1;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*2)=0.3;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*4)=0;
    end
    
    
    target_inds=cellinds(inds2); 

    for j=1:length(target_inds)    
        cinds=(cell_info(target_inds(j)).center(2)-1)*dimv_yxz(1)+cell_info(target_inds(j)).center(1);
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yxz(1)*dimv_yxz(2)); 
        zinds=dimv_yxz(1)*dimv_yxz(2)*2*(cell_info(target_inds(j)).slice-1);
        ave_stack2(cinds+circle_inds(labelinds)+zinds)=0.2;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*2)=1;
        ave_stack2(cinds+circle_inds(labelinds)+zinds+stacklen*4)=0;
    end

    write_colortiff_mex(fullfile(input_dir,'Registered\cl_topology.tif'),uint8(ave_stack2*255),int32(size(ave_stack2)));
end

return;

    ;

