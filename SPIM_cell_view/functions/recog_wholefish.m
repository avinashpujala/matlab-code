clear all;close all;

img_dir='Y:\Yu\20140312\spon2_20140312_213304_erik\Registered';

dim=read_LSstack_size(fullfile(img_dir,'Stack dimensions.log'));
stack_av=read_LSstack_fast_float(fullfile(img_dir,'ave.stackf'),dim);
writetiff(uint16(stack_av),fullfile(img_dir,'ave.tif'));


%%
dim=size(stack_av);
cell_info=struct;
cell_color=zeros([dim(1) dim(2)*2+1 dim(3) 3]);
cellnum=0;
h=figure;

br_threshold=150;

%%

cell_rad=4;


ave_rad=round(cell_rad/2)+1;

avedisk=makeDisk2(ave_rad,ave_rad*2+1);
maxdisk=makeDisk2(cell_rad+2,cell_rad*2+5);

[r1, c1]=find(avedisk);
r1=r1-ave_rad-1;
c1=c1-ave_rad-1;

[r2, c2]=find(maxdisk);
r2=r2-cell_rad-3;
c2=c2-cell_rad-3;

maskinds=r1*dim(1)+c1;
maxinds=r2*dim(1)+c2;

maximg=zeros(dim(1),dim(2),length(r2));
allmask=zeros(dim(1),dim(2));
imlen=dim(1)*dim(2);

skipz=[];
if(~isempty(skipz))
    zlist=setdiff(1:size(stack_av,3),skipz);
else
    zlist=1:size(stack_av,3);
end

for z=zlist
    
    %%
    im=stack_av(:,:,z);
    OK=0;
    allmask(:)=0;
    allmask2=allmask;
    br_filter=imerode(im>br_threshold,strel('arbitrary',avedisk));
    mask=br_filter;
    
    imrank=calc_rank_simple(im,cell_rad*2,mask);
    aveimg=zeros(size(im));
    for i=1:length(r1)
       aveimg=aveimg+translateImage2D(imrank,c1(i),r1(i));
    end
    aveimg=aveimg/length(r1);
    aveimg_ori=aveimg;
    
    
    %% recognizing cells in the first round

    totinds= find(br_filter>0);        
    avelist=aveimg(totinds);
    maximg=zeros(length(totinds),length(r2));

    for i=1:length(r2)
        tempinds=mod(totinds+maxinds(i)+imlen,imlen);
        tempinds(tempinds==0)=imlen;
        maximg(:,i)=aveimg(tempinds);
    end

    maximg2=max(maximg,[],2);

    inds=find(maximg2==avelist & aveimg(totinds) >0.5);
    mask2=zeros(dim(1),dim(2));

    for i=1:length(inds)
        cinds=totinds(inds(i))+maskinds;
        cinds(cinds > imlen)=[];
        cinds(cinds < 1)=[];
        mask2(cinds)=1;
    end

    allmask=mask2;
    mask3=imdilate(mask2,strel('arbitrary',maxdisk));
    
    %% recognizing  cells in second round
    
    totinds= find(br_filter>0 & mask3==0);        
    avelist=aveimg(totinds);
    maximg=zeros(length(totinds),length(r1));

    for i=1:length(r1)
        tempinds=mod(totinds+maskinds(i)+imlen,imlen);
        tempinds(tempinds==0)=imlen;
        maximg(:,i)=aveimg(tempinds);
    end

    maximg2=max(maximg,[],2);

    inds=find(maximg2==avelist & aveimg(totinds) >0.5);
    mask2=zeros(dim(1),dim(2));

    for i=1:length(inds)
        cinds=totinds(inds(i))+maskinds;
        cinds(cinds > imlen)=[];
        cinds(cinds < 1)=[];
        mask2(cinds)=1;
    end

    allmask=allmask+mask2;
    

    %% create each cell ROIs

    [celllabel, totcell]=bwlabel(allmask,8);
    if totcell>0
            cell_info=create_cell_info_fish(cell_info,celllabel, totcell,z);
    end

    cell_color(:,:,z,:)=reshape(imMask2D_fish(im,celllabel),[dim(1) dim(2)*2+1 1 3]);

    cellnum=cellnum+totcell;    
    disp(['Recognizing cells in the plane' num2str(z)]);
    
end

%%
cellmask_name = ['cellmask_' num2str(br_threshold) '.tif'];
writetiff8(cell_color/255,fullfile(img_dir,cellmask_name));
save(fullfile(img_dir,'cell_info.mat'),'cell_info');

close(h);