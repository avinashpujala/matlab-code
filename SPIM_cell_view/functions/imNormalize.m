function out=imNormalize(im,perc)
% out = imNormalize(im,perc);
% Inputs:
% im - Image stack
% perc - Number of data points to use to determine ceiling. For example,
%   if perc = 999, then uses the 1000th value below the max values.
% 

im=double(im);
temp=sort(im(:),'descend');
th1=temp(round(length(im(:))/(perc+1)));
th2=min(im(:));

out=(im-th2)/(th1-th2);
out(out>1)=1;


