function cell_inds=define_region_roi_new( cell_info, cell_inds, ave_stack)

dim=size(ave_stack);
target_inds=cell_inds;
celllabels=zeros(size(ave_stack));
for i=1:length(target_inds)
    if ~isempty(cell_info(target_inds(i)).slice)
        zplane=cell_info(target_inds(i)).slice;
        inds=(zplane-1)*dim(1)*dim(2)+cell_info(target_inds(i)).inds;
        celllabels(inds)=target_inds(i);

    end    
end

cmask=zeros(dim);
hf=figure;set(hf,'Position',[100 100 1200 1000]); 
ha=axes;


for z=1:dim(3)
    
    cmasks=imdilate(celllabels(:,:,z)>0, strel('arbitrary',ones(7,7)));
    tmp=repmat(imNormalize99(ave_stack(:,:,z)),[1 2 3]);
    temp_inds=find(cmasks>0);
    tmp(temp_inds)=0;
    tmp(temp_inds+dim(1)*dim(2)*2)=0.7;
    tmp(temp_inds+dim(1)*dim(2)*4)=1;
    cla;
    image(tmp);
    title(num2str(z));
    [x y button]=ginput(1);
    if button==2
        break;
    elseif button ==3
        continue;
    else
        h=imfreehand(ha);
        tmask=createMask(h); 
        cmask(:,:,z)=cmask(:,:,z)+tmask(:,1:dim(2));  
        [x y button]=ginput(1);
        if button ~=1
            continue;
        else            
            delete(h);
            h=imfreehand(ha);
            tmask=createMask(h); 
            cmask(:,:,z)=cmask(:,:,z)+tmask(:,1:dim(2)); 

        end    
        imagesc(cmask(:,:,z));
        drawnow;
        pause;
        
    end    
    
end

celllabel2=celllabels;
celllabel2(~cmask)=0;

CC=bwconncomp(celllabel2,8);
cell_inds=zeros(CC.NumObjects,1);

for j=1:length(cell_inds);
    cell_inds(j)=round(max(celllabels(CC.PixelIdxList{j})));
end

close(hf);
    