clear all;close all;


input_dirs=struct;

<<<<<<< HEAD
input_dirs(1).fname = 'X:\Avinash\11-1-2014\Fish2\G10_T5_SL1_1p5_2_20141101_201925';
=======
input_dirs(1).fname = 'X:\Avinash\10-25-2014\Fish1_G50_T1_SL_1_1p5_2_2p5_3_20141025_155831';
>>>>>>> 2e9c3e1459eeff2db82af6b81b042c29c759eb72



for fnum=1:length(input_dirs)
    
    input_dir=input_dirs(fnum).fname;
    disp(input_dir);
    sdim=double(read_LSstack_size(fullfile(input_dir,'Stack dimensions.log')));
    radius=40;
    interval=30;
    numpool=6;
    zcycle=60;
    xypixeldist=0.406;
    zpixeldist=5;

    matlabpool(6);
    rwidth=radius*2+1;
    zlist=1:sdim(3);
    ref_stacks=struct;
    refstack=zeros(sdim(1),sdim(2),sdim(3));

    for i=1:sdim(3)
        ref_stacks(i).sdim=sdim;
    end
    
   
    
    %%  reading files from TM0XXXX_CM0_CHN00.stacks
    [filelist,nfiles]=GetAllFileNames(input_dir,'stack',[],'CM0');
    tot_tlen=nfiles;
    
    
    
    parfor zz=1:length(zlist)
        d=ref_stacks(zz).sdim;
        fstack=zeros(d(1),d(2),zcycle,'uint16');
        for k=1:zcycle
            fname1=['TM',num2str(k,'%.5d'),'_CM0_CHN00.stack'];
            fstack(:,:,k)=read_LSstack_fast3(fullfile(input_dir,fname1),[d(1) d(2)],zz)
        end
        refstack(:,:,zz) = create_zslice_ave_mex(uint16(fstack), int32([d(1) d(2)]),int32((1:zcycle)))  
    end
    
    for i=1:sdim(3)    
        k=(-2:2)+i;
        inds=find(k>0 & k<=sdim(3));
        ref_stacks(i).refstack=refstack(:,:,k(inds));
    end

    %%


    output=struct;
    motion_param=struct;
    parfor zz=zlist

      
        
        %% reading files from TM0XXXX_CM0_CHN00.stacks
        disp(['Checking motion of plane', num2str(zz)]);
        d=ref_stacks(zz).sdim;  
        
        refpos=[-2:2]+zz;
        refpos=refpos(refpos>0 & refpos<= d(3));
        refz=find(refpos==zz);
          
        
        totcycle=floor(tot_tlen/zcycle);
        stack=zeros(d(1),d(2),2);
        fstack=zeros(d(1),d(2),zcycle,'uint16');    
        
        zcyclelist=[1 totcycle];

        for z=1:length(zcyclelist)
            for k=1:zcycle
                fname2=['TM',num2str((zcyclelist(z)-1)*zcycle+k,'%.5d'),'_CM0_CHN00.stack'];
                fstack(:,:,k)=read_LSstack_fast3(fullfile(input_dir,fname2),[d(1) d(2)],zz)
            end
            aveimg=create_zslice_ave_mex(uint16(fstack),int32([d(1) d(2)]),int32((1:zcycle)));
            stack(:,:,z)=stackRegister_fast(aveimg,ref_stacks(zz).refstack(:,:,refz));
        end
        
        %%

        sampleimg=double(mean(stack(:,:,1),3));

        regimg=repmat(imNormalize99(sampleimg),[1 1 3]);
        regimg2=regimg;
        pgrid=radius;
        ygrid_num=floor((d(1)-rwidth*2)/pgrid);
        xgrid_num=floor((d(2)-rwidth*2)/pgrid);
        npoints=ygrid_num*xgrid_num;
        indslist=zeros(npoints,2);

        [r, c]=find(ones(rwidth)>0);
        r=r-radius-1;c=c-radius-1;
        inds=(c-1)*d(1)+r;

        cc=1;
        for x=1:xgrid_num
            for y=1:ygrid_num
                rinds=(rwidth+(x-1)*pgrid)*d(1)+rwidth+(y-1)*pgrid+1;
                indslist(cc,1)=rinds;
                indslist(cc,2)=mean(sampleimg(rinds+inds));
                cc=cc+1;
            end
        end
        %%
        thre_inds=find(indslist(:,2)>150);
        indslist2=indslist(thre_inds,1);

        for i=1:length(indslist2)
            rinds=indslist2(i);
            regimg(rinds+inds)=1;
            regimg(rinds+inds+d(1)*d(2))=0;
            regimg(rinds+inds+d(1)*d(2)*2)=0;             
        end


        simg=double(stack(:,:,1));
        rs=zeros(length(indslist2),3,2);  
        tilt=zeros(length(indslist2),3);    
        tilt_med=zeros(length(indslist2),3);    
        tcourse=1:2;
        zmove=-2:2;

        for i=1:length(indslist2)
            rinds=indslist2(i);
            narray=zeros(length(r),1);
            source=fft2(reshape(simg(rinds+inds),[rwidth rwidth]));

            for j=1:2
                target=reshape(stack(rinds+inds+(j-1)*d(1)*d(2)),[rwidth rwidth]);
                buff=fftshift(ifft2(source.*conj(fft2(target))));
                [~,maxinds] = max(abs(buff(:)));

                a=-(mod(maxinds,rwidth)-radius-1);
                if a==-radius-1; a=radius;end

                rs(i,1,j)=a; 
                rs(i,2,j)=-(ceil(maxinds/rwidth)-radius-1);   

                moveinds=-rs(i,2,j)*d(1)-rs(i,1,j);

                tt=zeros(1,5);
                zinds=find(zmove+zz > 0 & zmove+zz<=d(3) );
                for k=1:length(zinds);                  
                         ztargetlist=ref_stacks(zz).refstack(rinds+moveinds+inds+(k-1)*d(1)*d(2));                     
                         tt(zinds(k))=corrcoef_pair_mex(target(:),ztargetlist(:));                   
                end



                [~,zmaxinds] = max(tt(zinds));
                rs(i,3,j)=zmove(zinds(zmaxinds));

            end

            py=polyfit(tcourse',squeeze(rs(i,1,:)),1);
            px=polyfit(tcourse',squeeze(rs(i,2,:)),1);
            pz=polyfit(tcourse',squeeze(rs(i,3,:)),1);

            tilt(i,1)=py(1)*(length(tcourse)-1);
            tilt(i,2)=px(1)*(length(tcourse)-1);
            tilt(i,3)=pz(1)*(length(tcourse)-1);

        end

        [r, c]=find(ones(3,3));
        median_inds=(c-2)*ygrid_num+(r-2);

        for jj=1:3

            move_matrix=zeros(ygrid_num,xgrid_num);
            ones_matrix=zeros(ygrid_num,xgrid_num);

            for ii=1:length(indslist2)
                move_matrix(thre_inds(ii))=tilt(ii,jj);
                ones_matrix(thre_inds(ii))=1;
            end

            for ii=1:length(indslist2)
                tmp = thre_inds(ii)+median_inds;
                inds1=thre_inds(ii)+median_inds(tmp>0 & tmp<=xgrid_num*ygrid_num);
                inds2=inds1(ones_matrix(inds1)>0);
                tilt_med(ii,jj)=median(move_matrix(inds2));
            end
        end

        output(zz).masks=regimg;
        output(zz).tilt=tilt;
        output(zz).tilt_med=tilt_med;
        output(zz).indslist2=indslist2;
        output(zz).regimg2=regimg2;

        motion_param(zz).tilt=tilt;
        motion_param(zz).tilt_med=tilt_med;
        motion_param(zz).indslist=indslist2;
        motion_param(zz).xymove_av=mean(sqrt(tilt_med(:,1).^2 + tilt_med(:,2).^2))*xypixeldist;
        motion_param(zz).xymove_sd=std(sqrt(tilt_med(:,1).^2 + tilt_med(:,2).^2)*xypixeldist,[],1);
        motion_param(zz).zmove_av=mean(abs(tilt_med(:,3)))*zpixeldist;
        motion_param(zz).zmove_sd=std(abs(tilt_med(:,3))*zpixeldist);


    end
    matlabpool close;


    %%
    h2=figure(2);set(h2,'Position',[50 50 400 800]);
    dim=size(output(1).masks);
    colorlist=[0 0 1;
               0 1 1;
               0 1 0;
               1 1 0;
               1 0 0;];

    zmovelist=[-2 -1 0 1 2];


    for zz=1:length(zlist)
        clf(h2);

        
        indslist2=output(zz).indslist2;
        tilt=output(zz).tilt_med;
        regimg2=output(zz).regimg2;
        ha=axes; set(ha,'Position',[0 0 1 1]);
        
        image(regimg2);hold on;    
        hq=quiver(ceil(indslist2/dim(1)),mod(indslist2,dim(1)),tilt(:,2),tilt(:,1),0,'linewidth',2,'Color',[1 0 0]);

        hold off;

        hU = get(hq,'UData') ;
        hV = get(hq,'VData') ;
        set(hq,'UData',10*hU,'VData',10*hV)
        axis off;

        CC=getframe(h2);    
        output(zz).arrows=CC.cdata;
        cla;
        
        set(ha,'Position',[0 0 1 1]);
        image(regimg2);hold on;

        for i=1:5
            inds=find(round(tilt(:,3))==zmovelist(i));
            if ~isempty(inds)
                for j=1:length(inds)
                    rectangle('Position',[ceil(indslist2(inds(j))/dim(1)),mod(indslist2(inds(j)),dim(1)), 20, 20],'FaceColor',colorlist(i,:));
                end
            end
        end

        hold off;
        axis off;

        CC=getframe(h2);    
        output(zz).zmotion=CC.cdata;

    end

    odim=size(output(1).arrows);
    outstack=zeros(odim(1),odim(2)*2+1,length(zlist),3);

    for i=1:length(zlist)
        outstack(:,1:odim(2),i,:)=reshape(output(i).arrows,[odim(1) odim(2) 1 3]);
        outstack(:,(1:odim(2))+odim(2)+1,i,:)=reshape(output(i).zmotion,[odim(1) odim(2) 1 3]);
    end

    write_colortiff_mex(fullfile(input_dir,'motion.tif'),uint8(outstack),int32(size(outstack)));
    save(fullfile(input_dir,'motion_param.mat'),'motion_param');
    %%

    h3=figure(3);
    set(h3,'Position',[100 100 1500 750]);
    subplot(1,2,1);
    for zz=1:length(zlist)
        indslist=output(zz).indslist2;
        tilt=output(zz).tilt_med;
        plot(ones(1,length(output(zz).indslist2))*zz,sqrt(tilt(:,1).^2+tilt(:,2).^2)*xypixeldist,'.');hold on;
    end
    errorbar([motion_param(zlist).xymove_av],[motion_param(zlist).xymove_sd],'r','LineWidth',2,'LineStyle','none');
    scatter(zlist,[motion_param.xymove_av],'ro','fill');hold off;
    xlim([min(zlist)-1 max(zlist)+1]);ylim([-1 5]);title('XY motion');

    subplot(1,2,2);
    for zz=1:length(zlist)
        indslist=output(zz).indslist2;
        tilt=output(zz).tilt_med;
        plot(ones(1,length(output(zz).indslist2))*zz,abs(tilt(:,3))*zpixeldist,'.');hold on;
    end
    errorbar([motion_param.zmove_av],[motion_param.zmove_sd],'r','LineWidth',2,'LineStyle', 'none');
    scatter(zlist,[motion_param.zmove_av],'ro','fill');hold off;
    xlim([min(zlist)-1 max(zlist)+1]);ylim([-1 10]);title('Z motion');


    set(h3,'PaperPositionMode','auto'); 
    saveas(h3,fullfile(input_dir,['motion_graph.tif']),'tif');
    saveas(h3,fullfile(input_dir,['motion_graph.eps']),'eps');
end











