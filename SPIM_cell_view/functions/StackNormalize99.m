function normStack = StackNormalize99(imgStack)
%% Similar to TK's imNormalize99, but for the whole image stack instead of a single image
imgStack = double(imgStack);
sortedPixels  = sort(unique(imgStack(:)),'descend');
th1 = sortedPixels(round(length(sortedPixels)/100)); % 99th percentile value
th2 = sortedPixels(end); % Min value

% mv = zeros(size(imgStack,3),1);
% for sliceNum = 1:size(imgStack,3); 
%     currentImg = imgStack(:,:,sliceNum);
%     mv(sliceNum) = min(currentImg(:));                        
% end
% th2 = max(mv);

normStack = (imgStack-th2)/(th1-th2);
% normStack(normStack<0)=0;
% normStack(normStack>1)=1;

