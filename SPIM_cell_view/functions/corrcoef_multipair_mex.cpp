#include"mex.h"
#include <stdlib.h>
#include <math.h>

void mexFunction( int Nreturned,  mxArray *returned[], int Noperand, const  mxArray *operand[] ){
  double  *X        = mxGetPr(operand[0]);
  double  *Y        = mxGetPr(operand[1]);
  double *corr_coef;
  
  int len1 =mxGetM(operand[0])*mxGetN(operand[0]);
  int len2=mxGetM(operand[1])*mxGetN(operand[1]);
  int nsample=len2/len1;
  
  if ((len2 % len1) !=0){
        mexPrintf("Error: input length mismatch \n"); 
        returned[0] = mxCreateNumericMatrix(1,1,mxDOUBLE_CLASS,mxREAL);    
        corr_coef= mxGetPr(returned[0]);
        *corr_coef=0;
  }else{
        double x_mean,x_dist,y_mean,y_dist,sub;
        double *x_sub, *y_sub;
        
        x_sub=(double *)mxCalloc(len1,sizeof(double));
        y_sub=(double *)mxCalloc(len1,sizeof(double));
        returned[0] = mxCreateNumericMatrix(nsample,1,mxDOUBLE_CLASS,mxREAL);    
        corr_coef= mxGetPr(returned[0]);
        
        x_mean=0;
        x_dist=0;
        

        int i=len1;
        while (i--){
            x_mean +=X[i];  
        }
        x_mean=x_mean/((double)len1);  

        i=len1;
        while (i--){ 
         x_dist   +=pow((X[i]-x_mean),2);
         x_sub[i] = X[i]-x_mean;
        }
        
        
        int nn;
        
        for (nn=0;nn<nsample; nn++){
            sub =0;
            y_dist=0;
            y_mean=0;

            i=len1;
            while (i--){
                y_mean +=Y[nn*len1+i];  
            }  
            y_mean=y_mean/((double)len1);  
            
            i=len1;
            while (i--){ 
                y_sub[i] = Y[nn*len1+i]-y_mean;
            }

            i=len1;
            while (i--){
                y_dist   +=pow(y_sub[i],2);
                sub      +=x_sub[i]*y_sub[i];
            }
            corr_coef[nn]=sub/(sqrt(x_dist)*sqrt(y_dist));
        }
  }
  
      
  
}

