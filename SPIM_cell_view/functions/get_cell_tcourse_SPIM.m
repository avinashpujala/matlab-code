clear all;close all;

input_dir= 'Z:\Misha\26mar14\gain_bar_1_20140326_143805\Registered';

%%
trial_tlen=240;
corr_thre=0.7;
move_thre=1;
imfreq = 2.13;   % very important!!

%%  getting time course of cells

load(fullfile(input_dir,'cell_info.mat'));
load(fullfile(input_dir,'motion_param.mat'));
ave_stack=readtiff(fullfile(input_dir,'ave.tif'));

numcell=length(cell_info);
dim=size(ave_stack);

zplane_past=1;
outputName = [input_dir, '/Plane' num2str(zplane_past, '%.2d') '.stack'];
disp(['Getting time course of plane',num2str(zplane_past)]);
stack=read_LSstack_fast1(outputName,dim);
totlen=size(stack,3)-1;


cell_resp=zeros(numcell,totlen,'single');


for i=1:numcell
    
    
    zplane=cell_info(i).slice;
    
    if(zplane_past~=zplane)        
        outputName = [input_dir, '/Plane' num2str(zplane, '%.2d') '.stack'];
        stack=read_LSstack_fast1(outputName,dim);
        disp(['Getting time course of plane',num2str(zplane)]);
    end        
    
    inds=cell_info(i).inds;
    
    tmp_matrix=zeros(1,totlen,'single');
    for j=1:length(inds)
        indt=inds(j)+(0:dim(1)*dim(2):dim(1)*dim(2)*(totlen-1));
        tmp=stack(indt);
        tmp_matrix=tmp_matrix+single(tmp);
    end
    
    cell_resp(i,:)=tmp_matrix/length(single(inds));  

    zplane_past=zplane;
end


cell_resp_dim=size(cell_resp);
write_LSstack_fast_float(fullfile(input_dir,'cell_resp.stackf'),cell_resp);
save(fullfile(input_dir,'cell_resp_dim'),'cell_resp_dim');

%% apply low cut filter trial num;


disp('applying low cut filter...');
cell_resp2=zeros(size(cell_resp),'single');

for i=1:numcell
    
    tcourse=cell_resp(i, :)';
    tcourse_lc = tcLowCut(tcourse, 450*imfreq,'gaussian',1);
    temp = sort(tcourse_lc,'ascend');
    tcourse_lc= tcourse_lc ./ mean(temp(1:round(length(temp)/3)));
    
    cell_resp2(i,:) = tcourse_lc;
end


totrepeats=floor(cell_resp_dim(2)/ trial_tlen);
cell_resp2=cell_resp2(:,1:totrepeats*trial_tlen);


cell_resp_dim=size(cell_resp2);
write_LSstack_fast_float(fullfile(input_dir,'cell_resp_lowcut.stackf'),cell_resp2);
save(fullfile(input_dir,'cell_resp_dim_lowcut'),'cell_resp_dim');


%% removing double_counted cells

disp('Removing multiple-counted cells...');
cell_resp2_ncorr=cell_resp2-repmat(squeeze(mean(reshape(cell_resp2,[cell_resp_dim(1) trial_tlen totrepeats]),3)),[1 totrepeats]);

[r,c]=find(ones(5,5));
r=r-3;c=c-3;
moveinds=c*dim(1)+r;

cell_remove=zeros(1,length(cell_info));
for z=1:dim(3)-1

    zplane1=zeros(dim(1),dim(2));
    zplane2=zeros(dim(1),dim(2));

    cinds1=find([cell_info.slice]==z  & cell_remove==0);
    cinds2=find([cell_info.slice]==z+1 & cell_remove==0);

    for i=1:length(cinds1)
        cellcenter=dim(1)*(cell_info(cinds1(i)).center(2)-1)+cell_info(cinds1(i)).center(1);
        mask=find(cellcenter+moveinds>0 & cellcenter+moveinds<dim(1)*dim(2));
        zplane1(cellcenter+moveinds(mask))=cinds1(i);
    end

    for i=1:length(cinds2)
        cellcenter=dim(1)*(cell_info(cinds2(i)).center(2)-1)+cell_info(cinds2(i)).center(1);
        mask=find(cellcenter+moveinds>0 & cellcenter+moveinds<dim(1)*dim(2));
        zplane2(cellcenter+moveinds(mask))=cinds2(i);
    end 

    zplane3=(zplane1>0).*(zplane2>0);

    CC=bwconncomp(zplane3);
    rlist=zeros(1,CC.NumObjects);
    clist=zeros(1,CC.NumObjects);

    for i=1:CC.NumObjects
        tmp=CC.PixelIdxList{i};
        cnum1=max(zplane1(tmp));
        cnum2=max(zplane2(tmp));
        clist(i)=cnum2;

        r=corrcoef_pair_mex(double(cell_resp2_ncorr(cnum1,:)),double(cell_resp2_ncorr(cnum2,:)));
        rlist(i)=r;

    end
    
    removelist=find(rlist>corr_thre);
    cell_remove(clist(removelist))=1;
end
keep_inds=find(cell_remove==0);
remove_inds=find(cell_remove>0);
disp([num2str(length(remove_inds)), 'cells removed by multi-counting'])

cell_info=cell_info(keep_inds);
cell_resp2=cell_resp2(keep_inds,:);


%%  calculating motion confidence

disp('Removing deformed cells....');
dists=zeros(1,length(cell_info));

for c=1:length(cell_info)
    
    z=cell_info(c).slice;
    yx=cell_info(c).center;
    
    grid_y=mod(motion_param(z).indslist,dim(1));grid_y(grid_y==0)=dim(1);
    grid_x=ceil(motion_param(z).indslist/dim(1));
    
    distance=sqrt((grid_y-yx(1)).^2 + (grid_x-yx(2)).^2);
    [V,inds]=sort(distance,'ascend');
    
    rlength=size(motion_param(z).tilt_med,1);
    cell_info(c).motion=mean(motion_param(z).tilt_med(inds(1:min([5 rlength])),:),1);  
    
end

motion_matrix=[cell_info.motion];
motion_y=motion_matrix(1:3:end);
motion_x=motion_matrix(2:3:end);
motion_z=motion_matrix(3:3:end);
remove_inds=find(abs(motion_y)>move_thre | abs(motion_x)>move_thre | abs(motion_z)>move_thre);
disp([num2str(length(remove_inds)), 'cells removed by motion_assessment'])

keep_inds=setdiff(1:length(cell_info),remove_inds);

cell_info=cell_info(keep_inds);
cell_resp2=cell_resp2(keep_inds,:);


cell_resp_dim=size(cell_resp2);
write_LSstack_fast_float(fullfile(input_dir,'cell_resp_processed.stackf'),cell_resp2);
save(fullfile(input_dir,'cell_resp_dim_processed'),'cell_resp_dim');
save(fullfile(input_dir,'cell_info_processed.mat'),'cell_info');





