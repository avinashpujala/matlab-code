
function [mdisk,  se, r, c, v,maskinds]=make_recog3D_disk(radius,dim)

mdisk=zeros(radius*2+1,radius*2+1,3);
mdisk(:,:,1)=makeDisk2(round(radius/2),radius*2+1);
mdisk(:,:,2)=makeDisk2(radius,radius*2+1);
mdisk(:,:,3)=makeDisk2(round(radius/2),radius*2+1);

se=strel(mdisk);
[r, cv]=find(mdisk);
v=ceil(cv/(radius*2+1));
c=cv-((v-1)*(radius*2+1));
r=r-radius-1;c=c-radius-1;
v=v-2;

maskinds=dim(1)*dim(2)*v + dim(1)*c + r;
