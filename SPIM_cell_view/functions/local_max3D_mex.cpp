#include"mex.h"   
#include <stdio.h>
#include <stdlib.h>


void mexFunction( int Nreturned, mxArray *returned[], int Noperand, const mxArray *operand[] ){
        
    float *img;       
    (float *)img =  (float *)mxGetData(operand[0]);
    
    int *moveinds;       
    (int *)moveinds =  (int *)mxGetData(operand[1]);  // [y x];    
    
    int *mask;       
    (int *)mask =  (int *)mxGetData(operand[2]);  // [y x];
    
    int *dim;       
    (int *)dim =  (int *)mxGetData(operand[3]);  // [y x];
    
    int ylim=dim[0];
    int xlim=dim[1]*dim[2];
    int imlen=ylim*xlim;
    int movelen=mxGetM(operand[1])*mxGetN(operand[1]);
    int masklen=mxGetM(operand[2])*mxGetN(operand[2]);
    
    unsigned char *output;       
    returned[0] = mxCreateNumericMatrix(ylim, xlim, mxUINT8_CLASS, mxREAL);
    output =  (unsigned char *)mxGetData(returned[0]);
    
    int before_inds, move_inds, minds;
    unsigned char tmp;
    float cent;
    
    
    for (int i=0; i<masklen; i++){
        tmp=1;
        minds=mask[i]-1;
        cent=img[minds];
        for (int j=0; j<movelen & tmp==1 ; j++){
            before_inds = ((minds+moveinds[j])+imlen) % imlen;
            tmp *= (cent>=img[before_inds]);
        }
        output[minds]=tmp;
    }
            
    mexUnlock();  
}
