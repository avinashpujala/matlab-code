function varargout = MapCellIndsToAveStack_beta(cellInds, cellInfo, avgStack)
%MapCellsIndsToAveStack  A function that maps cell indices to their location
%                        on the averaged image stack
% fh = MapCellsToAveStack(cellIndices, cellInfo,avStack);
%
% Inputs:
% cellInds = Vector of indices of cells to be mapped
% cellInfo = Matrix containing cell info (as created by TK's code)
% avgStack = Stack of temporally averaged registered planes onto which
%            the cells are to be mapped
% Outputs:
% fh = figure handle

normStack = StackNormalize99(avgStack);
normStack(normStack>1)=1; % This is what TK was doing
normStack_rgb = permute(repmat(normStack,[1 1 1 3]),[1 2 4 3]); % 3rd dim is for color channel, 4th dim is for slice
nCells=length(cellInfo);
dim = size(avgStack);
dimv_yx = size(normStack_rgb);
totImage=zeros(dimv_yx(1),dimv_yx(2));

circle=makeDisk2(7,15);
[r, v]=find(circle);
r=r-8;
v=v-8;
circle_inds  = r*dimv_yx(1)+v;

%% Uncomment this cell if same color is desired for al cells
colorValues = repmat([1 0 0],length(cellInds),1);

%%
cellPos_z = [cellInfo(cellInds).slice];

%    cVals =[cellInds(:), colorValues];
for zSlice  = 1:dimv_yx(4);
    fprintf(['\n Current Slice: ' num2str(zSlice) ' \n'])
    f = find(cellPos_z == zSlice);
    cellsInSlice = cellInds(f);
    if ~isempty(cellsInSlice)
    
    cVals = colorValues(f,:);
%     for jj=1:length(cellsInSlice)
      blah1 = [cellInfo(cellsInSlice).center];
      blah1 = blah1(1:2:end);
      blah2 = [cellInfo(cellsInSlice).center];
      blah2 = blah2(2:2:end);
        cinds=(blah2-1)*dimv_yx(1)+blah1;
        labelinds=find((cinds+circle_inds)>0 & (cinds+circle_inds)<=dimv_yx(1)*dimv_yx(2));
        lut = cVals(cellsInSlice,:);
        anatomy_image_yx = normStack_rgb(:,:,:,zSlice);
        anatomy_image_yx(cinds+circle_inds(labelinds))= lut(cellsInSlice,1);
        anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2))=lut(cellsInSlice,2);
        anatomy_image_yx(cinds+circle_inds(labelinds)+dimv_yx(1)*dimv_yx(2)*2)=lut(cellsInSlice,3);
        normStack_rgb(:,:,:,zSlice) = anatomy_image_yx;
        end
%     end
end

%%  Map Cells

figPos = getMonitorSize;
[figPos(1),figPos(2)] = deal(50);
figPos(4) = figPos(4)*0.8;
figPos(3) = min(0.95*figPos(4), figPos(3)*0.8);
figHandle = figure('position',figPos);
colormap(gray);
normStack_rgb(normStack_rgb>1)=1;
normStack_rgb(normStack_rgb<0)=0;
brightestPixel = max(normStack(:));
for jj = 1 :size(normStack_rgb,4);
    subaxis(5,9,jj, 'Spacing', 0, 'Padding', 0, 'Margin', 0);
    image(normStack_rgb(:,:,:,jj)); % Scaling to use the full color range
    axis tight
    axis off
end

varargout{1} = figHandle;
% hold on
% figure
% cb = colorbar;
% colormap(jet);
% %  cTick = get(cb,'YTick');
% %  cTickLabel = get(cb,'YTickLabel');
% tickGap = ceil(length(LUT)/10);
% cTick = 1:tickGap:length(LUT);
% cTickLabel = round(LUT(1:tickGap:end,1));
% cTickLabel = num2str(cTickLabel(:));
% set(cb,'YTick',cTick,'YTickLabel', cTickLabel)

return;



%%

cellinds_ex=define_region_roi_new( cellInfo, cells1, ave_stack);

%%
figure(2);
for j=1:length(cellinds_ex);
    cnum=j;
    subplot(10,10,j);plot(cell_resp_ave(cellinds_ex(j),:),'r','linewidth',1);
    title(num2str(cnum));
    ylim([130 200]);
    xlim([0 sum(timelist)]);
    line([5 5],[0.95 1.2])
    % line([104 104],[0.95 1.3])
end

%%
hb_cellinds=cellinds_ex;

save(fullfile(input_dir,'\Registered\hb_cellinds.mat'),'hb_cellinds');

%%
dim_x = size(cell_resp,2)/400;
IM = reshape(cell_resp(extracted_cellinds(14),:), 400 ,dim_x)';
figure('position',[50 50 1000 800]);
imagesc(IM);
title('cell-14')

end

