
%% Inputs
idx_fish        = 1;
w               = struct;
w.tptr          = 'modwtsqtwolog';
w.sorh          = 's';
w.scal          = 'mln';
w.n             = 2;
w.name          = 'db2';



%%
pd = procData_cell{idx_fish};
disp('Reading roi...')
roi = pd.roi;

disp('Reading table...')
tbl = pd.table;
dir_out = fileparts(pd.Properties.Source);
fn = fullfile(dir_out,'procTable.xlsx');

disp('Writing table...')
writetable(tbl,fn, 'FileType','spreadsheet');

disp('Reading bas...')
bas = pd.bas;
mtr = [bas.ch3(:), bas.ch4(:)];
mtr_den = mtr*0;

disp('Reading roi...')
roi_ts = pd.roi_ts;

disp('Denoising...')
tic
for mtr_ =  1:size(mtr,2)
    mtr_den(:,mtr_) = wden(mtr(:,mtr_),w.tptr, w.sorh, w.scal, w.n, w.name);
end
toc
bas.ch34_den = mtr_den;

roi_ts_den = roi_ts*0;
for iRoi = 1:size(roi_ts,1)
    roi_ts_den(iRoi,:) = wden(roi_ts(iRoi,:),w.tptr, w.sorh, w.scal, w.n, w.name);
end
disp('Appending denoised channels to procData.mat...')
tic
pd.Properties.Writable = true;
pd.bas = bas;
pd.roi_ts_den = roi_ts_den;
toc
% [mtr_trial,inds_incomp] = SegmentDataByEvents(mtr_smooth,1/6000,tbl.stimTiming,1,20);




%%



