
function zPlanes = FindPlanesInDir(varargin)
% FindPlanesInDir - Finds the indices of temporal planes within a directory
% zPlanes = FindPlanesInDir(inputDir)
% zPlanes = FindPlanesInDir(inputDir,fileStem)
% Inputs:
% inputDir - Directory containing the planes
% fileStem - Prefix for the names of the plane files, e.g., 'Plane'
inputDir = varargin{1};
itemsInDir = dir(inputDir);
if nargin < 2
    fileStem = 'Plane';
else
    fileStem = varargin{2};
end
filesInDir = {};
zPlanes = [];
for f = 1:length(itemsInDir)
    fileName = itemsInDir(f).name;
    startInd = strfind(lower(fileName),lower(fileStem));
    if ~isempty(startInd);
        filesInDir{f} = itemsInDir(f).name;
        endInd = strfind(fileName,lower('.'));
        zPlanes = [zPlanes(:); str2num(fileName(startInd + length(fileStem):endInd))];
    end
end

