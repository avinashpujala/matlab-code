
function SaveImages(I, imgDir, fileName,varargin)
% SaveImages - Given a 3D or 4D image stack, saves the images as a .tif stack
%   or as images of any other imgFormat. For 4D stack, assumes 4th dimension
%   is time or space and 3rd dimension is color.
% SaveImages(I,imgDir,fileName)
% SaveImages(I,imgDir,fileName,'imgFormat',imgFormat,'ext',ext)
% Inputs:
% I - Image stack of size [M N T].
% imgDir - Path to directory where images are to be saved.
%   fileName - Root file name to give image stack or series. If empty,
%   defaults to 'imgStack' for stack or 'img_0000x' for series,
%   where x = 1 for the first image.
% Optionals:
% imgFormat - 'stack' or 'series'. The former results in a stack being saved,
%   whereas the latter results in an image series.
% ext - 3 digit image extension such as 'tif'. If imgFormat = 'stack' then ext
%   defaults to 'tif'.

nWorkers = 10; % Matlab pool size when writing series
imgExt = 'bmp';
imgFormat = 'series';
filename = [];


if ndims(I) == 4
    nImages = size(I,4);
elseif ndims(I) == 3
    nImages = size(I,3);
elseif ndims(I)==2
    nImages = 1;
else
    disp('Check size of image input!')
end

p = gcp('nocreate');
if isempty(p)
    parpool(nWorkers);
end

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'imgformat'
                imgFormat = varargin{jj+1};
            case 'imgext'
                imgExt = varargin{jj+1};          
        end
    end
end

if ~exist(imgDir)
    mkdir(imgDir)
end

I = Standardize(I);
if strcmpi(imgFormat,'stack')
    saveAsStack(I,imgDir,fileName);
else
    saveAsSeries(I,imgDir,fileName,imgExt);
end

end

function saveAsStack(I,imgDir,fileName)
if isempty(fileName)
    fileName = 'imgStack';
end
fp = fullfile(imgDir,[fileName '.tif']);
nImages = size(I,3);
dispChunk = round(nImages/5);
nDims = ndims(I);
for imgNum = 1:size(I,nDims(end))
    if ndims(I)==4
        imwrite(I(:,:,:,imgNum),fp,'tif','WriteMode','append','Compression','none')
    else
        img = I(:,:,imgNum);
        imwrite(img,fp,'tif','WriteMode','append','Compression','none')
    end
    if mod(imgNum,dispChunk)==0
        disp(imgNum)
    end
end
end

function saveAsSeries(I,imgDir,fileName,imgExt)
if isempty(fileName)
    fileName = 'img';
end
imgExt(strfind(imgExt,'.'))=[];
fp = fullfile(imgDir,fileName);
nDims = ndims(I);
if ndims(I)>2
    imgInds = 1:size(I,nDims(end));
else
    imgInds = 1;
end

try
    if nDims ==4
        parfor jj = imgInds
            imwrite(I(:,:,jj),[fp '_' sprintf('%.5d', jj), '.' imgExt],imgExt)
        end
    else
        parfor jj = imgInds
            imwrite(I(:,:,jj),[fp '_' sprintf('%.5d', jj), '.' imgExt],imgExt)
        end
    end
    
catch
    if nDims ==4
        for jj = imgInds
            imwrite(I(:,:,jj),[fp '_' sprintf('%.5d', jj), '.' imgExt],imgExt)
        end
    else
        for jj = imgInds
            imwrite(I(:,:,jj),[fp '_' sprintf('%.5d', jj), '.' imgExt],imgExt)
        end
    end
end

end


