function nTimePts = TimePtsFromTPlane(imgDir)

imgInfo = imfinfo(fullfile(imgDir,'ave.tif'));
nPxlsInImg = imgInfo(1).Width * imgInfo(1).Height;

fid = fopen(fullfile(imgDir,'Plane01.stack'),'rb');
tic
A= fread(fid,'uint16');
toc
nTimePts = A/nPxlsInImg;