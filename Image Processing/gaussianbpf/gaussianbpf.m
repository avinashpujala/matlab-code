
function varargout = gaussianbpf(varargin)
% Butterworth Bandpass Filter
% This simple  function was written for my Digital Image Processing course
% at Eastern Mediterranean University taught by
% Assoc. Prof. Dr. Hasan Demirel
% for the 2010-2011 Spring Semester
% for the complete report:
% http://www.scribd.com/doc/51981950/HW4-Frequency-Domain-Bandpass-Filtering
%
% Written By:
% Leonardo O. Iheme (leonardo.iheme@cc.emu.edu.tr)
% 24th of March 2011
% filtered_image = gaussianbpf(img,d0,d1)
% filtered_image = gaussianbpf(img,d0,d1,'plotBool',0/1)
% [filtered_image, filter] = ....
% Inputs:
%   I = The input grey scale image
%   d0 = Lower cut off frequency
%   d1 = Higher cut off frequency
%   'plotBool' = 0 or 1; If 1, plots figures. 
% 
% Outputs:
% filtered_image = Filtered image
% filter = Filter applied in the Fourier domain
% 
% The function makes use of the simple principle that a bandpass filter
% can be obtained by multiplying a lowpass filter with a highpass filter
% where the lowpass filter has a higher cut off frquency than the high pass filter.
%
% Usage GAUSSIANBPF(I,DO,D1)
% Example
% ima = imread('grass.jpg');
% ima = rgb2gray(ima);
% filtered_image = gaussianbpf(ima,30,120);%
% Gaussian Bandpass Filter
%
% Modified by Avinash Pujala, HHMI, 2016

plotBool = 0;
filterFlag = 0;
I = varargin{1};

if all(size(varargin{2})>1)
    filter3 = varargin{2};
    filterFlag = 1;
else
    d0 = varargin{2};
    d1 = varargin{3};
end

for jj = 1:length(varargin)
    if strcmpi(varargin{jj},'plotBool')
        plotBool = varargin{jj+1};
    end
end


f = double(I);
[nx, ny] = size(f);
% f = uint8(f);
fftI = fft2(f,2*nx-1,2*ny-1);
fftI = fftshift(fftI);

% Initialize filter.
if filterFlag ==0
    filter1 = ones(2*nx-1,2*ny-1);
    filter2 = ones(2*nx-1,2*ny-1);
    filter3 = ones(2*nx-1,2*ny-1);
    for i = 1:2*nx-1
        for j =1:2*ny-1
            dist = ((i-(nx+1))^2 + (j-(ny+1))^2)^.5;
            % Use Gaussian filter.
            filter1(i,j) = exp(-dist^2/(2*d1^2));
            filter2(i,j) = exp(-dist^2/(2*d0^2));
            filter3(i,j) = 1.0 - filter2(i,j);
            filter3(i,j) = filter1(i,j).*filter3(i,j);
        end
    end
end
%## Update image with passed frequencies
% filtered_image = fftI + filter3.*fftI;
filter3 = filter3/max(filter3(:));
filtered_image = filter3.*fftI;


filtered_image = ifftshift(filtered_image);
filtered_image = ifft2(filtered_image,2*nx-1,2*ny-1);
filtered_image = real(filtered_image(1:nx,1:ny));
% filtered_image = uint8(filtered_image);

if plotBool
    ShowFIgs(f,fftI,filter3, filtered_image);
end

varargout{1} = filtered_image;
varargout{2} = filter3;
end

function ShowFIgs(f,fftI,filter3,filtered_image)
subplot(2,2,1)
imshow(f,[]);
title('Original Image')

subplot(2,2,2)
fftshow(fftI,'log')
title('Fourier Spectrum of Image')

subplot(2,2,3)
fftshow(filter3,'log')
title('Frequency Domain Filter Function Image')

subplot(2,2,4)
imshow(filtered_image,[])
title('Bandpass Filtered Image')
colormap(jet)
end