
function videoObject = StackToVid(imageStack,filename);
% V - Video of Image Stack in .avi format
% imageStack - Image Stack to be converted
blah = imageStack(:);
peak = max(blah);
baseline = 0.05*peak;
scale = [baseline peak];
fN = [filename '.avi'];
vidObj = VideoWriter(fN); % Prepare a video file
open(vidObj);
nFrames = size(imageStack,3);
for frame = 1:nFrames
    imagesc(imageStack(:,:,frame),scale), colormap(gray)
    title(['Converting Stack to Video...Please Wait.  Frame # ' num2str(frame)])
    set(gca,'ydir','normal','nextplot','replacechildren');
    axis image    
    currentFrame = getframe;
    writeVideo(vidObj,currentFrame); % Write each frame to the file
end
videoObject = vidObj;
close all
close(vidObj);
