function [y,x] =  RoiTimeCourseTempSol(inputStack,frameRate)
nFrames = size(inputStack,3);
x = (1:nFrames)/frameRate;
for frame = 1:nFrames
    blah = inputStack(:,:,frame);
    y(frame) = mean(blah(:));
end
