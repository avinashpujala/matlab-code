function Aout = readSI5betaData(filename,nFrames)

hTif = Tiff(filename);
Aout = zeros(hTif.getTag('ImageLength'),...
    hTif.getTag('ImageLength'),...
    nFrames, 'int16');
for i=1:nFrames
    disp(sprintf('Frame: %i',i));
    hTif.setDirectory(i);
    Aout(:,:,i) = hTif.read();
end

    
    
    