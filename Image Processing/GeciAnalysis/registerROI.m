function outputStack = registerROI(varargin)
% outputStack = registerROI(inputStack,referenceImageIndex)

% Take a stack of images as input (inputStack) and mark the image indexed by
% referenceImageIndex(default value = 1) the reference image, and all other
% images as the target images.
% Allow for the drawing of ROI(s) on the reference image.
% Compute the translation coordinates

%% Adjustable Parameters
kernelLength = 4; % In pixel units

inputStack = varargin{1};
imageLength = size(inputStack,1);
imageWidth = size(inputStack,2);
nFrames = size(inputStack,3);
outputStack = zeros(size(inputStack));

%% Checking Inputs for Errors
if size(inputStack,3)==1
    errordlg('Input to the function must be a stack, and not a single image!');
end

if nargin < 2
    referenceImageIndex = 1; % Setting the first image in the stack as the reference image
else
    referenceImageIndex = str2num(varargin{2});
end

 if any(referenceImageIndex)>size(inputStack,1)
    errordlg('The second argument must correspond to an image number from within the stack')
end

%% Defining ROIs on the Reference Image
if  numel(referenceImageIndex)==2;
    referenceImage = mean(inputStack(:,:,[referenceImageIndex(1):referenceImageIndex(2)]),3);
elseif numel(referenceImageIndex) > 2
    referenceImage = mean(inputStack(:,:,[referenceImageIndex]),3);
else
    referenceImage = inputStack(:,:,referenceImageIndex);
end
smoothedReferenceImage = smoothImage(referenceImage,kernelLength);
rois = setEllipticalRois(smoothedReferenceImage);
nRois = size(rois,2);
BW = zeros(size(inputStack));
[allRois.idx, allRois.xi, allRois.yi] = deal([]);
for rr = 1:nRois
    allRois.xi = [allRois.xi(:); rois{rr}.xi];
    allRois.yi =[allRois.yi(:); rois{rr}.yi];
    allRois.idx = [allRois.idx(:); rois{rr}.idx];
end
mat = BW(:,:,referenceImageIndex);
mat(allRois.idx)=1;
BW(:,:,referenceImageIndex) = mat;
outputStack(:,:,referenceImageIndex)= BW(:,:,referenceImageIndex).*smoothedReferenceImage;
[x{referenceImageIndex},y{referenceImageIndex}] = deal(allRois.xi,allRois.yi);
imageBuf = fft2(smoothedReferenceImage);

%% Finding Translation Coordinates for All Images in the Stack and Shifting the ROI Accordingly
remainingPlanes = 1:nFrames;
remainingPlanes(referenceImageIndex)=[];
warn = true;
for rr = remainingPlanes
    [dx,dy] = registerImagesMA(imageBuf,double(inputStack(:,:,rr)));
    x{rr} = x{referenceImageIndex} + dx;
    y{rr} = y{referenceImageIndex} + dy;
    try
        blah = sparse(x{rr},y{rr},1,imageLength,imageWidth);
    catch
        outX = find(x{rr}>imageLength | x{rr}<1);
        outY = find(y{rr}>imageWidth | x{rr}<1);
        outXY = [outX(:); outY(:)];
        x{rr}(outXY)=[];
        y{rr}(outXY) =[];
        blah = sparse(x{rr},y{rr},1,imageLength,imageWidth);
        if warn
              warndlg('Some ROI points out of range; those points clipped!')
              warn = false;
        end
    end
    BW(:,:,rr) = full(blah);
    smoothedImage = smoothImage(inputStack(:,:,rr),kernelLength);
    outputStack(:,:,rr) = BW(:,:,rr).*smoothedImage;
end





