function filtvec = butterworthLowpassFilter(vec, Fs, Fc, order)
% apply Butterworth low pass filter
% vec ... original 1d vector
% Fs ... sampling frequency in [Hz]
% Fc .... 3 dB cutoff frequency in [Hz]
% order .. order of Butterworth filter
if nargin <4
    order = 8;
end

h  = fdesign.lowpass('N,F3dB', order, Fc, Fs);
Hd = design(h, 'butter');
filtvec = filtfilt(Hd.sosMatrix,Hd.ScaleValues,vec);