 function Aout = readGeciData(varargin)
 
 % Aout = readGeciData();
 % Aout = readGeciData(filename);
 
if nargin == 0
[file,path] = uigetfile('*.tif');
filename = [path file];
end
cd(path)

hTif = Tiff(filename);
imgInfo = imfinfo(filename);
nFrames = size(imgInfo,1);
imageLength = hTif.getTag('ImageLength');
imageWidth = hTif.getTag('ImageWidth');

Aout = zeros(imageLength,imageWidth, size(imgInfo,1), 'int16'); 
% Unfortunately, many operations (such as conv2) not defined on class other than 'double'.
for i=1:size(imgInfo,1)
    hTif.setDirectory(i);
    Aout(:,:,i) = hTif.read();
end

hTif.close()
    
    
    