function outputImage = smoothImage(varargin)

% outputImage = smoothImage(inputImage);
% outputImage = smoothImage(inputImage,kernelLength); 
% inputImage must be an M X N matrix, while kernelLength is specified in
% pixels.
%
% 
% Future Implementation:
% outputImage = smoothImage(inputImage, kernelLength, kernelType);
% At present, the image always smoothed with a Hamming function, but in
% future versions of the programs, other kernel choices will be
% implemented.

if nargin<2
    errordlg('Please specify the length of the smoothing kernel!');
elseif nargin>2
     errordlg('Number of allowable inputs exceeded!');
end

%% Creating the Kernel Function
kernelLength = varargin{2};
kernel = hamming(round(kernelLength));
kernel = kernel(:)*kernel(:)';

%% Converting Image to Double Class for Convolution Operation
inputImage = varargin{1};
blah = whos('inputImage');
if ~strcmpi('double',blah.class)
    inputImage = double(inputImage);
end

%% Smoothing Image by Convolution with Kernel
outputImage = conv2(inputImage,kernel,'same');
