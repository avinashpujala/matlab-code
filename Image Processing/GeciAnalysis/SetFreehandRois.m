function rois = SetFreehandRois(im,varargin)
% SetFreehandRois - My variation of Minoru's setEllipticalRois.m wherein
%   the user can draw freehand ROIs
% rois = SetFreehandRois(im);
% rois = setFreeHandRois(im,'clrMap',clrMap,'figTitle',figTitle);
% Inputs:
% im - 3D image stack
% clrMap - Colormap to use when displaying ROIs
% figTitle - Figure title, can be used to display image number in stack

figTitle = '';
clrMap = gray(256);

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'clrmap'
                clrMap = varargin{jj+1};
            case 'figtitle'
                figTitle = varargin{jj+1};
        end
    end
end

% validateattributes(im,{'numeric'},{'2d'});
eH = [];
rois = [];
nRois = 0;
defaultScaling = 0.5;
hf = figure('Name',['ROIs ' figTitle]);
% hf = figure('Name',['ROIs ' figTitle],'units','normalized');
monitorSize = getMonitorSize;
lim = min(monitorSize(end-1:end));
mp = get(0,'MonitorPositions');
mp = mp(1,:);
set(hf,'Position',[mp(1,1)+30,mp(1,2),mp(1,3)*0.45,mp(1,4)*0.8]); % When running on my work desktop

% set(hf,'Position',[  0.0672   -0.5917    0.7578    1.3931])
% set(hf,'Position',[mp(1)+25 mp(2)+500 mp(3)*0.42 mp(4)*0.73]); % My laptop
 
h_window = axes('Parent',hf,'Units','normalized','Position',[0.05 0.05 0.9 0.8]);
h_addRoi = uicontrol(hf,'Style','Pushbutton','Units','normalized',...
    'Position',[0.15 0.86 0.1 0.03], 'String','Add ROI','Callback',@addRoi_callback);
h_removeRoi = uicontrol(hf,'Style','Pushbutton','Units','normalized',...
    'Position',[0.3 0.86 0.1 0.03],'String','Delete ROI','Callback',@deleteRoi_callback);
h_scale = uicontrol(hf,'Style','edit','Units','normalized','Position',[0.45 0.86 0.1 0.03],...
    'String',num2str(defaultScaling),'Callback',@updateScale_callback);
h_exit = uicontrol(hf,'Style','Pushbutton','Units','normalized',...
    'Position',[0.6 0.92 0.1 0.03],'String','Exit','Callback',@exit_callback);
base = min(im(:));
range = max(im(:))-min(im(:));
scale = base + defaultScaling*[0 range];
% imagesc(im,scale);axis off;
imagesc(im), axis off;
axis image
colormap(clrMap);

done = false;
while ~done
    uiwait(hf);
end

    function updateScale_callback(src,evt)
        scaling = str2double(get(h_scale,'String'));
        validateattributes(scaling,{'numeric'},{'scalar'});
        scale = base + scaling*[0 range];
        imagesc(im,scale);axis off;
    end

    function addRoi_callback(src,evt)
        nRois = nRois + 1;
        eH = imfreehand('closed','true');
        BW = createMask(eH);
        [xi,yi] = find(BW);
        
%         [BW, xi, yi] = roipoly;
        rois{nRois} = struct('idx',find(BW),...
            'xi',xi,'yi',yi,'bw',BW);
%         drawRois;
    end

    function deleteRoi_callback(src,evt)
        if nRois > 0
            rois(nRois) = [];
            delete(eH);
            nRois = nRois-1;
        end
%         drawRois;
    end

    function drawRois
        imagesc(im,scale);axis off;
        hold on;
        if nRois > 0
            for iRoi = 1:nRois
                xi = rois{iRoi}.xi;
                yi = rois{iRoi}.yi;
                line(xi, yi, 'Color', [1 0 0]);
                text(mean(xi),mean(yi),num2str(iRoi),'Color',[1 0 0]);
            end
        end
        hold off;
    end

    function exit_callback(src,evt)
        uiresume(hf);
        done=true;
        close(hf);
    end

end
        