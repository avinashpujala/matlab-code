function varargout =  PlayMovie(imageStack,frameRate,y,x)
% imageStack - Stack of .tif Images
% frameRate - Frame Rate
% y - Ca2+ Timeseries
% x - Time vector
frameSpeed = frameRate; % Frames Per Second
firstFrame = imageStack(:,:,1);
imageWidth = size(imageStack,2);
imageLength = size(imageStack,1);
baseline = min(imageStack(:));
% baseline = 0.2*max(firstFrame(:));
amplitude =max(imageStack(:));
scale = [baseline amplitude];
figure('Name','Ca^(2+) Timeseries')
shg
y = chebfilt(y,1/frameRate,1,'low');
U = uint16(imageStack);
tic
% M = moviein;
for fN = 1:size(imageStack,3)
        subplot(2,1,1)   
    axis equal, axis([0 imageWidth 0 imageLength])
    colormap(gray),imagesc(imageStack(:,:,fN),scale),shading interp
     drawnow 
    title(['Frame # ' num2str(fN) ',Frame speed  = ' num2str(frameSpeed) ' fps'])
    subplot(2,1,2)
    plot(x(1:fN),y(1:fN),'linewidth',2), set(gca,'ytick',[],'tickdir','out'),
    axis([0 x(end) min(y) max(y)]),drawnow
    M(:,fN) = getframe;
%     pause(1/frameSpeed)
end
toc
varargout{1} = M;