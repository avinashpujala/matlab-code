%function AnalyzeCaSignals()

frameRate = 30;
refFrameIdx = 1;

Aout = readGeciData(); % Load image stack
videoObj = StackToVid(Aout,'TempVideo'); % Convert image stack to a video file
implay('TempVideo.avi',2*frameRate);

%% Selecting Frames for ROI
default = num2str([1    10]); 
selectedFrames = inputdlg('Enter Starting and Ending Frame Index for ROI selection: ',...
    'Selecting Frames for ROI',1,default);
selectedFrames = str2double(selectedFrames);

outputStack = registerROI(Aout,selectedFrames);
[y,x] = RoiTimeCourseTempSol(outputStack,frameRate);
 M = PlayMovie(outputStack,frameRate,y,x);
% % implay(M,3*frameRate)
% data = calciumResponseCharacterize(y,frameRate);
%end

