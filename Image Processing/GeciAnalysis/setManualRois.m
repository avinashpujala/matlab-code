function rois = setManualRois(im)

validateattributes(im,{'numeric'},{'2d'});
rois = [];
nRois = 0;
defaultScaling = 1;
hf = figure('Name','ROIs');
set(hf,'Position',[100 100 1000 1000]);
colormap(gray);

h_window = axes('Parent',hf,'Units','normalized','Position',[0.05 0.05 0.9 0.8]);
h_addRoi = uicontrol(hf,'Style','Pushbutton','Units','normalized',...
    'Position',[0.15 0.86 0.1 0.03], 'String','Add ROI','Callback',@addRoi_callback);
h_removeRoi = uicontrol(hf,'Style','Pushbutton','Units','normalized',...
    'Position',[0.3 0.86 0.1 0.03],'String','Delete ROI','Callback',@deleteRoi_callback);
h_scale = uicontrol(hf,'Style','edit','Units','normalized','Position',[0.45 0.86 0.1 0.03],...
    'String',num2str(defaultScaling),'Callback',@updateScale_callback);
h_exit = uicontrol(hf,'Style','Pushbutton','Units','normalized',...
    'Position',[0.6 0.86 0.1 0.03],'String','Exit','Callback',@exit_callback);
base = min(im(:));
range = max(im(:))-min(im(:));
scale = base + defaultScaling*[0 range];
imagesc(im,scale);axis off;

done = false;
while ~done
    uiwait(hf);
end

    function updateScale_callback(src,evt)
        scaling = str2double(get(h_scale,'String'));
        validateattributes(scaling,{'numeric'},{'scalar'});
        scale = base + scaling*[0 range];
        imagesc(im,scale);axis off;
    end

    function addRoi_callback(src,evt)
        nRois = nRois + 1;
        [BW, xi, yi] = roipoly;
        rois{nRois} = struct('idx',find(BW),...
            'xi',xi,'yi',yi);
        drawRois;
    end

    function deleteRoi_callback(src,evt)
        if nRois > 0
            rois(nRois) = [];
            nRois = nRois-1;
        end
        drawRois;
    end

    function drawRois
        imagesc(im,scale);axis off;
        hold on;
        if nRois > 0
            for iRoi = 1:nRois
                xi = rois{iRoi}.xi;
                yi = rois{iRoi}.yi;
                line(xi, yi, 'Color', [1 0 0]);
                text(mean(xi),mean(yi),num2str(iRoi),'Color',[1 0 0]);
            end
        end
        hold off;
    end

    function exit_callback(src,evt)
        uiresume(hf);
        done=true;
        close(hf);
    end

end
        