function [data] = calciumResponseCharacterize(y,Fs)
% function [dff, rises, decays,ons] = calciumResponseCharacterize(y,Fs)
% [dff, rises, decays,ons] = calciumResponseCharacterize(y,Fs)
% - inputs
%   y ... signal, vector
%   Fs ... sampleRate [Hz]
%
% - outputs
%   data.pks ... peak heigth
%   data.locs ... peak index
%   data.rises ... rise time [s]
%   data.decays ... half decay time [s]

temp=sort(y(:),'descend');
th1=temp(round(length(y(:))/10));
th2=min(y(:));

y=(y-th2)/(th1-th2);

sy = butterworthLowpassFilter(y, Fs, 1, 6);
% cutoff at 1 Hz, 6-order Butterworth filter

dsy = diff(sy);

figure;
plot((1:numel(y))/Fs, sy,'b'); hold on;
plot((1:numel(y))/Fs, y,'k');

figure;
plot((1:numel(y))/Fs, [dsy 0],'r');

answer = inputdlg('Pick threshold value:');
th = str2num(answer{1});

[~,dsPkInd] = findpeaks(dsy,'MINPEAKHEIGHT', th);
nPeaks = numel(dsPkInd);

% peak detection
% find the 1st point when dsy goes negative after the peak of dsy
locs = zeros([nPeaks 1]);
for iPeak = 1:nPeaks
    found = false;
    currInd = dsPkInd(iPeak);
    while ~found
        if dsy(currInd) < 0
            locs(iPeak) = currInd;
            found = true;
        end
        currInd = currInd + 1;
    end
end

pks = sy(locs);

% onset detection
% find the 1st point when dsy goes negative before the peak of dsy
ons = zeros([nPeaks 1]);
for iPeak = 1:nPeaks
    found = false;
    currInd = dsPkInd(iPeak);
    while ~found
        if dsy(currInd) < 0
            ons(iPeak) = currInd + 1;
            found = true;
        end
        currInd = currInd - 1;
    end
end

rises = (locs-ons)/Fs;
for i=1:numel(ons)
    baseWindow = (-round(Fs):0) + ons(i);
    baseWindow(baseWindow <1)=[];
    base = mean(sy(baseWindow));
end
dff = pks/base-1;

% half off detection
% find the 1st point when sy goes below the half height
halfOff = zeros([nPeaks 1]);
for iPeak = 1:nPeaks
    found = false;
    currInd = locs(iPeak);
    halfTh = mean(sy([locs(iPeak) ons(iPeak)]));
    while ~found
        if sy(currInd) < halfTh
            halfOff(iPeak) = currInd;
            found = true;
        end
        if currInd < numel(sy)
            currInd = currInd + 1;
        else
            found = true;
%            lastPeak = iPeak;
        end
    end
end

decays = (halfOff- locs)/Fs;
        
figure;
plot((0:numel(y)-1)/Fs,y,'k-');
hold on;
plot((0:numel(y)-1)/Fs,sy);
plot((locs-1)/Fs,sy(locs),'r*');
% 
answer = inputdlg('Pick the peak indices you like:');
pkIdx = str2num(answer{1});

dff = dff(pkIdx)';
rises = rises(pkIdx);
decays = decays(pkIdx);
ons = ons(pkIdx);
data = struct('dffs',dff,'rises',rises,'decays', decays,'ons',ons,'pkIndices',pkIdx);
close all
