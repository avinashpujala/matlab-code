function [out] = roiExtractTimecourseTempSol(fname,frameRate,nFrame)



playing = false;
%chIdx = 1;


%  [h, d] = mkScim_openTif(fname);
h.SI4.scanFrameRate = frameRate; % need to check for each magnification
d = readSI5betaData(fname,nFrame);
%d  = squeeze(d(:,:,chIdx,:));
dim = size(d);
base = min(d(:));
range = median(d(:)) - base;
clims = base + int16(double(range).*[1.0 3.0]);

hf = figure('Name','Raw');
set(hf,'Position', [100 100 800 800]);
set(hf,'Renderer','zbuffer');
colormap(gray);
t = 1;

h_window=axes('Parent',hf,'Units','normalized','Position', [0.05 0.05 0.9 0.8]);
h_slider = uicontrol(hf,'Style','slider','Units','normalized','Position', [0.1 0.9 0.8 0.02],'SliderStep',[1/(dim(3)-1) 1/(dim(3)-1)],'Callback',@slider_callback);axis off;
h_text = uicontrol(hf,'Style','text','Units','normalized','Position', [0.45 0.93 0.1 0.02]);axis off;
h_play = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.15 0.86 0.1 0.03],'String','Play','Callback',@play_callback);
h_stop = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.3 0.86 0.1 0.03],'String','Stop','Callback',@stop_callback);
h_timeWindow = uicontrol(hf,'Style','edit','String',['1:','4000'],'Units','normalized','Position',[0.45 0.86 0.1 0.03]);
h_anly = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position',[0.6 0.86 0.1 0.03],'String','Analyze','Callback',@analyze_callback);
%h_startROI = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position', [0.15 0.86 0.1 0.03],'String','Start ROI','Callback',@startROI);
%h_resetROI = uicontrol(hf,'Style','Pushbutton','Units','normalized','Position', [0.3 0.86 0.1 0.03],'String','Reset ROI','Callback',@resetROI);
h_exit= uicontrol(hf,'Style','Pushbutton','Units','normalized','Position', [0.8 0.86 0.1 0.03],'String','EXIT','Callback',@exit_callback);
%h_tmin= uicontrol(hf,'Style','edit','Units','normalized','Position', [0.5 0.86 0.05 0.03],'String',num2str(1),'Callback',@zmin_change);
%h_tmax= uicontrol(hf,'Style','edit','Units','normalized','Position', [0.6 0.86 0.05 0.03],'String',num2str(dim(3)),'Callback',@zmax_change);

imagesc(d(:,:,t),clims);axis off;
set(h_text,'String', ['t=',num2str(t)]);

done = false;
while ~done
    uiwait(hf);
end


    function slider_callback(src,evt)
        v = get(src,'Value');
        t = round(v*(dim(3)-1))+1;
        
        imagesc(d(:,:,t),clims); axis off;
        set(h_text,'String', ['t=',num2str(t)]);
        
    end

    function play_callback(src,evt)
        startT = t;
        playing = true;
        for t=startT:dim(3)
            imagesc(d(:,:,t),clims); axis off;
            set(h_text,'String', ['t=',num2str(t)]);
            set(h_slider,'Value',(t-1)/dim(3));
            pause(0.03);
            if ~playing
                break;
            end
        end
    end

    function stop_callback(src,evt)
        playing = false;
        stopT = t;
        imagesc(d(:,:,t),clims); axis off;
        set(h_text,'String', ['t=',num2str(t)]);
    end

    function analyze_callback(src,evt)
        timeWindow = eval(get(h_timeWindow,'String'));
        validateattributes(timeWindow,{'numeric'},{'vector'});
        ave_d = mean(d(:,:,timeWindow),3);
        rois = setManualRois(ave_d);
        flat_d = reshape(d,dim(1)*dim(2),dim(3));
        for iRoi = 1:numel(rois)
            ts = mean(flat_d(rois{iRoi}.idx,:),1);
            %norm_ts = ts-min(ave_d(:));
            norm_ts = (ts-min(ave_d(:)))/max(ave_d(:));
            figure('Name',sprintf('timecourse: roi%01d',iRoi));
            plot((0:numel(norm_ts)-1)/h.SI4.scanFrameRate,norm_ts);
            xlabel('sec');
            rois{iRoi}.norm_ts = norm_ts;
        end 
        out.ave_d = ave_d;
        out.rois = rois;
        out.fname = fname;
        out.frameRate = h.SI4.scanFrameRate;
    end

    function exit_callback(src,evt)
        uiresume(hf);
        done=true;
        close(hf);
    end
    
end