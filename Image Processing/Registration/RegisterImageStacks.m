function varargout = RegisterImageStacks(refImg,imgStack,varargin)
% RegisterImageStacks - Given an image stack (3D - m,n,t, or 4D - m,n,z,t),
%   and a reference stack, outputs x, y translation coordinates for each image of the
%   stack w.r.t. 1st image(3D)/stack(4D) after Fourier-based registration
% xy = RegisterImageStacks(refImg, imgStack);
% xy = RegisterImageStacks(refImg,imgStack,'processType',processType)
% [xy,imgStack_reg, refImg] = RegisterImageStacks(refImg,imgStack,'processType',processType)
% Inputs:
% imgStack -  M x N x T, or M x N x Z x T where M, N are image dimensions,
%   Z is the slice dimension for image volumes, and T is the time dimension
%   for 4D temporal stacks.
% 'processType' - Serial or parallel for type of processing.
% Outputs:
% xy - If imgStack is 3D, then xy is a 2 x (T-1) matrix, where T is the number of time points,
%   and the 1st and and 2nd rows are the x and y translation coordinates at
%   each point. If imgStack is 4D, xy is 2 x Z x (T-1)
% imgStack_reg - Registered imgStack
% refImg - Reference image. This is returned because the program accepts
%   image stacks where the initial image dimensions were mismatched. In
%   such cases the refImg returned by the program might be a zero-padded
%   version of the input refImg.
% 
% Avinash Pujala, Koyama lab/HHMI, 2016

processType = 'serial';
for tt = 1:numel(varargin)
    if ischar(varargin{tt})
        switch varargin{tt}
            case 'process'
                processType = varargin{tt+1};
        end
    end
end

imgDims = size(imgStack);
if ndims(imgStack) < 2
    error('Only a single image!')
elseif ndims(imgStack)> 4
    error('Image stack must be at most 4D!')
end

imgDims_ref = size(refImg);

%---Zero pad to equalize dimensions
if imgDims(1)~= imgDims_ref(1)
    extraPxls = imgDims(1)-imgDims_ref(1);
    if extraPxls > 0
        refImg(end:end+extraPxls,:,:,:)=0;
    else
        imgStack(end:end-extraPxls,:,:,:) = 0;
    end
end

if imgDims(2)~= imgDims_ref(2)
    extraPxls = imgDims(2)-imgDims_ref(2);
    if extraPxls > 0
        refImg(:,end:end+extraPxls,:,:)=0;
    else
        imgStack(:,end-extraPxls,:,:) = 0;
    end
end


if numel(imgDims)>2
    xy = nan([2, imgDims(3:end)]);
end

imgInds = 1:imgDims(end);

if numel(imgDims) <= 3
    [xy(1,:),xy(2,:)] = RegisterImages_3D(refImg,imgStack);  
else
    if strcmpi(processType,'serial')
        for tt = imgInds
            disp(['Time step # ' num2str(tt)])
            blah = [];
            [blah(:,1),blah(:,2)] = RegisterImages_3D(refImg,imgStack(:,:,:,tt));
            blah = blah';
            xy(:,:,tt) = blah;
        end
    elseif strcmpi(processType,'parallel')
        parfor tt = imgInds
            disp(['Time step # ' num2str(tt)])
            blah = [];
            [blah(:,1),blah(:,2)] = RegisterImages_3D(refImg,imgStack(:,:,:,tt));
            blah = blah';
            xy(:,:,tt) = blah;
        end
    end
    
end


varargout{1} = xy;

if nargout >1
    for jj = 1:size(imgStack,3)
        imgStack(:,:,jj) = circshift(imgStack(:,:,jj),-[xy(1,jj),xy(2,jj)]);
    end
    varargout{2} = imgStack;
    varargout{3} = refImg;
end

end

function [dx, dy] = RegisterImages_3D(refImg, imgStack)
% [dx,dy] = registerImagesMA(refImg,im2);
% Inputs:
% refImg - Reference image (2D) or image stack (3D)
% imgStack - Image or image stack to register with reference
% Outputs:
% dx, dy - x and y translation coordinates for image alignment.
%
% Avinash Pujala, 2017

imgDims = size(refImg);
if numel(imgDims) == 2
    [dx,dy] = RegisterImages_2D(refImg,imgStack);
elseif numel(imgDims) == 3
    [dx,dy] = deal(nan(1,imgDims(3)));
    for zz = 1:imgDims(3)
        [dx(zz),dy(zz)] = RegisterImages_2D(refImg(:,:,zz),imgStack(:,:,zz));
    end
else
    error('Incorrect image dimensions!')
end

end

function [dx,dy] = RegisterImages_2D(refImg,im2)
I1 = fft2(refImg);
I2 = double(im2);

C = ifft2(conj(I1) .* (fft2(I2)));
C = fftshift(C);
[x, y] = find(C == max(C(:)));

dx = x - round(size(im2, 1) / 2 - .5) - 1;
dy = y - round(size(im2, 2) / 2 - .5) - 1;
end