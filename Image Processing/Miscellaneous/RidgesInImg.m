function varargout = RidgesInImg(img)
%RidgesInImg When given an image, returns an image with nonzero
%   values only along ridges along the x and y dimensions respectively
% I_x = RidgesInImg(img);
% [I_x,I_y] = RidgesInImg(img);
% Inputs:
% img - Image in which to detect images
% dim - Dimension along which to find images
% Outputs:
% img_ridge - Image with nonzero values only at ridges
% 
% Avinash Pujala, Koyama lab/JRC, 2018

%--- Default values

[Gx,Gy] = gradient(img);


B = Gx(:,1:end-1)>0 & Gx(:,2:end)<=0;
B(:,end+1) = img(:,end)*0;
I_x = B.*img;

B = Gy(1:end-1,:)>0 & Gy(2:end,:)<=0;
B(end+1,:) = img(end,:)*0;
I_y = B.*img;

varargout{1} = I_x;
varargout{2} = I_y;
end

