function IM = ReadImgStack(fPath)
%ReadImgStack - Given the path to an image stack and the image ext, will
%   read the entire stack
% I = ReadImgStack(fPath,imgExt)
%
% Avinash Pujala, Koyama lab/HHMI, 2016

% if isempty(strfind(imgExt,'.'))
%     imgExt= ['.' imgExt];
% end
% imgInfo = imfinfo([fPath imgExt]);
imgInfo = imfinfo(fPath);
nImages = length(imgInfo);

rgbFlag = 0;
if strcmpi(imgInfo(1).ColorType, 'truecolor')
    IM = zeros(imgInfo(1).Height,imgInfo(1).Width,3,nImages);
    rgbFlag = 1;
else
    IM = zeros(imgInfo(1).Height,imgInfo(1).Width,nImages);
end

I = cell(nImages,1);
dispChunk = round(nImages/3);
disp('Reading images...')
for imNum = 1:nImages
    %     blah = imread([fPath imgExt], imNum);
    blah = imread(fPath,imNum);
    if rgbFlag
        IM(:,:,:,imNum) = blah;
    else
        IM(:,:,imNum) = blah;
    end
    
    I{imNum} = blah;
    if mod(imNum,dispChunk)==0
        disp(num2str(imNum))
    end
end


end

