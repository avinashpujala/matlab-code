function y = Objective_2DGauss(sigma,img,imgInd,N)
% Objective_2DGauss - An objective function used to minimize Gaussian Fit
% y = Objective_2DGauss(gaussParams,img,N);

if mod(N,2)==0
    N = N+1;
end
% mu = gaussParams(1);
% sigma = gaussParams(2);
mu= 0;
gFun = MakeGaussian(N,mu,sigma);
gFun = gFun(:)*gFun(:)';

halfLen = median(1:N);
if numel(imgInd) ==1
    [pxlCoord(2),pxlCoord(1)] = ind2sub(size(img),imgInd);
else
    pxlCoord = imgInd;
end

img_crop = CropImgsAroundPxl(img,pxlCoord,halfLen-1);

y = (Standardize(img_crop)-Standardize(gFun)).^2;
y = sum(y(:));

end

