function IM_flt = BandpassImgStack(IM,lowerLim,upperLim)
%BandpassImgStack - Bandpasses an 3D image stack
%  IM_flt = BandpassImgStack(IM,lowerLim,upperLim);
% Inputs:
% IM - 3D image stack
% lowerLim - Lower end of the bandpass
% upperLim - Upper end of the bandpass
%
% Avinash Pujala, HHMI, 2016
IM_flt = zeros(size(IM));
for imNum = 1:size(IM,3)
    IM_flt(:,:,imNum) = Bandpass(IM(:,:,imNum),lowerLim,upperLim);
end
end

function img_flt = Bandpass(img, lowerLim,  upperLim)
getCtr = @(img)[round(size(img,1)/2+0.599), round(size(img,2)/2+0.599)];
imgCtr = getCtr(img);
r_in = lowerLim;
r_out = upperLim;
se_in = strel('disk',r_in);
se_out = strel('disk',r_out);
se_in_ctr = getCtr(se_in.getnhood);
se_out_ctr = getCtr(se_out.getnhood);
mask = zeros(size(img));
inInds = se_in.getneighbors;
inInds(:,1) = inInds(:,1) + imgCtr(1);
inInds(:,2) = inInds(:,2) + imgCtr(2);
inInds = sub2ind(size(img),inInds(:,1),inInds(:,2));
outInds = se_out.getneighbors;
outInds(:,1) = outInds(:,1) + imgCtr(1);
outInds(:,2) = outInds(:,2) + imgCtr(2);
outInds = sub2ind(size(img),outInds(:,1),outInds(:,2));
mask(outInds) = 1;
mask(inInds) =0;
img_fft = fftshift(fft2(img));
img_fft = img_fft.*mask;
img_flt = ifft2(fftshift(img_fft));
end

