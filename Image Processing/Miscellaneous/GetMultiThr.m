function varargout = GetMultiThr(img,nThr,varargin)
%GetMultiThr Get multiple thresholds for image quantization
%  thr = GetMultiThr(img,nThr);
% [thr,img_quant] =  GetMultiThr(img,nThr,'tol',tolerance,...
%                       'nMaxIter',maxNumOfIterations);
% Inputs:
% img - Image to quantize and get thresholds for.
% nThr - Number of threshold or levels of quantization
% 'tol' - tolerance (see GetGlobalThr)
% 'nMaxIter' - Max # of iterations for each threshold
% 'minThr' - Minimum threshold (If next threshold level falls below this value, then skips iteration)

tol= [];
nMaxIter = [];
minThr = 0;

if nargin < 2
    error('Minimum 2 inputs required!')
end

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'tol'
                tol = varargin{jj+1};
            case 'nmaxiter'
                nMaxIter = varargin{jj+1};
            case 'minthr'
                minThr = varargin{jj+1};
        end
    end
end

thr  = zeros(nThr,1);
imgDims = size(img);
nPxls= prod(imgDims);
img_quant = zeros(imgDims);
for jj = 1:nThr
    disp(['Getting threshold # ' num2str(jj)])   
    thr0= multithresh(img,1);
    [thr(jj),img_bw] = GetGlobalThr(img,tol,nMaxIter,thr0);
    oneInds = find(img_bw==1);
    if (numel(oneInds) < nPxls) && ~isempty(oneInds)  && thr(jj)> minThr      
        img_quant(oneInds) = nThr-jj+ 1;
        img(oneInds) = 0;
    else
        disp(['Only ' num2str(jj)-1 ' levels of quantization achieved!'])
        break;
    end    
end

varargout{1} = thr;
varargout{2} = img_quant;

end

