function varargout = GetGlobalThr(img,varargin)
%GetGlobalThr  - Gets global threshold using an iterative process for image
%   binarization
% thr = GetGlobalThr(img);
% thr = GetGlobalThr(img,tol);
% thr = GetGlobalThr(img,tol,nMaxIter,thr0);
% [thr,img_bw] = GetGlobalThr(...)
% Inputs:
% img - Image for which to determine threshold
% tol - Tolerance for iterations. If the difference between a certain
%   estimated value for current and previous iteration falls below this
%   value, then iterations stop
% nMaxIter - Max # of iterations to execute if tolerance is not reached (default = 50).
% thr0 - Initial threshold
% Avinash Pujala, JRC, 2016

% thr = mean(img(:))/5;
thr = mean(img(:));
tol = 1/100;
nMaxIter = 100;
if nargin ==2
   blah = varargin{1};
   if ~isempty(blah)
       tol = blah;
   end    
elseif nargin ==3
    blah= varargin{2};
    if ~isempty(blah)
           nMaxIter = blah;
    end 
elseif nargin ==4
    blah = varargin{3};
    if ~isempty(blah)
        thr = blah;
    else
        thr = mean(img(:));
    end
end


count = 0;
thr1 = DiffThr(img,thr);
dThr = abs(thr1-thr);
while (dThr > tol) && (count < nMaxIter)
    thr = thr1;
    thr1 = DiffThr(img, thr);
    dThr = abs(thr1 - thr);
    count = count + 1;
    disp(['Iter# ' num2str(count) ' , dThr = ' num2str(dThr*100)])
end


img_bw = zeros(size(img));
img_bw(img > thr1) = 1;

varargout{1}  = thr1;
varargout{2} = img_bw;

    function thr1 = DiffThr(img,thr0)
        sub = img(img<thr0);
        supra = img(img>=thr0);
        thr1 = 0.5*(mean(sub(:)) + mean(supra(:)));        
    end

end

