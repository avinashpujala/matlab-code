function stat_dist = BootStrapStat(X,varargin)
%BootStrapStat Returns the distribution of a statistic obtained after
%   bootstrapping
% stat_dist = BootStrapStat(X);
% stat_dist = BootStrapStat(X,'nIter',nIter,'func',func,'processing',processing);
% Inputs:
% X - Data to bootstrap; size must be [nSamples, nVariables]
% nIter - Number of iterations
% func - Function that gets obtains the statistic on sampled values, e.g.,
%   @mean, @std, etc. Function must be computable along first dim by
%   default.
% 'processing' - 'serial' (default) or 'parallel'; whether to use serial or parallel
%   loops
% Outputs:
% stat_dist - Distribution of the statistic of interest, obtained after
%   bootstrapping
%
% Avinash Pujala, JRC/HHMI, 2017

nIter = 1000;
func = @mean;
processing = 'serial';
poolSize = 10;

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'niter'
                nIter = varargin{jj+1};
            case 'func'
                func = varargin{jj+1};
            case 'processing'
                processing = varargin{jj+1};
        end
    end
end

stat_dist = nan(nIter,size(X,2));
if strcmpi(processing,'parallel')
    if matlabpool('size')==0
        matlabpool(poolSize)
    end
    iterVec = 1:nIter;
    parfor ii = iterVec
        X_sample = SampleWithRep(X);
        stat_dist(ii,:) = func(X_sample)
    end
else
    for ii = 1:nIter
        X_sample = SampleWithRep(X);
        stat_dist(ii,:) = func(X_sample);
    end
end
  

end

 function X_sampled =  SampleWithRep(X)
        rInds = ceil(rand(size(X,1),1)*size(X,1));
%         rInds = ceil(rand(size(X))*size(X,1));
%         rInds = rInds(:);
%         cInds = repmat(1:size(X,2),[size(X,1),1]);
%         cInds = cInds(:);
%         X_sampled = reshape(SparseIndex(X,rInds,cInds), size(X));
        X_sampled = X(rInds,:);
    end
