function Y = ResampleWithRep(X,varargin)
%ResampleWithRep Resample a variable X with replacement
% Y = ResampleWithRep(X);
% Y = ResampleWithRep(X,nReps);
% Inputs:
% X - A vector of values to resample
% nReps - Number of repetitions.
% Outputs:
% Y - Vector of length = length(X)*nReps that results from
%   resampling with replacement of X for nRep times.
%
% Avinash Pujala, JRC, 2017

if nargin > 1
    nReps = varargin{1};
else
    nReps = 1;
end
N = length(X);
Y = zeros(length(X)*nReps,1);
for n = 1:nReps
    inds = (n-1)*N+1:(n*N);
    Y(inds) = SampleWithRep(X);
end
end

function X_sampled =  SampleWithRep(X)
rInds = ceil(rand(size(X,1),1)*size(X,1));
X_sampled = X(rInds,:);
end