function varargout = PValue(X,val,varargin)
%PValue Given a set of random variables and a value or set of values
%   returns proabilities that the variables will have those values
% pVals = PValue(X, val);
% [pVals, zScores] =
%   PValue(X,val,'valRange',valRange,'processing',processing);
% Inputs:
% X - Matrix of random variables of dims [s, v], where s = # of samples,
%   and v = # of variables
% val - Scalar or a vector of values of length = v.
% valRange - 'joint' or 'independent' (default); If 'joint', treats the variables
%   X as having a similar range of values. If 'independent' then does not
%   make this assumption and computes the histograms for each of these
%   variables separately. The latter will lead to slower computation.
% processing - 'serial' (default) or 'parallel'; Serial or parallel processing.
%   'parallel' is only applicable when valRange is 'independent'.
% Outputs:
% pVals - A vector of probability values of length = v. For instance,
%   pVals(i), is the probability the variable X(:,i) will have a value as
%   extreme as val(i).
% zScores - A vector of zscores of length = v, such that z(i) = mean(Z(:,i)-val(i))/std(Z(:,1)); 
% 
% Avinash Pujala, JRC/HHMI, 2017 

valRange = 'independent';
processing = 'serial';
poolSize = 10;
nBins = 100;

for jj = 1:numel(varargin)
    if ischar(varargin{jj})
        switch lower(varargin{jj})
            case 'processing'
                processing = varargin{jj+1};
            case 'valrange'
                valRange = varargin{jj+1};
        end
    end
end

nVariables = size(X,2);
% nSamples = size(X,1);
if (numel(val)==1) && (nVariables>1)
    val = repmat(val,[1,nVariables]);
end

val = repmat(reshape(val,1,nVariables),[nBins,1]);

if strcmpi(valRange,'joint')
    [p,v] = hist(X,100);
    v = repmat(v(:),[1,nVariables]);
else
    [p,v] = deal(nan(100,nVariables));
    if strcmpi(processing,'parallel')
        if matlabpool('size')==0
            matlabpool(poolSize)
        end
        foo = 1:nVariables;
        parfor jj = foo
            [p(:,jj),v(:,jj)] = hist(X(:,jj),nBins);
        end        
    else
        for jj = 1:nVariables
            [p(:,jj),v(:,jj)] = hist(X(:,jj),nBins);
        end
    end
end

p = p./repmat(sum(p,1),size(p,1),1);
p_cum = cumsum(p,1);

[~,inds] = min(abs(val-v),[],1);
colInds = 1:nVariables;

p_lefts = SparseIndex(p_cum,max(inds-1,1),colInds);
p_atInd = SparseIndex(p,inds,colInds);
p_lefts = p_lefts + 0.5*p_atInd;
p_rights = 1-p_lefts;
pVals = min(p_lefts,p_rights);
% pVals = SparseIndex(p,inds,colInds);
% overBool = pVals >0.5;
% pVals(overBool) = 1-pVals(overBool);

zScores = (mean(X,1)-val(1,:))./std(X,[],1);

varargout{1} = pVals;
varargout{2} = zScores;

end

