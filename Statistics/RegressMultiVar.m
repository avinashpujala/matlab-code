
B = {};
B_int = {};
Rsq = {};
R_int = {};
tic
for cellInd = 1:size(R.Y,2);
    disp(['Cell ' num2str(cellInd)])
    [B{cellInd},B_int{cellInd},R_sq{cellInd},R_int{cellInd}] = regress(R.Y(:,cellInd),R.X);
end
toc
B = cell2mat(B);
B = B';
B_int = cell2mat(B_int);
R_sq = cell2mat(R_sq);
R_int = cell2mat(R_int);