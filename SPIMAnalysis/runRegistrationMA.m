
baseDirectory       = 'Z:\Minoru\2013-09-10\Timeseries_20130910_134808';
cameraIndex         = 0;
timeRange           = [0 588];
referenceTimePoints = 290:300;
warpingSettings     = [100 150 6];
forceOverwrite      = 0;
imSize=[2048 1084 51];

poolWorkers         = 12;

%% processing loop

if matlabpool('size') > 0
    matlabpool('close');
end;
matlabpool(poolWorkers);

for t = referenceTimePoints
    disp(['reading data at time point ' num2str(t)]);
    
    timeString = [repmat('0', 1, 5 - length(num2str(t))) num2str(t)];    
    stackName = [baseDirectory '/' 'TM' timeString '_CM0_CHN00.stack'];
        
    currentStack=double(read_LSstack_fast1(stackName,imSize(1:2)));
    
    if t == referenceTimePoints(1)
        stackAverage = currentStack;
    else
        stackAverage = stackAverage + currentStack;
    end
end

stackAverage = stackAverage / length(referenceTimePoints);


ROIs=set_LS_ROIs(stackAverage);

slices=ROIs.zrange(1):ROIs.zrange(2);
zSize = numel(slices);
xrange=ROIs.xrange(1):ROIs.xrange(2);
yrange=ROIs.yrange(1):ROIs.yrange(2);


parfor z = slices
%%for z = slices
    registerStacksMA_fast(z, baseDirectory, stackAverage(:,:,z), warpingSettings, imSize,timeRange, xrange, yrange);
end;

if matlabpool('size') > 0
    matlabpool('close');
end

%%
save(fullfile([baseDirectory, '/registered/'],'ROIs.mat'),'ROIs');