% Misha B. Ahrens and Philipp J. Keller, HHMI/Janelia Farm, 2012-2013
% Email: ahrensm@janelia.hhmi.org, kellerp@janelia.hhmi.org

function [dx, dy] = registerImagesMA(im1_buf, im2)

I2 = double(im2);

C = ifft2(conj(im1_buf) .* (fft2(I2)));
C = fftshift(C);
[x, y] = find(C == max(C(:)));

dx = x - round(size(im2, 1) / 2 - .5) - 1;
dy = y - round(size(im2, 2) / 2 - .5) - 1;