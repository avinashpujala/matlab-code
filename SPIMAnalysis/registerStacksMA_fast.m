% Misha B. Ahrens and Philipp J. Keller, HHMI/Janelia Farm, 2012-2013
% Email: ahrensm@janelia.hhmi.org, kellerp@janelia.hhmi.org

function registerStacksMA_fast(z, baseDirectory, referencePlane, warpingSettings, imSize, timeRange,xrange,yrange)


outputName = [baseDirectory '/registered/Plane' num2str(z, '%.2d') '.stack'];

referencePlane=referencePlane(yrange,xrange);
reference_struct=make_warp_ref(referencePlane, warpingSettings);


ref_buf=fft2(referencePlane);



for t= timeRange(1):timeRange(2)
    
    disp(['processing time point ' num2str(t)]);
    timeString = [repmat('0', 1, 5 - length(num2str(t))) num2str(t)];
    stackName = [baseDirectory '/' 'TM' timeString '_CM0_CHN00.stack'];

    currentPlane = double(read_LSstack_fast3(stackName,imSize(1:2),z));
    currentPlane = currentPlane(yrange,xrange);
    
    [dx, dy] = registerImagesMA(ref_buf, currentPlane);
    if dx > 0 && dy > 0
        currentPlane(1:(end - dx), 1:(end - dy)) = currentPlane((1 + dx):end, (1 + dy):end);
    elseif dx > 0 && dy < 0
        currentPlane(1:(end - dx), (1 - dy):end) = currentPlane((1 + dx):end, 1:(end + dy));
    elseif dx < 0 && dy > 0
        currentPlane((1 - dx):end, 1:(end - dy)) = currentPlane(1:(end + dx), (1 + dy):end);
    elseif dx < 0 && dy < 0
        currentPlane((1 - dx):end, (1 - dy):end) = currentPlane(1:(end + dx), 1:(end + dy));
    end;

    plane_Corrected = warpImagesMA(reference_struct, currentPlane, warpingSettings);

    write_LSstack_fast1(outputName,uint16(squeeze(plane_Corrected)));

end