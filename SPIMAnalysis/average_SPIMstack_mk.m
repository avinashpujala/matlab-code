

root_dir='Z:\Minoru\2013-09-10\Timeseries_20130910_134808\registered2';

timepoints=1:560;
fcycle = 40; %% 20 stack high gain, 20 stack low gain
Zrange=1:51;%%51;

load(fullfile(root_dir,'ROIs.mat'));

%%
dim=[ROIs.yrange(2)-ROIs.yrange(1)+1, ROIs.xrange(2)-ROIs.xrange(1)+1,Zrange(end)-Zrange(1)+1];
ave_img=zeros(dim(1), dim(2), dim(3), fcycle);

for z=Zrange
    disp(num2str(z));
    stack=read_LSstack_fast1(fullfile(root_dir,['Plane',num2str(z,'%.2d'),'.stack']),[dim(1) dim(2)]);
    
    for i=0:fcycle-1
        inds=find(mod(timepoints,fcycle)==i);
        ave_img(:,:,z,i+1)=ave_img(:,:,z,i+1)+reshape(sum(double(stack(:,:,timepoints(inds))),3),[dim(1) dim(2) 1 1]);
    end
    
    ave_img(:,:,z,:)=ave_img(:,:,z,:)./(length(timepoints)/fcycle);
end


%% calculate baseline
    
tot_ave=mean(ave_img,4);
maskimg=zeros(dim(1),dim(2),dim(3),3);

for i=1:dim(3)
    maskimg(:,:,i,:)=repmat(imNormalize99(tot_ave(:,:,i)),[1 1 1 3]);
end
%%
baseline_t=sort(ave_img,4,'ascend');
baseline=mean(baseline_t(:,:,:,1:10),4); %% bottom 25 %

avg_highGain = (mean(ave_img(:,:,:,11:20),4)./baseline)-1;
avg_lowGain  = (mean(ave_img(:,:,:,31:40),4)./baseline)-1;

highGainMaskInds = repmat(find(avg_highGain>0.05),[1 3]);
lowGainMaskInds  = repmat(find(avg_lowGain >0.05),[1 3]);
highGainMaskInds(:,2)=highGainMaskInds(:,2)+dim(1)*dim(2)*dim(3);
highGainMaskInds(:,3)=highGainMaskInds(:,3)+dim(1)*dim(2)*dim(3)*2;
lowGainMaskInds(:,2)=lowGainMaskInds(:,2)+dim(1)*dim(2)*dim(3);
lowGainMaskInds(:,3)=lowGainMaskInds(:,3)+dim(1)*dim(2)*dim(3)*2;

red_maskinds  = zeros(length(highGainMaskInds),3);
cyan_maskinds = zeros(length(lowGainMaskInds),3);

red_maskinds(:,1)=1;
cyan_maskinds(:,2)=1;
cyan_maskinds(:,3)=1;

%%
maskimg(highGainMaskInds(:))=red_maskinds(:);
maskimg(lowGainMaskInds(:))=cyan_maskinds(:);

writetiff8(maskimg,fullfile(root_dir,'result.tif'));









