clear all;close all;

input_dir='Y:\Takashi\SPIM\10132013g\Fish1-1\corrected\Registered';

load(fullfile(input_dir,'cell_info.mat'));
ave=readtiff(fullfile(input_dir,'ave.tif'));

numcell=length(cell_info);
dim=[size(ave,2) size(ave,1)];

zplane_past=1;
outputName = [input_dir, '/Plane' num2str(zplane_past, '%.2d') '.stack'];
stack=read_LSstack_fast1(outputName,dim);
totlen=size(stack,3)-1;


cell_resp=zeros(numcell,totlen,'single');

for i=1:numcell
    
    if mod(i,100)==0;disp(i);end
    
    zplane=cell_info(i).slice;
    
    if(zplane_past~=zplane)        
        outputName = [input_dir, '/Plane' num2str(zplane, '%.2d') '.stack'];
        stack=read_LSstack_fast1(outputName,dim);
    end        
    
    inds=cell_info(i).inds;
    
    tmp_matrix=zeros(1,totlen,'single');
    for j=1:length(inds)
        indt=inds(j)+(0:dim(1)*dim(2):dim(1)*dim(2)*(totlen-1));
        tmp=stack(indt);
        tmp_matrix=tmp_matrix+single(tmp);
    end
    
    cell_resp(i,:)=tmp_matrix/length(single(inds));  

    zplane_past=zplane;
end


%%
cell_resp2=zeros(size(cell_resp),'single');

for i=1:numcell
    
    tcourse=cell_resp(i, :)';
    tcourse_lc = tcLowCut(tcourse, 450,'gaussian',1);
    temp = sort(tcourse_lc,'ascend');
    tcourse_lc= tcourse_lc ./ mean(temp(1:round(length(temp)/3)));
    
    cell_resp2(i,:) = tcourse_lc;
end


%%
cell_resp_dim=size(cell_resp);
write_LSstack_fast_float(fullfile(input_dir,'cell_resp.stackf'),cell_resp);
write_LSstack_fast_float(fullfile(input_dir,'cell_resp_lowcut.stackf'),cell_resp2);
save(fullfile(input_dir,'cell_resp_dim.mat'),'cell_resp_dim');
