function out=translateImage2D(img,y,x)

dim=size(img);
out=zeros(dim(1),dim(2));

xmin=max([1 x+1]);xmax=min([dim(2)+x dim(2)]);
ymin=max([1 y+1]);ymax=min([dim(1)+y dim(1)]);
    out(ymin:ymax,xmin:xmax)=img(ymin-y:ymax-y,xmin-x:xmax-x);
end


