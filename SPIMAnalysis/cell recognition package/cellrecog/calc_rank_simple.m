function out=calc_rank_simple(im, radius,mask)

dim=size(im);

dimp=dim+2*radius;

imp=zeros(dimp);
oop=zeros(dimp);
mmp=zeros(dimp);

imp(radius+1:end-radius,radius+1:end-radius)=im;
mmp(radius+1:end-radius,radius+1:end-radius)=mask;
oop(radius+1:end-radius,radius+1:end-radius)=1;

inds_ori=find(mask);

inds=find(mmp);

mdisk=makeDisk2(radius,radius*2+1);

[r1, c1]=find(mdisk);
r1=r1-radius-1;
c1=c1-radius-1;

moveinds=r1*dimp(1)+c1;

tempones=double(maskones2D_mex(int32(size(im)),int32(mdisk),int32(size(mdisk))))';
tempones2=tempones(inds_ori);

out=zeros(size(im));
out(inds_ori)=double(localrank_2D_simple_mex(single(imp),int32(oop),int32(inds),int32(moveinds)))./tempones2;

