function out=imNormalize95(img)

temp=sort(img(:),'descend');
threshold=temp(round(length(temp)/20));
out=(double(img)-temp(end))./(threshold-temp(end));
out(out>1)=1;