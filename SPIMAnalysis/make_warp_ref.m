function out =make_warp_ref(reference,warpingSettings)

pointDensity = warpingSettings(1);
squareSize   = warpingSettings(2);

jVector = (squareSize + 1):pointDensity:(size(reference, 1) - squareSize);
kVector = (squareSize + 1):pointDensity:(size(reference, 2) - squareSize);
q=1;

for j = jVector
    for k = kVector
        imageSquareReference = reference((j - squareSize):(j + squareSize), (k - squareSize):(k + squareSize));
        out(q).ref_fft = fft2(double(imageSquareReference));
        q=q+1;
        %%
    end
end