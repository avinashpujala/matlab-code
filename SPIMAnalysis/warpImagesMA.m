% Misha B. Ahrens and Philipp J. Keller, HHMI/Janelia Farm, 2012-2013
% Email: ahrensm@janelia.hhmi.org, kellerp@janelia.hhmi.org

function correctedImage = warpImagesMA(reference_struct, input, warpingSettings)

pointDensity = warpingSettings(1);
squareSize   = warpingSettings(2);
maximumShift = warpingSettings(3);

jVector = (squareSize + 1):pointDensity:(size(input, 1) - squareSize);
kVector = (squareSize + 1):pointDensity:(size(input, 2) - squareSize);
dxArray = zeros(1, length(jVector) * length(kVector));
dyArray = zeros(1, length(jVector) * length(kVector));

q = 0;
for j = jVector
    for k = kVector
        q = q + 1;
        imageSquare = input((j - squareSize):(j + squareSize), (k - squareSize):(k + squareSize));
        I2 = imageSquare;
        C = ifftshift(ifft2(reference_struct(q).ref_fft .* conj(fft2(I2))));
        [dx, dy] = find(C == max(C(:)));
        dx = dx - squareSize - 2;
        dy = dy - squareSize - 2;
        
        if length(dx) ~= 1
            dx = 0;
            dy = 0;
        end;
        dxArray(q) = dx;
        dyArray(q) = dy;
    end;
end;

dxArray = dxArray .* (abs(dxArray) < maximumShift);
dyArray = dyArray .* (abs(dyArray) < maximumShift);

xShiftInterpolated = zeros(size(input,1),size(input,2));
yShiftInterpolated = zeros(size(input,1),size(input,2));

q = 0;
for j = jVector
    for k = kVector
        q = q + 1;
        xRange = max(1, j - squareSize):min(size(input, 1), j + squareSize);
        yRange = max(1, k - squareSize):min(size(input, 2), k + squareSize);
        
        xMaxShiftInterpolatedSquare = xShiftInterpolated(xRange, yRange);
        xNewShiftInterpolatedSquare = dxArray(q)*ones(size(xMaxShiftInterpolatedSquare));
        yMaxShiftInterpolatedSquare = yShiftInterpolated(xRange, yRange);
        yNewShiftInterpolatedSquare = dyArray(q)*ones(size(yMaxShiftInterpolatedSquare));
        
        xFinalShiftInterpolatedSquare = (abs(xMaxShiftInterpolatedSquare) >= abs(xNewShiftInterpolatedSquare)) .* xMaxShiftInterpolatedSquare + ...
            (abs(xMaxShiftInterpolatedSquare) < abs(xNewShiftInterpolatedSquare)) .* xNewShiftInterpolatedSquare;
        yFinalShiftInterpolatedSquare = (abs(yMaxShiftInterpolatedSquare) >= abs(yNewShiftInterpolatedSquare)) .* yMaxShiftInterpolatedSquare + ...
            (abs(yMaxShiftInterpolatedSquare) < abs(yNewShiftInterpolatedSquare)) .* yNewShiftInterpolatedSquare;
        
        xShiftInterpolated(xRange, yRange) = xFinalShiftInterpolatedSquare;
        yShiftInterpolated(xRange, yRange) = yFinalShiftInterpolatedSquare;
    end;
end;

noShiftVector = (1:numel(input))';
xShiftVector = xShiftInterpolated(:);
yShiftVector = yShiftInterpolated(:) * size(input, 1);
xyShift = noShiftVector - xShiftVector - yShiftVector;
imageVector = input(:);
xyShift = xyShift .* (xyShift >= 1) .* (xyShift <= length(noShiftVector)) + noShiftVector .* (xyShift < 1) + noShiftVector .* (xyShift > length(noShiftVector));
correctedImageVector = imageVector(xyShift);
correctedImage = reshape(correctedImageVector, size(input, 1), size(input, 2));