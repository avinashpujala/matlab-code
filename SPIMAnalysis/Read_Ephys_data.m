clear all; close all;

pathname='Y:\Minoru\2013-10-04\f1s1_20131004_114405\ephys';  %% ahrensm-ws1/data1/ drive
filename='f1s1.10chFlt';

[~,fname,~]=fileparts(filename);
outdir=[pathname,'/',fname,'/'];
%mkdir(outdir);

ch1use=1;
ch2use=1;

%%  reading 10chFLT file
%   10-channel of floating point values are written sequentially

h = fopen(fullfile(pathname,filename));
X = fread(h,'float');
fclose(h);

rawdata=struct;

rawdata.ch1 = X(1:10:end);  %% channel 1
rawdata.ch2 = X(2:10:end);  %% channel 2
rawdata.ch3 = X(3:10:end);  %% pulse from the LS microscope: 4V at the start of stack, 3.5V at each plane;
rawdata.stimParam4 = X(4:10:end);  %% Memory Experiment Parameter
rawdata.ch2aux = X(5:10:end);      %% Not used 
rawdata.ch_12lowPass = X(6:10:end);%% Not used
rawdata.stimParam3 = X(7:10:end);  %% Stimulation type (When Gain1 =1, When Gain2 =2)
rawdata.stimID = X(8:10:end);      %% Not used
rawdata.stimParam1 = X(9:10:end);  %% Velocity of the grating
rawdata.stimParam2 = X(10:10:end); %% Gain Setting 
len = length(rawdata.ch1);


%% detecting swim bouts

swimdata=kickassSwimDetect01(rawdata.ch1,rawdata.ch2); 

%%

ax(1)=subplot(3,1,1);
jplot(swimdata.fltCh1,'r');hold on;
jplot(swimdata.fltCh2,'b'); hold off;
title('Filtered swim signal');

ax(2)=subplot(3,1,2);
jplot(rawdata.stimParam2,'r','linewidth',2);ylim([0 0.1]);hold on;
plot(swimdata.swimStartT/10,'b');hold off;
title('Red=Gain setting, Blue=detected swims');


ax(3)=subplot(3,1,3);
jplot(rawdata.ch3,'r','linewidth',2);
title('lightsheet pulse');
linkaxes(ax,'x');
%%  


% calculate swim frequency difference
endtime=find(rawdata.ch3>3,1,'last');  %% when lightsheet pulse ends
swimStartT2  = swimdata.swimStartT(1:endtime);
stimParam33  = rawdata.stimParam3(1:endtime);

highGainSwimFreq=mean(swimStartT2(stimParam33==1));
lowGainSwimFreq=mean(swimStartT2(stimParam33==2));

% calculate swim power difference

sinds=find(swimdata.swimStartIndT<endtime);
spowers=zeros(1,2);

for i=1:length(sinds)
   
    stim = rawdata.stimParam3(swimdata.swimStartIndT(sinds(i)));
    sspan= swimdata.swimStartIndT(sinds(i)):swimdata.swimEndIndT(sinds(i));

    P1= ch1use*sum(swimdata.fltCh1(sspan)-swimdata.back1(sspan));
    P2= ch2use*sum(swimdata.fltCh2(sspan)-swimdata.back2(sspan));
    
    spowers(stim) = spowers(stim) + P1 + P2;
end

spowers(1)=spowers(1)/sum(stimParam33==1);
spowers(2)=spowers(2)/sum(stimParam33==2);
    
    
figure;
subplot(1,2,1);
bar([highGainSwimFreq lowGainSwimFreq]);
title('Comparison of swim frequency');
set(gca,'XTickLabel',{'High Gain','Low Gain'});
ylabel('Relative Swim Frequency');


subplot(1,2,2);
bar(spowers);
title('Comparison of swim power');
set(gca,'XTickLabel',{'High Gain','Low Gain'});
ylabel('Relative Swim power');










