function Y = tempFun(X,img)
%UNTITLED2 Summary of this function goes here
% y = tempFun(X,img);

% [m,theta,T(1),T(2)] = deal(X(1),X(2),X(3),X(4));
[theta,T(1),T(2)] = deal(X(1),X(2),X(3));
m = 1.5;
f = 100;
x_scale = 60;
y_scale = 5;
x = Standardize(logspace(0,0.05,1e4));
% x = Standardize(linspace(0,1,1e3));
S = sin(2*pi*f*x);
L = m*x;

y = S.*L;

midPt = ceil(size(img)*0.5);
y = [y(:),x(:)];
y(:,2) = round(y(:,2)*x_scale)+ 1 + midPt(1);

y(:,1) = y(:,1)*y_scale;
y(:,1) = round(y(:,1)-min(y(:,1))) + 1 + midPt(2);

% y = RotateTraj(y,theta);
% midPt = ceil(size(img)*0.5);
% y(:,1)= ceil(y(:,1));
% y(:,2) = ceil(y(:,2));

% y(y<1) =1;
% y(y>size(img,2),1)=size(img,2);
% y(y>size(img,1),2)=size(img,1);

try
    funInds = unique(sub2ind(size(img),y(:,1),y(:,2)));
catch
   funInds = unique(sub2ind(size(img),y(:,1),y(:,2)));
end
allInds = find(ones(size(img)));
nonFunInds = setdiff(allInds,funInds);

foo = circshift(img,round([T(2) T(1)]));
foo = imrotate(foo,theta,'crop');

% funSum = mean(foo(funInds));
% 
% nonFunSum = mean(foo(nonFunInds));

Y = foo; 
Y(funInds) = -0.5;

% blah = foo(imgInds);
% ker1 = MakeGaussian(30,0,1);
% ker2 = MakeGaussian(30,0,4);
% ker= ker1-ker2;
% ker = ker(:)*ker(:)';
% temp = zeros(size(foo));
% temp(imgInds) = 1;
% temp = conv2(temp,ker,'same');
% blah = foo.*temp;
% blah = blah(imgInds);

% Y = (numel(x)-sum(blah(:))).^2;

end

