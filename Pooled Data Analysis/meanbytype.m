function m = meanbytype(data,colNum1, colNum2)
% Takes as input the matrix 'data' and returns a matrix 'm' with fewer rows
% such that the all redundant values in colNum1 of 'data' are removed and
% all the values in colNum2 with the same values in colNum1 are averaged
% and become the second column

uniqueCol = unique(data(:,colNum1));
ulen = length(uniqueCol);

mat = zeros(ulen,3);
for jj = 1:ulen
    idx = find(data(:,colNum1)==uniqueCol(jj));
    mat(jj,2) = mean(data(idx,colNum2));
    mat(jj,3) = std(data(idx,colNum2));
end
mat(:,1) = uniqueCol(:);
m = mat; 

