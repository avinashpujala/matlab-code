function subLists = SubListsFromList(supraList,nItems)
%SubListsFromList Given a list of items (a vector, or a cell array)
%   returns specified number of sublists
% subLists = SublistsFromList(supraList,nSubs);
% Inputs:
% supraList - List to sublist
% nItems - Number of items in each sublist, except the last one if the list
%   size does not evenly divide into nItems
% Outputs:
% subLists - The sublists (cell array) obtained from the input list
% 
% Avinash Pujala, Koyama lab/JRC, 2018

supraList = squeeze(supraList);
dims = size(supraList);
if numel(dims)==2 && any(dims==1)
    nSubs  = ceil(numel(supraList)/nItems);    
    supraList = supraList(:);
else
   nSubs = ceil(size(supraList,1)/nItems);
end


subLists = cell(nSubs,1);
count = 0;
for jj = 1:nItems:length(supraList)
    count = count +1;
    lastInd = min(jj+nItems-1,length(supraList));
    subLists{count} = supraList(jj:lastInd,:);   
end

end

