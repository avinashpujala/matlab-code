function y = MakeGaussian(x,varargin)
% MakeGaussian When given an range of values returns a Gaussian function over
%   those values with specified input parameters 
% y = MakeGaussian(x);
% y = MakeGaussian(x,mu,sigma);
% Inputs:
% x - Domain of the Gaussian function. X can be a vector or a scalar. If
%   x is scalar, then Gaussian generated is a set of x values centered around 
%   the mean
% mu - Mean of the Gaussian (Default = 0);
% sigma - Standard deviation of the Gaussian
% 
% Avinash Pujala, Koyama lab/JRC, 2018

%Default parameters
mu = 0;
sigma = 1;

if nargin > 1
    mu = varargin{1};    
end
if nargin > 2
    sigma = varargin{2};
end

if isscalar(x)
    x = (1:x);
    x = x-median(x)+mu; 
end
sigma = sigma*numel(x)/6;
y = (1/(sqrt(2*pi)*sigma))*exp(-(1/2)*((x-mu)/sigma).^2);

end

