%% Read inputs
xlsDir = 'S:\Avinash\Ablations and behavior\Alx_eyeMovts';
xlsFile = 'DataPaths_imgs.xlsx';

xlsPath = fullfile(xlsDir, xlsFile);

fprintf(['File path: \n'])
disp(xlsPath)

%% Read xls data
[~, ~, foo]  = xlsread(xlsPath);
T = cell2table(foo(2:end,:),'VariableNames',foo(1,:)); % Convert to table
T

%% Processing parameters
fps = 300;
nBlocks = 4; % # of blocks in trl
nFramesInTrl = 300*60;

procData = ProcessFishImages_180629(T.Path(9:end),'nBlocks',nBlocks,...
    'nFramesInTrl',nFramesInTrl,'fps',fps);


