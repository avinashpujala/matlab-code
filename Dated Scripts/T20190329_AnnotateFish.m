
%% Inputs
imgDir = 'F:\Avinash\Ablations & Behavior\Machine Learning\Training set_20190330\images';

[imgDir_oneUp,~,~] = fileparts(imgDir);
maskDir = fullfile(imgDir_oneUp,['masks']);

if exist(maskDir)~=7
    mkdir(maskDir)
end

%% Read image names
imgNames = GetFilenames(imgDir,'ext','bmp');

%% Draw ROIS and save masks
nImgs = length(imgNames);
for iNum = 1:length(imgNames)
    img = imread(fullfile(imgDir,imgNames{iNum}));
    roi = SetFreehandRois(img,'clrMap','viridis','figTitle',['Img # ' ...
        num2str(iNum) '/' num2str(nImgs)]);
    mask = logical(zeros(size(img)));
    for roiNum = 1:length(roi)
        mask = mask | roi{roiNum}.bw;
    end
    imwrite(mask,fullfile(maskDir,imgNames{iNum}),'bmp');
end

%%
