

%% Inputs
dir_train =  'Y:\Avinash\Head-fixed tail free\20190612_head and tail stim\f2\e01_tailStim_polNeg\Autosave0_[00-11-1c-f1-75-10]_20190612_051433_PM';

%% Make directory for masks

dir_mask = fullfile(fileparts(dir_train),'imgs_mask');
if exist(dir_mask,'dir')~=7
    mkdir(dir_mask)
end

%% Read image paths
imgNames = GetFilenames(dir_train,'ext','bmp');
imgPaths = cell(length(imgNames),1);
for iImg = 1:length(imgNames) 
    imgPaths{iImg} = fullfile(dir_train, imgNames{iImg});
end

%% Draw ROIS and save masks
nImgs = length(imgPaths);
for iPath = 1:length(imgPaths)
    imgPath_ = imgPaths{iPath};
    img = imread(imgPath_);  
    roi = SetFreehandRois(img,'clrMap','viridis','figTitle',['Img # ' ...
        num2str(iPath) '/' num2str(nImgs)]);
    mask = logical(zeros(size(img)));
    for roiNum = 1:length(roi)
        mask = mask | roi{roiNum}.bw;
    end
    [~,imgName] = fileparts(imgPath_);
    imgName = [imgName '.bmp'];
    imwrite(mask,fullfile(dir_mask,imgName),'bmp');
end


%%
