

%% 
imgDirs = {'S:\Avinash\Ablations and behavior\Alx_eyeMovts\Behavior Rig\20190513_v2a ablation in R3\f1_ctrl\images';
    'S:\Avinash\Ablations and behavior\Alx_eyeMovts\Behavior Rig\20190513_v2a ablation in R3\f2_abl\images';
    'S:\Avinash\Ablations and behavior\Alx_eyeMovts\Behavior Rig\20190513_v2a ablation in R3\f2_abl\images'};

%% Inputs

maskDir = 'S:\Avinash\Ablations and behavior\Alx_eyeMovts\Behavior Rig\20190513_v2a ablation in R3\training_masks'


if exist(maskDir)~=7
    mkdir(maskDir)
end

%% Read image paths
nImages = 100;
nImagesInDir = ceil(nImages/length(imgDirs));
imgPaths = cell(length(imgDirs),1);
for jj = 80:length(imgDirs)
    imgNames = GetFilenames(imgDirs{jj},'ext','bmp');
    rp = randperm(length(imgNames));
    rp = rp(1:nImagesInDir);
    blah = cell(length(rp),1);
    for kk = 1:length(rp)
        ind_now = rp(kk);
        blah{kk} = fullfile(imgDirs{jj}, imgNames{ind_now});
    end
    imgPaths{jj} = blah;
end

%%  Consolidate image paths

imgPaths_all = [];
for iDir = 1:length(imgPaths)
    imgPaths_all = [imgPaths_all; imgPaths{iDir}];
end

%% Draw ROIS and save masks
nImgs = length(imgPaths_all);
for iPath = 1:length(imgPaths_all)
    imgPath_ = imgPaths_all{iPath};
    img = imread(imgPath_);  
    roi = SetFreehandRois(img,'clrMap','viridis','figTitle',['Img # ' ...
        num2str(iPath) '/' num2str(nImgs)]);
    mask = logical(zeros(size(img)));
    for roiNum = 1:length(roi)
        mask = mask | roi{roiNum}.bw;
    end
    [~,imgName] = fileparts(imgPath_);
    imgName = [imgName '.bmp'];
    imwrite(mask,fullfile(maskDir,imgName),'bmp');
end

%% Also save images
[trainingDir,~,~] = fileparts(maskDir);
trainingDir = fullfile(trainingDir,'training_images');
if exist(trainingDir)~=7
    mkdir(trainingDir)
end
for iPath = 1:length(imgPaths_all)
    imgPath_ = imgPaths_all{iPath};
    img = imread(imgPath_);
    
    [~,imgName] = fileparts(imgPath_);
    imgName = [imgName '.bmp'];
    
    imwrite(img, fullfile(trainingDir,imgName),'bmp');
end


%% Draw ROIS and save masks
% % % nImgs = length(imgNames);
% % % for iNum = 1:length(imgNames)
% % %     img = imread(fullfile(imgDir,imgNames{iNum}));
% % %     roi = SetFreehandRois(img,'clrMap','viridis','figTitle',['Img # ' ...
% % %         num2str(iNum) '/' num2str(nImgs)]);
% % %     mask = logical(zeros(size(img)));
% % %     for roiNum = 1:length(roi)
% % %         mask = mask | roi{roiNum}.bw;
% % %     end
% % %     imwrite(mask,fullfile(maskDir,imgNames{iNum}),'bmp');
% % % end

%%
