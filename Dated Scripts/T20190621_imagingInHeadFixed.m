
%% Add relevant paths
tic
disp('Adding relevant paths...')
% addpath(genpath('V:\Code\General'));
% addpath(genpath('V:\Code\GeciAnalysis'));
addpath(genpath('V:\Code\EphysReader'));
% addpath('V:\Code\ExperimentSummary');
% addpath('V:\Code\EphysAnalysis');
% addpath('V:\Code\MotorAdaptation'); % swimCharacterize, swimCharacterize2 also exist
addpath(genpath('V:\Code\ImagingAnalysis'));
addpath('V:\Code\ImagingAnalysis\bfmatlab'); % Required for readSI2016Tiff.m
javaaddpath('V:\Code\ImagingAnalysis\bfmatlab\bioformats_package.jar');
javaaddpath('V:\Code\ImagingAnalysis\bfmatlab\bioformats_package(2).jar');
javaaddpath('C:\Program Files\ImageJ\ij.jar'); % This needs to be run to read SI2015 tif
toc


%% Inputs

dir_ca = 'Y:\Avinash\Head-fixed tail free\201906714_full expt\f1\t01_ca';
file_bas = 't01_bas.16ch';

%%
dir_bas = fileparts(dir_ca);

fileList_ca = GetFilenames(dir_ca,'ext','tif');


%% Read bas

bas = mkEphysLoad(fullfile(dir_bas, file_bas));