step = [190 0 0 0]; % x y (z stage) z piezo in um
nStep = 2; % number of stitches
startPosition = hSI.hMotors.motorPosition;

for i  = 1:nStep
    disp(['moving to stitch #', num2str(i)]);
    hSI.hMotors.motorPosition = startPosition + (i-1)*step;
    pause(1);
    disp(['current position: ', num2str(hSI.hMotors.motorPosition)]);
    hSI.startGrab()
    disp(['grabbing stich #', num2str(i)]);
    while strcmp(hSI.acqState, 'grab')
        pause(1);
    end
end
    