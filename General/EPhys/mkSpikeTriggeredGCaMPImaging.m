zstep = 5; % z interval in um
nStep = 4; % number of steps
nTrial = 5; % number of trials
startPosition = hSI.hMotors.motorPosition;

for iTrial = 1:nTrial
    disp(['trial #', num2str(iTrial)]);    
    for iStep  = 1:nStep
        pause(10);
        disp([' moving to slice #', num2str(iStep)]);
        hSI.hMotors.motorPosition = startPosition + [0 0 0 (iStep-1)*zstep];
        pause(1);
        disp([' current position: ', num2str(hSI.hMotors.motorPosition)]);
        hSI.startGrab()
        disp([' grabbing trial #', num2str(iTrial), ' slice #', num2str(iStep)]);
        while strcmp(hSI.acqState, 'grab')
            pause(1);
        end
    end
end    

disp('moving to slice # 1');
hSI.hMotors.motorPosition = startPosition;
pause(1);
disp(['current position: ', num2str(hSI.hMotors.motorPosition)]);