function data = ConcatenateDataStructsFromEphys(varargin)
% ConcatenateDataStructsFromEphys - Interatively selects multiple ephys
%   files (.16ch, loaded with mkEphysLoad) and concatenates the data in
%   identical fields
% data = ConcatenateDataStructsFromEphys();
% data = ConcatenateDataStructsFromEphys(fileDir);

ext = '.16ch';

cp = cd;
if nargin ==1
    cd(varargin{1})
end
ext = ['*' ext];
[fn,pn] = uigetfile(ext,'Select files to load and concatenate','MultiSelect','on');

data = struct;
count = 0;
for ff = 1:length(fn)
    count = count +1;
    foo = mkEphysLoad(fullfile(pn,fn{ff}));
    fldNames = fieldnames(foo);
    for nn = 1:length(fldNames)
        if ischar(foo.(fldNames{nn}))
            data.(fldNames{nn}){count} = foo.(fldNames{nn});
        else
            if ff ==1
                data.(fldNames{nn}) = foo.(fldNames{nn});
            else
                data.(fldNames{nn}) = [data.(fldNames{nn}), foo.(fldNames{nn})];
            end
        end
        
    end
end
dt = data.t(2)-data.t(1);
t0 = data.t(1);
data.t = (0:length(data.t)-1)*dt + t0;

cd(cp)
end

