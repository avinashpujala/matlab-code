function output = normalize(input, range);
%% 

% (A - min) for slide minimum value to 0
% (A - min)/(max-min) for normalize all value between 0 and 1
% (A - min)/(max-min)*2 for normalize all value between 0 and 2
% (A - min)/(max-min)*2 - 1 for normalize all value between -1 and 1
% Masashi Tanimoto, 11/3/2016
%% 

A = input;
dim = size(A);
normMin = range(1);
normMax = range(2);
if numel(dim)==2
    nA = size(A,1);
    minA = min(input,[],1);
    maxA = max(input,[],1);
    if range==[0 1]
        % (A - min)/max, max = max-min
        output = (A - repmat(minA,nA,1))./repmat(maxA-minA,nA,1);
    else
        
    end
elseif numel(dim)==3
    
    
    
    
elseif numel(dim)==4
    
    
    
    
    
elseif numel(dim)>4
    display('Dimension of the input is > 4, not allowed to be processed.')
    return
end
% function end
end