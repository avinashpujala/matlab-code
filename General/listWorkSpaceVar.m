function out = listWorkSpaceVar(listOfVarNames,cellStrSearch,cellStrExclude)
%% listWorkSpaceVar lists variables in WorkSpace
% listOfVarNames = whos;
% cellStrSearch
% cellStrExclude
% *** exsmple ***
% Gets variables from current workspace.
% listOfVarNames = whos;
% out = listWorkSpaceVar(listOfVarNames,{'Of';'str'},{'t_'})
% empty string if you search all
% out = listWorkSpaceVar(listOfVarNames,{''},{'t_'})
% *** save variables *** after listWorkSpaceVar function
% nVar=numel(out);
% tic
% for iVar=1:nVar
%   if iVar==1
%     save(filename,out{iVar},'-v7.3')
%   else
%     save(filename,out{iVar},'-append')
%   end
% end
% toc
% reference: % save('test', '-regexp', '^(?!(a|d)$).')
%%
nVar=numel(listOfVarNames);
% Use strfind to do the regexp
nSearch=numel(cellStrSearch);
if nSearch==0 || isempty(nSearch)
  matchTF2=ones(nVar,1);
else
  matchTF = zeros(nVar,nSearch);
  for iSearch=1:nSearch
    matchTF(:,iSearch) = contains( {listOfVarNames.name}, cellStrSearch{iSearch});
  end
  matchTF2=any(matchTF,2);
end
% exclude
nExclude=numel(cellStrExclude);
if nExclude==0 || isempty(nExclude)
  excludeTF2=ones(nVar,1);
else
  excludeTF = zeros(nVar,nExclude);
  for iExclude=1:nExclude
    excludeTF(:,iExclude) = contains( {listOfVarNames.name}, cellStrExclude{iExclude});
  end
  excludeTF2=any(excludeTF,2); % 1 if varNames match exclude names
  excludeTF2=~excludeTF2; % 1 if varNames do not match exclude names
end
% Use find to get the indices of interest
TF=matchTF2.*excludeTF2;
indicesOfVariables = find(TF);
% Dump variables names into a cell array
variablesOfInterest = { listOfVarNames( indicesOfVariables ).name};
out=variablesOfInterest';