
function fileNames = GetFilenames(fileDir, varargin)
% GetFilenames - Given the path to a directory and an optional search
%   string, returns the names of files in the dir that have the seach
%   string in them (case-insensitive). Can also specify a file extension.
% fileNames = GetFileNames(fileDir);
% fileNames = GetFilenames(fileDir,'searchStr',searchStr,'ext',ext);
% Inputs:
% fileDir - Directory in which to look for files
% searchStr - Finds only files with names containing the string, nameMatchStr.
% ext - Finds files with specified extension. By default, searches all
%   extensions. If, however, ext = '/' then obtains the names of
%   subdirectories instead of files. The search string specification still
%   works.
%
% Avinash Pujala, JRC/HHMI, 2016

searchStr = '.';
ext = '*';

for jj = 1:numel(varargin)-1
    if ischar(varargin{jj})
        val = varargin{jj+1};
        switch lower(varargin{jj});
            case 'searchstr'
                searchStr = val;
            case 'ext'
                ext = val;
        end
    end
end

ext(1:strfind(ext,'.'))=[];
dirFlag = false;
if strcmp(ext,'/')
    ext = '*.*' ;
    filesInDir = dir(fullfile(fileDir,ext));
    filesInDir = filesInDir([filesInDir.isdir]);
    dirFlag = true;
else
    ext = ['*.' ext];
    filesInDir = dir(fullfile(fileDir,ext));
end
matchInds =[];
fldrInds = [];
if ~strcmpi(searchStr,'.')
    for fn = 1:length(filesInDir)
        fName = filesInDir(fn).name;
        if ~isempty(strfind(fName,searchStr))
            matchInds = [matchInds;fn];
        end
        if isdir(fullfile(fileDir,fName))
            fldrInds = [fldrInds,fn];
        end
    end
    nonMatchInds = setdiff(1:length(filesInDir),matchInds);
    if dirFlag
        remInds = nonMatchInds;
    else
        remInds = union(nonMatchInds,fldrInds);
    end
    filesInDir(remInds)=[];
    fileNames = {filesInDir.name};
else
    fileNames = {filesInDir.name};
end

end
