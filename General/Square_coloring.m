
% PX = [X1, X2];
% PY = [Y1, Y2];
% Bace : Filled up above this value
% color : [red, green, blue], ex) 'm', 'c'

function Square_coloring(PX, PY, Bace, color)
hold on;

xlimit = get(gca, 'XLim');                                                  % ???????
ylimit = get(gca, 'YLim');                                                  % 
colordflt = [1.0 1.0 0.9];                                                  % 
if nargin<4, color = colordflt; end                                         %   
if nargin<3, Bace = ylimit(1); end                                          % 
if nargin<2, PY = [ylimit(2), ylimit(2)]; end                               % 
if nargin<1, PX = [xlimit(1), xlimit(2)]; end                               % 

Area_handle = area(PX , PY, Bace);                                          % ????([X_1 X_2],[y1 y2])
hold off;                                                                   % 
set(Area_handle,'FaceColor', color);                                        % ??????
set(Area_handle,'LineStyle','none');                                        % ??????????????
set(Area_handle,'ShowBaseline','off');                                      % ???????????
set(gca,'layer','top');                                                     % grid????????????
set(Area_handle.Annotation.LegendInformation, 'IconDisplayStyle','off');    % legend??????????
children_handle = get(gca, 'Children');                                     % Axis?????????????????
set(gca, 'Children', circshift(children_handle,[-1 0]));                    % ????????????

set(gca,'Xlim',xlimit);							    % ?????
set(gca,'Ylim',ylimit);							    %
end