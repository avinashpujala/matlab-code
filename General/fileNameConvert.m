%% solution1
% Rename image file name in matlab - Stack Overflow
% http://stackoverflow.com/questions/2004211/rename-image-file-name-in-matlab

% dirData = dir('F:\Masashi\20160331f2B2\*.bmp');         %# Get the selected file data
% fileNames = {dirData.name};     %# Create a cell array of file names
% for iFile = 1:numel(fileNames)  %# Loop over the file names
%   newName = sprintf('20160331f2B2_%06d.bmp',iFile);  %# Make the new name
%   movefile(fileNames{iFile},newName);        %# Rename the file at the Current Folder
% end
%% solution2 *******only convert the 1st file and the 2nd file super slow and freeze while showing "Busy..."
% convert sequential file names by adding offset values
% filenames - Batch Change File Names in Matlab - Stack Overflow
% http://stackoverflow.com/questions/23267671/batch-change-file-names-in-matlab

list = dir('F:\Masashi\20160331_f2B2test\20160331f2B2_*.bmp'); % # assuming the file names are like 20160331f2B2_000001.bmp
% [pathstr,name,ext] = fileparts(list(1).name);

% offset = -91;  % # numbering offset amount
% name = list(1).name;   % the first filename
% number = sscanf(name,'20160331f2B2_%d.bmp',1);   % # we extract the number
    
for i = 1:length(list)   % # we go through every file name
    name = list(i).name;
    movefile(name,['F:\Masashi\20160331_f2B2test\20160331f2B2_' sprintf('%06d',i-1) '.bmp' ]);   % # we rename the file.
    pause(0.01);
end

% [filename pathname] = uigetfile('*.bmp','MultiSelect','on');
% fullFileName = fullfile(pathname, filename);
% fullFileName;
% fullFileWildCard = strrep(fullFileName, '000000', '*'); % Here you have to match the file name you selected above
% fullFileWileCard;