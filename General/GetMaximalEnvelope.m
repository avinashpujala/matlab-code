function y = GetMaximalEnvelope(x)
%GetMaximalEnvelope - Get the envelope of the maxima in a signal
%   Detailed explanation goes here

pks = GetPks(x);
pks = pks(:)';

origDims = size(x);
x = x(:)';

tt = linspace(0,1,length(x));
t = [0, tt(pks),1];
tt = [0,tt,1];
y_sub = [x(1) x(pks) x(end)];
y = interp1(t,y_sub,tt,'pchip');

y = reshape(y(2:end-1),origDims);


end

