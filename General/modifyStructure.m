% see also deal
% a(1).b =1;
% a(2).b =2;
% [c(1:numel(a)).d] = deal(a.b)

cellLR = 'L'
cellNames = fieldnames(roi.Position.(cellLR))

cellName = cellNames{15}

cellNum = numel(fieldnames(roi.Position.(cellLR).(cellName)))

roi.Position.(cellLR).(cellName).Z155 = roi.Position.(cellLR).(cellName)
roi.DFF.(cellLR).(cellName).Z155 = roi.DFF.(cellLR).(cellName)
for i = 1:cellNum
    roi.Position.(cellLR).(cellName) = rmfield(roi.Position.(cellLR).(cellName),['C' sprintf('%02d',i)])
    roi.DFF.(cellLR).(cellName) = rmfield(roi.DFF.(cellLR).(cellName),['C' sprintf('%02d',i)])
end
