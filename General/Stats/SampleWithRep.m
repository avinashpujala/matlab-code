function varargout = SampleWithRep(X,varargin)
%SampleWithRep Sample a set of data X along the first dimension with
%   replacement
% X_sampled = SampleWithRep(X);
% [X_sampled,sampledInds] = SampleWithRep(X,nIter);
% Inputs:
% X -Data set to sample, must be a vector.
% nIter - Number of iterations (Default = 1);
% Outputs:
% X_sampled - Sampled points; size(X_sampled) = [numel(X),nIter];
% sampledInds - Indices from the original set that were sampled.
%   size(sampledInds) = [numel(X),nIter];
nIter = 1;
if nargin ==2
    nIter = varargin{1};
end

X = X(:);
X_sampled = nan(numel(X),nIter);
sampledInds = X_sampled;
for iter = 1:nIter
    rInds = ceil(rand(numel(X),1)*numel(X));
    rInds(rInds>numel(X))=numel(X);
    sampledInds(:,iter) = rInds(:);
    X_sampled(:,iter) = X(rInds);
end

varargout{1} = X_sampled;
varargout{2} = sampledInds;

end

