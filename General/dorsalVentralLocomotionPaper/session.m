%% Get rid of false swims in data_xls
pks = cell2mat(data_xls(:,end));
pkInts = cell2mat(data_xls(:,end-1));

[label,model,llh] = mixGaussEm(pks(:)',3);
[mu_sort,sortInds] = sort(model.mu,'ascend');
sig_sort = squeeze(model.Sigma);
sig_sort = sig_sort(sortInds);

figure('Name','Peak sorting')
clrs = {'m','g','c'};
for lbl = 1:length(mu_sort)
    inds = find(label ==lbl);
    semilogy(inds,pks(inds),'.','color', clrs{lbl})
%     plot(pkInts(inds),pks(inds),'.','color',clrs{lbl})
    hold on
end
box off
set(gca,'color','k')
ylim([-inf inf])
xlabel('Indices')
ylabel('Distance peaks')

lbl = unique(label);
labelOfInterest = lbl(sortInds(2));
thr_lower = min(pks(label ==labelOfInterest));
thr_upper = max(pks(label ==  labelOfInterest));

keepInds = find(label == labelOfInterest);
data_xls = data_xls(keepInds,:);
