classdef gui2pdf < handle
  properties % public
    topLeft     = 'Project'        % text to be placed in top left location
    topMiddle   = '*companyName*'  % text to be placed in top middle location
    topRight    = '*date*'         % text to be placed in top right (auto date)
    bottomLeft  = '*pageNo*'       % text to be placed in bottom left (auto page number)
    bottomRight = '*pdfFilename*'  % text to be placed in bottom right (auto filename)
    
    
    
    % internal values
    pdfFilename = 'gui2pdf.pdf';    % internal variables used in auto generation process
    companyName = 'Matpi Ltd';     % used in copyright name
    tempPSname = '.gui2pdf.temp.ps';       % temp file used in generation
    pageFormatStr = 'Page %i / %i';% format of page number generation.
    
    % PS format options
    printFormat = '-dpsc2'         % format used in print command (must save to .ps)
    printExtras = {};              % any extra param pairs to pass into print command
    ps2pdfExtras = {'deletePSFile', 1} % extra args to ps2pdf
    openPDF = false;
    progressBar = false;
    userData = struct;
  end
  
  properties (SetAccess = private )
    otherText = cell(0);            % other text items can be added here
  end
  
  properties (Access=private)
    % where the image data is stored.
    imData = cell(0);
    % the extra arg pairs used to further format the text.
    top1Extra = cell(0)
    top2Extra = cell(0)
    top3Extra = cell(0)
    
    bottom1Extra = cell(0)
    bottom2Extra = cell(0)
    bottom3Extra = cell(0)
    % internal flag and handle for page numbers
    incPageNo
    hPage = [];
  end
  methods
    % constructor
    function obj = gui2pdf ( varargin )
      % obj = gui2pdf()
      % text strings can be passed in to preset the txt controls:
      % obj = gui2pdf(topLeft,topMiddle,topRight,bottomLeft,bottomRight)
      % any empty 
      if isempty ( which ( 'ps2pdf.m' ) )
        error ( 'gui2pdf:dependency:ps2pdf', 'This class requires ps2pdf function which can be downloaded from mathworks fileexchage' );
      end
      if nargin >=1; obj.topLeft      = varargin{1}; end
      if nargin >=2; obj.topMiddle    = varargin{2}; end
      if nargin >=3; obj.topRight     = varargin{3}; end
      if nargin >=4; obj.bottomLeft   = varargin{4}; end
      if nargin >=5; obj.bottomRight  = varargin{5}; end
    end
    function obj = addBlankPage ( obj )
      %  obj.addBlankPage()
      % add a blank page.
      obj.imData{end+1} = NaN;
    end
    function obj = addTextToPage ( obj, string, page, position, varargin )
      % obj.addTextToPage ( 'text', pageNo, [x y], argPairs )
      % pageNo == -1 : will add to the last page.
      if page == -1
        page = length ( obj.imData );
      end
      try tIndex = length ( obj.otherText{page} )+1; catch; tIndex = 1; end
      newItem.string = string;
      newItem.position = position;
      newItem.extras = varargin;
      obj.otherText{page}{tIndex} = newItem;
    end
    function obj = captureImage ( obj, hFig )
      % capture an image on HFig: uses getframe (hFig)
      obj.imData{end+1} = getframe ( hFig );
    end
    function obj = addTxtProperty ( obj, control, varargin )
      % obj.addTxtProperty ( txtControlName, arg-pairs )
      %
      % obj.addTxtProperty ( 'topLeft', 'color', 'red' );
      % obj.addTxtProperty ( 'topLeft', 'Position', [0.5 0.5 0] );
      for ii=1:2:length(varargin)
        parameter = varargin{ii};
        value = varargin{ii+1};
        switch control
          case 'topLeft'
            obj.top1Extra{end+1} = parameter;
            obj.top1Extra{end+1} = value;
          case 'topMiddle'
            obj.top2Extra{end+1} = parameter;
            obj.top2Extra{end+1} = value;
          case 'topRight'
            obj.bottom1Extra{end+1} = parameter;
            obj.bottom1Extra{end+1} = value;
          case 'bottomLeft'
            obj.bottom1Extra{end+1} = parameter;
            obj.bottom1Extra{end+1} = value;
          case 'bottomMiddle'
            obj.bottom2Extra{end+1} = parameter;
            obj.bottom2Extra{end+1} = value;
          case 'bottomRight'
            obj.bottom3Extra{end+1} = parameter;
            obj.bottom3Extra{end+1} = value;
          otherwise
            error ( 'gui2pdf:addTxtProperty:format', 'Incorrect control keywork to update txt property' );
        end
      end
    end
    function output = noPages ( obj )
      % obj.noPages()
      output = length ( obj.imData );
    end
    function obj = removePages ( obj, pages )
      % obj.removePages ( [1 4] );
      obj.imData(pages) = [];

      for ii=length(pages):-1:1
        if length ( obj.otherText ) >= pages(ii)
          obj.otherText(pages(ii)) = [];
        end
      end
    end
    function obj = reset ( obj )
      % obj.reset()
      %  resets all stored image data.
      obj.imData = cell(0);
    end
    function obj = writePDF ( obj, filename )
      % obj.writePDF()  % uses obj.pdfFilename
      % obj.writePDF( fileame )
      if nargin == 2
        obj.pdfFilename = filename;
      end
      if exist ( obj.tempPSname, 'file' )
        delete ( obj.tempPSname );
      end
      f = figure ( 'visible', 'off', 'PaperOrientation', 'Landscape', 'PaperUnits', 'normalized', 'PaperPositionMode', 'manual', 'PaperPosition', [0 0 1 1] );
      ax = axes ( 'position', [0 0 1 1], 'xtick', [], 'ytick', [], 'visible', 'off' );
      tx = axes ( 'position', [0 0 1 1], 'xtick', [], 'ytick', [], 'nextplot', 'add', 'visible', 'off' );
      
      % process each of the text controls:
      if ~isempty ( obj.bottomLeft )
        t = text ( 0.02, 0.01, '', 'parent', tx, 'horizontalalignment', 'left', 'interpreter', 'none' );
        obj.UpdateTxtControl ( t, obj.bottom1Extra );
        set ( t, 'string', obj.getString ( 'bottomLeft', t ) );
      end
      t = text ( 0.50, 0.01, '', 'parent', tx, 'horizontalalignment', 'center', 'interpreter', 'none' );
      obj.UpdateTxtControl ( t, obj.bottom2Extra );
      set ( t, 'string', obj.getCodeCopyright() );
      if ~isempty ( obj.bottomRight )
        t = text ( 0.98, 0.01, '', 'parent', tx, 'horizontalalignment', 'right', 'interpreter', 'none' );
        obj.UpdateTxtControl ( t, obj.bottom3Extra );
        set ( t, 'string', obj.getString ( 'bottomRight', t ) );
      end
      if ~isempty ( obj.topLeft )
        t = text ( 0.02, 0.98, '', 'parent', tx, 'horizontalalignment', 'left', 'interpreter', 'none' );      
        obj.UpdateTxtControl ( t, obj.top1Extra );
        set ( t, 'string', obj.getString ( 'topLeft', t ) );
      end
      if ~isempty ( obj.topMiddle )
        t = text ( 0.50, 0.98, '', 'parent', tx, 'horizontalalignment', 'center', 'interpreter', 'none' );
        obj.UpdateTxtControl ( t, obj.top2Extra );
        set ( t, 'string', obj.getString ( 'topMiddle', t ) );
      end
      if ~isempty ( obj.topRight )
        t = text ( 0.98, 0.98, '', 'parent', tx, 'horizontalalignment', 'right', 'interpreter', 'none' );
        obj.UpdateTxtControl ( t, obj.top3Extra );
        set ( t, 'string', obj.getString ( 'topRight', t ) );
      end
      
      % now add the image data
      nPages = length(obj.imData);
      if obj.progressBar
        h = waitbar ( 0, sprintf ( 'Please Wait for PDF Generation ...\n Developed by Matpi Ltd (www.matpi.com)' ), 'Name', 'MAPTI GUI2PDF' );
      end
      for ii=1:nPages
        if isstruct ( obj.imData{ii} )
          imagesc ( obj.imData{ii}.cdata, 'parent', ax )
        else
          set ( ax, 'XLim', [0 1] );
          set ( ax, 'YLim', [0 1] );
          delete ( get ( ax, 'Children' ) );
        end
        set ( ax, 'XTick', [] );
        set ( ax, 'YTick', [] );
        if obj.incPageNo 
          if ishandle ( obj.hPage )
            set ( obj.hPage, 'string', sprintf ( obj.pageFormatStr, ii, nPages ) );
          end
        end
        %%
        extras = [];
        if length ( obj.otherText ) >= ii
          if ~isempty ( obj.otherText{ii} )
            for jj=1:length(obj.otherText{ii})
              newItem = obj.otherText{ii}{jj};
              extras(end+1) = text ( newItem.position(1), newItem.position(2), newItem.string, 'parent', ax ); 
              if ~isempty ( newItem.extras )
                set ( extras(end), newItem.extras{:} );
              end
            end
          end
        end
        
        %%
        print ( f, obj.tempPSname, obj.printFormat, '-append', obj.printExtras{:} );
        delete ( extras );
        if obj.progressBar
          waitbar ( ii/nPages, h );
        end
      end
      delete ( f );
      if ( obj.progressBar ) delete ( h ); end
      %
      if ~isempty ( obj.imData )
        ps2pdf( 'psfile', obj.tempPSname, 'pdffile', obj.pdfFilename, obj.ps2pdfExtras{:} );
        if obj.openPDF
          open (  obj.pdfFilename )
        end
      else 
        error ( 'gui2pdf:writePDF:nodata', 'No captured data was available to be saved to a PDF file' );
      end
      obj.incPageNo = false;
    end
  end
  methods ( Access = private )
    % get the copyright code of the owner.
    function output = getCodeCopyright ( obj )
      output = sprintf ( 'PDF Generated using code Copyright (%s) to Matpi Ltd ', char(169) );
    end
    % get the string property
    function output = getString ( obj, var, h )
      output = obj.(var);
      switch output
        case '*companyName*'
          output = sprintf ( '%s Copyright %s', char ( 169 ), obj.companyName );
        case '*date*'
          output = datestr(now);
        case '*pageNo*'
          output = '';
          obj.incPageNo = true;
          obj.hPage = h;
        case '*pdfFilename*'
          output = obj.pdfFilename;
      end
      if isnumeric ( output )
        output = num2str ( output );
      end
    end
    % add any extra arg pairs to the txt control
    function obj = UpdateTxtControl ( obj, t, extras )
      for ii=1:2:length ( extras )
        try
          set ( t, extras{ii}, extras{ii+1} );
        catch le
          fprintf ( 2, 'gui2pdf:extra:%s\n', le.message );
        end
      end
    end
  end
end




