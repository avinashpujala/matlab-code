
%% Max amps
stimType = 'spont_fast';
ablGrp = 'Alx_young';
% trtmnt = 'ctrl';

% xl = 200;
% yl = 200;

xl = 200;
yl = 200;

close all

dMat = cell(2,1);
X = cell(2,1);
Y = cell(2,1);
bWid = cell(2,1);
for trt = 1:2
    if trt ==1
        trtmnt = 'ctrl';
    else
        trtmnt = 'abl';
    end
    inds_keep = find(strcmpi(grpData.stimType,stimType) & ...
        strcmpi(grpData.ablationType,ablGrp) &...
        strcmpi(grpData.ablationBool,trtmnt) &(grpData.bendNum ==1));
%     x = grpData.headBendAmpMax(inds_keep);
%     y = grpData.tailBendAmpMax(inds_keep);
    
    x = grpData.headBendVelMax(inds_keep);
    y = grpData.tailBendVelMax(inds_keep);
    
    inds_keep = ~isnan(x) & ~isnan(y);
    inds_keep = (x >4) & (y >4);
    x = x(inds_keep);
    y = y(inds_keep);
    
    countMat = dataDensity(x,y,xl,yl);
    probMat  = countMat/numel(x);
    
    ker = gausswin(20)*gausswin(20)';
    ker = ker/sum(ker);
    
    figure
    pcolor(1:xl,1:yl,probMat),shading interp
    box off
    title(['Probability, ' ablGrp ', ' stimType ', ' trtmnt])
    xlabel('Max head bend amp')
    ylabel('Max tail bend amp')
    
    figure
    % probMat_conv = conv2(probMat,ker,'same');
    [bWid{trt},dMat{trt},X{trt},Y{trt}] = kde2d([x(:),y(:)],250,[0,0],[xl,yl]);   
    % pcolor(1:xl,1:yl,probMat_conv), shading interp
    pcolor(X{trt},Y{trt},dMat{trt}),shading interp
    xlim([0,xl])
    ylim([0,yl])
    box off
    title(['Probability - kde, ' ablGrp ', ' stimType ', ' trtmnt])
    xlabel('Max head bend amp')
    ylabel('Max tail bend amp')    
end
figure('Name','Contour plots of control and ablated')
contour(X{1},Y{1},dMat{1},5,'b','LineWidth',2)
hold on
contour(X{2},Y{2},dMat{2},5,'r','LineWidth',2)
xlim([0, xl])
ylim([0, yl])
box off
legend('Control','Ablated')
title(['Probability - kde contour, ' ablGrp ', ' stimType ', ' 'Control vs Ablated'])
xlabel('Max head bend amp')
ylabel('Max tail bend amp')

%% All amps
stimType = 'spont_fast';
ablGrp = 'Alx_old';
% trtmnt = 'ctrl';

xl = 120;
yl = 120;

% xl = 80;
% yl = 120;

% close all

dMat = cell(2,1);
X = cell(2,1);
Y = cell(2,1);
bWid = cell(2,1);
for trt = 1:2
    if trt ==1
        trtmnt = 'ctrl';
    else
        trtmnt = 'abl';
    end
    inds_keep = find(strcmpi(grpData.stimType,stimType) & ...
        strcmpi(grpData.ablationType,ablGrp) &...
        strcmpi(grpData.ablationBool,trtmnt));
    %     x = grpData.headBendAmps(inds_keep);
    %     y = grpData.tailBendAmps(inds_keep);
    
    inds_keep = find(strcmpi(grpData.stimType,stimType) & ...
        strcmpi(grpData.ablationType,ablGrp) &...
        strcmpi(grpData.ablationBool,trtmnt) & grpData.bendNum==1);
    
    x = grpData.headBendVelMax(inds_keep);
    y = grpData.tailBendVelMax(inds_keep);
    
    inds_keep = ~isnan(x) & ~isnan(y);
    inds_keep = (x >4) & (y >4);
    x = x(inds_keep);
    y = y(inds_keep);
    
    countMat = dataDensity(x,y,xl,yl,[0,xl,0,xl]);
    probMat  = countMat/numel(x);
    xx = linspace(0,xl,size(probMat,2));
    yy = linspace(0,yl,size(probMat,2));
    
    ker = gausswin(20)*gausswin(20)';
    ker = ker/sum(ker);
    
    figure
    pcolor(xx,yy,probMat),shading interp
    box off
    title(['Probability, ' ablGrp ', ' stimType ', ' trtmnt])
    xlabel('Max head bend amp')
    ylabel('Max tail bend amp')
    
    figure
%     probMat_conv = conv2(probMat,ker,'same');
    [bWid{trt},dMat{trt},X{trt},Y{trt}] = kde2d([x(:),y(:)],256,[0,0],[xl,yl]);   
    % pcolor(1:xl,1:yl,probMat_conv), shading interp
    pcolor(X{trt},Y{trt},dMat{trt}),shading interp
    xlim([0,xl])
    ylim([0,yl])
    box off
    title(['Probability - kde, ' ablGrp ', ' stimType ', ' trtmnt])
    xlabel('Max head bend amp')
    ylabel('Max tail bend amp')    
end
figure('Name','Contour plots of control and ablated')
contour(X{1},Y{1},dMat{1},5,'b','LineWidth',2)
hold on
contour(X{2},Y{2},dMat{2},5,'r','LineWidth',2)
xlim([0, xl])
ylim([0, yl])
box off
legend('Control','Ablated')
title(['Probability - kde contour, ' ablGrp ', ' stimType ', ' 'Control vs Ablated'])
xlabel('Max head bend amp')
ylabel('Max tail bend amp')


%% Total swim distance
stimType = 'spont_fast';
ablGrp = 'Alx_young';
% trtmnt = 'ctrl';

xl = 200;
yl = 200;

% xl = 80;
% yl = 120;

% close all
dMat = cell(2,1);
X = cell(2,1);
Y = cell(2,1);
bWid = cell(2,1);
figure
for trt = 1:2
    if trt ==1
        trtmnt = 'ctrl';
    else
        trtmnt = 'abl';
    end
    inds_keep = find(strcmpi(grpData.stimType,stimType) & ...
        strcmpi(grpData.ablationType,ablGrp) &...
        strcmpi(grpData.ablationBool,trtmnt));
%     x = grpData.headBendAmpMax(inds_keep);
%     y = grpData.tailBendAmpMax(inds_keep);
      y = grpData.swimDist_total(inds_keep);
      y(isnan(y))=[];
    
      [prob, vals] = hist(y,50);
      prob = prob/sum(prob);
      plot(vals,prob)
      hold on          

end
title(['Total swim distance/episode, '  ablGrp])
xlabel('Swim distance (mm)')
ylabel('Prob')
legend('Ctrl' ,'Abl')
xlim([-inf inf])

%% Scatter plot, distance from the edge

scatter(grpData.headBendAmpMax,grpData.tailBendAmpMax,5, grpData.distFromEdge_min)
xlabel('Head bend amplitude max')
ylabel('Tail bend amplitude max')
title('Scatter plot of head bend amplitude max vs tail bend amplitude max, color-coded by distance from edge')
colormap(jet)
shg

%%
fishNum = 6;
% inds_keep = find(grpData.fishNum==fishNum & grpData.bendNum ==1);
inds_keep = find(grpData.fishNum==fishNum & grpData.bendNum ==1);
grpName = unique(grpData.ablationType(inds_keep));
stimType  = unique(grpData.stimType(inds_keep));
trtmnt = unique(grpData.ablationBool(inds_keep));

x = grpData.headBendAmpMax(inds_keep);
y = grpData.tailBendAmpMax(inds_keep);

% figure
scatter(x,y)
hold on
shg
disp([grpName,stimType,trtmnt])

