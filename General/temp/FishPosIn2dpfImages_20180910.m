

%% Inputs
imgDir = 'T:\Behavior Rig\Minoru\Escape development\2016-5-4-Alx-Kaded-DOB-2016-5-2\needle_500Hz';
imgInds = 7169:10889;
nFramesInTrl = [];
nBlocks = 1;
imgExt = 'bmp';
headDiam = 1.5; % In mm
tryCorrect = true;
arenaDiam = 50; % In mm
cropWid = 90; % In pxls
saveDir = fullfile(imgDir,'proc\saved');
if exist(saveDir,'dir')~=7
    mkdir(saveDir)
end

%%
I = ReadImgSequence(imgDir,imgExt,imgInds);
[I_back,ref] = SubtractBackground(I);

pxlSize = GetPxlSpacing(ref,'diam', arenaDiam);

kerSize = ceil(headDiam/pxlSize);
fp = GetFishPos_180307(I_back,'kerSize',kerSize,'process','parallel',...
    'tryCorrect', tryCorrect);

I_crop = CropImgsAroundPxl(I,fp,cropWid,'procType','parallel');
I_back_crop = CropImgsAroundPxl(I_back, fp, cropWid, 'procType','parallel');

SaveImages(I_crop,saveDir,'Escape','imgFormat','series','ext','tif');