
%% Inputs
trlDir = 'S:\Avinash\Ablations and behavior\Alx\2018\Jan\20180128-AlxKaede-conv 24hpf-abl old 60hpf-behav 5dpf\f1_ctrl\fastDir_01-28-18-232325\dark';
% trlDir = 'S:\Avinash\Ablations and behavior\Intermediate RS\20160901\Behavior\Fish4_ctrl2\fastDir_09-02-16-011042\dark';
% trlDir  = 'S:\Avinash\Ablations and behavior\Alx\2018\Jan\20180128-AlxKaede-conv 24hpf-abl old 60hpf-behav 5dpf\f1_ctrl\fastDir_01-28-18-232325\vib'
searchStr = 'Trl';


%% Go through each trial directory and track

% Get the names of all the trial directories
dirNames = GetFilenames(trlDir,'ext','/','searchStr',searchStr);

% Go into each trial directory and if solution (Features_... directory)
% doesn't exist then track, else skip to the next trial directory

for trl = 1:length(dirNames)
    trlDir_sub = fullfile(trlDir,dirNames{trl});
    subDirNames = GetFilenames(trlDir_sub,'ext','/','searchStr','Features');  
    if isempty(subDirNames) || (length(dir(fullfile(trlDir_sub,subDirNames{1}))) < 500)
        tic
        demse_autoMODAL_ap
        % Finally, get shape metrics, append to PAR and ManualFit and save
        disp('Getting shape metrics...')
        [PAR, ManualFit] = ShapeMetrics(PAR,ManualFit);    
    else
        disp(['Solution for trial # ' num2str(dirNames{trl}) ' seems to already exist. Skipping!'])
    end   
    toc
end