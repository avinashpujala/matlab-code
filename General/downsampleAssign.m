% function [timeSeriesOut trigFrameOut] = downsampleAssign(timeSeriesIn,frameRateBef,frameRateAft,trigFrameBef,trigFrameAft)
%% downsampleAssign reduces sampling rate
% Input and output Frame rate is user-specified.
% High-frame-rate time-series data is converted to low-frame-rate time-series
% by rolling average.
% The input variable, trigFrame, specifies at which frame the function
% defines the onset of rolling average.
% Inputs
% 
% Outputs
% 
% Example
% timeSeriesIn = ;
% frameRateBef = 50.1756;
% frameRateAft = 11.5902;
% trigFrameBef = 501.7811;
% trigFrameAft = ;
% 
%% 
if frameRateOut > frameRateIn
    display('frameRateOut must be equal or smaller than frameRateIn')
    return
end
nFrameBef = numel(timeSeriesIn);
nFrameAft = numel()

timeSeriesOut = ;

timingAftPostTrig = [1:nFrameAft-floor(trigFrameAft)]/frameRateAft;
timingBefPostTrig = [1:nFrameBef-floor(trigFrameAft)]/frameRateBef;
i=1 %nFrameAft-floor(trigFrameAft)
    if i ==1
        befIdx = find(timingAftPostTrig(i) > timingBefPostTrig); % includes the edge frame, the largest frame is the edge frame
        nBefIdx = numel(befIdx);
        sum = single(zeros(1,1));
        for befIdx
            sum = sum + timeSeriesIn(floor(trigFrameBef)+befIdx); % includes the edge frame, subtract later
        end
        sum = sum - timeSeriesIn(floor(trigFrameBef)+max(befIdx,[],2))/2 % subtract weight of edge frame is weighed 0.5
        timeSeriesOut(floor(trigFrameAft)+i) = sum / (nBefIdx-0.5) ; % edge frame is weighed 0.5
    elseif
        
    else
        befIdx = find((timingAftPostTrig(i) > timingBefPostTrig).* ...
            (timingBefPostTrig > timingAftPostTrig(i-1))); % not includes the early edge frame, but include the late edge frame
        nBefIdx = numel(befIdx);
        sum = single(zeros(1,1));
        for befIdx
            sum = sum + timeSeriesIn(floor(trigFrameBef)+befIdx);
        end
        sum = sum + timeSeriesIn(floor(trigFrameBef)+min(befIdx,[],2)-1)/2 % add weight of the early edge frame, edge frame is weighed 0.5
        sum = sum - timeSeriesIn(floor(trigFrameBef)+max(befIdx,[],2)+1)/2 % subtract weight of the late edge frame, weighed 0.5
        timeSeriesOut(floor(trigFrameAft)+i) = sum / (nBefIdx+0.5-0.5) ; % edge frame is weighed 0.5
    end
    
    
        
        
        

        