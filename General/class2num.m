function number = class2num(classChar)
% 'single'	Single-precision number
% 'double'	Double-precision number
% 'int8'	Signed 8-bit integer
% 'int16'	Signed 16-bit integer
% 'int32'	Signed 32-bit integer
% 'int64'	Signed 64-bit integer
% 'uint8'	Unsigned 8-bit integer
% 'uint16'	Unsigned 16-bit integer
% 'uint32'	Unsigned 32-bit integer
% 'uint64'	Unsigned 64-bit integer
% 'logical'

if strcmp(classChar, 'int8') || strcmp(classChar, 'uint8')
    number = 8;
elseif strcmp(classChar, 'int16') || strcmp(classChar, 'uint16')
    number = 16;
elseif strcmp(classChar, 'int32') || strcmp(classChar, 'uint32')
    number = 32;
elseif strcmp(classChar, 'int64') || strcmp(classChar, 'uint64')
    number = 64;
else 
    number = 0;
end
end % function ends