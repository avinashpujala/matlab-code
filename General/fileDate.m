function [Y, M, D, H, MN, S] = fileDate(fileInfo)
%% Get individual components of date & time in 1 Sec resolution
% % example
% fileInfo = dir(dirRead); % fileInfo structure will contain fileInfo
% [Y, M, D, H, MN, S] = fileDate(fileInfo(fileIdx));
% % original code was online help
% FileInfo = dir('YourFileWithExtention');
% [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
%%
[Y, M, D, H, MN, S] = datevec(fileInfo.datenum);

end