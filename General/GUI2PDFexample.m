% 
% GUI2PDF example
% create the gui2pdf object 
obj = gui2pdf( 'DEMO - Example' );
% createa your gui
da = figure;
ax = axes ( 'visible', 'off' );
text ( 0.5, 0.7, 'Demo GUI2PDF Generation', 'parent', ax, 'horizontalalignment', 'center' );
text ( 0.5, 0.5, 'Code Developed By Matpi Ltd.', 'parent', ax, 'horizontalalignment', 'center' );
% capture the image
obj.captureImage ( da );
cla(ax);
set ( ax, 'visible', 'on' );
x = -pi:0.01:pi;
% now do some random plotting
for i=1:10
  plot ( ax, x, sin(5*i*x) );
  obj.captureImage ( da );
end
delete ( da );
% once plotting has finished update some of the txt properties
obj.addTxtProperty ( 'topLeft', 'color', 'red' );
obj.addTxtProperty ( 'topLeft', 'FontSize', 12 );
% add a blank page
obj.addBlankPage();
% add some custom text to that page only
obj.addTextToPage ( 'FINISHED!!!', -1, [0.1 0.4], 'color', 'blue' );
% multiple text objects can be added
obj.addTextToPage ( 'multiple text objects can be added to a page', -1, [0.1 0.2], 'color', 'blue', 'interpreter', 'tex' );
%
% Update some PDF generation parameters
obj.openPDF = true;
obj.progressBar = true;
% write the PDF
obj.writePDF();
