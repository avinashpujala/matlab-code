function y_flt = sstfilt(y,Fs,freqRange)
%sstfilt Banpass filtering using SST
% y_flt = sstfilt(y,Fs,freqRange);
% Inputs:
% y - Signal to filter
% Fs - Sampling frequency
% freqRange - 2-element vector indicating the bandpass frequency range

[S,freq] = wsst(y,Fs);
y_flt = iwsst(S,freq,freqRange);

end

