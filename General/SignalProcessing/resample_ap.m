
function X_resampled = resample_ap(X,varargin)

%resample_ap Resamples a signal at a different sampling rate.
% I wrote this function as an alternative to MATLAB's "resample", which I
%   like because of the distortion it can cause to the signal at the ends,
%   perhaps because I don't know how to use it properly. In any case, this
%   is a simpler alternative
% X_resampled = resample_ap(X,Fs_new, Fs);
% X_resampled = resample_ap(X,N);

% Inputs:
% X - N x K signal matrix, where N is the number of points in a signal and K is
%   the number of signals
% Fs_new - New sampling rate
% Fs - Current sampling rate
% N - Desired length of the vector after resampling
%
% Avinash Pujala, JRC, 2018

if isvector(X)
    X = X(:);
end

t = 1:size(X,1);
if numel(varargin)>1
    Fs_new = varargin{1};
    Fs = varargin{2};
    tt = linspace(t(1),t(end),round(length(t)*Fs_new/Fs));
elseif numel(varargin)==1
    N = varargin{1};
    tt = linspace(t(1),t(end),N);
end

X_resampled = zeros(length(tt),size(X,2));
if ndims(X)==2 && all(size(X)>1)
    for jj = 1:size(X,2)
        X_resampled(:,jj) = interp1(t,X(:,jj),tt,'spline');
    end
else
    X_resampled = interp1(t,X,tt,'spline');
end

end



