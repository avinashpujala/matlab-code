function out = searchFieldName(in,searchName)
%% searchField searches fieldname in a structure
% out = searchFieldName(structure, 'zoom')

%%
fn{1} = fieldnames(in);
for i=1:numel(fn{1})
    t_fn{1}=fn{1}{i};
    if isstruct(in.(t_fn{1}))
        fn{2}=fieldnames(in.(t_fn{1}));
        for ii=1:numel(fn{2})
            t_fn{2}=fn{2}{ii};
            if strfind(lower(t_fn{2}),lower(searchName)) > 0
                i
                ii
                in.(t_fn{1})
                fprintf([searchName ' is found in the ' t_fn{1} 'field\n'])
                fprintf([t_fn{1} '.' t_fn{2} ' is ' num2str(in.(t_fn{1}).(t_fn{2})) '\n'])
            end
        end
    end
end
end