function [curve_sorted,dist_order] = SortCurvePoints(varargin)
%SortCurvePoints - When given a set of points that lie on a curve in an image
%   (such as obtained by bwmorph(img,'thin',inf), for instance), if the
%   points are not in order going from one of the curve to the other, then
%   this function orders them that way.
% curve_sorted = SortCurvePoints(curve);
% [curve_sorted, dist_order] = SortCurvePoints(curve,startPt);
% 
% Avinash Pujala, JRC/Koyama lab, 2018

curve = varargin{1};
if nargin == 1
    startPt = curve(1,:);
else
    startPt = varargin{2};
end

S = @(v,V) sqrt(sum((repmat(v,size(V,1),1)-V).^2,2));
pts_new = curve;
s = S(startPt,pts_new);
s_new = s;
count= 0;
curve_sorted = zeros(size(curve));
dist_order = zeros(size(s));
while length(s_new)>=1
    count = count + 1;
    [d, ind] = min(s_new);
    dist_order(count)=d;
    curve_sorted(count,:) = pts_new(ind,:);
    startPt = pts_new(ind,:);  
    pts_new(ind,:) = [];
    s_new = S(startPt,pts_new);
end
zerInds = find(curve_sorted(:,1)==0 | curve_sorted(:,2)==0);
curve_sorted(zerInds,:) = [];
dist_order(zerInds) = [];
curve_sorted = cat(1,startPt,curve_sorted);

%=== Circularly shift points such that the first and last points of the
%   curve end up being the end points. Here, the assumption is that the end
%   points much have the greatest distance between them
dS = DeltaDistance(curve_sorted);
[~, endInd] = max(dS);
curve_sorted = circshift(curve_sorted,[-endInd,0]);

end


function [curve_sorted, distVec] = SortCurvePoints_optional(curve)

[curve_ord,s_ord] = SortCurvePoints(curve(1,:),curve);
[~,jumpInd] = max(s_ord);
if jumpInd > length(s_ord)-jumpInd
    curve_ord(jumpInd:end,:)=[];
    s_ord(jumpInd:end) = [];
else
    curve_ord(1:jumpInd-1,:)=[];
    s_ord(1:jumpInd-1) = [];
end
%###
if ~isempty(curve_ord)
    curve_ord = [round(curve(1,:)); curve_ord];
    s_ord = [0; s_ord];
end

distVec = cumsum(s_ord);

end