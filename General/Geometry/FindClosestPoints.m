function varargout = FindClosestPoints(set1,set2)
%FindClosesPoints - Given two sets of points (coordinates; currently only in 2D),
%   returns two points, one from each set such that the distance between
%   these two points is thes shortest between any two such points.
% closestPoints = FindClosestPts(set1,set2);
% [closestPoints, minDist,D] = FindClosestPts(set1,set2);
% Inputs:
% set1 - Coordinates (in 2D) of the 1st set of points. Size  = [N, 2], where N is
%   the number of points
% set2 - Coordinates of the 2nd set of points. Size = [N, 2]
% Outputs:
% closestPoints - Two closest points. Cell array of length 2. The 1st cell
%   holds all the closest points from set 1 whereas the 2nd cell holds the
%   closest points from the second set 
% minDist - The distance between the closest points
% D - The distances matrix
% 
% Avinash Pujala, Koyama lab/JRC, 2018

if (size(set1,2) >2) || (size(set2,2) >2)
    error('The dimensions of the inputs are incorrect! The size of 2nd dimension must be 2')
end

X_one = repmat(set1(:,1),1,size(set2,1));
Y_one = repmat(set1(:,2),1,size(set2,1));

X_two = repmat(set2(:,1)',size(set1,1),1);
Y_two = repmat(set2(:,2)',size(set1,1),1);

D = sqrt((X_one-X_two).^2 + (Y_one-Y_two).^2);

[r,c] = find(D == min(D(:)));

minDist = SparseIndex(D,r,c);
closestPts{1} = set1(r,:);
closestPts{2} = set2(c,:);

varargout{1} = closestPts;
varargout{2} = minDist;
varargout{3} = D;
end

