function varargout = MakeEllipses_concentric(a,b, varargin)
%MakeEllipses_concentric - A simple function to generate a set of concentric ellipses based on
%   specified major and minor axis lengths, and other parameters for the
%   outermost ellipse. This was originally conceived as a substitute for
%   generating a filled ellipse
%
% ellipses = MakeEllipses_concentric(a,b);
% [ellipses, foci] = MakeEllipses_concentric(a,b,[cx,cy],angle)
% Inputs:
% a = width in pixels
% b = height in pixels
% [cx,cy] = [horizontal center, vertical center]
% angle =  orientation of ellipse in degrees
% Ouputs:
% ellipses - Coordinates of the concentric ellipses, N x 2, where N is the # of points
%   making up the ellipses. The 2 columns are the x- and y coordinates respectively.
% foci - The coordinates of the two foci of the outermost ellipse, 2 X 2, where
%   foci(1,:) are the x and y coordinates of the first focus and f(:,2) of
%   the 2nd focus.


theta = 0;
[cx,cy] = deal(0);

if nargin  ==3
    cx = varargin{1}(1);
    cy = varargin{1}(2);
elseif nargin==4
    cx = varargin{1}(1);
    cy = varargin{1}(2);
    theta = varargin{2};
end

theta=-theta/180*pi;

alpha=[cos(theta) -sin(theta)
    sin(theta) cos(theta)];

r=0:0.1:2*pi+0.1;
meshpts = linspace(0,1,ceil(max(a,b)));
ellipses = nan(numel(r),2,numel(meshpts));
count = 0;
for jj = meshpts
    count = count + 1;
    ellipse = [jj*(a*cos(r))' jj*(b*sin(r))']*alpha;
    ellipse(:,1) = ellipse(:,1) + cx;
    ellipse(:,2) = ellipse(:,2) + cy;
    ellipses(:,:,count) = ellipse;
end
ellipses = reshape(permute(ellipses,[1,3,2]),[numel(r)*numel(meshpts),2]);

m = sqrt(max(a,b)^2-min(a,b)^2);
th = -theta;
if a<b
   th = -theta+(pi/2);
end
c = [cx,cy];
f1 = c - m*[cos(th),sin(th)];
f2 = c + m*[cos(th),sin(th)];
foci = [f1; f2];

varargout{1} = ellipses;
varargout{2} = foci;

end

