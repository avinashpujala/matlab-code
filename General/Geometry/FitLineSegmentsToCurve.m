function varargout = FitLineSegmentsToCurve(curve,nSegs,varargin)
%FitLineSegmentsToCurve When given a curve, fits specified number of line
%   segments to the curve and returns these lines along with optional
%   outputs such as the angles between these lines (0-360 degrees).
% curve_fit = FitLineSegmentsToCurve(curve,nSegs);
% [curve_fit, thetas] = FitLineSegmentsToCurve(curve,nSegs);
% Inputs:
% curve = Curve to fit; size(curve) = [N, 2], where N is the number of
%   points, and the 2 columns are the x- and y- coordinates of the curve
%   respectively.
% nSegs - Number of line segments to fit
% removeNans - Boolean; If true, then interpolates the original curve
%   by excluding NaNs and then fits the line segents. Default = true.
% Outputs:
% curve_fit - Line segments fit to the curve. Curve_fit is a cell array of
%   length = nSegs. Each cell contains a matrix of size = [k, 2]where k is 
%   the length of each segment and the 2 columns correspond to the x- and
%   y-coordinates respectively.
% thetas - Angles between the lines in degrees and range [0, 360]; 
%   thetas is a vector of length nSegs-1.
% or = Absolute angles of the fit line segments in the range [-180 180];
%   size(or) = [nSegs,1].
% 
% Avinash Pujala, Koyama lab/JRC, 2018

removeNans = true;

if nargin ==3
    removeNans = varargin{1};
end


inds_sub = ceil(linspace(1,size(curve,1),nSegs+1));
inds_sub(inds_sub>size(curve,1))= size(curve,1);

C = zeros(nSegs,1);
curve_fit = cell(nSegs,1);
or = C;
for seg = 1:length(inds_sub)-1
    curve_seg = curve(inds_sub(seg):inds_sub(seg+1),:);
     n = 1:size(curve_seg,1); 
    for jj = 1:size(curve_seg,2)
        if removeNans && sum(isnan(curve_seg(:,jj)))                     
            nanInds = isnan(curve_seg(:,jj));
            if (size(curve_seg,1)-sum(nanInds))>3
                yy = interp1(n(~nanInds),curve_seg(~nanInds,jj),n(:),...
                    'spline','extrap');
                curve_seg(:,jj) = yy(:);
            else
                disp('Not enough points to interpolate after removing NaNs')
            end            
        end
    end
    t = linspace(0,1,size(curve_seg,1));
    X = [ones(length(t),1),t(:)];
    M = lscov(X,curve_seg);
    curve_pred  = X*M;
    curve_fit{seg} = curve_pred;
    curve_pred = curve_pred - repmat(curve_pred(1,:),size(curve_pred,1),1);
    C(seg) = curve_pred(end,1) + curve_pred(end,2)*1i;
%     or(seg) = angle(C(seg))*180/pi;
    or(seg) = mod(angle(C(seg))+2*pi,2*pi)*180/pi; %[0, 360]
end
thetas = angle(C(1:end-1).*conj(C(2:end)))*180/pi;

varargout{1} = curve_fit;
varargout{2} = thetas;
varargout{3} = or;
end

