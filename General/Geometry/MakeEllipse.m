function varargout = MakeEllipse(a,b, varargin)
%MakeEllipse - A simple function to generate an ellipse based on specified
%major and minor axis lengths, and other parameters
%
% ellipse = MakeEllipse(a,b);
% [ellipse, foci] = MakeEllipse(a,b,[cx,cy],angle)
% Inputs:
% a - width in pixels
% b - height in pixels
% [cx,cy] - [horizontal center, vertical center]
% angle -  Orientation of ellipse in degrees
% Outputs:
% ellipse - Coordinates of the ellipse, N x 2, where N is the # of points
%   making up the ellipse. The 2 columns are the x- and y coordinates respectively.
% foci - The coordinates of the two foci of the ellipse, 2 X 2, where
%   foci(1,:) are the x and y coordinates of the first focus and f(:,2) of
%   the 2nd focus.

theta = 0;
[cx,cy] = deal(0);

if nargin  ==3
    cx = varargin{1}(1);
    cy = varargin{1}(2);
elseif nargin==4
    cx = varargin{1}(1);
    cy = varargin{1}(2);
    theta = varargin{2};
end

theta=theta/180*pi;

r=0:0.1:2*pi+0.1;
p=[(a*cos(r))' (b*sin(r))'];

alpha=[cos(theta) -sin(theta)
    sin(theta) cos(theta)];

ellipse = p*alpha;
ellipse(:,1) = ellipse(:,1) + cx;
ellipse(:,2) = ellipse(:,2) + cy;

m = sqrt(max(a,b)^2-min(a,b)^2);
th = -theta;
if a<b
   th = -theta+(pi/2);
end
c = [cx,cy];
f1 = c - m*[cos(th),sin(th)];
f2 = c + m*[cos(th),sin(th)];
foci = [f1; f2];

varargout{1} = ellipse;
varargout{2} = foci;

end

