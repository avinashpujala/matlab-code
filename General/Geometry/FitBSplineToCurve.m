function varargout = FitBSplineToCurve(curve,varargin)
%FitBSplineToCurve Given a curve and some input parameters, returns a
%   smoothened curve after fitting with B-Spline periodic functions
% curve_fit = FitBSplineToCurve(curve);
% curve_fit = FitBSplineToCurve(curve,order,resolution,length);
% [curve_fit,N,BB,D1,D2] = FitBSplineToCurve(...)
% Inputs:
% curve - The curve to fit.
% order - Order of the B-splines. If [], then defaults to 4.

% smoothness - Larger values lead to less smooth curves (Default = 6).

%   If [], then uses default.
% length - Desired length of the output curve (default = 60). If [],
%   then length is same as input curve.
% Outputs:
% curve_fit - Curve obtained from fitting.
% N - B-spline basis used to fit.
% BB - Coefficients, such that curve_fit = N*BB;
% D1 - 1st derivative of the basis to get tangents
% D2 - 2nd derivative of the basis
% 
% Avinash Pujala, Koyama lab/JRC, 2018
% Based on scripts from Fontaine et al(see auto_init.m)

% Defaults
order = 4;
smoothness = 6;
len = size(curve,1);

if nargin == 2
    if ~isempty(varargin{1})
        order = varargin{1};
    end    
elseif nargin == 3
    if ~isempty(varargin{1})
        order = varargin{1};
    end
    if ~isempty(varargin{2})
        smoothness = varargin{2};
    end
elseif nargin == 4
    if ~isempty(varargin{1})
        order = varargin{1};
    end
    if ~isempty(varargin{2})
        smoothness = varargin{2};
    end
    if ~isempty(varargin{3})
        len = varargin{3};
    end
end

mLen = size(curve,1);
npts = max(round(mLen/smoothness),order);
% dLen = len-mLen;
t = linspace(0,1,mLen);
tt = linspace(0,1,len);
curve_spline = interp1(t(:),curve,tt(:));


% xx = spline(t,curve(:,1),tt);
% yy = spline(t,curve(:,2),tt);


knotv = oknotP(npts,order,1,0);
T = linspace(-1,0,len);
[N,D1,D2] = dbasisP(order,T,npts,knotv);
% BB = pinv(N)*[xx(:),yy(:)];
BB = pinv(N)*curve_spline;
curve_fit = N*BB;

varargout{1} = curve_fit;
varargout{2} = N;
varargout{3} = BB;
varargout{4} = D1;
varargout{5} = D2;

end

