function [] = relocateField()
%% Move field to another filed (and remove the original field)

% To remove field from a structure,
% ach20160908.f1.SC.cell002 = rmfield(ach20160908.f1.SC.cell002,'soma')

%%
for i=1:2
if i==1
    cellLR = 'L';
    cellList.L = fieldnames(roi.Position.(cellLR));
elseif i==2
    cellLR = 'R';
    cellList.R = fieldnames(roi.Position.(cellLR));
end

for k=1:numel(cellList.(cellLR))
cellName = cellList.(cellLR){k}

if numel(roi.Position.(cellLR).(cellName))==1
    fieldnames(roi.Position.(cellLR).(cellName))
    display(['Selected cell type contains ' num2str(t_nCells) ' cells in the roi structure array'])
else
    % This causes a warning: Struct field assignment overwrites a value with class "double".
    roi.Position.(cellLR).(cellName).C1 = roi.Position.(cellLR).(cellName);
end
end
end
% END function
end